﻿using System.Collections.Generic;
using Nop.Core.Domain.Blogs;
using Nop.Plugin.WebApi.Models.Blogs;


namespace Nop.Plugin.WebApi.Factories
{
    /// <summary>
    /// Represents the interface of the blog model factory
    /// </summary>
    public partial interface IBlogModelFactory
    {
      
        /// <summary>
        /// Prepare blog post model
        /// </summary>
        /// <param name="model">Blog post model</param>
        /// <param name="blogPost">Blog post entity</param>
        /// <param name="prepareComments">Whether to prepare blog comments</param>
        void PrepareBlogPostModel(BlogPostModel model, BlogPost blogPost, bool prepareComments);

        /// <summary>
        /// Prepare blog post list model
        /// </summary>
        /// <param name="command">Blog paging filtering model</param>
        /// <returns>Blog post list model</returns>
        BlogPostListModel PrepareBlogPostListModel(BlogPagingFilteringModel command);

      
       
    }
}