﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.WebApi.Infrastructure.Cache;
using Nop.Plugin.WebApi.Models.Catalog;
using Nop.Plugin.WebApi.Models.Category;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.Topics;
using Nop.Services.Vendors;

namespace Nop.Plugin.WebApi.Factories
{
	public partial class ApiCatalogModelFactory : IApiCatalogModelFactory
	{

		#region Fields

		private readonly IProductModelFactory _productModelFactory;
		private readonly ICategoryService _categoryService;
		private readonly IManufacturerService _manufacturerService;
		private readonly IProductService _productService;
		private readonly IVendorService _vendorService;
		private readonly ICategoryTemplateService _categoryTemplateService;
		private readonly IManufacturerTemplateService _manufacturerTemplateService;
		private readonly IWorkContext _workContext;
		private readonly IStoreContext _storeContext;
		private readonly ICurrencyService _currencyService;
		private readonly IPictureService _pictureService;
		private readonly ILocalizationService _localizationService;
		private readonly IPriceFormatter _priceFormatter;
		private readonly IWebHelper _webHelper;
		private readonly ISpecificationAttributeService _specificationAttributeService;
		private readonly IProductTagService _productTagService;
		private readonly IAclService _aclService;
		private readonly IStoreMappingService _storeMappingService;
		private readonly ITopicService _topicService;
		private readonly IEventPublisher _eventPublisher;
		private readonly ISearchTermService _searchTermService;
		private readonly HttpContextBase _httpContext;
		private readonly MediaSettings _mediaSettings;
		private readonly CatalogSettings _catalogSettings;
		private readonly VendorSettings _vendorSettings;
		private readonly BlogSettings _blogSettings;
		private readonly ForumSettings _forumSettings;
		private readonly ICacheManager _cacheManager;
		private readonly DisplayDefaultMenuItemSettings _displayDefaultMenuItemSettings;
		private readonly IStoreService _storeService;
		#endregion

		#region Constructors

		public ApiCatalogModelFactory(IProductModelFactory productModelFactory,
			 ICategoryService categoryService,
			 IManufacturerService manufacturerService,
			 IProductService productService,
			 IVendorService vendorService,
			 ICategoryTemplateService categoryTemplateService,
			 IManufacturerTemplateService manufacturerTemplateService,
			 IWorkContext workContext,
			 IStoreContext storeContext,
			 ICurrencyService currencyService,
			 IPictureService pictureService,
			 ILocalizationService localizationService,
			 IPriceFormatter priceFormatter,
			 IWebHelper webHelper,
			 ISpecificationAttributeService specificationAttributeService,
			 IProductTagService productTagService,
			 IAclService aclService,
			 IStoreMappingService storeMappingService,
			 ITopicService topicService,
			 IEventPublisher eventPublisher,
			 ISearchTermService searchTermService,
			 HttpContextBase httpContext,
			 MediaSettings mediaSettings,
			 CatalogSettings catalogSettings,
			 VendorSettings vendorSettings,
			 BlogSettings blogSettings,
			 ForumSettings forumSettings,
			 ICacheManager cacheManager,
			 DisplayDefaultMenuItemSettings displayDefaultMenuItemSettings,
			IStoreService storeService)
		{
			_productModelFactory = productModelFactory;
			_categoryService = categoryService;
			_manufacturerService = manufacturerService;
			_productService = productService;
			_vendorService = vendorService;
			_categoryTemplateService = categoryTemplateService;
			_manufacturerTemplateService = manufacturerTemplateService;
			_workContext = workContext;
			_storeContext = storeContext;
			_currencyService = currencyService;
			_pictureService = pictureService;
			_localizationService = localizationService;
			_priceFormatter = priceFormatter;
			_webHelper = webHelper;
			_specificationAttributeService = specificationAttributeService;
			_productTagService = productTagService;
			_aclService = aclService;
			_storeMappingService = storeMappingService;
			_topicService = topicService;
			_eventPublisher = eventPublisher;
			_searchTermService = searchTermService;
			_httpContext = httpContext;
			_mediaSettings = mediaSettings;
			_catalogSettings = catalogSettings;
			_vendorSettings = vendorSettings;
			_blogSettings = blogSettings;
			_forumSettings = forumSettings;
			_cacheManager = cacheManager;
			_displayDefaultMenuItemSettings = displayDefaultMenuItemSettings;
			_storeService = storeService;
		}

		#endregion

		#region Utilities

		/// <summary>
		/// Get child category identifiers
		/// </summary>
		/// <param name="parentCategoryId">Parent category identifier</param>
		/// <returns>List of child category identifiers</returns>
		protected virtual List<int> GetChildCategoryIds(int parentCategoryId)
		{

			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_CHILD_IDENTIFIERS_MODEL_KEY,
				 parentCategoryId,
				 string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
				 storeId);
			return _cacheManager.Get(cacheKey, () =>
			{
				var categoriesIds = new List<int>();
				var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId);
				foreach (var category in categories)
				{
					categoriesIds.Add(category.Id);
					categoriesIds.AddRange(GetChildCategoryIds(category.Id));
				}
				return categoriesIds;
			});
		}

		/// <summary>
		/// Prepare category (simple) models
		/// </summary>
		/// <returns>List of category (simple) models</returns>
		public virtual List<CategorySimpleModel> PrepareCategorySimpleModels()
		{
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			//load and cache them
			string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_ALL_MODEL_KEY,
				 _workContext.WorkingLanguage.Id,
				 string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
				 storeId);
			return _cacheManager.Get(cacheKey, () => PrepareCategorySimpleModels(0));
		}

		/// <summary>
		/// Prepare category (simple) models
		/// </summary>
		/// <param name="rootCategoryId">Root category identifier</param>
		/// <param name="loadSubCategories">A value indicating whether subcategories should be loaded</param>
		/// <param name="allCategories">All available categories; pass null to load them internally</param>
		/// <returns>List of category (simple) models</returns>
		public virtual List<CategorySimpleModel> PrepareCategorySimpleModels(int rootCategoryId,
			 bool loadSubCategories = true, IList<Category> allCategories = null)
		{
			var result = new List<CategorySimpleModel>();
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			//little hack for performance optimization.
			//we know that this method is used to load top and left menu for categories.
			//it'll load all categories anyway.
			//so there's no need to invoke "GetAllCategoriesByParentCategoryId" multiple times (extra SQL commands) to load childs
			//so we load all categories at once
			//if you don't like this implementation if you can uncomment the line below (old behavior) and comment several next lines (before foreach)
			//var categories = _categoryService.GetAllCategoriesByParentCategoryId(rootCategoryId);
			if (allCategories == null)
			{
				//load categories if null passed
				//we implemeneted it this way for performance optimization - recursive iterations (below)
				//this way all categories are loaded only once
				allCategories = _categoryService.GetAllCategories(storeId: storeId);
			}
			var categories = allCategories.Where(c => c.ParentCategoryId == rootCategoryId).ToList();
			foreach (var category in categories)
			{
				var categoryModel = new CategorySimpleModel
				{
					Id = category.Id,
					Name = category.GetLocalized(x => x.Name),
					SeName = category.GetSeName(),
					IncludeInTopMenu = category.IncludeInTopMenu
				};

				//number of products in each category
				if (_catalogSettings.ShowCategoryProductNumber)
				{
					string cacheKey = string.Format(ModelCacheEventConsumer.CATEGORY_NUMBER_OF_PRODUCTS_MODEL_KEY,
						 string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()),
						 storeId,
						 category.Id);
					categoryModel.NumberOfProducts = _cacheManager.Get(cacheKey, () =>
					{
						var categoryIds = new List<int> {category.Id};
						//include subcategories
						if (_catalogSettings.ShowCategoryProductNumberIncludingSubcategories)
							categoryIds.AddRange(GetChildCategoryIds(category.Id));
						return _productService.GetNumberOfProductsInCategory(categoryIds, storeId);
					});
				}

				if (loadSubCategories)
				{
					var subCategories = PrepareCategorySimpleModels(category.Id, loadSubCategories, allCategories);
					categoryModel.SubCategories.AddRange(subCategories);
				}
				result.Add(categoryModel);
			}

			return result;
		}

		#endregion

		#region Top Menu
		public virtual TopMenuModel PrepareTopMenuModel()
		{
			//categories
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			var cachedCategoriesModel = PrepareCategorySimpleModels();

			//top menu topics
			string topicCacheKey = string.Format(ModelCacheEventConsumer.TOPIC_TOP_MENU_MODEL_KEY,
				 _workContext.WorkingLanguage.Id,
				 storeId,
				 string.Join(",", _workContext.CurrentCustomer.GetCustomerRoleIds()));
			var cachedTopicModel = _cacheManager.Get(topicCacheKey, () =>
				 _topicService.GetAllTopics(_storeContext.CurrentStore.Id)
				 .Where(t => t.IncludeInTopMenu)
				 .Select(t => new TopMenuModel.TopMenuTopicModel
				 {
					 Id = t.Id,
					 Name = t.GetLocalized(x => x.Title),
					 SeName = t.GetSeName()
				 })
				 .ToList()
			);
			var model = new TopMenuModel
			{
				Categories = cachedCategoriesModel,
				Topics = cachedTopicModel,
				NewProductsEnabled = _catalogSettings.NewProductsEnabled,
				BlogEnabled = _blogSettings.Enabled,
				ForumEnabled = _forumSettings.ForumsEnabled,
				DisplayHomePageMenuItem = _displayDefaultMenuItemSettings.DisplayHomePageMenuItem,
				DisplayNewProductsMenuItem = _displayDefaultMenuItemSettings.DisplayNewProductsMenuItem,
				DisplayProductSearchMenuItem = _displayDefaultMenuItemSettings.DisplayProductSearchMenuItem,
				DisplayCustomerInfoMenuItem = _displayDefaultMenuItemSettings.DisplayCustomerInfoMenuItem,
				DisplayBlogMenuItem = _displayDefaultMenuItemSettings.DisplayBlogMenuItem,
				DisplayForumsMenuItem = _displayDefaultMenuItemSettings.DisplayForumsMenuItem,
				DisplayContactUsMenuItem = _displayDefaultMenuItemSettings.DisplayContactUsMenuItem
			};
			return model;
		}
		#endregion
	}
}
