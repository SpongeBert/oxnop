﻿using Nop.Core.Domain.Customers;
using Nop.Plugin.WebApi.Models.Customer;

namespace Nop.Plugin.WebApi.Factories
{
    /// <summary>
    /// Represents the interface of the customer model factory
    /// </summary>
    public partial interface ICustomerModelFactory
    {
        CustomerInfoModel PrepareCustomerInfoModel(CustomerInfoModel model, Customer customer, 
            bool excludeProperties, string overrideCustomCustomerAttributesXml = "");
    }
}
