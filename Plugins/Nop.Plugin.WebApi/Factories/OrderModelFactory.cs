﻿using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Catalog;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Tracking;
using Nop.Plugin.WebApi.Models.Common;
using Nop.Plugin.WebApi.Models.Order;
using Nop.Services.Stores;
using Nop.Services.Customers;
using Nop.Plugin.WebApi.Models.Addresses;
using Nop.Core.Domain.Localization;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Services;
using System.Net;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Newtonsoft.Json.Linq;
using System.Text;

namespace Nop.Plugin.WebApi.Factories
{
	/// <summary>
	/// Represents the order model factory
	/// </summary>
	public partial class OrderModelFactory : IOrderModelFactory
	{
		#region Fields

		private readonly IAddressModelFactory _addressModelFactory;
		private readonly IOrderService _orderService;
		private readonly IWorkContext _workContext;
		private readonly ICurrencyService _currencyService;
		private readonly IPriceFormatter _priceFormatter;
		private readonly IOrderProcessingService _orderProcessingService;
		private readonly IDateTimeHelper _dateTimeHelper;
		private readonly IPaymentService _paymentService;
		private readonly ILocalizationService _localizationService;
		private readonly IShippingService _shippingService;
		private readonly ICountryService _countryService;
		private readonly IProductAttributeParser _productAttributeParser;
		private readonly IDownloadService _downloadService;
		//private readonly IStoreContext _storeContext;
		private readonly IOrderTotalCalculationService _orderTotalCalculationService;
		private readonly IRewardPointService _rewardPointService;

		private readonly OrderSettings _orderSettings;
		private readonly TaxSettings _taxSettings;
		private readonly CatalogSettings _catalogSettings;
		private readonly ShippingSettings _shippingSettings;
		private readonly AddressSettings _addressSettings;
		private readonly RewardPointsSettings _rewardPointsSettings;
		private readonly PdfSettings _pdfSettings;
		private readonly ILanguageService _languageService;
		private readonly IStoreService _storeService;
		private readonly ICustomerService _customerService;
		private readonly IPictureService _pictureService;

		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;


		#endregion

		#region Constructors

		public OrderModelFactory(IAddressModelFactory addressModelFactory,
			 IOrderService orderService,
			 IWorkContext workContext,
			 ICurrencyService currencyService,
			 IPriceFormatter priceFormatter,
			 IOrderProcessingService orderProcessingService,
			 IDateTimeHelper dateTimeHelper,
			 IPaymentService paymentService,
			 ILocalizationService localizationService,
			 IShippingService shippingService,
			 ICountryService countryService,
			 IProductAttributeParser productAttributeParser,
			 IDownloadService downloadService,
			 //IStoreContext storeContext,
			 IOrderTotalCalculationService orderTotalCalculationService,
			 IRewardPointService rewardPointService,
			 CatalogSettings catalogSettings,
			 OrderSettings orderSettings,
			 TaxSettings taxSettings,
			 ShippingSettings shippingSettings,
			 AddressSettings addressSettings,
			 RewardPointsSettings rewardPointsSettings,
			 PdfSettings pdfSettings, ILanguageService languageService, IStoreService storeService, ICustomerService customerService, IPictureService pictureService,
			ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICmsNopProductMappingService cmsNopProductMappingService,
				ICmsNopOrderMappingService cmsNopOrderMappingService)
		{
			_addressModelFactory = addressModelFactory;
			_orderService = orderService;
			_workContext = workContext;
			_currencyService = currencyService;
			_priceFormatter = priceFormatter;
			_orderProcessingService = orderProcessingService;
			_dateTimeHelper = dateTimeHelper;
			_paymentService = paymentService;
			_localizationService = localizationService;
			_shippingService = shippingService;
			_countryService = countryService;
			_productAttributeParser = productAttributeParser;
			_downloadService = downloadService;
			//this._storeContext = storeContext;
			_orderTotalCalculationService = orderTotalCalculationService;
			_rewardPointService = rewardPointService;

			_catalogSettings = catalogSettings;
			_orderSettings = orderSettings;
			_taxSettings = taxSettings;
			_shippingSettings = shippingSettings;
			_addressSettings = addressSettings;
			_rewardPointsSettings = rewardPointsSettings;
			_pdfSettings = pdfSettings;
			_languageService = languageService;
			_storeService = storeService;
			_customerService = customerService;
			_pictureService = pictureService;



			_workContext.WorkingLanguage = _languageService.GetAllLanguages().FirstOrDefault();
			_workContext.WorkingCurrency = _currencyService.GetAllCurrencies().FirstOrDefault();

			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_cmsNopOrderMappingService = cmsNopOrderMappingService;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Prepare the customer order list model
		/// </summary>
		/// <returns>Customer order list model</returns>
		public virtual CustomerOrderListModel PrepareCustomerOrderListModel(int userid)
		{
			UpdateOrderStatus(userid);
			var model = new CustomerOrderListModel();
			int storeid = _storeService.GetAllStores().FirstOrDefault().Id;
			int languageId = _languageService.GetAllLanguages().FirstOrDefault().Id;

			Language workingLanguage = _languageService.GetLanguageById(languageId);

			var orders = _orderService.SearchOrders(storeid,
				 customerId: userid);
			foreach (var order in orders)
			{
				var orderModel = new CustomerOrderListModel.OrderDetailsModel
				{
					Id = order.Id,
					CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
					OrderStatusEnum = order.OrderStatus,
					OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
					PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
					ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
					IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order),
					CustomOrderNumber = order.CustomOrderNumber
				};
				var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
				orderModel.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, workingLanguage);

				model.Orders.Add(orderModel);
			}

			var recurringPayments = _orderService.SearchRecurringPayments(storeid, userid);
			var customer = _customerService.GetCustomerById(userid);
			foreach (var recurringPayment in recurringPayments)
			{
				var recurringPaymentModel = new CustomerOrderListModel.RecurringOrderModel
				{
					Id = recurringPayment.Id,
					StartDate = _dateTimeHelper.ConvertToUserTime(recurringPayment.StartDateUtc, DateTimeKind.Utc).ToString(),
					CycleInfo = string.Format("{0} {1}", recurringPayment.CycleLength, recurringPayment.CyclePeriod.GetLocalizedEnum(_localizationService, _workContext)),
					NextPayment = recurringPayment.NextPaymentDate.HasValue ? _dateTimeHelper.ConvertToUserTime(recurringPayment.NextPaymentDate.Value, DateTimeKind.Utc).ToString() : "",
					TotalCycles = recurringPayment.TotalCycles,
					CyclesRemaining = recurringPayment.CyclesRemaining,
					InitialOrderId = recurringPayment.InitialOrder.Id,
					InitialOrderNumber = recurringPayment.InitialOrder.CustomOrderNumber,
					CanCancel = _orderProcessingService.CanCancelRecurringPayment(customer, recurringPayment),
					CanRetryLastPayment = _orderProcessingService.CanRetryLastRecurringPayment(customer, recurringPayment)
				};

				model.RecurringOrders.Add(recurringPaymentModel);
			}

			return model;
		}

		/// <summary>
		/// Prepare the order details model
		/// </summary>
		/// <param name="order">Order</param>
		/// <returns>Order details model</returns>
		public virtual OrderDetailsModel PrepareOrderDetailsModel(Order order)
		{
			if (order == null)
				throw new ArgumentNullException("order");

			if (UpdateOrderStatusSingleOrder(order))
			{
				order.OrderStatus = OrderStatus.Complete;
				order = _orderService.GetOrderById(order.Id);
			}

			var model = new OrderDetailsModel();

			model.Id = order.Id;
			model.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);
			model.OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext);
			model.IsReOrderAllowed = _orderSettings.IsReOrderAllowed;
			model.IsReturnRequestAllowed = _orderProcessingService.IsReturnRequestAllowed(order);
			model.PdfInvoiceDisabled = _pdfSettings.DisablePdfInvoicesForPendingOrders && order.OrderStatus == OrderStatus.Pending;
			model.CustomOrderNumber = order.CustomOrderNumber;

			//shipping info
			model.ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext);
			if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
			{
				model.IsShippable = true;
				model.PickUpInStore = order.PickUpInStore;
				if (!order.PickUpInStore)
				{
					_addressModelFactory.PrepareAddressModel(model.ShippingAddress, order.ShippingAddress, false, _addressSettings);
				}
				else
					if (order.PickupAddress != null)
						model.PickupAddress = new AddressModel
						{
							Address1 = order.PickupAddress.Address1,
							City = order.PickupAddress.City,
							CountryName = order.PickupAddress.Country != null ? order.PickupAddress.Country.Name : string.Empty,
							ZipPostalCode = order.PickupAddress.ZipPostalCode
						};
				model.ShippingMethod = order.ShippingMethod;


				//shipments (only already shipped)
				var shipments = order.Shipments.Where(x => x.ShippedDateUtc.HasValue).OrderBy(x => x.CreatedOnUtc).ToList();
				foreach (var shipment in shipments)
				{
					var shipmentModel = new OrderDetailsModel.ShipmentBriefModel
					{
						Id = shipment.Id,
						TrackingNumber = shipment.TrackingNumber,
					};
					if (shipment.ShippedDateUtc.HasValue)
						shipmentModel.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
					if (shipment.DeliveryDateUtc.HasValue)
						shipmentModel.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);
					model.Shipments.Add(shipmentModel);
				}
			}


			//billing info
			_addressModelFactory.PrepareAddressModel(model.BillingAddress, order.BillingAddress, false, _addressSettings);

			//VAT number
			model.VatNumber = order.VatNumber;

			//payment method
			var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
			model.PaymentMethod = paymentMethod != null ? paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id) : order.PaymentMethodSystemName;
			model.PaymentMethodStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext);
			model.CanRePostProcessPayment = _paymentService.CanRePostProcessPayment(order);
			//custom values
			model.CustomValues = order.DeserializeCustomValues();

			//order subtotal
			if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax && !_taxSettings.ForceTaxExclusionFromOrderSubtotal)
			{
				//including tax

				//order subtotal
				var orderSubtotalInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalInclTax, order.CurrencyRate);
				model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
				//discount (applied to order subtotal)
				var orderSubTotalDiscountInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountInclTax, order.CurrencyRate);
				if (orderSubTotalDiscountInclTaxInCustomerCurrency > decimal.Zero)
					model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
			}
			else
			{
				//excluding tax

				//order subtotal
				var orderSubtotalExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubtotalExclTax, order.CurrencyRate);
				model.OrderSubtotal = _priceFormatter.FormatPrice(orderSubtotalExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
				//discount (applied to order subtotal)
				var orderSubTotalDiscountExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderSubTotalDiscountExclTax, order.CurrencyRate);
				if (orderSubTotalDiscountExclTaxInCustomerCurrency > decimal.Zero)
					model.OrderSubTotalDiscount = _priceFormatter.FormatPrice(-orderSubTotalDiscountExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
			}

			if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
			{
				//including tax

				//order shipping
				var orderShippingInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingInclTax, order.CurrencyRate);
				model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
				//payment method additional fee
				var paymentMethodAdditionalFeeInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeInclTax, order.CurrencyRate);
				if (paymentMethodAdditionalFeeInclTaxInCustomerCurrency > decimal.Zero)
					model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
			}
			else
			{
				//excluding tax

				//order shipping
				var orderShippingExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderShippingExclTax, order.CurrencyRate);
				model.OrderShipping = _priceFormatter.FormatShippingPrice(orderShippingExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
				//payment method additional fee
				var paymentMethodAdditionalFeeExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.PaymentMethodAdditionalFeeExclTax, order.CurrencyRate);
				if (paymentMethodAdditionalFeeExclTaxInCustomerCurrency > decimal.Zero)
					model.PaymentMethodAdditionalFee = _priceFormatter.FormatPaymentMethodAdditionalFee(paymentMethodAdditionalFeeExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
			}

			//tax
			bool displayTax = true;
			bool displayTaxRates = true;
			if (_taxSettings.HideTaxInOrderSummary && order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
			{
				displayTax = false;
				displayTaxRates = false;
			}
			else
			{
				if (order.OrderTax == 0 && _taxSettings.HideZeroTax)
				{
					displayTax = false;
					displayTaxRates = false;
				}
				else
				{
					displayTaxRates = _taxSettings.DisplayTaxRates && order.TaxRatesDictionary.Any();
					displayTax = !displayTaxRates;

					var orderTaxInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTax, order.CurrencyRate);
					//TODO pass languageId to _priceFormatter.FormatPrice
					model.Tax = _priceFormatter.FormatPrice(orderTaxInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

					foreach (var tr in order.TaxRatesDictionary)
					{
						model.TaxRates.Add(new OrderDetailsModel.TaxRate
						{
							Rate = _priceFormatter.FormatTaxRate(tr.Key),
							//TODO pass languageId to _priceFormatter.FormatPrice
							Value = _priceFormatter.FormatPrice(_currencyService.ConvertCurrency(tr.Value, order.CurrencyRate), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
						});
					}
				}
			}
			model.DisplayTaxRates = displayTaxRates;
			model.DisplayTax = displayTax;
			model.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoOrderDetailsPage;
			model.PricesIncludeTax = order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax;

			//discount (applied to order total)
			var orderDiscountInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderDiscount, order.CurrencyRate);
			if (orderDiscountInCustomerCurrency > decimal.Zero)
				model.OrderTotalDiscount = _priceFormatter.FormatPrice(-orderDiscountInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);


			//gift cards
			foreach (var gcuh in order.GiftCardUsageHistory)
			{
				model.GiftCards.Add(new OrderDetailsModel.GiftCard
				{
					CouponCode = gcuh.GiftCard.GiftCardCouponCode,
					Amount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(gcuh.UsedValue, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage),
				});
			}

			//reward points           
			if (order.RedeemedRewardPointsEntry != null)
			{
				model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
				model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-(_currencyService.ConvertCurrency(order.RedeemedRewardPointsEntry.UsedAmount, order.CurrencyRate)), true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);
			}

			//total
			var orderTotalInCustomerCurrency = _currencyService.ConvertCurrency(order.OrderTotal, order.CurrencyRate);
			model.OrderTotal = _priceFormatter.FormatPrice(orderTotalInCustomerCurrency, true, order.CustomerCurrencyCode, false, _workContext.WorkingLanguage);

			//checkout attributes
			model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;

			//order notes
			foreach (var orderNote in order.OrderNotes
				 .Where(on => on.DisplayToCustomer)
				 .OrderByDescending(on => on.CreatedOnUtc)
				 .ToList())
			{
				model.OrderNotes.Add(new OrderDetailsModel.OrderNote
				{
					Id = orderNote.Id,
					HasDownload = orderNote.DownloadId > 0,
					Note = orderNote.FormatOrderNoteText(),
					CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
				});
			}


			//purchased products
			model.ShowSku = _catalogSettings.ShowSkuOnProductDetailsPage;
			var orderItems = order.OrderItems;
			foreach (var orderItem in orderItems)
			{
				var orderItemModel = new OrderDetailsModel.OrderItemModel
				{
					Id = orderItem.Id,
					OrderItemGuid = orderItem.OrderItemGuid,
					Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
					ProductId = orderItem.Product.Id,
					ProductName = orderItem.Product.GetLocalized(x => x.Name),
					ProductSeName = orderItem.Product.GetSeName(),
					Quantity = orderItem.Quantity,
					AttributeInfo = orderItem.AttributeDescription,
				};

				//picture
				var orderItemPicture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
				orderItemModel.PictureThumbnailUrl = _pictureService.GetPictureUrl(orderItemPicture, 120, true);

				//rental info
				if (orderItem.Product.IsRental)
				{
					var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
					var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
					orderItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
						 rentalStartDate, rentalEndDate);
				}
				model.Items.Add(orderItemModel);

				//unit price, subtotal
				if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
				{
					//including tax
					var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
					orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);

					var priceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceInclTax, order.CurrencyRate);
					orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
				}
				else
				{
					//excluding tax
					var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
					orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);

					var priceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.PriceExclTax, order.CurrencyRate);
					orderItemModel.SubTotal = _priceFormatter.FormatPrice(priceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
				}

				//downloadable products
				if (_downloadService.IsDownloadAllowed(orderItem))
					orderItemModel.DownloadId = orderItem.Product.DownloadId;
				if (_downloadService.IsLicenseDownloadAllowed(orderItem))
					orderItemModel.LicenseId = orderItem.LicenseDownloadId.HasValue ? orderItem.LicenseDownloadId.Value : 0;
			}

			return model;
		}

		/// <summary>
		/// Prepare the shipment details model
		/// </summary>
		/// <param name="shipment">Shipment</param>
		/// <returns>Shipment details model</returns>
		public virtual ShipmentDetailsModel PrepareShipmentDetailsModel(Shipment shipment)
		{
			if (shipment == null)
				throw new ArgumentNullException("shipment");

			var order = shipment.Order;
			if (order == null)
				throw new Exception("order cannot be loaded");
			var model = new ShipmentDetailsModel();

			model.Id = shipment.Id;
			if (shipment.ShippedDateUtc.HasValue)
				model.ShippedDate = _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc);
			if (shipment.DeliveryDateUtc.HasValue)
				model.DeliveryDate = _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc);

			//tracking number and shipment information
			if (!String.IsNullOrEmpty(shipment.TrackingNumber))
			{
				model.TrackingNumber = shipment.TrackingNumber;
				var shipmentTracker = shipment.GetShipmentTracker(_shippingService, _shippingSettings);
				if (shipmentTracker != null)
				{
					model.TrackingNumberUrl = shipmentTracker.GetUrl(shipment.TrackingNumber);
					if (_shippingSettings.DisplayShipmentEventsToCustomers)
					{
						var shipmentEvents = shipmentTracker.GetShipmentEvents(shipment.TrackingNumber);
						if (shipmentEvents != null)
							foreach (var shipmentEvent in shipmentEvents)
							{
								var shipmentStatusEventModel = new ShipmentDetailsModel.ShipmentStatusEventModel();
								var shipmentEventCountry = _countryService.GetCountryByTwoLetterIsoCode(shipmentEvent.CountryCode);
								shipmentStatusEventModel.Country = shipmentEventCountry != null
																					 ? shipmentEventCountry.GetLocalized(x => x.Name)
																					 : shipmentEvent.CountryCode;
								shipmentStatusEventModel.Date = shipmentEvent.Date;
								shipmentStatusEventModel.EventName = shipmentEvent.EventName;
								shipmentStatusEventModel.Location = shipmentEvent.Location;
								model.ShipmentStatusEvents.Add(shipmentStatusEventModel);
							}
					}
				}
			}

			//products in this shipment
			model.ShowSku = _catalogSettings.ShowSkuOnProductDetailsPage;
			foreach (var shipmentItem in shipment.ShipmentItems)
			{
				var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
				if (orderItem == null)
					continue;

				var shipmentItemModel = new ShipmentDetailsModel.ShipmentItemModel
				{
					Id = shipmentItem.Id,
					Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
					ProductId = orderItem.Product.Id,
					ProductName = orderItem.Product.GetLocalized(x => x.Name),
					ProductSeName = orderItem.Product.GetSeName(),
					AttributeInfo = orderItem.AttributeDescription,
					QuantityOrdered = orderItem.Quantity,
					QuantityShipped = shipmentItem.Quantity,
				};
				//rental info
				if (orderItem.Product.IsRental)
				{
					var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
					var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
					shipmentItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
						 rentalStartDate, rentalEndDate);
				}
				model.Items.Add(shipmentItemModel);
			}

			//order details model
			model.Order = PrepareOrderDetailsModel(order);

			return model;
		}

		/// <summary>
		/// Prepare the customer reward points model
		/// </summary>
		/// <param name="page">Number of items page; pass null to load the first page</param>
		/// <returns>Customer reward points model</returns>
		public virtual CustomerRewardPointsModel PrepareCustomerRewardPoints(int userid)
		{

	 		var customer = _customerService.GetCustomerById(userid);
			int storeid = _storeService.GetAllStores().FirstOrDefault().Id;
			var pageSize = _rewardPointsSettings.PageSize;
			var model = new CustomerRewardPointsModel();
			//var list = _rewardPointService.GetRewardPointsHistory(customer.Id, showNotActivated: true, pageIndex: --page ?? 0, pageSize: pageSize);
		 	var list = _rewardPointService.GetRewardPointsHistory(customer.Id, showNotActivated: true);

			
			model.RewardPoints = list.Select(rph =>
			{
				var activatingDate = _dateTimeHelper.ConvertToUserTime(rph.CreatedOnUtc, DateTimeKind.Utc);
				return new CustomerRewardPointsModel.RewardPointsHistoryModel
				{
					Points = rph.Points,
					PointsBalance = rph.PointsBalance.HasValue ? rph.PointsBalance.ToString()
						 : string.Format(_localizationService.GetResource("RewardPoints.ActivatedLater"), activatingDate),
					Message = rph.Message,
					CreatedOn = activatingDate
				};
			}).ToList();

			model.PagerModel = new PagerModel
			{
				PageSize = list.PageSize,
				TotalRecords = list.TotalCount,
				PageIndex = list.PageIndex,
				ShowTotalSummary = true,
				RouteActionName = "CustomerRewardPointsPaged",
				UseRouteLinks = true,
				RouteValues = new RewardPointsRouteValues { page = 0 }
			};

			//current amount/balance
			Nop.Core.Domain.Directory.Currency  WorkingCurrency = _currencyService.GetAllCurrencies().FirstOrDefault();

			int rewardPointsBalance = _rewardPointService.GetRewardPointsBalance(customer.Id, storeid);
			decimal rewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(rewardPointsBalance);
			decimal rewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(rewardPointsAmountBase, WorkingCurrency);
			model.RewardPointsBalance = rewardPointsBalance;
			model.RewardPointsAmount = _priceFormatter.FormatPrice(rewardPointsAmount, true, false);
			//minimum amount/balance
			int minimumRewardPointsBalance = _rewardPointsSettings.MinimumRewardPointsToUse;
			decimal minimumRewardPointsAmountBase = _orderTotalCalculationService.ConvertRewardPointsToAmount(minimumRewardPointsBalance);
			decimal minimumRewardPointsAmount = _currencyService.ConvertFromPrimaryStoreCurrency(minimumRewardPointsAmountBase, WorkingCurrency);
			model.MinimumRewardPointsBalance = minimumRewardPointsBalance;
			model.MinimumRewardPointsAmount = _priceFormatter.FormatPrice(minimumRewardPointsAmount, true, false);
			return model;

		

		}

		#endregion


		#region Custom Methods

		public bool UpdateOrderStatusSingleOrder(Order order)
		{
			try
			{
				if (order.OrderStatus == OrderStatus.Processing || order.OrderStatus == OrderStatus.Pending)
				{
					var cmsNopOrder = _cmsNopOrderMappingService.GetByNopOrderId(order.Id);
					if (cmsNopOrder == null)
						return false;

					var cmsNopCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
					if (cmsNopCustomer == null)
						return false;

					var url = "https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + cmsNopCustomer.CmsCustomerId + "/order/" + cmsNopOrder.CmsOrderId;
					var token = GetToken();
					var request = (HttpWebRequest)WebRequest.Create(url);
					request.Headers.Add("X-Auth-Token", token);
					request.Headers.Add("customerId", cmsNopCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
					request.Headers.Add("orderId", cmsNopOrder.CmsOrderId.ToString(CultureInfo.InvariantCulture));
					request.ContentType = "application/json";
					request.Method = "GET";
					request.AutomaticDecompression = DecompressionMethods.GZip;

					var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
					var responseData = responseReader.ReadToEnd();
					if (!string.IsNullOrEmpty(responseData))
					{
						var jsonText = JsonConvert.DeserializeObject<CmsOrderApiModel>(responseData);
						if (jsonText.status.ToLower() == "done")
						{
							order.OrderStatus = OrderStatus.Complete;
							_orderService.UpdateOrder(order);

							UpdateCustomerLoyaltyPointsOnLightSpeed(order, cmsNopCustomer);
							return true;
						}
					}
				}
				return false;

			}
			catch (Exception exception)
			{
				return false;
			}
		}


		public void UpdateOrderStatus(int customerId)
		{
			try
			{

				int storeId = _storeService.GetAllStores().FirstOrDefault().Id;
				var orders = _orderService.SearchOrders(storeId: storeId,
				 customerId: customerId);
				foreach (var order in orders)
				{
					if (order.OrderStatus == OrderStatus.Processing || order.OrderStatus == OrderStatus.Pending)
					{
						var cmsNopOrder = _cmsNopOrderMappingService.GetByNopOrderId(order.Id);
						if (cmsNopOrder == null)
							continue;

						var cmsNopCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
						if (cmsNopCustomer == null)
							continue;

						var url = "https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + cmsNopCustomer.CmsCustomerId + "/order/" + cmsNopOrder.CmsOrderId;
						var token = GetToken();
						var request = (HttpWebRequest)WebRequest.Create(url);
						request.Headers.Add("X-Auth-Token", token);
						request.Headers.Add("customerId", cmsNopCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
						request.Headers.Add("orderId", cmsNopOrder.CmsOrderId.ToString(CultureInfo.InvariantCulture));
						request.ContentType = "application/json";
						request.Method = "GET";
						request.AutomaticDecompression = DecompressionMethods.GZip;

						var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
						var responseData = responseReader.ReadToEnd();
						if (!string.IsNullOrEmpty(responseData))
						{
							var jsonText = JsonConvert.DeserializeObject<CmsOrderApiModel>(responseData);
							if (jsonText.status.ToLower() == "done")
							{
								order.OrderStatus = OrderStatus.Complete;
								_orderService.UpdateOrder(order);
								UpdateCustomerLoyaltyPointsOnLightSpeed(order, cmsNopCustomer);
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				return;
			}
		}


		public string GetToken()
		{

			string token = string.Empty;
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			//string postData = "{\"companyId\": 14500,\"deviceId\": \"test\",\"password\": \"bK6VrHCbjctGZdZ5q7\",\"username\": \"staging@webshopcompany.be\"}";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"resto1234\",\"username\": \"lightspeed@cfe.com\"}";
			//string postData = "{\"companyId\": 32479,\"deviceId\": \"prod\",\"password\": \"z7y65tR\",\"username\": \"euc1@webshopcompany.be\"}";

			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			token = json.GetValue("token").Value<string>();
			return token;
		}


		public void UpdateCustomerLoyaltyPointsOnLightSpeed(Order order, CmsCustomerNopCustomerMapping checkCmsCustomer)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				int storeId = _storeService.GetAllStores().FirstOrDefault().Id;

				var postData = new StringBuilder();
				//postData.Append("{\"deliveryCity\":\"" + order.ShippingAddress.City + "\",");
				//postData.Append("\"deliveryCountry\":\"" + order.ShippingAddress.Country.TwoLetterIsoCode + "\",");
				//postData.Append("\"deliveryStreet\":\"" + order.ShippingAddress.Address1 + "\",");
				//postData.Append("\"deliveryStreetNumber\":\"" + order.ShippingAddress.Address2 + "\",");
				//postData.Append("\"deliveryZip\":\"" + order.ShippingAddress.ZipPostalCode + "\",");
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + order.Customer.Email + "\",");

				var loyaltyPoints = "0 reward points available.";
				var customerRewardPointsBalance = _rewardPointService.GetRewardPointsBalance(order.Customer.Id, storeId);
				if (customerRewardPointsBalance > 0)
					loyaltyPoints = customerRewardPointsBalance + " reward points available.";

				postData.Append("\"firstName\":\"" + loyaltyPoints + "\",");
				postData.Append("\"lastName\":\"" + order.Customer.GetFullName() + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);

				}



			}
			catch (Exception exception)
			{
				return;
			}
		}

		#endregion

	}
}
