﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Messages;
using Nop.Core;
using Nop.Services.Localization;
using Nop.Plugin.WebApi.Models.Customer;
using Nop.Services.Stores;
using System.Linq;

    
namespace Nop.Plugin.WebApi.Factories
{
    /// <summary>
    /// Represents the customer model factory
    /// </summary>
    public partial class CustomerModelFactory : ICustomerModelFactory
    {
        #region Fields
        private readonly CustomerSettings _customerSettings;
        private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
		  private readonly IStoreService _storeService;
        #endregion

        #region Ctor

        public CustomerModelFactory(
            ILocalizationService localizationService,
            IWorkContext workContext,
            IStoreContext storeContext,
            INewsLetterSubscriptionService newsLetterSubscriptionService,
				CustomerSettings customerSettings, IStoreService storeService
         )
        {
            _localizationService = localizationService;
            _workContext = workContext;
            _storeContext = storeContext;
            _newsLetterSubscriptionService = newsLetterSubscriptionService;
            _customerSettings = customerSettings;
				_storeService = storeService;
        }

        #endregion


        #region CustomerInfo Factory Method

        public virtual CustomerInfoModel PrepareCustomerInfoModel(CustomerInfoModel model, Customer customer, 
        bool excludeProperties, string overrideCustomCustomerAttributesXml = "")
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (customer == null)
                throw new ArgumentNullException("customer");
				var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
      
            if (!excludeProperties)
            {

                
                model.FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
                model.LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
                model.Gender = customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender);
                var dateOfBirth = customer.GetAttribute<DateTime?>(SystemCustomerAttributeNames.DateOfBirth);

					 var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, storeId);
          

                model.Email = customer.Email;
                model.Username = customer.Username;
            }
            else
            {
                if (_customerSettings.UsernamesEnabled && !_customerSettings.AllowUsersToChangeUsernames)
                    model.Username = customer.Username;
            }

            return model;
        }

        #endregion 

    }
}
