﻿using System;
using System.Collections.Generic;  
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.News;
using Nop.Plugin.WebApi.Factories;
using Nop.Plugin.WebApi.Models.News;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.News;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;


namespace Nop.Plugin.WebApi.Controllers
{
    [NopHttpsRequirement(SslRequirement.No)]
    public partial class ApiNewsController : BasePluginController
    {
        #region Fields

        private readonly INewsModelFactory _newsModelFactory;
        private readonly INewsService _newsService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IWebHelper _webHelper;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IPermissionService _permissionService;
        private readonly IEventPublisher _eventPublisher;

        private readonly NewsSettings _newsSettings;
        private readonly LocalizationSettings _localizationSettings;
        private readonly CaptchaSettings _captchaSettings;

        #endregion

        #region Ctor

		  public ApiNewsController(INewsModelFactory newsModelFactory,
            INewsService newsService,
            IWorkContext workContext, 
            IStoreContext storeContext,
            ILocalizationService localizationService,
            IWorkflowMessageService workflowMessageService,
            IWebHelper webHelper,
            ICustomerActivityService customerActivityService,
            IStoreMappingService storeMappingService,
            IPermissionService permissionService,
            IEventPublisher eventPublisher,
            NewsSettings newsSettings,
            LocalizationSettings localizationSettings, 
            CaptchaSettings captchaSettings)
        {
            this._newsModelFactory = newsModelFactory;
            this._newsService = newsService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._localizationService = localizationService;
            this._workflowMessageService = workflowMessageService;
            this._webHelper = webHelper;
            this._customerActivityService = customerActivityService;
            this._storeMappingService = storeMappingService;
            this._permissionService = permissionService;
            this._eventPublisher = eventPublisher;

            this._newsSettings = newsSettings;
            this._localizationSettings = localizationSettings;
            this._captchaSettings = captchaSettings;
        }

        #endregion
        
        #region Methods

        public virtual ActionResult HomePageNews()
        {
            if (!_newsSettings.Enabled || !_newsSettings.ShowNewsOnMainPage)
					 //return Content("");
				return Json(new { success = true, message = ""}, JsonRequestBehavior.AllowGet);

            var model = _newsModelFactory.PrepareHomePageNewsItemsModel();
				//return PartialView(model);
				if (model == null)
				{
					return Json(new { success = false, message = "No News" }, JsonRequestBehavior.AllowGet);
				}
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult NewsList(NewsPagingFilteringModel command)
        {
            if (!_newsSettings.Enabled)
              //  return RedirectToRoute("HomePage");
					return Json(new { success = false, message = "News setting is not enabled" }, JsonRequestBehavior.AllowGet);

            var model = _newsModelFactory.PrepareNewsItemListModel(command);
				if (model == null)
				{
					return Json(new { success = false, message = "No News" }, JsonRequestBehavior.AllowGet);
				}
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
           // return View(model);
        }

    
        public virtual ActionResult NewsItemDetails(int newsItemId)
        {
            if (!_newsSettings.Enabled)
					return Json(new { success = false, message = "News setting is not enabled" }, JsonRequestBehavior.AllowGet);

            var newsItem = _newsService.GetNewsById(newsItemId);
            if (newsItem == null ||
                !newsItem.Published ||
                (newsItem.StartDateUtc.HasValue && newsItem.StartDateUtc.Value >= DateTime.UtcNow) ||
                (newsItem.EndDateUtc.HasValue && newsItem.EndDateUtc.Value <= DateTime.UtcNow) ||
                //Store mapping
                !_storeMappingService.Authorize(newsItem))
					 //return RedirectToRoute("HomePage");
					return Json(new { success = false, message = "No News" }, JsonRequestBehavior.AllowGet);

            var model = new NewsItemModel();
            model = _newsModelFactory.PrepareNewsItemModel(model, newsItem, true);
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
           
           // return View(model);
        }

      

    

        #endregion
    }
}
