﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.WebApi.Factories;
using Nop.Plugin.WebApi.Models.Category;
using Nop.Services.Catalog;
using Nop.Services.Media;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Services.Localization;
using System;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Services;
using Nop.Core.Data;
using Nop.Services.Customers;

namespace Nop.Plugin.WebApi.Controllers
{
	public class ApiCategoryController : BasePluginController
	{

		#region Fields

		private readonly ICategoryService _categoryService;
		private readonly IPictureService _pictureService;

		private readonly IStoreService _storeService;
		private readonly IProductModelFactory _productModelFactory;
		private readonly IProductService _productService;			
		private readonly IRepository<CmsProductNopProductMapping> _cmsProductNopProductMappingRepository;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly ICustomerService _customerService;
		private readonly ILanguageService _languageService;
		#endregion

		#region Cont

		public ApiCategoryController(ICategoryService categoryService, IPictureService pictureService, IStoreService storeService, IProductModelFactory productModelFactory,
		IProductService productService, IRepository<CmsProductNopProductMapping> cmsProductNopProductMappingRepository, ICmsNopProductMappingService cmsNopProductMappingService,
			ICustomerService customerService, ILanguageService languageService)
		{
			_categoryService = categoryService;
			_pictureService = pictureService;
			_storeService = storeService;
			_productModelFactory = productModelFactory;
			_productService = productService;
		
			_cmsProductNopProductMappingRepository = cmsProductNopProductMappingRepository;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_customerService = customerService;
			_languageService = languageService;
		}

		#endregion

		#region Utility
		public string GetPublicName(string categoryName)
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == categoryName.ToLower());

			if (category == null)
			{
				return categoryName;
			};

			int languageId = _languageService.GetAllLanguages().FirstOrDefault().Id;

			var publicDisplayName = category.GetLocalized(x => x.PublicDisplayName, languageId);

			if (string.IsNullOrEmpty(publicDisplayName))
			{
				return categoryName;
			}

			return publicDisplayName;
		}
		#endregion

		#region < GET ALL CATEGORIES >

		public ActionResult AllCategories()
		{
			//var allCategories = _categoryService.GetAllCategories();
			var allCategories = _categoryService.GetAllCategories(storeId: _storeService.GetAllStores().FirstOrDefault().Id);
			IList<CustomCategory> categoryList = new List<CustomCategory>();
			for (int i = 0; i < allCategories.Count; i++)
			{
				var item = allCategories[i];
				//string name = item.PublicDisplayName ? "" : item.Name;
				categoryList.Add(new CustomCategory()
				{
					Id = item.Id,
					Name = GetPublicName(item.Name),
					ImageUrl = _pictureService.GetPictureUrl(item.PictureId)
				});
			}

			return Json(categoryList, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region < GET PRODUCT BY CATEGORY ID >

		public ActionResult GetProductsByCategoryId(int id)
		{
		  	var productCategory = _categoryService.GetProductCategoriesByCategoryId(id);
			if (productCategory == null)
			{
				return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);
			}

			IList<CategoryProductList> categoryByProductIdList = new List<CategoryProductList>();

			for (int i = 0; i < productCategory.Count; i++)
			{
				var item = productCategory[i];
				string url = _pictureService.GetPictureUrl(_pictureService.GetPicturesByProductId(item.Product.Id).FirstOrDefault().Id);
				string imgUrl = url ?? "";
				categoryByProductIdList.Add(new CategoryProductList
				{
					Id = item.Product.Id,
					Name = item.Product.Name,
					Price = item.Product.Price,
					ImageUrl = _pictureService.GetPicturesByProductId(item.Product.Id).FirstOrDefault() == null ? "" : imgUrl,
					FreeShipping = item.Product.IsFreeShipping,
					StockAvailability = item.Product.StockQuantity
				});
			}
			return Json(categoryByProductIdList, JsonRequestBehavior.AllowGet);
		}


		public ActionResult GetCatProducts(string Category, string CuserId)
		{

			int userId = 0;
			if (CuserId != "null")
				userId = Convert.ToInt32(CuserId);
									  
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == Category.ToLower());
			int categoryId = 0;
			if (category == null)
			{
				return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);
			}


			if (category != null)
				categoryId = category.Id;

		//	var categoryList = new List<int> { categoryId };
		//	var dayProducts = _productService.SearchProducts(categoryIds: categoryList);

			var dayProducts = _categoryService.GetProductCategoriesByCategoryId(categoryId);

			List<Product> prodList = new List<Product>();
			foreach (var prod in dayProducts)
			{
				prodList.Add(prod.Product);
			}
		

		//	var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder),userId:userId);

			var model = _productModelFactory.PrepareProductOverviewModels(prodList.ToList(), userId: userId);
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetCatProductsByCatId(int categoryId, string CuserId)
		{
			int userId = 0; 		
			if (CuserId != "null")
				userId = Convert.ToInt32(CuserId);

			var category = _categoryService.GetCategoryById(categoryId);  			
			if (category == null)
			{
				return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);
			}

			var currentCustomer = _customerService.GetCustomerById(userId);

			var companyId = CheckCompanyMappingAvailability(currentCustomer.Username != null ? currentCustomer.Username.Substring(0, 3) : "001");

				 
			//var categoryList = new List<int> { category.Id };
			//var dayProducts = _productService.SearchProducts(categoryIds: new List<int> { category.Id });
			List<Product> prodList = new List<Product>();
			var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { category.Id });
			foreach (var product in categoryProducts)
			{
				var productCompanyMapping = (from cmsProduct in _cmsProductNopProductMappingRepository.Table
													  where cmsProduct.Company == companyId && cmsProduct.NopProductId == product.Id
													  select cmsProduct).FirstOrDefault();

				if (productCompanyMapping != null)
				{ 				
					prodList.Add(product);
					continue;
				}
			}

			//var dayProducts = _categoryService.GetProductCategoriesByCategoryId(category.Id);
			
			//List<Product> prodList = new List<Product>();
		//	foreach (var prod in dayProducts)
			//{
			//	prodList.Add(prod.Product);
			//} 		

			//var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder), userId: userId);

			var model = _productModelFactory.PrepareProductOverviewModels(prodList.ToList(), userId: userId);
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}


		public ActionResult GetSubCategories(int categoryId)
		{
			string displayCat = string.Empty;
			bool existingProduct = false;
			var categoryList = new List<int> { categoryId };
			IList<Category> SubCategories = _categoryService.GetAllCategoriesByParentCategoryId(categoryId);
			if (SubCategories == null)
			{	 			
				var products = _productService.SearchProducts(categoryIds: categoryList);
				if (products.Any())
				{
					existingProduct = false;
					return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);
				}
				
				else
				{
					existingProduct = true;
					return Json(new { success = true, message = "", data = products, displayProduct = existingProduct}, JsonRequestBehavior.AllowGet);
				}

				
			}
			else
			{					
				IList<Category> subCategoriesExisting = new List<Category>();
				foreach (var subcategory in SubCategories)
				{	
					var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { subcategory.Id });
					if (!categoryProducts.Any())
						continue;	 

					subCategoriesExisting.Add(subcategory);
				}
				if (!subCategoriesExisting.Any())
					existingProduct = false;
				else
					existingProduct = true;
				return Json(new { success = true, message = "", data = subCategoriesExisting, displayProduct = existingProduct }, JsonRequestBehavior.AllowGet);
			} 
			
		}


		public ActionResult GetSubCategoriesbyName(string categoryName, int userId)
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == categoryName.ToLower()); 			
			if (category == null)
			{
				return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);
			}

			var currentCustomer = _customerService.GetCustomerById(userId);

			var companyId = CheckCompanyMappingAvailability(currentCustomer.Username != null ? currentCustomer.Username.Substring(0, 3) : "001");
			
			IList<Category> SubCategories = _categoryService.GetAllCategoriesByParentCategoryId(category.Id);
			if (SubCategories == null)
			{			
			return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);	 			
			}
			else
			{
				IList<CustomSubCategories> subCategoriesExisting = new List<CustomSubCategories>();
				foreach (var subcategory in SubCategories)
				{	
				
					//var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { subcategory.Id });

					//if (!categoryProducts.Any())
					//	continue;
				//	var customSubCategories = new CustomSubCategories
				//	{
				//		SubCategoryId = subcategory.Id,
				//		SubCategoryName = GetPublicName(subcategory.Name)
				//	};
				//	subCategoriesExisting.Add(customSubCategories); 	

					var flag = 0;
					var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { subcategory.Id });
					foreach (var product in categoryProducts)
					{
						var productCompanyMapping = (from cmsProduct in _cmsProductNopProductMappingRepository.Table
															  where cmsProduct.Company == companyId && cmsProduct.NopProductId == product.Id
															  select cmsProduct).FirstOrDefault();

						if (productCompanyMapping != null)
						{
							flag = 1;
							break;
						}
					}

					if (flag == 1)
					{ 					
					var customSubCategories = new CustomSubCategories
					{
					SubCategoryId = subcategory.Id,
					SubCategoryName = GetPublicName(subcategory.Name)
					};
					subCategoriesExisting.Add(customSubCategories); 
					}

				}
				
				return Json(new { success = true, message = "", data = subCategoriesExisting }, JsonRequestBehavior.AllowGet);
			} 				
		}
		#endregion

		#region < FEATURED CATEGORIES >

		public ActionResult FeaturedCategories()
		{
			var homePageCategories = _categoryService.GetAllCategoriesDisplayedOnHomePage();


			if (homePageCategories == null)
			{
				return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);
			}

			IList<CustomCategory> homePageCategory = new List<CustomCategory>();

			for (int i = 0; i < homePageCategories.Count; i++)
			{
				var item = homePageCategories[i];
				var cat = new CustomCategory
				{
					Id = item.Id,
					Name = item.Name,
					ImageUrl = _pictureService.GetPictureUrl(item.PictureId)
				};

				var homePagesSubCategories = _categoryService.GetAllCategoriesByParentCategoryId(item.Id);
				if (homePagesSubCategories != null)
				{
					ICollection<CustomSubCategories> subCatList = new List<CustomSubCategories>();
					foreach (var subcat in homePagesSubCategories)
					{
						var subcategory = new CustomSubCategories
						{
							SubCategoryId = subcat.Id,
							SubCategoryName = subcat.Name,
							SubCategoryImageUrl = _pictureService.GetPictureUrl(subcat.PictureId)
						};
						subCatList.Add(subcategory);
					}
					cat.SubCategories = subCatList;
				}

				homePageCategory.Add(cat);
			}

			return Json(homePageCategory, JsonRequestBehavior.AllowGet);
		}

		#endregion

		public string CheckCompanyMappingAvailability(string actualCompanyId)
		{
			var checkMappings = _cmsNopProductMappingService.GetByCompanyId(actualCompanyId);
			if (checkMappings.Any())
			{
				return actualCompanyId;
			}
			else
			{
				return "001";
			}

		}

	}
}
