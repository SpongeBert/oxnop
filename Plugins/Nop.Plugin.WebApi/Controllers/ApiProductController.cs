﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;
using Nop.Plugin.WebApi.Factories;
using Nop.Plugin.WebApi.Models.Product;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Services.Orders;
using System.Web.Script.Serialization;
using Nop.Services.Messages;
using System;
using Nop.Core.Domain.Localization;
using Nop.Services.Events;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.WebApi.Controllers
{
	public class ApiProductController : BasePluginController
	{

		#region Fields

		private readonly IProductService _productApiService;
		private readonly IProductService _productService;
		protected readonly IAclService _aclService;
		protected readonly IStoreMappingService _storeMappingService;
		private readonly IProductModelFactory _productModelFactory;
		private readonly ICustomerActivityService _customerActivityService;
		private readonly ICustomerService _customerService;
		private readonly ILocalizationService _localizationService;
		private readonly ShoppingCartSettings _shoppingCartSettings;
		private readonly IStoreService _storeService;
		private readonly CatalogSettings _catalogSettings;
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly LocalizationSettings _localizationSettings;
		private readonly IOrderService _orderService;
		private readonly IEventPublisher _eventPublisher;
		#endregion

		#region Const

		public ApiProductController(IProductService productApiService, IProductService productService, IAclService aclService,
			IStoreMappingService storeMappingService, IStoreService storeService, ICustomerActivityService customerActivityService,
			ICustomerService customerService, ILocalizationService localizationService, ShoppingCartSettings shoppingCartSettings,
			IProductModelFactory productModelFactory, CatalogSettings catalogSettings, IWorkflowMessageService workflowMessageService,
			LocalizationSettings localizationSettings, IOrderService orderService, IEventPublisher eventPublisher)
		{
			_productApiService = productApiService;
			_productService = productService;
			_aclService = aclService;
			_storeMappingService = storeMappingService;
			_storeService = storeService;
			_customerActivityService = customerActivityService;
			_customerService = customerService;
			_localizationService = localizationService;
			_shoppingCartSettings = shoppingCartSettings;
			_productModelFactory = productModelFactory;
			_catalogSettings = catalogSettings;
			_workflowMessageService = workflowMessageService;
			_localizationSettings = localizationSettings;
			_orderService = orderService;
			_eventPublisher = eventPublisher;
		}

		#endregion

		#region  GET PRODUCT BY ID

		public ActionResult ProductDetails(int productId, int userid, int updatecartitemid = 0)
		{
			var product = _productService.GetProductById(productId);
			if (product == null || product.Deleted)
				return Json(new { success = false, message = "Produt Not Found" });

			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			var customer = _customerService.GetCustomerById(userid);

			//update existing shopping cart or wishlist  item?
			ShoppingCartItem updatecartitem = null;
			if (_shoppingCartSettings.AllowCartItemEditing && updatecartitemid > 0)
			{
				var cart = customer.ShoppingCartItems
					 .LimitPerStore(storeId)
					 .ToList();
				updatecartitem = cart.FirstOrDefault(x => x.Id == updatecartitemid);
			}

			//activity log
			_customerActivityService.InsertActivity("PublicStore.ViewProduct", _localizationService.GetResource("ActivityLog.PublicStore.ViewProduct"), product.Name + "  from app");

			//model
			var model = _productModelFactory.PrepareProductDetailsModel(product, updatecartitem);

			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult GetProductById(int id, int userid, int updatecartitemid = 0)
		{
			if (id <= 0)
			{
				return Json(new { success = false, message = "Bad request" });
			}

			var product = _productApiService.GetProductById(id);

			//Check Product is Simple or grouped product					

			if (product == null)
			{
				return Json(new { success = false, message = "Product not found" });
			}

			var customer = _customerService.GetCustomerById(userid);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			//update existing shopping cart or wishlist  item?
			ShoppingCartItem updatecartitem = null;
			if (_shoppingCartSettings.AllowCartItemEditing && updatecartitemid > 0)
			{
				var cart = customer.ShoppingCartItems
					 .LimitPerStore(storeId)
					 .ToList();
				updatecartitem = cart.FirstOrDefault(x => x.Id == updatecartitemid);
			}

			var model = _productModelFactory.PrepareProductDetailsModel(product, updatecartitem);

			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region HOME FEATURED PRODUCTS

		public ActionResult FeaturedProducts()
		{
			var products = _productService.GetAllProductsDisplayedOnHomePage();
			//ACL and store mapping
			products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
			//availability dates
			products = products.Where(p => p.IsAvailable()).ToList();

			if (!products.Any())
				return Json(new { success = false, message = "No products found" }, JsonRequestBehavior.AllowGet);

			var model = _productModelFactory.PrepareProductOverviewModels(products).ToList();
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Search By SKU

		public ActionResult GetProductBySku(string sku)
		{
			var product = _productApiService.GetProductBySku(sku);
			if (product == null)
			{
				var model = new ProductShortDetailModel
				{
					Id = 0,
					Name = "",
					SKU = "",
					Comments = ""
				};
				return Json(new { success = false, message = "No product found.", data = model }, JsonRequestBehavior.AllowGet);
			}
			var productShortDetailModel = new ProductShortDetailModel
			{
				Id = product.Id,
				Name = product.Name,
				SKU = product.Sku,
				Comments = product.AdminComment
			};
			return Json(new { success = true, message = "", data = productShortDetailModel }, JsonRequestBehavior.AllowGet);
		}

		#endregion



		#region Check Stock

		public ActionResult Checkstock(string productname)
		{
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			var products = _productService.SearchProducts(
			showHidden: true,
			keywords: productname,
			storeId: storeId
			);

			var prodList = new List<ProductDetailsModel>();
			foreach (var item in products)
			{
				var model = new ProductDetailsModel
				{
					Name = item.Name,
					Sku = item.Sku,
					StockAvailability = item.StockQuantity.ToString(CultureInfo.InvariantCulture),
					IsFreeShipping = item.IsFreeShipping
				};
				prodList.Add(model);
			}

			if (prodList.Count < 0)
			{
				return Json(new { success = false, message = "No products found" }, JsonRequestBehavior.AllowGet);
			}
			// return model 
			return Json(new { success = true, message = "", data = prodList }, JsonRequestBehavior.AllowGet);

		}

		#endregion

		#region RelatedProducts

		public ActionResult RelatedProducts(int productId)
		{
			var productIds = _productService.GetRelatedProductsByProductId1(productId).Select(x => x.ProductId2).ToArray();

			//load products
			var products = _productService.GetProductsByIds(productIds);

			//availability dates
			products = products.Where(p => p.IsAvailable()).ToList();

			if (!products.Any())
				return Json(new { success = false, message = "no related products" }, JsonRequestBehavior.AllowGet);

			var model = _productModelFactory.PrepareProductOverviewModels(products).ToList();

			return Json(new { success = false, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Email a friend

		public ActionResult ProductEmailAFriend(int productId)
		{
			var product = _productService.GetProductById(productId);
			if (product == null || product.Deleted || !product.Published || !_catalogSettings.EmailAFriendEnabled)
				return Json(new { success = false, message = "No product found" }, JsonRequestBehavior.AllowGet);

			var model = new ProductEmailAFriendModel();
			model = _productModelFactory.PrepareProductEmailAFriendModel(model, product, false);
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

	
		public ActionResult ApiProductEmailSend(string details)
		{

			var postModel = new JavaScriptSerializer().Deserialize<ProductEmailAFriendModel>(details);

			var product = _productService.GetProductById(postModel.ProductId);
			if (product == null || product.Deleted || !product.Published || !_catalogSettings.EmailAFriendEnabled)
				return Json(new { success = false, message = "No product found" }, JsonRequestBehavior.AllowGet);
			try
			{
				var CurrentCustomer = _customerService.GetCustomerById(postModel.customerId);
				//email
				_workflowMessageService.SendProductEmailAFriendMessage(CurrentCustomer,
							 1, product,
							  postModel.YourEmailAddress, postModel.FriendEmail,
							  Core.Html.HtmlHelper.FormatText(postModel.PersonalMessage, false, true, false, false, false, false));

				var model = _productModelFactory.PrepareProductEmailAFriendModel(postModel, product, true);
				model.SuccessfullySent = true;
				//model.Result = _localizationService.GetResource("Products.EmailAFriend.SuccessfullySent");

				return Json(new { success = true, message = _localizationService.GetResource("Products.EmailAFriend.SuccessfullySent"), data = model }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
			}


		}

		#endregion

		#region Product reviews


		public  ActionResult ProductReviews(int productId, int userId)
		{
			var product = _productService.GetProductById(productId);
			if (product == null || product.Deleted || !product.Published || !product.AllowCustomerReviews)
				return Json(new { success = false, message = "Either product is deleted or reviews not allowed to customer" }, JsonRequestBehavior.AllowGet);


			var model = new ProductReviewsModel();
			model = _productModelFactory.PrepareProductReviewsModel(model, product);

			var CurrentCustomer = _customerService.GetCustomerById(userId);

			//only registered users can leave reviews
			if (CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
				//	ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));
				return Json(new { success = false, message = _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews") }, JsonRequestBehavior.AllowGet);


			if (_catalogSettings.ProductReviewPossibleOnlyAfterPurchasing &&
				 !_orderService.SearchOrders(customerId: CurrentCustomer.Id, productId: productId, osIds: new List<int> { (int)OrderStatus.Complete }).Any())
				//ModelState.AddModelError(string.Empty, _localizationService.GetResource("Reviews.ProductReviewPossibleOnlyAfterPurchasing"));
				return Json(new { success = false, message = _localizationService.GetResource("Reviews.ProductReviewPossibleOnlyAfterPurchasing") }, JsonRequestBehavior.AllowGet);

			//default value
			model.AddProductReview.Rating = _catalogSettings.DefaultProductRatingValue;
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);

		}

		
		public  ActionResult ProductReviewsAdd(string details)
		{

			var model = new JavaScriptSerializer().Deserialize<ProductReviewsModel>(details);

			var product = _productService.GetProductById(model.ProductId);
			//if (product == null || product.Deleted || !product.Published || !product.AllowCustomerReviews)
			//	return Json(new { success = false, message = " No products found" }, JsonRequestBehavior.AllowGet);

			var customer = _customerService.GetCustomerById(model.UserId);
			int storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			if (_catalogSettings.ProductReviewPossibleOnlyAfterPurchasing &&
				 !_orderService.SearchOrders(customerId: customer.Id, productId: model.ProductId, osIds: new List<int> { (int)OrderStatus.Complete }).Any())
				//ModelState.AddModelError(string.Empty, _localizationService.GetResource("Reviews.ProductReviewPossibleOnlyAfterPurchasing"));
				return Json(new { success = false, message = _localizationService.GetResource("Reviews.ProductReviewPossibleOnlyAfterPurchasing") }, JsonRequestBehavior.AllowGet);

			

			//save review
			int rating = model.AddProductReview.Rating;
			if (rating < 1 || rating > 5)
				rating = _catalogSettings.DefaultProductRatingValue;
			bool isApproved = !_catalogSettings.ProductReviewsMustBeApproved;

			var productReview = new ProductReview
			{
				ProductId = product.Id,
				CustomerId = model.UserId,
				Title = model.TTitle,
				ReviewText = model.TReviewText,
				Rating = rating,
				HelpfulYesTotal = 0,
				HelpfulNoTotal = 0,
				IsApproved = isApproved,
				CreatedOnUtc = DateTime.UtcNow,
				StoreId = storeId,
			};
			product.ProductReviews.Add(productReview);
			_productService.UpdateProduct(product);

			//update product totals
			_productService.UpdateProductReviewTotals(product);

			//notify store owner
			if (_catalogSettings.NotifyStoreOwnerAboutNewProductReviews)
				_workflowMessageService.SendProductReviewNotificationMessage(productReview, _localizationSettings.DefaultAdminLanguageId);

			//activity log
			_customerActivityService.InsertActivity("PublicStore.AddProductReview", _localizationService.GetResource("ActivityLog.PublicStore.AddProductReview"), product.Name);

			//raise event
			if (productReview.IsApproved)
				_eventPublisher.Publish(new ProductReviewApprovedEvent(productReview));

			model = _productModelFactory.PrepareProductReviewsModel(model, product);
			model.AddProductReview.Title = null;
			model.AddProductReview.ReviewText = null;

			model.AddProductReview.SuccessfullyAdded = true;
			if (!isApproved)
			{
				model.AddProductReview.Result = _localizationService.GetResource("Reviews.SeeAfterApproving");
				return Json(new { success = true, message = _localizationService.GetResource("Reviews.SeeAfterApproving"), data = model }, JsonRequestBehavior.AllowGet);
			}
			else
			{
				model.AddProductReview.Result = _localizationService.GetResource("Reviews.SuccessfullyAdded");
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
			}


		}

		#endregion

	}
}
