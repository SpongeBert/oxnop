﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.WebApi.Models.Common;
using Nop.Services.Catalog;
using Nop.Services.Media;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Themes;
using Nop.Services.Localization;
using Nop.Core.Domain.Messages;
using Nop.Services.Messages;
using Nop.Plugin.WebApi.Models.Email;
using System.Web.Script.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using Nop.Services.Configuration;
using Nop.Plugin.WebApi.Factories;
using Nop.Services.Stores;
using Nop.Plugin.WebApi.Models.Category;
using Nop.Services.Customers;
using Nop.Core.Data;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Services;

namespace Nop.Plugin.WebApi.Controllers
{
	public class ApiCommonController : BasePluginController
	{
		#region Fields
		
		private readonly IPictureService _pictureService;
		private readonly ICacheManager _cacheManager;
		private readonly StoreInformationSettings _storeInformationSettings;
		private readonly IThemeContext _themeContext;
		private readonly IWebHelper _webHelper;
		private readonly CatalogSettings _catalogSettings;
		private readonly IProductService _productService;
		private readonly EmailAccountSettings _emailAccountSettings;
		private readonly IEmailAccountService _emailAccountService;
		private readonly IQueuedEmailService _queuedEmailService;
		private readonly ISettingService _settingService;
		private readonly IProductModelFactory _productModelFactory;
		private readonly ICategoryService _categoryService;
		private readonly IStoreService _storeService;
		private readonly IRepository<CmsProductNopProductMapping> _cmsProductNopProductMappingRepository;
		private readonly ICustomerService _customerService;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly ILanguageService _languageService;
		#endregion

		#region Cont

		public ApiCommonController(IPictureService pictureService,
				ICacheManager cacheManager, StoreInformationSettings storeInformationSettings, IThemeContext themeContext,
				IWebHelper webHelper, CatalogSettings catalogSettings, IProductService productService, EmailAccountSettings emailAccountSettings,
				IEmailAccountService emailAccountService, IQueuedEmailService queuedEmailService, ISettingService settingService, IProductModelFactory productModelFactory,
			 ICategoryService categoryService, IStoreService storeService, IRepository<CmsProductNopProductMapping> cmsProductNopProductMappingRepository, ICustomerService customerService,
			ICmsNopProductMappingService cmsNopProductMappingService, ILanguageService languageService)
		{
		
			
			_pictureService = pictureService;
			_cacheManager = cacheManager;
			_storeInformationSettings = storeInformationSettings;
			_themeContext = themeContext;
			_webHelper = webHelper;
			_catalogSettings = catalogSettings;
			_productService = productService;
			_emailAccountSettings = emailAccountSettings;
			_emailAccountService = emailAccountService;
			_queuedEmailService = queuedEmailService;
			_settingService = settingService;
			_productModelFactory = productModelFactory;
			_categoryService = categoryService;
			_storeService = storeService;
			_cmsProductNopProductMappingRepository = cmsProductNopProductMappingRepository;
			_customerService = customerService;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_languageService = languageService;
		}

		#endregion

		#region LOGO

		public ActionResult Logo()
		{
			var CurrentStore = _storeService.GetAllStores().FirstOrDefault();
			var model = new LogModel
			{					
				StoreName = CurrentStore.GetLocalized(x => x.Name)
			};

			var cacheKey = string.Format("Nop.pres.logo-{0}-{1}-{2}", CurrentStore.Id, _themeContext.WorkingThemeName, _webHelper.IsCurrentConnectionSecured());

			model.LogoPath = _cacheManager.Get(cacheKey, () =>
			{
				var logo = "";
				var logoPictureId = _storeInformationSettings.LogoPictureId;
				if (logoPictureId > 0)
				{
					logo = _pictureService.GetPictureUrl(logoPictureId, showDefaultPicture: false);
				}
				if (String.IsNullOrEmpty(logo))
				{
					//use default logo
					logo = string.Format("{0}Themes/{1}/Content/images/logo.png", _webHelper.GetStoreLocation(), _themeContext.WorkingThemeName);
				}

				return logo;

			});

			return Json(new { success = true, message = "", data = model.LogoPath }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region SEARCH TERM AUTOCOMPLETE

		public ActionResult SearchTermAutoComplete(string term)
		{
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			if (String.IsNullOrWhiteSpace(term) || term.Length < _catalogSettings.ProductSearchTermMinimumLength)
				return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);

			//products
			var productNumber = _catalogSettings.ProductSearchAutoCompleteNumberOfProducts > 0 ?
				 _catalogSettings.ProductSearchAutoCompleteNumberOfProducts : 10;

			int languageId = _languageService.GetAllLanguages().FirstOrDefault().Id;

			var products = _productService.SearchProducts(
				 storeId: storeId,
				 keywords: term,
				 languageId: languageId,
				 visibleIndividuallyOnly: true,
				 pageSize: productNumber);

			var modelList = _productModelFactory.PrepareProductOverviewModels(products).ToList();

			return Json(new { success = true, message = "", data = modelList }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Email

		public ActionResult SendNotification(string details)
		{
			var model = new JavaScriptSerializer().Deserialize<EmailModel>(details);

			#region createPDF

			var fileName = string.Format("sample_{0}_{1}.pdf", "pdf", CommonHelper.GenerateRandomDigitCode(4));
			var filePath = Path.Combine(CommonHelper.MapPath("~/content/files/ExportImport"), fileName);

			var stream = new FileStream(filePath, FileMode.Create);
			var pageSize = PageSize.A4;
			var doc = new Document(pageSize);
			var pdfWriter = PdfWriter.GetInstance(doc, stream);
			doc.Open();

			var headerTable = new PdfPTable(3);
			headerTable.DefaultCell.Border = Rectangle.BOX;

			headerTable.AddCell(new Paragraph(" Sr.no  "));
			headerTable.AddCell(new Paragraph(" ITEM  "));
			headerTable.AddCell(new Paragraph(" Qty  "));
			headerTable.AddCell(new Paragraph(" Comments  "));

			var idx = 1;
			foreach (var items in model.pdfContent)
			{
				headerTable.AddCell(new Paragraph("" + idx));
				headerTable.AddCell(new Paragraph(items.ItemName));
				headerTable.AddCell(new Paragraph("" + items.Qty));
				headerTable.AddCell(new Paragraph("" + items.Comments));
				idx++;
			}

			doc.Add(headerTable);
			doc.Close();

			#endregion

			#region SendEmail

			if (string.IsNullOrEmpty(model.toEmailAddress))
			{
				return Json(new { success = false, message = "To email Address is blank" }, JsonRequestBehavior.AllowGet);
			}

			var bcc = !string.IsNullOrEmpty(_settingService.GetSetting("BCC").Value) ? _settingService.GetSetting("BCC").Value : "";
			var bodyReplaced = "";
			const string subjectReplaced = "List of Products";

			var str = new StringBuilder();
			str.Append("<table><tr> <td>  Hi,	</td> </tr>	<tr><td>	Please find the attached file	 </td></tr>	</table>");
			var emailAccountId = _emailAccountSettings.DefaultEmailAccountId;
			var emailAccount = _emailAccountService.GetEmailAccountById(emailAccountId);

			model.toEmailAddress = model.toEmailAddress.Trim(',');
			var emailList = model.toEmailAddress.Split(',');

			foreach (var email1 in emailList)
			{
				var email = new QueuedEmail
				{
					Priority = QueuedEmailPriority.High,
					From = !string.IsNullOrEmpty(model.fromEmail) ? model.fromEmail : emailAccount.Email,
					FromName = !string.IsNullOrEmpty(model.fromName) ? model.fromName : emailAccount.DisplayName,
					To = email1,
					ToName = model.toName,
					ReplyTo = model.replyToEmailAddress,
					ReplyToName = model.replyToName,
					CC = string.Empty,
					Bcc = bcc,
					Subject = subjectReplaced,
					Body = str.ToString(),
					AttachmentFilePath = filePath,
					AttachmentFileName = fileName,
					CreatedOnUtc = DateTime.UtcNow,
					EmailAccountId = emailAccount.Id
				};
				_queuedEmailService.InsertQueuedEmail(email);

			}

			#endregion


			#region CSV

			var csvFileName = string.Format("sample_{0}_{1}.csv", "csv", CommonHelper.GenerateRandomDigitCode(4));
			var csvFilePath = Path.Combine(CommonHelper.MapPath("~/content/files/ExportImport"), csvFileName);

			var fs = new FileStream(csvFilePath, FileMode.Append, FileAccess.Write);
			var sw = new StreamWriter(fs);
			sw.WriteLine("Sr.no,ITEM,Qty,Comments");

			var idx2 = 1;
			foreach (var items in model.pdfContent)
			{
				sw.WriteLine(idx2 + "," + items.ItemName + "," + items.Qty + "," + items.Comments);
				idx2++;
			}
			sw.Close();
			fs.Close();

			var emailToAdmin = new QueuedEmail
			{
				Priority = QueuedEmailPriority.High,
				From = emailAccount.Email,
				FromName = emailAccount.DisplayName,
				To = !string.IsNullOrEmpty(_settingService.GetSetting("AdminEmailID").Value) ? _settingService.GetSetting("AdminEmailID").Value : "", //settingService.GetSetting("AdminEmailID")==null ? "" : settingService.GetSetting("AdminEmailID"),
				ToName = !string.IsNullOrEmpty(_settingService.GetSetting("AdminEmailID").Value) ? _settingService.GetSetting("AdminEmailID").Value : "",
				ReplyTo = string.Empty,
				ReplyToName = string.Empty,
				CC = string.Empty,
				Subject = subjectReplaced,
				Body = str.ToString(),
				AttachmentFilePath = csvFilePath,
				AttachmentFileName = csvFileName,
				CreatedOnUtc = DateTime.UtcNow,
				EmailAccountId = emailAccount.Id
			};
			_queuedEmailService.InsertQueuedEmail(emailToAdmin);

			#endregion

			return Json(new { success = true, message = "email sent successfully!" }, JsonRequestBehavior.AllowGet);
		}



		public ActionResult SendEmail(string details)
		{
			var model = new JavaScriptSerializer().Deserialize<CommonEmailModel>(details);

			#region SendEmail
			if (string.IsNullOrEmpty(model.toEmailAddress))
				return Json(new { success = false, message = "To email Address is blank" }, JsonRequestBehavior.AllowGet);


			var bcc = !string.IsNullOrEmpty(_settingService.GetSetting("BCC").Value) ? _settingService.GetSetting("BCC").Value : "";

			var emailAccountId = _emailAccountSettings.DefaultEmailAccountId;
			var emailAccount = _emailAccountService.GetEmailAccountById(emailAccountId);

			model.toEmailAddress = model.toEmailAddress.Trim(',');
			var emailList = model.toEmailAddress.Split(',');

			foreach (var email1 in emailList)
			{
				var email = new QueuedEmail
				{
					Priority = QueuedEmailPriority.High,
					From = !string.IsNullOrEmpty(model.fromEmail) ? model.fromEmail : emailAccount.Email,
					FromName = !string.IsNullOrEmpty(model.fromName) ? model.fromName : emailAccount.DisplayName,
					To = email1,
					ToName = model.toName,
					CC = string.Empty,
					Bcc = bcc,
					Subject = model.subject,
					Body = model.content,
					CreatedOnUtc = DateTime.UtcNow,
					EmailAccountId = emailAccount.Id
				};
				_queuedEmailService.InsertQueuedEmail(email);
			}
			#endregion
			return Json(new { success = true, message = "email sent successfully!" }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult CreatePdf()
		{
			var fileName = string.Format("sample_{0}_{1}.pdf", "pdf", CommonHelper.GenerateRandomDigitCode(4));
			var filePath = Path.Combine(CommonHelper.MapPath("~/content/files/ExportImport"), fileName);

			var stream = new FileStream(filePath, FileMode.Create);
			var pageSize = PageSize.A4;
			var doc = new Document(pageSize);
			var pdfWriter = PdfWriter.GetInstance(doc, stream);
			doc.Open();

			doc.Add(new Paragraph("----Sample PDF File----"));
			doc.Add(new Paragraph("Name : John Adams"));
			doc.Add(new Paragraph("Email: admin@yourstore.com"));
			doc.Add(new Paragraph("Mobile : 9874563210"));
			doc.Close();

			return Json(new { success = true, message = "PDF created successfully!" }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Utility
		public string GetPublicName(string categoryName)
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == categoryName.ToLower());

			if (category == null)
			{
				return categoryName;
			};

			int languageId = _languageService.GetAllLanguages().FirstOrDefault().Id;

			var publicDisplayName = category.GetLocalized(x => x.PublicDisplayName, languageId);

			if (string.IsNullOrEmpty(publicDisplayName))
			{
				return categoryName;
			}

			return publicDisplayName;
		}
		#endregion

		#region HomePage Product Sliders

		public ActionResult GetDayOfferProducts(string CuserId)
		{
			int UserId = 0;
			if (CuserId != "null")
			{ 				
					UserId = Convert.ToInt32(CuserId);				
			}
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "gerechten");
			if (category == null)
			{
				return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);
			}

			//Get company details 
			var customer = _customerService.GetCustomerById(UserId);
	
			var companyId = CheckCompanyMappingAvailability(customer.Username != null ? customer.Username.Substring(0, 3) : "001");

			var childCategories = _categoryService.GetAllCategoriesByParentCategoryId(category.Id);
			if (childCategories.Any())
			{
				var model = new HomePageSubCategoryModel();
				foreach (var childCategory in childCategories)
				{
					var flag = 0;
					var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { childCategory.Id });
					//	if (!categoryProducts.Any())
					//	continue;		

					foreach (var product in categoryProducts)
					{
						var productCompanyMapping = (from cmsProduct in _cmsProductNopProductMappingRepository.Table
															  where cmsProduct.Company == companyId && cmsProduct.NopProductId == product.Id
															  select cmsProduct).FirstOrDefault();

						if (productCompanyMapping != null)
						{
							flag = 1;
							break;
						}
					}

					if (flag == 1)
					{
						var subCategoryRecord = new CustomSubCategories
						{
							SubCategoryId = childCategory.Id,
							SubCategoryName = GetPublicName(childCategory.Name)
						};
						model.SubCategoryList.Add(subCategoryRecord);
					}
				}
				model.ParentCategoryId = category.Id;
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
			}
			else
			{
				//var categoryList = new List<int> { category.Id };
				//var dayProducts = _productService.SearchProducts(categoryIds: categoryList);
				//var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder), userId: UserId);
				//return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);

				var dayProducts = _categoryService.GetProductCategoriesByCategoryId(category.Id);

				List<Product> prodList = new List<Product>();
				foreach (var prod in dayProducts)
				{
					prodList.Add(prod.Product);
				} 			
				var model = _productModelFactory.PrepareProductOverviewModels(prodList.ToList(), userId: UserId);
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);

			}

		}

		public ActionResult GetLunchProducts(string CuserId)
		{ 	
			 	 int UserId = 0;
				 if (CuserId != "null")
				 {
					 if (CuserId == "undefined")
					 {
						 UserId = 0;
					 }
					 else
					 {
						 UserId = Convert.ToInt32(CuserId);
					 }
				 }
				 var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "broodjes");
				 if (category == null)
				 {
					 return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);
				 }
				 //Get company details 
				 //	var companyId = UserId > 0 ? UserId.ToString().Substring(0, 3) : "001";

				 var customer = _customerService.GetCustomerById(UserId);
				 //var companyId = UserId > 0 ? UserId.ToString().Substring(0, 3) : "001";

				// var companyId = customer.Username != null ? customer.Username.Substring(0, 3) : "001";
				 var companyId = CheckCompanyMappingAvailability(customer.Username != null ? customer.Username.Substring(0, 3) : "001");

				 var childCategories = _categoryService.GetAllCategoriesByParentCategoryId(category.Id);
				 if (childCategories.Any())
				 {
					 var model = new HomePageSubCategoryModel();
					 foreach (var childCategory in childCategories)
					 {
						 var flag = 0;

						 var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { childCategory.Id });
						 //	if (!categoryProducts.Any())
						 //		continue;



						 foreach (var product in categoryProducts)
						 {
							 var productCompanyMapping = (from cmsProduct in _cmsProductNopProductMappingRepository.Table
																	where cmsProduct.Company == companyId && cmsProduct.NopProductId == product.Id
																	select cmsProduct).FirstOrDefault();

							 if (productCompanyMapping != null)
							 {
								 flag = 1;
								 break;
							 }
						 }

						 if (flag == 1)
						 {
							 var customSubCategoryRecord = new CustomSubCategories
							 {
								 SubCategoryId = childCategory.Id,
								 SubCategoryName = GetPublicName(childCategory.Name)
							 };
							 model.SubCategoryList.Add(customSubCategoryRecord);
						 }
					 }
					 model.ParentCategoryId = category.Id;
					 var Subcat1 = model.SubCategoryList.FirstOrDefault();
					 var dayProducts1 = _categoryService.GetProductCategoriesByCategoryId(Subcat1.SubCategoryId);	
				  
					 List<Product> prodList = new List<Product>();
					 foreach (var prod in dayProducts1)
					 {
						 prodList.Add(prod.Product);
					 }

					 var Pmodel = _productModelFactory.PrepareProductOverviewModels(prodList.ToList(), userId: UserId);
					 return Json(new { success = true, message = "", data = Pmodel }, JsonRequestBehavior.AllowGet);
					// return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);

				 }
				 else
				 {

					 var lunchProducts = _categoryService.GetProductCategoriesByCategoryId(category.Id);

					 List<Product> prodList = new List<Product>();
					 foreach (var prod in lunchProducts)
					 {
						 prodList.Add(prod.Product);
					 }
					 var model = _productModelFactory.PrepareProductOverviewModels(prodList.ToList(), userId: UserId);
					 return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
				 }
		}

		public ActionResult GetDrinkProducts(string CuserId)
		{
			int UserId = 0;
			if (CuserId != "null")
			{
				if (CuserId == "undefined")
				{
					UserId = 0;
				}
				else
				{
					UserId = Convert.ToInt32(CuserId);
				}
			}
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "dranken");
			if (category == null)
			{
				return Json(new { success = false, message = "No record exists" }, JsonRequestBehavior.AllowGet);
			}

			//Get company details 
			//var companyId = UserId > 0 ? UserId.ToString().Substring(0, 3) : "001";

			var customer = _customerService.GetCustomerById(UserId);
			//var companyId = UserId > 0 ? UserId.ToString().Substring(0, 3) : "001";

			//var companyId = customer.Username != null ? customer.Username.Substring(0, 3) : "001";
			var companyId = CheckCompanyMappingAvailability(customer.Username != null ? customer.Username.Substring(0, 3) : "001");

			var childCategories = _categoryService.GetAllCategoriesByParentCategoryId(category.Id);
			if (childCategories.Any())
			{
				var model = new HomePageSubCategoryModel();
				foreach (var childCategory in childCategories)
				{
					var flag = 0;
					var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { childCategory.Id });
					//	if (!categoryProducts.Any())
					//	continue;		

					foreach (var product in categoryProducts)
					{
						var productCompanyMapping = (from cmsProduct in _cmsProductNopProductMappingRepository.Table
															  where cmsProduct.Company == companyId && cmsProduct.NopProductId == product.Id
															  select cmsProduct).FirstOrDefault();

						if (productCompanyMapping != null)
						{
							flag = 1;
							break;
						}
					}

					if (flag == 1)
					{
						var subCategoryRecord = new CustomSubCategories
						{
							SubCategoryId = childCategory.Id,
							SubCategoryName = GetPublicName(childCategory.Name)
						};
						model.SubCategoryList.Add(subCategoryRecord);
					}
				}
				model.ParentCategoryId = category.Id;
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
			}
			else
			{
				var drinkProducts = _categoryService.GetProductCategoriesByCategoryId(category.Id);

				List<Product> prodList = new List<Product>();
				foreach (var prod in drinkProducts)
				{
					prodList.Add(prod.Product);
				}

				//var model = _productModelFactory.PrepareProductOverviewModels(drinkProducts.ToList().OrderBy(x => x.DisplayOrder), userId: UserId);

				var model = _productModelFactory.PrepareProductOverviewModels(prodList.ToList(), userId: UserId);
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
			}
		}


		public string CheckCompanyMappingAvailability(string actualCompanyId)
		{
			var checkMappings = _cmsNopProductMappingService.GetByCompanyId(actualCompanyId);
			if (checkMappings.Any())
			{
				return actualCompanyId;
			}
			else
			{
				return "001";
			}

		}

		#endregion

	}
}
