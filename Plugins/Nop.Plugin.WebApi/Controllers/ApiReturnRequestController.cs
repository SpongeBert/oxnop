﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Plugin.WebApi.Factories;
using Nop.Plugin.WebApi.Models.Order;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;

namespace Nop.Plugin.WebApi.Controllers
{
	public partial class ApiReturnRequestController : BasePluginController
    {
        #region Fields

        private readonly IReturnRequestModelFactory _returnRequestModelFactory;
        private readonly IReturnRequestService _returnRequestService;
        private readonly IOrderService _orderService;
        private readonly IWorkContext _workContext;
        private readonly IStoreService _storeService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ICustomNumberFormatter _customNumberFormatter;
        private readonly IDownloadService _downloadService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly OrderSettings _orderSettings;

        #endregion

        #region Constructors

        public ApiReturnRequestController(IReturnRequestModelFactory returnRequestModelFactory,
            IReturnRequestService returnRequestService,
            IOrderService orderService, 
            IWorkContext workContext, 
            IStoreService storeService,
            IOrderProcessingService orderProcessingService,
            ILocalizationService localizationService,
            ICustomerService customerService,
            IWorkflowMessageService workflowMessageService,
            ICustomNumberFormatter customNumberFormatter,
            IDownloadService downloadService,
            LocalizationSettings localizationSettings,
            OrderSettings orderSettings)
        {
            this._returnRequestModelFactory = returnRequestModelFactory;
            this._returnRequestService = returnRequestService;
            this._orderService = orderService;
            this._workContext = workContext;
				this._storeService = storeService;
            this._orderProcessingService = orderProcessingService;
            this._localizationService = localizationService;
            this._customerService = customerService;
            this._workflowMessageService = workflowMessageService;
            this._customNumberFormatter = customNumberFormatter;
            this._downloadService = downloadService;
            this._localizationSettings = localizationSettings;
            this._orderSettings = orderSettings;
        }

        #endregion

        #region Methods

		  public ActionResult CustomerReturnRequests(int customerId)
        {
           var model = _returnRequestModelFactory.PrepareCustomerReturnRequestsModel(customerId);
			  if(model == null)
			  {
				  return Json(new { success = false, message = "No records found" }, JsonRequestBehavior.AllowGet);
			  }

            return Json(model,JsonRequestBehavior.AllowGet);	
        }

     
        public  ActionResult ReturnRequest(int orderId,int userId)
        {
            var order = _orderService.GetOrderById(orderId);	
				//if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
				if (order == null || order.Deleted || userId != order.CustomerId)
				{ 					
					return Json(new { success = false, message = "Order does not exists" }, JsonRequestBehavior.AllowGet);
				}

            if (!_orderProcessingService.IsReturnRequestAllowed(order))
				{				
					return Json(new { success = false, message = "Return Request not allowed" }, JsonRequestBehavior.AllowGet);
				}

            var model = new SubmitReturnRequestModel();
            model = _returnRequestModelFactory.PrepareSubmitReturnRequestModel(model, order);
            //return Json(model,JsonRequestBehavior.AllowGet);
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);

        }

		  //[HttpPost, ActionName("ReturnRequest")]
		  //[ValidateInput(false)]
		  //[PublicAntiForgery]
        public  ActionResult ReturnRequestSubmit(string details)
        {
			  //string details = "{OrderId:2043;CustmerID:1;ReturnRequestReasonId:1;ReturnRequestActionId:2;Comments:'testing';Items:'[{ProductID:'57',Quantity:'1'},{ProductID:'57',Quantity:'2'}]' }";
			 // string details1 = "{'OrderId':'2043','CustomerID':'1','ReturnRequestReasonId':'1','ReturnRequestActionId':'2','Comments':'testing','Items':[{'ProductID':'57','Quantity':'1'}] }";

			  SubmitReturnRequestModel response = new JavaScriptSerializer().Deserialize<SubmitReturnRequestModel>(details);
            var order = _orderService.GetOrderById(response.OrderId);
				int custId = response.CustomerID;

				//if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
				if (order == null || order.Deleted || custId != order.CustomerId)				
				return Json(new { success = false, message = "Order does not exists" }, JsonRequestBehavior.AllowGet);		

				if (!_orderProcessingService.IsReturnRequestAllowed(order))
				{ 					
					return Json(new { success = false, message = "Return Request not allowed" }, JsonRequestBehavior.AllowGet);					
				}

            int count = 0;

            var downloadId = 0;
            if (_orderSettings.ReturnRequestsAllowFiles)
            {
					var download = _downloadService.GetDownloadByGuid(response.UploadedFileGuid);
                if (download != null)
                    downloadId = download.Id;  					
            }

            //returnable products
            var orderItems = order.OrderItems.Where(oi => !oi.Product.NotReturnable);

				var currentCustomer = _customerService.GetCustomerById(custId);

            foreach (var orderItem in orderItems)
            {
               int quantity = 0; //parse quantity
					if(response !=null)
					{
						var responseItems = response.Items;
 						foreach(var itm in responseItems)
						{
							quantity = itm.Quantity;
						}

					}
					 //foreach (string formKey in form.AllKeys)
					 //	 if (formKey.Equals(string.Format("quantity{0}", orderItem.Id), StringComparison.InvariantCultureIgnoreCase))
					 //	 {
					 //		  int.TryParse(form[formKey], out quantity);
					 //		  break;
					 //	 }

                if (quantity > 0)
                {
						 var rrr = _returnRequestService.GetReturnRequestReasonById(response.ReturnRequestReasonId);
						 var rra = _returnRequestService.GetReturnRequestActionById(response.ReturnRequestActionId);
                    
                    var rr = new ReturnRequest
                    {
                        CustomNumber = "",
                        StoreId = _storeService.GetAllStores().FirstOrDefault().Id,
                        OrderItemId = orderItem.Id,
                        Quantity = quantity,
								CustomerId = custId,
                        ReasonForReturn = rrr != null ? rrr.GetLocalized(x => x.Name) : "not available",
                        RequestedAction = rra != null ? rra.GetLocalized(x => x.Name) : "not available",
								CustomerComments = response.Comments,
                        UploadedFileId = downloadId,
                        StaffNotes = string.Empty,
                        ReturnRequestStatus = ReturnRequestStatus.Pending,
                        CreatedOnUtc = DateTime.UtcNow,
                        UpdatedOnUtc = DateTime.UtcNow
                    };
                    currentCustomer.ReturnRequests.Add(rr);
						  _customerService.UpdateCustomer(currentCustomer);
                    //set return request custom number
                    rr.CustomNumber = _customNumberFormatter.GenerateReturnRequestCustomNumber(rr);
						  _customerService.UpdateCustomer(currentCustomer);
                    //notify store owner
                    _workflowMessageService.SendNewReturnRequestStoreOwnerNotification(rr, orderItem, _localizationSettings.DefaultAdminLanguageId);
                    //notify customer
                    _workflowMessageService.SendNewReturnRequestCustomerNotification(rr, orderItem, order.CustomerLanguageId);

                    count++;
                }
            }

				var model = _returnRequestModelFactory.PrepareSubmitReturnRequestModel(response, order);
            if (count > 0)
                model.Result = _localizationService.GetResource("ReturnRequests.Submitted");
            else
                model.Result = _localizationService.GetResource("ReturnRequests.NoItemsSubmitted");

           // return Json(model, JsonRequestBehavior.AllowGet);	 
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public  ActionResult UploadFileReturnRequest()
        {
            if (!_orderSettings.ReturnRequestsEnabled && !_orderSettings.ReturnRequestsAllowFiles)
            {
                return Json(new
                {
                    success = false,
                    downloadGuid = Guid.Empty,
                }, MimeTypes.TextPlain);
            }

            //we process it distinct ways based on a browser
            //find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
            Stream stream = null;
            var fileName = "";
            var contentType = "";
            if (String.IsNullOrEmpty(Request["qqfile"]))
            {
                // IE
                HttpPostedFileBase httpPostedFile = Request.Files[0];
                if (httpPostedFile == null)					
					 return Json(new { success = false, message = "No file uploaded" }, JsonRequestBehavior.AllowGet);
                stream = httpPostedFile.InputStream;
                fileName = Path.GetFileName(httpPostedFile.FileName);
                contentType = httpPostedFile.ContentType;
            }
            else
            {
                //Webkit, Mozilla
                stream = Request.InputStream;
                fileName = Request["qqfile"];
            }

            var fileBinary = new byte[stream.Length];
            stream.Read(fileBinary, 0, fileBinary.Length);

            var fileExtension = Path.GetExtension(fileName);
            if (!String.IsNullOrEmpty(fileExtension))
                fileExtension = fileExtension.ToLowerInvariant();

            int validationFileMaximumSize = _orderSettings.ReturnRequestsFileMaximumSize;
            if (validationFileMaximumSize > 0)
            {
                //compare in bytes
                var maxFileSizeBytes = validationFileMaximumSize * 1024;
                if (fileBinary.Length > maxFileSizeBytes)
                {
                    //when returning JSON the mime-type must be set to text/plain
                    //otherwise some browsers will pop-up a "Save As" dialog.
                    return Json(new
                    {
                        success = false,
                        message = string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), validationFileMaximumSize),
                        downloadGuid = Guid.Empty,
                    }, MimeTypes.TextPlain);
                }
            }

            var download = new Download
            {
                DownloadGuid = Guid.NewGuid(),
                UseDownloadUrl = false,
                DownloadUrl = "",
                DownloadBinary = fileBinary,
                ContentType = contentType,
                //we store filename without extension for downloads
                Filename = Path.GetFileNameWithoutExtension(fileName),
                Extension = fileExtension,
                IsNew = true
            };
            _downloadService.InsertDownload(download);

            //when returning JSON the mime-type must be set to text/plain
            //otherwise some browsers will pop-up a "Save As" dialog.
            return Json(new
            {
                success = true,
                message = _localizationService.GetResource("ShoppingCart.FileUploaded"),
                downloadUrl = Url.Action("GetFileUpload", "Download", new {downloadId = download.DownloadGuid}),
                downloadGuid = download.DownloadGuid,
            }, MimeTypes.TextPlain);
        }

        #endregion
    }
}
