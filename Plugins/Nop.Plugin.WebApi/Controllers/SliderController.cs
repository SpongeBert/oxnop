﻿using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Web.Framework.Controllers;
using System.Web.Mvc;
using Nop.Core.Infrastructure;
using Nop.Plugin.WebApi.Models.Slider;

namespace Nop.Plugin.WebApi.Controllers
{
	public class SliderController : BasePluginController
	{
		#region fields
		private readonly IStoreContext _storeContext;
		private readonly IPictureService _pictureService;
		private readonly ISettingService _settingService;
		#endregion

		#region const

		public SliderController()
		{
			_storeContext = EngineContext.Current.Resolve<IStoreContext>();
			_pictureService = EngineContext.Current.Resolve<IPictureService>();
			_settingService = EngineContext.Current.Resolve<ISettingService>();
		}

		#endregion

		#region <PUBLIC BANNER INFO>

		public ActionResult PublicBannerInfo()
		{

			var bannerSetting = _settingService.LoadSetting<NivoSliderSettings>(_storeContext.CurrentStore.Id);

			var model = new BannerDto
			{
				Picture1Url = _pictureService.GetPictureUrl(bannerSetting.Picture1Id, showDefaultPicture: false),
				Text1 = bannerSetting.Text1,
				Link1 = bannerSetting.Link1,
				Picture2Url = _pictureService.GetPictureUrl(bannerSetting.Picture2Id, showDefaultPicture: false),
				Text2 = bannerSetting.Text2,
				Link2 = bannerSetting.Link2,
				Picture3Url = _pictureService.GetPictureUrl(bannerSetting.Picture3Id, showDefaultPicture: false),
				Text3 = bannerSetting.Text3,
				Link3 = bannerSetting.Link3,
				Picture4Url = _pictureService.GetPictureUrl(bannerSetting.Picture4Id, showDefaultPicture: false),
				Text4 = bannerSetting.Text4,
				Link4 = bannerSetting.Link4,
				Picture5Url = _pictureService.GetPictureUrl(bannerSetting.Picture5Id, showDefaultPicture: false),
				Text5 = bannerSetting.Text5,
				Link5 = bannerSetting.Link5
			};

			if (string.IsNullOrEmpty(model.Picture1Url) && string.IsNullOrEmpty(model.Picture2Url) &&
				  string.IsNullOrEmpty(model.Picture3Url) && string.IsNullOrEmpty(model.Picture4Url) &&
				  string.IsNullOrEmpty(model.Picture5Url))
				return Json(new { success = false, message = "pictures not uploaded" }, JsonRequestBehavior.AllowGet);

			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		#endregion

	}
}
