﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Orders;
using Nop.Plugin.WebApi.Factories;
using Nop.Plugin.WebApi.Models.Order;
using Nop.Plugin.WebApi.Models.Payment;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using StarringJane.Worldline.Sips;
using StarringJane.Worldline.Sips.Models;

namespace Nop.Plugin.WebApi.Controllers
{
	public class ApiOrderController : BasePluginController
	{

		#region Fields
		private readonly IOrderModelFactory _orderModelFactory;
		private readonly IOrderService _orderService;
		private readonly IOrderProcessingService _orderProcessingService;
		private readonly ILanguageService _languageService;
		private readonly IWorkflowMessageService _workflowMessageService;

		private readonly IShipmentService _shipmentService;
		private readonly IPaymentService _paymentService;
		private readonly IWebHelper _webHelper;
		private readonly ISettingService _settingService;
		private readonly IStoreService _storeService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ILogger _logger;
		private readonly ICustomerService _customerService;
		
		#endregion

		#region Cont

		public ApiOrderController(IOrderModelFactory orderModelFactory, IOrderService orderService, IOrderProcessingService orderProcessingService,
			ILanguageService languageService, IWorkflowMessageService workflowMessageService, IShipmentService shipmentService,
			IPaymentService paymentService, IWebHelper webHelper, ISettingService settingService, IStoreService storeService,
			IGenericAttributeService genericAttributeService, ILogger logger, ICustomerService customerService)
		{
			_orderModelFactory = orderModelFactory;
			_orderService = orderService;
			_orderProcessingService = orderProcessingService;
			_languageService = languageService;
			_workflowMessageService = workflowMessageService;

			_shipmentService = shipmentService;
			_paymentService = paymentService;
			_webHelper = webHelper;
			_settingService = settingService;
			_storeService = storeService;
			_genericAttributeService = genericAttributeService;
			_logger = logger;
			_customerService = customerService;
		}

		#endregion

		#region methods

		public ActionResult CustomerOrders(int userid = 0)
		{
			if (userid == 0)
				return Json(new { success = false, message = "user does not exists"},JsonRequestBehavior.AllowGet);

			var model = _orderModelFactory.PrepareCustomerOrderListModel(userid);

			return model == null ? Json(new { success = false, message ="No orders placed yet"}, JsonRequestBehavior.AllowGet) : Json(new { success = true, message = "Customer order found.", data =model}, JsonRequestBehavior.AllowGet);
		}

		public ActionResult OrdersDetail(int orderId = 0)
		{
			if (orderId == 0)
				return Json(new { success = false, message = "Order does not exists"},JsonRequestBehavior.AllowGet);

			var order = _orderService.GetOrderById(orderId);
			var model = _orderModelFactory.PrepareOrderDetailsModel(order);
			return model == null ? Json(new { success = false, message = "No orders placed yet" }, JsonRequestBehavior.AllowGet) : Json(new { success = true, message = "Customer order found.", data = model }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult DeleteOrder(int orderId = 0)
		{
			var order = _orderService.GetOrderById(orderId);
			if (order == null)
				return Json(new { success = false, message = "Order does not Exists" }, JsonRequestBehavior.AllowGet);

			_orderProcessingService.DeleteOrder(order);

			return Json(new { success = true, message = "Order deleted successfully" }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult OrderNotesAdd(string details)
		{

			var response = new JavaScriptSerializer().Deserialize<OrderNotesModel>(details);
			var orderId = response.OrderId;
			var message = response.Message;
			const bool displayToCustomer = true;

			var order = _orderService.GetOrderById(orderId);
			if (order == null)
				return Json(new { success = false, message = "Order does not exists" }, JsonRequestBehavior.AllowGet);

			var orderNote = new OrderNote
			{
				DisplayToCustomer = displayToCustomer,
				Note = message,
				CreatedOnUtc = DateTime.UtcNow,
			};
			order.OrderNotes.Add(orderNote);
			_orderService.UpdateOrder(order);

			//new order notification
			if (displayToCustomer)
			{
				//email
				var languageId = _languageService.GetAllLanguages().FirstOrDefault().Id;
				_workflowMessageService.SendNewOrderNoteAddedCustomerNotification(
					 orderNote, languageId);

			}

			return Json(new { success = true, message = "Order notes added successfully" }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult ReOrder(int orderId, int userId = 0)
		{
			var order = _orderService.GetOrderById(orderId);
			if (order == null || order.Deleted || userId != order.CustomerId)
				return Json(new { success = false, message = "Order does not Exists" }, JsonRequestBehavior.AllowGet);

			_orderProcessingService.ReOrder(order);

			return Json(new { success = true, message = "Go to cart to view items" }, JsonRequestBehavior.AllowGet);
		}


		#endregion


		#region payment


		  public ActionResult PlaceOnConfirm(int OrderId)
        {
            var client = new SipsClient();
			  
				Order order = _orderService.GetOrderById(OrderId);	
	        	   
            // 1. Generate a unique identifier for this transaction (so you keep track of the transaction history)
            var transactionReference = GetTransactionReference(client, order);

			
			   int  storeScope = _storeService.GetAllStores().FirstOrDefault().Id;	 
            var _worldlinePaymentSettings = _settingService.LoadSetting<WorldlinePaymentSettings>(storeScope);

				var merchantId = _worldlinePaymentSettings.Account; // Use "002001000000001" if you're using the Sandbox uri
				var interfaceVersion = _worldlinePaymentSettings.InterfaceVersion; // "IR_WS_2.14" is the current interface version
				

				//var merchantId = "002001000000001"; // Use "002001000000001" if you're using the Sandbox uri
				//var interfaceVersion = "IR_WS_2.11"; // "IR_WS_2.14" is the current interface version
			
				var normalReturnUrl = "http://www.oxcfe.be/api/gotomobile/" + OrderId;
				//var normalReturnUrl = "http://www.oxcfe-test.be/api/gotomobile/" + OrderId;
						
				
				var automaticResponseUrl = "http://www.oxcfe.be/api/gotoNotification/" + OrderId;
				//var automaticResponseUrl = "http://www.oxcfe-test.be/api/gotoNotification/" + OrderId;
				  			  							
            var paymentRequest = client.GetPaymentRequest(merchantId, interfaceVersion, transactionReference, normalReturnUrl, automaticResponseUrl, customerIpAddress: _webHelper.GetCurrentIpAddress());

			

            // 3. Set your PaymentRequest data (not Seal, SealAlgorithm, Key & KeyVersion)

				paymentRequest.SetCustomer(order.Customer.Id.ToString(), _languageService.GetLanguageById(order.CustomerLanguageId).LanguageCulture.Split('-')[0]);
				//paymentRequest.SetCustomer(order.Customer.Id.ToString(), _languageService.GetAllLanguages().FirstOrDefault().LanguageCulture.Split('-')[0]);


				var billingContact = new Contact()
				 {
					 Firstname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)),
					 Lastname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName)),
					 Phone = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
					 Email = order.Customer.Email
				 };
				var billingAddress = new StarringJane.Worldline.Sips.Models.Address()
				{
					Street = string.Empty,
					Company = string.Empty,
					City = string.Empty,
					ZipCode = string.Empty,
					Country = string.Empty,
					State = string.Empty
				};
				paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Customer);
				paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Billing);

			
				
				//if (order.ShippingAddress != null)
				//{
				//	 Custom Code - 09112017 - commented as we will not send address
				//	 var shippingContact = new Contact()
				//	 {
				//		  Firstname = Normalize(order.ShippingAddress.FirstName),
				//		  Lastname = Normalize(order.ShippingAddress.LastName),
				//		  Phone = order.ShippingAddress.PhoneNumber,
				//		  Email = order.ShippingAddress.Email
				//	 };
				//	 var shippingAddress = new Address()
				//	 {
				//		  Street = Normalize(string.Format("{0} {1}", order.ShippingAddress.Address1, order.ShippingAddress.Address2).Trim()),
				//		  Company = Normalize(order.ShippingAddress.Company),
				//		  City = Normalize(order.ShippingAddress.City),
				//		  ZipCode = Normalize(order.ShippingAddress.ZipPostalCode.Length > 10 ? order.ShippingAddress.ZipPostalCode.Substring(0, 10) : order.ShippingAddress.ZipPostalCode),
				//		  Country = order.ShippingAddress.Country?.ThreeLetterIsoCode,
				//		  State = order.ShippingAddress.StateProvince?.Name
				//	 };
				//	 paymentRequest.SetAddressAndContactInfo(shippingContact, shippingAddress, AddressType.Delivery);
				//}

            // Worldline only accepts value in smallest unit of currency


				paymentRequest.SetOrderDetails(order.Id.ToString(), (int)Math.Round(order.OrderTotal * 100), order.CustomerCurrencyCode);


				//foreach (var item in order.OrderItems)
				//{
				//	var taxAmount = (item.UnitPriceInclTax - item.UnitPriceExclTax) * 100;
				//	paymentRequest.ShoppingCartDetail.ShoppingCartItemList.Add(new ShoppingCartItem()
				//	{
				//		ProductSKU = item.Product.Sku,
				//		ProductName = item.Product.Name,
				//		ProductQuantity = item.Quantity.ToString(),
				//		ProductUnitAmount = ((int)Math.Round(item.UnitPriceExclTax * 100)).ToString(),
				//		ProductUnitTaxAmount = taxAmount >= 1 ? ((int)Math.Round(taxAmount)).ToString() : string.Empty
				//	});
				//}

            // 4. Now set Seal, SealAlgorithm, Key & KeyVersion for your PaymentRequest
            var secretKey = _worldlinePaymentSettings.SecretKey;

				//var secretKey = "002001000000001_KEY1";

            if (!string.IsNullOrEmpty(_worldlinePaymentSettings.SealAlgorithm))
                paymentRequest.SetSealAndKeyVersion(secretKey, sealAlgorithm: _worldlinePaymentSettings.SealAlgorithm); // Use "002001000000001_KEY1" if you're using the Sandbox uri
            else
                paymentRequest.SetSealAndKeyVersion(secretKey);

            // 5. Send your PaymentRequest to Worldline & receive a RedirectionModel (with redirection Uri and data)
            var redirection = client.SendPaymentRequest(paymentRequest, _worldlinePaymentSettings.UseSandbox);

				

            // 6. If the payment request was successful (RedirectionStatusCode == "00") redirect to Worldline
				if (redirection != null && redirection.RedirectionStatusCode == "00")
				{
					//client.RedirectToWorldline(redirection);
					return Json(new { success = true, message = " ", data = redirection.RedirectionData, Url=redirection.RedirectionUrl,Seal=redirection.Seal }, JsonRequestBehavior.AllowGet);
				}
				// 7. If the payment request was not succesful show an error message
				else if (redirection != null)
				{
					var errorMessage = string.Empty;
					switch (redirection.RedirectionStatusCode)
					{
						case "30": errorMessage = "Request format is not valid."; break;
						case "34": errorMessage = "There is a security problem: for example, the calculated seal is incorrect."; break;
						case "94": errorMessage = "Transaction already exists."; break;
						case "99": errorMessage = "Service temporarily unavailable."; break;
						default: errorMessage = redirection.RedirectionStatusMessage; break;
					}
					_logger.Information("Error in Worldline Payment Provider, Status code: " + redirection.RedirectionStatusCode + ", Message: " + errorMessage);
					return Json(new { success = false, message = "Error in Worldline Payment Provider, Status code: " + redirection.RedirectionStatusCode + ", Message: " + errorMessage }, JsonRequestBehavior.AllowGet);
				}
				else
				{
					_logger.Information("Error in Worldline Payment Provider: Unable to retrieve a redirection model.");
					return Json(new { success = false, message = "Error in Worldline Payment Provider: Unable to retrieve a redirection model." }, JsonRequestBehavior.AllowGet);
				}

			  	return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);

        }

		  public ActionResult PlaceOnConfirmAndroid(int OrderId)
		  {
			  var client = new SipsClient();

			  Order order = _orderService.GetOrderById(OrderId);

			  // 1. Generate a unique identifier for this transaction (so you keep track of the transaction history)
			  var transactionReference = GetTransactionReference(client, order);


			  int storeScope = _storeService.GetAllStores().FirstOrDefault().Id;
			  var _worldlinePaymentSettings = _settingService.LoadSetting<WorldlinePaymentSettings>(storeScope);

			  var merchantId = _worldlinePaymentSettings.Account; // Use "002001000000001" if you're using the Sandbox uri
			  var interfaceVersion = _worldlinePaymentSettings.InterfaceVersion; // "IR_WS_2.14" is the current interface version


			  //var merchantId = "002001000000001"; // Use "002001000000001" if you're using the Sandbox uri
			  //var interfaceVersion = "IR_WS_2.11"; // "IR_WS_2.14" is the current interface version

			  var normalReturnUrl = "http://www.oxcfe.be/api/gotomobilea/" + OrderId;
			  //var normalReturnUrl = "http://www.oxcfe-test.be/api/gotomobile/" + OrderId;


			  var automaticResponseUrl = "http://www.oxcfe.be/api/gotoNotificationa/" + OrderId;
			  //var automaticResponseUrl = "http://www.oxcfe-test.be/api/gotoNotification/" + OrderId;

			  var paymentRequest = client.GetPaymentRequest(merchantId, interfaceVersion, transactionReference, normalReturnUrl, automaticResponseUrl, customerIpAddress: _webHelper.GetCurrentIpAddress());



			  // 3. Set your PaymentRequest data (not Seal, SealAlgorithm, Key & KeyVersion)

			  paymentRequest.SetCustomer(order.Customer.Id.ToString(), _languageService.GetLanguageById(order.CustomerLanguageId).LanguageCulture.Split('-')[0]);
			  //paymentRequest.SetCustomer(order.Customer.Id.ToString(), _languageService.GetAllLanguages().FirstOrDefault().LanguageCulture.Split('-')[0]);


			  var billingContact = new Contact()
			  {
				  Firstname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)),
				  Lastname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName)),
				  Phone = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
				  Email = order.Customer.Email
			  };
			  var billingAddress = new StarringJane.Worldline.Sips.Models.Address()
			  {
				  Street = string.Empty,
				  Company = string.Empty,
				  City = string.Empty,
				  ZipCode = string.Empty,
				  Country = string.Empty,
				  State = string.Empty
			  };
			  paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Customer);
			  paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Billing);


			  //if (order.ShippingAddress != null)
			  //{
			  //	 Custom Code - 09112017 - commented as we will not send address
			  //	 var shippingContact = new Contact()
			  //	 {
			  //		  Firstname = Normalize(order.ShippingAddress.FirstName),
			  //		  Lastname = Normalize(order.ShippingAddress.LastName),
			  //		  Phone = order.ShippingAddress.PhoneNumber,
			  //		  Email = order.ShippingAddress.Email
			  //	 };
			  //	 var shippingAddress = new Address()
			  //	 {
			  //		  Street = Normalize(string.Format("{0} {1}", order.ShippingAddress.Address1, order.ShippingAddress.Address2).Trim()),
			  //		  Company = Normalize(order.ShippingAddress.Company),
			  //		  City = Normalize(order.ShippingAddress.City),
			  //		  ZipCode = Normalize(order.ShippingAddress.ZipPostalCode.Length > 10 ? order.ShippingAddress.ZipPostalCode.Substring(0, 10) : order.ShippingAddress.ZipPostalCode),
			  //		  Country = order.ShippingAddress.Country?.ThreeLetterIsoCode,
			  //		  State = order.ShippingAddress.StateProvince?.Name
			  //	 };
			  //	 paymentRequest.SetAddressAndContactInfo(shippingContact, shippingAddress, AddressType.Delivery);
			  //}

			  // Worldline only accepts value in smallest unit of currency


			  paymentRequest.SetOrderDetails(order.Id.ToString(), (int)Math.Round(order.OrderTotal * 100), order.CustomerCurrencyCode);


			  //foreach (var item in order.OrderItems)
			  //{
			  //	var taxAmount = (item.UnitPriceInclTax - item.UnitPriceExclTax) * 100;
			  //	paymentRequest.ShoppingCartDetail.ShoppingCartItemList.Add(new ShoppingCartItem()
			  //	{
			  //		ProductSKU = item.Product.Sku,
			  //		ProductName = item.Product.Name,
			  //		ProductQuantity = item.Quantity.ToString(),
			  //		ProductUnitAmount = ((int)Math.Round(item.UnitPriceExclTax * 100)).ToString(),
			  //		ProductUnitTaxAmount = taxAmount >= 1 ? ((int)Math.Round(taxAmount)).ToString() : string.Empty
			  //	});
			  //}

			  // 4. Now set Seal, SealAlgorithm, Key & KeyVersion for your PaymentRequest
			  var secretKey = _worldlinePaymentSettings.SecretKey;

			  //var secretKey = "002001000000001_KEY1";

			  if (!string.IsNullOrEmpty(_worldlinePaymentSettings.SealAlgorithm))
				  paymentRequest.SetSealAndKeyVersion(secretKey, sealAlgorithm: _worldlinePaymentSettings.SealAlgorithm); // Use "002001000000001_KEY1" if you're using the Sandbox uri
			  else
				  paymentRequest.SetSealAndKeyVersion(secretKey);

			  // 5. Send your PaymentRequest to Worldline & receive a RedirectionModel (with redirection Uri and data)
			  var redirection = client.SendPaymentRequest(paymentRequest, _worldlinePaymentSettings.UseSandbox);



			  // 6. If the payment request was successful (RedirectionStatusCode == "00") redirect to Worldline
			  if (redirection != null && redirection.RedirectionStatusCode == "00")
			  {
				  //client.RedirectToWorldline(redirection);
				  return Json(new { success = true, message = " ", data = redirection.RedirectionData, Url = redirection.RedirectionUrl, Seal = redirection.Seal }, JsonRequestBehavior.AllowGet);
			  }
			  // 7. If the payment request was not succesful show an error message
			  else if (redirection != null)
			  {
				  var errorMessage = string.Empty;
				  switch (redirection.RedirectionStatusCode)
				  {
					  case "30": errorMessage = "Request format is not valid."; break;
					  case "34": errorMessage = "There is a security problem: for example, the calculated seal is incorrect."; break;
					  case "94": errorMessage = "Transaction already exists."; break;
					  case "99": errorMessage = "Service temporarily unavailable."; break;
					  default: errorMessage = redirection.RedirectionStatusMessage; break;
				  }
				  _logger.Information("Error in Worldline Payment Provider, Status code: " + redirection.RedirectionStatusCode + ", Message: " + errorMessage);
				  return Json(new { success = false, message = "Error in Worldline Payment Provider, Status code: " + redirection.RedirectionStatusCode + ", Message: " + errorMessage }, JsonRequestBehavior.AllowGet);
			  }
			  else
			  {
				  _logger.Information("Error in Worldline Payment Provider: Unable to retrieve a redirection model.");
				  return Json(new { success = false, message = "Error in Worldline Payment Provider: Unable to retrieve a redirection model." }, JsonRequestBehavior.AllowGet);
			  }

			  return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);

		  }



		  public ActionResult PlaceOnConfirmIOS(int OrderId)
		  {
			  var client = new SipsClient();

			  Order order = _orderService.GetOrderById(OrderId);

			  // 1. Generate a unique identifier for this transaction (so you keep track of the transaction history)
			  var transactionReference = GetTransactionReference(client, order);


			  int storeScope = _storeService.GetAllStores().FirstOrDefault().Id;
			  var _worldlinePaymentSettings = _settingService.LoadSetting<WorldlinePaymentSettings>(storeScope);

			  var merchantId = _worldlinePaymentSettings.Account; // Use "002001000000001" if you're using the Sandbox uri
			  var interfaceVersion = _worldlinePaymentSettings.InterfaceVersion; // "IR_WS_2.14" is the current interface version


			  //var merchantId = "002001000000001"; // Use "002001000000001" if you're using the Sandbox uri
			  //var interfaceVersion = "IR_WS_2.11"; // "IR_WS_2.14" is the current interface version

			  var normalReturnUrl = "http://www.oxcfe.be/api/gotomobile/" + OrderId;
			  //var normalReturnUrl = "http://www.oxcfe-test.be/api/gotomobile/" + OrderId;


			  var automaticResponseUrl = "http://www.oxcfe.be/api/gotoNotification/" + OrderId;
			  //var automaticResponseUrl = "http://www.oxcfe-test.be/api/gotoNotification/" + OrderId;

			  var paymentRequest = client.GetPaymentRequest(merchantId, interfaceVersion, transactionReference, normalReturnUrl, automaticResponseUrl, customerIpAddress: _webHelper.GetCurrentIpAddress());

			  paymentRequest.PaymentMeanBrandList.Add("VISA");
			  paymentRequest.PaymentMeanBrandList.Add("VPAY");
			  paymentRequest.PaymentMeanBrandList.Add("MASTERCARD");
			  paymentRequest.PaymentMeanBrandList.Add("MAESTRO");
			  

			  // 3. Set your PaymentRequest data (not Seal, SealAlgorithm, Key & KeyVersion)

			  paymentRequest.SetCustomer(order.Customer.Id.ToString(), _languageService.GetLanguageById(order.CustomerLanguageId).LanguageCulture.Split('-')[0]);
			  //paymentRequest.SetCustomer(order.Customer.Id.ToString(), _languageService.GetAllLanguages().FirstOrDefault().LanguageCulture.Split('-')[0]);


			  var billingContact = new Contact()
			  {
				  Firstname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)),
				  Lastname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName)),
				  Phone = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
				  Email = order.Customer.Email
			  };
			  var billingAddress = new StarringJane.Worldline.Sips.Models.Address()
			  {
				  Street = string.Empty,
				  Company = string.Empty,
				  City = string.Empty,
				  ZipCode = string.Empty,
				  Country = string.Empty,
				  State = string.Empty
			  };
			  paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Customer);
			  paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Billing);


			  //if (order.ShippingAddress != null)
			  //{
			  //	 Custom Code - 09112017 - commented as we will not send address
			  //	 var shippingContact = new Contact()
			  //	 {
			  //		  Firstname = Normalize(order.ShippingAddress.FirstName),
			  //		  Lastname = Normalize(order.ShippingAddress.LastName),
			  //		  Phone = order.ShippingAddress.PhoneNumber,
			  //		  Email = order.ShippingAddress.Email
			  //	 };
			  //	 var shippingAddress = new Address()
			  //	 {
			  //		  Street = Normalize(string.Format("{0} {1}", order.ShippingAddress.Address1, order.ShippingAddress.Address2).Trim()),
			  //		  Company = Normalize(order.ShippingAddress.Company),
			  //		  City = Normalize(order.ShippingAddress.City),
			  //		  ZipCode = Normalize(order.ShippingAddress.ZipPostalCode.Length > 10 ? order.ShippingAddress.ZipPostalCode.Substring(0, 10) : order.ShippingAddress.ZipPostalCode),
			  //		  Country = order.ShippingAddress.Country?.ThreeLetterIsoCode,
			  //		  State = order.ShippingAddress.StateProvince?.Name
			  //	 };
			  //	 paymentRequest.SetAddressAndContactInfo(shippingContact, shippingAddress, AddressType.Delivery);
			  //}

			  // Worldline only accepts value in smallest unit of currency


			  paymentRequest.SetOrderDetails(order.Id.ToString(), (int)Math.Round(order.OrderTotal * 100), order.CustomerCurrencyCode);


			  //foreach (var item in order.OrderItems)
			  //{
			  //	var taxAmount = (item.UnitPriceInclTax - item.UnitPriceExclTax) * 100;
			  //	paymentRequest.ShoppingCartDetail.ShoppingCartItemList.Add(new ShoppingCartItem()
			  //	{
			  //		ProductSKU = item.Product.Sku,
			  //		ProductName = item.Product.Name,
			  //		ProductQuantity = item.Quantity.ToString(),
			  //		ProductUnitAmount = ((int)Math.Round(item.UnitPriceExclTax * 100)).ToString(),
			  //		ProductUnitTaxAmount = taxAmount >= 1 ? ((int)Math.Round(taxAmount)).ToString() : string.Empty
			  //	});
			  //}

			  // 4. Now set Seal, SealAlgorithm, Key & KeyVersion for your PaymentRequest
			  var secretKey = _worldlinePaymentSettings.SecretKey;

			  //var secretKey = "002001000000001_KEY1";

			  if (!string.IsNullOrEmpty(_worldlinePaymentSettings.SealAlgorithm))
				  paymentRequest.SetSealAndKeyVersion(secretKey, sealAlgorithm: _worldlinePaymentSettings.SealAlgorithm); // Use "002001000000001_KEY1" if you're using the Sandbox uri
			  else
				  paymentRequest.SetSealAndKeyVersion(secretKey);

			  // 5. Send your PaymentRequest to Worldline & receive a RedirectionModel (with redirection Uri and data)
			  var redirection = client.SendPaymentRequest(paymentRequest, _worldlinePaymentSettings.UseSandbox);



			  // 6. If the payment request was successful (RedirectionStatusCode == "00") redirect to Worldline
			  if (redirection != null && redirection.RedirectionStatusCode == "00")
			  {
				  //client.RedirectToWorldline(redirection);
				  return Json(new { success = true, message = " ", data = redirection.RedirectionData, Url = redirection.RedirectionUrl, Seal = redirection.Seal }, JsonRequestBehavior.AllowGet);
			  }
			  // 7. If the payment request was not succesful show an error message
			  else if (redirection != null)
			  {
				  var errorMessage = string.Empty;
				  switch (redirection.RedirectionStatusCode)
				  {
					  case "30": errorMessage = "Request format is not valid."; break;
					  case "34": errorMessage = "There is a security problem: for example, the calculated seal is incorrect."; break;
					  case "94": errorMessage = "Transaction already exists."; break;
					  case "99": errorMessage = "Service temporarily unavailable."; break;
					  default: errorMessage = redirection.RedirectionStatusMessage; break;
				  }
				  _logger.Information("Error in Worldline Payment Provider, Status code: " + redirection.RedirectionStatusCode + ", Message: " + errorMessage);
				  return Json(new { success = false, message = "Error in Worldline Payment Provider, Status code: " + redirection.RedirectionStatusCode + ", Message: " + errorMessage }, JsonRequestBehavior.AllowGet);
			  }
			  else
			  {
				  _logger.Information("Error in Worldline Payment Provider: Unable to retrieve a redirection model.");
				  return Json(new { success = false, message = "Error in Worldline Payment Provider: Unable to retrieve a redirection model." }, JsonRequestBehavior.AllowGet);
			  }

			  return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);

		  }

		  public ActionResult PlaceOnBCMC(int OrderId)
		  {
			  var client = new SipsClient();

			  Order order = _orderService.GetOrderById(OrderId);

			  // 1. Generate a unique identifier for this transaction (so you keep track of the transaction history)
			  var transactionReference = GetTransactionReference(client, order);


			  int storeScope = _storeService.GetAllStores().FirstOrDefault().Id;
			  var _worldlinePaymentSettings = _settingService.LoadSetting<WorldlinePaymentSettings>(storeScope);

			  var merchantId = _worldlinePaymentSettings.Account; // Use "002001000000001" if you're using the Sandbox uri
			  var interfaceVersion = _worldlinePaymentSettings.InterfaceVersion; // "IR_WS_2.14" is the current interface version


			  //var merchantId = "002001000000001"; // Use "002001000000001" if you're using the Sandbox uri
			  //var interfaceVersion = "IR_WS_2.11"; // "IR_WS_2.14" is the current interface version

			  var normalReturnUrl = "http://www.oxcfe.be/api/gotomobile/" + OrderId;
			  //var normalReturnUrl = "http://www.oxcfe-test.be/api/gotomobile/" + OrderId;


			  var automaticResponseUrl = "http://www.oxcfe.be/api/gotoNotification/" + OrderId;
			  //var automaticResponseUrl = "http://www.oxcfe-test.be/api/gotoNotification/" + OrderId;

			  var paymentRequest = client.GetPaymentRequest(merchantId, interfaceVersion, transactionReference, normalReturnUrl, automaticResponseUrl, customerIpAddress: _webHelper.GetCurrentIpAddress());

			  paymentRequest.PaymentMeanBrandList.Add("BCMC");	  


			  // 3. Set your PaymentRequest data (not Seal, SealAlgorithm, Key & KeyVersion)

			  paymentRequest.SetCustomer(order.Customer.Id.ToString(), _languageService.GetLanguageById(order.CustomerLanguageId).LanguageCulture.Split('-')[0]);
			  //paymentRequest.SetCustomer(order.Customer.Id.ToString(), _languageService.GetAllLanguages().FirstOrDefault().LanguageCulture.Split('-')[0]);


			  var billingContact = new Contact()
			  {
				  Firstname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)),
				  Lastname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName)),
				  Phone = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
				  Email = order.Customer.Email
			  };
			  var billingAddress = new StarringJane.Worldline.Sips.Models.Address()
			  {
				  Street = string.Empty,
				  Company = string.Empty,
				  City = string.Empty,
				  ZipCode = string.Empty,
				  Country = string.Empty,
				  State = string.Empty
			  };
			  paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Customer);
			  paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Billing);

		 
			  // Worldline only accepts value in smallest unit of currency		

			  paymentRequest.SetOrderDetails(order.Id.ToString(), (int)Math.Round(order.OrderTotal * 100), order.CustomerCurrencyCode);

		  
			  // 4. Now set Seal, SealAlgorithm, Key & KeyVersion for your PaymentRequest
			  var secretKey = _worldlinePaymentSettings.SecretKey;

			  //var secretKey = "002001000000001_KEY1";

			  if (!string.IsNullOrEmpty(_worldlinePaymentSettings.SealAlgorithm))
				  paymentRequest.SetSealAndKeyVersion(secretKey, sealAlgorithm: _worldlinePaymentSettings.SealAlgorithm); // Use "002001000000001_KEY1" if you're using the Sandbox uri
			  else
				  paymentRequest.SetSealAndKeyVersion(secretKey);

			  // 5. Send your PaymentRequest to Worldline & receive a RedirectionModel (with redirection Uri and data)
			  var redirection = client.SendPaymentRequest(paymentRequest, _worldlinePaymentSettings.UseSandbox);



			  // 6. If the payment request was successful (RedirectionStatusCode == "00") redirect to Worldline
			  if (redirection != null && redirection.RedirectionStatusCode == "00")
			  {
				  //client.RedirectToWorldline(redirection);
				  return Json(new { success = true, message = " ", data = redirection.RedirectionData, Url = redirection.RedirectionUrl, Seal = redirection.Seal }, JsonRequestBehavior.AllowGet);
			  }
			  // 7. If the payment request was not succesful show an error message
			  else if (redirection != null)
			  {
				  var errorMessage = string.Empty;
				  switch (redirection.RedirectionStatusCode)
				  {
					  case "30": errorMessage = "Request format is not valid."; break;
					  case "34": errorMessage = "There is a security problem: for example, the calculated seal is incorrect."; break;
					  case "94": errorMessage = "Transaction already exists."; break;
					  case "99": errorMessage = "Service temporarily unavailable."; break;
					  default: errorMessage = redirection.RedirectionStatusMessage; break;
				  }
				  _logger.Information("Error in Worldline Payment Provider, Status code: " + redirection.RedirectionStatusCode + ", Message: " + errorMessage);
				  return Json(new { success = false, message = "Error in Worldline Payment Provider, Status code: " + redirection.RedirectionStatusCode + ", Message: " + errorMessage }, JsonRequestBehavior.AllowGet);
			  }
			  else
			  {
				  _logger.Information("Error in Worldline Payment Provider: Unable to retrieve a redirection model.");
				  return Json(new { success = false, message = "Error in Worldline Payment Provider: Unable to retrieve a redirection model." }, JsonRequestBehavior.AllowGet);
			  }

			  return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);

		  }

		  private string GetTransactionReference(SipsClient client, Order order)
         {
            var prefix = "ox";
            var transactionId = Guid.NewGuid().ToString().Replace("-", "");  
			
			 				 
            // Save transactionId for order (only one per order, so the new transactionId will replace the old one)
				var orderTransactionIDAttribute = _genericAttributeService.GetAttributesForEntity(order.Id, "Order").FirstOrDefault(x => x.Key == "TransactionID");
				//var orderTransactionIDAttribute = _genericAttributeService.GetAttributesForEntity(customerId, "Customer").FirstOrDefault(x => x.Key == "TransactionID" && x.Key == "PaymentOrderReferences");
					  
			   if (orderTransactionIDAttribute != null)
				{
					orderTransactionIDAttribute.Value = transactionId;
					_genericAttributeService.UpdateAttribute(orderTransactionIDAttribute);
				}
				else
				{
					_genericAttributeService.SaveAttribute<string>(order, "TransactionID", transactionId);
					//_genericAttributeService.SaveAttribute<string>(customer, "TransactionID", transactionId);
				  
				}

            var transactionReference = string.Format("{0}{1}", prefix, transactionId);
            return transactionReference.Substring(0, Math.Min(35, transactionReference.Length));

			//	return Json(new { success = true, message = "", data =  transactionReference.Substring(0, Math.Min(35, transactionReference.Length)) }, JsonRequestBehavior.AllowGet);
        }


		  public string Normalize(string s)
		  {
			  var normalizeDictionary = new Dictionary<string, string>();
			  if (!normalizeDictionary.ContainsKey("Š")) normalizeDictionary.Add("Š", "S");
			  if (!normalizeDictionary.ContainsKey("š")) normalizeDictionary.Add("š", "s");
			  if (!normalizeDictionary.ContainsKey("Ð")) normalizeDictionary.Add("Ð", "Dj");
			  if (!normalizeDictionary.ContainsKey("ð")) normalizeDictionary.Add("ð", "dj");
			  if (!normalizeDictionary.ContainsKey("Ž")) normalizeDictionary.Add("Ž", "Z");
			  if (!normalizeDictionary.ContainsKey("ž")) normalizeDictionary.Add("ž", "z");
			  if (!normalizeDictionary.ContainsKey("Č")) normalizeDictionary.Add("Č", "C");
			  if (!normalizeDictionary.ContainsKey("č")) normalizeDictionary.Add("č", "c");
			  if (!normalizeDictionary.ContainsKey("Ć")) normalizeDictionary.Add("Ć", "C");
			  if (!normalizeDictionary.ContainsKey("ć")) normalizeDictionary.Add("ć", "c");
			  if (!normalizeDictionary.ContainsKey("À")) normalizeDictionary.Add("À", "A");
			  if (!normalizeDictionary.ContainsKey("Á")) normalizeDictionary.Add("Á", "A");
			  if (!normalizeDictionary.ContainsKey("Â")) normalizeDictionary.Add("Â", "A");
			  if (!normalizeDictionary.ContainsKey("Ã")) normalizeDictionary.Add("Ã", "A");
			  if (!normalizeDictionary.ContainsKey("Ä")) normalizeDictionary.Add("Ä", "A");
			  if (!normalizeDictionary.ContainsKey("Å")) normalizeDictionary.Add("Å", "A");
			  if (!normalizeDictionary.ContainsKey("Æ")) normalizeDictionary.Add("Æ", "A");
			  if (!normalizeDictionary.ContainsKey("Ç")) normalizeDictionary.Add("Ç", "C");
			  if (!normalizeDictionary.ContainsKey("È")) normalizeDictionary.Add("È", "E");
			  if (!normalizeDictionary.ContainsKey("É")) normalizeDictionary.Add("É", "E");
			  if (!normalizeDictionary.ContainsKey("Ê")) normalizeDictionary.Add("Ê", "E");
			  if (!normalizeDictionary.ContainsKey("Ë")) normalizeDictionary.Add("Ë", "E");
			  if (!normalizeDictionary.ContainsKey("Ì")) normalizeDictionary.Add("Ì", "I");
			  if (!normalizeDictionary.ContainsKey("Í")) normalizeDictionary.Add("Í", "I");
			  if (!normalizeDictionary.ContainsKey("Î")) normalizeDictionary.Add("Î", "I");
			  if (!normalizeDictionary.ContainsKey("Ï")) normalizeDictionary.Add("Ï", "I");
			  if (!normalizeDictionary.ContainsKey("Ñ")) normalizeDictionary.Add("Ñ", "N");
			  if (!normalizeDictionary.ContainsKey("Ò")) normalizeDictionary.Add("Ò", "O");
			  if (!normalizeDictionary.ContainsKey("Ó")) normalizeDictionary.Add("Ó", "O");
			  if (!normalizeDictionary.ContainsKey("Ô")) normalizeDictionary.Add("Ô", "O");
			  if (!normalizeDictionary.ContainsKey("Õ")) normalizeDictionary.Add("Õ", "O");
			  if (!normalizeDictionary.ContainsKey("Ö")) normalizeDictionary.Add("Ö", "O");
			  if (!normalizeDictionary.ContainsKey("Ø")) normalizeDictionary.Add("Ø", "O");
			  if (!normalizeDictionary.ContainsKey("Ù")) normalizeDictionary.Add("Ù", "U");
			  if (!normalizeDictionary.ContainsKey("Ú")) normalizeDictionary.Add("Ú", "U");
			  if (!normalizeDictionary.ContainsKey("Û")) normalizeDictionary.Add("Û", "U");
			  if (!normalizeDictionary.ContainsKey("Ü")) normalizeDictionary.Add("Ü", "U");
			  if (!normalizeDictionary.ContainsKey("Ý")) normalizeDictionary.Add("Ý", "Y");
			  if (!normalizeDictionary.ContainsKey("Þ")) normalizeDictionary.Add("Þ", "B");
			  if (!normalizeDictionary.ContainsKey("ß")) normalizeDictionary.Add("ß", "Ss");
			  if (!normalizeDictionary.ContainsKey("à")) normalizeDictionary.Add("à", "a");
			  if (!normalizeDictionary.ContainsKey("á")) normalizeDictionary.Add("á", "a");
			  if (!normalizeDictionary.ContainsKey("â")) normalizeDictionary.Add("â", "a");
			  if (!normalizeDictionary.ContainsKey("ã")) normalizeDictionary.Add("ã", "a");
			  if (!normalizeDictionary.ContainsKey("ä")) normalizeDictionary.Add("ä", "a");
			  if (!normalizeDictionary.ContainsKey("å")) normalizeDictionary.Add("å", "a");
			  if (!normalizeDictionary.ContainsKey("æ")) normalizeDictionary.Add("æ", "a");
			  if (!normalizeDictionary.ContainsKey("ç")) normalizeDictionary.Add("ç", "c");
			  if (!normalizeDictionary.ContainsKey("è")) normalizeDictionary.Add("è", "e");
			  if (!normalizeDictionary.ContainsKey("é")) normalizeDictionary.Add("é", "e");
			  if (!normalizeDictionary.ContainsKey("ê")) normalizeDictionary.Add("ê", "e");
			  if (!normalizeDictionary.ContainsKey("ë")) normalizeDictionary.Add("ë", "e");
			  if (!normalizeDictionary.ContainsKey("ì")) normalizeDictionary.Add("ì", "i");
			  if (!normalizeDictionary.ContainsKey("í")) normalizeDictionary.Add("í", "i");
			  if (!normalizeDictionary.ContainsKey("î")) normalizeDictionary.Add("î", "i");
			  if (!normalizeDictionary.ContainsKey("ï")) normalizeDictionary.Add("ï", "i");
			  if (!normalizeDictionary.ContainsKey("ñ")) normalizeDictionary.Add("ñ", "n");
			  if (!normalizeDictionary.ContainsKey("ò")) normalizeDictionary.Add("ò", "o");
			  if (!normalizeDictionary.ContainsKey("ó")) normalizeDictionary.Add("ó", "o");
			  if (!normalizeDictionary.ContainsKey("ô")) normalizeDictionary.Add("ô", "o");
			  if (!normalizeDictionary.ContainsKey("õ")) normalizeDictionary.Add("õ", "o");
			  if (!normalizeDictionary.ContainsKey("ö")) normalizeDictionary.Add("ö", "o");
			  if (!normalizeDictionary.ContainsKey("ø")) normalizeDictionary.Add("ø", "o");
			  if (!normalizeDictionary.ContainsKey("ù")) normalizeDictionary.Add("ù", "u");
			  if (!normalizeDictionary.ContainsKey("ú")) normalizeDictionary.Add("ú", "u");
			  if (!normalizeDictionary.ContainsKey("û")) normalizeDictionary.Add("û", "u");
			  if (!normalizeDictionary.ContainsKey("ý")) normalizeDictionary.Add("ý", "y");
			  if (!normalizeDictionary.ContainsKey("þ")) normalizeDictionary.Add("þ", "b");
			  if (!normalizeDictionary.ContainsKey("ÿ")) normalizeDictionary.Add("ÿ", "y");
			  if (!normalizeDictionary.ContainsKey("Ŕ")) normalizeDictionary.Add("Ŕ", "R");
			  if (!normalizeDictionary.ContainsKey("ŕ")) normalizeDictionary.Add("ŕ", "r");
			  if (!normalizeDictionary.ContainsKey("`")) normalizeDictionary.Add("`", "'");
			  if (!normalizeDictionary.ContainsKey("´")) normalizeDictionary.Add("´", "'");
			  if (!normalizeDictionary.ContainsKey("„")) normalizeDictionary.Add("„", ",");
			  if (!normalizeDictionary.ContainsKey("“")) normalizeDictionary.Add("“", "\\");
			  if (!normalizeDictionary.ContainsKey("”")) normalizeDictionary.Add("”", "\\");
			  if (!normalizeDictionary.ContainsKey("&acirc;€™")) normalizeDictionary.Add("&acirc;€™", "'");
			  if (!normalizeDictionary.ContainsKey("{")) normalizeDictionary.Add("{", "");
			  if (!normalizeDictionary.ContainsKey("~")) normalizeDictionary.Add("~", "");
			  if (!normalizeDictionary.ContainsKey("–")) normalizeDictionary.Add("–", "-");
			  if (!normalizeDictionary.ContainsKey("’")) normalizeDictionary.Add("’", "'");
			  if (!normalizeDictionary.ContainsKey(">")) normalizeDictionary.Add(">", " ");
			  if (!normalizeDictionary.ContainsKey("<")) normalizeDictionary.Add("<", " ");
			  if (!normalizeDictionary.ContainsKey(";")) normalizeDictionary.Add(";", " ");
			  if (!normalizeDictionary.ContainsKey("^")) normalizeDictionary.Add("^", " ");
			  if (!normalizeDictionary.ContainsKey("µ")) normalizeDictionary.Add("µ", "u");
			  if (!normalizeDictionary.ContainsKey("/")) normalizeDictionary.Add("/", " ");
			  if (!normalizeDictionary.ContainsKey("\\")) normalizeDictionary.Add("\\", " ");
			  if (!normalizeDictionary.ContainsKey("ü")) normalizeDictionary.Add("ü", "u");
			  if (!normalizeDictionary.ContainsKey("(")) normalizeDictionary.Add("(", "");
			  if (!normalizeDictionary.ContainsKey(")")) normalizeDictionary.Add(")", "");

			  foreach (var kvp in normalizeDictionary.Where(x => s.Contains(x.Key))) s = s.Replace(kvp.Key, kvp.Value);
			  return s;
		  }

 	  	
		  public ActionResult GenerateSeal(int OrderId)
        {
			  try
			  {
				  var client = new SipsClient();

				  var Order = _orderService.GetOrderById(OrderId);

				  var transactionReference = GetTransactionReference(client, Order);
				  //+ (int)Math.Round(Order.OrderTotal * 100) +

				  //string Url= "http://www.oxcfe.be/api/gotoNotificationa/" + OrderId;
				  string rdata = "";
				  string Url = "http://www.oxcfe.be/api/gotoSips/" + OrderId;

				  //string Url = "http://www.oxcfe.be";


				  string token = string.Empty;
				  string url = @"https://office-server.sips-atos.com/rs-services/v2/checkout/paymentProviderInitialize";
				  string postData = "{\"amount\":\"" + (int)Math.Round(Order.OrderTotal * 100) + "\", \"captureMode\":\"AUTHOR_CAPTURE\", \"currencyCode\":\"978\", \"interfaceVersion\":\"IR_WS_2.11\", \"keyVersion\":\"1\", \"merchantId\":\"225005036700001\", ";
				  postData = postData + "\"merchantReturnUrl\":\"" + Url + "\", \"orderChannel\":\"INTERNET\", \"paymentMeanBrand\":\"BCMCMOBILE\", \"responseKeyVersion\":\"1\",";
				  string sealData = @"" + (int)Math.Round(Order.OrderTotal * 100) + "AUTHOR_CAPTURE978IR_WS_2.11225005036700001" + Url + "INTERNETBCMCMOBILE1" + transactionReference;
				  var seal = ReadSeal(sealData, "PCv4DO5zG-pBOIfjojGnslk1Q0jG7Wd5iVHHLhk2XXk");
				  postData = postData + "\"transactionReference\":\"" + transactionReference + "\", \"seal\":\"" + seal + "\"}";

				  var request = (HttpWebRequest)WebRequest.Create(url);
				  request.ContentType = "application/json";
				  request.Method = "POST";
				  request.ContentLength = postData.Length;
				  request.AutomaticDecompression = DecompressionMethods.GZip;

				  var requestWriter = new StreamWriter(request.GetRequestStream());
				  requestWriter.Write(postData);
				  requestWriter.Close();

				  var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				  var responseData = responseReader.ReadToEnd();


				  //JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
				  //var result = jsSerializer.DeserializeObject(responseData);
				  //Dictionary<string, object> obj2 = new Dictionary<string, object>();
				  //obj2 = (Dictionary<string, object>)(result);

				  //string redirectData = obj2["redirectionData"].ToString();


				  //string urlt = @"https://office-server.sips-atos.com/rs-services/v2/checkout/paymentProviderFinalize";
				  //string postDatat = "{\"interfaceVersion\":\"IR_WS_2.11\", \"keyVersion\":\"1\", \"merchantId\":\"225005036700001\",";
				  //postDatat = postDatat + "\"messageVersion\":\"0.1\", \"redirectionData\":\"" + redirectData + "\",";
				  //string vsealData = @"IR_WS_2.112250050367000010.1" + redirectData + transactionReference;
				  //var vseal = ReadSeal(vsealData, "PCv4DO5zG-pBOIfjojGnslk1Q0jG7Wd5iVHHLhk2XXk");

				  //postDatat = postDatat + "\"transactionReference\":\"" + transactionReference + "\", \"seal\":\"" + vseal + "\"}";

				  //var requestt = (HttpWebRequest)WebRequest.Create(urlt);
				  //requestt.ContentType = "application/json";
				  //requestt.Method = "POST";
				  //requestt.ContentLength = postDatat.Length;
				  //requestt.AutomaticDecompression = DecompressionMethods.GZip;

				  //var requestWritert = new StreamWriter(requestt.GetRequestStream());
				  //requestWritert.Write(postDatat);
				  //requestWritert.Close();

				  //var responseReadert = new StreamReader(requestt.GetResponse().GetResponseStream());
				  //var responseDatat = responseReadert.ReadToEnd();

				  return Json(new { success = true, message = " ", data = responseData }, JsonRequestBehavior.AllowGet);
			  }
       	  catch(Exception ex)
			  {
				  _logger.InsertLog(LogLevel.Error, "Bancontact Direct mode Initialize request  :: " + OrderId, ex.Message + "  ||   " + ex.StackTrace);
				  ViewBag.Message = ex.Message;
				  return View("~/Plugins/WebApi/Views/MobileException.cshtml");
			  }
        }
		
		
		private string ByteArrayToHEX(byte[] ba)
	    {
	        StringBuilder hex = new StringBuilder(ba.Length * 2);
	        foreach (byte b in ba)
	            hex.AppendFormat("{0:x2}", b);
	        return hex.ToString();
	    }			 
	
	
        public string ReadSeal(string data,string key)
        {
            string sealAlgorithm = "HMAC-SHA-256";
            var rr = "";            
            UTF8Encoding utF8Encoding = new UTF8Encoding();
            byte[] bytes = utF8Encoding.GetBytes(data);
            HMAC hmac = sealAlgorithm == "HMAC-SHA-1" ? (HMAC)new HMACSHA1() : (sealAlgorithm == "HMAC-SHA-384" ? (HMAC)new HMACSHA384() : (sealAlgorithm == "HMAC-SHA-512" ? (HMAC)new HMACSHA512() : (HMAC)new HMACSHA256()));
            hmac.Key = utF8Encoding.GetBytes(key);
            hmac.Initialize();
				return this.ByteArrayToHEX(hmac.ComputeHash(bytes));           
        }






		  public ActionResult PlaceOnPayzen(int OrderId)
		  {
			  try
			  {
				  var model = new PaymentInfoModel();
				  Order order = _orderService.GetOrderById(OrderId);
				  if (order == null)
					  return Json(new { success = false, message = "Order Does not exists!" }, JsonRequestBehavior.AllowGet);

				  int storeScope = _storeService.GetAllStores().FirstOrDefault().Id;
				  var payzenPaymentSettings = _settingService.LoadSetting<PayzenPaymentSettings>(storeScope);

				  model.vads_site_id = payzenPaymentSettings.ShopId; //"19677451";// "15063320";
				  model.vads_amount = string.Format("{0:0.##}", (order.OrderTotal * 100));
				  model.vads_trans_date = order.CreatedOnUtc.ToString("yyyyMMddHHmmss");
				  model.vads_trans_id = order.Id.ToString("000000");

				  var certificate = string.Empty;
				  var ctxMode = string.Empty;

				  if (payzenPaymentSettings.UseSandbox)
				  {
					  certificate = payzenPaymentSettings.TestCertificate;
					  ctxMode = "TEST";
				  }
				  else
				  {
					  certificate = payzenPaymentSettings.ProductionCertificate;
					  ctxMode = "PRODUCTION";
				  }

				  model.vads_ctx_mode = ctxMode;

				  var signString = "INTERACTIVE+" + model.vads_amount + "+" + ctxMode + "+978+PAYMENT+SINGLE+" + model.vads_site_id + "+" + model.vads_trans_date + "+" + model.vads_trans_id + "+V2+" + certificate;

				  var signature = PayzenGenerateSeal(signString, certificate);

				  model.signature = signature;
				  return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
			  }
			  catch (Exception ex)
			  {
				  return Json(new { success = true, message = ex.Message.ToString()}, JsonRequestBehavior.AllowGet);
			  }

		  }

		  public string PayzenGenerateSeal(string data, string key)
		  {
			  string sealAlgorithm = "HMAC-SHA-256";
			  var rr = "";
			  UTF8Encoding utF8Encoding = new UTF8Encoding();
			  byte[] bytes = utF8Encoding.GetBytes(data);
			  HMAC hmac = (HMAC)new HMACSHA256();
			  hmac.Key = utF8Encoding.GetBytes(key);
			  hmac.Initialize();
			  return Convert.ToBase64String(hmac.ComputeHash(bytes));
		  }
        
        #endregion


		

	}
}
