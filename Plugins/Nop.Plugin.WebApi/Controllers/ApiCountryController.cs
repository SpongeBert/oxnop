﻿using System.Web.Mvc;
using Nop.Plugin.WebApi.Factories;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using System.Linq;
using Nop.Plugin.WebApi.Models.Common;

namespace Nop.Plugin.WebApi.Controllers
{
	public partial class ApiCountryController : BasePluginController
	{
		#region Fields

        private readonly ICountryModelFactory _countryModelFactory;
		  private readonly ICountryService _countryService;
		  private readonly ILanguageService _languageService;
		  private readonly ILocalizationService _localizationService;

	    #endregion

		#region Constructors

		public ApiCountryController(ICountryModelFactory countryModelFactory,ICountryService countryService, ILanguageService languageService, ILocalizationService localizationService)
		{
            this._countryModelFactory = countryModelFactory;
				this._countryService = countryService;
				this._languageService = languageService;
				this._localizationService = localizationService;
		}

        #endregion

        #region States / provinces
				       
        public virtual ActionResult GetStatesByCountryId(string countryId, bool addSelectStateItem=false)
        {
            var model = _countryModelFactory.GetStatesByCountryId(countryId, addSelectStateItem);
           // return Json(model, JsonRequestBehavior.AllowGet);
				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
        }


		  public virtual ActionResult GetAllCountries()
		  {
			  int languageId = _languageService.GetAllLanguages().FirstOrDefault().Id;

			  var countryList = _countryService.GetAllCountries(languageId);

			  CountryModel model = new CountryModel();

			  //countries
			  model.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
			  foreach (var c in _countryService.GetAllCountries(showHidden: true))
				  model.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == model.CountryId) });


			  return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		  }


        #endregion
    }
}
