﻿using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.WebApi.Factories;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.WebApi.Controllers
{
	public partial class ApiCatalogController	:BasePluginController
	{
		 #region Fields

		  private readonly IApiCatalogModelFactory _catalogModelFactory;
		  //private readonly IProductModelFactory _productModelFactory;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IProductService _productService;
        private readonly IVendorService _vendorService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly IProductTagService _productTagService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IPermissionService _permissionService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly MediaSettings _mediaSettings;
        private readonly CatalogSettings _catalogSettings;
        private readonly VendorSettings _vendorSettings;

        #endregion

        #region Constructors

		  //public CatalogController(ICatalogModelFactory catalogModelFactory,
		  //	 IProductModelFactory productModelFactory,
		  public ApiCatalogController(
			  IApiCatalogModelFactory catalogModelFactory,
            ICategoryService categoryService, 
            IManufacturerService manufacturerService,
            IProductService productService, 
            IVendorService vendorService,
            IWorkContext workContext, 
            IStoreContext storeContext,
            ILocalizationService localizationService,
            IWebHelper webHelper,
            IProductTagService productTagService,
            IGenericAttributeService genericAttributeService,
            IAclService aclService,
            IStoreMappingService storeMappingService,
            IPermissionService permissionService, 
            ICustomerActivityService customerActivityService,
            MediaSettings mediaSettings,
            CatalogSettings catalogSettings,
            VendorSettings vendorSettings)
        {
				_catalogModelFactory = catalogModelFactory;
				//_productModelFactory = productModelFactory;
            _categoryService = categoryService;
            _manufacturerService = manufacturerService;
            _productService = productService;
            _vendorService = vendorService;
            _workContext = workContext;
            _storeContext = storeContext;
            _localizationService = localizationService;
            _webHelper = webHelper;
            _productTagService = productTagService;
            _genericAttributeService = genericAttributeService;
            _aclService = aclService;
            _storeMappingService = storeMappingService;
            _permissionService = permissionService;
            _customerActivityService = customerActivityService;
            _mediaSettings = mediaSettings;
            _catalogSettings = catalogSettings;
            _vendorSettings = vendorSettings;
        }

        #endregion

		#region TopMenu
		  
		  public object TopMenu()
		  {
			  var model = _catalogModelFactory.PrepareTopMenuModel();
			  return Json(new{success=true,message="",data=model},JsonRequestBehavior.AllowGet);
		  }
		  
		#endregion

	}
	
}
