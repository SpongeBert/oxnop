﻿using System.Globalization;
using Nop.Services.Customers;
using System;
using System.Web.Mvc;
using Nop.Web.Framework.Controllers;
using Nop.Plugin.WebApi.Models.Customer;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Core.Domain.Common;
using System.Web.Script.Serialization;
using Nop.Plugin.WebApi.Models.Addresses;
using Nop.Services.Directory;
using Nop.Services.Localization;
using System.Linq;
using Nop.Web.Framework.Security;
using Nop.Plugin.WebApi.Models.Media;
using Nop.Services.Media;
using Nop.Core.Domain.Media;
using Nop.Plugin.PostXCustom.Services;
using Nop.Plugin.PostXCustom.Models.Public.Customers;
using Nop.Plugin.PostXCustom.Factories.Public;
using Nop.Plugin.PostXCustom.Models.Public.Media;
using OnBarcode.Barcode;
using OnBarcode.Barcode.ASPNET;	
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Nop.Services.Logging;

namespace Nop.Plugin.WebApi.Controllers
{
	public class ApiCustomerController : BasePluginController
	{
		#region fields
		private readonly ICustomerService _customerService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly CustomerSettings _customerSettings;
		private readonly ICustomerRegistrationService _customerRegistrationService;
		private readonly IAddressService _addressService;
		private readonly ICountryService _countryService;
		private readonly IStateProvinceService _stateProvinceService;
		private readonly ILocalizationService _localizationService;

		private readonly IPictureService _pictureService;
		private readonly MediaSettings _mediaSettings;
		private readonly ICompanyRepresentativeCustomerMappingService _companyRepresentativeCustomerMappingService;
		private readonly ICustomerModelFactory _customerModelFactory;
		private readonly ILogger _logger;
		private readonly Nop.Plugin.WebApi.Factories.IOrderModelFactory _orderModelFactory;
		private readonly ILanguageService _languageService;
		private readonly AddressSettings _addressSettings;
		private readonly IAddressModelFactory _addressModelFactory;
		#endregion

		#region const

		public ApiCustomerController(ICustomerService customerService, IGenericAttributeService genericAttributeService, CustomerSettings customerSettings,
			ICustomerRegistrationService customerRegistrationService, IAddressService addressService, ICountryService countryService,
		IStateProvinceService stateProvinceService, ILocalizationService localizationService, IPictureService pictureService,MediaSettings mediaSettings,
			ICompanyRepresentativeCustomerMappingService companyRepresentativeCustomerMappingService, ICustomerModelFactory customerModelFactory, ILogger logger, Nop.Plugin.WebApi.Factories.IOrderModelFactory orderModelFactory,
			ILanguageService languageService, AddressSettings addressSettings, IAddressModelFactory addressModelFactory)
		{
			_customerService = customerService;
			_genericAttributeService = genericAttributeService;
			_customerSettings = customerSettings;
			_customerRegistrationService = customerRegistrationService;
			_addressService = addressService;
			_countryService = countryService;
			_stateProvinceService = stateProvinceService;
			_localizationService = localizationService;
			_pictureService = pictureService;
			_mediaSettings = mediaSettings;
			_companyRepresentativeCustomerMappingService = companyRepresentativeCustomerMappingService;
			_customerModelFactory = customerModelFactory;
		   _logger = logger;
			_orderModelFactory = orderModelFactory;
			_languageService = languageService;
			_addressSettings = addressSettings;
			_addressModelFactory = addressModelFactory;
		}

		#endregion

		#region CustomerInfo	  

		public ActionResult CustomerInfo(int UserId)
		{
		
			var currentCustomer = _customerService.GetCustomerById(UserId);
			if (currentCustomer == null)
			{
				return Json(new { success = false, message = "Customer does not exists" }, JsonRequestBehavior.AllowGet);
			}
			var model = new CustomerInfoModel
			{
				Id = UserId,
				FirstName = currentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
				LastName = currentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
				Gender = currentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Gender),
				DOB = currentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.DateOfBirth),
				Username = currentCustomer.Username,
				Email = currentCustomer.Email,
				CountryId = currentCustomer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId),
				StateProvinceId = currentCustomer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId)
			};


			//countries and states
			if (_customerSettings.CountryEnabled)
			{
				model.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
				foreach (var c in _countryService.GetAllCountries(showHidden: true))
				{
					model.AvailableCountries.Add(new SelectListItem
					{
						Text = c.Name,
						Value = c.Id.ToString(CultureInfo.InvariantCulture),
						Selected = c.Id == model.CountryId
					});
				}

				if (_customerSettings.StateProvinceEnabled)
				{
					//states
					var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
					if (states.Any())
					{
						model.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectState"), Value = "0" });

						foreach (var s in states)
						{
							model.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(CultureInfo.InvariantCulture), Selected = (s.Id == model.StateProvinceId) });
						}
					}
					else
					{
						var anyCountrySelected = model.AvailableCountries.Any(x => x.Selected);

						model.AvailableStates.Add(new SelectListItem
						{
							Text = _localizationService.GetResource(anyCountrySelected ? "Admin.Address.OtherNonUS" : "Admin.Address.SelectState"),
							Value = "0"
						});
					}
				}
			}

			//existing addresses
			var addresses = currentCustomer.Addresses;
			if (addresses.Count > 0)
			{

				foreach (var address in addresses)
				{
					var editAddress = new EditAddressModel
					{
						Id = address.Id,
						FirstName = address.FirstName,
						LastName = address.LastName,
						PhoneNumber = address.PhoneNumber,
						Address1 = address.Address1,
						Address2 = address.Address2,
						City = address.City,
						ZipPostalCode = address.ZipPostalCode,
						FaxNumber = address.FaxNumber,
						Company = address.Company,
						Country = address.Country.Name,
						Email = address.Email,
						StateProvinceId = address.StateProvinceId
					};
					model.ExistingAddresses.Add(editAddress);
				}
			}


			#region Company Image
			var attributeList = _genericAttributeService.GetAttributesForEntity(UserId, "Customer");
			if (attributeList.Any())
			{
				var pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
				if (pictureAttribute != null)
				{
					model.PictureId = Convert.ToInt32(pictureAttribute.Value);

					//var pictureSize = _mediaSettings.VendorThumbPictureSize;
					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.PictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PictureModel = pictureModel;
				}
				else
				{
					var companyRepresentative =
						_companyRepresentativeCustomerMappingService.GetByCustomerId(currentCustomer.Id);
					if (companyRepresentative != null)
					{
						var customer = _customerService.GetCustomerById(companyRepresentative.CompanyRepresentativeId);
						var companyAttributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
						if (companyAttributeList.Any())
						{
							pictureAttribute = companyAttributeList.FirstOrDefault(x => x.Key == "PictureId");
							if (pictureAttribute != null)
							{
								model.PictureId = Convert.ToInt32(pictureAttribute.Value);

								//var pictureSize = _mediaSettings.VendorThumbPictureSize;
								var pictureSize = _mediaSettings.AvatarPictureSize;
								var picture = _pictureService.GetPictureById(model.PictureId);
								var pictureModel = new PublicPictureModel
								{
									FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
									ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
									Title = model.FirstName,
									AlternateText = model.FirstName
								};
								model.PictureModel = pictureModel;
							}
						}
					}
				}
			}
			 var customrPictureAttribute = attributeList.FirstOrDefault(x => x.Key == "CustomerPictureId");
				if (customrPictureAttribute != null)
				{
					model.CustomerPictureId = Convert.ToInt32(customrPictureAttribute.Value);

					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.CustomerPictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PersonalPictureModel = pictureModel;
				}	

			
			var personalNumberAttribute = attributeList.FirstOrDefault(x => x.Key == "PersonalNumber");

			//status.PersonalNumber = (personalNumberAttribute != null) ? Convert.ToInt32(personalNumberAttribute.Value) : 0;
			//model.PersonalNumber = (personalNumberAttribute != null) ? personalNumberAttribute.Value : currentCustomer.CompanyId.ToString("D3") + currentCustomer.PersonalId.ToString("D5");
		  model.PersonalNumber = (personalNumberAttribute != null && personalNumberAttribute.Value != "0") ? personalNumberAttribute.Value : currentCustomer.CompanyId.ToString("D3") + currentCustomer.PersonalId.ToString("D5");

			model.CompanyId = currentCustomer.CompanyId;

	 	#endregion

				return Json(new { success = true, message = "",data = model }, JsonRequestBehavior.AllowGet);
			
		}


		

		public ActionResult EditCustomerInfo(string json)
		{
			var model = new JavaScriptSerializer().Deserialize<CustomerInfoModel>(json);

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
			{
				return Json(new { success = false, message = "Customer does not exists" }, JsonRequestBehavior.AllowGet);
			}

			if (_customerSettings.UsernamesEnabled && _customerSettings.AllowUsersToChangeUsernames)
			{
				_customerRegistrationService.SetUsername(customer, model.Username.Trim());
			}
			var requireValidation = _customerSettings.UserRegistrationType == UserRegistrationType.EmailValidation;
			_customerRegistrationService.SetEmail(customer, model.Email.Trim(), requireValidation);

			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DOB);

			return Json(new { success = true, message = "Info Updated Successfully" }, JsonRequestBehavior.AllowGet);

		}


		public  ActionResult UserInfo(int userId)
		{			
			var model = new PublicCustomerInfoModel();
			var CurrentCustomer = _customerService.GetCustomerById(userId);

			model = _customerModelFactory.PrepareCustomerInfoModel(model, CurrentCustomer, false);

			var attributeList = _genericAttributeService.GetAttributesForEntity(userId, "Customer");
			if (attributeList.Any())
			{
				var pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
				if (pictureAttribute != null)
				{
					model.PictureId = Convert.ToInt32(pictureAttribute.Value);

					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.PictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PictureModel = pictureModel;
				}
				else
				{
					var companyRepresentative =
						_companyRepresentativeCustomerMappingService.GetByCustomerId(userId);
					if (companyRepresentative != null)
					{
						var customer = _customerService.GetCustomerById(companyRepresentative.CompanyRepresentativeId);
						var companyAttributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
						if (companyAttributeList.Any())
						{
							pictureAttribute = companyAttributeList.FirstOrDefault(x => x.Key == "PictureId");
							if (pictureAttribute != null)
							{
								model.PictureId = Convert.ToInt32(pictureAttribute.Value);

								var pictureSize = _mediaSettings.AvatarPictureSize;
								var picture = _pictureService.GetPictureById(model.PictureId);
								var pictureModel = new PublicPictureModel
								{
									FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
									ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
									Title = model.FirstName,
									AlternateText = model.FirstName
								};
								model.PictureModel = pictureModel;
							}
						}
					}
				}


				var customrPictureAttribute = attributeList.FirstOrDefault(x => x.Key == "CustomerPictureId");
				if (customrPictureAttribute != null)
				{
					model.CustomerPictureId = Convert.ToInt32(customrPictureAttribute.Value);

					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.CustomerPictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PersonalPictureModel = pictureModel;
				}
						

			}

			var personalNumberAttribute = attributeList.FirstOrDefault(x => x.Key == "PersonalNumber");

			//status.PersonalNumber = (personalNumberAttribute != null) ? Convert.ToInt32(personalNumberAttribute.Value) : 0;
			//model.PersonalNumber = (personalNumberAttribute != null) ? personalNumberAttribute.Value : CurrentCustomer.CompanyId.ToString("D3") + CurrentCustomer.PersonalId.ToString("D5");

			model.PersonalNumber = (personalNumberAttribute != null && personalNumberAttribute.Value != "0") ? personalNumberAttribute.Value : CurrentCustomer.CompanyId.ToString("D3") + CurrentCustomer.PersonalId.ToString("D5");
		 	model.CompanyId = CurrentCustomer.CompanyId;
		 
			return Json(new { success = true, message = "",data = model }, JsonRequestBehavior.AllowGet);
			//return View("~/Plugins/PostXCustom/Views/PublicCustomer/Info.cshtml", model);
		}



			
		#endregion


		#region My account / Addresses

		[HttpPost]
		[PublicAntiForgery]
		[NopHttpsRequirement(SslRequirement.Yes)]
		public virtual ActionResult AddressDelete(string json)
		{
			var model = new JavaScriptSerializer().Deserialize<DeleteAddressModel>(json);
			if (!string.IsNullOrEmpty(model.CustomerId.ToString(CultureInfo.InvariantCulture)) && model.CustomerId == 0)
				return Json(new { success = false, message = "No Customer found." }, JsonRequestBehavior.AllowGet);

			var customer = _customerService.GetCustomerById(model.CustomerId);
			if (customer == null)
				return Json(new { success = false, message = "Customer not found." }, JsonRequestBehavior.AllowGet);


			//find address (ensure that it belongs to the current customer)
			var address = customer.Addresses.FirstOrDefault(a => a.Id == model.AddressId);

			if (address == null)
				return Json(new { success = false, message = "Address not found" }, JsonRequestBehavior.AllowGet);

			customer.RemoveAddress(address);
			_customerService.UpdateCustomer(customer);
			//now delete the address record
			_addressService.DeleteAddress(address);
			return Json(new { success = true, message = "Address Deleted Successfully" }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult EditCustomerAddress(string json)
		{
			var model = new JavaScriptSerializer().Deserialize<EditAddressModel>(json);
			var address = _addressService.GetAddressById(model.Id);

			if (address == null)
				return Json(new { success = false, message = "Address Not Exist" }, JsonRequestBehavior.AllowGet);

			address.FirstName = model.FirstName;
			address.LastName = model.LastName;
			address.PhoneNumber = model.PhoneNumber;
			address.Address1 = model.Address1;
			address.Address2 = model.Address2;
			address.City = model.City;
			address.ZipPostalCode = model.ZipPostalCode;
			address.FaxNumber = model.FaxNumber;
			address.Company = model.Company;
			address.Country.Name = model.Country;
			address.Email = model.Email;

			_addressService.UpdateAddress(address);

			return Json(new { success = true, message = "Address Updated Successfully" }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult NewCustomerAddress(string json)
		{
			var model = new JavaScriptSerializer().Deserialize<EditAddressModel>(json);
			var currentCustomer = _customerService.GetCustomerById(model.Id);

			var address = new Address
			{
				Address1 = model.Address1,
				Address2 = model.Address2,
				City = model.City,
				Company = model.Company,
				Email = model.Email,
				PhoneNumber = model.PhoneNumber,
				ZipPostalCode = model.ZipPostalCode,
				CountryId = model.CountryId,
				StateProvinceId = model.StateProvinceId, 				
				CreatedOnUtc = DateTime.UtcNow
			};

			currentCustomer.Addresses.Add(address);
			_customerService.UpdateCustomer(currentCustomer);
			return Json(new { success = true, message = "New Address Added Successfully",data=address.Id}, JsonRequestBehavior.AllowGet);

		}


		public virtual ActionResult GetAddress(int addressId)
		{
			
			var address = _addressService.GetAddressById(addressId);
			if (address == null)
				return Json(new { success = false, message = "No Addresses Found" }, JsonRequestBehavior.AllowGet);

			var model = new AddressModel();
			model.Address1 = address.Address1;
			model.FirstName = address.FirstName;
			model.Id = address.Id;
			model.Address2 = address.Address2;
			model.City = address.City;
			model.PhoneNumber = address.PhoneNumber;
			model.ZipPostalCode = address.ZipPostalCode;
			model.Email = address.Email;

			//  return View(model);
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Barcode
		public ActionResult GenerateBarcode(string personalNumber)
		{
			string barcodeData = "000000000000";

			if (!string.IsNullOrEmpty(personalNumber))
			{
				barcodeData = "0000" + personalNumber;
			}
				Linear barcode = new Linear();
				barcode.Type = BarcodeType.EAN13;
				barcode.Data = barcodeData;
				barcode.drawBarcode("test.png");
				var image = barcode.drawBarcode();
				ImageConverter converter = new ImageConverter();
				var byteArraye = (byte[])converter.ConvertTo(image, typeof(byte[]));
				//return File(byteArraye, "image/jpeg");	
				String base64string = Convert.ToBase64String(byteArraye);
				return Json(new { Img = base64string }, JsonRequestBehavior.AllowGet);
		
	  } 
		#endregion


		#region Customer Reward Points
		//My account / Reward points
		public virtual ActionResult CustomerRewardPoints(int UserId)
		{
			var currentCustomer = _customerService.GetCustomerById(UserId);
			if (currentCustomer == null)
			{
				return Json(new { success = false, message = "Customer does not exists" }, JsonRequestBehavior.AllowGet);
			}
			var model = _orderModelFactory.PrepareCustomerRewardPoints(UserId);

		
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
			
		}

		#endregion

	}
}
