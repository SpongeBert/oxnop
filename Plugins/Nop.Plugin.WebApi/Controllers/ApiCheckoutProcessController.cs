﻿using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using System;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Nop.Plugin.WebApi.Models.CheckoutProcess;
using Nop.Core.Domain.Orders;
using Nop.Services.Common;
using System.Linq;
using Nop.Services.Shipping;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Customers;
using Nop.Services.Localization;
using Nop.Core.Domain.Payments;
using Nop.Services.Directory;
using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using System.Text;
using System.IO;
using System.Net;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Newtonsoft.Json;
using System.Globalization;
using Nop.Plugin.PostXCustom.Services;
using Newtonsoft.Json.Linq;
using Nop.Services.Discounts;
using Nop.Services.Tax;
using Nop.Core.Domain.Tax;
using Nop.Services.Payments;
using Nop.Services.Events;
using Nop.Services.Logging;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Services.Messages;
using Nop.Core.Domain.Localization;
using Nop.Services.CustomService;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Common;
using System.Security.Cryptography;
using System.Xml;
using System.Collections.Specialized;
using Nop.Core.Plugins;
using Nop.Plugin.PostXCustom.Factories.Public;
using Nop.Core;
using Nop.Plugin.PostXCustom.Models.Public.Checkout;


namespace Nop.Plugin.WebApi.Controllers
{
	public class ApiCheckoutProcessController : BasePluginController
	{

		#region Field
		private readonly ICustomerService _customerService;
		private readonly IOrderService _orderService;
		private readonly IProductService _productService;
		private readonly ICustomNumberFormatter _customNumberFormatter;
		private readonly IAddressService _addressService;
		private readonly IStoreService _storeService;
		private readonly IShoppingCartService _shoppingCartService;
		private readonly IShippingService _shippingService;
		private readonly ILocalizationService _localizationService;
		private readonly IGenericAttributeService _genericAttributeService;

		private readonly ICurrencyService _currencyService;
		private readonly ILanguageService _languageService;
		private readonly ICheckoutAttributeService _checkoutAttributeService;
		private readonly ICheckoutAttributeParser _checkoutAttributeParser;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICheckoutAttributeFormatter _checkoutAttributeFormatter;
		private readonly IOrderTotalCalculationService _orderTotalCalculationService;
		private readonly ITaxService _taxService;
		private readonly TaxSettings _taxSettings;
		private readonly IPaymentService _paymentService;
		private readonly IEventPublisher _eventPublisher;
		private readonly ILogger _logger;
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly IPdfService _pdfService;


		private readonly OrderSettings _orderSettings;
		private readonly LocalizationSettings _localizationSettings;
		private readonly ICustomCodeService _customCodeService;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly IRewardPointService _rewardPointService;
		private readonly ShippingSettings _shippingSettings;
		private readonly ICountryService _countryService;
		private readonly IStateProvinceService _stateProvinceService;

		private readonly IPluginFinder _pluginFinder;
		private readonly PaymentSettings _paymentSettings;
		private readonly IOrderProcessingService _orderProcessingService;
		private readonly AddressSettings _addressSettings;
		private readonly ICheckoutModelFactory _checkoutModelFactory;
		private readonly IWebHelper _webHelper;



		#endregion

		#region Const

		public ApiCheckoutProcessController(ICustomerService customerService, IStoreService storeService, IOrderService orderService, IProductService productService,
			ICustomNumberFormatter customNumberFormatter, IAddressService addressService, IShoppingCartService shoppingCartService, IShippingService shippingService,
			ILocalizationService localizationService, IGenericAttributeService genericAttributeService, ICurrencyService currencyService, ILanguageService languageService,
			ICheckoutAttributeService checkoutAttributeService, ICheckoutAttributeParser checkoutAttributeParser, ICmsNopOrderMappingService cmsNopOrderMappingService,
			ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICheckoutAttributeFormatter checkoutAttributeFormatter, IOrderTotalCalculationService orderTotalCalculationService,
			ITaxService taxService, TaxSettings taxSettings, IPaymentService paymentService, IEventPublisher eventPublisher, ILogger logger, IWorkflowMessageService workflowMessageService,
			IPdfService pdfService, OrderSettings orderSettings, LocalizationSettings localizationSettings, ICustomCodeService customCodeService, ICmsNopProductMappingService cmsNopProductMappingService,
			IRewardPointService rewardPointService, ShippingSettings shippingSettings, ICountryService countryService, IStateProvinceService stateProvinceService,
			IPluginFinder pluginFinder, PaymentSettings paymentSettings, IOrderProcessingService orderProcessingService, AddressSettings addressSettings,
			ICheckoutModelFactory checkoutModelFactory,IWebHelper webHelper)
		{
			_customerService = customerService;
			_orderService = orderService;
			_productService = productService;
			_customNumberFormatter = customNumberFormatter;
			_addressService = addressService;
			_shoppingCartService = shoppingCartService;
			_storeService = storeService;
			_shippingService = shippingService;
			_localizationService = localizationService;
			_genericAttributeService = genericAttributeService;
			_currencyService = currencyService;
			_languageService = languageService;
			_checkoutAttributeService = checkoutAttributeService;
			_checkoutAttributeParser = checkoutAttributeParser;
			_cmsNopOrderMappingService = cmsNopOrderMappingService;
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_checkoutAttributeFormatter = checkoutAttributeFormatter;
			_orderTotalCalculationService = orderTotalCalculationService;
			_taxService = taxService;
			_taxSettings = taxSettings;
			_paymentService = paymentService;
			_eventPublisher = eventPublisher;
			_logger = logger;
			_workflowMessageService = workflowMessageService;
			_pdfService = pdfService;
			_orderSettings = orderSettings;
			_localizationSettings = localizationSettings;
			_customCodeService = customCodeService;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_rewardPointService = rewardPointService;
			_shippingSettings = shippingSettings;
			_countryService = countryService;
			_stateProvinceService = stateProvinceService;
			_pluginFinder = pluginFinder;
			_paymentSettings = paymentSettings;
			_orderProcessingService = orderProcessingService;
			_addressSettings = addressSettings;
			_checkoutModelFactory = checkoutModelFactory;
			_webHelper = webHelper;
		}
		#endregion

		#region COMPLETE CHECKOUT PROCESS

		public ActionResult CompleteCheckoutProcess(string details)
		{

			if (details == null)
			{
				return this.Json("Checkout details are empty", JsonRequestBehavior.AllowGet);
			}

			CompleteCheckoutProcess model = new JavaScriptSerializer().Deserialize<CompleteCheckoutProcess>(details);


			int CustID = int.Parse(model.CustomerId);

			var custRecord = _customerService.GetCustomerById(CustID);

			if (custRecord == null)
				return this.Json("Customer Not Exist", JsonRequestBehavior.AllowGet);

			//var addBillingAddress = _addressService.GetAddressById(model.BillingAddressID);


			int storeid = _storeService.GetAllStores().FirstOrDefault().Id;
			//var addBillingAddress = custRecord.BillingAddress;

			var pickupPoints = _shippingService.GetPickupPoints(custRecord.BillingAddress,
			custRecord, "Pickup.PickupInStore", storeid).PickupPoints.ToList();
			var selectedPoint = pickupPoints.FirstOrDefault();

		
			if (model.ShippingAddressID != 0)
			{
				var addShippingAddress = _addressService.GetAddressById(model.ShippingAddressID);
				custRecord.ShippingAddress = addShippingAddress;
				_customerService.UpdateCustomer(custRecord);
			}
			else
			{

				if (_shippingSettings.AllowPickUpInStore && selectedPoint != null)
				{
					var pickUpInStoreShippingOption = new ShippingOption
					{
						Name = string.Format(_localizationService.GetResource("Checkout.PickupPoints.Name"), selectedPoint.Name),
						Rate = selectedPoint.PickupFee,
						Description = selectedPoint.Description,
						ShippingRateComputationMethodSystemName = selectedPoint.ProviderSystemName
					};

					_genericAttributeService.SaveAttribute(custRecord, SystemCustomerAttributeNames.SelectedShippingOption, pickUpInStoreShippingOption, storeid);
					_genericAttributeService.SaveAttribute(custRecord, SystemCustomerAttributeNames.SelectedPickupPoint, selectedPoint, storeid);

				}
			}




			model.PaymentStatusId = (int)PaymentStatus.Pending;
			model.OrderStatusId = (int)OrderStatus.Pending;
			if (model.ShippingAddressID != 0)
			{
				model.ShippingStatus = (int)ShippingStatus.NotYetShipped;
			}
			else
			{
				model.ShippingStatus = (int)ShippingStatus.ShippingNotRequired;
			}

			string currencycode = _currencyService.GetAllCurrencies().FirstOrDefault().CurrencyCode;
			decimal currencyRate = _currencyService.GetAllCurrencies().FirstOrDefault().Rate;

			model.CustomerCurrencyCode = currencycode;

			int languageid = 1;
			languageid = _languageService.GetAllLanguages().FirstOrDefault().Id;
			model.CustomerLanguageId = languageid;

			var newOrder = new Order();

			newOrder.StoreId = storeid;
			newOrder.CustomerId = CustID;
			if (model.ShippingAddressID != 0)
			{
				newOrder.ShippingAddressId = model.ShippingAddressID;
			}
			else
			{

				if (_shippingSettings.AllowPickUpInStore && selectedPoint != null)
				{
					var pickupPoint = custRecord.GetAttribute<PickupPoint>(SystemCustomerAttributeNames.SelectedPickupPoint, storeid);
					var country = _countryService.GetCountryByTwoLetterIsoCode(pickupPoint.CountryCode);
					var state = _stateProvinceService.GetStateProvinceByAbbreviation(pickupPoint.StateAbbreviation);

					newOrder.PickUpInStore = true;
					newOrder.PickupAddress = new Address
					{
						Address1 = pickupPoint.Address,
						City = pickupPoint.City,
						Country = country,
						StateProvince = state,
						ZipPostalCode = pickupPoint.ZipPostalCode,
						CreatedOnUtc = DateTime.UtcNow,
					};
				}
			}
			//	newOrder.BillingAddressId = shippingRecord.Id; 		
			newOrder.PaymentStatusId = model.PaymentStatusId;

			newOrder.ShippingStatusId = model.ShippingStatus;
			newOrder.CustomerCurrencyCode = model.CustomerCurrencyCode;
			newOrder.CustomerLanguageId = model.CustomerLanguageId;
			newOrder.OrderStatusId = model.OrderStatusId;
			newOrder.CurrencyRate = currencyRate;
			//newOrder.OrderTotal = model.OrderTotal;
			newOrder.CreatedOnUtc = DateTime.UtcNow;
			newOrder.OrderGuid = Guid.NewGuid();
			newOrder.CustomOrderNumber = string.Empty;


			int OrdeId = newOrder.Id;
			decimal _price = 0;

			var cart = custRecord.ShoppingCartItems
					 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
					 .LimitPerStore(storeid)
					 .ToList();

			foreach (ShoppingCartItem itm in cart)
			{
				OrderItem orderItem = new OrderItem();

				orderItem.ProductId = itm.ProductId;
				orderItem.Product = itm.Product;
				orderItem.Quantity = itm.Quantity;
				orderItem.OrderItemGuid = Guid.NewGuid();
				orderItem.OrderId = OrdeId;
				orderItem.UnitPriceInclTax = itm.Product.Price;
				orderItem.UnitPriceExclTax = itm.Product.Price;
				orderItem.PriceInclTax = itm.Product.Price;
				orderItem.PriceExclTax = itm.Product.Price;
				orderItem.ItemWeight = itm.Product.Weight;

				newOrder.OrderItems.Add(orderItem);
				_price += itm.Product.Price;

			}


			var checkOutAttributes = model.CheckoutAttributeValues;
			var allAttributes = new List<CompleteCheckoutProcess.CheckoutAttributeModel>();

			if (!string.IsNullOrEmpty(checkOutAttributes))
			{
				var pattributes = checkOutAttributes.Split(';');
				foreach (var att in pattributes)
				{
					var attValues = att.Split(',');
					var attrId = attValues[0].Split(':')[1];
					var attrVal = attValues[1].Split(':')[1];
					var attNew = new Nop.Plugin.WebApi.Models.CheckoutProcess.CompleteCheckoutProcess.CheckoutAttributeModel
					{
						AttId = attrId,
						AttValues = attrVal
					};
					allAttributes.Add(attNew);
				}
			}

			ParseAndSaveCheckoutAttributes(cart, CustID, allAttributes);


			var checkoutAttributesXml = custRecord.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, storeid);
			var checkoutAttributeDescription = _checkoutAttributeFormatter.FormatAttributes(checkoutAttributesXml, custRecord);

			//newOrder.OrderTotal = _price;
			//newOrder.OrderSubtotalInclTax = _price;
			//newOrder.OrderSubtotalExclTax = _price;
			newOrder.CheckoutAttributesXml = checkoutAttributesXml;
			newOrder.CheckoutAttributeDescription = checkoutAttributeDescription;


			//shipping total
			decimal tax;
			List<DiscountForCaching> shippingTotalDiscounts;
			var orderShippingTotalInclTax = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart, true, out tax, out shippingTotalDiscounts);
			var orderShippingTotalExclTax = _orderTotalCalculationService.GetShoppingCartShippingTotal(cart, false);

			//newOrder.OrderShippingTotalInclTax = orderShippingTotalInclTax.Value;
			//details.OrderShippingTotalExclTax = orderShippingTotalExclTax.Value;

			newOrder.OrderSubtotalInclTax = orderShippingTotalInclTax.Value;
			newOrder.OrderSubtotalExclTax = orderShippingTotalExclTax.Value;


			//foreach (var disc in shippingTotalDiscounts)
			//	if (!details.AppliedDiscounts.ContainsDiscount(disc))
			//		details.AppliedDiscounts.Add(disc);

			//payment total
			var paymentAdditionalFee = _paymentService.GetAdditionalHandlingFee(cart, model.PaymentMethodSystemName);
			newOrder.PaymentMethodAdditionalFeeInclTax =

			//details.PaymentAdditionalFeeInclTax = _taxService.GetPaymentMethodAdditionalFee(paymentAdditionalFee, true, custRecord);
				//details.PaymentAdditionalFeeExclTax = _taxService.GetPaymentMethodAdditionalFee(paymentAdditionalFee, false, custRecord);
			newOrder.PaymentMethodAdditionalFeeInclTax = _taxService.GetPaymentMethodAdditionalFee(paymentAdditionalFee, true, custRecord);
			newOrder.PaymentMethodAdditionalFeeExclTax = _taxService.GetPaymentMethodAdditionalFee(paymentAdditionalFee, false, custRecord);

			//tax amount
			SortedDictionary<decimal, decimal> taxRatesDictionary;
			newOrder.OrderTax = _orderTotalCalculationService.GetTaxTotal(cart, out taxRatesDictionary);

			//VAT number
			var customerVatStatus = (VatNumberStatus)custRecord.GetAttribute<int>(SystemCustomerAttributeNames.VatNumberStatusId);
			if (_taxSettings.EuVatEnabled && customerVatStatus == VatNumberStatus.Valid)
				//details.VatNumber = details.Customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);
				newOrder.VatNumber = custRecord.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);

			//tax rates		
			newOrder.TaxRates = taxRatesDictionary.Aggregate(string.Empty, (current, next) =>
				 string.Format("{0}{1}:{2};   ", current, next.Key.ToString(CultureInfo.InvariantCulture), next.Value.ToString(CultureInfo.InvariantCulture)));

			//order total (and applied discounts, gift cards, reward points)
			List<AppliedGiftCard> appliedGiftCards;
			List<DiscountForCaching> orderAppliedDiscounts;
			decimal orderDiscountAmount;
			int redeemedRewardPoints;
			decimal redeemedRewardPointsAmount;
			var orderTotal = _orderTotalCalculationService.GetShoppingCartTotal(cart, out orderDiscountAmount,
				 out orderAppliedDiscounts, out appliedGiftCards, out redeemedRewardPoints, out redeemedRewardPointsAmount);

			newOrder.OrderDiscount = orderDiscountAmount;
			newOrder.RewardPointsHistoryEntryId = redeemedRewardPoints;
			//	newOrder.RedeemedRewardPointsAmount = redeemedRewardPointsAmount;
			//newOrder.AppliedGiftCards = appliedGiftCards;
			newOrder.OrderTotal = orderTotal.Value;


			//newOrder.PaymentMethodSystemName = model.PaymentMethodSystemName;
			if (newOrder.OrderTotal == 0)
			{
				newOrder.PaymentMethodSystemName = "payments.loyaltypoints";
			}
			else
			{
				//load payment method
				var paymentMethodSystemName = custRecord.GetAttribute<string>(
					 SystemCustomerAttributeNames.SelectedPaymentMethod,
					 _genericAttributeService, storeid);
				var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentMethodSystemName);

				//newOrder.PaymentMethodSystemName = "Payments.Worldline";
				newOrder.PaymentMethodSystemName = paymentMethodSystemName;
			}



			//discount history
			//foreach (var disc in orderAppliedDiscounts)
			//	if (!details.AppliedDiscounts.ContainsDiscount(disc))
			//		details.AppliedDiscounts.Add(disc);

			if (_taxSettings.AllowCustomersToSelectTaxDisplayType)
				//details.CustomerTaxDisplayType = (TaxDisplayType)custRecord.GetAttribute<int>(SystemCustomerAttributeNames.TaxDisplayTypeId, storeid);
				newOrder.CustomerTaxDisplayTypeId = (int)((TaxDisplayType)custRecord.GetAttribute<int>(SystemCustomerAttributeNames.TaxDisplayTypeId, storeid));
			else
				newOrder.CustomerTaxDisplayTypeId = (int)_taxSettings.TaxDisplayType;

			//sub total (incl tax)
			decimal orderSubTotalDiscountAmount;
			List<DiscountForCaching> orderSubTotalAppliedDiscounts;
			decimal subTotalWithoutDiscountBase;
			decimal subTotalWithDiscountBase;
			_orderTotalCalculationService.GetShoppingCartSubTotal(cart, true, out orderSubTotalDiscountAmount,
				 out orderSubTotalAppliedDiscounts, out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);


			newOrder.OrderSubtotalInclTax = subTotalWithoutDiscountBase;
			newOrder.OrderSubTotalDiscountInclTax = orderSubTotalDiscountAmount;



			//sub total (excl tax)
			_orderTotalCalculationService.GetShoppingCartSubTotal(cart, false, out orderSubTotalDiscountAmount,
				 out orderSubTotalAppliedDiscounts, out subTotalWithoutDiscountBase, out subTotalWithDiscountBase);
			//details.OrderSubTotalExclTax = subTotalWithoutDiscountBase;
			//details.OrderSubTotalDiscountExclTax = orderSubTotalDiscountAmount;
			newOrder.OrderSubtotalExclTax = subTotalWithoutDiscountBase;
			newOrder.OrderSubTotalDiscountExclTax = orderSubTotalDiscountAmount;

			// save order
			_orderService.InsertOrder(newOrder);

			newOrder.CustomOrderNumber = _customNumberFormatter.GenerateOrderCustomNumber(newOrder);

			newOrder.OrderNotes.Add(new OrderNote { Note = "Order placed on mobile App", OrderId = newOrder.Id, CreatedOnUtc = DateTime.UtcNow, DisplayToCustomer = false });

			_orderService.UpdateOrder(newOrder);


			//reward points history
			if (redeemedRewardPointsAmount > decimal.Zero)
			{
				_rewardPointService.AddRewardPointsHistoryEntry(newOrder.Customer, -redeemedRewardPoints, storeid,
				  string.Format(_localizationService.GetResource("RewardPoints.Message.RedeemedForOrder", newOrder.CustomerLanguageId), newOrder.CustomOrderNumber),
				  newOrder, redeemedRewardPointsAmount);
				_customerService.UpdateCustomer(newOrder.Customer);
			}

			if (newOrder.PaymentMethodSystemName.ToLower() == "payments.loyaltypoints")
			{
				newOrder.OrderStatusId = (int)OrderStatus.Processing;
				newOrder.PaymentStatusId = (int)PaymentStatus.Paid;
				_orderService.UpdateOrder(newOrder);
				_eventPublisher.Publish(new OrderPlacedEvent(newOrder));
			}

			//raise event       
			//_eventPublisher.Publish(new OrderPlacedEvent(newOrder));

			foreach (ShoppingCartItem item in cart)
			{
				//inventory
				_productService.AdjustInventory(item.Product, -item.Quantity, item.AttributesXml,
					 string.Format("PlaceOrder", newOrder.Id));
			}


			//clear shopping cart
			cart.ToList().ForEach(sci => _shoppingCartService.DeleteShoppingCartItem(sci, false));


			//Clear loyalty point usage record from generic attribute                     
			_genericAttributeService.SaveAttribute(custRecord, SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, false, storeid);
			_genericAttributeService.SaveAttribute(custRecord, "PointsToUse", 0, storeid);


			return Json(newOrder.Id, JsonRequestBehavior.AllowGet);
			//return this.Json("Checkout details submited Successfuly");
		}

		protected void ParseAndSaveCheckoutAttributes(List<ShoppingCartItem> cart, int customerId, List<CompleteCheckoutProcess.CheckoutAttributeModel> Attmodel)
		{
			string attributesXml = "";
			int storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			var customer = _customerService.GetCustomerById(customerId);
			var checkoutAttributes = _checkoutAttributeService.GetAllCheckoutAttributes(storeId, !cart.RequiresShipping());

			//var attribute = _checkoutAttributeService.GetCheckoutAttributeById(attributeId);



			foreach (var attribute in checkoutAttributes)
			{

				string controlId = string.Format("checkout_attribute_{0}", attribute.Id);

				var item = Attmodel.FirstOrDefault(i => Int32.Parse(i.AttId) == attribute.Id);

				//var lastItem = Attmodel.LastOrDefault(i => Int32.Parse(i.AttId) == attribute.Id);

				//if(attribute.Id == item)

				switch (attribute.AttributeControlType)
				{
					case AttributeControlType.DropdownList:
					case AttributeControlType.RadioList:
					case AttributeControlType.ColorSquares:
						{

							if (!string.IsNullOrEmpty(item.AttValues))
							{
								attributesXml = _checkoutAttributeParser.AddCheckoutAttribute(attributesXml,
												attribute, item.AttValues);
							}
						}
						break;
					case AttributeControlType.TextBox:
					case AttributeControlType.MultilineTextbox:
						{
							//	string controlId = string.Format("checkout_attribute_{0}", attributeId);

							if (!String.IsNullOrEmpty(item.AttValues))
							{
								attributesXml = _checkoutAttributeParser.AddCheckoutAttribute(attributesXml,
									  attribute, item.AttValues);
							}
						}
						break;
					default:
						break;
				}


			}
			//}
			//validate conditional attributes (if specified)
			foreach (var attribute in checkoutAttributes)
			{
				var conditionMet = _checkoutAttributeParser.IsConditionMet(attribute, attributesXml);
				if (conditionMet.HasValue && !conditionMet.Value)
					attributesXml = _checkoutAttributeParser.RemoveCheckoutAttribute(attributesXml, attribute);
			}


			//save checkout attributes
			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CheckoutAttributes, attributesXml, storeId);
		}
		#endregion

		#region Checkout Process

		public ActionResult ConfirmOrder(string details)
		{

			//details = "{orderStatus : 'Pending',BillingAddressID:'1',ShippingAddressID:'1', customerId:'1',shipToSameAddress :'false', cartItem: [{productId : '25', pname:'	adidas Consortium Campus 80s Running Shoes', productQty : '1'}]}";

			var model = new JavaScriptSerializer().Deserialize<ConfirmOrderProcess>(details);

			int storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			var customerId = int.Parse(model.CustomerId);
			var custRecord = _customerService.GetCustomerById(customerId);

			if (custRecord == null)
				return Json(new { success = false, message = "Customer Not Exist" });

			// commented to set default billing and pickup point
			//var addBillingAddress = _addressService.GetAddressById(model.BillingAddressID);
			//var addShippingAddress = _addressService.GetAddressById(model.ShippingAddressID);


			var addBillingAddress = custRecord.BillingAddress;

			var pickupPoints = _shippingService.GetPickupPoints(custRecord.BillingAddress,
			custRecord, "Pickup.PickupInStore", storeId).PickupPoints.ToList();
			var selectedPoint = pickupPoints.FirstOrDefault();

			var addShippingAddress = selectedPoint;

			var pickUpInStoreShippingOption = new ShippingOption
			{
				Name = string.Format(_localizationService.GetResource("Checkout.PickupPoints.Name"), selectedPoint.Name),
				Rate = selectedPoint.PickupFee,
				Description = selectedPoint.Description,
				ShippingRateComputationMethodSystemName = selectedPoint.ProviderSystemName
			};
			_genericAttributeService.SaveAttribute(custRecord, SystemCustomerAttributeNames.SelectedShippingOption, pickUpInStoreShippingOption, storeId);
			_genericAttributeService.SaveAttribute(custRecord, SystemCustomerAttributeNames.SelectedPickupPoint, selectedPoint, storeId);


			//if (custRecord.Active && !custRecord.Deleted)
			//{
			//	custRecord.BillingAddress = addBillingAddress;
			//	custRecord.ShippingAddress = model.ShipToSameAddress ? addBillingAddress : addShippingAddress;
			//	_customerService.UpdateCustomer(custRecord);
			//}
			if (custRecord.Active == true && custRecord.Deleted == false)
			{

				custRecord.BillingAddress = addBillingAddress;
				custRecord.ShippingAddress = null;

				//if (model.ShipToSameAddress == true)
				//{
				//	custRecord.ShippingAddress = addBillingAddress;
				//}

				//else
				//{
				//	custRecord.ShippingAddress = addShippingAddress;
				//}

				_customerService.UpdateCustomer(custRecord);
			}

			ConfirmOrderProcess modelnew = new ConfirmOrderProcess();


			modelnew.BillingAddressID = addBillingAddress.Id;
			//modelnew.ShippingAddressID = addShippingAddress.Id;

			modelnew.BillingAddress.Address1 = addBillingAddress.Address1;
			modelnew.BillingAddress.Address2 = addBillingAddress.Address2;
			modelnew.BillingAddress.City = addBillingAddress.City;
			modelnew.BillingAddress.Company = addBillingAddress.Company;
			modelnew.BillingAddress.CountryName = addBillingAddress.Country.Name;
			modelnew.BillingAddress.CountryId = addBillingAddress.CountryId;
			//modelnew.BillingAddress.CreatedOnUtc = addBillingAddress.CreatedOnUtc;
			modelnew.BillingAddress.Email = addBillingAddress.Email;
			modelnew.BillingAddress.FaxNumber = addBillingAddress.FaxNumber;
			modelnew.BillingAddress.FirstName = addBillingAddress.FirstName;
			modelnew.BillingAddress.LastName = addBillingAddress.LastName;
			modelnew.BillingAddress.PhoneNumber = addBillingAddress.PhoneNumber;
			modelnew.BillingAddress.StateProvinceName = addBillingAddress.StateProvince.Name;
			modelnew.BillingAddress.StateProvinceId = addBillingAddress.StateProvinceId;
			modelnew.BillingAddress.ZipPostalCode = addBillingAddress.ZipPostalCode;


			//modelnew.ShippingAddress.Address1 = custRecord.ShippingAddress.Address1;
			//modelnew.ShippingAddress.Address2 = custRecord.ShippingAddress.Address2;
			//modelnew.ShippingAddress.City = custRecord.ShippingAddress.City;
			//modelnew.ShippingAddress.Company = custRecord.ShippingAddress.Company;
			//modelnew.ShippingAddress.CountryName = custRecord.ShippingAddress.Country.Name;
			//modelnew.ShippingAddress.CountryId = custRecord.ShippingAddress.CountryId;
			////modelnew.ShippingAddress.CreatedOnUtc = custRecord.ShippingAddress.CreatedOnUtc;
			//modelnew.ShippingAddress.Email = custRecord.ShippingAddress.Email;
			//modelnew.ShippingAddress.FaxNumber = custRecord.ShippingAddress.FaxNumber;
			//modelnew.ShippingAddress.FirstName = custRecord.ShippingAddress.FirstName;
			//modelnew.ShippingAddress.LastName = custRecord.ShippingAddress.LastName;
			//modelnew.ShippingAddress.PhoneNumber = custRecord.ShippingAddress.PhoneNumber;
			//modelnew.ShippingAddress.StateProvinceName = custRecord.ShippingAddress.StateProvince.Name;
			//modelnew.ShippingAddress.StateProvinceId = custRecord.ShippingAddress.StateProvinceId;
			//modelnew.ShippingAddress.ZipPostalCode = custRecord.ShippingAddress.ZipPostalCode;

			return Json(new { success = true, message = "", data = modelnew }, JsonRequestBehavior.AllowGet);
		}

		#endregion


		#region mobile redirection



		public ActionResult SetOrderStatus(int orderId)
		{
			string requestUrl = HttpContext.Request.RawUrl;
			var order = _orderService.GetOrderById(orderId);
			var paymentBrandName = string.Empty;
			var paymentInfo = new StarringJane.Worldline.Sips.Models.PaymentInfo(Request.Form);

			var customer = order.Customer;

			var orderTransactionIDAttribute = _genericAttributeService.GetAttributesForEntity(order.Id, "Order").FirstOrDefault(x => x.Key == "TransactionID");
			if (orderTransactionIDAttribute != null)
			{
				this._logger.Information("[Worldline] Marking transactionId=" + paymentInfo.Data["transactionReference"] + " at time: " + DateTime.UtcNow);
			}

			string responseCodeVal = "Payment Response Code: " + paymentInfo.Data["responseCode"].ToString() + " -  Description:" + ExceptionDescription(paymentInfo.Data["responseCode"]);

			StringBuilder strmessage = new StringBuilder();
			strmessage.Append("Payment Response for Order ::");
			strmessage.Append("<br/>");
			strmessage.Append("User Info :" + customer.Id + "-" + customer.Email + "-" + customer.GetFullName());
			strmessage.Append("<br/>");
			strmessage.Append("Order Id: " + orderId);
			strmessage.Append("<br/>");
			strmessage.Append("Payment Response Code: " + paymentInfo.Data["responseCode"].ToString());
			strmessage.Append("<br/>");
			strmessage.Append("Description:" + ExceptionDescription(paymentInfo.Data["responseCode"]));
			strmessage.Append("<br/>");
			strmessage.Append("Redirection Url:" + requestUrl);
			strmessage.Append("<br/>");
			strmessage.Append("transactionId:" + paymentInfo.Data["transactionReference"]);



			_logger.InsertLog(LogLevel.Information, "SIPS  after payment  for order :: " + orderId, "Payment Response Code for Order :: " + responseCodeVal);

			_customCodeService.SendInformationEmail("Payment Response Code for Order  ::  " + orderId + "- Response Code::" + paymentInfo.Data["responseCode"].ToString(), "Payment Response Code for Order :: " + strmessage.ToString());

			if (paymentInfo.Data.Any())
			{
				var paymentMethod = paymentInfo.Data.FirstOrDefault(c => c.Key.ToLower() == "paymentmeanbrand");
				if (paymentMethod.Key != null && paymentMethod.Value != null)
				{
					paymentBrandName = paymentMethod.Value;
					switch (paymentMethod.Value.ToLower())
					{
						case "bcmc":

							ViewBag.AutoClose = "";
							break;

						default:
							ViewBag.AutoClose = "Dit scherm sluit automatisch in 5 seconden";
							break;

					}
				}
			}


			//if (paymentInfo.Data["responseCode"] == "00" || paymentInfo.Data["responseCode"] == "60")
			if (paymentInfo.Data["responseCode"] == "00")
			{
				try
				{
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
					order.OrderStatusId = (int)OrderStatus.Processing;
					order.PaymentStatusId = (int)PaymentStatus.Paid;
					_orderService.UpdateOrder(order);
					//LightSpeedOrderStatusUpdate(orderId);
					//LightSpeedOrderCreate(orderId, paymentInfo);
					LightSpeedOrderCreate(orderId, paymentInfo);
					_logger.InsertLog(LogLevel.Information, "Order pushed to LS from App :: " + orderId);
					//notifications//Custom Code // Notification to be send after payment success	  

					SendNotificationsAndSaveNotesApp(orderId);
					_logger.InsertLog(LogLevel.Information, "Order Notification Sent  from App:: " + orderId);


					if (!string.IsNullOrEmpty(paymentBrandName))
					{
						if (paymentBrandName.ToLower() == "bcmc")
							return View("~/Plugins/WebApi/Views/OrderStatus.cshtml");
					}

					return View("~/Plugins/WebApi/Views/OrderStatusOther.cshtml");
					// Order successful (accepted or pending)
					// Save the payment data in your website's database, f.e. paymentInfo.Data["transactionReference"]
				}
				catch (Exception ex)
				{
					_logger.InsertLog(LogLevel.Error, "SIPS error after payment success for nop order :: " + orderId, ex.Message + "  ||   " + ex.StackTrace);
					_customCodeService.SendErrorEmail("SIPS error after payment success for nop order ::  " + orderId, ex.Message, ex.StackTrace);
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);

					return View("~/Plugins/WebApi/Views/MobileException.cshtml");
				}


			}
			else if (paymentInfo.Data["responseCode"] == "17")
			{
				ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
				// Order cancelled		 			
				return View("~/Plugins/WebApi/Views/OrderCancelled.cshtml");

			}
			else
			{
				ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
				return View("~/Plugins/WebApi/Views/MobileException.cshtml");
				// You can find more information on the response codes in the appendix
			}
		}


		public ActionResult SetOrderStatusAndroid(int orderId)
		{
			string requestUrl = HttpContext.Request.RawUrl;
			var order = _orderService.GetOrderById(orderId);
			var paymentBrandName = string.Empty;
			var paymentInfo = new StarringJane.Worldline.Sips.Models.PaymentInfo(Request.Form);

			var customer = order.Customer;
			var orderTransactionIDAttribute = _genericAttributeService.GetAttributesForEntity(order.Id, "Order").FirstOrDefault(x => x.Key == "TransactionID");
			if (orderTransactionIDAttribute != null)
			{
				this._logger.Information("[Worldline] Marking transactionId=" + paymentInfo.Data["transactionReference"] + " at time: " + DateTime.UtcNow);
			}

			string responseCodeVal = "Payment Response Code: " + paymentInfo.Data["responseCode"].ToString() + " -  Description:" + ExceptionDescription(paymentInfo.Data["responseCode"]);

			StringBuilder strmessage = new StringBuilder();
			strmessage.Append("Payment Response for Order ::");
			strmessage.Append("<br/>");
			strmessage.Append("User Info :" + customer.Id + "-" + customer.Email + "-" + customer.GetFullName());
			strmessage.Append("<br/>");
			strmessage.Append("Order Id: " + orderId);
			strmessage.Append("<br/>");
			strmessage.Append("Payment Response Code: " + paymentInfo.Data["responseCode"].ToString());
			strmessage.Append("<br/>");
			strmessage.Append("Description:" + ExceptionDescription(paymentInfo.Data["responseCode"]));
			strmessage.Append("<br/>");
			strmessage.Append("Redirection Url:" + requestUrl);
			strmessage.Append("<br/>");
			strmessage.Append("transactionId:" + paymentInfo.Data["transactionReference"]);


			_logger.InsertLog(LogLevel.Information, "SIPS  after payment  for order :: " + orderId, "Payment Response Code for Order :: " + responseCodeVal);

			_customCodeService.SendInformationEmail("Payment Response Code for Order  ::  " + orderId + "- Response Code::" + paymentInfo.Data["responseCode"].ToString(), "Payment Response Code for Order :: " + strmessage.ToString());

			if (paymentInfo.Data.Any())
			{
				var paymentMethod = paymentInfo.Data.FirstOrDefault(c => c.Key.ToLower() == "paymentmeanbrand");
				if (paymentMethod.Key != null && paymentMethod.Value != null)
				{
					paymentBrandName = paymentMethod.Value;
					switch (paymentMethod.Value.ToLower())
					{
						case "bcmc":
							ViewBag.AutoClose = "";
							break;

						default:
							ViewBag.AutoClose = "Dit scherm sluit automatisch in 5 seconden";
							break;

					}
				}
			}


			//if (paymentInfo.Data["responseCode"] == "00" || paymentInfo.Data["responseCode"] == "60")
			if (paymentInfo.Data["responseCode"] == "00")
			{
				try
				{
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
					order.OrderStatusId = (int)OrderStatus.Processing;
					order.PaymentStatusId = (int)PaymentStatus.Paid;
					_orderService.UpdateOrder(order);
					//LightSpeedOrderStatusUpdate(orderId);
					LightSpeedOrderCreate(orderId, paymentInfo);
					_logger.InsertLog(LogLevel.Information, "Order pushed to LS from App :: " + orderId);
					//notifications//Custom Code // Notification to be send after payment success	  

					SendNotificationsAndSaveNotesApp(orderId);
					_logger.InsertLog(LogLevel.Information, "Order Notification Sent  from App:: " + orderId);


					_logger.InsertLog(LogLevel.Information, "Redirecting after payment to our site:: " + orderId + " Payment Mode:" + paymentBrandName);

					if (!string.IsNullOrEmpty(paymentBrandName))
					{
						if (paymentBrandName.ToLower() == "bcmc")
						{
							return View("~/Plugins/WebApi/Views/OrderStatusAndroidOther.cshtml");
						}
					}

					return View("~/Plugins/WebApi/Views/OrderStatusAndroid.cshtml");

					// Order successful (accepted or pending)
					// Save the payment data in your website's database, f.e. paymentInfo.Data["transactionReference"]
				}
				catch (Exception ex)
				{
					_logger.InsertLog(LogLevel.Error, "SIPS error after payment success for nop order :: " + orderId, ex.Message + "  ||   " + ex.StackTrace);

					_customCodeService.SendErrorEmail("SIPS error after payment success for nop order ::  " + orderId, ex.Message, ex.StackTrace);
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);

					return View("~/Plugins/WebApi/Views/MobileExceptionAndroid.cshtml");
				}


			}
			else if (paymentInfo.Data["responseCode"] == "17")
			{
				ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
				// Order cancelled	 			
				return View("~/Plugins/WebApi/Views/OrderCancelledAndroid.cshtml");

			}
			else
			{
				ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
				return View("~/Plugins/WebApi/Views/MobileExceptionAndroid.cshtml");
				// You can find more information on the response codes in the appendix
			}
		}


		public ActionResult CustomerCancel()
		{
			return View("~/Plugins/WebApi/Views/OrderCancelled.cshtml");
		}
		private string ExceptionDescription(string responseCode)
		{
			switch (responseCode)
			{
				case "02": return "Authorisation request to be performed via telephone with the issuer, as the card authorisation threshold has been exceeded. You need to be authorised to force transactions";
				case "03": return "Invalid Merchant contract";
				case "05": return "Authorisation refused";
				case "11": return "Used for differed check. The PAN is blocked.";
				case "12": return "Invalid transaction, check the request parameters";
				case "14": return "Invalid PAN or payment mean data(ex: card security code)";
				case "17": return "Buyer cancellation";
				case "24": return "Operation not authorized.The operation you wish to perform is not compliant with the transaction status";
				case "25": return "Transaction unknown by Sips";
				case "30": return "Format error";
				case "34": return "Fraud suspicion(seal erroneous)";
				case "40": return "Function not supported: the operation that you wish to perform is not part of the operation type for which you are authorised";
				case "51": return "Amount too high";
				case "54": return "Payment mean expiry date is past";
				case "60": return "Transaction pending";
				case "63": return "Security rules not observed, transaction stopped";
				case "75": return "Exceeded number of PAN attempts";
				case "90": return "Service temporarily not available";
				case "94": return "Duplicated transaction: the transactionReference has been used previously";
				case "97": return "Time frame exceeded, transaction refused";
				case "99": return "Temporary problem at the Sips server level";
				default: return string.Empty;
			}
		}

		#endregion

		#region LS
		public void LightSpeedOrderCreate(long orderId, StarringJane.Worldline.Sips.Models.PaymentInfo paymentInfo)
		{
			#region Push Order to CMS
			var currentCustomer = new Customer();
			try
			{
				var order = _orderService.GetOrderById(Convert.ToInt32(orderId));
				currentCustomer = _customerService.GetCustomerById(order.CustomerId);

				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order not found in webshop :: " + orderId,
						 "Order push to cms :: Error :: Order not found in webshop :: " + orderId, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId, "Order push to cms :: Error :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				if (order.PaymentStatus != PaymentStatus.Paid)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, "Current payment status :: " + order.PaymentStatus);

					return;
				}

				var cmsNopOrder = _cmsNopOrderMappingService.GetByNopOrderId(order.Id);
				if (cmsNopOrder != null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id + " :: LS Order Id :: " + cmsNopOrder.CmsOrderId,
						 "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId + " :: LS Order Id :: " + cmsNopOrder.CmsOrderId,
						 "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id, string.Empty);
					return;
				}

				var checkCmsCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
				if (checkCmsCustomer == null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, string.Empty);
					return;
				}

				string token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, string.Empty);
					return;
				}

				//string url = @"http://staging-exact-integration.posios.com/PosServer/rest/onlineordering/customer/" + checkCmsCustomer.CmsCustomerId + "/order";
				string url = @"https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + checkCmsCustomer.CmsCustomerId + "/order";
				var postData = new StringBuilder();
				postData.Append("{\"customerId\":" + checkCmsCustomer.CmsCustomerId + ",");

				var currentData = DateTime.Now;
				string newDate = currentData.Year + "-" + currentData.Month + "-" + currentData.Day + "T" + currentData.Hour + ":" +
									  currentData.Minute + ":" + currentData.Second + "+00:00";
				postData.Append("\"deliveryDate\":\"" + newDate + "\",");

				var orderType = GetOrderType(order);
				postData.Append("\"type\":\"" + orderType + "\",");
				postData.Append("\"status\":\"" + "ACCEPTED" + "\",");
				postData.Append("\"description\":\"" + "order" + "\",");

				if (orderType.ToLower() == "delivery")
				{
					var deliveryTime = string.Empty;
					var deliveryTimeDescripton = order.CheckoutAttributeDescription.Substring(order.CheckoutAttributeDescription.LastIndexOf("Gev"));

					if (!string.IsNullOrEmpty(deliveryTimeDescripton))
					{
						deliveryTime = deliveryTimeDescripton;
					}

					postData.Append("\"note\":\"" + "Order Reference: " + order.Id + " || " + deliveryTime + "\",");
				}
				else
					postData.Append("\"note\":\"" + "Order Reference: " + order.Id + "\",");

				postData.Append("\"orderItems\":[");
				var totalItems = order.OrderItems.Count;
				var counter = 0;
				foreach (var orderItem in order.OrderItems)
				{
					counter++;
					var cmsProduct = _cmsNopProductMappingService.GetByNopProductId(orderItem.ProductId);
					if (cmsProduct == null)
					{
						_logger.InsertLog(LogLevel.Error, "Error :: Order sync with CMS :: Nop Order Id :: " + orderItem.OrderId,
							 "Could not find mapped CMS product against Nop Product :: ID :: " + orderItem.ProductId + " :: Product Name :: " +
							 orderItem.Product.Name, currentCustomer);
						break;
					}

					postData.Append("{\"productId\":" + cmsProduct.CmsProductId + ",");
					postData.Append("\"amount\":" + orderItem.Quantity + ",");



					var attributePriceDeduction = 0M;
					if (!String.IsNullOrEmpty(orderItem.AttributesXml))
					{
						postData.Append("\"modifiers\":[");
						var xmlDoc = new XmlDocument();
						xmlDoc.LoadXml(orderItem.AttributesXml);

						var nodeCounter = 0;
						var attrCounter = 0;
						var nodeList = xmlDoc.SelectNodes(@"//Attributes/ProductAttribute");

						foreach (XmlNode node in xmlDoc.SelectNodes(@"//Attributes/ProductAttribute"))
						{
							nodeCounter++;
							if (node.Attributes != null && node.Attributes["ID"] != null)
							{
								int attributeId;
								var nopAttributeValueId = node.InnerText;
								if (!string.IsNullOrEmpty(nopAttributeValueId))
								{
									var nopCmsProductAttributeValueMapping = _cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(Convert.ToInt32(nopAttributeValueId));

									var nopCmsProductAttributeMapping = new NopCmsProductAttributeMapping();
									if (nopCmsProductAttributeValueMapping != null)
									{
										nopCmsProductAttributeMapping = _cmsNopProductMappingService.GetNopCmsProductAttributeById(nopCmsProductAttributeValueMapping.NopCmsProductAttributeMappingId);
									}

									if (attrCounter > 0)
										postData.Append(",{");
									else
										postData.Append("{");

									if (nopCmsProductAttributeMapping != null)
									{
										postData.Append("\"description\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");
										postData.Append("\"modifierId\":" + nopCmsProductAttributeMapping.CmsProductAttributeId + ",");
										postData.Append("\"modifierName\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");

									}
									foreach (XmlNode attributeValue in node.SelectNodes("ProductAttributeValue"))
									{
										var value = attributeValue.SelectSingleNode("Value").InnerText.Trim();
										if (!String.IsNullOrEmpty(value))
										{
											var cmsProductAttributeValue = _cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(Convert.ToInt32(value));
											if (cmsProductAttributeValue != null)
											{
												postData.Append("\"modifierValueId\":" + cmsProductAttributeValue.CmsProductAttributeValueMappingId + ",");
												postData.Append("\"price\":" + cmsProductAttributeValue.Price + ",");
												postData.Append("\"priceWithoutVat\":" + cmsProductAttributeValue.Price);
												attributePriceDeduction += cmsProductAttributeValue.Price;
											}
										}
									}

									postData.Append("}");
									attrCounter++;
								}
							}
						}
						postData.Append("],");
					}
					var orderItemPrice = orderItem.PriceInclTax - attributePriceDeduction;

					postData.Append("\"totalPrice\":" + orderItemPrice + ",");
					postData.Append("\"totalPriceWithoutVat\":" + orderItemPrice + ",");
					postData.Append("\"unitPrice\":" + orderItemPrice + ",");
					postData.Append("\"unitPriceWithoutVat\":" + orderItemPrice);
					postData.Append(counter >= totalItems ? "}" : "},");
				}
				//Temp Testing
				//postData.Append("],\"orderPayments\":[{");
				//postData.Append("\"amount\":" + order.OrderTotal + ",");
				//postData.Append("\"paymentTypeId\":" + 39893 + ",");
				//postData.Append("\"paymentTypeTypeId\":" + 4 + "}");


				var paymentTypeId = 0;
				var paymentTypeTypeId = 0;
				var paymentOption = string.Empty;

				GetLSPaymentInformation(paymentInfo, ref paymentTypeId, ref paymentTypeTypeId, ref paymentOption);

				if (paymentTypeId == 0 || paymentTypeTypeId == 0)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId,
						 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, string.Empty);
					return;
				}

				postData.Append("],\"orderPayments\":[{");
				postData.Append("\"amount\":" + order.OrderTotal + ",");
				postData.Append("\"paymentTypeId\":" + paymentTypeId + ",");
				postData.Append("\"paymentTypeTypeId\":" + paymentTypeTypeId + "}");



				if (order.RedeemedRewardPointsEntry != null)
				{
					//Use Credit points
					//postData.Append(",{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
					//postData.Append("\"paymentTypeId\":" + 5208 + ",");
					//postData.Append("\"paymentTypeTypeId\":" + 15 + "}");

					//Use Cash
					postData.Append(",{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
					postData.Append("\"paymentTypeId\":" + 36191 + ",");
					postData.Append("\"paymentTypeTypeId\":" + 200 + "}");
				}



				postData.Append("],\"orderTaxInfo\":[{");
				postData.Append("\"tax\":" + "0" + ",");
				postData.Append("\"taxRate\":" + "0" + ",");
				postData.Append("\"totalWithTax\":" + order.OrderTotal + ",");
				postData.Append("\"totalWithoutTax\":" + order.OrderTotal);
				postData.Append("}]}");

				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Order#: " + order.Id, postData.ToString(), currentCustomer);

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					if (orderType.ToLower() == "delivery")
					{
						UpdateCustomerAddressOnLightSpeed(order, checkCmsCustomer);
					}
					else
					{
						UpdateCustomerNameOnLightSpeed(Convert.ToInt32(responseData), checkCmsCustomer, order.Customer, order);
					}


					var cmsOrderNopOrderMap = new CmsOrderNopOrderMapping
					{
						NopOrderId = order.Id,
						CmsOrderId = Convert.ToInt32(responseData),
						CreatedOnUtc = DateTime.UtcNow,
						UpdatedOnUtc = DateTime.UtcNow
					};

					_cmsNopOrderMappingService.Insert(cmsOrderNopOrderMap);
					order.CustomOrderNumber = responseData;
					_orderService.UpdateOrder(order);
				}

			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error Creating Order on CMS for nop order :: " + orderId,
					 ex.Message + "  ||   " + ex.StackTrace, currentCustomer);

				_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId, ex.Message, ex.StackTrace);

				return;
			}
			#endregion
		}

		public string GetToken()
		{
			try
			{
				string token = string.Empty;

				//string url = @"http://staging-exact-integration.posios.com/PosServer/rest/token";
				//string postData = "{\"companyId\": 14500,\"deviceId\": \"prod\",\"password\": \"bK6VrHCbjctGZdZ5q7\",\"username\": \"staging@webshopcompany.be\"}";

				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}


		public void GetLSPaymentInformation(StarringJane.Worldline.Sips.Models.PaymentInfo paymentInfo, ref int paymentTypeId, ref int paymentTypeTypeId, ref string paymentOption)
		{
			if (paymentInfo.Data.Any())
			{
				var paymentMethod = paymentInfo.Data.FirstOrDefault(c => c.Key.ToLower() == "paymentmeanbrand");
				if (paymentMethod.Key != null && paymentMethod.Value != null)
				{
					paymentOption = paymentMethod.Value;
					//switch (paymentMethod.Value.ToLower())
					//{
					//	case "visa":
					//		paymentTypeId = 40580;
					//		paymentTypeTypeId = 801;
					//		break;
					//	case "mastercard":
					//		paymentTypeId = 39893;
					//		paymentTypeTypeId = 801;
					//		break;
					//	case "bcmc":
					//		paymentTypeId = 36192;
					//		paymentTypeTypeId = 801;
					//		break;
					//	case "maestro":
					//		paymentTypeId = 40976;
					//		paymentTypeTypeId = 801;
					//		break;
					//	case "vpay":
					//		paymentTypeId = 40977;
					//		paymentTypeTypeId = 801;
					//		break;
					//}

					
					switch (paymentMethod.Value.ToLower())
					{
						case "visa":
							paymentTypeId = 65537;
							paymentTypeTypeId = 801;
							break;
						case "mastercard":
							paymentTypeId = 65538;
							paymentTypeTypeId = 801;
							break;
						case "bcmc":
							paymentTypeId = 65536;
							paymentTypeTypeId = 801;
							break;
						case "maestro":
							paymentTypeId = 65557;
							paymentTypeTypeId = 801;
							break;
						case "vpay":
							paymentTypeId = 40977;
							paymentTypeTypeId = 801;
							break;
					}

				}
			}
		}


		public string GetOrderType(Order order)
		{
			if (order == null)
				return "takeaway";

			if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
			{
				if (order.CheckoutAttributeDescription.ToLower().Contains("levering"))
					return "delivery";
			}
			return "takeaway";
		}

		public void UpdateCustomerAddressOnLightSpeed(Order order, CmsCustomerNopCustomerMapping checkCmsCustomer)
		{
			var currentCustomer = new Customer();
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				currentCustomer = _customerService.GetCustomerById(order.CustomerId);
				int storeId = _storeService.GetAllStores().FirstOrDefault().Id;

				var token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, currentCustomer);

					_customCodeService.SendErrorEmail("Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, string.Empty);
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"deliveryCity\":\"" + order.ShippingAddress.City + "\",");
				postData.Append("\"deliveryCountry\":\"" + order.ShippingAddress.Country.TwoLetterIsoCode + "\",");
				postData.Append("\"deliveryStreet\":\"" + order.ShippingAddress.Address1 + "\",");
				postData.Append("\"deliveryStreetNumber\":\"" + order.ShippingAddress.Address2 + "\",");
				postData.Append("\"deliveryZip\":\"" + order.ShippingAddress.ZipPostalCode + "\",");
				postData.Append("\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + order.Customer.Email + "\",");

				//var loyaltyPoints = "0 reward points available.";
				//var customerRewardPointsBalance = _rewardPointService.GetRewardPointsBalance(order.CustomerId, storeId);
				//if (customerRewardPointsBalance > 0)
				//	loyaltyPoints = customerRewardPointsBalance + " reward points available.";

				//postData.Append("\"firstName\":\"" + loyaltyPoints + "\",");
				//postData.Append("\"lastName\":\"" + order.Customer.GetFullName() + "\"");
				//postData.Append("}");


				var clientName = order.Customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.FirstName);
				var clientLastName = order.Customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
				{
					//clientName = order.CheckoutAttributeDescription;
					var dataArray = order.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1].Trim() + " | " + dataArray[3].Trim();
					clientLastName = order.Customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);

				}
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating customer address on LS for order id #" + order.Id, exception.Message + "  ::  " + exception.StackTrace, currentCustomer);

				_customCodeService.SendErrorEmail("Error updating customer address on LS for order id #" + order.Id, exception.Message, exception.StackTrace);
				return;
			}
		}

		protected void SendNotificationsAndSaveNotesApp(long orderId)
		{

			var customer = new Customer();
			try
			{
				var order = _orderService.GetOrderById(Convert.ToInt32(orderId));
				customer = _customerService.GetCustomerById(order.CustomerId);

				_logger.InsertLog(LogLevel.Information, "Order Notification Email from App : " + orderId);

				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId,
						"Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, customer);

					_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order ::  " + orderId, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				//send email notifications
				var orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
				if (orderPlacedStoreOwnerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to store owner) has been queued. Queued email identifier: {0}.", orderPlacedStoreOwnerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}

				var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 _pdfService.PrintOrderToPdf(order) : null;
				var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 "order.pdf" : null;
				var orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService
					 .SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
				if (orderPlacedCustomerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: " + orderId,
					ex.Message + "  ||   " + ex.StackTrace, customer);
				_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order :: " + orderId, ex.Message, ex.StackTrace);

				return;
			}
		}

		public void UpdateCustomerNameOnLightSpeed(int cmsOrderId, CmsCustomerNopCustomerMapping checkCmsCustomer, Nop.Core.Domain.Customers.Customer customer, Nop.Core.Domain.Orders.Order nopOrder)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerNameOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + customer.Email + "\",");

				var clientName = customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.FirstName);
				var clientLastName = customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(nopOrder.CheckoutAttributeDescription))
				{
					//clientName = nopOrder.CheckoutAttributeDescription;
					var dataArray = nopOrder.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1] + " | " + dataArray[3];

					clientLastName = customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
				}

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating Name for customer on Light Speed CMS :: LS OrderId :: " + cmsOrderId + " || Nop Order Id :: " + nopOrder.Id + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message + "  ::  " + exception.StackTrace);
			}
		}

		#endregion


		public ActionResult SetRedirectionStatus(int orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(orderId);

				var paymentInfo = new StarringJane.Worldline.Sips.Models.PaymentInfo(Request.Form);


				NameValueCollection form = new NameValueCollection();
				for (int i = 0; i < Request.Form.Keys.Count; i++)
				{
					form.Add(Request.Form.Keys[i], Server.UrlDecode(Request.Form[Request.Form.Keys[i]]));
					_logger.Information(Request.Form.Keys[i] + " " + form[Request.Form.Keys[i]]);
				}

				var paymentBrandName = string.Empty;
				var customer = order.Customer;
				var orderTransactionIDAttribute = _genericAttributeService.GetAttributesForEntity(order.Id, "Order").FirstOrDefault(x => x.Key == "TransactionID");
				if (orderTransactionIDAttribute != null)
				{
					this._logger.Information("[Worldline] Marking transactionId=" + paymentInfo.Data["transactionReference"] + " at time: " + DateTime.UtcNow);
				}
				if (paymentInfo.Data.Any())
				{
					var paymentMethod = paymentInfo.Data.FirstOrDefault(c => c.Key.ToLower() == "paymentmeanbrand");
					if (paymentMethod.Key != null && paymentMethod.Value != null)
					{
						paymentBrandName = paymentMethod.Value;
						switch (paymentMethod.Value.ToLower())
						{
							case "bcmc":

								ViewBag.AutoClose = "";
								break;

							default:
								ViewBag.AutoClose = "Dit scherm sluit automatisch in 5 seconden";
								break;

						}
					}
				}


				//if (paymentInfo.Data["responseCode"] == "00" || paymentInfo.Data["responseCode"] == "60")
				if (paymentInfo.Data["responseCode"] == "00")
				{
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
					if (!string.IsNullOrEmpty(paymentBrandName))
					{
						if (paymentBrandName.ToLower() == "bcmc")
							return View("~/Plugins/WebApi/Views/OrderStatus.cshtml");
					}

					return View("~/Plugins/WebApi/Views/OrderStatusOther.cshtml");
					// Order successful (accepted or pending)										
				}
				else if (paymentInfo.Data["responseCode"] == "17")
				{
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
					_logger.InsertLog(LogLevel.Error, "SIPS error after payment  for nop order :: " + orderId, ExceptionDescription(paymentInfo.Data["responseCode"]) + "  ||   " + order.Customer.Email);

					// Order cancelled
					return View("~/Plugins/WebApi/Views/OrderCancelled.cshtml");
				}
				else
				{
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
					_logger.InsertLog(LogLevel.Error, "SIPS error after payment  for nop order :: " + orderId, ExceptionDescription(paymentInfo.Data["responseCode"]) + "  ||   " + order.Customer.Email);

					return View("~/Plugins/WebApi/Views/MobileException.cshtml");
					// You can find more information on the response codes in the appendix
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "SIPS error after payment success for nop order :: " + orderId, ex.Message + "  ||   " + ex.StackTrace);
				_customCodeService.SendErrorEmail("SIPS error after payment success for nop order ::  " + orderId, ex.Message, ex.StackTrace);
				ViewBag.Message = ex.Message;
				return View("~/Plugins/WebApi/Views/MobileException.cshtml");
			}
		}


		public ActionResult SetRedirectionStatusAndroid(int orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(orderId);
				var paymentBrandName = string.Empty;
				var paymentInfo = new StarringJane.Worldline.Sips.Models.PaymentInfo(Request.Form);

				var customer = order.Customer;
				var orderTransactionIDAttribute = _genericAttributeService.GetAttributesForEntity(order.Id, "Order").FirstOrDefault(x => x.Key == "TransactionID");
				if (orderTransactionIDAttribute != null)
				{
					this._logger.Information("[Worldline] Marking transactionId=" + paymentInfo.Data["transactionReference"] + " at time: " + DateTime.UtcNow);
				}
				if (paymentInfo.Data.Any())
				{
					var paymentMethod = paymentInfo.Data.FirstOrDefault(c => c.Key.ToLower() == "paymentmeanbrand");
					if (paymentMethod.Key != null && paymentMethod.Value != null)
					{
						paymentBrandName = paymentMethod.Value;
						switch (paymentMethod.Value.ToLower())
						{
							case "bcmc":
								ViewBag.AutoClose = "";
								break;
							default:
								ViewBag.AutoClose = "Dit scherm sluit automatisch in 5 seconden";
								break;

						}
					}
				}


				//if (paymentInfo.Data["responseCode"] == "00" || paymentInfo.Data["responseCode"] == "60")
				if (paymentInfo.Data["responseCode"] == "00")
				{
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
					_logger.InsertLog(LogLevel.Information, "Redirecting after payment to our site:: " + orderId);

					//if (!string.IsNullOrEmpty(paymentBrandName))
					//{
					//	if (paymentBrandName.ToLower() == "bcmc")
					//	{
					//		_logger.InsertLog(LogLevel.Information, "Redirecting from Bancontact App:: " + orderId);
					//		return View("~/Plugins/WebApi/Views/OrderStatusAndroidOther.cshtml");
					//	}
					//}

					return View("~/Plugins/WebApi/Views/OrderStatusAndroid.cshtml");
					// Order successful (accepted or pending)				

				}
				else if (paymentInfo.Data["responseCode"] == "17")
				{
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
					_logger.InsertLog(LogLevel.Error, "SIPS error after payment  for nop order :: " + orderId, ExceptionDescription(paymentInfo.Data["responseCode"]) + "  ||   " + order.Customer.Email);

					// Order cancelled
					return View("~/Plugins/WebApi/Views/OrderCancelledAndroid.cshtml");
				}
				else
				{
					ViewBag.Message = ExceptionDescription(paymentInfo.Data["responseCode"]);
					_logger.InsertLog(LogLevel.Error, "SIPS error after payment  for nop order :: " + orderId, ExceptionDescription(paymentInfo.Data["responseCode"]) + "  ||   " + order.Customer.Email);

					return View("~/Plugins/WebApi/Views/MobileExceptionAndroid.cshtml");
					// You can find more information on the response codes in the appendix
				}
			}

			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "SIPS error after payment success for nop order :: " + orderId, ex.Message + "  ||   " + ex.StackTrace);
				_customCodeService.SendErrorEmail("SIPS error after payment success for nop order ::  " + orderId, ex.Message, ex.StackTrace);
				ViewBag.Message = ex.Message;
				return View("~/Plugins/WebApi/Views/MobileException.cshtml");
			}
		}




		public ActionResult UpdateOrderStatusIfOrderZero(int orderId)
		{
			var order = _orderService.GetOrderById(orderId);
			if (order != null)
			{
				//LightSpeedOrderStatusUpdate(orderId);

				//LightSpeedOrderStatusUpdate(orderId);
				//LightSpeedOrderCreate(orderId);
				//_logger.InsertLog(LogLevel.Information, "Order pushed to LS from App :: " + orderId);
				//notifications//Custom Code // Notification to be send after payment success	 
				//SendNotificationsAndSaveNotesApp(orderId);
				//_logger.InsertLog(LogLevel.Information, "Order Notification Sent  from App:: " + orderId);
				return Json(new { success = true, message = "Order Updated On LS" }, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
			}
		}


		#region Direct mode redirection

		public object SetDirectRedirection(string data, string seal, int orderId)
		{
			try
			{

				var base64EncodedBytes = System.Convert.FromBase64String(data);
				var dataString = System.Text.Encoding.UTF8.GetString(base64EncodedBytes); 				
				var dataArray = dataString.Split('|');

				var redirectionDataA = dataArray.FirstOrDefault(x => x.Contains("redirectionData")).Split('=');
				var transactionReferenceA = dataArray.FirstOrDefault(x => x.Contains("transactionReference")).Split('=');


				string redirectionData = redirectionDataA[1];
				string transactionReference = transactionReferenceA[1];

				//if (!String.IsNullOrEmpty(redirectData))
				//{

				string urlt = @"https://office-server.sips-atos.com/rs-services/v2/checkout/paymentProviderFinalize";
				string postDatat = "{\"interfaceVersion\":\"IR_WS_2.11\", \"keyVersion\":\"1\", \"merchantId\":\"225005036700001\",";
				postDatat = postDatat + "\"messageVersion\":\"0.1\", \"redirectionData\":\"" + redirectionData + "\",";
				string vsealData = @"IR_WS_2.112250050367000010.1" + redirectionData + transactionReference;
				var vseal = ReadSeal(vsealData, "PCv4DO5zG-pBOIfjojGnslk1Q0jG7Wd5iVHHLhk2XXk");

				postDatat = postDatat + "\"transactionReference\":\"" + transactionReference + "\", \"seal\":\"" + vseal + "\"}";

				var requestt = (HttpWebRequest)WebRequest.Create(urlt);
				requestt.ContentType = "application/json";
				requestt.Method = "POST";
				requestt.ContentLength = postDatat.Length;
				requestt.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWritert = new StreamWriter(requestt.GetRequestStream());
				requestWritert.Write(postDatat);
				requestWritert.Close();

				var responseReadert = new StreamReader(requestt.GetResponse().GetResponseStream());
				var responseDatat = responseReadert.ReadToEnd();

				//_logger.InsertLog(LogLevel.Information, "step3 received on :: " + DateTime.Now, responseDatat);
				_logger.InsertLog(LogLevel.Information, "orderId=" + orderId + "| ResponseData" + responseDatat);

				JavaScriptSerializer jsSerializer2 = new JavaScriptSerializer();
				var resulttest = jsSerializer2.DeserializeObject(responseDatat);
				Dictionary<string, object> objtest = new Dictionary<string, object>();
				objtest = (Dictionary<string, object>)(resulttest);

				string responseCode = objtest["responseCode"].ToString();

				_logger.InsertLog(LogLevel.Information, "Finalizerequest Response Code :: " + DateTime.Now, responseCode);

				if (responseCode == "00")
				{

					//string requestUrl = HttpContext.Request.RawUrl;
					var order = _orderService.GetOrderById(orderId);
					if (order != null)
					{
						order.OrderStatusId = (int)OrderStatus.Processing;
						order.PaymentStatusId = (int)PaymentStatus.Paid;
						_orderService.UpdateOrder(order);
						var paymentTypeId = 65536;
						var paymentTypeTypeId = 801;




						//LightSpeedOrderStatusUpdate(orderId);

						LightSpeedOrderCreateBCMC(orderId, paymentTypeId, paymentTypeTypeId);
						_logger.InsertLog(LogLevel.Information, "Order pushed to LS from App :: " + orderId);
						//notifications//Custom Code // Notification to be send after payment success	 
						SendNotificationsAndSaveNotesApp(orderId);
						_logger.InsertLog(LogLevel.Information, "Order Notification Sent  from App:: " + orderId);

					}
					return View("~/Plugins/WebApi/Views/OrderStatusAndroid.cshtml");

				}
				else
				{
					//	return "";
					ViewBag.Message = ExceptionDescription(responseCode);
					return View("~/Plugins/WebApi/Views/MobileExceptionAndroid.cshtml");
				}



			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Bancontact Direct mode  :: " + ex.Message + "  ||   " + ex.StackTrace);
				ViewBag.Message = ex.Message;
				return View("~/Plugins/WebApi/Views/MobileException.cshtml");
			}

		}
		#endregion


		private string ByteArrayToHEX(byte[] ba)
		{
			StringBuilder hex = new StringBuilder(ba.Length * 2);
			foreach (byte b in ba)
				hex.AppendFormat("{0:x2}", b);
			return hex.ToString();
		}


		public string ReadSeal(string data, string key)
		{
			string sealAlgorithm = "HMAC-SHA-256";
			var rr = "";
			UTF8Encoding utF8Encoding = new UTF8Encoding();
			byte[] bytes = utF8Encoding.GetBytes(data);
			HMAC hmac = sealAlgorithm == "HMAC-SHA-1" ? (HMAC)new HMACSHA1() : (sealAlgorithm == "HMAC-SHA-384" ? (HMAC)new HMACSHA384() : (sealAlgorithm == "HMAC-SHA-512" ? (HMAC)new HMACSHA512() : (HMAC)new HMACSHA256()));
			hmac.Key = utF8Encoding.GetBytes(key);
			hmac.Initialize();
			return this.ByteArrayToHEX(hmac.ComputeHash(bytes));
		}


		#region LS Direct Mode
		public void LightSpeedOrderCreateBCMC(long orderId, int paymentTypeId, int paymentTypeTypeId)
		{
			#region Push Order to CMS
			var currentCustomer = new Customer();
			try
			{
				var order = _orderService.GetOrderById(Convert.ToInt32(orderId));
				currentCustomer = _customerService.GetCustomerById(order.CustomerId);

				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order not found in webshop :: " + orderId,
						 "Order push to cms :: Error :: Order not found in webshop :: " + orderId, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId, "Order push to cms :: Error :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				if (order.PaymentStatus != PaymentStatus.Paid)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, "Current payment status :: " + order.PaymentStatus);

					return;
				}
				var cmsNopOrder = _cmsNopOrderMappingService.GetByNopOrderId(order.Id);
				if (cmsNopOrder != null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id + " :: LS Order Id :: " + cmsNopOrder.CmsOrderId,
						 "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId + " :: LS Order Id :: " + cmsNopOrder.CmsOrderId,
						 "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id, string.Empty);
					return;
				}


				var checkCmsCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
				if (checkCmsCustomer == null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, string.Empty);
					return;
				}

				string token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, string.Empty);
					return;
				}

				//string url = @"http://staging-exact-integration.posios.com/PosServer/rest/onlineordering/customer/" + checkCmsCustomer.CmsCustomerId + "/order";
				string url = @"https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + checkCmsCustomer.CmsCustomerId + "/order";
				var postData = new StringBuilder();
				postData.Append("{\"customerId\":" + checkCmsCustomer.CmsCustomerId + ",");

				var currentData = DateTime.Now;
				string newDate = currentData.Year + "-" + currentData.Month + "-" + currentData.Day + "T" + currentData.Hour + ":" +
									  currentData.Minute + ":" + currentData.Second + "+00:00";
				postData.Append("\"deliveryDate\":\"" + newDate + "\",");

				var orderType = GetOrderType(order);
				postData.Append("\"type\":\"" + orderType + "\",");
				postData.Append("\"status\":\"" + "ACCEPTED" + "\",");
				postData.Append("\"description\":\"" + "order" + "\",");

				if (orderType.ToLower() == "delivery")
				{
					var deliveryTime = string.Empty;
					var deliveryTimeDescripton = order.CheckoutAttributeDescription.Substring(order.CheckoutAttributeDescription.LastIndexOf("Gev"));

					if (!string.IsNullOrEmpty(deliveryTimeDescripton))
					{
						deliveryTime = deliveryTimeDescripton;
					}

					postData.Append("\"note\":\"" + "Order Reference: " + order.Id + " || " + deliveryTime + "\",");
				}
				else
					postData.Append("\"note\":\"" + "Order Reference: " + order.Id + "\",");

				postData.Append("\"orderItems\":[");
				var totalItems = order.OrderItems.Count;
				var counter = 0;
				foreach (var orderItem in order.OrderItems)
				{
					counter++;
					var cmsProduct = _cmsNopProductMappingService.GetByNopProductId(orderItem.ProductId);
					if (cmsProduct == null)
					{
						_logger.InsertLog(LogLevel.Error, "Error :: Order sync with CMS :: Nop Order Id :: " + orderItem.OrderId,
							 "Could not find mapped CMS product against Nop Product :: ID :: " + orderItem.ProductId + " :: Product Name :: " +
							 orderItem.Product.Name, currentCustomer);
						break;
					}

					postData.Append("{\"productId\":" + cmsProduct.CmsProductId + ",");
					postData.Append("\"amount\":" + orderItem.Quantity + ",");

					var attributePriceDeduction = 0M;
					if (!String.IsNullOrEmpty(orderItem.AttributesXml))
					{
						postData.Append("\"modifiers\":[");
						var xmlDoc = new XmlDocument();
						xmlDoc.LoadXml(orderItem.AttributesXml);

						var nodeCounter = 0;
						var attrCounter = 0;
						var nodeList = xmlDoc.SelectNodes(@"//Attributes/ProductAttribute");

						foreach (XmlNode node in xmlDoc.SelectNodes(@"//Attributes/ProductAttribute"))
						{
							nodeCounter++;
							if (node.Attributes != null && node.Attributes["ID"] != null)
							{
								int attributeId;
								var nopAttributeValueId = node.InnerText;
								if (!string.IsNullOrEmpty(nopAttributeValueId))
								{
									var nopCmsProductAttributeValueMapping = _cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(Convert.ToInt32(nopAttributeValueId));

									var nopCmsProductAttributeMapping = new NopCmsProductAttributeMapping();
									if (nopCmsProductAttributeValueMapping != null)
									{
										nopCmsProductAttributeMapping = _cmsNopProductMappingService.GetNopCmsProductAttributeById(nopCmsProductAttributeValueMapping.NopCmsProductAttributeMappingId);
									}

									if (attrCounter > 0)
										postData.Append(",{");
									else
										postData.Append("{");

									if (nopCmsProductAttributeMapping != null)
									{
										postData.Append("\"description\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");
										postData.Append("\"modifierId\":" + nopCmsProductAttributeMapping.CmsProductAttributeId + ",");
										postData.Append("\"modifierName\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");

									}
									foreach (XmlNode attributeValue in node.SelectNodes("ProductAttributeValue"))
									{
										var value = attributeValue.SelectSingleNode("Value").InnerText.Trim();
										if (!String.IsNullOrEmpty(value))
										{
											var cmsProductAttributeValue = _cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(Convert.ToInt32(value));
											if (cmsProductAttributeValue != null)
											{
												postData.Append("\"modifierValueId\":" + cmsProductAttributeValue.CmsProductAttributeValueMappingId + ",");
												postData.Append("\"price\":" + cmsProductAttributeValue.Price + ",");
												postData.Append("\"priceWithoutVat\":" + cmsProductAttributeValue.Price);
												attributePriceDeduction += cmsProductAttributeValue.Price;
											}
										}
									}

									postData.Append("}");
									attrCounter++;
								}
							}
						}
						postData.Append("],");
					}
					var orderItemPrice = orderItem.PriceInclTax - attributePriceDeduction;
					postData.Append("\"totalPrice\":" + orderItemPrice + ",");
					postData.Append("\"totalPriceWithoutVat\":" + orderItemPrice + ",");
					postData.Append("\"unitPrice\":" + orderItemPrice + ",");
					postData.Append("\"unitPriceWithoutVat\":" + orderItemPrice);
					postData.Append(counter >= totalItems ? "}" : "},");
				}
				//Temp Testing
				//postData.Append("],\"orderPayments\":[{");
				//postData.Append("\"amount\":" + order.OrderTotal + ",");
				//postData.Append("\"paymentTypeId\":" + 39893 + ",");
				//postData.Append("\"paymentTypeTypeId\":" + 4 + "}");


				//var paymentTypeId = 0;
				//	var paymentTypeTypeId = 0;
				var paymentOption = string.Empty;

				//GetLSPaymentInformation(paymentInfo, ref paymentTypeId, ref paymentTypeTypeId, ref paymentOption);

				if (paymentTypeId == 0 || paymentTypeTypeId == 0)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId,
						 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, currentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, string.Empty);
					return;
				}

				postData.Append("],\"orderPayments\":[{");
				postData.Append("\"amount\":" + order.OrderTotal + ",");
				postData.Append("\"paymentTypeId\":" + paymentTypeId + ",");
				postData.Append("\"paymentTypeTypeId\":" + paymentTypeTypeId + "}");



				if (order.RedeemedRewardPointsEntry != null)
				{
					//Use Credit points
					//postData.Append(",{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
					//postData.Append("\"paymentTypeId\":" + 5208 + ",");
					//postData.Append("\"paymentTypeTypeId\":" + 15 + "}");

					//Use Cash
					postData.Append(",{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
					postData.Append("\"paymentTypeId\":" + 36191 + ",");
					postData.Append("\"paymentTypeTypeId\":" + 200 + "}");
				}



				postData.Append("],\"orderTaxInfo\":[{");
				postData.Append("\"tax\":" + "0" + ",");
				postData.Append("\"taxRate\":" + "0" + ",");
				postData.Append("\"totalWithTax\":" + order.OrderTotal + ",");
				postData.Append("\"totalWithoutTax\":" + order.OrderTotal);
				postData.Append("}]}");

				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Order#: " + order.Id, postData.ToString(), currentCustomer);

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					if (orderType.ToLower() == "delivery")
					{
						UpdateCustomerAddressOnLightSpeed(order, checkCmsCustomer);
					}
					else
					{
						UpdateCustomerNameOnLightSpeed(Convert.ToInt32(responseData), checkCmsCustomer, order.Customer, order);
					}


					var cmsOrderNopOrderMap = new CmsOrderNopOrderMapping
					{
						NopOrderId = order.Id,
						CmsOrderId = Convert.ToInt32(responseData),
						CreatedOnUtc = DateTime.UtcNow,
						UpdatedOnUtc = DateTime.UtcNow
					};

					_cmsNopOrderMappingService.Insert(cmsOrderNopOrderMap);
					order.CustomOrderNumber = responseData;
					_orderService.UpdateOrder(order);
				}

			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error Creating Order on CMS for nop order :: " + orderId,
					 ex.Message + "  ||   " + ex.StackTrace, currentCustomer);

				_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId, ex.Message, ex.StackTrace);

				return;
			}
			#endregion
		}

		#endregion


		public ActionResult GotoMobileApp()
		{
			return View("~/Plugins/WebApi/Views/OrderStatusAndroid.cshtml");
		}

		#region Payment

		public ActionResult PaymentMethod(int userId)
		{
			var currentCustomer = _customerService.GetCustomerById(userId);
			int storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			//validation
			var cart = currentCustomer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				 .LimitPerStore(storeId)
				 .ToList();
			if (!cart.Any())
				return Json(new { success = false, message = "Cart is Empty." }, JsonRequestBehavior.AllowGet);


			if (currentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
				return Json(new { success = false, message = "Please Login to Proceed Further" }, JsonRequestBehavior.AllowGet);

			//Check whether payment workflow is required
			//we ignore reward points during cart total calculation
			bool isPaymentWorkflowRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart, false);
			if (!isPaymentWorkflowRequired)
			{
				_genericAttributeService.SaveAttribute<string>(currentCustomer,
					 SystemCustomerAttributeNames.SelectedPaymentMethod, null, storeId);
				return Json(new { success = true, message = "Go to Payment Details" }, JsonRequestBehavior.AllowGet);
			}

			//filter by country
			int filterByCountryId = 0;
			if (_addressSettings.CountryEnabled &&
				 currentCustomer.BillingAddress != null &&
				 currentCustomer.BillingAddress.Country != null)
			{
				filterByCountryId = currentCustomer.BillingAddress.Country.Id;
			}

			//model
			var paymentMethodModel = _checkoutModelFactory.PreparePaymentMethodModel(cart, filterByCountryId);

			//if (_paymentSettings.BypassPaymentMethodSelectionIfOnlyOne &&
			//	 paymentMethodModel.PaymentMethods.Count == 1 && !paymentMethodModel.DisplayRewardPoints)
			//{
			//		_genericAttributeService.SaveAttribute(currentCustomer,
			//		 SystemCustomerAttributeNames.SelectedPaymentMethod,
			//		 paymentMethodModel.PaymentMethods[0].PaymentMethodSystemName,
			//		 storeId);	 				
			//	return Json(new { success = true, message = "", data = paymentMethodModel }, JsonRequestBehavior.AllowGet);
			//}

			var sipsPaymentMethod = paymentMethodModel.PaymentMethods.FirstOrDefault(x => x.PaymentMethodSystemName.ToLower() == "payments.worldline");
			var attributeList = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, "Customer");
			if (attributeList.Any())
			{
				var genericAttribute = attributeList.FirstOrDefault(x => x.Key == "AuthorizationTypeId");
				if (genericAttribute == null)
				{
					_genericAttributeService.SaveAttribute(currentCustomer,
						SystemCustomerAttributeNames.SelectedPaymentMethod,
						sipsPaymentMethod.PaymentMethodSystemName,
						storeId);
				}
				else
				{
					int languageid = 1;
					languageid = _languageService.GetAllLanguages().FirstOrDefault().Id;

					var loyaltyPaymentMethod = paymentMethodModel.PaymentMethods.FirstOrDefault(x => x.PaymentMethodSystemName.ToLower() == "payments.loyaltypoints");
					paymentMethodModel.PaymentMethods.Remove(loyaltyPaymentMethod);
					if (genericAttribute.Value == "2")
					{
						var invoicePay = _paymentService.LoadPaymentMethodBySystemName("Payments.Invoice");
						if (invoicePay != null)
						{
							var pmModel = new CheckoutPaymentMethodModel.PaymentMethodModel
							{
								Name = invoicePay.GetLocalizedFriendlyName(_localizationService, languageid),
								Description = _paymentSettings.ShowPaymentMethodDescriptions ? invoicePay.PaymentMethodDescription : string.Empty,
								PaymentMethodSystemName = invoicePay.PluginDescriptor.SystemName,
								LogoUrl = invoicePay.PluginDescriptor.GetLogoUrl(_webHelper)
							};

							paymentMethodModel.PaymentMethods.Add(pmModel);
						}

						//var InvoicePaymentMethod = paymentMethodModel.PaymentMethods.FirstOrDefault(x => x.PaymentMethodSystemName.ToLower() == "payments.invoice");
						
					}
					else
					{
						_genericAttributeService.SaveAttribute(currentCustomer,
							SystemCustomerAttributeNames.SelectedPaymentMethod,
							sipsPaymentMethod.PaymentMethodSystemName,
							storeId);
					}


				}
			}
			else
			{
				_genericAttributeService.SaveAttribute(currentCustomer,
					 SystemCustomerAttributeNames.SelectedPaymentMethod,
					 sipsPaymentMethod.PaymentMethodSystemName,
					 storeId);
			}

			//  return View(paymentMethodModel);
			return Json(new { success = true, message = "", data = paymentMethodModel }, JsonRequestBehavior.AllowGet);
		}



		public ActionResult SaveSelectedPaymentMethod(string paymentmethod, int userId)
		{

			var currentCustomer = _customerService.GetCustomerById(userId);
			int storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			//validation
			var cart = currentCustomer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				 .LimitPerStore(storeId)
				 .ToList();
			if (!cart.Any())
				return Json(new { success = false, message = "Cart is Empty" }, JsonRequestBehavior.AllowGet);

			if (currentCustomer.IsGuest() && !_orderSettings.AnonymousCheckoutAllowed)
				return Json(new { success = false, message = "Please Login to Proceed Further" }, JsonRequestBehavior.AllowGet);


			//Check whether payment workflow is required
			bool isPaymentWorkflowRequired = _orderProcessingService.IsPaymentWorkflowRequired(cart);
			if (!isPaymentWorkflowRequired)
			{
				_genericAttributeService.SaveAttribute<string>(currentCustomer,
					 SystemCustomerAttributeNames.SelectedPaymentMethod, null, storeId);
				return Json(new { success = true, message = "Go to Payment Details" }, JsonRequestBehavior.AllowGet);
			}
			//payment method 
			if (String.IsNullOrEmpty(paymentmethod))
				return Json(new { success = false, message = "Payment Methods is Empty" }, JsonRequestBehavior.AllowGet);
			// return PaymentMethod();

			var paymentMethodInst = _paymentService.LoadPaymentMethodBySystemName(paymentmethod);
			if (paymentMethodInst == null ||
				 !paymentMethodInst.IsPaymentMethodActive(_paymentSettings) ||
				 !_pluginFinder.AuthenticateStore(paymentMethodInst.PluginDescriptor, storeId) ||
				 !_pluginFinder.AuthorizedForUser(paymentMethodInst.PluginDescriptor, currentCustomer))
				return Json(new { success = false, message = "Go to Payment Methods" }, JsonRequestBehavior.AllowGet);

			var attributeList = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, "Customer");
			if (attributeList.Any())
			{
				var genericAttribute = attributeList.FirstOrDefault(x => x.Key == "AuthorizationTypeId");
				if (genericAttribute != null)
				{
					_genericAttributeService.SaveAttribute(currentCustomer,
				 SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, storeId);
					return Json(new { success = true, message = "Go to Payment Details"}, JsonRequestBehavior.AllowGet);
				}
			}
			//save
			_genericAttributeService.SaveAttribute(currentCustomer,
				 SystemCustomerAttributeNames.SelectedPaymentMethod, paymentmethod, storeId);

			return Json(new { success = true, message = "Go to Payment Details" }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Payzen methods

		public ActionResult SetPayzenRedirection(int orderId)
		{
			string requestUrl = HttpContext.Request.RawUrl;
			var order = _orderService.GetOrderById(orderId);
			var paymentInfo = new StarringJane.Worldline.Sips.Models.PaymentInfo(Request.Form);
			return Json(new { success = true, message = "got it" }, JsonRequestBehavior.AllowGet);

		}

		#endregion

	}
}
