﻿using System.Web.Mvc;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Topics;
using Nop.Plugin.WebApi.Factories;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;

namespace Nop.Plugin.WebApi.Controllers
{
	public partial class ApiTopicController : BasePluginController
    {
        #region Fields

        private readonly ITopicModelFactory _topicModelFactory;
        private readonly ITopicService _topicService;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IAclService _aclService;
        private readonly IPermissionService _permissionService;

        #endregion

        #region Constructors

        public ApiTopicController(ITopicModelFactory topicModelFactory,
            ITopicService topicService,
            ILocalizationService localizationService,
            IStoreMappingService storeMappingService,
            IAclService aclService,
            IPermissionService permissionService)
        {
            this._topicModelFactory = topicModelFactory;
            this._topicService = topicService;
            this._localizationService = localizationService;
            this._storeMappingService = storeMappingService;
            this._aclService = aclService;
            this._permissionService = permissionService;
        }

        #endregion

        #region Methods


		  public virtual ActionResult TopicDetails(int topicId)
		  {
			  var model = _topicModelFactory.PrepareTopicModelById(topicId);
			  if (model == null)
				  return Json(new { success = false, message = "No Topics Found" }, JsonRequestBehavior.AllowGet);

			  //template
			  //var templateViewPath = _topicModelFactory.PrepareTemplateViewPath(model.TopicTemplateId);
			  //return View(templateViewPath, model);
			  //return Json(model, JsonRequestBehavior.AllowGet);
			  return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);


		  }


		  //public virtual ActionResult TopicDetailsPopup(string systemName)
		  //{
		  //	 var model = _topicModelFactory.PrepareTopicModelBySystemName(systemName);
		  //	 if (model == null)
		  //		  return RedirectToRoute("HomePage");

		  //	 ViewBag.IsPopup = true;

		  //	 //template
		  //	 var templateViewPath = _topicModelFactory.PrepareTemplateViewPath(model.TopicTemplateId);
		  //	 return PartialView(templateViewPath, model);
		  //}

        [ChildActionOnly]
		  public  ActionResult TopicBlock(string systemName)
		  {
			  var model = _topicModelFactory.PrepareTopicModelBySystemName(systemName);
			  if (model == null)
			  {
				  return Json(new { success = false, message = "" }, JsonRequestBehavior.AllowGet);
				  //return Json("", JsonRequestBehavior.AllowGet);
			  }
			  //return Json(model, JsonRequestBehavior.AllowGet);

			  return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
			  // return PartialView(model);
		  }

     
        #endregion
    }
}
