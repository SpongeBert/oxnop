﻿using System.Web.Mvc;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Plugin.WebApi.Factories;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security.Captcha;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Localization;
using Nop.Plugin.WebApi.Models.Blogs;
using Nop.Services.Blogs;
using System;
using Nop.Services.Stores;

namespace Nop.Web.Controllers
{
	public partial class ApiBlogController : BasePluginController
	{
		#region Fields

		private readonly IBlogService _blogService;			
		private readonly ILocalizationService _localizationService;	 		
		private readonly IBlogModelFactory _blogModelFactory;
		private readonly BlogSettings _blogSettings;
		private readonly LocalizationSettings _localizationSettings;
		private readonly IStoreMappingService _storeMappingService;

		#endregion

		#region Constructors

		public ApiBlogController(IBlogService blogService,		
			ILocalizationService localizationService,	
			IBlogModelFactory blogModelFactory,				
				BlogSettings blogSettings,
				LocalizationSettings localizationSettings,
			IStoreMappingService storeMappingService
				)
		{
			this._blogService = blogService;	
			this._localizationService = localizationService;
			this._storeMappingService = storeMappingService;
			this._blogModelFactory = blogModelFactory;				
			this._blogSettings = blogSettings;
			this._localizationSettings = localizationSettings;
			
		}

		#endregion

		#region Methods


		  public virtual ActionResult BlogLists(BlogPagingFilteringModel command)
        {
            if (!_blogSettings.Enabled)
                return RedirectToRoute("HomePage");

            var model = _blogModelFactory.PrepareBlogPostListModel(command);
				if (model == null)
				{
					return Json(new { success = false, message = "No Blog found." }, JsonRequestBehavior.AllowGet);
				} 

				return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
            //return View("List", model);
        }

		  public virtual ActionResult BlogPostDetails(int blogPostId)
		  {
			  if (!_blogSettings.Enabled)
				  return Json(new { success = false, message = "Blog setting is disabled" }, JsonRequestBehavior.AllowGet);
				  //return RedirectToRoute("HomePage");

			  var blogPost = _blogService.GetBlogPostById(blogPostId);
			  if (blogPost == null ||
					(blogPost.StartDateUtc.HasValue && blogPost.StartDateUtc.Value >= DateTime.UtcNow) ||
					(blogPost.EndDateUtc.HasValue && blogPost.EndDateUtc.Value <= DateTime.UtcNow))
				  return Json(new { success = true, message = "No Posts" }, JsonRequestBehavior.AllowGet);

			  //Store mapping
			  if (!_storeMappingService.Authorize(blogPost))
				  return Json(new { success = true, message = "Not authorized"}, JsonRequestBehavior.AllowGet);

			  
			  var model = new BlogPostModel();
			  _blogModelFactory.PrepareBlogPostModel(model, blogPost, true);

			  //return View(model);
			  return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		  }
		

		#endregion
	}
}
