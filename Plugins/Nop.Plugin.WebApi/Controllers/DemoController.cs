﻿using System;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.WebApi.Controllers
{
	public class DemoController : BasePluginController
	{
		public ActionResult Test()
		{
			return Content("ok");
		}
		public ActionResult Auth(string id, string pwd)
		{
			string token = Guid.NewGuid().ToString();
			HttpRuntime.Cache.Add(id, token, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(30), CacheItemPriority.NotRemovable, null);
			//return token;
			return Json(new { success = true, message = "", data = token }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult Check(string id, string token)
		{
			var col = HttpRuntime.Cache;
			foreach (var item in col)
			{
				var str = item;
			}
			if (HttpRuntime.Cache[id] != null)
			{
				string verifyToken = HttpRuntime.Cache[id].ToString();
				if (verifyToken == token)
					return Json(new { success = true, message = "ok" }, JsonRequestBehavior.AllowGet);
				return Json(new { success = false, message = "false" }, JsonRequestBehavior.AllowGet);
			}

			return Json(new { success = false, message = "false" }, JsonRequestBehavior.AllowGet);
		}

	}
}
