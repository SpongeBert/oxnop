﻿using Nop.Core.Domain.Orders;
using Nop.Plugin.WebApi.Factories;
using Nop.Plugin.WebApi.Models.ShoppingCart;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Web.Framework.Controllers;
using System.Web.Mvc;
using System.Linq;
using System.Web.Script.Serialization;
using Nop.Services.Catalog;
using System.Collections.Generic;
using System;
using Nop.Services.Stores;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Core;
using Nop.Plugin.WebApi.Models.CheckoutProcess;
using Nop.Plugin.WebApi.Models.Media;
using Nop.Services.Security;
using Nop.Plugin.WebApi.Infrastructure.Cache;
using Nop.Services.Tax;
using Nop.Services.Media;
using Nop.Core.Domain.Media;
using Nop.Services.Shipping.Date;
using Nop.Services.Directory;
using Nop.Core.Caching;

namespace Nop.Plugin.WebApi.Controllers
{
	public class ApiShoppingCartController : BasePluginController
	{
		#region fields
		private readonly ICustomerService _customerService;
		private readonly IProductService _productService;
		private readonly IShoppingCartService _shoppingCartService;
		private readonly IStoreService _storeService;
		private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
		private readonly ICheckoutAttributeService _checkoutAttributeService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ShoppingCartSettings _shoppingCartSettings;
		private readonly IProductAttributeService _productAttributeService;
		private readonly ICheckoutAttributeParser _checkoutAttributeParser;
		private readonly IProductAttributeParser _productAttributeParser;
		private readonly IDiscountService _discountService;
		private readonly ILocalizationService _localizationService;
		private readonly IRewardPointService _rewardPointService;


       private readonly ITaxService _taxService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IPriceFormatter _priceFormatter;
		  private readonly IPictureService _pictureService;
		  private readonly MediaSettings _mediaSettings;
		  private readonly IDateRangeService _dateRangeService;
		  private readonly ICurrencyService _currencyService;
		  private readonly IPermissionService _permissionService;
		  private readonly IDownloadService _downloadService;
	
		#endregion

		#region const

		public ApiShoppingCartController(ICustomerService customerService, IProductService productService, IShoppingCartService shoppingCartService, IStoreService storeService,
			IShoppingCartModelFactory shoppingCartModelFactory, ICheckoutAttributeService checkoutAttributeService, IGenericAttributeService genericAttributeService,
			ShoppingCartSettings shoppingCartSettings, IProductAttributeService productAttributeService, ICheckoutAttributeParser checkoutAttributeParser,
			IProductAttributeParser productAttributeParser,IDiscountService discountService, ILocalizationService localizationService, IRewardPointService rewardPointService,  ITaxService taxService,  
            IPriceCalculationService priceCalculationService,MediaSettings mediaSettings,	IPermissionService permissionService,
				IPriceFormatter priceFormatter, IPictureService pictureService, IDateRangeService dateRangeService, ICurrencyService currencyService, IDownloadService downloadService)
		{
			_customerService = customerService;
			_productService = productService;
			_shoppingCartService = shoppingCartService;
			_storeService = storeService;
			_shoppingCartModelFactory = shoppingCartModelFactory;
			_checkoutAttributeService = checkoutAttributeService;
			_genericAttributeService = genericAttributeService;
			_shoppingCartSettings = shoppingCartSettings;
			_productAttributeService = productAttributeService;
			_checkoutAttributeParser = checkoutAttributeParser;
			_productAttributeParser = productAttributeParser;
			_discountService = discountService;
			_localizationService = localizationService;
			_rewardPointService = rewardPointService;	
			
			 _taxService = taxService;
            _currencyService = currencyService;
            _priceCalculationService = priceCalculationService;
            _priceFormatter = priceFormatter;
				_pictureService = pictureService;
				_mediaSettings = mediaSettings;
				_dateRangeService = dateRangeService;
				_currencyService = currencyService;
				_permissionService = permissionService;
				_downloadService = downloadService;
				
		}

		#endregion

		#region  GET USER CART ITEMS

		public ActionResult GetUserCartItems(int userId)
		{
			var customer = _customerService.GetCustomerById(userId);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			var cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				 .LimitPerStore(storeId)
				 .ToList();

			if (!cart.Any())
				return Json(new { success = false, message = "Uw winkelwagen is leeg" }, JsonRequestBehavior.AllowGet);

			var model = new ShoppingCartModel();
			//  var model = new GetShoppingCartItemsModel(); 			
			model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}


		public ActionResult GetWishlistItems(int userId)
		{
			var customer = _customerService.GetCustomerById(userId);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			var cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
				 .LimitPerStore(storeId)
				 .ToList();

			if (!cart.Any())
				return Json(new { success = false, message = "Wishlist is empty" }, JsonRequestBehavior.AllowGet);

			var model = new WishlistModel();
			model = _shoppingCartModelFactory.PrepareWishlistModel(model, cart);
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region ADD TO CART

		public ActionResult AddToCart(string details)
		{
			var response = new JavaScriptSerializer().Deserialize<AddToShoppingCartModel>(details);

			var customer = _customerService.GetCustomerById(response.CustomerId);
			if (customer == null)
				return Json(new { success = false, message = "Customer Not Exist" }, JsonRequestBehavior.AllowGet);

			var product = _productService.GetProductById(response.productId);
			if (product == null)
				return Json(new { success = false, message = "Product Not Exist" }, JsonRequestBehavior.AllowGet);

			if (product.ProductType != ProductType.SimpleProduct)
			{
				return Json(new { success = false, message = "Only simple products could be added to the cart" }, JsonRequestBehavior.AllowGet);
			}

			var cartType = (ShoppingCartType)response.ShoppingCartType;
			var attXml = "";
			var quantity = response.EnteredQuantity;
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			var addToCartWarnings = _shoppingCartService.AddToCart(customer, product, cartType, storeId, attXml,
				quantity: quantity);

			return Json(new { success = true, message = "Item added to Cart Successfully" }, JsonRequestBehavior.AllowGet);
		}

		//[HttpPost]
		public ActionResult AddProductToCart_Details(string details)
		{
			
			// details = "{'Quantity':'1','ShoppingCartType':'1','CustomerId':'1',productId:'52',ProductAttributeValues:'ProductAttid:1,ProductAttVal:39;ProductAttid:8,ProductAttVal:40'}"; 	 		 	  
			//details = "{'Quantity':'1','ShoppingCartType':'2','CustomerId':'1',productId:'52',ProductAttributeValues:'ProductAttid:1,ProductAttVal:38;ProductAttid:8,ProductAttVal:40'}"; 	

			if (string.IsNullOrEmpty(details))
			{
				return Json(new { success = false, message = "Invalid request" }, JsonRequestBehavior.AllowGet);
			}

			var response = new JavaScriptSerializer().Deserialize<AddToShoppingCartModel>(details);

		
			var productattributes = response.ProductAttributeValues;
			//var allAttributes = new List<string>();

			//if (!string.IsNullOrEmpty(productattributes))
			//{
			//	var pattributes = productattributes.Split(';');
			//	foreach (var att in pattributes)
			//	{
			//		allAttributes.Add(att);
			//	}
			//}

			var _productAttributes = _productAttributeService.GetProductAttributeMappingsByProductId(response.productId).ToList();
			foreach (var attr in _productAttributes)
			{
				var _IsRequired = attr.IsRequired;
				var attrId = attr.Id;
				if (_IsRequired)
				{
					var input = "ProductAttid:" + attrId;
					var isExist = productattributes.Contains(input);
					if (!isExist)
					{
						return Json(new
						{
							success = false,
							message = "Please fill all the required fields."
						}, JsonRequestBehavior.AllowGet);
					}
				}

			}

			var shoptype = response.ShoppingCartType;

			var product = _productService.GetProductById(response.productId);
			if (product == null)
			{
				return Json(new { success = false, message = "No product found" }, JsonRequestBehavior.AllowGet);
			}

			//we can add only simple products
			if (product.ProductType != ProductType.SimpleProduct)
			{
				return Json(new
				{
					success = false,
					message = "Only simple products could be added to the cart"
				}, JsonRequestBehavior.AllowGet);
			}

			var customer = _customerService.GetCustomerById(response.CustomerId);
			if (customer == null)
				return Json(new{success = false,message = "Customer not found. Please try with valid customer."}, JsonRequestBehavior.AllowGet);

			var storeid = _storeService.GetAllStores().FirstOrDefault().Id;

			#region Update existing shopping cart item?

			var updatecartitemid = 0;	 // get from details
			ShoppingCartItem updatecartitem = null;
			if (_shoppingCartSettings.AllowCartItemEditing && updatecartitemid > 0)
			{
				//search with the same cart type as specified
				var cart = customer.ShoppingCartItems
					 .Where(x => x.ShoppingCartTypeId == shoptype)
					 .LimitPerStore(storeid)
					 .ToList();
				updatecartitem = cart.FirstOrDefault(x => x.Id == updatecartitemid);

				//is it this product?
				if (updatecartitem != null && product.Id != updatecartitem.ProductId)
				{
					return Json(new
					{
						success = false,
						message = "This product does not match a passed shopping cart item identifier"
					}, JsonRequestBehavior.AllowGet);
				}
			}

			#endregion



			#region Quantity

			var quantity = response.EnteredQuantity;

			#endregion

			var addToCartWarnings = new List<string>();

			//product and gift card attributes
			//var attributes = ParseProductAttributes(product, allAttributes, addToCartWarnings);
			var _list = new List<string>();
			_list.Add(response.ProductAttributeValues);
			string attributes = ParseProductAttributes(product, _list, addToCartWarnings);

			//rental attributes
			DateTime? rentalStartDate = null;
			DateTime? rentalEndDate = null;


			var cartType = updatecartitem == null ? (ShoppingCartType)shoptype :
				//if the item to update is found, then we ignore the specified "shoppingCartTypeId" parameter
				 updatecartitem.ShoppingCartType;

			//save item
			if (updatecartitem == null)
			{
				//add to the cart
				addToCartWarnings.AddRange(_shoppingCartService.AddToCart(customer,
					 product, cartType, storeid,
					 attributes, 0,
					 rentalStartDate, rentalEndDate, quantity, true));
			}
			else
			{
				var cart = customer.ShoppingCartItems
					 .Where(x => x.ShoppingCartType == updatecartitem.ShoppingCartType)
					 .LimitPerStore(storeid)
					 .ToList();
				var otherCartItemWithSameParameters = _shoppingCartService.FindShoppingCartItemInTheCart(
					 cart, updatecartitem.ShoppingCartType, product, attributes, 0,
					 rentalStartDate, rentalEndDate);
				if (otherCartItemWithSameParameters != null &&
					 otherCartItemWithSameParameters.Id == updatecartitem.Id)
				{
					//ensure it's some other shopping cart item
					otherCartItemWithSameParameters = null;
				}
				//update existing item
				addToCartWarnings.AddRange(_shoppingCartService.UpdateShoppingCartItem(customer,
					 updatecartitem.Id, attributes, 0,
					 rentalStartDate, rentalEndDate, quantity, true));
				if (otherCartItemWithSameParameters != null && !addToCartWarnings.Any())
				{
					//delete the same shopping cart item (the other one)
					_shoppingCartService.DeleteShoppingCartItem(otherCartItemWithSameParameters);
				}
			}

			#region Return result

			if (addToCartWarnings.Any())
			{
				//cannot be added to the cart/wishlist
				//let's display warnings
				return Json(new
				{
					success = false,
					message = addToCartWarnings.ToArray()
				}, JsonRequestBehavior.AllowGet);

			}


			if ((ShoppingCartType)response.ShoppingCartType == ShoppingCartType.Wishlist)
			{
				return Json(new { success = true, message = "Item added to Wishlist Successfully" }, JsonRequestBehavior.AllowGet);
			}
			return Json(new { success = true, message = "Item added to Cart Successfully" }, JsonRequestBehavior.AllowGet);

			#endregion
		}


		#endregion


		#region DELETE CART ITEM

		public ActionResult UpdateUserCart(string details)
		{

			var response = new JavaScriptSerializer().Deserialize<AddToShoppingCartModel>(details);
			var customer = _customerService.GetCustomerById(response.CustomerId);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			var cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				 .LimitPerStore(storeId)
				 .ToList();

			// var allIdsToRemove = form["removefromcart"] != null ? form["removefromcart"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();

			var allIdsToRemove = response.cartItemId; /// need cart item id from response

			//current warnings <cart item identifier, warnings>
			var innerWarnings = new Dictionary<int, IList<string>>();
			foreach (var sci in cart)
			{
				var update = false;
				if (sci.Id == allIdsToRemove)
				{
					update = true;
				}

				if (update)
				{					

					var newQuantity = response.EnteredQuantity;	// quantity from details if updated or same								  
					var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(customer,
						sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice,
						sci.RentalStartDateUtc, sci.RentalEndDateUtc,
						newQuantity);

					if (currSciWarnings.Any())
					{
						innerWarnings.Add(sci.Id, currSciWarnings);
					}
					
					break;
				}
			}

			//parse and save checkout attributes	  

			ParseAndSaveCheckoutAttributes(cart, response.CustomerId);

			//updated cart
			cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				 .LimitPerStore(storeId)
				 .ToList();
			var model = new ShoppingCartModel();
			model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);
			//update current warnings
			foreach (var kvp in innerWarnings)
			{
				//kvp = <cart item identifier, warnings>
				var sciId = kvp.Key;
				var warnings = kvp.Value;
				//find model
				var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
				if (sciModel != null)
				{
					foreach (var w in warnings)
						if (!sciModel.Warnings.Contains(w))
							sciModel.Warnings.Add(w);
					return Json(new { success = false, message = "Error While updating" }, JsonRequestBehavior.AllowGet);
				}

			
			}

			return Json(new { success = true, message = "Cart updated successfully" }, JsonRequestBehavior.AllowGet);
		}


		//[ValidateInput(false)]
		//[HttpPost, ActionName("Cart")]		
		public ActionResult UpdateCart(string details)
		{
			var response = new JavaScriptSerializer().Deserialize<AddToShoppingCartModel>(details);
			var customer = _customerService.GetCustomerById(response.CustomerId);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			var cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				 .LimitPerStore(storeId)
				 .ToList();

			// var allIdsToRemove = form["removefromcart"] != null ? form["removefromcart"].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => int.Parse(x)).ToList() : new List<int>();

			var allIdsToRemove = response.cartItemId; /// need cart item id from response

			//current warnings <cart item identifier, warnings>
			//var innerWarnings = new Dictionary<int, IList<string>>();
			foreach (var sci in cart)
			{
				var remove = false;
				if (sci.Id == allIdsToRemove)
				{
					remove = true;
				}

				if (remove)
				{
					_shoppingCartService.DeleteShoppingCartItem(sci, ensureOnlyActiveCheckoutAttributes: true);
					break;
				}
				//else
				//{

				//	var newQuantity = response.EnteredQuantity;	// quantity from details if updated or same								  
				//	var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(customer,
				//		sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice,
				//		sci.RentalStartDateUtc, sci.RentalEndDateUtc,
				//		newQuantity);
				//	innerWarnings.Add(sci.Id, currSciWarnings);
				//	break;
				//}
			}

			//parse and save checkout attributes	  

			ParseAndSaveCheckoutAttributes(cart, response.CustomerId);

			//updated cart
			//cart = customer.ShoppingCartItems
			//	 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
			//	 .LimitPerStore(storeId)
			//	 .ToList();
			//var model = new ShoppingCartModel();
			//model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);
			//update current warnings
			//foreach (var kvp in innerWarnings)
			//{
			//	//kvp = <cart item identifier, warnings>
			//	var sciId = kvp.Key;
			//	var warnings = kvp.Value;
			//	//find model
			//	var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
			//	if (sciModel != null)
			//	{
			//		foreach (var w in warnings)
			//			if (!sciModel.Warnings.Contains(w))
			//				sciModel.Warnings.Add(w);
			//		return Json(new { success = false, message = "Error While Deleting" }, JsonRequestBehavior.AllowGet);
			//	}
			//}

			return Json(new { success = true, message = "Item deleted successfully" }, JsonRequestBehavior.AllowGet);
		}

		#endregion


		#region Parse Attributes

		[NonAction]
		protected string ParseProductAttributes(Product product, List<string> details, List<string> errors)
		{
			//var attributesXml = "";

			//#region Product attributes

			//var productAttributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);

			//foreach (var attribute in productAttributes)
			//{
			//	if (details.Count > 0)
			//	{
			//		foreach (var item in details)
			//		{
			//			var attValues = item.Split(',');
			//			var attrId = attValues[0].Split(':')[1];
			//			var attrVal = attValues[1].Split(':')[1];

			//			if (int.Parse(attrId) == attribute.ProductAttributeId)
			//			{

			//				var selectedAttributeId = int.Parse(attrVal);
			//				if (selectedAttributeId > 0)
			//				{
			//					attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
			//					  attribute, selectedAttributeId.ToString());
			//				}
			//			}
			//		}
			//	}
			//}

			////validate conditional attributes (if specified)
			//foreach (var attribute in productAttributes)
			//{
			//	var conditionMet = _productAttributeParser.IsConditionMet(attribute, attributesXml);
			//	if (conditionMet.HasValue && !conditionMet.Value)
			//	{
			//		attributesXml = _productAttributeParser.RemoveProductAttribute(attributesXml, attribute);
			//	}
			//}
			string attributesXml = "";

			#region Product attributes

			var productAttributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id).ToList();

			//var a = AttributeControlType.Checkboxes;
			var attList = GetItems(details);

			foreach (var attribute in productAttributes)
			{
				string attrVal = "";
				var result = attList.Find(x => x.Key == attribute.Id);
				if (result.Key != 0)
				{
					attrVal = result.Value.ToString();
				}
				string controlId = attrVal;

				switch (attribute.AttributeControlType)
				{
					case AttributeControlType.DropdownList:
					case AttributeControlType.RadioList:
					case AttributeControlType.ColorSquares:
					case AttributeControlType.ImageSquares:
						{
							//var ctrlAttributes = form[controlId];
							var ctrlAttributes = controlId;
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								int selectedAttributeId = int.Parse(ctrlAttributes);
								if (selectedAttributeId > 0)
								{
									//get quantity entered by customer
									int quantity = 1;
									//  var quantityStr = string.Format("product_attribute_{0}_{1}_qty", attribute.Id, selectedAttributeId);
									//var quantityStr = form[string.Format("product_attribute_{0}_{1}_qty", attribute.Id, selectedAttributeId)];
									//if (quantityStr != null && (!int.TryParse(quantityStr, out quantity) || quantity < 1))
									if (quantity <= 0)
										errors.Add(_localizationService.GetResource("ShoppingCart.QuantityShouldPositive"));

									attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
										 attribute, selectedAttributeId.ToString(), quantity > 1 ? (int?)quantity : null);
								}
							}
						}
						break;
					case AttributeControlType.Checkboxes:
						{
							//var ctrlAttributes = form[controlId];
							var ctrlAttributes = controlId;
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								foreach (var itm in ctrlAttributes.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries))
								{
									int selectedAttributeId = int.Parse(itm);
									if (selectedAttributeId > 0)
									{
										//get quantity entered by customer
										var quantity = 1;
										//var quantityStr = form[string.Format("product_attribute_{0}_{1}_qty", attribute.Id, item)];
										//  var quantityStr = string.Format("product_attribute_{0}_{1}_qty", attribute.Id, itm);
										//	  if (quantityStr != null && (!int.TryParse(quantityStr, out quantity) || quantity < 1))
										if (quantity <= 0)
											errors.Add(_localizationService.GetResource("ShoppingCart.QuantityShouldPositive"));

										attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
											 attribute, selectedAttributeId.ToString(), quantity > 1 ? (int?)quantity : null);
									}
								}
							}
						}
						break;
					case AttributeControlType.ReadonlyCheckboxes:
						{
							//load read-only (already server-side selected) values
							var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
							foreach (var selectedAttributeId in attributeValues
								 .Where(v => v.IsPreSelected)
								 .Select(v => v.Id)
								 .ToList())
							{
								//get quantity entered by customer
								var quantity = 1;
								//var quantityStr = form[string.Format("product_attribute_{0}_{1}_qty", attribute.Id, selectedAttributeId)];
								// var quantityStr = string.Format("product_attribute_{0}_{1}_qty", attribute.Id, selectedAttributeId);
								// if (quantityStr != null && (!int.TryParse(quantityStr, out quantity) || quantity < 1))
								if (quantity <= 0)
									errors.Add(_localizationService.GetResource("ShoppingCart.QuantityShouldPositive"));

								attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
									 attribute, selectedAttributeId.ToString(), quantity > 1 ? (int?)quantity : null);
							}
						}
						break;
					case AttributeControlType.TextBox:
					case AttributeControlType.MultilineTextbox:
						{
							//var ctrlAttributes = form[controlId];
							var ctrlAttributes = controlId;
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								string enteredText = ctrlAttributes.Trim();
								attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
									 attribute, enteredText);
							}
						}
						break;
					case AttributeControlType.Datepicker:
						{
							//var day = form[controlId + "_day"];
							//var month = form[controlId + "_month"];
							//var year = form[controlId + "_year"];

							var day = controlId + "_day";
							var month = controlId + "_month";
							var year = controlId + "_year";
							DateTime? selectedDate = null;
							try
							{
								selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
							}
							catch { }
							if (selectedDate.HasValue)
							{
								attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
									 attribute, selectedDate.Value.ToString("D"));
							}
						}
						break;
					case AttributeControlType.FileUpload:
						{
							Guid downloadGuid;
							//Guid.TryParse(form[controlId], out downloadGuid);
							Guid.TryParse(controlId, out downloadGuid);
							var download = _downloadService.GetDownloadByGuid(downloadGuid);
							if (download != null)
							{
								attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
										  attribute, download.DownloadGuid.ToString());
							}
						}
						break;
					default:
						break;
					//  }								  
				}
			}

			//validate conditional attributes (if specified)
			foreach (var attribute1 in productAttributes)
			{
				var conditionMet = _productAttributeParser.IsConditionMet(attribute1, attributesXml);
				if (conditionMet.HasValue && !conditionMet.Value)
				{
					attributesXml = _productAttributeParser.RemoveProductAttribute(attributesXml, attribute1);
				}
			} 					 
				 

			#endregion

			

			return attributesXml;
		}

		[NonAction]
		protected void ParseAndSaveCheckoutAttributes(List<ShoppingCartItem> cart, int UserId)
		{
			if (cart == null)
				throw new ArgumentNullException("cart");


			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			var customer = _customerService.GetCustomerById(UserId);

			var attributesXml = "";
			var checkoutAttributes = _checkoutAttributeService.GetAllCheckoutAttributes(storeId, !cart.RequiresShipping());

			foreach (var attribute in checkoutAttributes)
			{
				var controlId = string.Format("checkout_attribute_{0}", attribute.Id);
				var attributeValues = _checkoutAttributeService.GetCheckoutAttributeValues(attribute.Id);
				foreach (var selectedAttributeId in attributeValues
					 .Where(v => v.IsPreSelected)
					 .Select(v => v.Id)
					 .ToList())
				{
					attributesXml = _checkoutAttributeParser.AddCheckoutAttribute(attributesXml,
									attribute, selectedAttributeId.ToString());
				}
			}

			//validate conditional attributes (if specified)
			foreach (var attribute in checkoutAttributes)
			{
				var conditionMet = _checkoutAttributeParser.IsConditionMet(attribute, attributesXml);
				if (conditionMet.HasValue && !conditionMet.Value)
					attributesXml = _checkoutAttributeParser.RemoveCheckoutAttribute(attributesXml, attribute);
			}

			//save checkout attributes
			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CheckoutAttributes, attributesXml, storeId);
		}
		#endregion

		#region Wishlist

		public virtual ActionResult UpdateWishlist(string details)
		{
			var response = new JavaScriptSerializer().Deserialize<AddToShoppingCartModel>(details);
			var customer = _customerService.GetCustomerById(response.CustomerId);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			if (customer == null)
				return Json(new { success = false, message = "Customer Not Exist" }, JsonRequestBehavior.AllowGet);

			var cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
				 .LimitPerStore(storeId)
				 .ToList();

			var allIdsToRemove = response.cartItemId;

			//current warnings <cart item identifier, warnings>
			var innerWarnings = new Dictionary<int, IList<string>>();
			foreach (var sci in cart)
			{
				var remove = false;
				if (sci.Id == allIdsToRemove)
					_shoppingCartService.DeleteShoppingCartItem(sci, ensureOnlyActiveCheckoutAttributes: true);
				else
				{
					var newQuantity = response.EnteredQuantity;

					var currSciWarnings = _shoppingCartService.UpdateShoppingCartItem(customer,
						 sci.Id, sci.AttributesXml, sci.CustomerEnteredPrice,
						 sci.RentalStartDateUtc, sci.RentalEndDateUtc,
						 newQuantity);
					innerWarnings.Add(sci.Id, currSciWarnings);
				}
			}

			//updated wishlist
			cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.Wishlist)
				 .LimitPerStore(storeId)
				 .ToList();
			var model = new WishlistModel();
			model = _shoppingCartModelFactory.PrepareWishlistModel(model, cart);
			//update current warnings
			foreach (var kvp in innerWarnings)
			{
				//kvp = <cart item identifier, warnings>
				var sciId = kvp.Key;
				var warnings = kvp.Value;
				//find model
				var sciModel = model.Items.FirstOrDefault(x => x.Id == sciId);
				if (sciModel != null)
					foreach (var w in warnings)
						if (!sciModel.Warnings.Contains(w))
							sciModel.Warnings.Add(w);
			}
			// return View(model);
			return Json(new { success = true, message = "Wishlist Item deleted successfully" }, JsonRequestBehavior.AllowGet);
		}

		#endregion	 

		#region Confirm Order
		public ActionResult OrderSummary(string details)
		{
			var response = new JavaScriptSerializer().Deserialize<AddToShoppingCartModel>(details);
			var customer = _customerService.GetCustomerById(response.CustomerId);
			if (customer == null)
				return Json(new { success = false, message = "Customer Not Exist" }, JsonRequestBehavior.AllowGet);

			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			var cart = customer.ShoppingCartItems
					  .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
					  .LimitPerStore(storeId)
					  .ToList();
			var model = new ShoppingCartModel();
			model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart, false,
				 prepareEstimateShippingIfEnabled: false,
				 prepareAndDisplayOrderReviewData: true);
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}


		public ActionResult OrderTotals(string details)
		{
			var response = new JavaScriptSerializer().Deserialize<AddToShoppingCartModel>(details);
			var customer = _customerService.GetCustomerById(response.CustomerId);
			if (customer == null)
				return Json(new { success = false, message = "Customer Not Exist" }, JsonRequestBehavior.AllowGet);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			var cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				 .LimitPerStore(storeId)
				 .ToList();

			var model = _shoppingCartModelFactory.PrepareOrderTotalsModel(cart, true);
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region "Discounts"

	
		public ActionResult ApplyDiscountCoupon(string details)
		{
			var response = new JavaScriptSerializer().Deserialize<DiscountModel>(details);
			var customer = _customerService.GetCustomerById(response.UserId);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			string discountcouponcode = response.DiscountCode;

			//trim
			if (discountcouponcode != null)
				discountcouponcode = discountcouponcode.Trim();

			//cart
			var cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				 .LimitPerStore(storeId)
				 .ToList();

			//parse and save checkout attributes
			ParseAndSaveCheckoutAttributes(cart, response.UserId);

			var modelnew = new DiscountModel();

			//var model = new ShoppingCartModel();
			if (!String.IsNullOrWhiteSpace(discountcouponcode))
			{
				//we find even hidden records here. this way we can display a user-friendly message if it's expired
				var discounts = _discountService.GetAllDiscountsForCaching(couponCode: discountcouponcode, showHidden: true)
					 .Where(d => d.RequiresCouponCode)
					 .ToList();
				if (discounts.Any())
				{
					var userErrors = new List<string>();
					var anyValidDiscount = discounts.Any(discount =>
					{
						var validationResult = _discountService.ValidateDiscount(discount, customer, new[] { discountcouponcode });
						userErrors.AddRange(validationResult.Errors);

						return validationResult.IsValid;

					});

					if (anyValidDiscount)
					{
						//valid
						customer.ApplyDiscountCouponCode(discountcouponcode);
						modelnew.DiscountCode = discountcouponcode;
						var validDiscount = _discountService.GetAllDiscounts(couponCode:discountcouponcode);
						modelnew.Id = validDiscount.FirstOrDefault().Id;
					
						//model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.Applied"));
						//model.DiscountBox.IsApplied = true;
					}
					else
					{
						if (userErrors.Any())
						{
							//some user errors
							//model.DiscountBox.Messages = userErrors;
							return Json(new { success = false, message = userErrors.ToString() }, JsonRequestBehavior.AllowGet);
						}
						else
							//general error text
							//model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
							return Json(new { success = false, message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount") }, JsonRequestBehavior.AllowGet);
					}
				}
				else
					//discount cannot be found
					//model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
					return Json(new { success = false, message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount") }, JsonRequestBehavior.AllowGet);
			}
			else
				//empty coupon code
				//	model.DiscountBox.Messages.Add(_localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount"));
				return Json(new { success = false, message = _localizationService.GetResource("ShoppingCart.DiscountCouponCode.WrongDiscount") }, JsonRequestBehavior.AllowGet);

			//model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);
			//return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		

			return Json(new { success = true, message = "", data = modelnew }, JsonRequestBehavior.AllowGet);
			//return View(model);


		}


		
		public virtual ActionResult RemoveDiscountCoupon(string details)
		{

			var response = new JavaScriptSerializer().Deserialize<DiscountModel>(details);
			var customer = _customerService.GetCustomerById(response.UserId);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			

			//var model = new ShoppingCartModel();	
		  
			//get discount identifier
			int discountId = 0;
			if (response != null)
			{
				discountId = response.Id; 
			}
			
			//foreach (var formValue in form.AllKeys)
			//	if (formValue.StartsWith("removediscount-", StringComparison.InvariantCultureIgnoreCase))
			//		discountId = Convert.ToInt32(formValue.Substring("removediscount-".Length));

			var discount = _discountService.GetDiscountById(discountId);
			

			if (discount != null)
				customer.RemoveDiscountCouponCode(discount.CouponCode);


			//var cart = customer.ShoppingCartItems
			//	 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
			//	 .LimitPerStore(storeId)
			//	 .ToList();
		//	model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);
			//return View(model);
			return Json(new { success = true, message = "Coupon removed successfully!"}, JsonRequestBehavior.AllowGet);
		}

		#endregion


		#region Loyality points
		
					
		public  ActionResult ApplyLoyaltyPoints(string details, int pointToUse = 0)
		{
			var response = new JavaScriptSerializer().Deserialize<ShoppingCartModel.LoyaltyBoxModel>(details);
			var customer = _customerService.GetCustomerById(response.UserId);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			//cart
			var cart = customer.ShoppingCartItems
				 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				 .LimitPerStore(storeId)
				 .ToList();

			//parse and save checkout attributes
			ParseAndSaveCheckoutAttributes(cart, response.UserId);

			pointToUse = response.PointsToUse;


			var availableLoyaltyPoints = _rewardPointService.GetRewardPointsBalance(customer.Id,storeId);

			if (availableLoyaltyPoints == 0)
			{
				_genericAttributeService.SaveAttribute(customer,
					 SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, false, storeId);				
				return Json(new { success = false, message = "There are no points available to redeem." }, JsonRequestBehavior.AllowGet);
	 		}

			if (availableLoyaltyPoints < pointToUse)
			{
				_genericAttributeService.SaveAttribute(customer,
					 SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, false,storeId);  				
				return Json(new { success = false, message = "You have less points available. Please enter appropriate value to redeem." }, JsonRequestBehavior.AllowGet);
	  		}


			//var model = new ShoppingCartModel();
			if (pointToUse > 0)
			{
				_genericAttributeService.SaveAttribute(customer,
					SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, true, storeId);
				_genericAttributeService.SaveAttribute(customer, "PointsToUse", pointToUse,
					storeId);
			}
			else
			{
				_genericAttributeService.SaveAttribute(customer,
					SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, false, storeId);					
				return Json(new { success = false, message = "Please enter points to redeem." }, JsonRequestBehavior.AllowGet);

			}

			//model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);

			//return View(model);

			return Json(new { success = true, message = "Loyality points applied successfully!" }, JsonRequestBehavior.AllowGet);
		}

	
				
		public  ActionResult RemoveLoyalty(string details)
		{

			var response = new JavaScriptSerializer().Deserialize<ShoppingCartModel.LoyaltyBoxModel>(details);
			var customer = _customerService.GetCustomerById(response.UserId);
			var storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			var model = new ShoppingCartModel();

			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.UseRewardPointsDuringCheckout, false, storeId);

			_genericAttributeService.SaveAttribute(customer, "PointsToUse", 0, storeId);


			//var cart = customer.ShoppingCartItems
			//	 .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
			//	 .LimitPerStore(storeId)
			//	 .ToList();
			//model = _shoppingCartModelFactory.PrepareShoppingCartModel(model, cart);
			//return View(model);
			return Json(new { success = true, message = "Loyality points removed successfully!" }, JsonRequestBehavior.AllowGet);
		}

		#endregion




		public ActionResult CheckoutAttributeChange(string details)
		{

			var response = new JavaScriptSerializer().Deserialize<CompleteCheckoutProcess>(details);
			int customerId = int.Parse(response.CustomerId);
			var customer = _customerService.GetCustomerById(customerId);
			int storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			var cart = customer.ShoppingCartItems
				  .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
				  .LimitPerStore(storeId)
				  .ToList();
		
			ParseAndSaveCheckoutAttributes(cart, customerId);
			var attributeXml = customer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes,
				  _genericAttributeService, storeId);

			var enabledAttributeIds = new List<int>();
			var disabledAttributeIds = new List<int>();
			var attributes = _checkoutAttributeService.GetAllCheckoutAttributes(storeId, !cart.RequiresShipping());
			foreach (var attribute in attributes)
			{
				var conditionMet = _checkoutAttributeParser.IsConditionMet(attribute, attributeXml);
				if (conditionMet.HasValue)
				{
					if (conditionMet.Value)
						enabledAttributeIds.Add(attribute.Id);
					else
						disabledAttributeIds.Add(attribute.Id);
				}
			}

			//return Json(new
			//{
			//	enabledattributeids = enabledAttributeIds.ToArray(),
			//	disabledattributeids = disabledAttributeIds.ToArray()
			//});

			return Json(new { success = true, message = "Loyality points removed successfully!", eData = enabledAttributeIds.ToArray(), dData = disabledAttributeIds.ToArray() }, JsonRequestBehavior.AllowGet);

		}

		//handle product attribute selection event. this way we return new price, overridden gtin/sku/mpn
		//currently we use this method on the product details pages
		[HttpGet]
		public ActionResult ProductDetails_AttributeChange(int productId, List<string> details, int userId)
		{
			bool validateAttributeConditions = true;
			bool loadPicture = true;
			var product = _productService.GetProductById(productId);
			if (product == null)
				return Json(new { success = false, message = "Product not found" }, JsonRequestBehavior.AllowGet);
																																		 
			var CurrentCustomer = _customerService.GetCustomerById(userId);
			var workCurrency = _currencyService.GetAllCurrencies().FirstOrDefault();

			var errors = new List<string>();
			string attributeXml = ParseProductAttributes(product, details, errors);

			int storeId = _storeService.GetAllStores().FirstOrDefault().Id;

			//foreach (var item in details)
			//{			  				 
			//  string[] attValues = item.Split(',');
			//  string attrId = attValues[0].Split(':')[1];
			//  string attrVal = attValues[1].Split(':')[1];
			//}



			//rental attributes
			DateTime? rentalStartDate = null;
			DateTime? rentalEndDate = null;
			//if (product.IsRental)
			//{
			//	 ParseRentalDates(product, details, out rentalStartDate, out rentalEndDate);
			//}

			//sku, mpn, gtin
			string sku = product.FormatSku(attributeXml, _productAttributeParser);
			string mpn = product.FormatMpn(attributeXml, _productAttributeParser);
			string gtin = product.FormatGtin(attributeXml, _productAttributeParser);

			//price
			string price = "";
			if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices) && !product.CustomerEntersPrice)
			{
				//we do not calculate price of "customer enters price" option is enabled
				List<DiscountForCaching> scDiscounts;
				decimal discountAmount;
				decimal finalPrice = _priceCalculationService.GetUnitPrice(product,
					 CurrentCustomer,
					 ShoppingCartType.ShoppingCart,
					 1, attributeXml, 0,
					 rentalStartDate, rentalEndDate,
					 true, out discountAmount, out scDiscounts);
				decimal taxRate;
				decimal finalPriceWithDiscountBase = _taxService.GetProductPrice(product, finalPrice, out taxRate);
				decimal finalPriceWithDiscount = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceWithDiscountBase, workCurrency);
				price = _priceFormatter.FormatPrice(finalPriceWithDiscount);
			}

			//stock
			var stockAvailability = product.FormatStockMessage(attributeXml, _localizationService, _productAttributeParser, _dateRangeService);

			//conditional attributes
			var enabledAttributeMappingIds = new List<int>();
			var disabledAttributeMappingIds = new List<int>();
			if (validateAttributeConditions)
			{
				var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
				foreach (var attribute in attributes)
				{
					var conditionMet = _productAttributeParser.IsConditionMet(attribute, attributeXml);
					if (conditionMet.HasValue)
					{
						if (conditionMet.Value)
							enabledAttributeMappingIds.Add(attribute.Id);
						else
							disabledAttributeMappingIds.Add(attribute.Id);
					}
				}
			}

			//picture. used when we want to override a default product picture when some attribute is selected
			var pictureFullSizeUrl = "";
			var pictureDefaultSizeUrl = "";
			if (loadPicture)
			{
				//just load (return) the first found picture (in case if we have several distinct attributes with associated pictures)
				//actually we're going to support pictures associated to attribute combinations (not attribute values) soon. it'll more flexible approach
				var attributeValues = _productAttributeParser.ParseProductAttributeValues(attributeXml);
				var attributeValueWithPicture = attributeValues.FirstOrDefault(x => x.PictureId > 0);
				if (attributeValueWithPicture != null)
				{
					//var productAttributePictureCacheKey = string.Format(ModelCacheEventConsumer.PRODUCTATTRIBUTE_PICTURE_MODEL_KEY,
					//				  attributeValueWithPicture.PictureId,
					//				  _webHelper.IsCurrentConnectionSecured(),
					//				  storeId);
					//var pictureModel = _cacheManager.Get(productAttributePictureCacheKey, () =>
					//{
					  var pictureModel = new PictureModel();
						var valuePicture = _pictureService.GetPictureById(attributeValueWithPicture.PictureId);
						if (valuePicture != null)
						{
							//return new PictureModel
							//{
								pictureModel.FullSizeImageUrl = _pictureService.GetPictureUrl(valuePicture);
								pictureModel.ImageUrl = _pictureService.GetPictureUrl(valuePicture, _mediaSettings.ProductDetailsPictureSize);
							//};
						}

						//return new PictureModel();
					//});
					pictureFullSizeUrl = pictureModel.FullSizeImageUrl;
					pictureDefaultSizeUrl = pictureModel.ImageUrl;
				}

			}

			return Json(new
			{
				gtin = gtin,
				mpn = mpn,
				sku = sku,
				price = price,
				stockAvailability = stockAvailability,
				enabledattributemappingids = enabledAttributeMappingIds.ToArray(),
				disabledattributemappingids = disabledAttributeMappingIds.ToArray(),
				pictureFullSizeUrl = pictureFullSizeUrl,
				pictureDefaultSizeUrl = pictureDefaultSizeUrl,
				message = errors.Any() ? errors.ToArray() : null
			}, JsonRequestBehavior.AllowGet);
		}


		[NonAction]
		protected virtual List<KeyValuePair<int, string>> GetItems(List<string> details)
		{

			var dtList = new List<KeyValuePair<int, string>>();

			if (details != null)
			{

				if (!string.IsNullOrEmpty(details[0]))
				{
					string[] attValues = details[0].Split(';');

					for (int j = 0; j < attValues.Length - 1; j++)
					{
						string[] attValues1 = attValues[j].Split(',');
						int attrId = Int32.Parse(attValues1[0].Split(':')[1]);
						string attrVal = attValues1[1].Split(':')[1];

						dtList.Add(new KeyValuePair<int, string>(attrId, attrVal));
					}
				}
			}
			return dtList;
		}


	}
}