﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Plugin.PostXCustom;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Services;
using Nop.Plugin.WebApi.Models.Auth;
using Nop.Plugin.WebApi.Models.Customer;
using Nop.Plugin.WebApi.Models.Media;
using Nop.Services.Authentication;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.WebApi.Controllers
{
	public class AuthController : BasePluginController
	{
		#region Fields
		private readonly ICustomerService _customerService;
		private readonly ICustomerRegistrationService _customerRegistrationService;
		private readonly CustomerSettings _customerSettings;
		private readonly IWorkContext _workContext;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly IAuthenticationService _authenticationService;
		private readonly IEventPublisher _eventPublisher;
		private readonly IStoreContext _storeContext;
		private readonly IShoppingCartService _shoppingCartService;
		private readonly ILanguageService _languageService;
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly ILocalizationService _localizationService;
		private readonly ICustomerActivityService _customerActivityService;
		private readonly MediaSettings _mediaSettings;
		private readonly IPictureService _pictureService;
		private readonly IStoreService _storeService;
		private readonly ISettingService _settingService;
		private readonly ICompanyRepresentativeCustomerMappingService _companyRepresentativeCustomerMappingService;
		#endregion

		#region Cont

		public AuthController(ICustomerService customerService, ICustomerRegistrationService customerRegistrationService, CustomerSettings customerSettings,
			IWorkContext workContext, IGenericAttributeService genericAttributeService, IAuthenticationService authenticationService,
			IEventPublisher eventPublisher, IStoreContext storeContext, IShoppingCartService shoppingCartService, ILanguageService languageService,
			IWorkflowMessageService workflowMessageService, ILocalizationService localizationService, ICustomerActivityService customerActivityService,
			MediaSettings mediaSettings, IPictureService pictureService, IStoreService storeService, ISettingService settingService,
			ICompanyRepresentativeCustomerMappingService companyRepresentativeCustomerMappingService)
		{
			_customerService = customerService;
			_customerRegistrationService = customerRegistrationService;
			_customerSettings = customerSettings;
			_workContext = workContext;
			_genericAttributeService = genericAttributeService;
			_authenticationService = authenticationService;
			_eventPublisher = eventPublisher;
			_storeContext = storeContext;
			_shoppingCartService = shoppingCartService;
			_languageService = languageService;
			_workflowMessageService = workflowMessageService;
			_localizationService = localizationService;
			_customerActivityService = customerActivityService;
			_mediaSettings = mediaSettings;
			_pictureService = pictureService;
			_storeService = storeService;
			_settingService = settingService;
			_companyRepresentativeCustomerMappingService = companyRepresentativeCustomerMappingService;
		}

		#endregion

		#region REGISTER CUSTOMER
		public ActionResult RegisterCustomer(string json)
		{
			if (json == null)
			{
				return Json(new { success = false, message = "Details are empty" }, JsonRequestBehavior.AllowGet);
			}

			var model = new JavaScriptSerializer().Deserialize<ApiSignup>(json);
			if (model == null)
				return Json(new { success = false, message = "Enter All Fields" }, JsonRequestBehavior.AllowGet);

			int userId = int.Parse(model.Id);

			var customer = _customerService.GetCustomerById(userId);
	  			
		  // var customer = currentCustomer;

			int storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			customer.RegisteredInStoreId = storeId;

			#region Assign Username

			var parentCompany = _customerService.GetAllCustomers().FirstOrDefault(x => x.CompanyId == 1 && x.PersonalId == 1);
			int personId = 0;
			AssignPersonId(parentCompany.Id, out personId);
			int companyId = parentCompany.CompanyId;
			model.Username = companyId.ToString("D3") + personId.ToString("D5");
			//model.Password = Guid.NewGuid().ToString("N").Substring(0, 8);
			Random generator = new Random();
			var password = generator.Next(0, 1000000).ToString("D6");
			model.Password = password;
			#endregion


			var isApproved = true;//_customerSettings.UserRegistrationType == UserRegistrationType.Standard;
			var registrationRequest = new CustomerRegistrationRequest(customer,
				 model.Email,
				 _customerSettings.UsernamesEnabled ? model.Username : model.Email,
				 model.Password,
				 _customerSettings.DefaultPasswordFormat,
				 storeId,
				 isApproved);
			var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);

			if (registrationResult.Success)
			{

				customer.PersonalId = personId;
				customer.CompanyId = companyId;

				_customerService.UpdateCustomer(customer);


				var att1 = new GenericAttribute
				{
					EntityId = customer.Id,
					KeyGroup = "Customer",
					StoreId = 0,
					Key = "FirstName",
					Value = model.FirstName,
				};

				var att2 = new GenericAttribute
				{
					EntityId = customer.Id,
					KeyGroup = "Customer",
					StoreId = 0,
					Key = "LastName",
					Value = model.LastName
				};
				var att3 = new GenericAttribute
				{
					EntityId = customer.Id,
					KeyGroup = "Customer",
					StoreId = 0,
					Key = "Gender",
					Value = model.Gender
				};


				if (!string.IsNullOrEmpty(model.FirstName))
				{
					_genericAttributeService.InsertAttribute(att1);
				}

				if (!string.IsNullOrEmpty(model.LastName))
				{
					_genericAttributeService.InsertAttribute(att2);
				}

				if (!string.IsNullOrEmpty(model.Gender))
				{
					_genericAttributeService.InsertAttribute(att3);
				}

			



				if (customer.Id > 0)
					AssignCompanyRepresentativeMapping(0, customer.Id, parentCompany.Id);



				//var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
				//InsertUpdateAttribute(customer, attributeList.ToList(), "ContractTypeId", "3");

				//var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
				//var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeScope);

				//InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractC.ToString(CultureInfo.InvariantCulture));


				var companyAttributeList = _genericAttributeService.GetAttributesForEntity(parentCompany.Id, "Customer");
				var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");

				InsertUpdateAttribute(customer, attributeList.ToList(), "AccessWord", model.Password);

				var companyName = parentCompany.GetAttribute<string>(SystemCustomerAttributeNames.Company);
				//Company Name
				if (!string.IsNullOrEmpty(companyName))
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, companyName);

				var contractTypeId = GetAttributeValue(companyAttributeList.ToList(), "ContractTypeId");
				InsertUpdateAttribute(customer, attributeList.ToList(), "ContractTypeId", (!string.IsNullOrEmpty(contractTypeId) ? contractTypeId : "0"));

				var vatRegimeId = GetAttributeValue(companyAttributeList.ToList(), "VatRegimeId");
				InsertUpdateAttribute(customer, attributeList.ToList(), "VatRegimeId", (!string.IsNullOrEmpty(vatRegimeId) ? vatRegimeId : "0"));

				InsertUpdateAttribute(customer, attributeList.ToList(), "ContractTypeId", "4");
				  				
				var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeId);

				InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractD.ToString(CultureInfo.InvariantCulture));

				InsertUpdateAttribute(customer, attributeList.ToList(), "PersonalNumber", customer.Username);

				int languageId = _languageService.GetAllLanguages().FirstOrDefault().Id;

				//notifications
				if (_customerSettings.NotifyNewCustomerRegistration)
				{
					_workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, languageId);
					_workflowMessageService.SendCustomerWelcomeMessage(customer, languageId);
				}
				//raise event       
				_eventPublisher.Publish(new CustomerRegisteredEvent(customer));

			}
			else
			{
				return Json(new { success = false, message = registrationResult.Errors[0].ToString() }, JsonRequestBehavior.AllowGet);
			}


			return Json(new { success = true, message = "Registered Successfully" }, JsonRequestBehavior.AllowGet);
		}


		private void AssignPersonId(int parentCompanyId, out int personId)
		{
			var companyUserList = _companyRepresentativeCustomerMappingService.GetCustomersBySalesPerson(parentCompanyId);

			if (companyUserList.Any())
			{
				var customerIdList = companyUserList.Select(x => x.AssociatedCustomerId);
				var customerList = _customerService.GetAllCustomers().Where(x => customerIdList.Contains(x.Id));
				var topCustomerIdRecord = customerList.OrderByDescending(x => x.PersonalId).First();
				if (topCustomerIdRecord == null)
				{
					personId = 2;
					return;
				}
				personId = topCustomerIdRecord.PersonalId + 1;
				return;
			}
			personId = 2;
		}

		public virtual void AssignCompanyRepresentativeMapping(int recordId, int associatedCustomerId, int companyRepresentativeId)
		{
			if (associatedCustomerId == 0)
			{
				return;
			}

			var salesPersonCustomerMap = _companyRepresentativeCustomerMappingService.GetByCustomerId(associatedCustomerId);

			if (salesPersonCustomerMap == null && companyRepresentativeId == 0)
				return;


			if (salesPersonCustomerMap == null && companyRepresentativeId > 0)
			{
				salesPersonCustomerMap = new CompanyRepresentativeCustomerMapping
				{
					AssociatedCustomerId = associatedCustomerId,
					CompanyRepresentativeId = companyRepresentativeId
				};
				_companyRepresentativeCustomerMappingService.Insert(salesPersonCustomerMap);
				return;
			}

			if (salesPersonCustomerMap != null && companyRepresentativeId == 0)
			{
				_companyRepresentativeCustomerMappingService.Delete(salesPersonCustomerMap);
				return;
			}

			if (salesPersonCustomerMap != null && companyRepresentativeId > 0)
			{
				salesPersonCustomerMap.CompanyRepresentativeId = companyRepresentativeId;
				_companyRepresentativeCustomerMappingService.Update(salesPersonCustomerMap);
			}
		}

		public string GetAttributeValue(List<GenericAttribute> genericAttributes, string attributeToCheck)
		{
			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeToCheck);
			if (genericAttribute == null)
				return null;

			return genericAttribute.Value;
		}

		public void InsertUpdateAttribute(Customer customer, List<GenericAttribute> genericAttributes, string attributeName, string attributeValue)
		{
			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeName);
			if (genericAttribute == null)
			{
				genericAttribute = new GenericAttribute
				{
					EntityId = customer.Id,
					Key = attributeName,
					KeyGroup = "Customer",
					Value = attributeValue,
					StoreId = 0
				};
				_genericAttributeService.InsertAttribute(genericAttribute);
			}
			else
			{
				genericAttribute.Value = attributeValue;
				_genericAttributeService.UpdateAttribute(genericAttribute);
			}
		}




		#endregion

		#region LOGIN CUSTOMER

		[HttpGet]
		public ActionResult LoginCustomer(string json)
		{
			if (json == null)
			{
				return Json(new { success = false, message = "Login Details are Empty" });
			}

			var model = new JavaScriptSerializer().Deserialize<ApiLogin>(json);
			if (model == null)
				return Json(new { success = false, message = "Login Details are Empty" }, JsonRequestBehavior.AllowGet);

			var loginResult = _customerRegistrationService.ValidateCustomer(model.UserId, model.Password);

			Customer guestCustomer = null;

			if (model.GuestId != 0)
			{
				guestCustomer = _customerService.GetCustomerById(model.GuestId);
				if (guestCustomer == null)
					return Json(new { success = false, message = "Guest Customer Details Not Found." }, JsonRequestBehavior.AllowGet);
			}


			var status = new LoginStatus { CustomerId = 0 };
			switch (loginResult)
			{
				case CustomerLoginResults.Successful:
					{					

						status.CustomerId = _customerService.GetCustomerByUsername(model.UserId).Id;

						var currentCustomer = _customerService.GetCustomerById(status.CustomerId);

						status.FirstName = currentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
						status.LastName = currentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
						status.Gender = currentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Gender);
						status.Dob = currentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.DateOfBirth);
						status.Username = currentCustomer.Username;
						status.Email = currentCustomer.Email;

						var attributeList = _genericAttributeService.GetAttributesForEntity(currentCustomer.Id, "Customer");
						var customerPictureAttribute = attributeList.FirstOrDefault(x => x.Key == "CustomerPictureId");
						if (customerPictureAttribute != null)
						{
							status.UserImage = Convert.ToInt32(customerPictureAttribute.Value);

							var pictureSize = _mediaSettings.AvatarPictureSize;
							var picture = _pictureService.GetPictureById(status.UserImage);
							var pictureModel = new PictureModel
							{
								FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
								ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize)
							};
							status.UserPictureModel = pictureModel;
						}

						var personalNumberAttribute = attributeList.FirstOrDefault(x => x.Key == "PersonalNumber");

						//status.PersonalNumber = (personalNumberAttribute != null) ? Convert.ToInt32(personalNumberAttribute.Value) : 0;
					  	//status.PersonalNumber = (personalNumberAttribute != null) ? personalNumberAttribute.Value : currentCustomer.CompanyId.ToString("D3") + currentCustomer.PersonalId.ToString("D5");
						status.PersonalNumber = (personalNumberAttribute != null && personalNumberAttribute.Value != "0") ? personalNumberAttribute.Value : currentCustomer.CompanyId.ToString("D3") + currentCustomer.PersonalId.ToString("D5");
						
						status.CompanyId = currentCustomer.CompanyId;

						var AuthType = attributeList.FirstOrDefault(x => x.Key == "AuthorizationTypeId");
						status.AuthorizationTypeId = AuthType.Value;
					  

						var custRoles = currentCustomer.CustomerRoles.ToList();
						var roles = new StringBuilder();
						foreach (var role in custRoles)
						{
							roles.Append(role.Id);
							roles.Append(":");
							roles.Append(role.Name);
							roles.Append(",");
						}

						status.Customerroles = roles.ToString();
						status.CustomerId = currentCustomer.Id;
						status.Status = "Login Successfully";
						//migrate shopping cart

						if (guestCustomer != null)
						{
							_shoppingCartService.MigrateShoppingCart(guestCustomer, currentCustomer, true);
						}

						return Json(new { success = true, message = "", data = status }, JsonRequestBehavior.AllowGet);
						
					}

				case CustomerLoginResults.CustomerNotExist:
					{
						status.Status = "Customer not Exist";
						return Json(new { success = false, message = "", data = status }, JsonRequestBehavior.AllowGet);
					}

				case CustomerLoginResults.Deleted:
					{
						status.Status = "User Deleted";
						return Json(new { success = false, message = "", data = status }, JsonRequestBehavior.AllowGet);
					}

				case CustomerLoginResults.NotActive:
					{
						status.Status = "User Not Active";
						return Json(new { success = false, message = "", data = status }, JsonRequestBehavior.AllowGet);
					}

				case CustomerLoginResults.NotRegistered:
					{
						status.Status = "User Not Registered";
						return Json(new { success = false, message = "", data = status }, JsonRequestBehavior.AllowGet);
					}

				case CustomerLoginResults.WrongPassword:
				default:
					{
						status.Status = "Wrong password";
						return Json(new { success = false, message = "", data = status }, JsonRequestBehavior.AllowGet);
					}

			}

		}

		#endregion

		#region CurrentCustomer

		public object CreateGuest()
		{

			var customer = _customerService.InsertGuestCustomer();

			var model = new CustomerInfoModel
			{
				Id = customer.Id,
				FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
				LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
				Email = customer.Email
			};	 			
			return Json(new { success = true, message = "", data = model }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Forgot Password
		public ActionResult PasswordRecoverySend(string details)
		{
			var model = new JavaScriptSerializer().Deserialize<PasswordRecoveryModel>(details);

			var customer = _customerService.GetCustomerByUsername(model.Username);
			if (customer == null || !customer.Active || customer.Deleted)
				return Json(new { success = false, message = "Username does not exists.." }, JsonRequestBehavior.AllowGet);

			//var customer = _customerService.GetCustomerByEmail(model.Email); 			
			//if (customer == null || !customer.Active || customer.Deleted)
			//	return Json(new { success = false, message = "Email not found" }, JsonRequestBehavior.AllowGet);


			if (customer.Email == null)
				return Json(new { success = false, message = "No Emails found.." }, JsonRequestBehavior.AllowGet);

		  
			//save token and current date
			var passwordRecoveryToken = Guid.NewGuid();
			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.PasswordRecoveryToken,
				passwordRecoveryToken.ToString());
			DateTime? generatedDateTime = DateTime.UtcNow;
			_genericAttributeService.SaveAttribute(customer,
				SystemCustomerAttributeNames.PasswordRecoveryTokenDateGenerated, generatedDateTime);

			var languageid = _languageService.GetAllLanguages().FirstOrDefault().Id;

			//send email
			_workflowMessageService.SendCustomerPasswordRecoveryMessage(customer, languageid);
			return Json(new { success = true, message = "Email is sent  to reset password" }, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region My account / Change password


		public ActionResult ChangePassword(string details)
		{
			if (string.IsNullOrEmpty(details))
				return Json(new { success = false, message = "Invalid request." }, JsonRequestBehavior.AllowGet);

			var model = new JavaScriptSerializer().Deserialize<ChangePasswordModel>(details);
			if (!string.IsNullOrEmpty(model.CustomerId.ToString(CultureInfo.InvariantCulture)) && model.CustomerId == 0)
				return Json(new { success = false, message = "No Customer found." }, JsonRequestBehavior.AllowGet);

			
			var customer = _customerService.GetCustomerById(model.CustomerId);
			if (customer == null)
				return Json(new { success = false, message = "No Customer found." }, JsonRequestBehavior.AllowGet);

			if (string.IsNullOrEmpty(model.OldPassword) || string.IsNullOrEmpty(model.NewPassword) || string.IsNullOrEmpty(model.ConfirmNewPassword))
				return Json(new { success = false, message = "Invalid details." }, JsonRequestBehavior.AllowGet);

			if (model.NewPassword != model.ConfirmNewPassword)
				return Json(new { success = false, message = "New password and confirm password do not match." }, JsonRequestBehavior.AllowGet);

			var changePasswordRequest = new ChangePasswordRequest(customer.Email,
					 true, _customerSettings.DefaultPasswordFormat, model.NewPassword, model.OldPassword);
			var changePasswordResult = _customerRegistrationService.ChangePassword(changePasswordRequest);
			if (changePasswordResult.Success)
			{
				var result = _localizationService.GetResource("Account.ChangePassword.Success");
				return Json(new { success = true, message = result }, JsonRequestBehavior.AllowGet);
			}

			//If we got this far, something failed, redisplay form
			return Json(new { success = false, message = "Error occured.", data = changePasswordResult.Errors }, JsonRequestBehavior.AllowGet);
		}

		#endregion





		#region Generate SHA1
		public ActionResult GenerateSha1(string details, int customerId, string corderId)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(details);
			SHA1 sha = new SHA1CryptoServiceProvider();
			var hash = sha.ComputeHash(bytes);
			var sb = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
				sb.Append(hash[i].ToString("x2"));
			//return sb.ToString();
			var customer = _customerService.GetCustomerById(customerId);
			int storeId = 0;
			if (_storeService.GetAllStores().FirstOrDefault() != null)
			{
				storeId = _storeService.GetAllStores().FirstOrDefault().Id;
			}
			var att1 = new GenericAttribute
			{
				EntityId = customer.Id,
				KeyGroup = "Customer",
				StoreId = storeId,
				Key = "PaymentOrderReferences",
				Value = corderId
			};
			_genericAttributeService.InsertAttribute(att1);

			return Json(new { success = true, message = "", data = sb.ToString() }, JsonRequestBehavior.AllowGet);
		}

		#endregion
	}
}
