using Autofac;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.WebApi.Factories;

namespace Nop.Plugin.WebApi.Infrastructure
{
    /// <summary>
    /// Dependency registrar
    /// </summary>
    public class DependencyRegistrar : IDependencyRegistrar
    {
        /// <summary>
        /// Register services and interfaces
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="typeFinder">Type finder</param>
        /// <param name="config">Config</param>
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
			  //builder.RegisterType<JsonFieldsSerializer>().As<IJsonFieldsSerializer>().InstancePerLifetimeScope();

            builder.RegisterType<CustomerModelFactory>().As<ICustomerModelFactory>()
                .InstancePerLifetimeScope();
            builder.RegisterType<ShoppingCartModelFactory>().As<IShoppingCartModelFactory>()
                .InstancePerLifetimeScope();
            builder.RegisterType<AddressModelFactory>().As<IAddressModelFactory>()
              .InstancePerLifetimeScope();
				builder.RegisterType<ProductModelFactory>().As<IProductModelFactory>()
					 .InstancePerLifetimeScope();
				builder.RegisterType<OrderModelFactory>().As<IOrderModelFactory>()
				 .InstancePerLifetimeScope();
				builder.RegisterType<TopicModelFactory>().As<ITopicModelFactory>()
				 .InstancePerLifetimeScope();


				builder.RegisterType<ReturnRequestModelFactory>().As<IReturnRequestModelFactory>()
			 .InstancePerLifetimeScope();

				builder.RegisterType<CountryModelFactory>().As<ICountryModelFactory>()
			 .InstancePerLifetimeScope();

				builder.RegisterType<BlogModelFactory>().As<IBlogModelFactory>()
			 .InstancePerLifetimeScope();
				builder.RegisterType<NewsModelFactory>().As<INewsModelFactory>()
			 .InstancePerLifetimeScope();


  
        }

        /// <summary>
        /// Order of this dependency registrar implementation
        /// </summary>
        public int Order
        {
            get { return 1; }
        }
    }
}
