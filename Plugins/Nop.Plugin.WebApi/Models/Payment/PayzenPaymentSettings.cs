﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Configuration;

namespace Nop.Plugin.WebApi.Models.Payment
{	
	public class PayzenPaymentSettings : ISettings
	{
		public bool UseSandbox { get; set; }
		public string ShopId { get; set; }
		public string TestCertificate { get; set; }
		public string ProductionCertificate { get; set; }
	}
}
