﻿namespace Nop.Plugin.WebApi.Models.ShoppingCart
{
   public class AddToShoppingCartModel
    {

		  public int EnteredQuantity { get; set; }
        public int ShoppingCartType { get; set; }
        public int CustomerId { get; set; }
        public int productId { get; set; }
		  public int cartItemId{get;set;}

	 	  public string ProductAttributeValues { get; set; }
    }	

}
