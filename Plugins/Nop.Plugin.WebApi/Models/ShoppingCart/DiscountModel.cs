﻿using Nop.Plugin.WebApi.Models.Media;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.WebApi.Models.ShoppingCart
{
    public partial class DiscountModel : BaseNopEntityModel
    {
	   
	    public string DiscountCode { get; set; }
		 public int UserId { get; set; }
		  
    }
}