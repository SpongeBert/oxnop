﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.WebApi.Models.Product
{
    public partial class ProductTagModel : BaseNopEntityModel
    {
        public string Name { get; set; }

        public string SeName { get; set; }

        public int ProductCount { get; set; }
    }
}