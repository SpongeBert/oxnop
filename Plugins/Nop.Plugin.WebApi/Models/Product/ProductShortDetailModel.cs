﻿namespace Nop.Plugin.WebApi.Models.Product
{
	public class ProductShortDetailModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string SKU { get; set; }
		public string Comments { get; set; }
	}
}
