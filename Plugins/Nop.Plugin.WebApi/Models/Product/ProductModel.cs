﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Stores;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Mvc;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Core.Domain.Orders;
using Nop.Plugin.WebApi.Models.Media;

namespace Nop.Plugin.WebApi.Models.Product
{
	public class ProductModel : BaseNopEntityModel
	{
		private ICollection<ProductCategory> _productCategories;
		private ICollection<ProductPicture> _productPictures;

		public IList<ProductModel> AssociatedProducts { get; set; }

		public int ProductId { get; set; }

		public ProductModel()
		{
			_productCategories = new List<ProductCategory>();
			_productPictures = new List<ProductPicture>();
			ImageURL = new List<string>();
			Products = new List<ProductModel>();
			AssociatedProducts = new List<ProductModel>();
		  // 7 june
			ProductPrice = new ProductPriceModel();
			AddToCart = new AddToCartModel();
			ProductAttributes = new List<ProductAttributeModel>();
			SpecificationAttributeModels = new List<ProductSpecificationModel>();
			TierPrices = new List<TierPriceModel>();
			DefaultPictureModel = new PictureModel();
			PictureModels = new List<PictureModel>();
		
			//end 7 june

		}

	  /// <summary>
	  /// 7 june
	  /// </summary>
		public ProductPriceModel ProductPrice { get; set; }
		public AddToCartModel AddToCart { get; set; }
		public IList<ProductAttributeModel> ProductAttributes { get; set; }

		public GiftCardModel GiftCard { get; set; }

		public bool FreeShippingNotificationEnabled { get; set; }
		public string DeliveryDate { get; set; }
	
		public string StockAvailability { get; set; }	
		public bool DisplayBackInStockSubscription { get; set; }
		public IList<ProductSpecificationModel> ProductSpecifications { get; set; }
		public IList<TierPriceModel> TierPrices { get; set; }
		public bool DisplayDiscontinuedMessage { get; set; } 
		public string CurrentStoreName { get; set; }
		public IList<ProductSpecificationModel> SpecificationAttributeModels { get; set; }
		//picture
		public PictureModel DefaultPictureModel { get; set; }
		public IList<PictureModel> PictureModels { get; set; }
	 //end 7 june

		public ICollection<ProductModel> Products { get; set; }


		public string HomePagepictureUrl { get; set; }

		public List<string> ImageURL { get; set; }

		/// <summary>
		/// Gets or sets the product type identifier
		/// </summary>
		//public int ProductTypeId { get; set; }
		public ProductType ProductType { get; set; }
		/// <summary>
		/// Gets or sets the parent product identifier. It's used to identify associated products (only with "grouped" products)
		/// </summary>
		public int ParentGroupedProductId { get; set; }
		/// <summary>
		/// Gets or sets the values indicating whether this product is visible in catalog or search results.
		/// It's used when this product is associated to some "grouped" one
		/// This way associated products could be accessed/added/etc only from a grouped product details page
		/// </summary>
		public bool VisibleIndividually { get; set; }

		/// <summary>
		/// Gets or sets the name
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Gets or sets the short description
		/// </summary>
		public string ShortDescription { get; set; }
		/// <summary>
		/// Gets or sets the full description
		/// </summary>
		public string FullDescription { get; set; }

		/// <summary>
		/// Gets or sets the admin comment
		/// </summary>
		public string AdminComment { get; set; }

		/// <summary>
		/// Gets or sets a value of used product template identifier
		/// </summary>
		public int ProductTemplateId { get; set; }

		/// <summary>
		/// Gets or sets a vendor identifier
		/// </summary>
		public int VendorId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to show the product on home page
		/// </summary>
		public bool ShowOnHomePage { get; set; }

		/// <summary>
		/// Gets or sets the meta keywords
		/// </summary>
		public string MetaKeywords { get; set; }
		/// <summary>
		/// Gets or sets the meta description
		/// </summary>
		public string MetaDescription { get; set; }
		/// <summary>
		/// Gets or sets the meta title
		/// </summary>
		public string MetaTitle { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the product allows customer reviews
		/// </summary>
		public bool AllowCustomerReviews { get; set; }
		/// <summary>
		/// Gets or sets the rating sum (approved reviews)
		/// </summary>
		public int ApprovedRatingSum { get; set; }
		/// <summary>
		/// Gets or sets the rating sum (not approved reviews)
		/// </summary>
		public int NotApprovedRatingSum { get; set; }
		/// <summary>
		/// Gets or sets the total rating votes (approved reviews)
		/// </summary>
		public int ApprovedTotalReviews { get; set; }
		/// <summary>
		/// Gets or sets the total rating votes (not approved reviews)
		/// </summary>
		public int NotApprovedTotalReviews { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the entity is subject to ACL
		/// </summary>
		public bool SubjectToAcl { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether the entity is limited/restricted to certain stores
		/// </summary>
		public bool LimitedToStores { get; set; }

		/// <summary>
		/// Gets or sets the SKU
		/// </summary>
		public string Sku { get; set; }
		/// <summary>
		/// Gets or sets the manufacturer part number
		/// </summary>
		public string ManufacturerPartNumber { get; set; }
		/// <summary>
		/// Gets or sets the Global Trade Item Number (GTIN). These identifiers include UPC (in North America), EAN (in Europe), JAN (in Japan), and ISBN (for books).
		/// </summary>
		public string Gtin { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the product is gift card
		/// </summary>
		public bool IsGiftCard { get; set; }
		/// <summary>
		/// Gets or sets the gift card type identifier
		/// </summary>
		public int GiftCardTypeId { get; set; }
		/// <summary>
		/// Gets or sets gift card amount that can be used after purchase. If not specified, then product price will be used.
		/// </summary>
		public decimal? OverriddenGiftCardAmount { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the product requires that other products are added to the cart (Product X requires Product Y)
		/// </summary>
		public bool RequireOtherProducts { get; set; }
		/// <summary>
		/// Gets or sets a required product identifiers (comma separated)
		/// </summary>
		public string RequiredProductIds { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether required products are automatically added to the cart
		/// </summary>
		public bool AutomaticallyAddRequiredProducts { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the product is download
		/// </summary>
		public bool IsDownload { get; set; }
		/// <summary>
		/// Gets or sets the download identifier
		/// </summary>
		public int DownloadId { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether this downloadable product can be downloaded unlimited number of times
		/// </summary>
		public bool UnlimitedDownloads { get; set; }
		/// <summary>
		/// Gets or sets the maximum number of downloads
		/// </summary>
		public int MaxNumberOfDownloads { get; set; }
		/// <summary>
		/// Gets or sets the number of days during customers keeps access to the file.
		/// </summary>
		public int? DownloadExpirationDays { get; set; }
		/// <summary>
		/// Gets or sets the download activation type
		/// </summary>
		public int DownloadActivationTypeId { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether the product has a sample download file
		/// </summary>
		public bool HasSampleDownload { get; set; }
		/// <summary>
		/// Gets or sets the sample download identifier
		/// </summary>
		public int SampleDownloadId { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether the product has user agreement
		/// </summary>
		public bool HasUserAgreement { get; set; }
		/// <summary>
		/// Gets or sets the text of license agreement
		/// </summary>
		public string UserAgreementText { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the product is recurring
		/// </summary>
		public bool IsRecurring { get; set; }
		/// <summary>
		/// Gets or sets the cycle length
		/// </summary>
		public int RecurringCycleLength { get; set; }
		/// <summary>
		/// Gets or sets the cycle period
		/// </summary>
		public int RecurringCyclePeriodId { get; set; }
		/// <summary>
		/// Gets or sets the total cycles
		/// </summary>
		public int RecurringTotalCycles { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the product is rental
		/// </summary>
		public bool IsRental { get; set; }
		/// <summary>
		/// Gets or sets the rental length for some period (price for this period)
		/// </summary>
		public int RentalPriceLength { get; set; }
		/// <summary>
		/// Gets or sets the rental period (price for this period)
		/// </summary>
		public int RentalPricePeriodId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the entity is ship enabled
		/// </summary>
		public bool IsShipEnabled { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether the entity is free shipping
		/// </summary>
		public bool IsFreeShipping { get; set; }
		/// <summary>
		/// Gets or sets a value this product should be shipped separately (each item)
		/// </summary>
		public bool ShipSeparately { get; set; }
		/// <summary>
		/// Gets or sets the additional shipping charge
		/// </summary>
		public decimal AdditionalShippingCharge { get; set; }
		/// <summary>
		/// Gets or sets a delivery date identifier
		/// </summary>
		public int DeliveryDateId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the product is marked as tax exempt
		/// </summary>
		public bool IsTaxExempt { get; set; }
		/// <summary>
		/// Gets or sets the tax category identifier
		/// </summary>
		public int TaxCategoryId { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether the product is telecommunications or broadcasting or electronic services
		/// </summary>
		public bool IsTelecommunicationsOrBroadcastingOrElectronicServices { get; set; }

		/// <summary>
		/// Gets or sets a value indicating how to manage inventory
		/// </summary>
		public int ManageInventoryMethodId { get; set; }
		/// <summary>
		/// Gets or sets a product availability range identifier
		/// </summary>
		public int ProductAvailabilityRangeId { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether multiple warehouses are used for this product
		/// </summary>
		public bool UseMultipleWarehouses { get; set; }
		/// <summary>
		/// Gets or sets a warehouse identifier
		/// </summary>
		public int WarehouseId { get; set; }
		/// <summary>
		/// Gets or sets the stock quantity
		/// </summary>
		public int StockQuantity { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether to display stock availability
		/// </summary>
		public bool DisplayStockAvailability { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether to display stock quantity
		/// </summary>
		public bool DisplayStockQuantity { get; set; }
		/// <summary>
		/// Gets or sets the minimum stock quantity
		/// </summary>
		public int MinStockQuantity { get; set; }
		/// <summary>
		/// Gets or sets the low stock activity identifier
		/// </summary>
		public int LowStockActivityId { get; set; }
		/// <summary>
		/// Gets or sets the quantity when admin should be notified
		/// </summary>
		public int NotifyAdminForQuantityBelow { get; set; }
		/// <summary>
		/// Gets or sets a value backorder mode identifier
		/// </summary>
		public int BackorderModeId { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether to back in stock subscriptions are allowed
		/// </summary>
		public bool AllowBackInStockSubscriptions { get; set; }
		/// <summary>
		/// Gets or sets the order minimum quantity
		/// </summary>
		public int OrderMinimumQuantity { get; set; }
		/// <summary>
		/// Gets or sets the order maximum quantity
		/// </summary>
		public int OrderMaximumQuantity { get; set; }
		/// <summary>
		/// Gets or sets the comma seperated list of allowed quantities. null or empty if any quantity is allowed
		/// </summary>
		public string AllowedQuantities { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether we allow adding to the cart/wishlist only attribute combinations that exist and have stock greater than zero.
		/// This option is used only when we have "manage inventory" set to "track inventory by product attributes"
		/// </summary>
		public bool AllowAddingOnlyExistingAttributeCombinations { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether this product is returnable (a customer is allowed to submit return request with this product)
		/// </summary>
		public bool NotReturnable { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to disable buy (Add to cart) button
		/// </summary>
		public bool DisableBuyButton { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether to disable "Add to wishlist" button
		/// </summary>
		public bool DisableWishlistButton { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether this item is available for Pre-Order
		/// </summary>
		public bool AvailableForPreOrder { get; set; }
		/// <summary>
		/// Gets or sets the start date and time of the product availability (for pre-order products)
		/// </summary>
		public DateTime? PreOrderAvailabilityStartDateTimeUtc { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether to show "Call for Pricing" or "Call for quote" instead of price
		/// </summary>
		public bool CallForPrice { get; set; }
		/// <summary>
		/// Gets or sets the price
		/// </summary>
		public decimal Price { get; set; }
		/// <summary>
		/// Gets or sets the old price
		/// </summary>
		public decimal OldPrice { get; set; }
		/// <summary>
		/// Gets or sets the product cost
		/// </summary>
		public decimal ProductCost { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether a customer enters price
		/// </summary>
		public bool CustomerEntersPrice { get; set; }
		/// <summary>
		/// Gets or sets the minimum price entered by a customer
		/// </summary>
		public decimal MinimumCustomerEnteredPrice { get; set; }
		/// <summary>
		/// Gets or sets the maximum price entered by a customer
		/// </summary>
		public decimal MaximumCustomerEnteredPrice { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether base price (PAngV) is enabled. Used by German users.
		/// </summary>
		public bool BasepriceEnabled { get; set; }
		/// <summary>
		/// Gets or sets an amount in product for PAngV
		/// </summary>
		public decimal BasepriceAmount { get; set; }
		/// <summary>
		/// Gets or sets a unit of product for PAngV (MeasureWeight entity)
		/// </summary>
		public int BasepriceUnitId { get; set; }
		/// <summary>
		/// Gets or sets a reference amount for PAngV
		/// </summary>
		public decimal BasepriceBaseAmount { get; set; }
		/// <summary>
		/// Gets or sets a reference unit for PAngV (MeasureWeight entity)
		/// </summary>
		public int BasepriceBaseUnitId { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether this product is marked as new
		/// </summary>
		public bool MarkAsNew { get; set; }
		/// <summary>
		/// Gets or sets the start date and time of the new product (set product as "New" from date). Leave empty to ignore this property
		/// </summary>
		public DateTime? MarkAsNewStartDateTimeUtc { get; set; }
		/// <summary>
		/// Gets or sets the end date and time of the new product (set product as "New" to date). Leave empty to ignore this property
		/// </summary>
		public DateTime? MarkAsNewEndDateTimeUtc { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this product has tier prices configured
		/// <remarks>The same as if we run this.TierPrices.Count > 0
		/// We use this property for performance optimization:
		/// if this property is set to false, then we do not need to load tier prices navigation property
		/// </remarks>
		/// </summary>
		public bool HasTierPrices { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether this product has discounts applied
		/// <remarks>The same as if we run this.AppliedDiscounts.Count > 0
		/// We use this property for performance optimization:
		/// if this property is set to false, then we do not need to load Applied Discounts navigation property
		/// </remarks>
		/// </summary>
		public bool HasDiscountsApplied { get; set; }

		/// <summary>
		/// Gets or sets the weight
		/// </summary>
		public decimal Weight { get; set; }
		/// <summary>
		/// Gets or sets the length
		/// </summary>
		public decimal Length { get; set; }
		/// <summary>
		/// Gets or sets the width
		/// </summary>
		public decimal Width { get; set; }
		/// <summary>
		/// Gets or sets the height
		/// </summary>
		public decimal Height { get; set; }

		/// <summary>
		/// Gets or sets the available start date and time
		/// </summary>
		public DateTime? AvailableStartDateTimeUtc { get; set; }
		/// <summary>
		/// Gets or sets the available end date and time
		/// </summary>
		public DateTime? AvailableEndDateTimeUtc { get; set; }

		/// <summary>
		/// Gets or sets a display order.
		/// This value is used when sorting associated products (used with "grouped" products)
		/// This value is used when sorting home page products
		/// </summary>
		public int DisplayOrder { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether the entity is published
		/// </summary>
		public bool Published { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether the entity has been deleted
		/// </summary>
		public bool Deleted { get; set; }

		/// <summary>
		/// Gets or sets the date and time of product creation
		/// </summary>
		public DateTime CreatedOnUtc { get; set; }
		/// <summary>
		/// Gets or sets the date and time of product update
		/// </summary>
		public DateTime UpdatedOnUtc { get; set; }






		/// <summary>
		/// Gets or sets the product type
		/// </summary>
		//public ProductType ProductType
		//{
		//	get
		//	{
		//		return (ProductType)this.ProductTypeId;
		//	}
		//	set
		//	{
		//		this.ProductTypeId = (int)value;
		//	}
		//}

		/// <summary>
		/// Gets or sets the backorder mode
		/// </summary>
		public BackorderMode BackorderMode
		{
			get
			{
				return (BackorderMode)this.BackorderModeId;
			}
			set
			{
				this.BackorderModeId = (int)value;
			}
		}

		/// <summary>
		/// Gets or sets the download activation type
		/// </summary>
		public DownloadActivationType DownloadActivationType
		{
			get
			{
				return (DownloadActivationType)this.DownloadActivationTypeId;
			}
			set
			{
				this.DownloadActivationTypeId = (int)value;
			}
		}

		/// <summary>
		/// Gets or sets the gift card type
		/// </summary>
		public GiftCardType GiftCardType
		{
			get
			{
				return (GiftCardType)this.GiftCardTypeId;
			}
			set
			{
				this.GiftCardTypeId = (int)value;
			}
		}

		/// <summary>
		/// Gets or sets the low stock activity
		/// </summary>
		public LowStockActivity LowStockActivity
		{
			get
			{
				return (LowStockActivity)this.LowStockActivityId;
			}
			set
			{
				this.LowStockActivityId = (int)value;
			}
		}

		/// <summary>
		/// Gets or sets the value indicating how to manage inventory
		/// </summary>
		public ManageInventoryMethod ManageInventoryMethod
		{
			get
			{
				return (ManageInventoryMethod)this.ManageInventoryMethodId;
			}
			set
			{
				this.ManageInventoryMethodId = (int)value;
			}
		}

		/// <summary>
		/// Gets or sets the cycle period for recurring products
		/// </summary>
		public RecurringProductCyclePeriod RecurringCyclePeriod
		{
			get
			{
				return (RecurringProductCyclePeriod)this.RecurringCyclePeriodId;
			}
			set
			{
				this.RecurringCyclePeriodId = (int)value;
			}
		}

		/// <summary>
		/// Gets or sets the period for rental products
		/// </summary>
		public RentalPricePeriod RentalPricePeriod
		{
			get
			{
				return (RentalPricePeriod)this.RentalPricePeriodId;
			}
			set
			{
				this.RentalPricePeriodId = (int)value;
			}
		}

		public virtual ICollection<ProductCategory> ProductCategories
		{
			get { return _productCategories ?? (_productCategories = new List<ProductCategory>()); }
			protected set { _productCategories = value; }
		}

		public virtual ICollection<ProductPicture> ProductPictures
		{
			get { return _productPictures ?? (_productPictures = new List<ProductPicture>()); }
			protected set { _productPictures = value; }
		}


		public partial class TierPriceModel : BaseNopModel
		{
			public string Price { get; set; }

			public int Quantity { get; set; }
		}

		public partial class ProductAttributeModel : BaseNopEntityModel
		{
			public ProductAttributeModel()
			{
				AllowedFileExtensions = new List<string>();
				Values = new List<ProductAttributeValueModel>();
			}

			public int ProductId { get; set; }

			public int ProductAttributeId { get; set; }

			public string Name { get; set; }

			public string Description { get; set; }

			public string TextPrompt { get; set; }

			public bool IsRequired { get; set; }

			/// <summary>
			/// Default value for textboxes
			/// </summary>
			public string DefaultValue { get; set; }
			/// <summary>
			/// Selected day value for datepicker
			/// </summary>
			public int? SelectedDay { get; set; }
			/// <summary>
			/// Selected month value for datepicker
			/// </summary>
			public int? SelectedMonth { get; set; }
			/// <summary>
			/// Selected year value for datepicker
			/// </summary>
			public int? SelectedYear { get; set; }

			/// <summary>
			/// A value indicating whether this attribute depends on some other attribute
			/// </summary>
			public bool HasCondition { get; set; }

			/// <summary>
			/// Allowed file extensions for customer uploaded files
			/// </summary>
			public IList<string> AllowedFileExtensions { get; set; }

			public AttributeControlType AttributeControlType { get; set; }

			public IList<ProductAttributeValueModel> Values { get; set; }

		}

		public partial class ProductPriceModel : BaseNopModel
		{
			/// <summary>
			/// The currency (in 3-letter ISO 4217 format) of the offer price 
			/// </summary>
			public string CurrencyCode { get; set; }

			public string OldPrice { get; set; }

			public string Price { get; set; }
			public string PriceWithDiscount { get; set; }
			public decimal PriceValue { get; set; }

			public bool CustomerEntersPrice { get; set; }

			public bool CallForPrice { get; set; }

			public int ProductId { get; set; }

			public bool HidePrices { get; set; }

			//rental
			public bool IsRental { get; set; }
			public string RentalPrice { get; set; }

			/// <summary>
			/// A value indicating whether we should display tax/shipping info (used in Germany)
			/// </summary>
			public bool DisplayTaxShippingInfo { get; set; }
			/// <summary>
			/// PAngV baseprice (used in Germany)
			/// </summary>
			public string BasePricePAngV { get; set; }
		}

		public partial class ProductAttributeValueModel : BaseNopEntityModel
		{
			public ProductAttributeValueModel()
			{
				ImageSquaresPictureModel = new PictureModel();
			}

			public string Name { get; set; }

			public string ColorSquaresRgb { get; set; }

			//picture model is used with "image square" attribute type
			public PictureModel ImageSquaresPictureModel { get; set; }

			public string PriceAdjustment { get; set; }

			public decimal PriceAdjustmentValue { get; set; }

			public bool IsPreSelected { get; set; }

			//product picture ID (associated to this value)
			public int PictureId { get; set; }

			public bool CustomerEntersQty { get; set; }

			public int Quantity { get; set; }
		}

		public partial class AddToCartModel : BaseNopModel
		{
			public AddToCartModel()
			{
				this.AllowedQuantities = new List<SelectListItem>();
			}
			public int ProductId { get; set; }

			//qty
			[NopResourceDisplayName("Products.Qty")]
			public int EnteredQuantity { get; set; }
			public string MinimumQuantityNotification { get; set; }
			public List<SelectListItem> AllowedQuantities { get; set; }

			//price entered by customers
			[NopResourceDisplayName("Products.EnterProductPrice")]
			public bool CustomerEntersPrice { get; set; }
			[NopResourceDisplayName("Products.EnterProductPrice")]
			public decimal CustomerEnteredPrice { get; set; }
			public String CustomerEnteredPriceRange { get; set; }

			public bool DisableBuyButton { get; set; }
			public bool DisableWishlistButton { get; set; }

			//rental
			public bool IsRental { get; set; }

			//pre-order
			public bool AvailableForPreOrder { get; set; }
			public DateTime? PreOrderAvailabilityStartDateTimeUtc { get; set; }

			//updating existing shopping cart or wishlist item?
			public int UpdatedShoppingCartItemId { get; set; }
			public ShoppingCartType? UpdateShoppingCartItemType { get; set; }
		}

		public partial class GiftCardModel : BaseNopModel
		{
			public bool IsGiftCard { get; set; }

			[NopResourceDisplayName("Products.GiftCard.RecipientName")]
			[AllowHtml]
			public string RecipientName { get; set; }
			[NopResourceDisplayName("Products.GiftCard.RecipientEmail")]
			[AllowHtml]
			public string RecipientEmail { get; set; }
			[NopResourceDisplayName("Products.GiftCard.SenderName")]
			[AllowHtml]
			public string SenderName { get; set; }
			[NopResourceDisplayName("Products.GiftCard.SenderEmail")]
			[AllowHtml]
			public string SenderEmail { get; set; }
			[NopResourceDisplayName("Products.GiftCard.Message")]
			[AllowHtml]
			public string Message { get; set; }

			public GiftCardType GiftCardType { get; set; }
		}

	}
}
