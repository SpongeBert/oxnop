﻿namespace Nop.Plugin.WebApi.Models.Common
{
	public class LogModel
	{
		public string StoreName { get; set; }

		public string LogoPath { get; set; }

		public string LogoUrl { get; set; }
	}
}
