﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nop.Plugin.WebApi.Models.Common
{
	public class CountryModel
	{

		public CountryModel()
		{ 		
			this.AvailableCountries = new List<SelectListItem>();	 			
		}

		public string CountryName { get; set; } 
		public int CountryId { get; set; }
		public IList<SelectListItem> AvailableCountries { get; set; }
	}
}
