﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;

namespace Nop.Plugin.WebApi.Models.Common
{
	public class ProductOverviewModel
	{
		public int ProductId { get; set; }
		public string ProductName { get; set; }
		public string ShortDescription { get; set; }
		public string FullDescription { get; set; }
		public string SeName { get; set; }
		public ProductType ProductType { get; set; }
	}
}
