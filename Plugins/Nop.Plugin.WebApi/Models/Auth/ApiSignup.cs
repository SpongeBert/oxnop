﻿namespace Nop.Plugin.WebApi.Models.Auth
{
	public class ApiSignup
	{
		public string Id { get; set; }
		public string Username { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Gender { get; set; }
		public string Password { get; set; }		
	}
}
