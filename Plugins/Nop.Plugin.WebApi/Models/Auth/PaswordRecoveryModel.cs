﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.WebApi.Models.Auth
{															  
    public class PasswordRecoveryModel : BaseNopModel
    {	     												  
        public string Email { get; set; }

        public string Result { get; set; }

		  public string Username { get; set; }
    }
}