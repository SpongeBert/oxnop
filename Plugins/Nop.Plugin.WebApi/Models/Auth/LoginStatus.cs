﻿using Nop.Plugin.WebApi.Models.Media;
namespace Nop.Plugin.WebApi.Models.Auth
{
	public class LoginStatus
	{
		public int CustomerId { get; set; }
		public string Status { get; set; }

		public string Username { get; set; }
		public string Gender { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string Dob { get; set; }
		public string Customerroles { get; set; }
		public int UserImage { get; set; }
		public PictureModel UserPictureModel { get; set; }
		public string PersonalNumber { get; set; }
		public int CompanyId { get; set; }
		public string AuthorizationTypeId { get; set; }
	}
}
