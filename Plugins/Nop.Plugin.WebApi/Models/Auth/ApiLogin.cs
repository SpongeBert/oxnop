﻿namespace Nop.Plugin.WebApi.Models.Auth
{
	public class ApiLogin
	{
		public string UserId { get; set; }
		public string Password { get; set; }
		public int GuestId { get; set; }

	}
}
