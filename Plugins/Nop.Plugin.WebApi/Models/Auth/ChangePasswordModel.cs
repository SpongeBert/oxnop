﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.WebApi.Models.Auth
{
	public class ChangePasswordModel : BaseNopModel
	{
		public ChangePasswordModel()
		{
			CustomerId = 0;
		}

		public string OldPassword { get; set; }

		public string NewPassword { get; set; }

		public string ConfirmNewPassword { get; set; }

		public int CustomerId { get; set; }
	}
}
