﻿namespace Nop.Plugin.WebApi.Models.Email
{
	public class PDFContentModel
	{
		public string ItemName { get; set; }
		public int Qty { get; set; }
		public string Comments { get; set; }
	}
}
