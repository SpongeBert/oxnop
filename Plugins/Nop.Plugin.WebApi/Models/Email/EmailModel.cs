﻿using System.Collections.Generic;

namespace Nop.Plugin.WebApi.Models.Email
{
	public class EmailModel
	{
		public EmailModel()
		{
			pdfContent = new List<PDFContentModel>();
		}
		public string toEmailAddress { get; set; }
		public string toName { get; set; }
		public string attachmentFilePath { get; set; }
		public string attachmentFileName { get; set; }
		public string replyToEmailAddress { get; set; }
		public string replyToName { get; set; }
		public string fromEmail { get; set; }
		public string fromName { get; set; }
		public string subject { get; set; }
		public List<PDFContentModel> pdfContent { get; set; }

	}
}
