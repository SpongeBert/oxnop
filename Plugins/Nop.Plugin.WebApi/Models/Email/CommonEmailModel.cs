﻿namespace Nop.Plugin.WebApi.Models.Email
{
	public class CommonEmailModel
	{
		public CommonEmailModel()
		{
			
		}
		public string toEmailAddress { get; set; }
		public string toName { get; set; }		 		
		public string fromEmail { get; set; }
		public string fromName { get; set; }
		public string subject { get; set; }
		public string content { get; set; }


	}
}
