﻿using System;
using System.Collections.Generic;
using FluentValidation.Attributes;
using Nop.Plugin.WebApi.Models.Media;
using Nop.Plugin.WebApi.Models.News;
using Nop.Web.Framework.Mvc;


namespace Nop.Plugin.WebApi.Models.News
{
   
    public partial class NewsItemModel : BaseNopEntityModel
    {
        public NewsItemModel()
        {
            Comments = new List<NewsCommentModel>();
            AddNewComment = new AddNewsCommentModel();
				PictureModel = new PictureModel();
				PictureId = 0;
        }

        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string SeName { get; set; }

        public string Title { get; set; }
        public string Short { get; set; }
        public string Full { get; set; }
        public bool AllowComments { get; set; }
        public int NumberOfComments { get; set; }
        public DateTime CreatedOn { get; set; }

        public IList<NewsCommentModel> Comments { get; set; }
        public AddNewsCommentModel AddNewComment { get; set; }
		  #region Custom Code
		  
		  public int PictureId { get; set; }

		  public PictureModel PictureModel { get; set; }
		  #endregion
    }
}