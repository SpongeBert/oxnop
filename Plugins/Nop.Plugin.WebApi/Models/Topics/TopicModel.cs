﻿using Nop.Plugin.WebApi.Models.Media;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.WebApi.Models.Topics
{
    public partial class TopicModel : BaseNopEntityModel
    {
	    public TopicModel()
		 {
			 PictureModel = new PictureModel();
			 PictureId = 0;
	    }

	    public string SystemName { get; set; }

        public bool IncludeInSitemap { get; set; }

        public bool IsPasswordProtected { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public string MetaKeywords { get; set; }

        public string MetaDescription { get; set; }

        public string MetaTitle { get; set; }

        public string SeName { get; set; }

        public int TopicTemplateId { get; set; }
		  #region Custom Code

		  public int PictureId { get; set; }

		  public PictureModel PictureModel { get; set; }
		  #endregion
    }
}