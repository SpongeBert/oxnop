﻿using System.Collections.Generic;

namespace Nop.Plugin.WebApi.Models.Category
{
	public class CustomCategory
	{
		private ICollection<CustomSubCategories> _subCategories;
		public int Id { get; set; }
		public string Name { get; set; }
		public string ImageUrl { get; set; }
		public ICollection<CustomSubCategories> SubCategories
		{
			get
			{
				if (_subCategories == null)
				{
					_subCategories = new List<CustomSubCategories>();
				}

				return _subCategories;
			}
			set { _subCategories = value; }
		}
	}
}
