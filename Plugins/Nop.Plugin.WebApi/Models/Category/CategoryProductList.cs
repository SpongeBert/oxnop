﻿namespace Nop.Plugin.WebApi.Models.Category
{
	public class CategoryProductList
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public decimal Price { get; set; }

		public string Discount { get; set; }

		public string ImageUrl { get; set; }
		public int StockAvailability { get; set; }

		public bool FreeShipping { get; set; }
	}
}
