﻿using System.Collections.Generic;
namespace Nop.Plugin.WebApi.Models.Category
{
	public class HomePageSubCategoryModel
	{
		public HomePageSubCategoryModel()
		{
			SubCategoryList = new List<CustomSubCategories>();
		}				
		public int ParentCategoryId { get; set; }		 
		public List<CustomSubCategories> SubCategoryList { get; set; }	 		
	}
}
