﻿namespace Nop.Plugin.WebApi.Models.Category
{
	public class CustomSubCategories
	{
		public int SubCategoryId { get; set; }
		public string SubCategoryName { get; set; }
		public string SubCategoryImageUrl { get; set; }
	}
}
