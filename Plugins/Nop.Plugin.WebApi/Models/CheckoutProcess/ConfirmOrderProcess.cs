﻿using Newtonsoft.Json;
using Nop.Plugin.WebApi.Models.Addresses;

namespace Nop.Plugin.WebApi.Models.CheckoutProcess
{
	public class ConfirmOrderProcess
	{

		public ConfirmOrderProcess()
		{ 			
			//CartItem = new List<CompleteCheckoutProcessCartItems>();
		}

	//	private ICollection<CompleteCheckoutProcess> _checkoutShippingAddress;
		private AddressModel _shippingAddress;


		private AddressModel _billingAddress;
		//public ICollection<CompleteCheckoutProcessCartItems> _cartItem;

		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("customerId")]
		public string CustomerId { get; set; }

		//[JsonProperty("paymentStatus")]
		//public string PaymentStatus { get; set; }


		//[JsonProperty("paymentMethodSystemName")]
		//public string PaymentMethodSystemName { get; set; }


		public int BillingAddressID{get; set;}
		public int ShippingAddressID{get; set;}


		[JsonProperty("billingAddress")]
		public AddressModel BillingAddress
		{
			get
			{
				if (_billingAddress == null)
				{
					_billingAddress = new AddressModel();
				}

				return _billingAddress;
			}
			set { _billingAddress = value; }
		}

		[JsonProperty("shippingAddress")]
		public AddressModel ShippingAddress
		{
			get
			{
				if (_shippingAddress == null)
				{
					_shippingAddress = new AddressModel();
				}

				return _shippingAddress;
			}
			set { _shippingAddress = value; }
		}

		[JsonProperty("shipToSameAddress")]
		public bool ShipToSameAddress { get; set; }


		[JsonProperty("orderStatus")]
		public string OrderStatus { get; set; }

		[JsonProperty("orderTotal")]
		public decimal OrderTotal { get; set; }

		//[JsonProperty("cartItem")]
		//public ICollection<CompleteCheckoutProcessCartItems> CartItem
		//{
		//	get
		//	{
		//		if (_cartItem == null)
		//		{
		//			_cartItem = new List<CompleteCheckoutProcessCartItems>();
		//		}

		//		return _cartItem;
		//	}

		//	set { _cartItem = value; }
		//}

		public int PaymentStatusId { get; set; }

		public int OrderStatusId { get; set; }
	}
}
