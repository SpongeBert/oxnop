﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Nop.Core.Domain.Common;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.WebApi.Models.CheckoutProcess
{
	public class CompleteCheckoutProcess
	{		
		public CompleteCheckoutProcess()
		{	
			CartItem = new List<CompleteCheckoutProcessCartItems>();
			CheckoutAttributes = new List<CheckoutAttributeModel>();
		}

		private ICollection<CompleteCheckoutProcess> CheckoutShippingAddress;

		private Address _shippingAddress;

		private Address _billingAddress;
		public IList<CheckoutAttributeModel> CheckoutAttributes { get; set; }

		public ICollection<CompleteCheckoutProcessCartItems> _cartItem;

		[JsonProperty("id")]
		public string Id { get; set; }

		[JsonProperty("customerId")]
		public string CustomerId { get; set; }

		[JsonProperty("paymentStatus")]
		public string PaymentStatus { get; set; }
		public int CheckOutAttributeId { get; set; }
		public string CheckOutAttributeValueId { get; set; }
		public string CheckoutAttributeValues { get; set; }

		[JsonProperty("paymentMethodSystemName")]
		public string PaymentMethodSystemName { get; set; }
		public string CustomerCurrencyCode { get; set; }
		public int CustomerLanguageId { get; set; }
		public int ShippingStatus { get; set; }

		public int BillingAddressID { get; set; }
		public int ShippingAddressID { get; set; }

		[JsonProperty("billingAddress")]
		public Address BillingAddress
		{
			get
			{
				if (_billingAddress == null)
				{
					_billingAddress = new Address();
				}

				return _billingAddress;
			}
			set { _billingAddress = value; }
		}

		[JsonProperty("shippingAddress")]
		public Address ShippingAddress
		{
			get
			{
				if (_shippingAddress == null)
				{
					_shippingAddress = new Address();
				}

				return _shippingAddress;
			}
			set { _shippingAddress = value; }
		}

		[JsonProperty("shipToSameAddress")]
		public bool ShipToSameAddress { get; set; }


		[JsonProperty("orderStatus")]
		public string OrderStatus { get; set; }

		[JsonProperty("orderTotal")]
		public decimal OrderTotal { get; set; }

		[JsonProperty("cartItem")]
		public ICollection<CompleteCheckoutProcessCartItems> CartItem
		{
			get
			{
				if (_cartItem == null)
				{
					_cartItem = new List<CompleteCheckoutProcessCartItems>();
				}

				return _cartItem;
			}

			set { _cartItem = value; }
		}

		public int PaymentStatusId { get; set; }

		public int OrderStatusId { get; set; }

		public partial class CheckoutAttributeModel : BaseNopModel
		{
			public string AttId {get; set;}	 	

			public string AttValues { get; set; }
		}

		
	}
}
