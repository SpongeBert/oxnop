﻿using System;

namespace Nop.Plugin.WebApi.Models.CheckoutProcess
{
	public class CompleteCheckoutProcessCartItems
	{
		public int ProductId { get; set; }
		public int ProductQty { get; set; }
		public DateTime CreatedOn { get; set; }
	}
}
