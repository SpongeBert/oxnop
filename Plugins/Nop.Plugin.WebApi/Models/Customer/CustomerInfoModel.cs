﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Plugin.PostXCustom.Models.Public.Media;
using Nop.Plugin.WebApi.Models.Addresses;
using Nop.Plugin.WebApi.Models.Media;
using Nop.Web.Framework;

namespace Nop.Plugin.WebApi.Models.Customer
{
	public class CustomerInfoModel
	{
		public CustomerInfoModel()
		{
			//ExistingAddresses = new List<Addresses.AddressModel>();
			ExistingAddresses = new List<EditAddressModel>();
			AvailableCountries = new List<SelectListItem>();
			AvailableStates = new List<SelectListItem>(); 			
			PictureModel = new PublicPictureModel();
			PersonalPictureModel = new PublicPictureModel();
		}
		public int Id { get; set; }
		public string Username { get; set; }
		public string Gender { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string DOB { get; set; }

		//public IList<Addresses.AddressModel> ExistingAddresses { get; set; }

		public string CountryName { get; set; }
		public string StateProvinceName { get; set; }
		
		public int CountryId { get; set; }
		public IList<SelectListItem> AvailableCountries { get; set; }

		public int StateProvinceId { get; set; }
		public IList<SelectListItem> AvailableStates { get; set; }

		public IList<EditAddressModel> ExistingAddresses { get; set; }
		
	
		[UIHint("Picture")]
		[NopResourceDisplayName("Account.Fields.Picture")]
		public int PictureId { get; set; }

		public PublicPictureModel PictureModel { get; set; }

		[UIHint("Picture")]
		[NopResourceDisplayName("Account.Fields.Picture")]
		public int CustomerPictureId { get; set; }		  
		public PublicPictureModel PersonalPictureModel { get; set; }
		public string PersonalNumber { get; set; }
		public int CompanyId { get; set; }
		
		
	}
}
