﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Nop.Plugin.WebApi.Models.Addresses
{
	public class EditAddressModel
	{

		public EditAddressModel()
        {
            AvailableCountries = new List<SelectListItem>();
            AvailableStates = new List<SelectListItem>();	            
        }

		public int Id { get; set; }
		public string FirstName { get; set; }

		

		/// <summary>
		/// Gets or sets the last name
		/// </summary>
		public string LastName { get; set; }

		/// <summary>
		/// Gets or sets the email
		/// </summary>
		public string Email { get; set; }

		/// <summary>
		/// Gets or sets the company
		/// </summary>
		public string Company { get; set; }

		/// <summary>
		/// Gets or sets the city
		/// </summary>
		public string City { get; set; }

		/// <summary>
		/// Gets or sets the address 1
		/// </summary>
		public string Address1 { get; set; }

		/// <summary>
		/// Gets or sets the address 2
		/// </summary>
		public string Address2 { get; set; }

		/// <summary>
		/// Gets or sets the zip/postal code
		/// </summary>
		public string ZipPostalCode { get; set; }

		/// <summary>
		/// Gets or sets the phone number
		/// </summary>
		public string PhoneNumber { get; set; }

		/// <summary>
		/// Gets or sets the fax number
		/// </summary>
		public string FaxNumber { get; set; }

		/// <summary>
		/// Gets or sets the country
		/// </summary>
		public string Country { get; set; }

		public string State { get; set; }

		
		public int? CountryId { get; set; }
		public int? StateProvinceId { get; set; }
		public string CountryName { get; set; }
		public string StateProvinceName { get; set; }

		public IList<SelectListItem> AvailableCountries { get; set; }
		public IList<SelectListItem> AvailableStates { get; set; }

		
	}
}
