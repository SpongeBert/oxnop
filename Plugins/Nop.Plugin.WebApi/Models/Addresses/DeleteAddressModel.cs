﻿namespace Nop.Plugin.WebApi.Models.Addresses
{
	public class DeleteAddressModel
	{
		public DeleteAddressModel()
		{
			CustomerId = 0;
			AddressId = 0;
		}
		public int CustomerId { get; set; }

		public int AddressId { get; set; }
	}
}
