﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.WebApi
{
	public partial class RouteProvider : IRouteProvider
	{
		public void RegisterRoutes(RouteCollection routes)
		{
			#region Banner
			routes.MapRoute("HomePageSlider",
				  "api/homePageSlider",
				  new { controller = "Slider", action = "PublicBannerInfo" },
				  new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region Sample Rought

			routes.MapRoute("auth",
				  "api/auth",
				  new { controller = "Demo", action = "Auth" },
				  new[] { "Nop.Plugin.WebApi.Controllers" }
			);

			routes.MapRoute("check",
				  "api/check",
				  new { controller = "Demo", action = "Check" },
				  new[] { "Nop.Plugin.WebApi.Controllers" }
			);

			routes.MapRoute("postjson",
				  "api/postjson",
				  new { controller = "Demo", action = "Postjson" },
				  new[] { "Nop.Plugin.WebApi.Controllers" }
			);

			#endregion

			#region Register-Login

			routes.MapRoute("apilogin",
				  "api/apilogin/{json}",
				  new { controller = "Auth", action = "LoginCustomer", json = UrlParameter.Optional },
				  new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("apisignup",
				  "api/apisignup",
				  new { controller = "Auth", action = "RegisterCustomer" },
				  new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region Categories

			routes.MapRoute("apiallcategories",
				 "api/allcategories",
				 new { controller = "ApiCategory", action = "AllCategories" },
				 new[] { "Nop.Plugin.WebApi.Controllers" });
			routes.MapRoute("apisubcategories",
				"api/allSubCategories/{categoryId}",
				new { controller = "ApiCategory", action = "GetSubCategories", categoryId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("apisubcategoriesbyname",
				"api/allSubCategoriesbyname/{categoryName}/{userId}",
				new { controller = "ApiCategory", action = "GetSubCategoriesbyName", categoryName = UrlParameter.Optional,userId=UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("productsbycategoryid",
				 "api/category/{id}",
				 new { controller = "ApiCategory", action = "GetProductsByCategoryId" },
				 new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("productsbyfeaturedcategoryid",
				 "api/featuredCategories/{id}",
				 new { controller = "ApiCategory", action = "GetProductsByCategoryId" },
				 new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("featuredcategories",
				 "api/featuredCategories",
				 new { controller = "ApiCategory", action = "FeaturedCategories" },
				 new[] { "Nop.Plugin.WebApi.Controllers" });


			routes.MapRoute("productsbyCatName",
		 "api/GetProducts/{category}/{CuserId}",
		 new { controller = "ApiCategory", action = "GetCatProducts", category = UrlParameter.Optional,CuserId = UrlParameter.Optional },
		 new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("productsbyCatId",
		 "api/GetProductsByCatId/{categoryId}/{CuserId}",
		 new { controller = "ApiCategory", action = "GetCatProductsByCatId", categoryId = UrlParameter.Optional, CuserId = UrlParameter.Optional },
		 new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region Logo

			routes.MapRoute("Logo",
				  "api/applogo",
				  new { controller = "ApiCommon", action = "Logo" },
				  new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region AutoSearch

			routes.MapRoute("AutoSearch",
							"api/autoSearch/{term}",
							new { controller = "ApiCommon", action = "SearchTermAutoComplete" },
							new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region Product

			//routes.MapRoute("ProductsDetailsById",
			//					"api/ProductsDetails/{id}",
			//					new { controller = "ApiProduct", action = "GetProductById" },
			//					new[] { "Nop.Plugin.WebApi.Controllers" }
			//		 );

			routes.MapRoute("ProductsDetailsById",
						"api/ProductsDetails/{productId}/{userid}",
						new { controller = "ApiProduct", action = "ProductDetails" },
						new[] { "Nop.Plugin.WebApi.Controllers" }
			 );

			routes.MapRoute("HomePageFeaturedProduct",
					  "api/featuredProduct",
					  new { controller = "ApiProduct", action = "FeaturedProducts" },
					  new[] { "Nop.Plugin.WebApi.Controllers" }
			);

			routes.MapRoute("HomePageFeaturedProductById",
				  "api/featuredProduct/{sku}",
				  new { controller = "ApiProduct", action = "GetProductById" },
				  new[] { "Nop.Plugin.WebApi.Controllers" }
			);

			routes.MapRoute("ProductsDetailsBySKU",
				  "api/ProductsDetailsBySKU/{sku}",
				  new { controller = "ApiProduct", action = "GetProductBySKU" },
				  new[] { "Nop.Plugin.WebApi.Controllers" }
			);

			//	routes.MapRoute("RecentlyViewedv",
			//	  "api/RecentlyViewed",
			//	  new { controller = "ApiProduct", action = "RecentlyViewedProducts" },
			//	  new[] { "Nop.Plugin.WebApi.Controllers" }
			//);

			routes.MapRoute("CreateGuest",
			  "api/CreateGuest",
			  new { controller = "Auth", action = "CreateGuest" },
			  new[] { "Nop.Plugin.WebApi.Controllers" }
		);


			routes.MapRoute("ProductDetailsAttributeChange",
			  "api/ProductDetails_AttributeChange/{productId}/{details}/{userId}",
			  new { controller = "ApiShoppingCart", action = "ProductDetails_AttributeChange", productId = UrlParameter.Optional, details = UrlParameter.Optional, userId = UrlParameter.Optional },
			  new[] { "Nop.Plugin.WebApi.Controllers" }
		);

			#endregion

			#region ShoppingCart

			routes.MapRoute("CheckoutProcess",
							  "api/checkoutprocess/{details}",
							  new { controller = "ApiCheckoutProcess", action = "CompleteCheckoutProcess", details=UrlParameter.Optional},
							  new[] { "Nop.Plugin.WebApi.Controllers" }
					);



			routes.MapRoute("UserShoppingCart",
									"api/usershoppingcart/{userId}",
									new { controller = "ApiShoppingCart", action = "GetUserCartItems", userId = UrlParameter.Optional },
									new[] { "Nop.Plugin.WebApi.Controllers" }
					  );

			routes.MapRoute("UserWishlist",
								"api/userWishlist/{userId}",
								new { controller = "ApiShoppingCart", action = "GetWishlistItems", userId = UrlParameter.Optional },
								new[] { "Nop.Plugin.WebApi.Controllers" }
				  );


			routes.MapRoute("AddTooCart",
						  "api/addtoocart/{details}",
						  new { controller = "ApiShoppingCart", action = "AddProductToCart_Details", details = UrlParameter.Optional },
							 new[] { "Nop.Plugin.WebApi.Controllers" });


			routes.MapRoute("DeleteItemsFromUserCart",
								"api/deleteItemsfromUsercart/{details}",
								new { controller = "ApiShoppingCart", action = "UpdateCart", details = UrlParameter.Optional },
								new[] { "Nop.Plugin.WebApi.Controllers" }
					 );

			routes.MapRoute("DeleteItemsFromWishlist",
							"api/deleteWishlist/{details}",
							new { controller = "ApiShoppingCart", action = "UpdateWishlist", details = UrlParameter.Optional },
							new[] { "Nop.Plugin.WebApi.Controllers" }
				 );

			routes.MapRoute("UpdateCart",
							"api/UpdateCart/{details}",
							new { controller = "ApiShoppingCart", action = "UpdateUserCart", details = UrlParameter.Optional },
							new[] { "Nop.Plugin.WebApi.Controllers" }
				 );



			#region Generate SHA

			routes.MapRoute("CreateSha1",
						"api/GetSHA/{details}/{CustomerId}/{corderId}",
						new { controller = "Auth", action = "GenerateSha1", details = UrlParameter.Optional, CustomerId = UrlParameter.Optional, corderId = UrlParameter.Optional },
						new[] { "Nop.Plugin.WebApi.Controllers" }
			 );

			routes.MapRoute("TransactionReference",
					"api/GetTransactionreference/{corderId}",
					new { controller = "ApiOrder", action = "GetTransactionReference",  corderId = UrlParameter.Optional },
					new[] { "Nop.Plugin.WebApi.Controllers" }
		 );


			routes.MapRoute("OrderConfirm",
				"api/PlaceOnConfirm/{OrderId}",
				new { controller = "ApiOrder", action = "PlaceOnConfirm", OrderId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" }
	 );

			routes.MapRoute("OrderConfirmA",
				"api/PlaceOnConfirmA/{OrderId}",
				new { controller = "ApiOrder", action = "PlaceOnConfirmAndroid", OrderId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" }
	 );


			routes.MapRoute("IOSOrder",
			"api/PlaceOnIOS/{OrderId}",
			new { controller = "ApiOrder", action = "PlaceOnConfirmIOS", OrderId = UrlParameter.Optional },
			new[] { "Nop.Plugin.WebApi.Controllers" }
 );

			routes.MapRoute("GenerateSeal",
				"api/GetSeal/{OrderId}",
				new { controller = "ApiOrder", action = "GenerateSeal", OrderId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" }
	 );


			routes.MapRoute("OrderOnBCMC",
				"api/PlaceOnBCMC/{OrderId}",
				new { controller = "ApiOrder", action = "PlaceOnBCMC", OrderId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" }
	 );


			routes.MapRoute("PayzenOrder",
		"api/PlaceOnPayzenn/{OrderId}",
		new { controller = "ApiOrder", action = "PlaceOnPayzen", OrderId = UrlParameter.Optional },
		new[] { "Nop.Plugin.WebApi.Controllers" }
);

			
			
			#endregion




			#endregion

			#region Customer

			routes.MapRoute("allcustomer",
							 "api/allcustomer",
							 new { controller = "ApiCustomer", action = "AllCustomer" },
							 new[] { "Nop.Plugin.WebApi.Controllers" }
				);

			routes.MapRoute("customerInformation",
							 "api/customerInformation/{UserId}",
							 new { controller = "ApiCustomer", action = "CustomerInfo", UserId = UrlParameter.Optional },
							 new[] { "Nop.Plugin.WebApi.Controllers" }
				);

			routes.MapRoute("editcustomerinformation",
							 "api/editcustomerinformation",
							 new { controller = "ApiCustomer", action = "EditCustomerInfo" },
							 new[] { "Nop.Plugin.WebApi.Controllers" }
				);

			routes.MapRoute("editcustomeraddress",
							 "api/editcustomeraddress",
							 new { controller = "ApiCustomer", action = "EditCustomerAddress" },
							 new[] { "Nop.Plugin.WebApi.Controllers" }
				);

			routes.MapRoute("newcustomeraddress",
							 "api/newcustomeraddress",
							 new { controller = "ApiCustomer", action = "NewCustomerAddress" },
							 new[] { "Nop.Plugin.WebApi.Controllers" }
				);

				routes.MapRoute("loadcustomeraddress",
							 "api/loadAddress/{addressId}",
							 new { controller = "ApiCustomer", action = "GetAddress" },
							 new[] { "Nop.Plugin.WebApi.Controllers" }
				);
			


			routes.MapRoute("UserInfo",
						 "api/GetUserInfo/{UserId}",
						 new { controller = "ApiCustomer", action = "UserInfo", UserId = UrlParameter.Optional },
						 new[] { "Nop.Plugin.WebApi.Controllers" }
			);


			routes.MapRoute("customerRewards",
						 "api/customerRewardPoints/{UserId}",
						 new { controller = "ApiCustomer", action = "CustomerRewardPoints", UserId = UrlParameter.Optional },
						 new[] { "Nop.Plugin.WebApi.Controllers" }
			);


			#endregion

			#region SendEmail

			routes.MapRoute("SendEmail",
							"api/SendEmail",
							new { controller = "ApiCommon", action = "SendNotification" },
							new[] { "Nop.Plugin.WebApi.Controllers" });



			routes.MapRoute("SendCustomerEmail",
					"api/SendCustomerEmail",
					new { controller = "ApiCommon", action = "SendEmail" },
					new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region PDF

			routes.MapRoute("CreatePDF",
							"api/pdf",
							new { controller = "ApiCommon", action = "createPDF" },
							new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region Order

			routes.MapRoute("GetOrdersByCustomerId",
				  "api/GetCustomerOrders/{userId}",
				  new { controller = "ApiOrder", action = "CustomerOrders", userId = UrlParameter.Optional },
				  new[] { "Nop.Plugin.WebApi.Controllers" }
			);


			routes.MapRoute("GetOrderDetails",
				  "api/GetOrderDetails/{OrderId}",
				  new { controller = "ApiOrder", action = "OrdersDetail", OrderId = UrlParameter.Optional },
				  new[] { "Nop.Plugin.WebApi.Controllers" }
			);

			routes.MapRoute("DeleteOrders",
			  "api/DeleteOrder/{OrderId}",
			  new { controller = "ApiOrder", action = "DeleteOrder", OrderId = UrlParameter.Optional },
			  new[] { "Nop.Plugin.WebApi.Controllers" }
		);

			#endregion

			#region CheckStock

			routes.MapRoute("Checkstocks",
				  "api/CheckProductStock/{productname}",
				  new { controller = "ApiProduct", action = "Checkstock", productname = UrlParameter.Optional },
				  new[] { "Nop.Plugin.WebApi.Controllers" }
			);

			#endregion

			#region Related Products

			routes.MapRoute("RelatedProducts",
			  "api/GetRelatedProducts/{productId}",
			  new { controller = "ApiProduct", action = "RelatedProducts", productId = UrlParameter.Optional },
			  new[] { "Nop.Plugin.WebApi.Controllers" }
		);

			#endregion

			#region Forgot Password

			routes.MapRoute("Forgotpassword",
			  "api/forgot/{details}",
			  new { controller = "Auth", action = "PasswordRecoverySend", details = UrlParameter.Optional },
			  new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion


			#region CheckOut

			routes.MapRoute("Placeorder",
					  "api/ConfirmOrderdetails/{details}",
					  new { controller = "ApiCheckoutProcess", action = "ConfirmOrder", details = UrlParameter.Optional },
						 new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion


			#region Order Confirm

			routes.MapRoute("Confirm",
				  "api/Confirm/{details}",
				  new { controller = "ApiShoppingCart", action = "OrderSummary", details = UrlParameter.Optional },
					 new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("OrderTotal",
				  "api/OrderSummary/{details}",
				  new { controller = "ApiShoppingCart", action = "OrderTotals", details = UrlParameter.Optional },
					 new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region Order Notes
			routes.MapRoute("OrderNote",
			  "api/AddOrderNotes/{details}",
			  new { controller = "ApiOrder", action = "OrderNotesAdd", details = UrlParameter.Optional },
				 new[] { "Nop.Plugin.WebApi.Controllers" });


			#endregion


			#region HomePage Data
			routes.MapRoute("dayofferproducts",
	"api/dayofferproducts/{CuserId}",
	new { controller = "ApiCommon", action = "GetDayOfferProducts", CuserId = UrlParameter.Optional },
	new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("lunchproducts",
	"api/lunchproducts/{CuserId}",
	new { controller = "ApiCommon", action = "GetLunchProducts", CuserId =UrlParameter.Optional},
	new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("drinkproducts",
	"api/drinkproducts/{CuserId}",
	new { controller = "ApiCommon", action = "GetDrinkProducts", CuserId = UrlParameter.Optional },
	new[] { "Nop.Plugin.WebApi.Controllers" });
			#endregion

			#region Reorder
			routes.MapRoute("Reorders",
		  "api/Reorder/{OrderId}/{userId}",
		  new { controller = "ApiOrder", action = "ReOrder", OrderId = UrlParameter.Optional, userId = UrlParameter.Optional },
			 new[] { "Nop.Plugin.WebApi.Controllers" });
			#endregion

			#region Topics

			routes.MapRoute("GetTopic",
			 "api/GetTopicsdetails/{TopicId}",
			 new { controller = "ApiTopic", action = "TopicDetails", TopicId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region Return requests

			routes.MapRoute("GetCustomerReturnRequest",
		 "api/GetCustomerReturns/{CustomerId}",
		 new { controller = "ApiReturnRequest", action = "CustomerReturnRequests", CustomerId = UrlParameter.Optional },
			new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("GetReturnRequest",
		 "api/GetReturn/{OrderId}/{userId}",
		 new { controller = "ApiReturnRequest", action = "ReturnRequest", OrderId = UrlParameter.Optional, userId = UrlParameter.Optional },
			new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("SubmitReturn",
						  "api/SubmitReturnRequests/{details}",
						  new { controller = "ApiReturnRequest", action = "ReturnRequestSubmit", details = UrlParameter.Optional },
							 new[] { "Nop.Plugin.WebApi.Controllers" });


			#endregion


			#region Countries/States

			routes.MapRoute("GetStates",
		  "api/GetStatesByCountry/{CountryId}",
		  new { controller = "ApiCountry", action = "GetStatesByCountryId", CountryId = UrlParameter.Optional },
			 new[] { "Nop.Plugin.WebApi.Controllers" });


			routes.MapRoute("GetAllCountries",
		 "api/GetCountriesList",
		 new { controller = "ApiCountry", action = "GetAllCountries" },
			new[] { "Nop.Plugin.WebApi.Controllers" });


			#endregion

			#region change password

			routes.MapRoute("changepwd",
			  "api/changepassword/{details}",
			  new { controller = "Auth", action = "ChangePassword", userId = UrlParameter.Optional, details = UrlParameter.Optional },
			  new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion


			#region Blogs

			routes.MapRoute("GetBlogsList",
			 "api/BlogsList",
			 new { controller = "ApiBlog", action = "BlogLists", TopicId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });


			routes.MapRoute("BlogDetails",
			 "api/GetBlogDetails/{blogPostId}",
			 new { controller = "ApiBlog", action = "BlogPostDetails", blogPostId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region News

			routes.MapRoute("GetNewsList",
			 "api/NewsList",
			 new { controller = "ApiNews", action = "NewsList", TopicId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });


			routes.MapRoute("NewsDetails",
			 "api/GetNewsDetails/{newsItemId}",
			 new { controller = "ApiNews", action = "NewsItemDetails", newsItemId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region "Coupons"
			routes.MapRoute("GetCoupons",
			"api/ApplyCoupon/{details}",
			new { controller = "ApiShoppingCart", action = "ApplyDiscountCoupon", details = UrlParameter.Optional },
			  new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("RemoveCoupons",
			"api/DeleteCoupons/{details}",
			new { controller = "ApiShoppingCart", action = "RemoveDiscountCoupon", details = UrlParameter.Optional },
			  new[] { "Nop.Plugin.WebApi.Controllers" });
		
			#endregion


			#region "LoyalityPoints"
			routes.MapRoute("ApplyLoyalityPoints",
			"api/ApplyLoyalityPoints/{details}",
			new { controller = "ApiShoppingCart", action = "ApplyLoyaltyPoints", details = UrlParameter.Optional },
			  new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("DeleteLoyalityPoints",
		  "api/DeleteLoyalityPoints/{details}",
		  new { controller = "ApiShoppingCart", action = "RemoveLoyalty", details = UrlParameter.Optional },
			 new[] { "Nop.Plugin.WebApi.Controllers" });


			#endregion

			#region "email a freind"
			//product email a friend
			routes.MapRoute("EmailFriend",
								 "api/productemailfriend/{productId}",
								 new { controller = "ApiProduct", action = "ProductEmailAFriend", productId = UrlParameter.Optional },
								  new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("EmailSendToFriend",
								 "api/productsendtofriend/{details}",
								 new { controller = "ApiProduct", action = "ApiProductEmailSend", details = UrlParameter.Optional },
								  new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			#region "Product Reviews"
			routes.MapRoute("AllProductReviews",
							 "api/getproductreviews/{productId}/{userId}",
							 new { controller = "ApiProduct", action = "ProductReviews", productId = UrlParameter.Optional, userId = UrlParameter.Optional },
							  new[] { "Nop.Plugin.WebApi.Controllers" });

			routes.MapRoute("AddingProductReviews",
						 "api/AddReviews/{details}",
						 new { controller = "ApiProduct", action = "ProductReviewsAdd", details = UrlParameter.Optional },
						  new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion		  

			#region "Barcode"
			routes.MapRoute("Getbarcode",
							"api/generatebarcode/{personalNumber}",
							new { controller = "ApiCustomer", action = "GenerateBarcode", personalNumber = UrlParameter.Optional },
							 new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion

			 #region	"Mobile Redirection"
			//routes.MapRoute("MobileRedirection",
			//		"api/gotomobile/{orderId}",
			//		new { controller = "ApiCheckoutProcess", action = "SetOrderStatus", orderId = UrlParameter.Optional, result = UrlParameter.Optional },  				
			//		new[] { "Nop.Plugin.WebApi.Controllers" });
		  
			routes.MapRoute("MobileRedirection",
				"api/gotomobile/{orderId}",
				new { controller = "ApiCheckoutProcess", action = "SetRedirectionStatus", orderId = UrlParameter.Optional, result = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });


			//routes.MapRoute("NotificationRedirection",
			//		"api/gotoNotification/{orderId}",
			//		new { controller = "ApiCheckoutProcess", action = "SetRedirectionStatus", orderId = UrlParameter.Optional, result = UrlParameter.Optional },
			//		new[] { "Nop.Plugin.WebApi.Controllers" });
			routes.MapRoute("NotificationRedirection",
				"api/gotoNotification/{orderId}",
				new { controller = "ApiCheckoutProcess", action = "SetOrderStatus", orderId = UrlParameter.Optional, result = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });


			//routes.MapRoute("MobileRedirectionA",
			//		"api/gotomobilea/{orderId}",
			//		new { controller = "ApiCheckoutProcess", action = "SetOrderStatusAndroid", orderId = UrlParameter.Optional, result = UrlParameter.Optional },
			//		new[] { "Nop.Plugin.WebApi.Controllers" });


			routes.MapRoute("MobileRedirectionA",
					"api/gotomobilea/{orderId}",
					new { controller = "ApiCheckoutProcess", action = "SetRedirectionStatusAndroid", orderId = UrlParameter.Optional, result = UrlParameter.Optional },
					new[] { "Nop.Plugin.WebApi.Controllers" });

			//routes.MapRoute("NotificationRedirectionA",
			//		"api/gotoNotificationa/{orderId}",
			//		new { controller = "ApiCheckoutProcess", action = "SetRedirectionStatusAndroid", orderId = UrlParameter.Optional, result = UrlParameter.Optional },
			//		new[] { "Nop.Plugin.WebApi.Controllers" });
			routes.MapRoute("NotificationRedirectionA",
					"api/gotoNotificationa/{orderId}",
					new { controller = "ApiCheckoutProcess", action = "SetOrderStatusAndroid", orderId = UrlParameter.Optional, result = UrlParameter.Optional },
					new[] { "Nop.Plugin.WebApi.Controllers" });


			routes.MapRoute("UpdateOrderOnLS",
				"api/UpdateOrderToLS/{orderId}",
				new { controller = "ApiCheckoutProcess", action = "UpdateOrderStatusIfOrderZero", orderId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });


			routes.MapRoute("SipsDirect",
				"api/gotoSips/{orderId}",
				new { controller = "ApiCheckoutProcess", action = "SetDirectRedirection", orderId = UrlParameter.Optional },
				new[] { "Nop.Plugin.WebApi.Controllers" });


			

					routes.MapRoute("ComingBack",
				"api/BackToApp",
				new { controller = "ApiCheckoutProcess", action = "GotoMobileApp"},
				new[] { "Nop.Plugin.WebApi.Controllers" });

			#endregion


					#region PaymentMethods
					routes.MapRoute("GetPaymentMethod",
								 "api/GetPaymentMethods/{userId}",
								 new { controller = "ApiCheckoutProcess", action = "PaymentMethod", userId = UrlParameter.Optional },
								 new { userId = @"\d+" },
								 new[] { "Nop.Plugin.NopWebApi.Controllers" });

					routes.MapRoute("SetPaymentMethod",
									"api/SelectPaymentMethod/{paymentmethod}/{userId}",
									new { controller = "ApiCheckoutProcess", action = "SaveSelectedPaymentMethod", paymentmethod = UrlParameter.Optional, userId = UrlParameter.Optional },
									new[] { "Nop.Plugin.NopWebApi.Controllers" });

					routes.MapRoute("GetPaymentInfo",
								"api/SetPaymentInfo/{userId}",
								new { controller = "ApiCheckoutProcess", action = "PaymentInfo", userId = UrlParameter.Optional },
								new[] { "Nop.Plugin.NopWebApi.Controllers" });

					#endregion


					routes.MapRoute("PayzenRedirection",
						"api/gotoPayzenReturn/{orderId}",
						new { controller = "ApiCheckoutProcess", action = "SetPayzenRedirection", orderId = UrlParameter.Optional },
						new[] { "Nop.Plugin.WebApi.Controllers" });



		}
		public int Priority
		{
			get
			{
				return 0;
			}
		}
	}
}
