﻿using Nop.Core.Plugins;
using Nop.Services.Shipping.Tracking;

namespace Nop.Plugin.WebApi
{
    public class WebApiProvider : BasePlugin
    {
        #region Fields
        
        #endregion

        #region Ctor

        #endregion

        #region Properties

        /// <summary>
        /// Gets a shipment tracker
        /// </summary>
        public IShipmentTracker ShipmentTracker
        {
            get { return null; }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Install the plugin
        /// </summary>
        public override void Install()
        {
            base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override void Uninstall()
        {
            base.Uninstall();
		  }

        #endregion
    }
}
