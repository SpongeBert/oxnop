﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.Payzen.Controllers;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;

namespace Nop.Plugin.Payments.Payzen
{
	public class PayzenPaymentProcessor : BasePlugin, IPaymentMethod
	{
		#region Fields				
		private readonly ILocalizationService _localizationService;
		private readonly IOrderTotalCalculationService _orderTotalCalculationService;
		private readonly ISettingService _settingService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly IWorkContext _workContext;
		private readonly HttpContextBase _httpContext;
		private readonly IWebHelper _webHelper;

		#endregion

		#region Ctor

		public PayzenPaymentProcessor(ILocalizationService localizationService,
			 IOrderTotalCalculationService orderTotalCalculationService,
			 ISettingService settingService, IGenericAttributeService genericAttributeService, IWorkContext workContext,
			HttpContextBase httpContext, IWebHelper webHelper)
		{
			_localizationService = localizationService;
			_orderTotalCalculationService = orderTotalCalculationService;
			_settingService = settingService;
			_genericAttributeService = genericAttributeService;
			_workContext = workContext;
			_httpContext = httpContext;
			_webHelper = webHelper;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Process a payment
		/// </summary>
		/// <param name="processPaymentRequest">Payment info required for an order processing</param>
		/// <returns>Process payment result</returns>
		public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
		{
			var result = new ProcessPaymentResult();
			result.NewPaymentStatus = PaymentStatus.Pending;
			return result;
		}

		/// <summary>
		/// Post process payment (used by payment gateways that require redirecting to a third-party URL)
		/// </summary>
		/// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
		public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
		{
			
			var redirectUrl = _webHelper.GetStoreLocation(false) + "PaymentPayzen/ProcessPaymentInfo?orderId="+ postProcessPaymentRequest.Order.Id;
			_httpContext.Response.Redirect(redirectUrl);
			//nothing
		}

		/// <summary>
		/// Returns a value indicating whether payment method should be hidden during checkout
		/// </summary>
		/// <param name="cart">Shoping cart</param>
		/// <returns>true - hide; false - display.</returns>
		public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
		{
			//you can put any logic here
			//for example, hide this payment method if all products in the cart are downloadable
			//or hide this payment method if current customer is from certain country

			//var attributeList = _genericAttributeService.GetAttributesForEntity(_workContext.CurrentCustomer.Id, "Customer");
			//if (attributeList.Any())
			//{
			//	var genericAttribute = attributeList.FirstOrDefault(x => x.Key == "AuthorizationTypeId");
			//	if (genericAttribute == null)
			//		return true;

			//	if (genericAttribute.Value == "2")
			//		return false;
			//}

			return false;
		}

		/// <summary>
		/// Gets additional handling fee
		/// </summary>
		/// <param name="cart">Shoping cart</param>
		/// <returns>Additional handling fee</returns>
		public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
		{
			var result = this.CalculateAdditionalFee(_orderTotalCalculationService, cart, 0, false);
			return result;
		}

		/// <summary>
		/// Captures payment
		/// </summary>
		/// <param name="capturePaymentRequest">Capture payment request</param>
		/// <returns>Capture payment result</returns>
		public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
		{
			var result = new CapturePaymentResult();
			result.AddError("Capture method not supported");
			return result;
		}

		/// <summary>
		/// Refunds a payment
		/// </summary>
		/// <param name="refundPaymentRequest">Request</param>
		/// <returns>Result</returns>
		public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
		{
			var result = new RefundPaymentResult();
			result.AddError("Refund method not supported");
			return result;
		}

		/// <summary>
		/// Voids a payment
		/// </summary>
		/// <param name="voidPaymentRequest">Request</param>
		/// <returns>Result</returns>
		public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
		{
			var result = new VoidPaymentResult();
			result.AddError("Void method not supported");
			return result;
		}

		/// <summary>
		/// Process recurring payment
		/// </summary>
		/// <param name="processPaymentRequest">Payment info required for an order processing</param>
		/// <returns>Process payment result</returns>
		public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
		{
			var result = new ProcessPaymentResult();
			result.AddError("Recurring payment not supported");
			return result;
		}

		/// <summary>
		/// Cancels a recurring payment
		/// </summary>
		/// <param name="cancelPaymentRequest">Request</param>
		/// <returns>Result</returns>
		public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
		{
			var result = new CancelRecurringPaymentResult();
			result.AddError("Recurring payment not supported");
			return result;
		}

		/// <summary>
		/// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
		/// </summary>
		/// <param name="order">Order</param>
		/// <returns>Result</returns>
		public bool CanRePostProcessPayment(Order order)
		{
			if (order == null)
				throw new ArgumentNullException("order");

			//it's not a redirection payment method. So we always return false
			return false;
		}

		/// <summary>
		/// Gets a route for provider configuration
		/// </summary>
		/// <param name="actionName">Action name</param>
		/// <param name="controllerName">Controller name</param>
		/// <param name="routeValues">Route values</param>
		public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
		{
			actionName = "Configure";
			controllerName = "PaymentPayzen";
			routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Payzen.Controllers" }, { "area", null } };
		}

		/// <summary>
		/// Gets a route for payment info
		/// </summary>
		/// <param name="actionName">Action name</param>
		/// <param name="controllerName">Controller name</param>
		/// <param name="routeValues">Route values</param>
		public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
		{
			actionName = "PaymentInfo";
			controllerName = "PaymentPayzen";
			routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Payzen.Controllers" }, { "area", null } };
		}

		/// <summary>
		/// Get the type of controller
		/// </summary>
		/// <returns>Type</returns>
		public Type GetControllerType()
		{
			return typeof(PaymentPayzenController);
		}

		/// <summary>
		/// Install the plugin
		/// </summary>
		public override void Install()
		{
			var settings = new PayzenPaymentSettings
			{
				UseSandbox = true
			};
			_settingService.SaveSetting(settings);

			base.Install();
		}

		/// <summary>
		/// Uninstall the plugin
		/// </summary>
		public override void Uninstall()
		{
			_settingService.DeleteSetting<PayzenPaymentSettings>();
			base.Uninstall();
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value indicating whether capture is supported
		/// </summary>
		public bool SupportCapture
		{
			get { return false; }
		}

		/// <summary>
		/// Gets a value indicating whether partial refund is supported
		/// </summary>
		public bool SupportPartiallyRefund
		{
			get { return false; }
		}

		/// <summary>
		/// Gets a value indicating whether refund is supported
		/// </summary>
		public bool SupportRefund
		{
			get { return false; }
		}

		/// <summary>
		/// Gets a value indicating whether void is supported
		/// </summary>
		public bool SupportVoid
		{
			get { return false; }
		}

		/// <summary>
		/// Gets a recurring payment type of payment method
		/// </summary>
		public RecurringPaymentType RecurringPaymentType
		{
			get { return RecurringPaymentType.NotSupported; }
		}

		/// <summary>
		/// Gets a payment method type
		/// </summary>
		public PaymentMethodType PaymentMethodType
		{
			get { return PaymentMethodType.Standard; }
		}

		/// <summary>
		/// Gets a value indicating whether we should display a payment information page for this plugin
		/// </summary>
		public bool SkipPaymentInfo
		{
			get { return true; }
		}

		/// <summary>
		/// Gets a payment method description that will be displayed on checkout pages in the public store
		/// </summary>
		public string PaymentMethodDescription
		{
			//return description of this payment method to be display on "payment method" checkout step. good practice is to make it localizable
			//for example, for a redirection payment method, description may be like this: "You will be redirected to PayPal site to complete the payment"
			get { return _localizationService.GetResource("Plugins.Payment.Payzen.PaymentMethodDescription"); }
		}

		#endregion
	}
}
