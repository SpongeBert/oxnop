﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Plugin.Payments.Payzen.Models;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.CustomService;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Payments.Payzen.Controllers
{
	public class PaymentPayzenController : BasePaymentController
	{
		private readonly IWorkContext _workContext;
		private readonly IStoreService _storeService;
		private readonly ISettingService _settingService;
		private readonly ILocalizationService _localizationService;
		private readonly IOrderService _orderService;
		private readonly ILogger _logger;
		private readonly IOrderProcessingService _orderProcessingService;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICustomCodeService _customCodeService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;

		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly OrderSettings _orderSettings;
		private readonly LocalizationSettings _localizationSettings;
		private readonly IPdfService _pdfService;
		private readonly IRewardPointService _rewardPointService;
		private readonly IStoreContext _storeContext;


		public PaymentPayzenController(IWorkContext workContext, IStoreService storeService,
			ISettingService settingService, ILocalizationService localizationService, IOrderService orderService,
			ILogger logger, IOrderProcessingService orderProcessingService, ICmsNopOrderMappingService cmsNopOrderMappingService,
			ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICustomCodeService customCodeService, IGenericAttributeService genericAttributeService,
			ICmsNopProductMappingService cmsNopProductMappingService, IWorkflowMessageService workflowMessageService, OrderSettings orderSettings,
			LocalizationSettings localizationSettings, IPdfService pdfService, IRewardPointService rewardPointService, IStoreContext storeContext)
		{

			_workContext = workContext;
			_storeService = storeService;
			_settingService = settingService;
			_localizationService = localizationService;
			_orderService = orderService;
			_logger = logger;
			_orderProcessingService = orderProcessingService;
			_cmsNopOrderMappingService = cmsNopOrderMappingService;
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_customCodeService = customCodeService;
			_genericAttributeService = genericAttributeService;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_workflowMessageService = workflowMessageService;
			_orderSettings = orderSettings;
			_localizationSettings = localizationSettings;
			_pdfService = pdfService;
			_rewardPointService = rewardPointService;
			_storeContext = storeContext;
		}

		[AdminAuthorize]
		[ChildActionOnly]
		public ActionResult Configure()
		{
			//load settings for a chosen store scope
			var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
			var payzenPaymentSettings = _settingService.LoadSetting<PayzenPaymentSettings>(storeScope);

			var model = new ConfigurationModel();
			model.UseSandbox = payzenPaymentSettings.UseSandbox;
			model.ShopId = payzenPaymentSettings.ShopId;
			model.TestCertificate = payzenPaymentSettings.TestCertificate;
			model.ProductionCertificate = payzenPaymentSettings.ProductionCertificate;

			model.ActiveStoreScopeConfiguration = storeScope;
			if (storeScope > 0)
			{
				model.UseSandbox = _settingService.SettingExists(payzenPaymentSettings, x => x.UseSandbox, storeScope);
				model.ShopId_OverrideForStore = _settingService.SettingExists(payzenPaymentSettings, x => x.ShopId, storeScope);
				model.TestCertificate_OverrideForStore = _settingService.SettingExists(payzenPaymentSettings, x => x.TestCertificate, storeScope);
				model.ProductionCertificate_OverrideForStore = _settingService.SettingExists(payzenPaymentSettings, x => x.ProductionCertificate, storeScope);
			}
			return View("~/Plugins/Payments.Payzen/Views/PaymentPayzen/Configure.cshtml", model);
		}

		[HttpPost]
		[AdminAuthorize]
		[ChildActionOnly]
		public ActionResult Configure(ConfigurationModel model)
		{
			if (!ModelState.IsValid)
				return Configure();

			//load settings for a chosen store scope
			var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
			var payzenPaymentSettings = _settingService.LoadSetting<PayzenPaymentSettings>(storeScope);

			//save settings
			payzenPaymentSettings.UseSandbox = model.UseSandbox;
			payzenPaymentSettings.ShopId = model.ShopId;
			payzenPaymentSettings.TestCertificate = model.TestCertificate;
			payzenPaymentSettings.ProductionCertificate = model.ProductionCertificate;

			/* We do not clear cache after each setting update.
			 * This behavior can increase performance because cached settings will not be cleared 
			 * and loaded from database after each update */

			_settingService.SaveSettingOverridablePerStore(payzenPaymentSettings, x => x.UseSandbox, model.UseSandbox_OverrideForStore, storeScope, false);
			_settingService.SaveSettingOverridablePerStore(payzenPaymentSettings, x => x.ShopId, model.ShopId_OverrideForStore, storeScope, false);
			_settingService.SaveSettingOverridablePerStore(payzenPaymentSettings, x => x.TestCertificate, model.TestCertificate_OverrideForStore, storeScope, false);
			_settingService.SaveSettingOverridablePerStore(payzenPaymentSettings, x => x.ProductionCertificate, model.ProductionCertificate_OverrideForStore, storeScope, false);

			//now clear settings cache
			_settingService.ClearCache();

			SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

			return Configure();
		}

		[ChildActionOnly]
		public ActionResult PaymentInfo()
		{
			return View("~/Plugins/Payments.Payzen/Views/PaymentPayzen/PaymentInfo.cshtml");
		}


		public ActionResult ProcessPaymentInfo(int orderId)
		{
			var storeScope = this.GetActiveStoreScopeConfiguration(_storeService, _workContext);
			var payzenPaymentSettings = _settingService.LoadSetting<PayzenPaymentSettings>(storeScope);
			var model = new PaymentInfoModel();
			var order = _orderService.GetOrderById(orderId);
			if (order == null)
				return View("~/Plugins/Payments.Payzen/Views/PaymentPayzen/PaymentInfo.cshtml", model);
			model.vads_site_id = payzenPaymentSettings.ShopId; //"19677451";// "15063320";
			model.vads_amount = string.Format("{0:0.##}", (order.OrderTotal * 100));
			model.vads_trans_date = order.CreatedOnUtc.ToString("yyyyMMddHHmmss");
			model.vads_trans_id = order.Id.ToString("000000");

			var certificate = string.Empty;
			var ctxMode = string.Empty;

			if (payzenPaymentSettings.UseSandbox)
			{
				certificate = payzenPaymentSettings.TestCertificate;
				ctxMode = "TEST";
			}
			else
			{
				certificate = payzenPaymentSettings.ProductionCertificate;
				ctxMode = "PRODUCTION";
			}

			model.vads_ctx_mode = ctxMode;

			var signString = "INTERACTIVE+" + model.vads_amount + "+" + ctxMode + "+978+PAYMENT+SINGLE+" + model.vads_site_id + "+" + model.vads_trans_date + "+" + model.vads_trans_id + "+V2+" + certificate;


			var signature = GenerateSeal(signString, certificate);

			model.signature = signature;
			return View("~/Plugins/Payments.Payzen/Views/PaymentPayzen/PaymentInfo.cshtml", model);
		}


		//[HttpPost]
		//public ActionResult PayzenResult(string vads_order_id,string vads_trans_status, string vads_trans_id, string vads_payment_config,string vads_trans_date, string vads_capture_delay, string vads_amount,
		//	string vads_currency, string vads_change_rate, string vads_effective_amount, string vads_effective_currenc, string vads_auth_result, string vads_threeds_enrolled, string vads_threeds_status,
		//	string vads_risk_control, string vads_card_brand)
		[HttpPost]
		public ActionResult PayzenResult()
		{
			_logger.InsertLog(LogLevel.Information, "Payzen Result Check Step 1");

			var sb = new StringBuilder();
			NameValueCollection form = new NameValueCollection();
			for (int i = 0; i < Request.Form.Keys.Count; i++)
			{
				form.Add(Request.Form.Keys[i], Server.UrlDecode(Request.Form[Request.Form.Keys[i]]));
				sb.Append(Request.Form.Keys[i] + ": " + Server.UrlDecode(Request.Form[Request.Form.Keys[i]]) + " || ");
				//_logger.Information(Request.Form.Keys[i] + " " + form[Request.Form.Keys[i]]);
			}

			//sb.Append("vads_order_id: " + vads_order_id);
			//sb.Append(" || vads_trans_status: " + vads_trans_status);
			//sb.Append(" || vads_trans_id: " + vads_trans_id);
			//sb.Append(" || vads_payment_config: " + vads_payment_config);
			//sb.Append(" || vads_trans_date: " + vads_trans_date);
			//sb.Append(" || vads_capture_delay: " + vads_capture_delay);
			//sb.Append(" || vads_amount: " + vads_amount);
			//sb.Append(" || vads_currency: " + vads_currency);
			//sb.Append(" || vads_change_rate: " + vads_change_rate);
			//sb.Append(" || vads_effective_amount: " + vads_effective_amount);
			//sb.Append(" || vads_effective_currenc: " + vads_effective_currenc);
			//sb.Append(" || vads_auth_result: " + vads_auth_result);
			//sb.Append(" || vads_threeds_enrolled: " + vads_threeds_enrolled);
			//sb.Append(" || vads_threeds_status: " + vads_threeds_status);
			//sb.Append(" || vads_risk_control: " + vads_risk_control);
			//sb.Append(" || vads_card_brand: " + vads_card_brand);

			_logger.InsertLog(LogLevel.Information, "Payzen Result Check Step 2", sb.ToString());


			if (form["vads_trans_status"].ToLower() == "captured")
			{
				var orderId = Convert.ToInt32(form["vads_trans_id"]);
				var order = _orderService.GetOrderById(orderId);
				if (order != null)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note =
								"Received status= Captured" + " for Order=" + orderId,
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					if (_orderProcessingService.CanMarkOrderAsPaid(order))
					{
						_logger.Information("Payzen Marking OrderId=" + orderId + " as paid");
						_orderProcessingService.MarkOrderAsPaid(order);
						_orderService.UpdateOrder(order);
					}

					LightSpeedOrderCreate(orderId, form["vads_card_brand"]);//Replaced with Order create on payment success.
					//notifications//Custom Code // Notification to be send after payment success
					SendNotificationsAndSaveNotes(orderId);
					return RedirectToRoute("CheckoutCompleted", new { orderId = orderId });
				}
			}




			var result = Request.Form;
			return View("~/Plugins/Payments.Payzen/Views/PaymentPayzen/Result.cshtml");
		}



		public ActionResult PayzenReturn(string vads_order_id, string vads_trans_status, string vads_trans_id, string vads_payment_config, string vads_trans_date, string vads_capture_delay, string vads_amount,
			string vads_currency, string vads_change_rate, string vads_effective_amount, string vads_effective_currenc, string vads_auth_result, string vads_threeds_enrolled, string vads_threeds_status,
			string vads_risk_control, string vads_card_brand)
		{
			var result = Request.Form;

			return View("~/Plugins/Payments.Payzen/Views/PaymentPayzen/Result.cshtml");
		}


		[NonAction]
		public override IList<string> ValidatePaymentForm(FormCollection form)
		{
			var warnings = new List<string>();
			return warnings;
		}

		[NonAction]
		public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
		{
			var paymentInfo = new ProcessPaymentRequest();
			return paymentInfo;
		}


		public string GenerateSeal(string data, string key)
		{
			string sealAlgorithm = "HMAC-SHA-256";
			var rr = "";
			UTF8Encoding utF8Encoding = new UTF8Encoding();
			byte[] bytes = utF8Encoding.GetBytes(data);
			HMAC hmac = (HMAC)new HMACSHA256();
			hmac.Key = utF8Encoding.GetBytes(key);
			hmac.Initialize();
			return Convert.ToBase64String(hmac.ComputeHash(bytes));
		}



		#region LS Order Create

		public void LightSpeedOrderStatusUpdate(int orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(orderId);
				if (order == null)
					return;

				if (order.PaymentStatus == PaymentStatus.Paid)
				{
					var cmsNopOrder = _cmsNopOrderMappingService.GetByNopOrderId(order.Id);
					if (cmsNopOrder == null)
						return;

					var cmsNopCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
					if (cmsNopCustomer == null)
						return;

					var url = "https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + cmsNopCustomer.CmsCustomerId + "/order/" + cmsNopOrder.CmsOrderId;
					var token = GetToken();

					var postData = new StringBuilder();
					postData.Append("{\"status\":\"ACCEPTED\"}");


					var request = (HttpWebRequest)WebRequest.Create(url);
					request.Headers.Add("X-Auth-Token", token);
					request.Headers.Add("customerId", cmsNopCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
					request.Headers.Add("orderId", cmsNopOrder.CmsOrderId.ToString(CultureInfo.InvariantCulture));
					request.ContentType = "application/json";
					request.Method = "PATCH";
					request.ContentLength = postData.Length;
					request.AutomaticDecompression = DecompressionMethods.GZip;

					var requestWriter = new StreamWriter(request.GetRequestStream());
					requestWriter.Write(postData);
					requestWriter.Close();

					var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
					var responseData = responseReader.ReadToEnd();
					if (!string.IsNullOrEmpty(responseData))
					{
						var jsonText = JsonConvert.DeserializeObject<CmsOrderApiModel>(responseData);
						if (jsonText.status.ToLower() == "done")
						{
							order.OrderStatus = OrderStatus.Complete;
							_orderService.UpdateOrder(order);
							return;
						}
					}
				}
				return;

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating order on CMS for nop order :: " + orderId, exception.Message + "  ||   " + exception.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Error updating order on CMS for nop order ::  " + orderId, exception.Message, exception.StackTrace);
				return;
			}
		}


		public void LightSpeedOrderCreate(long orderId, string paymentInfo)
		{
			#region Push Order to CMS
			try
			{
				var order = _orderService.GetOrderById(Convert.ToInt32(orderId));
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order not found in webshop :: " + orderId,
						 "Order push to cms :: Error :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId, "Order push to cms :: Error :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				if (order.PaymentStatus != PaymentStatus.Paid)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, "Current payment status :: " + order.PaymentStatus);

					return;
				}

				var checkCmsCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
				if (checkCmsCustomer == null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, string.Empty);
					return;
				}

				string token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, string.Empty);
					return;
				}

				string url = @"https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + checkCmsCustomer.CmsCustomerId + "/order";
				var postData = new StringBuilder();
				postData.Append("{\"customerId\":" + checkCmsCustomer.CmsCustomerId + ",");

				var currentData = DateTime.Now;
				string newDate = currentData.Year + "-" + currentData.Month + "-" + currentData.Day + "T" + currentData.Hour + ":" +
									  currentData.Minute + ":" + currentData.Second + "+00:00";
				postData.Append("\"deliveryDate\":\"" + newDate + "\",");

				var orderType = GetOrderType(order);
				postData.Append("\"type\":\"" + orderType + "\",");
				postData.Append("\"status\":\"" + "ACCEPTED" + "\",");
				postData.Append("\"description\":\"" + "order" + "\",");


				var deliveryTime = string.Empty;
				var deliveryTimeDescripton = order.CheckoutAttributeDescription.Substring(order.CheckoutAttributeDescription.LastIndexOf("Gev"));

				if (!string.IsNullOrEmpty(deliveryTimeDescripton))
				{
					deliveryTime = deliveryTimeDescripton;
				}

				postData.Append("\"note\":\"" + "Order Reference: " + order.Id + " || " + deliveryTime + "\",");



				postData.Append("\"orderItems\":[");
				var totalItems = order.OrderItems.Count;
				var counter = 0;

				foreach (var orderItem in order.OrderItems)
				{
					var attributePriceDeduction = 0M;
					counter++;
					var cmsProduct = _cmsNopProductMappingService.GetByNopProductId(orderItem.ProductId);
					if (cmsProduct == null)
					{
						_logger.InsertLog(LogLevel.Error, "Error :: Order sync with CMS :: Nop Order Id :: " + orderItem.OrderId,
							 "Could not find mapped CMS product against Nop Product :: ID :: " + orderItem.ProductId + " :: Product Name :: " +
							 orderItem.Product.Name, _workContext.CurrentCustomer);
						break;
					}

					postData.Append("{\"productId\":" + cmsProduct.CmsProductId + ",");
					postData.Append("\"amount\":" + orderItem.Quantity + ",");
					if (!String.IsNullOrEmpty(orderItem.AttributesXml))
					{
						postData.Append("\"modifiers\":[");
						var xmlDoc = new XmlDocument();
						xmlDoc.LoadXml(orderItem.AttributesXml);

						var nodeCounter = 0;
						var attrCounter = 0;
						var nodeList = xmlDoc.SelectNodes(@"//Attributes/ProductAttribute");

						foreach (XmlNode node in xmlDoc.SelectNodes(@"//Attributes/ProductAttribute"))
						{
							nodeCounter++;
							if (node.Attributes != null && node.Attributes["ID"] != null)
							{
								int attributeId;
								var nopAttributeValueId = node.InnerText;
								if (!string.IsNullOrEmpty(nopAttributeValueId))
								{
									var nopCmsProductAttributeValueMapping = _cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(Convert.ToInt32(nopAttributeValueId));

									var nopCmsProductAttributeMapping = new NopCmsProductAttributeMapping();
									if (nopCmsProductAttributeValueMapping != null)
									{
										nopCmsProductAttributeMapping = _cmsNopProductMappingService.GetNopCmsProductAttributeById(nopCmsProductAttributeValueMapping.NopCmsProductAttributeMappingId);
									}

									if (attrCounter > 0)
										postData.Append(",{");
									else
										postData.Append("{");

									if (nopCmsProductAttributeMapping != null)
									{
										postData.Append("\"description\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");
										postData.Append("\"modifierId\":" + nopCmsProductAttributeMapping.CmsProductAttributeId + ",");
										postData.Append("\"modifierName\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");

									}
									foreach (XmlNode attributeValue in node.SelectNodes("ProductAttributeValue"))
									{
										var value = attributeValue.SelectSingleNode("Value").InnerText.Trim();
										if (!String.IsNullOrEmpty(value))
										{
											var cmsProductAttributeValue = _cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(Convert.ToInt32(value));
											if (cmsProductAttributeValue != null)
											{
												postData.Append("\"modifierValueId\":" + cmsProductAttributeValue.CmsProductAttributeValueMappingId + ",");
												postData.Append("\"price\":" + cmsProductAttributeValue.Price + ",");
												postData.Append("\"priceWithoutVat\":" + cmsProductAttributeValue.Price);
												attributePriceDeduction += cmsProductAttributeValue.Price;
											}
										}
									}

									postData.Append("}");
									attrCounter++;
								}
							}
						}
						postData.Append("],");
					}
					var orderItemPrice = orderItem.PriceInclTax - attributePriceDeduction;
					postData.Append("\"totalPrice\":" + orderItemPrice + ",");
					postData.Append("\"totalPriceWithoutVat\":" + orderItemPrice + ",");
					postData.Append("\"unitPrice\":" + orderItemPrice + ",");
					postData.Append("\"unitPriceWithoutVat\":" + orderItemPrice);
					//postData.Append("\"totalPriceWithoutVat\":" + orderItem.PriceExclTax + ",");
					//postData.Append("\"unitPrice\":" + orderItem.UnitPriceInclTax + ",");
					//postData.Append("\"unitPriceWithoutVat\":" + orderItem.UnitPriceExclTax);
					postData.Append(counter >= totalItems ? "}" : "},");
				}
				//Temp Testing
				//postData.Append("],\"orderPayments\":[{");
				//postData.Append("\"amount\":" + order.OrderTotal + ",");
				//postData.Append("\"paymentTypeId\":" + 39893 + ",");
				//postData.Append("\"paymentTypeTypeId\":" + 4 + "}");

				var paymentTypeId = 0;
				var paymentTypeTypeId = 0;
				var paymentOption = string.Empty;

				GetLSPaymentInformation(paymentInfo, ref paymentTypeId, ref paymentTypeTypeId, ref paymentOption);

				if (paymentTypeId == 0 || paymentTypeTypeId == 0)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId,
						 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, string.Empty);
					return;
				}

				postData.Append("],\"orderPayments\":[{");
				postData.Append("\"amount\":" + order.OrderTotal + ",");
				postData.Append("\"paymentTypeId\":" + paymentTypeId + ",");
				postData.Append("\"paymentTypeTypeId\":" + paymentTypeTypeId + "}");

				if (order.RedeemedRewardPointsEntry != null)
				{
					//Use Credit points
					//postData.Append(",{\"amount\":" + orderPlacedEvent.Order.RedeemedRewardPointsEntry.UsedAmount + ",");
					//postData.Append("\"paymentTypeId\":" + 5208 + ",");
					//postData.Append("\"paymentTypeTypeId\":" + 15 + "}");

					//Use Cash
					postData.Append(",{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
					postData.Append("\"paymentTypeId\":" + 36191 + ",");
					//postData.Append("\"paymentTypeTypeId\":" + 1 + "}");
					postData.Append("\"paymentTypeTypeId\":" + 200 + "}");
				}


				postData.Append("],\"orderTaxInfo\":[{");
				postData.Append("\"tax\":" + "0" + ",");
				postData.Append("\"taxRate\":" + "0" + ",");
				postData.Append("\"totalWithTax\":" + order.OrderTotal + ",");
				postData.Append("\"totalWithoutTax\":" + order.OrderTotal);
				postData.Append("}]}");

				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Order#: " + order.Id, postData.ToString(), _workContext.CurrentCustomer);

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					if (orderType.ToLower() == "delivery")
					{
						UpdateCustomerAddressOnLightSpeed(order, checkCmsCustomer);
					}
					else
					{
						//Update customer name with delivery Info on LS
						UpdateCustomerNameOnLightSpeed(Convert.ToInt32(responseData), checkCmsCustomer, order.Customer, order);
					}

					var cmsOrderNopOrderMap = new CmsOrderNopOrderMapping
					{
						NopOrderId = order.Id,
						CmsOrderId = Convert.ToInt32(responseData),
						CreatedOnUtc = DateTime.UtcNow,
						UpdatedOnUtc = DateTime.UtcNow
					};

					_cmsNopOrderMappingService.Insert(cmsOrderNopOrderMap);

					order.CustomOrderNumber = responseData;
					_orderService.UpdateOrder(order);
				}

			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error Creating Order on CMS for nop order :: " + orderId,
					 ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId, ex.Message, ex.StackTrace);

				return;
			}
			#endregion
		}

		public string GetToken()
		{
			try
			{
				string token;
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		public string GetOrderType(Core.Domain.Orders.Order order)
		{
			if (order == null)
				return "takeaway";

			if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
			{
				if (order.CheckoutAttributeDescription.ToLower().Contains("levering"))
					return "delivery";
			}
			return "takeaway";
		}

		public void UpdateCustomerAddressOnLightSpeed(Core.Domain.Orders.Order order, CmsCustomerNopCustomerMapping checkCmsCustomer)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, string.Empty);
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"deliveryCity\":\"" + order.ShippingAddress.City + "\",");
				postData.Append("\"deliveryCountry\":\"" + order.ShippingAddress.Country.TwoLetterIsoCode + "\",");
				postData.Append("\"deliveryStreet\":\"" + order.ShippingAddress.Address1 + "\",");
				postData.Append("\"deliveryStreetNumber\":\"" + order.ShippingAddress.Address2 + "\",");
				postData.Append("\"deliveryZip\":\"" + order.ShippingAddress.ZipPostalCode + "\",");
				postData.Append("\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + order.Customer.Email + "\",");

				//var loyaltyPoints = "0 reward points available.";
				//var customerRewardPointsBalance = _rewardPointService.GetRewardPointsBalance(order.Customer.Id, _storeContext.CurrentStore.Id);
				//if (customerRewardPointsBalance > 0)
				//	loyaltyPoints = customerRewardPointsBalance + " reward points available.";

				//postData.Append("\"firstName\":\"" + loyaltyPoints + "\",");
				//postData.Append("\"lastName\":\"" + order.Customer.GetFullName() + "\"");
				//postData.Append("}");


				var clientName = order.Customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.FirstName);
				var clientLastName = order.Customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
				{
					//clientName = order.CheckoutAttributeDescription;
					var dataArray = order.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1].Trim() + " | " + dataArray[3].Trim();
					clientLastName = order.Customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);

				}
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating customer address on LS for order id #" + order.Id, exception.Message + "  ::  " + exception.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Error updating customer address on LS for order id #" + order.Id, exception.Message, exception.StackTrace);
				return;
			}
		}

		protected void SendNotificationsAndSaveNotes(int orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(orderId);
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId,
						"Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order ::  " + orderId, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				//send email notifications
				var orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
				if (orderPlacedStoreOwnerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to store owner) has been queued. Queued email identifier: {0}.", orderPlacedStoreOwnerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}

				var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 _pdfService.PrintOrderToPdf(order) : null;
				var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 "order.pdf" : null;
				var orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService
					 .SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
				if (orderPlacedCustomerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: " + orderId,
					ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);
				_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order :: " + orderId, ex.Message, ex.StackTrace);

				return;
			}
		}

		public void GetLSPaymentInformation(string paymentInfo, ref int paymentTypeId, ref int paymentTypeTypeId, ref string paymentOption)
		{
			if (!string.IsNullOrEmpty(paymentInfo))
			{

				paymentOption = paymentInfo;
				switch (paymentInfo.ToLower())
				{
					case "edenred_tr":
						paymentTypeId = 56272;
						paymentTypeTypeId = 801;
						break;
					case "edenred_ec":
						paymentTypeId = 56272;
						paymentTypeTypeId = 801;
						break;
					case "edenred_tc":
						paymentTypeId = 56272;
						paymentTypeTypeId = 801;
						break;
				}
			}
		}


		public void UpdateCustomerNameOnLightSpeed(int cmsOrderId, CmsCustomerNopCustomerMapping checkCmsCustomer, Nop.Core.Domain.Customers.Customer customer, Nop.Core.Domain.Orders.Order nopOrder)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerNameOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + customer.Email + "\",");

				var clientName = customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.FirstName);
				var clientLastName = customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(nopOrder.CheckoutAttributeDescription))
				{
					//clientName = nopOrder.CheckoutAttributeDescription;
					//var dataArray = nopOrder.CheckoutAttributeDescription.Split(':');
					var dataArray = nopOrder.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1].Trim() + " | " + dataArray[3].Trim();
					clientLastName = customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
				}

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating Name for customer on Light Speed CMS :: LS OrderId :: " + cmsOrderId + " || Nop Order Id :: " + nopOrder.Id + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message + "  ::  " + exception.StackTrace);
			}
		}
		#endregion

	}
}
