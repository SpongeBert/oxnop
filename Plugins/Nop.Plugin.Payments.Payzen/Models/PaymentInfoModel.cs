﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.Payzen.Models
{	
	public class PaymentInfoModel : BaseNopModel
	{	
		
		public string vads_action_mode { get; set; }	 
		
		public string vads_amount { get; set; }	
		
		public string vads_ctx_mode { get; set; }

		
		public string vads_currency { get; set; }
		
		public string vads_page_action { get; set; }
		
		public string vads_payment_config { get; set; }

		public string vads_site_id { get; set; }

		public string vads_trans_date { get; set; }

		public string vads_trans_id { get; set; }

		public string vads_version { get; set; }

		public string signature { get; set; }
	}


}
