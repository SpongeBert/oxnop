﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Payments.Payzen.Models
{	
	public class ConfigurationModel : BaseNopModel
	{
		public int ActiveStoreScopeConfiguration { get; set; }


		[NopResourceDisplayName("Plugins.Payments.Payzen.Fields.UseSandbox")]
		public bool UseSandbox { get; set; }
		public bool UseSandbox_OverrideForStore { get; set; }

		[NopResourceDisplayName("Plugins.Payments.Payzen.Fields.ShopId")]
		public string ShopId { get; set; }
		public bool ShopId_OverrideForStore { get; set; }

		[NopResourceDisplayName("Plugins.Payments.Payzen.Fields.TestCertificate")]
		public string TestCertificate { get; set; }
		public bool TestCertificate_OverrideForStore { get; set; }

		[NopResourceDisplayName("Plugins.Payments.Payzen.Fields.ProductionCertificate")]
		public string ProductionCertificate { get; set; }
		public bool ProductionCertificate_OverrideForStore { get; set; }
	}
}
