﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{	

	public partial class CmsNopOrderMappingService : ICmsNopOrderMappingService
	{
		#region Fields
		private readonly IRepository<CmsOrderNopOrderMapping> _cmsOrderNopOrdermappingRepository;
		private readonly IWorkContext _workContext;
		private readonly IRepository<Customer> _customerRepository;
		#endregion

		#region Ctor

		public CmsNopOrderMappingService(IRepository<CmsOrderNopOrderMapping> cmsOrderNopOrdermappingRepository, IWorkContext workContext, IRepository<Customer> customerRepository)
		{
			_cmsOrderNopOrdermappingRepository = cmsOrderNopOrdermappingRepository;
			_workContext = workContext;
			_customerRepository = customerRepository;
		}

		#endregion


		#region Methods

		public void Insert(CmsOrderNopOrderMapping cmsOrderNopOrderMapping)
		{
			if (cmsOrderNopOrderMapping == null)
				throw new ArgumentNullException("cmsOrderNopOrderMapping");
			_cmsOrderNopOrdermappingRepository.Insert(cmsOrderNopOrderMapping);

		}

		public void Update(CmsOrderNopOrderMapping cmsOrderNopOrderMapping)
		{
			if (cmsOrderNopOrderMapping == null)
				throw new ArgumentNullException("cmsOrderNopOrderMapping");

			_cmsOrderNopOrdermappingRepository.Update(cmsOrderNopOrderMapping);
		}

		public void Delete(CmsOrderNopOrderMapping cmsOrderNopOrderMapping)
		{
			if (cmsOrderNopOrderMapping == null)
				throw new ArgumentNullException("cmsOrderNopOrderMapping");
			_cmsOrderNopOrdermappingRepository.Delete(cmsOrderNopOrderMapping);
		}

		public CmsOrderNopOrderMapping GetByNopOrderId(int nopOrderId)
		{
			var query = _cmsOrderNopOrdermappingRepository.Table.FirstOrDefault(x => x.NopOrderId == nopOrderId);
			return query;
		}

		public CmsOrderNopOrderMapping GetByCmsOrderId(int cmsOrderId)
		{
			var query = _cmsOrderNopOrdermappingRepository.Table.FirstOrDefault(x => x.CmsOrderId == cmsOrderId);
			return query;
		}

		public List<CmsOrderNopOrderMapping> GetAllOrderMappings()
		{
			var query = _cmsOrderNopOrdermappingRepository.Table;
			return query.ToList();
		}

		#endregion
	}
}
