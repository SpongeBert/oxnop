﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial interface ICompanyRepresentativeCustomerMappingService
	{
		void Insert(CompanyRepresentativeCustomerMapping salesPersonCustomerMapping);

		void Update(CompanyRepresentativeCustomerMapping salesPersonCustomerMapping);

		void Delete(CompanyRepresentativeCustomerMapping salesPersonCustomerMapping);

		CompanyRepresentativeCustomerMapping GetByCustomerId(int customerId);

		CompanyRepresentativeCustomerMapping GetById(int id);

		List<CompanyRepresentativeCustomerMapping> GetCustomersBySalesPerson(int salesPersonId);

		List<int> GetCustomersBySalesPersonId(int salesPersonId);

		/// <summary>
		/// Gets all customers
		/// </summary>
		/// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
		/// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
		/// <param name="affiliateId">Affiliate identifier</param>
		/// <param name="vendorId">Vendor identifier</param>
		/// <param name="customerRoleIds">A list of customer role identifiers to filter by (at least one match); pass null or empty list in order to load all customers; </param>
		/// <param name="email">Email; null to load all customers</param>
		/// <param name="username">Username; null to load all customers</param>
		/// <param name="firstName">First name; null to load all customers</param>
		/// <param name="lastName">Last name; null to load all customers</param>
		/// <param name="dayOfBirth">Day of birth; 0 to load all customers</param>
		/// <param name="monthOfBirth">Month of birth; 0 to load all customers</param>
		/// <param name="company">Company; null to load all customers</param>
		/// <param name="phone">Phone; null to load all customers</param>
		/// <param name="zipPostalCode">Phone; null to load all customers</param>
		/// <param name="ipAddress">IP address; null to load all customers</param>
		/// <param name="loadOnlyWithShoppingCart">Value indicating whether to load customers only with shopping cart</param>
		/// <param name="sct">Value indicating what shopping cart type to filter; userd when 'loadOnlyWithShoppingCart' param is 'true'</param>
		/// <param name="pageIndex">Page index</param>
		/// <param name="pageSize">Page size</param>
		/// <returns>Customers</returns>
		IPagedList<Customer> GetAllCustomers(DateTime? createdFromUtc = null,
			 DateTime? createdToUtc = null, int affiliateId = 0, int vendorId = 0,
			 int[] customerRoleIds = null, string email = null, string username = null,
			 string firstName = null, string lastName = null,
			 int dayOfBirth = 0, int monthOfBirth = 0,
			 string company = null, string phone = null, string zipPostalCode = null,
			 string ipAddress = null, bool loadOnlyWithShoppingCart = false, ShoppingCartType? sct = null,
			 int pageIndex = 0, int pageSize = int.MaxValue, List<int> customerIds = null, string keywords = null);


		/// <summary>
		/// Search orders
		/// </summary>
		/// <param name="storeId">Store identifier; null to load all orders</param>
		/// <param name="vendorId">Vendor identifier; null to load all orders</param>
		/// <param name="customerId">Customer identifier; null to load all orders</param>
		/// <param name="productId">Product identifier which was purchased in an order; 0 to load all orders</param>
		/// <param name="affiliateId">Affiliate identifier; 0 to load all orders</param>
		/// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
		/// <param name="warehouseId">Warehouse identifier, only orders with products from a specified warehouse will be loaded; 0 to load all orders</param>
		/// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
		/// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
		/// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
		/// <param name="osIds">Order status identifiers; null to load all orders</param>
		/// <param name="psIds">Payment status identifiers; null to load all orders</param>
		/// <param name="ssIds">Shipping status identifiers; null to load all orders</param>
		/// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
		/// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
		/// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
		/// <param name="pageIndex">Page index</param>
		/// <param name="pageSize">Page size</param>
		/// <returns>Orders</returns>
		IPagedList<Order> SearchOrders(int storeId = 0,
			 int vendorId = 0, int customerId = 0,
			 int productId = 0, int affiliateId = 0, int warehouseId = 0,
			 int billingCountryId = 0, string paymentMethodSystemName = null,
			 DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
			 List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
			 string billingEmail = null, string billingLastName = "",
			 string orderNotes = null, int pageIndex = 0, int pageSize = int.MaxValue, List<int> customerIds = null);

		/// <summary>
		/// Get order average report
		/// </summary>
		/// <param name="storeId">Store identifier; pass 0 to ignore this parameter</param>
		/// <param name="vendorId">Vendor identifier; pass 0 to ignore this parameter</param>
		/// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
		/// <param name="orderId">Order identifier; pass 0 to ignore this parameter</param>
		/// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
		/// <param name="osIds">Order status identifiers</param>
		/// <param name="psIds">Payment status identifiers</param>
		/// <param name="ssIds">Shipping status identifiers</param>
		/// <param name="startTimeUtc">Start date</param>
		/// <param name="endTimeUtc">End date</param>
		/// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
		/// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
		/// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
		/// <returns>Result</returns>
		OrderAverageReportLine GetOrderAverageReportLine(int storeId = 0, int vendorId = 0,
			 int billingCountryId = 0, int orderId = 0, string paymentMethodSystemName = null,
			 List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
			 DateTime? startTimeUtc = null, DateTime? endTimeUtc = null,
			 string billingEmail = null, string billingLastName = "", string orderNotes = null, List<int> customerIds = null);

		/// <summary>
		/// Get profit report
		/// </summary>
		/// <param name="storeId">Store identifier; pass 0 to ignore this parameter</param>
		/// <param name="vendorId">Vendor identifier; pass 0 to ignore this parameter</param>
		/// <param name="orderId">Order identifier; pass 0 to ignore this parameter</param>
		/// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
		/// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
		/// <param name="startTimeUtc">Start date</param>
		/// <param name="endTimeUtc">End date</param>
		/// <param name="osIds">Order status identifiers; null to load all records</param>
		/// <param name="psIds">Payment status identifiers; null to load all records</param>
		/// <param name="ssIds">Shipping status identifiers; null to load all records</param>
		/// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
		/// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
		/// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
		/// <returns>Result</returns>
		decimal ProfitReport(int storeId = 0, int vendorId = 0,
			 int billingCountryId = 0, int orderId = 0, string paymentMethodSystemName = null,
			 List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
			 DateTime? startTimeUtc = null, DateTime? endTimeUtc = null,
			 string billingEmail = null, string billingLastName = "", string orderNotes = null, List<int> customerIds = null);



		List<Customer> GetAllChildCustomerInfoByCompanyId(string companyId);
	}
}
