﻿using System.Collections.Generic;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial interface ICmsNopCustomerMappingService
	{
		void Insert(CmsCustomerNopCustomerMapping cmsCustomerNopCustomerMapping);

		void Update(CmsCustomerNopCustomerMapping cmsCustomerNopCustomerMapping);

		void Delete(CmsCustomerNopCustomerMapping cmsCustomerNopCustomerMapping);

		CmsCustomerNopCustomerMapping GetByNopCustomerId(int nopCustomerId);

		CmsCustomerNopCustomerMapping GetByCmsCustomerId(int cmsCustomerId);

		List<CmsCustomerNopCustomerMapping> GetAllCustomerMapping(bool showDeleted = false);
	}
}
