﻿using System.Collections.Generic;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial interface ILightSpeedOfflineOrdersService
	{
		void Insert(LightSpeedOfflineOrders lightSpeedOfflineOrders);

		void Update(LightSpeedOfflineOrders lightSpeedOfflineOrders);

		void Delete(LightSpeedOfflineOrders lightSpeedOfflineOrders);

		LightSpeedOfflineOrders GetById(int id);

		LightSpeedOfflineOrders GetByLightSpeedOrderId(int orderId);

		LightSpeedOfflineOrders GetByLightSpeedOrderIdAndCustomerId(int orderId, int customerId);

		List<LightSpeedOfflineOrders> GetAllOrderMappings();

		List<LightSpeedOfflineOrders> GetAllByCustomerId(int customerId);
	}
}
