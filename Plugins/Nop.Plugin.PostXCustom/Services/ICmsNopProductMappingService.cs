﻿using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial interface ICmsNopProductMappingService
	{
		void Insert(CmsProductNopProductMapping cmsProductNopProductMapping);

		void Update(CmsProductNopProductMapping cmsProductNopProductMapping);

		void Delete(CmsProductNopProductMapping cmsProductNopProductMapping);

		CmsProductNopProductMapping GetByNopProductId(int nopProductId);

		CmsProductNopProductMapping GetByCmsProductId(int cmsProductId);

		List<CmsProductNopProductMapping> GetByCompanyId(string companyId);

		List<CmsProductNopProductMapping> GetAllProductMapping();

		List<string> CompanyList();

		#region Product Attribute
		/// <summary>
		/// Gets a product attribute 
		/// </summary>
		/// <param name="productAttributeName">Product attribute Name</param>
		/// <returns>Product attribute </returns>
		ProductAttribute GetProductAttributeByName(string productAttributeName);


		/// <summary>
		/// Gets a predefined product attribute value
		/// </summary>
		/// <param name="name">Predefined product attribute value </param>
		/// <returns>Predefined product attribute value</returns>
		PredefinedProductAttributeValue GetPredefinedProductAttributeValueByName(string predefinedProductAttributeName);


		/// <summary>
		/// Gets product attribute mappings by product identifier
		/// </summary>
		/// <param name="productId">The product identifier</param>
		/// <param name="productAttrId"></param>
		/// <returns>Product attribute mapping collection</returns>
		ProductAttributeMapping GetProductAttributeMappingsByProductIdProductAttributeId(int productId, int productAttrId);


		/// <summary>
		/// Gets product attribute values by product attribute mapping identifier
		/// </summary>
		/// <param name="productAttributeMappingId">The product attribute mapping identifier</param>
		/// <param name="productAttributeValueName"></param>
		/// <returns>Product attribute values</returns>
		ProductAttributeValue GetProductAttributeValuesByprodAttrMapIdByprodAttrValName(int productAttributeMappingId ,string productAttributeValueName);

		#endregion

		#region Nop Cms product Attribute Mapping
		void InsertNopCmsProductAttributeMapping(NopCmsProductAttributeMapping nopCmsProductAttributeMapping);

		void UpdateNopCmsProductAttributeMapping(NopCmsProductAttributeMapping nopCmsProductAttributeMapping);

		NopCmsProductAttributeMapping GetNopCmsProductAttribute(int nopProductAttributeId);

		NopCmsProductAttributeMapping GetNopCmsProductAttributeMappingByCmsProductAttributeId(int cmsProductAttributeId);

		NopCmsProductAttributeMapping GetNopCmsProductAttributeById(int id);

		#endregion

		#region Nop Cms product Attribute Value Mapping
		void InsertNopCmsProductAttributeValueMapping(NopCmsProductAttributeValueMapping nopCmsProductAttributeValueMapping);

		void UpdateNopCmsProductAttributeValueMapping(NopCmsProductAttributeValueMapping nopCmsProductAttributeValueMapping);
		void DeleteNopCmsProductAttributeValueMapping(int productAttributeValueId);

		NopCmsProductAttributeValueMapping GetNopCmsProductAttributeValueMappingByProductAttributeValueId(int productAttributeValueId);

		#endregion


	}
}
