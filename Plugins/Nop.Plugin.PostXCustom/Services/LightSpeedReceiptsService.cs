﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial class LightSpeedReceiptsService : ILightSpeedReceiptsService
	{

		#region Fields
		private readonly IRepository<LightSpeedReceiptsMapping> _lightSpeedReceiptsMappingRepository;
		#endregion

		#region Ctor
		public LightSpeedReceiptsService(IRepository<LightSpeedReceiptsMapping> lightSpeedReceiptsMappingRepository)
		{
			_lightSpeedReceiptsMappingRepository = lightSpeedReceiptsMappingRepository;
		}
		#endregion

		public void Insert(LightSpeedReceiptsMapping lightSpeedReceiptsMapping)
		{
			if (lightSpeedReceiptsMapping == null)
				throw new ArgumentNullException("lightSpeedReceiptsMapping");
			_lightSpeedReceiptsMappingRepository.Insert(lightSpeedReceiptsMapping);
		}

		public void Update(LightSpeedReceiptsMapping lightSpeedReceiptsMapping)
		{
			if (lightSpeedReceiptsMapping == null)
				throw new ArgumentNullException("lightSpeedReceiptsMapping");

			_lightSpeedReceiptsMappingRepository.Update(lightSpeedReceiptsMapping);
		}

		public void Delete(LightSpeedReceiptsMapping lightSpeedReceiptsMapping)
		{
			if (lightSpeedReceiptsMapping == null)
				throw new ArgumentNullException("lightSpeedReceiptsMapping");
			_lightSpeedReceiptsMappingRepository.Delete(lightSpeedReceiptsMapping);
		}

		public LightSpeedReceiptsMapping GetById(int id)
		{
			var query = _lightSpeedReceiptsMappingRepository.Table.FirstOrDefault(x => x.Id == id);
			return query;
		}

		public LightSpeedReceiptsMapping GetByLightSpeedReceiptId(int receiptId)
		{
			var query = _lightSpeedReceiptsMappingRepository.Table.FirstOrDefault(x => x.ReceiptId == receiptId);
			return query;
		}

		public LightSpeedReceiptsMapping GetByLightSpeedReceiptIdAndLightSpeedCustomerId(int receiptId, int customerId)
		{
			var query = _lightSpeedReceiptsMappingRepository.Table.FirstOrDefault(x => x.ReceiptId == receiptId && x.LightSpeedCustomerId == customerId);
			return query;
		}

		public LightSpeedReceiptsMapping GetByLightSpeedReceiptIdAndNopCustomerId(int receiptId, int customerId)
		{
			var query = _lightSpeedReceiptsMappingRepository.Table.FirstOrDefault(x => x.ReceiptId == receiptId && x.NopCustomerId == customerId);
			return query;
		}

		public List<LightSpeedReceiptsMapping> GetAllReceipts()
		{
			var query = _lightSpeedReceiptsMappingRepository.Table;
			return query.ToList();
		}

		public List<LightSpeedReceiptsMapping> GetAllByLightSpeedCustomerId(int customerId)
		{
			var query = _lightSpeedReceiptsMappingRepository.Table.Where(x => x.LightSpeedCustomerId == customerId);
			return query.ToList();
		}

		public List<LightSpeedReceiptsMapping> GetAllByNopCustomerId(int customerId)
		{
			var query = _lightSpeedReceiptsMappingRepository.Table.Where(x => x.NopCustomerId == customerId);
			return query.ToList();
		}


		public List<LightSpeedReceiptsMapping> GetAllReceiptsFilter(DateTime orderDate, DateTime? toDate, int lightSpeedCustomerId=0, int receiptId = 0, int nopCustomerId = 0)
		{
			var query = _lightSpeedReceiptsMappingRepository.Table.ToList();
			if (toDate.HasValue)
			{
				query = query.Where(x => x.ReceiptCreateDate >= orderDate && x.ReceiptCreateDate <= toDate).ToList();
			}
			else
			{
				query = query.Where(x => x.ReceiptCreateDate.Date == orderDate.Date).ToList();
			}

			if (lightSpeedCustomerId > 0)
			{
				query = query.Where(x => x.LightSpeedCustomerId == lightSpeedCustomerId).ToList();
			}

			if (receiptId > 0)
			{
				query = query.Where(x => x.ReceiptId == receiptId).ToList();
			}

			if (nopCustomerId > 0)
			{
				query = query.Where(x => x.NopCustomerId == nopCustomerId).ToList();
			}


			return query.ToList();
		}

	}
}
