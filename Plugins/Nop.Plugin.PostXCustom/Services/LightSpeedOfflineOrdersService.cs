﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial class LightSpeedOfflineOrdersService : ILightSpeedOfflineOrdersService
	{
		#region Fields
		private readonly IRepository<LightSpeedOfflineOrders> _lightSpeedOfflineOrdersRepository;
		#endregion

		#region Ctor
		public LightSpeedOfflineOrdersService(IRepository<LightSpeedOfflineOrders> lightSpeedOfflineOrdersRepository)
		{
			_lightSpeedOfflineOrdersRepository = lightSpeedOfflineOrdersRepository;
		}
		#endregion

		#region Methods

		public void Insert(LightSpeedOfflineOrders lightSpeedOfflineOrders)
		{
			if (lightSpeedOfflineOrders == null)
				throw new ArgumentNullException("lightSpeedOfflineOrders");
			_lightSpeedOfflineOrdersRepository.Insert(lightSpeedOfflineOrders);
		}

		public void Update(LightSpeedOfflineOrders lightSpeedOfflineOrders)
		{
			if (lightSpeedOfflineOrders == null)
				throw new ArgumentNullException("lightSpeedOfflineOrders");

			_lightSpeedOfflineOrdersRepository.Update(lightSpeedOfflineOrders);
		}

		public void Delete(LightSpeedOfflineOrders lightSpeedOfflineOrders)
		{
			if (lightSpeedOfflineOrders == null)
				throw new ArgumentNullException("lightSpeedOfflineOrders");
			_lightSpeedOfflineOrdersRepository.Delete(lightSpeedOfflineOrders);
		}

		public LightSpeedOfflineOrders GetById(int id)
		{
			var query = _lightSpeedOfflineOrdersRepository.Table.FirstOrDefault(x => x.Id == id);
			return query;
		}

		public LightSpeedOfflineOrders GetByLightSpeedOrderId(int orderId)
		{
			var query = _lightSpeedOfflineOrdersRepository.Table.FirstOrDefault(x => x.LightSpeedOrderId == orderId);
			return query;
		}

		public LightSpeedOfflineOrders GetByLightSpeedOrderIdAndCustomerId(int orderId, int customerId)
		{
			var query = _lightSpeedOfflineOrdersRepository.Table.FirstOrDefault(x => x.LightSpeedOrderId == orderId && x.LightSpeedCustomerId==customerId);
			return query;
		}

		public List<LightSpeedOfflineOrders> GetAllOrderMappings()
		{
			var query = _lightSpeedOfflineOrdersRepository.Table;
			return query.ToList();
		}

		public List<LightSpeedOfflineOrders> GetAllByCustomerId(int customerId)
		{
			var query = _lightSpeedOfflineOrdersRepository.Table.Where(x=>x.LightSpeedCustomerId==customerId);
			return query.ToList();
		}

		#endregion
	}
}
