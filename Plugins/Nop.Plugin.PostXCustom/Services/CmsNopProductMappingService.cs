﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial class CmsNopProductMappingService : ICmsNopProductMappingService
	{
		#region Fields
		private readonly IRepository<CmsProductNopProductMapping> _cmsProductNopProductMappingRepository;
		private readonly IWorkContext _workContext;
		private readonly IRepository<Product> _productRepository;
		private readonly IRepository<ProductAttribute> _productAttributeRepository;
		private readonly IRepository<PredefinedProductAttributeValue> _predefinedProductAttributeValueRepository;
		private readonly IRepository<ProductAttributeMapping> _productAttributeMappingRepository;
		private readonly IRepository<ProductAttributeValue> _productAttributeValueRepository;
		private readonly IRepository<NopCmsProductAttributeMapping> _nopCmsProductAttributeMapping;
		private readonly IRepository<NopCmsProductAttributeValueMapping> _nopCmsProductAttributeValueMapping;

		#endregion

		#region Ctor

		public CmsNopProductMappingService(IRepository<CmsProductNopProductMapping> cmsProductNopProductMappingRepository, IWorkContext workContext, IRepository<Product> productRepository,
			IRepository<ProductAttribute> productAttributeRepository,
			IRepository<PredefinedProductAttributeValue> predefinedProductAttributeValueRepository,
			IRepository<ProductAttributeMapping> productAttributeMappingRepository,
			IRepository<ProductAttributeValue> productAttributeValueRepository,
			IRepository<NopCmsProductAttributeMapping> nopCmsProductAttributeMapping,
			IRepository<NopCmsProductAttributeValueMapping> nopCmsProductAttributeValueMapping)
		{
			_cmsProductNopProductMappingRepository = cmsProductNopProductMappingRepository;
			_workContext = workContext;
			_productRepository = productRepository;
			_productAttributeRepository = productAttributeRepository;
			_predefinedProductAttributeValueRepository = predefinedProductAttributeValueRepository;
			_productAttributeMappingRepository = productAttributeMappingRepository;
			_productAttributeValueRepository = productAttributeValueRepository;
			_nopCmsProductAttributeMapping = nopCmsProductAttributeMapping;
			_nopCmsProductAttributeValueMapping = nopCmsProductAttributeValueMapping;
		}

		#endregion


		#region Methods

		public void Insert(CmsProductNopProductMapping cmsProductNopProductMapping)
		{
			if (cmsProductNopProductMapping == null)
				throw new ArgumentNullException("cmsProductNopProductMapping");
			_cmsProductNopProductMappingRepository.Insert(cmsProductNopProductMapping);

		}

		public void Update(CmsProductNopProductMapping cmsProductNopProductMapping)
		{
			if (cmsProductNopProductMapping == null)
				throw new ArgumentNullException("cmsProductNopProductMapping");

			_cmsProductNopProductMappingRepository.Update(cmsProductNopProductMapping);
		}

		public void Delete(CmsProductNopProductMapping cmsProductNopProductMapping)
		{
			if (cmsProductNopProductMapping == null)
				throw new ArgumentNullException("cmsProductNopProductMapping");
			_cmsProductNopProductMappingRepository.Delete(cmsProductNopProductMapping);
		}

		public CmsProductNopProductMapping GetByNopProductId(int nopProductId)
		{
			var query = _cmsProductNopProductMappingRepository.Table.FirstOrDefault(x => x.NopProductId == nopProductId);
			return query;
		}

		public CmsProductNopProductMapping GetByCmsProductId(int cmsProductId)
		{
			var query = _cmsProductNopProductMappingRepository.Table.FirstOrDefault(x => x.CmsProductId == cmsProductId);
			return query;
		}

		public List<CmsProductNopProductMapping> GetByCompanyId(string companyId)
		{
			var query = _cmsProductNopProductMappingRepository.Table.Where(x => x.Company == companyId);
			return query.ToList();
		}

		public List<CmsProductNopProductMapping> GetAllProductMapping()
		{
			var query = _cmsProductNopProductMappingRepository.Table;
			return query.ToList();
		}


		public List<string> CompanyList()
		{	
			var query = _cmsProductNopProductMappingRepository.Table.Where(x=>!string.IsNullOrEmpty(x.Company)).Select(x=>x.Company).Distinct();
			return query.ToList();
		}



		#region Product Attribute
		/// <summary>
		/// Gets a product attribute 
		/// </summary>
		/// <param name="productAttributeName">Product attribute Name</param>
		/// <returns>Product attribute </returns>
		public  ProductAttribute GetProductAttributeByName(string productAttributeName)
		{
			var query = (from prodAtt in _productAttributeRepository.Table
							where prodAtt.Name.ToLower() == productAttributeName.ToLower()
							select prodAtt).FirstOrDefault();

			return query;
		}
		/// <summary>
		/// Gets a predefined product attribute value
		/// </summary>
		/// <param name="id">Predefined product attribute value identifier</param>
		/// <returns>Predefined product attribute value</returns>
		public virtual PredefinedProductAttributeValue GetPredefinedProductAttributeValueByName(string predefinedProductAttributeName)
		{
			if (predefinedProductAttributeName == null)
				return null;

			var query = (from ppav in _predefinedProductAttributeValueRepository.Table
							where ppav.Name.ToLower() == predefinedProductAttributeName.ToLower()
							select ppav).FirstOrDefault();
			return query;
		}
		/// <summary>
		/// Gets product attribute mappings by product identifier
		/// </summary>
		/// <param name="productId">The product identifier</param>
		/// <returns>Product attribute mapping collection</returns>
		public virtual ProductAttributeMapping GetProductAttributeMappingsByProductIdProductAttributeId(int productId, int productAttrId)
		{
			
				var query = (from pam in _productAttributeMappingRepository.Table
								orderby pam.DisplayOrder, pam.Id
								where pam.ProductId == productId	 &&  pam.ProductAttributeId == productAttrId
								select pam).FirstOrDefault();
		
				return query;	
		}



		/// <summary>
		/// Gets product attribute values by product attribute mapping identifier
		/// </summary>
		/// <param name="productAttributeMappingId">The product attribute mapping identifier</param>
		/// <param name="productAttributeValueName"></param>
		/// <returns>Product attribute values</returns>
		public virtual ProductAttributeValue GetProductAttributeValuesByprodAttrMapIdByprodAttrValName(int productAttributeMappingId, string productAttributeValueName)
		{
			var query = (from pav in _productAttributeValueRepository.Table
							 orderby pav.DisplayOrder, pav.Id
							 where pav.ProductAttributeMappingId == productAttributeMappingId && pav.Name.ToLower() == productAttributeValueName.ToLower()
							 select pav).FirstOrDefault();

			return query;
		}
		#endregion

		#region Nop Cms product Attribute Mapping
		public void InsertNopCmsProductAttributeMapping(NopCmsProductAttributeMapping nopCmsProductAttributeMapping)
		{
			if (nopCmsProductAttributeMapping == null)
				throw new ArgumentNullException("nopCmsProductAttributeMapping");
			_nopCmsProductAttributeMapping.Insert(nopCmsProductAttributeMapping);
		}

		public void UpdateNopCmsProductAttributeMapping(NopCmsProductAttributeMapping nopCmsProductAttributeMapping)
		{
			if (nopCmsProductAttributeMapping == null)
				throw new ArgumentNullException("nopCmsProductAttributeMapping");
			_nopCmsProductAttributeMapping.Update(nopCmsProductAttributeMapping);
		}

		public NopCmsProductAttributeMapping GetNopCmsProductAttribute(int nopProductAttributeId)
		{
			var query = _nopCmsProductAttributeMapping.Table.FirstOrDefault(x => x.NopProductAttributeId == nopProductAttributeId);
			return query;
		}
		public NopCmsProductAttributeMapping GetNopCmsProductAttributeMappingByCmsProductAttributeId(int cmsProductAttributeId)
		{
			var query = _nopCmsProductAttributeMapping.Table.FirstOrDefault(x => x.CmsProductAttributeId == cmsProductAttributeId);
			return query;
		}


		public NopCmsProductAttributeMapping GetNopCmsProductAttributeById(int id)
		{
			var query = _nopCmsProductAttributeMapping.Table.FirstOrDefault(x => x.Id == id);
			return query;
		}

		#endregion

		#region Nop Cms product Attribute Value Mapping
		public void InsertNopCmsProductAttributeValueMapping(NopCmsProductAttributeValueMapping nopCmsProductAttributeValueMapping)
		{
			if (nopCmsProductAttributeValueMapping == null)
				throw new ArgumentNullException("nopCmsProductAttributeValueMapping");
			_nopCmsProductAttributeValueMapping.Insert(nopCmsProductAttributeValueMapping);
		}

		public void UpdateNopCmsProductAttributeValueMapping(NopCmsProductAttributeValueMapping nopCmsProductAttributeValueMapping)
		{
			if (nopCmsProductAttributeValueMapping == null)
				throw new ArgumentNullException("nopCmsProductAttributeValueMapping");
			_nopCmsProductAttributeValueMapping.Update(nopCmsProductAttributeValueMapping);
		}
		public void DeleteNopCmsProductAttributeValueMapping(int productAttributeValueId)
		{
			if (productAttributeValueId <= 0)
				throw new ArgumentNullException("productAttributeVlaueId");

			var query = (from nopcmsPAVM in _nopCmsProductAttributeValueMapping.Table
							 where nopcmsPAVM.ProductAttributeValueId == productAttributeValueId
							 select nopcmsPAVM);
			if(query.Any())
			{
				_nopCmsProductAttributeValueMapping.Delete(query.FirstOrDefault());
			}
			
		}

		public NopCmsProductAttributeValueMapping GetNopCmsProductAttributeValueMappingByProductAttributeValueId(int productAttributeValueId)
		{
			if (productAttributeValueId <= 0)
				throw new ArgumentNullException("productAttributeVlaueId");

			var query = (from nopcmsPAVM in _nopCmsProductAttributeValueMapping.Table
							 where nopcmsPAVM.ProductAttributeValueId == productAttributeValueId
							 select nopcmsPAVM);
			return query.FirstOrDefault();
		}

		#endregion

		#endregion
	}
}
