﻿using System.Collections.Generic;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial interface ICmsNopOrderMappingService
	{
		void Insert(CmsOrderNopOrderMapping cmsOrderNopOrderMapping);

		void Update(CmsOrderNopOrderMapping cmsOrderNopOrderMapping);

		void Delete(CmsOrderNopOrderMapping cmsOrderNopOrderMapping);

		CmsOrderNopOrderMapping GetByNopOrderId(int nopOrderId);

		CmsOrderNopOrderMapping GetByCmsOrderId(int cmsOrderId);

		List<CmsOrderNopOrderMapping> GetAllOrderMappings();
	}
}
