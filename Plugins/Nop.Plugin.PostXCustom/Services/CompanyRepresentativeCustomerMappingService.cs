﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial class CompanyRepresentativeCustomerMappingService : ICompanyRepresentativeCustomerMappingService
	{
		#region Fields
		private readonly IRepository<CompanyRepresentativeCustomerMapping> _salesPersonCustomerMappingRepository;
		private readonly IRepository<GenericAttribute> _gaRepository;
		private readonly IWorkContext _workContext;
		private readonly IRepository<Customer> _customerRepository;
		private readonly IRepository<CustomerRole> _customerRoleRepository;
		private readonly IRepository<Address> _addressRepository;
		private readonly IRepository<Order> _orderRepository;
		private readonly IRepository<OrderItem> _orderItemRepository;
		#endregion

		#region Ctor
		public CompanyRepresentativeCustomerMappingService(IRepository<CompanyRepresentativeCustomerMapping> salesPersonCustomerMappingRepository,
			IRepository<GenericAttribute> gaRepository,
			IWorkContext workContext,
			IRepository<Customer> customerRepository,
			IRepository<CustomerRole> customerRoleRepository,
			IRepository<Address> addressRepository,
			IRepository<Order> orderRepository,
			IRepository<OrderItem> orderItemRepository)
		{
			_salesPersonCustomerMappingRepository = salesPersonCustomerMappingRepository;
			_gaRepository = gaRepository;
			_workContext = workContext;
			_customerRepository = customerRepository;
			_customerRoleRepository = customerRoleRepository;
			_addressRepository = addressRepository;
			_orderRepository = orderRepository;
			_orderItemRepository = orderItemRepository;
		}
		#endregion

		#region Methods
		public void Insert(CompanyRepresentativeCustomerMapping salesPersonCustomerMapping)
		{
			if (salesPersonCustomerMapping == null)
				throw new ArgumentNullException("salesPersonCustomerMapping");
			_salesPersonCustomerMappingRepository.Insert(salesPersonCustomerMapping);
		}

		public void Update(CompanyRepresentativeCustomerMapping salesPersonCustomerMapping)
		{
			if (salesPersonCustomerMapping == null)
				throw new ArgumentNullException("salesPersonCustomerMapping");

			_salesPersonCustomerMappingRepository.Update(salesPersonCustomerMapping);
		}

		public void Delete(CompanyRepresentativeCustomerMapping salesPersonCustomerMapping)
		{
			if (salesPersonCustomerMapping == null)
				throw new ArgumentNullException("salesPersonCustomerMapping");
			_salesPersonCustomerMappingRepository.Delete(salesPersonCustomerMapping);
		}

		public CompanyRepresentativeCustomerMapping GetByCustomerId(int customerId)
		{
			var query = _salesPersonCustomerMappingRepository.Table.FirstOrDefault(x => x.AssociatedCustomerId == customerId);
			return query;
		}

		public CompanyRepresentativeCustomerMapping GetById(int id)
		{
			var query = _salesPersonCustomerMappingRepository.Table.FirstOrDefault(x => x.Id == id);
			return query;
		}

		public List<CompanyRepresentativeCustomerMapping> GetCustomersBySalesPerson(int salesPersonId)
		{
			var query = _salesPersonCustomerMappingRepository.Table.Where(x => x.CompanyRepresentativeId == salesPersonId);
			return query.ToList();
		}

		public virtual List<int> GetCustomersBySalesPersonId(int salesPersonId)
		{
			if (salesPersonId == null)
				throw new ArgumentNullException("salesPersonId");

			var query = from sp in _salesPersonCustomerMappingRepository.Table
							where sp.CompanyRepresentativeId == salesPersonId
							select sp.AssociatedCustomerId;
			return query.ToList();
		}

		#endregion

		#region SalespersonCustomer
		/// <summary>
		/// Gets all customers
		/// </summary>
		/// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
		/// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
		/// <param name="affiliateId">Affiliate identifier</param>
		/// <param name="vendorId">Vendor identifier</param>
		/// <param name="customerRoleIds">A list of customer role identifiers to filter by (at least one match); pass null or empty list in order to load all customers; </param>
		/// <param name="email">Email; null to load all customers</param>
		/// <param name="username">Username; null to load all customers</param>
		/// <param name="firstName">First name; null to load all customers</param>
		/// <param name="lastName">Last name; null to load all customers</param>
		/// <param name="dayOfBirth">Day of birth; 0 to load all customers</param>
		/// <param name="monthOfBirth">Month of birth; 0 to load all customers</param>
		/// <param name="company">Company; null to load all customers</param>
		/// <param name="phone">Phone; null to load all customers</param>
		/// <param name="zipPostalCode">Phone; null to load all customers</param>
		/// <param name="ipAddress">IP address; null to load all customers</param>
		/// <param name="loadOnlyWithShoppingCart">Value indicating whether to load customers only with shopping cart</param>
		/// <param name="sct">Value indicating what shopping cart type to filter; userd when 'loadOnlyWithShoppingCart' param is 'true'</param>
		/// <param name="pageIndex">Page index</param>
		/// <param name="pageSize">Page size</param>
		/// <returns>Customers</returns>
		public virtual IPagedList<Customer> GetAllCustomers(DateTime? createdFromUtc = null,
			 DateTime? createdToUtc = null, int affiliateId = 0, int vendorId = 0,
			 int[] customerRoleIds = null, string email = null, string username = null,
			 string firstName = null, string lastName = null,
			 int dayOfBirth = 0, int monthOfBirth = 0,
			 string company = null, string phone = null, string zipPostalCode = null,
			 string ipAddress = null, bool loadOnlyWithShoppingCart = false, ShoppingCartType? sct = null,
			 int pageIndex = 0, int pageSize = int.MaxValue, List<int> customerIds = null, string keywords = null)
		{
			var query = _customerRepository.Table;


			var companyId = _workContext.CurrentCustomer.Username.Substring(0, 3);
			query = query.Where(c => c.Username.StartsWith(companyId));


			if (customerRoleIds != null && customerRoleIds.Length > 0)
				query = query.Where(c => c.CustomerRoles.Select(cr => cr.Id).Intersect(customerRoleIds).Any());

			if (createdFromUtc.HasValue)
				query = query.Where(c => createdFromUtc.Value <= c.CreatedOnUtc);
			if (createdToUtc.HasValue)
				query = query.Where(c => createdToUtc.Value >= c.CreatedOnUtc);


			//Get Customer Ids 
			if (!_workContext.CurrentCustomer.IsAdmin())
			{

				query = query.Where(c => customerIds.Contains(c.Id));
			}
			else
			{
				if (customerIds != null)
					if (customerIds.Count > 0)
						query = query.Where(c => customerIds.Contains(c.Id));
			}


			if (affiliateId > 0)
				query = query.Where(c => affiliateId == c.AffiliateId);
			if (vendorId > 0)
				query = query.Where(c => vendorId == c.VendorId);
			query = query.Where(c => !c.Deleted);

			if (!String.IsNullOrWhiteSpace(email))
				query = query.Where(c => c.Email.Contains(email));
			if (!String.IsNullOrWhiteSpace(username))
				query = query.Where(c => c.Username.Contains(username));

			if (!String.IsNullOrWhiteSpace(firstName))
			{
				query = query
					 .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Customer = x, Attribute = y })
					 .Where((z => z.Attribute.KeyGroup == "Customer" &&
						  z.Attribute.Key == SystemCustomerAttributeNames.FirstName &&
						  z.Attribute.Value.Contains(firstName)))
					 .Select(z => z.Customer);
			}

			if (!String.IsNullOrWhiteSpace(lastName))
			{

				query = query
					 .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Customer = x, Attribute = y })
					 .Where((z => z.Attribute.KeyGroup == "Customer" &&
						  z.Attribute.Key == SystemCustomerAttributeNames.LastName &&
						  z.Attribute.Value.Contains(lastName)))
					 .Select(z => z.Customer);
			}
			//date of birth is stored as a string into database.
			//we also know that date of birth is stored in the following format YYYY-MM-DD (for example, 1983-02-18).
			//so let's search it as a string
			if (dayOfBirth > 0 && monthOfBirth > 0)
			{
				//both are specified
				var dateOfBirthStr = monthOfBirth.ToString("00", CultureInfo.InvariantCulture) + "-" + dayOfBirth.ToString("00", CultureInfo.InvariantCulture);

				query = query
					 .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Customer = x, Attribute = y })
					 .Where((z => z.Attribute.KeyGroup == "Customer" &&
						  z.Attribute.Key == SystemCustomerAttributeNames.DateOfBirth &&
						  z.Attribute.Value.Substring(5, 5) == dateOfBirthStr))
					 .Select(z => z.Customer);
			}
			else if (dayOfBirth > 0)
			{
				//only day is specified
				var dateOfBirthStr = dayOfBirth.ToString("00", CultureInfo.InvariantCulture);
				//EndsWith is not supported by SQL Server Compact
				//so let's use the following workaround http://social.msdn.microsoft.com/Forums/is/sqlce/thread/0f810be1-2132-4c59-b9ae-8f7013c0cc00

				//we also cannot use Length function in SQL Server Compact (not supported in this context)
				//z.Attribute.Value.Length - dateOfBirthStr.Length = 8
				//dateOfBirthStr.Length = 2
				query = query
					 .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Customer = x, Attribute = y })
					 .Where((z => z.Attribute.KeyGroup == "Customer" &&
						  z.Attribute.Key == SystemCustomerAttributeNames.DateOfBirth &&
						  z.Attribute.Value.Substring(8, 2) == dateOfBirthStr))
					 .Select(z => z.Customer);
			}
			else if (monthOfBirth > 0)
			{
				//only month is specified
				var dateOfBirthStr = "-" + monthOfBirth.ToString("00", CultureInfo.InvariantCulture) + "-";
				query = query
					 .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Customer = x, Attribute = y })
					 .Where((z => z.Attribute.KeyGroup == "Customer" &&
						  z.Attribute.Key == SystemCustomerAttributeNames.DateOfBirth &&
						  z.Attribute.Value.Contains(dateOfBirthStr)))
					 .Select(z => z.Customer);
			}
			////search by company
			//search by company
			if (!String.IsNullOrWhiteSpace(company))
			{
				query = query
					 .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Customer = x, Attribute = y })
					 .Where((z => z.Attribute.KeyGroup == "Customer" &&
						  z.Attribute.Key == SystemCustomerAttributeNames.Company &&
						  z.Attribute.Value.Contains(company)))
					 .Select(z => z.Customer);
			}
			//search by phone
			if (!String.IsNullOrWhiteSpace(phone))
			{
				query = query
					 .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Customer = x, Attribute = y })
					 .Where((z => z.Attribute.KeyGroup == "Customer" &&
						  z.Attribute.Key == SystemCustomerAttributeNames.Phone &&
						  z.Attribute.Value.Contains(phone)))
					 .Select(z => z.Customer);
			}
			//search by zip
			if (!String.IsNullOrWhiteSpace(zipPostalCode))
			{
				query = query
					 .Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Customer = x, Attribute = y })
					 .Where((z => z.Attribute.KeyGroup == "Customer" &&
						  z.Attribute.Key == SystemCustomerAttributeNames.ZipPostalCode &&
						  z.Attribute.Value.Contains(zipPostalCode)))
					 .Select(z => z.Customer);
			}

			//search by IpAddress
			if (!String.IsNullOrWhiteSpace(ipAddress) && CommonHelper.IsValidIpAddress(ipAddress))
			{
				query = query.Where(w => w.LastIpAddress == ipAddress);
			}

			if (loadOnlyWithShoppingCart)
			{
				int? sctId = null;
				if (sct.HasValue)
					sctId = (int)sct.Value;

				query = sct.HasValue ?
					 query.Where(c => c.ShoppingCartItems.Any(x => x.ShoppingCartTypeId == sctId)) :
					 query.Where(c => c.ShoppingCartItems.Any());
			}




			if (keywords != null)
			{
				query = query.Join(_gaRepository.Table, x => x.Id, y => y.EntityId, (x, y) => new { Customer = x, Attribute = y })
						  .Where((z => z.Attribute.KeyGroup == "Customer" &&
								(z.Attribute.Key == SystemCustomerAttributeNames.FirstName ||
								z.Attribute.Key == SystemCustomerAttributeNames.LastName) &&
								(z.Attribute.Value + " " + z.Attribute.Value).Contains(keywords)
								))
						  .Select(z => z.Customer);
			}

			query = query.OrderByDescending(c => c.CreatedOnUtc);

			IPagedList<Customer> customers = null;

			customers = new PagedList<Customer>(query, pageIndex, pageSize);

			return customers;
		}
		#endregion

		#region SalesPersonOrder
		/// <summary>
		/// Search orders
		/// </summary>
		/// <param name="storeId">Store identifier; 0 to load all orders</param>
		/// <param name="vendorId">Vendor identifier; null to load all orders</param>
		/// <param name="customerId">Customer identifier; 0 to load all orders</param>
		/// <param name="productId">Product identifier which was purchased in an order; 0 to load all orders</param>
		/// <param name="affiliateId">Affiliate identifier; 0 to load all orders</param>
		/// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
		/// <param name="warehouseId">Warehouse identifier, only orders with products from a specified warehouse will be loaded; 0 to load all orders</param>
		/// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
		/// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
		/// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
		/// <param name="osIds">Order status identifiers; null to load all orders</param>
		/// <param name="psIds">Payment status identifiers; null to load all orders</param>
		/// <param name="ssIds">Shipping status identifiers; null to load all orders</param>
		/// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
		/// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
		/// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
		/// <param name="pageIndex">Page index</param>
		/// <param name="pageSize">Page size</param>
		/// <returns>Orders</returns>
		public virtual IPagedList<Order> SearchOrders(int storeId = 0,
			 int vendorId = 0, int customerId = 0,
			 int productId = 0, int affiliateId = 0, int warehouseId = 0,
			 int billingCountryId = 0, string paymentMethodSystemName = null,
			 DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
			 List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
			 string billingEmail = null, string billingLastName = "",
			 string orderNotes = null, int pageIndex = 0, int pageSize = int.MaxValue, List<int> customerIds = null)
		{
			var query = _orderRepository.Table;
			//Get Customer Ids 
			if (!_workContext.CurrentCustomer.IsAdmin())
			{

				query = query.Where(o => customerIds.Contains(o.CustomerId));
			}



			if (storeId > 0)
				query = query.Where(o => o.StoreId == storeId);
			if (vendorId > 0)
			{
				query = query
					 .Where(o => o.OrderItems
					 .Any(orderItem => orderItem.Product.VendorId == vendorId));
			}
			if (customerId > 0)
				query = query.Where(o => o.CustomerId == customerId);
			if (productId > 0)
			{
				query = query
					 .Where(o => o.OrderItems
					 .Any(orderItem => orderItem.Product.Id == productId));
			}
			if (warehouseId > 0)
			{
				var manageStockInventoryMethodId = (int)ManageInventoryMethod.ManageStock;
				query = query
					 .Where(o => o.OrderItems
					 .Any(orderItem =>
						  //"Use multiple warehouses" enabled
						  //we search in each warehouse
						  (orderItem.Product.ManageInventoryMethodId == manageStockInventoryMethodId &&
						  orderItem.Product.UseMultipleWarehouses &&
						  orderItem.Product.ProductWarehouseInventory.Any(pwi => pwi.WarehouseId == warehouseId))
						  ||
						  //"Use multiple warehouses" disabled
						  //we use standard "warehouse" property
						  ((orderItem.Product.ManageInventoryMethodId != manageStockInventoryMethodId ||
						  !orderItem.Product.UseMultipleWarehouses) &&
						  orderItem.Product.WarehouseId == warehouseId))
						  );
			}
			if (billingCountryId > 0)
				query = query.Where(o => o.BillingAddress != null && o.BillingAddress.CountryId == billingCountryId);
			if (!String.IsNullOrEmpty(paymentMethodSystemName))
				query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);
			if (affiliateId > 0)
				query = query.Where(o => o.AffiliateId == affiliateId);
			if (createdFromUtc.HasValue)
				query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
			if (createdToUtc.HasValue)
				query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);
			if (osIds != null && osIds.Any())
				query = query.Where(o => osIds.Contains(o.OrderStatusId));
			if (psIds != null && psIds.Any())
				query = query.Where(o => psIds.Contains(o.PaymentStatusId));
			if (ssIds != null && ssIds.Any())
				query = query.Where(o => ssIds.Contains(o.ShippingStatusId));
			if (!String.IsNullOrEmpty(billingEmail))
				query = query.Where(o => o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.Email) && o.BillingAddress.Email.Contains(billingEmail));
			if (!String.IsNullOrEmpty(billingLastName))
				query = query.Where(o => o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.LastName) && o.BillingAddress.LastName.Contains(billingLastName));
			if (!String.IsNullOrEmpty(orderNotes))
				query = query.Where(o => o.OrderNotes.Any(on => on.Note.Contains(orderNotes)));
			query = query.Where(o => !o.Deleted);
			query = query.OrderByDescending(o => o.CreatedOnUtc);

			//database layer paging
			return new PagedList<Order>(query, pageIndex, pageSize);
		}

		/// <summary>
		/// Get profit report
		/// </summary>
		/// <param name="storeId">Store identifier; pass 0 to ignore this parameter</param>
		/// <param name="vendorId">Vendor identifier; pass 0 to ignore this parameter</param>
		/// <param name="orderId">Order identifier; pass 0 to ignore this parameter</param>
		/// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
		/// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
		/// <param name="startTimeUtc">Start date</param>
		/// <param name="endTimeUtc">End date</param>
		/// <param name="osIds">Order status identifiers; null to load all records</param>
		/// <param name="psIds">Payment status identifiers; null to load all records</param>
		/// <param name="ssIds">Shipping status identifiers; null to load all records</param>
		/// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
		/// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
		/// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
		/// <returns>Result</returns>
		public virtual decimal ProfitReport(int storeId = 0, int vendorId = 0,
			 int billingCountryId = 0, int orderId = 0, string paymentMethodSystemName = null,
			 List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
			 DateTime? startTimeUtc = null, DateTime? endTimeUtc = null,
			 string billingEmail = null, string billingLastName = "", string orderNotes = null, List<int> customerIds = null)
		{
			//We cannot use String.IsNullOrEmpty() in SQL Compact
			var dontSearchEmail = String.IsNullOrEmpty(billingEmail);
			//We cannot use String.IsNullOrEmpty() in SQL Compact
			var dontSearchLastName = String.IsNullOrEmpty(billingLastName);
			//We cannot use String.IsNullOrEmpty() in SQL Compact
			var dontSearchOrderNotes = String.IsNullOrEmpty(orderNotes);
			//We cannot use String.IsNullOrEmpty() in SQL Compact
			var dontSearchPaymentMethods = String.IsNullOrEmpty(paymentMethodSystemName);

			var orders = _orderRepository.Table;
			if (osIds != null && osIds.Any())
				orders = orders.Where(o => osIds.Contains(o.OrderStatusId));
			if (psIds != null && psIds.Any())
				orders = orders.Where(o => psIds.Contains(o.PaymentStatusId));
			if (ssIds != null && ssIds.Any())
				orders = orders.Where(o => ssIds.Contains(o.ShippingStatusId));

			var query = from orderItem in _orderItemRepository.Table
							join o in orders on orderItem.OrderId equals o.Id
							where (storeId == 0 || storeId == o.StoreId) &&
									(orderId == 0 || orderId == o.Id) &&
									(billingCountryId == 0 || (o.BillingAddress != null && o.BillingAddress.CountryId == billingCountryId)) &&
									(dontSearchPaymentMethods || paymentMethodSystemName == o.PaymentMethodSystemName) &&
									(!startTimeUtc.HasValue || startTimeUtc.Value <= o.CreatedOnUtc) &&
									(!endTimeUtc.HasValue || endTimeUtc.Value >= o.CreatedOnUtc) &&
									(!o.Deleted) &&
									(vendorId == 0 || orderItem.Product.VendorId == vendorId) &&
									//we do not ignore deleted products when calculating order reports
									//(!p.Deleted)
									(dontSearchEmail || (o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.Email) && o.BillingAddress.Email.Contains(billingEmail))) &&
									(dontSearchLastName || (o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.LastName) && o.BillingAddress.LastName.Contains(billingLastName))) &&
									(dontSearchOrderNotes || o.OrderNotes.Any(oNote => oNote.Note.Contains(orderNotes)))
							select orderItem;

			var productCost = Convert.ToDecimal(query.Sum(orderItem => (decimal?)orderItem.OriginalProductCost * orderItem.Quantity));

			var reportSummary = GetOrderAverageReportLine(
				 storeId: storeId,
				 vendorId: vendorId,
				 billingCountryId: billingCountryId,
				 orderId: orderId,
				 paymentMethodSystemName: paymentMethodSystemName,
				 osIds: osIds,
				 psIds: psIds,
				 ssIds: ssIds,
				 startTimeUtc: startTimeUtc,
				 endTimeUtc: endTimeUtc,
				 billingEmail: billingEmail,
				 billingLastName: billingLastName,
				 orderNotes: orderNotes,
				 customerIds: customerIds);
			var profit = reportSummary.SumOrders - reportSummary.SumShippingExclTax - reportSummary.SumTax - productCost;
			return profit;
		}

		/// <summary>
		/// Get order average report
		/// </summary>
		/// <param name="storeId">Store identifier; pass 0 to ignore this parameter</param>
		/// <param name="vendorId">Vendor identifier; pass 0 to ignore this parameter</param>
		/// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
		/// <param name="orderId">Order identifier; pass 0 to ignore this parameter</param>
		/// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
		/// <param name="osIds">Order status identifiers</param>
		/// <param name="psIds">Payment status identifiers</param>
		/// <param name="ssIds">Shipping status identifiers</param>
		/// <param name="startTimeUtc">Start date</param>
		/// <param name="endTimeUtc">End date</param>
		/// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
		/// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
		/// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
		/// <returns>Result</returns>
		public virtual OrderAverageReportLine GetOrderAverageReportLine(int storeId = 0,
			 int vendorId = 0, int billingCountryId = 0,
			 int orderId = 0, string paymentMethodSystemName = null,
			 List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
			 DateTime? startTimeUtc = null, DateTime? endTimeUtc = null,
			 string billingEmail = null, string billingLastName = "", string orderNotes = null, List<int> customerIds = null)
		{
			var query = _orderRepository.Table;
			query = query.Where(o => !o.Deleted);
			if (storeId > 0)
				query = query.Where(o => o.StoreId == storeId);
			if (orderId > 0)
				query = query.Where(o => o.Id == orderId);
			if (vendorId > 0)
			{
				query = query
					 .Where(o => o.OrderItems
					 .Any(orderItem => orderItem.Product.VendorId == vendorId));
			}
			if (billingCountryId > 0)
				query = query.Where(o => o.BillingAddress != null && o.BillingAddress.CountryId == billingCountryId);
			if (!String.IsNullOrEmpty(paymentMethodSystemName))
				query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);
			if (osIds != null && osIds.Any())
				query = query.Where(o => osIds.Contains(o.OrderStatusId));
			if (psIds != null && psIds.Any())
				query = query.Where(o => psIds.Contains(o.PaymentStatusId));
			if (ssIds != null && ssIds.Any())
				query = query.Where(o => ssIds.Contains(o.ShippingStatusId));
			if (startTimeUtc.HasValue)
				query = query.Where(o => startTimeUtc.Value <= o.CreatedOnUtc);
			if (endTimeUtc.HasValue)
				query = query.Where(o => endTimeUtc.Value >= o.CreatedOnUtc);
			if (!String.IsNullOrEmpty(billingEmail))
				query = query.Where(o => o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.Email) && o.BillingAddress.Email.Contains(billingEmail));
			if (!String.IsNullOrEmpty(billingLastName))
				query = query.Where(o => o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.LastName) && o.BillingAddress.LastName.Contains(billingLastName));
			if (!String.IsNullOrEmpty(orderNotes))
				query = query.Where(o => o.OrderNotes.Any(on => on.Note.Contains(orderNotes)));
			//Get Customer Ids 
			if (!_workContext.CurrentCustomer.IsAdmin())
			{
				query = query.Where(o => customerIds.Contains(o.CustomerId));
			}
			var item = (from oq in query
							group oq by 1 into result
							select new
							{
								OrderCount = result.Count(),
								OrderShippingExclTaxSum = result.Sum(o => o.OrderShippingExclTax),
								OrderTaxSum = result.Sum(o => o.OrderTax),
								OrderTotalSum = result.Sum(o => o.OrderTotal)
							}
						).Select(r => new OrderAverageReportLine
						{
							CountOrders = r.OrderCount,
							SumShippingExclTax = r.OrderShippingExclTaxSum,
							SumTax = r.OrderTaxSum,
							SumOrders = r.OrderTotalSum
						})
							  .FirstOrDefault();

			item = item ?? new OrderAverageReportLine
			{
				CountOrders = 0,
				SumShippingExclTax = decimal.Zero,
				SumTax = decimal.Zero,
				SumOrders = decimal.Zero,
			};
			return item;
		}
		#endregion


		#region Custom Generic Attribute Methods

		public List<Customer> GetAllChildCustomerInfoByCompanyId(string companyId)
		{
			var query = _customerRepository.Table.Where(x =>
				!string.IsNullOrEmpty(x.Username) && x.Username.StartsWith(companyId));
			return query.ToList();
		}

		#endregion
	}
}
