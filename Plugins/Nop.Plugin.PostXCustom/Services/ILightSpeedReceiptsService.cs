﻿using System;
using System.Collections.Generic;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	public partial interface ILightSpeedReceiptsService
	{
		void Insert(LightSpeedReceiptsMapping lightSpeedReceiptsMapping);

		void Update(LightSpeedReceiptsMapping lightSpeedReceiptsMapping);

		void Delete(LightSpeedReceiptsMapping lightSpeedReceiptsMapping);

		LightSpeedReceiptsMapping GetById(int id);

		LightSpeedReceiptsMapping GetByLightSpeedReceiptId(int receiptId);

		LightSpeedReceiptsMapping GetByLightSpeedReceiptIdAndLightSpeedCustomerId(int receiptId, int customerId);

		LightSpeedReceiptsMapping GetByLightSpeedReceiptIdAndNopCustomerId(int receiptId, int customerId);

		List<LightSpeedReceiptsMapping> GetAllReceipts();

		List<LightSpeedReceiptsMapping> GetAllByLightSpeedCustomerId(int customerId);

		List<LightSpeedReceiptsMapping> GetAllByNopCustomerId(int customerId);

		List<LightSpeedReceiptsMapping> GetAllReceiptsFilter(DateTime orderDate, DateTime? toDate,int lightSpeedCustomerId=0, int receiptId = 0, int nopCustomerId = 0);
	}
}
