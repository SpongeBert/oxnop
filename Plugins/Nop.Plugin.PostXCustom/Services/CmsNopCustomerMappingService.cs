﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{	 
	public partial class CmsNopCustomerMappingService : ICmsNopCustomerMappingService
	{
		#region Fields
		private readonly IRepository<CmsCustomerNopCustomerMapping> _cmsCustomerNopCustomerMappingRepository;
		#endregion

		#region Ctor

		public CmsNopCustomerMappingService(IRepository<CmsCustomerNopCustomerMapping> cmsCustomerNopCustomerMappingRepository)
		{
			_cmsCustomerNopCustomerMappingRepository = cmsCustomerNopCustomerMappingRepository;
		}

		#endregion


		#region Methods

		public void Insert(CmsCustomerNopCustomerMapping cmsProductNopProductMapping)
		{
			cmsProductNopProductMapping.Deleted = false;

			if (cmsProductNopProductMapping == null)
				throw new ArgumentNullException("cmsProductNopProductMapping");
			_cmsCustomerNopCustomerMappingRepository.Insert(cmsProductNopProductMapping);

		}

		public void Update(CmsCustomerNopCustomerMapping cmsProductNopProductMapping)
		{
			if (cmsProductNopProductMapping == null)
				throw new ArgumentNullException("cmsProductNopProductMapping");

			_cmsCustomerNopCustomerMappingRepository.Update(cmsProductNopProductMapping);
		}

		public void Delete(CmsCustomerNopCustomerMapping cmsProductNopProductMapping)
		{
			if (cmsProductNopProductMapping == null)
				throw new ArgumentNullException("cmsProductNopProductMapping");
			_cmsCustomerNopCustomerMappingRepository.Delete(cmsProductNopProductMapping);
		}

		public CmsCustomerNopCustomerMapping GetByNopCustomerId(int nopProductId)
		{
			var query = _cmsCustomerNopCustomerMappingRepository.Table.FirstOrDefault(x => x.NopCustomerId == nopProductId);
			return query;
		}

		public CmsCustomerNopCustomerMapping GetByCmsCustomerId(int cmsProductId)
		{
			var query = _cmsCustomerNopCustomerMappingRepository.Table.FirstOrDefault(x => x.CmsCustomerId == cmsProductId);
			return query;
		}

		public List<CmsCustomerNopCustomerMapping> GetAllCustomerMapping(bool showDeleted = false)
		{					
			var query = _cmsCustomerNopCustomerMappingRepository.Table.ToList();

			if (!showDeleted)
			{
				query = query.Where(x => x.Deleted != true).ToList();
			}				

			return query.ToList();
		}

		#endregion
	}

}
