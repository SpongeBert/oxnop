﻿using System.Collections.Generic;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{	
	public partial interface ICmsNopCategoryMappingService
	{
		void Insert(CmsCategoryNopCategoryMapping cmsCategoryNopCategoryMapping);

		void Update(CmsCategoryNopCategoryMapping cmsCategoryNopCategoryMapping);

		void Delete(CmsCategoryNopCategoryMapping cmsCategoryNopCategoryMapping);

		CmsCategoryNopCategoryMapping GetByNopCategoryId(int nopCategoryId);

		CmsCategoryNopCategoryMapping GetByCmsCategoryId(int cmsCategoryId);

		List<CmsCategoryNopCategoryMapping> GetAllCategoryMapping();

        CmsCategoryNopCategoryMapping GetByCmsCategoryName(string cmsCategoryName);


    }
}
