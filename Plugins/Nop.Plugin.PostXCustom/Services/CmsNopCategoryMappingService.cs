﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Services
{
	
	public partial class CmsNopCategoryMappingService : ICmsNopCategoryMappingService
	{
		#region Fields
		private readonly IRepository<CmsCategoryNopCategoryMapping> _cmsCategoryNopCategoryRepository;
		#endregion

		#region Ctor

		public CmsNopCategoryMappingService(IRepository<CmsCategoryNopCategoryMapping> cmsCategoryNopCategoryRepository)
		{
			_cmsCategoryNopCategoryRepository = cmsCategoryNopCategoryRepository;
		}

		#endregion


		#region Methods

		public void Insert(CmsCategoryNopCategoryMapping cmsProductNopProductMapping)
		{
			if (cmsProductNopProductMapping == null)
				throw new ArgumentNullException("cmsProductNopProductMapping");
			_cmsCategoryNopCategoryRepository.Insert(cmsProductNopProductMapping);

		}

		public void Update(CmsCategoryNopCategoryMapping cmsProductNopProductMapping)
		{
			if (cmsProductNopProductMapping == null)
				throw new ArgumentNullException("cmsProductNopProductMapping");

			_cmsCategoryNopCategoryRepository.Update(cmsProductNopProductMapping);
		}

		public void Delete(CmsCategoryNopCategoryMapping cmsProductNopProductMapping)
		{
			if (cmsProductNopProductMapping == null)
				throw new ArgumentNullException("cmsProductNopProductMapping");
			_cmsCategoryNopCategoryRepository.Delete(cmsProductNopProductMapping);
		}

		public CmsCategoryNopCategoryMapping GetByNopCategoryId(int nopProductId)
		{
			var query = _cmsCategoryNopCategoryRepository.Table.FirstOrDefault(x => x.NopCategoryId == nopProductId);
			return query;
		}

		public CmsCategoryNopCategoryMapping GetByCmsCategoryId(int cmsProductId)
		{
			var query = _cmsCategoryNopCategoryRepository.Table.FirstOrDefault(x => x.CmsCategoryId == cmsProductId);
			return query;
		}

        public CmsCategoryNopCategoryMapping GetByCmsCategoryName(string cmsCategoryName)
        {
            var query = _cmsCategoryNopCategoryRepository.Table.FirstOrDefault(x => x.CmsCategoryName.ToLower() == cmsCategoryName.ToLower());
            return query;
        }

        public List<CmsCategoryNopCategoryMapping> GetAllCategoryMapping()
		{
			var query = _cmsCategoryNopCategoryRepository.Table;
			return query.ToList();
		}

		#endregion
	}
}
