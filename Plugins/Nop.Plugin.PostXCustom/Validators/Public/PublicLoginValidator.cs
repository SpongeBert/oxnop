﻿using FluentValidation;
using Nop.Core.Domain.Customers;
using Nop.Plugin.PostXCustom.Models.Public.Customers;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.PostXCustom.Validators.Public
{	
	public partial class PublicLoginValidator : BaseNopValidator<PublicLoginModel>
	{
		public PublicLoginValidator(ILocalizationService localizationService, CustomerSettings customerSettings)
		{
			if (!customerSettings.UsernamesEnabled)
			{
				//login by email
				RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.Login.Fields.Email.Required"));
				RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
			}
		}
	}
}
