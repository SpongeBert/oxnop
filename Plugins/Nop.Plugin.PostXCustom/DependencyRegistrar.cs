﻿using Autofac;
using Autofac.Core;
using Nop.Core.Configuration;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.PostXCustom.Data;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Factories.Public;
using Nop.Plugin.PostXCustom.Services;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom
{
	public partial class DependencyRegistrar : IDependencyRegistrar
	{
		#region IDependencyRegistrar Members

		/// <summary>
		/// Register Services and Interfaces
		/// </summary>
		/// <param name="builder">Container builder</param>
		/// <param name="typeFinder">Type finder</param>
		/// <param name="config">Config</param>
		public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
		{	
			builder.RegisterType<CompanyRepresentativeCustomerMappingService>().As<ICompanyRepresentativeCustomerMappingService>().InstancePerLifetimeScope();
			builder.RegisterType<CmsNopProductMappingService>().As<ICmsNopProductMappingService>().InstancePerLifetimeScope();
			builder.RegisterType<CmsNopCategoryMappingService>().As<ICmsNopCategoryMappingService>().InstancePerLifetimeScope();
			builder.RegisterType<CmsNopCustomerMappingService>().As<ICmsNopCustomerMappingService>().InstancePerLifetimeScope();
			builder.RegisterType<CmsNopOrderMappingService>().As<ICmsNopOrderMappingService>().InstancePerLifetimeScope();
			builder.RegisterType<LightSpeedOfflineOrdersService>().As<ILightSpeedOfflineOrdersService>().InstancePerLifetimeScope();
			builder.RegisterType<LightSpeedReceiptsService>().As<ILightSpeedReceiptsService>().InstancePerLifetimeScope();

			builder.RegisterType<CustomProductService>().As<ICustomProductService>().InstancePerLifetimeScope();


			builder.RegisterType<CustomerModelFactory>().As<ICustomerModelFactory>().InstancePerLifetimeScope();
			builder.RegisterType<CheckoutModelFactory>().As<ICheckoutModelFactory>().InstancePerLifetimeScope();
			builder.RegisterType<AddressModelFactory>().As<IAddressModelFactory>().InstancePerLifetimeScope();
			builder.RegisterType<OrderModelFactory>().As<IOrderModelFactory>().InstancePerLifetimeScope();
			builder.RegisterType<ProductModelFactory>().As<IProductModelFactory>().InstancePerLifetimeScope();	 		
			//data context
			this.RegisterPluginDataContext<PostXCustomObjectContext>(builder, "wines_custom_object_context");
			
			builder.RegisterType<EfRepository<CompanyRepresentativeCustomerMapping>>()
			.As<IRepository<CompanyRepresentativeCustomerMapping>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("wines_custom_object_context")).InstancePerLifetimeScope();
			builder.RegisterType<EfRepository<CmsProductNopProductMapping>>()
			.As<IRepository<CmsProductNopProductMapping>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("wines_custom_object_context")).InstancePerLifetimeScope();

			builder.RegisterType<EfRepository<CmsCategoryNopCategoryMapping>>()
			.As<IRepository<CmsCategoryNopCategoryMapping>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("wines_custom_object_context")).InstancePerLifetimeScope();

			builder.RegisterType<EfRepository<CmsCustomerNopCustomerMapping>>()
			.As<IRepository<CmsCustomerNopCustomerMapping>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("wines_custom_object_context")).InstancePerLifetimeScope();
			builder.RegisterType<EfRepository<CmsOrderNopOrderMapping>>()
			.As<IRepository<CmsOrderNopOrderMapping>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("wines_custom_object_context")).InstancePerLifetimeScope();

			builder.RegisterType<EfRepository<LightSpeedOfflineOrders>>()
			.As<IRepository<LightSpeedOfflineOrders>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("wines_custom_object_context")).InstancePerLifetimeScope();

			builder.RegisterType<EfRepository<LightSpeedReceiptsMapping>>()
			.As<IRepository<LightSpeedReceiptsMapping>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("wines_custom_object_context")).InstancePerLifetimeScope();

			builder.RegisterType<EfRepository<NopCmsProductAttributeMapping>>()
			.As<IRepository<NopCmsProductAttributeMapping>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("wines_custom_object_context")).InstancePerLifetimeScope();

			builder.RegisterType<EfRepository<NopCmsProductAttributeValueMapping>>()
		.As<IRepository<NopCmsProductAttributeValueMapping>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("wines_custom_object_context")).InstancePerLifetimeScope();

		}


		/// <summary>
		/// Order of this registrar implementation
		/// </summary>
		public int Order
		{
			get { return 1; }
		}
		#endregion
	}
}
