﻿using Nop.Core.Domain.Orders;
using Nop.Plugin.PostXCustom.Models.Public.Order;

namespace Nop.Plugin.PostXCustom.Factories.Public
{
	public partial interface IOrderModelFactory
	{
		/// <summary>
		/// Prepare the customer order list model
		/// </summary>
		/// <returns>Customer order list model</returns>
		CustomerOrderListModel PrepareCustomerOrderListModel();

		/// <summary>
		/// Prepare the order details model
		/// </summary>
		/// <param name="order">Order</param>
		/// <returns>Order details model</returns>
		OrderDetailsModel PrepareOrderDetailsModel(Order order);				  

		/// <summary>
		/// Prepare the customer reward points model
		/// </summary>
		/// <param name="page">Number of items page; pass null to load the first page</param>
		/// <returns>Customer reward points model</returns>
		CustomerRewardPointsModel PrepareCustomerRewardPoints();
	}
}
