﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Seo;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Infrastructure;
using Nop.Plugin.PostXCustom.Models.Public.Catalog;
using Nop.Plugin.PostXCustom.Models.Public.Media;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.CustomService;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping.Date;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework.Security.Captcha;

namespace Nop.Plugin.PostXCustom.Factories.Public
{
	/// <summary>
	/// Represents the product model factory
	/// </summary>
	public partial class ProductModelFactory : IProductModelFactory
	{
		#region Fields

		private readonly ISpecificationAttributeService _specificationAttributeService;
		private readonly ICategoryService _categoryService;
		private readonly IManufacturerService _manufacturerService;
		private readonly IProductService _productService;
		private readonly IVendorService _vendorService;
		private readonly IProductTemplateService _productTemplateService;
		private readonly IProductAttributeService _productAttributeService;
		private readonly IWorkContext _workContext;
		private readonly IStoreContext _storeContext;
		private readonly ITaxService _taxService;
		private readonly ICurrencyService _currencyService;
		private readonly IPictureService _pictureService;
		private readonly ILocalizationService _localizationService;
		private readonly IMeasureService _measureService;
		private readonly IPriceCalculationService _priceCalculationService;
		private readonly IPriceFormatter _priceFormatter;
		private readonly IWebHelper _webHelper;
		private readonly IDateTimeHelper _dateTimeHelper;
		private readonly IProductTagService _productTagService;
		private readonly IAclService _aclService;
		private readonly IStoreMappingService _storeMappingService;
		private readonly IPermissionService _permissionService;
		private readonly IDownloadService _downloadService;
		private readonly IProductAttributeParser _productAttributeParser;
		private readonly IDateRangeService _dateRangeService;
		private readonly MediaSettings _mediaSettings;
		private readonly CatalogSettings _catalogSettings;
		private readonly VendorSettings _vendorSettings;
		private readonly CustomerSettings _customerSettings;
		private readonly CaptchaSettings _captchaSettings;
		private readonly SeoSettings _seoSettings;
		private readonly ICacheManager _cacheManager;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ICustomerService _customerService;
		private readonly IRepository<CmsProductNopProductMapping> _cmsProductNopProductMappingRepository;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly IOrderService _orderService;
		private readonly ICustomCodeService _customCodeService;
		
		
		#endregion

		#region Constructors

		public ProductModelFactory(ISpecificationAttributeService specificationAttributeService,
			 ICategoryService categoryService,
			 IManufacturerService manufacturerService,
			 IProductService productService,
			 IVendorService vendorService,
			 IProductTemplateService productTemplateService,
			 IProductAttributeService productAttributeService,
			 IWorkContext workContext,
			 IStoreContext storeContext,
			 ITaxService taxService,
			 ICurrencyService currencyService,
			 IPictureService pictureService,
			 ILocalizationService localizationService,
			 IMeasureService measureService,
			 IPriceCalculationService priceCalculationService,
			 IPriceFormatter priceFormatter,
			 IWebHelper webHelper,
			 IDateTimeHelper dateTimeHelper,
			 IProductTagService productTagService,
			 IAclService aclService,
			 IStoreMappingService storeMappingService,
			 IPermissionService permissionService,
			 IDownloadService downloadService,
			 IProductAttributeParser productAttributeParser,
			 IDateRangeService dateRangeService,
			 MediaSettings mediaSettings,
			 CatalogSettings catalogSettings,
			 VendorSettings vendorSettings,
			 CustomerSettings customerSettings,
			 CaptchaSettings captchaSettings,
			 SeoSettings seoSettings,
			 ICacheManager cacheManager, IGenericAttributeService genericAttributeService, ICustomerService customerService
			,IRepository<CmsProductNopProductMapping> cmsProductNopProductMappingRepository, ICmsNopProductMappingService cmsNopProductMappingService,
			IOrderService orderService, ICustomCodeService customCodeService)
		{
			this._specificationAttributeService = specificationAttributeService;
			this._categoryService = categoryService;
			this._manufacturerService = manufacturerService;
			this._productService = productService;
			this._vendorService = vendorService;
			this._productTemplateService = productTemplateService;
			this._productAttributeService = productAttributeService;
			this._workContext = workContext;
			this._storeContext = storeContext;
			this._taxService = taxService;
			this._currencyService = currencyService;
			this._pictureService = pictureService;
			this._localizationService = localizationService;
			this._measureService = measureService;
			this._priceCalculationService = priceCalculationService;
			this._priceFormatter = priceFormatter;
			this._webHelper = webHelper;
			this._dateTimeHelper = dateTimeHelper;
			this._productTagService = productTagService;
			this._aclService = aclService;
			this._storeMappingService = storeMappingService;
			this._permissionService = permissionService;
			this._downloadService = downloadService;
			this._productAttributeParser = productAttributeParser;
			this._dateRangeService = dateRangeService;
			this._mediaSettings = mediaSettings;
			this._catalogSettings = catalogSettings;
			this._vendorSettings = vendorSettings;
			this._customerSettings = customerSettings;
			this._captchaSettings = captchaSettings;
			this._seoSettings = seoSettings;
			this._cacheManager = cacheManager;

			_genericAttributeService = genericAttributeService;
			_customerService = customerService;
			_cmsProductNopProductMappingRepository = cmsProductNopProductMappingRepository;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_orderService = orderService;
			_customCodeService = customCodeService;
			
		}

		#endregion

		#region Utilities


		


		public string CheckCompanyMappingAvailability(string actualCompanyId)
		{
			var checkMappings = _cmsNopProductMappingService.GetByCompanyId(actualCompanyId);
			if (checkMappings.Any())
			{
				return actualCompanyId;
			}
			else
			{
				return "001";
			}

		}

		/// <summary>
		/// Prepare the product review overview model
		/// </summary>
		/// <param name="product">Product</param>
		/// <returns>Product review overview model</returns>
		protected virtual ProductReviewOverviewModel PrepareProductReviewOverviewModel(Product product)
		{
			ProductReviewOverviewModel productReview;

			if (_catalogSettings.ShowProductReviewsPerStore)
			{
				string cacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_REVIEWS_MODEL_KEY, product.Id, _storeContext.CurrentStore.Id);

				productReview = _cacheManager.Get(cacheKey, () =>
				{
					return new ProductReviewOverviewModel
					{
						RatingSum = product.ProductReviews
										 .Where(pr => pr.IsApproved && pr.StoreId == _storeContext.CurrentStore.Id)
										 .Sum(pr => pr.Rating),
						TotalReviews = product
										 .ProductReviews
										 .Count(pr => pr.IsApproved && pr.StoreId == _storeContext.CurrentStore.Id)
					};
				});
			}
			else
			{
				productReview = new ProductReviewOverviewModel()
				{
					RatingSum = product.ApprovedRatingSum,
					TotalReviews = product.ApprovedTotalReviews
				};
			}
			if (productReview != null)
			{
				productReview.ProductId = product.Id;
				productReview.AllowCustomerReviews = product.AllowCustomerReviews;
			}
			return productReview;
		}

		/// <summary>
		/// Prepare the product overview price model
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="forceRedirectionAfterAddingToCart">Whether to force redirection after adding to cart</param>
		/// <returns>Product overview price model</returns>
		protected virtual ProductOverviewModel.ProductPriceModel PrepareProductOverviewPriceModel(Product product, bool forceRedirectionAfterAddingToCart = false)
		{
			if (product == null)
				throw new ArgumentNullException("product");

			var priceModel = new ProductOverviewModel.ProductPriceModel
			{
				ForceRedirectionAfterAddingToCart = forceRedirectionAfterAddingToCart
			};

			switch (product.ProductType)
			{
				case ProductType.GroupedProduct:
					{
						#region Grouped product

						var associatedProducts = _productService.GetAssociatedProducts(product.Id,
							 _storeContext.CurrentStore.Id);

						//add to cart button (ignore "DisableBuyButton" property for grouped products)
						priceModel.DisableBuyButton =
							 !_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart) ||
							 !_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);

						//add to wishlist button (ignore "DisableWishlistButton" property for grouped products)
						priceModel.DisableWishlistButton =
							 !_permissionService.Authorize(StandardPermissionProvider.EnableWishlist) ||
							 !_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);

						//compare products
						priceModel.DisableAddToCompareListButton = !_catalogSettings.CompareProductsEnabled;
						switch (associatedProducts.Count)
						{
							case 0:
								{
									//no associated products
								}
								break;
							default:
								{
									//we have at least one associated product
									//compare products
									priceModel.DisableAddToCompareListButton = !_catalogSettings.CompareProductsEnabled;
									//priceModel.AvailableForPreOrder = false;

									if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
									{
										//find a minimum possible price
										decimal? minPossiblePrice = null;
										Product minPriceProduct = null;
										foreach (var associatedProduct in associatedProducts)
										{
											var tmpMinPossiblePrice = _priceCalculationService.GetFinalPrice(associatedProduct, _workContext.CurrentCustomer);

											if (associatedProduct.HasTierPrices)
											{
												//calculate price for the maximum quantity if we have tier prices, and choose minimal
												tmpMinPossiblePrice = Math.Min(tmpMinPossiblePrice,
													 _priceCalculationService.GetFinalPrice(associatedProduct, _workContext.CurrentCustomer, quantity: int.MaxValue));
											}

											if (!minPossiblePrice.HasValue || tmpMinPossiblePrice < minPossiblePrice.Value)
											{
												minPriceProduct = associatedProduct;
												minPossiblePrice = tmpMinPossiblePrice;
											}
										}
										if (minPriceProduct != null && !minPriceProduct.CustomerEntersPrice)
										{
											if (minPriceProduct.CallForPrice)
											{
												priceModel.OldPrice = null;
												priceModel.Price = _localizationService.GetResource("Products.CallForPrice");
											}
											else if (minPossiblePrice.HasValue)
											{
												//calculate prices
												decimal taxRate;
												decimal finalPriceBase = _taxService.GetProductPrice(minPriceProduct, minPossiblePrice.Value, out taxRate);
												decimal finalPrice = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceBase, _workContext.WorkingCurrency);

												priceModel.OldPrice = null;
												priceModel.Price = String.Format(_localizationService.GetResource("Products.PriceRangeFrom"), _priceFormatter.FormatPrice(finalPrice));
												priceModel.PriceValue = finalPrice;

												//PAngV baseprice (used in Germany)
												priceModel.BasePricePAngV = product.FormatBasePrice(finalPrice,
													 _localizationService, _measureService, _currencyService, _workContext,
													 _priceFormatter);
											}
											else
											{
												//Actually it's not possible (we presume that minimalPrice always has a value)
												//We never should get here
												Debug.WriteLine("Cannot calculate minPrice for product #{0}", product.Id);
											}
										}
									}
									else
									{
										//hide prices
										priceModel.OldPrice = null;
										priceModel.Price = null;
									}
								}
								break;
						}

						#endregion
					}
					break;
				case ProductType.SimpleProduct:
				default:
					{
						#region Simple product

						//add to cart button
						priceModel.DisableBuyButton = product.DisableBuyButton ||
																!_permissionService.Authorize(StandardPermissionProvider.EnableShoppingCart) ||
																!_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);

						//add to wishlist button
						priceModel.DisableWishlistButton = product.DisableWishlistButton ||
																	  !_permissionService.Authorize(StandardPermissionProvider.EnableWishlist) ||
																	  !_permissionService.Authorize(StandardPermissionProvider.DisplayPrices);
						//compare products
						priceModel.DisableAddToCompareListButton = !_catalogSettings.CompareProductsEnabled;

						//rental
						priceModel.IsRental = product.IsRental;

						//pre-order
						if (product.AvailableForPreOrder)
						{
							priceModel.AvailableForPreOrder = !product.PreOrderAvailabilityStartDateTimeUtc.HasValue ||
																		 product.PreOrderAvailabilityStartDateTimeUtc.Value >=
																		 DateTime.UtcNow;
							priceModel.PreOrderAvailabilityStartDateTimeUtc = product.PreOrderAvailabilityStartDateTimeUtc;
						}

						//prices
						if (_permissionService.Authorize(StandardPermissionProvider.DisplayPrices))
						{
							if (!product.CustomerEntersPrice)
							{
								if (product.CallForPrice)
								{
									//call for price
									priceModel.OldPrice = null;
									priceModel.Price = _localizationService.GetResource("Products.CallForPrice");
								}
								else
								{
									//prices
									var minPossiblePrice = _priceCalculationService.GetFinalPrice(product, _workContext.CurrentCustomer);

									if (product.HasTierPrices)
									{
										//calculate price for the maximum quantity if we have tier prices, and choose minimal
										minPossiblePrice = Math.Min(minPossiblePrice,
											 _priceCalculationService.GetFinalPrice(product, _workContext.CurrentCustomer, quantity: int.MaxValue));
									}

									decimal taxRate;
									decimal oldPriceBase = _taxService.GetProductPrice(product, product.OldPrice, out taxRate);
									decimal finalPriceBase = _taxService.GetProductPrice(product, minPossiblePrice, out taxRate);

									decimal oldPrice = _currencyService.ConvertFromPrimaryStoreCurrency(oldPriceBase, _workContext.WorkingCurrency);
									decimal finalPrice = _currencyService.ConvertFromPrimaryStoreCurrency(finalPriceBase, _workContext.WorkingCurrency);

									//do we have tier prices configured?
									var tierPrices = new List<TierPrice>();
									if (product.HasTierPrices)
									{
										tierPrices.AddRange(product.TierPrices.OrderBy(tp => tp.Quantity)
											 .FilterByStore(_storeContext.CurrentStore.Id)
											 .FilterForCustomer(_workContext.CurrentCustomer)
											 .FilterByDate()
											 .RemoveDuplicatedQuantities());
									}
									//When there is just one tier price (with  qty 1), there are no actual savings in the list.
									var displayFromMessage = tierPrices.Any() &&
																	 !(tierPrices.Count == 1 && tierPrices[0].Quantity <= 1);
									if (displayFromMessage)
									{
										priceModel.OldPrice = null;
										priceModel.Price = String.Format(_localizationService.GetResource("Products.PriceRangeFrom"), _priceFormatter.FormatPrice(finalPrice));
										priceModel.PriceValue = finalPrice;
									}
									else
									{
										if (finalPriceBase != oldPriceBase && oldPriceBase != decimal.Zero)
										{
											priceModel.OldPrice = _priceFormatter.FormatPrice(oldPrice);
											priceModel.Price = _priceFormatter.FormatPrice(finalPrice);
											priceModel.PriceValue = finalPrice;
										}
										else
										{
											priceModel.OldPrice = null;
											priceModel.Price = _priceFormatter.FormatPrice(finalPrice);
											priceModel.PriceValue = finalPrice;
										}
									}
									if (product.IsRental)
									{
										//rental product
										priceModel.OldPrice = _priceFormatter.FormatRentalProductPeriod(product,
											 priceModel.OldPrice);
										priceModel.Price = _priceFormatter.FormatRentalProductPeriod(product,
											 priceModel.Price);
									}


									//property for German market
									//we display tax/shipping info only with "shipping enabled" for this product
									//we also ensure this it's not free shipping
									priceModel.DisplayTaxShippingInfo = _catalogSettings.DisplayTaxShippingInfoProductBoxes
																					&& product.IsShipEnabled &&
																					!product.IsFreeShipping;


									//PAngV baseprice (used in Germany)
									priceModel.BasePricePAngV = product.FormatBasePrice(finalPrice,
										 _localizationService, _measureService, _currencyService, _workContext,
										 _priceFormatter);
								}
							}
						}
						else
						{
							//hide prices
							priceModel.OldPrice = null;
							priceModel.Price = null;
						}

						#endregion
					}
					break;
			}

			return priceModel;
		}

		/// <summary>
		/// Prepare the product overview picture model
		/// </summary>
		/// <param name="product">Product</param>
		/// <param name="productThumbPictureSize">Product thumb picture size (longest side); pass null to use the default value of media settings</param>
		/// <returns>Picture model</returns>
		protected virtual PublicPictureModel PrepareProductOverviewPictureModel(Product product, int? productThumbPictureSize = null)
		{
			if (product == null)
				throw new ArgumentNullException("product");

			var productName = product.GetLocalized(x => x.Name);
			//If a size has been set in the view, we use it in priority
			int pictureSize = productThumbPictureSize.HasValue
				 ? productThumbPictureSize.Value
				 : _mediaSettings.ProductThumbPictureSize;

			//prepare picture model
			var cacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_DEFAULTPUBLICPICTURE_MODEL_KEY,
				 product.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(),
				 _storeContext.CurrentStore.Id);

			PublicPictureModel defaultPictureModel = _cacheManager.Get(cacheKey, () =>
			{
				var picture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
				var pictureModel = new PublicPictureModel
				{
					ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
					FullSizeImageUrl = _pictureService.GetPictureUrl(picture)
				};
				//"title" attribute
				pictureModel.Title = (picture != null && !string.IsNullOrEmpty(picture.TitleAttribute))
					 ? picture.TitleAttribute
					 : string.Format(_localizationService.GetResource("Media.Product.ImageLinkTitleFormat"), productName);
				//"alt" attribute
				pictureModel.AlternateText = (picture != null && !string.IsNullOrEmpty(picture.AltAttribute))
					 ? picture.AltAttribute
					 : string.Format(_localizationService.GetResource("Media.Product.ImageAlternateTextFormat"),
						  productName);

				return pictureModel;
			});

			return defaultPictureModel;
		}
		/// <summary>
		/// Prepare the product specification models
		/// </summary>
		/// <param name="product">Product</param>
		/// <returns>List of product specification model</returns>
		public virtual IList<ProductSpecificationModel> PrepareProductSpecificationModel(Product product)
		{
			if (product == null)
				throw new ArgumentNullException("product");

			string cacheKey = string.Format(ModelCacheEventConsumer.PRODUCT_SPECS_PICTURE_MODEL_KEY, product.Id, _workContext.WorkingLanguage.Id);
			return _cacheManager.Get(cacheKey, () =>
				 _specificationAttributeService.GetProductSpecificationAttributes(product.Id, 0, null, true)
				 .Select(psa =>
				 {
					 var m = new ProductSpecificationModel
					 {
						 SpecificationAttributeId = psa.SpecificationAttributeOption.SpecificationAttributeId,
						 SpecificationAttributeName = psa.SpecificationAttributeOption.SpecificationAttribute.GetLocalized(x => x.Name),
						 ColorSquaresRgb = psa.SpecificationAttributeOption.ColorSquaresRgb
					 };

					 switch (psa.AttributeType)
					 {
						 case SpecificationAttributeType.Option:
							 m.ValueRaw = HttpUtility.HtmlEncode(psa.SpecificationAttributeOption.GetLocalized(x => x.Name));
							 break;
						 case SpecificationAttributeType.CustomText:
							 m.ValueRaw = HttpUtility.HtmlEncode(psa.CustomValue);
							 break;
						 case SpecificationAttributeType.CustomHtmlText:
							 m.ValueRaw = psa.CustomValue;
							 break;
						 case SpecificationAttributeType.Hyperlink:
							 m.ValueRaw = string.Format("<a href='{0}' target='_blank'>{0}</a>", psa.CustomValue);
							 break;
						 default:
							 break;
					 }
					 return m;
				 }).ToList()
			);
		}
		#endregion

		#region Methods	

		/// <summary>
		/// Prepare the product overview models
		/// </summary>
		/// <param name="products">Collection of products</param>
		/// <param name="preparePriceModel">Whether to prepare the price model</param>
		/// <param name="preparePictureModel">Whether to prepare the picture model</param>
		/// <param name="productThumbPictureSize">Product thumb picture size (longest side); pass null to use the default value of media settings</param>
		/// <param name="prepareSpecificationAttributes">Whether to prepare the specification attribute models</param>
		/// <param name="forceRedirectionAfterAddingToCart">Whether to force redirection after adding to cart</param>
		/// <returns>Collection of product overview model</returns>
		public virtual IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products,
			 bool preparePriceModel = true, bool preparePictureModel = true,
			 int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
			 bool forceRedirectionAfterAddingToCart = false)
		{
			if (products == null)
				throw new ArgumentNullException("products");

			//Check for company			
			var companyId = CheckCompanyMappingAvailability(_workContext.CurrentCustomer.Username != null ? _workContext.CurrentCustomer.Username.Substring(0, 3) : "001");

																																					  


			//if (_workContext.CurrentCustomer.CompanyId > 0)
			//{
			//	var company = _customerService.GetAllCustomers().OrderBy(x=>x.Id).FirstOrDefault(x => x.CompanyId == _workContext.CurrentCustomer.CompanyId);
			//	if (company != null)
			//	{
			//		companyId = company.Id;
			//	}
			//}

			var models = new List<ProductOverviewModel>();
			if (!string.IsNullOrEmpty(companyId))
			{	

				//var checkMappings = _customerService.GetProductCompaniesByCompanyId(companyId);	 
				//if (checkMappings.Any())
				//{
					foreach (var product in products)
					{
					var productCompanyMapping =(from cmsProduct in _cmsProductNopProductMappingRepository.Table
														 where cmsProduct.Company == companyId && cmsProduct.NopProductId == product.Id
														 select cmsProduct).FirstOrDefault();
					//var productCompanyMapping = _customerService.GetProductCompanyByProductAndCompanyId(product.Id, companyId);
						if (productCompanyMapping == null)
						{
							continue;
						}

						var model = new ProductOverviewModel
						{
							Id = product.Id,
							Name = product.GetLocalized(x => x.Name),
							ShortDescription = product.GetLocalized(x => x.ShortDescription),
							FullDescription = product.GetLocalized(x => x.FullDescription),
							SeName = product.GetSeName(),
							Sku = product.Sku,
							ProductType = product.ProductType,
							MarkAsNew = product.MarkAsNew &&
								  (!product.MarkAsNewStartDateTimeUtc.HasValue || product.MarkAsNewStartDateTimeUtc.Value < DateTime.UtcNow) &&
								  (!product.MarkAsNewEndDateTimeUtc.HasValue || product.MarkAsNewEndDateTimeUtc.Value > DateTime.UtcNow)
						};	  
						

						//price
						if (preparePriceModel)
						{
							model.ProductPrice = PrepareProductOverviewPriceModel(product, forceRedirectionAfterAddingToCart);
						}

						//picture
						if (preparePictureModel)
						{
							model.DefaultPictureModel = PrepareProductOverviewPictureModel(product, productThumbPictureSize);
						}

						//specs
						if (prepareSpecificationAttributes)
						{
							model.SpecificationAttributeModels = PrepareProductSpecificationModel(product);
						}

						//reviews
						model.ReviewOverviewModel = PrepareProductReviewOverviewModel(product);

						models.Add(model);
					}
					return models;
				//}	
			}

			foreach (var product in products)
			{

				var model = new ProductOverviewModel
				{
					Id = product.Id,
					Name = product.GetLocalized(x => x.Name),
					ShortDescription = product.GetLocalized(x => x.ShortDescription),
					FullDescription = product.GetLocalized(x => x.FullDescription),
					SeName = product.GetSeName(),
					Sku = product.Sku,
					ProductType = product.ProductType,
					MarkAsNew = product.MarkAsNew &&
						  (!product.MarkAsNewStartDateTimeUtc.HasValue || product.MarkAsNewStartDateTimeUtc.Value < DateTime.UtcNow) &&
						  (!product.MarkAsNewEndDateTimeUtc.HasValue || product.MarkAsNewEndDateTimeUtc.Value > DateTime.UtcNow)
				};

				//price
				if (preparePriceModel)
				{
					model.ProductPrice = PrepareProductOverviewPriceModel(product, forceRedirectionAfterAddingToCart);
				}

				//picture
				if (preparePictureModel)
				{
					model.DefaultPictureModel = PrepareProductOverviewPictureModel(product, productThumbPictureSize);
				}

				//specs
				if (prepareSpecificationAttributes)
				{
					model.SpecificationAttributeModels = PrepareProductSpecificationModel(product);
				}

				//reviews
				model.ReviewOverviewModel = PrepareProductReviewOverviewModel(product);

				models.Add(model);
			}

			return models;
		}		

		#endregion
	}
}
