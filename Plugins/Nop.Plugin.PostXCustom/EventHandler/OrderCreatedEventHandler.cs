﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Orders;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Core.Domain.Payments;
using Newtonsoft.Json;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Services.CustomService;
using Nop.Services.Messages;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Customers;

namespace Nop.Plugin.PostXCustom.EventHandler
{
	public class OrderCreatedEventHandler : IConsumer<OrderPlacedEvent>
	{
		#region Fields
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;
		private readonly IOrderService _orderService;
		private readonly ICustomerService _customerService;
		private readonly IStoreContext _storeContext;
		private readonly IWorkContext _workContext;
		private readonly ILogger _logger;
		private readonly IEventPublisher _eventPublisher;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly HttpClient _client;
		private readonly IRewardPointService _rewardPointService;
		private readonly ICustomCodeService _customCodeService;

		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly OrderSettings _orderSettings;
		private readonly LocalizationSettings _localizationSettings;
		private readonly IPdfService _pdfService;
		#endregion

		#region Ctor

		public OrderCreatedEventHandler(ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICmsNopProductMappingService cmsNopProductMappingService,
			 ICmsNopOrderMappingService cmsNopOrderMappingService, ICustomerService customerService, IStoreContext storeContext, IWorkContext workContext, ILogger logger, IEventPublisher eventPublisher,
			 IGenericAttributeService genericAttributeService, IOrderService orderService, IRewardPointService rewardPointService, ICustomCodeService customCodeService,
			 IWorkflowMessageService workflowMessageService, OrderSettings orderSettings, LocalizationSettings localizationSettings, IPdfService pdfService)
		{
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_cmsNopOrderMappingService = cmsNopOrderMappingService;
			_orderService = orderService;
			_customerService = customerService;
			_storeContext = storeContext;
			_workContext = workContext;
			_logger = logger;
			_eventPublisher = eventPublisher;
			_genericAttributeService = genericAttributeService;
			_client = new HttpClient();
			_rewardPointService = rewardPointService;
			_customCodeService = customCodeService;

			_workflowMessageService = workflowMessageService;
			_orderSettings = orderSettings;
			_localizationSettings = localizationSettings;
			_pdfService = pdfService;
		}

		#endregion

		public void HandleEvent(OrderPlacedEvent orderPlacedEvent)
		{

			if (orderPlacedEvent.Order.PaymentMethodSystemName.ToLower() == "payments.loyaltypoints")
			{
				LightSpeedOrderCreate(orderPlacedEvent.Order);
			}


			if (orderPlacedEvent.Order.PaymentMethodSystemName.ToLower() == "payments.invoice")
			{
				SendNotificationsAndSaveNotes(orderPlacedEvent.Order.Id);
			}
		}


		public void LightSpeedOrderCreate(Order order)
		{
			#region Push Order to CMS
			try
			{
				if (order.PaymentStatus != PaymentStatus.Paid)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, _workContext.CurrentCustomer);
					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + order.Id,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, "Current payment status :: " + order.PaymentStatus);
					return;
				}

				var checkCmsCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
				if (checkCmsCustomer == null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, _workContext.CurrentCustomer);
					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + order.Id,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, string.Empty);
					return;
				}

				string token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + order.Id,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + order.Id, _workContext.CurrentCustomer);
					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + order.Id,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + order.Id, string.Empty);
					return;
				}

				string url = @"https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + checkCmsCustomer.CmsCustomerId + "/order";
				var postData = new StringBuilder();
				postData.Append("{\"customerId\":" + checkCmsCustomer.CmsCustomerId + ",");

				var currentData = DateTime.Now;
				string newDate = currentData.Year + "-" + currentData.Month + "-" + currentData.Day + "T" + currentData.Hour + ":" + currentData.Minute + ":" + currentData.Second + "+00:00";
				postData.Append("\"deliveryDate\":\"" + newDate + "\",");

				var orderType = GetOrderType(order);
				postData.Append("\"type\":\"" + orderType + "\",");
				postData.Append("\"status\":\"" + "ACCEPTED" + "\",");
				postData.Append("\"description\":\"" + "order" + "\",");

				var deliveryTime = string.Empty;
				var deliveryTimeDescripton = order.CheckoutAttributeDescription.Substring(order.CheckoutAttributeDescription.LastIndexOf("Gev"));
				if (!string.IsNullOrEmpty(deliveryTimeDescripton))
				{
					deliveryTime = deliveryTimeDescripton;
				}

				postData.Append("\"note\":\"" + "Order Reference: " + order.Id + " || " + deliveryTime + "\",");
				postData.Append("\"orderItems\":[");
				var totalItems = order.OrderItems.Count;
				var counter = 0;
				foreach (var orderItem in order.OrderItems)
				{
					counter++;
					var cmsProduct = _cmsNopProductMappingService.GetByNopProductId(orderItem.ProductId);
					if (cmsProduct == null)
					{
						_logger.InsertLog(LogLevel.Error, "Error :: Order sync with CMS :: Nop Order Id :: " + orderItem.OrderId,
							 "Could not find mapped CMS product against Nop Product :: ID :: " + orderItem.ProductId + " :: Product Name :: " +
							 orderItem.Product.Name, _workContext.CurrentCustomer);
						break;
					}

					postData.Append("{\"productId\":" + cmsProduct.CmsProductId + ",");
					postData.Append("\"amount\":" + orderItem.Quantity + ",");
					postData.Append("\"totalPrice\":" + orderItem.PriceInclTax + ",");
					postData.Append("\"totalPriceWithoutVat\":" + orderItem.PriceExclTax + ",");
					postData.Append("\"unitPrice\":" + orderItem.UnitPriceInclTax + ",");
					postData.Append("\"unitPriceWithoutVat\":" + orderItem.UnitPriceExclTax);
					postData.Append(counter >= totalItems ? "}" : "},");
				}

				//Only Cash payment method to be used in case of full loyalty points
				postData.Append("],\"orderPayments\":[");								  
				if (order.RedeemedRewardPointsEntry != null)
				{	
					//Use Cash
					postData.Append("{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
					postData.Append("\"paymentTypeId\":" + 36191 + ",");
					//postData.Append("\"paymentTypeTypeId\":" + 1 + "}");
					postData.Append("\"paymentTypeTypeId\":" + 200 + "}");
				}																						  
				postData.Append("],\"orderTaxInfo\":[{");
				postData.Append("\"tax\":" + "0" + ",");
				postData.Append("\"taxRate\":" + "0" + ",");
				postData.Append("\"totalWithTax\":" + order.OrderTotal + ",");
				postData.Append("\"totalWithoutTax\":" + order.OrderTotal);
				postData.Append("}]}");

				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Order#: " + order.Id, postData.ToString(), _workContext.CurrentCustomer);

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					if (orderType.ToLower() == "delivery")
					{
						UpdateCustomerAddressOnLightSpeed(order, checkCmsCustomer);
					}
					else
					{
						//Update customer name with delivery Info on LS
						UpdateCustomerNameOnLightSpeed(Convert.ToInt32(responseData), checkCmsCustomer, order.Customer, order);
					}

					var cmsOrderNopOrderMap = new CmsOrderNopOrderMapping
					{
						NopOrderId = order.Id,
						CmsOrderId = Convert.ToInt32(responseData),
						CreatedOnUtc = DateTime.UtcNow,
						UpdatedOnUtc = DateTime.UtcNow
					};

					_cmsNopOrderMappingService.Insert(cmsOrderNopOrderMap);

					order.CustomOrderNumber = responseData;
					_orderService.UpdateOrder(order);

					SendNotificationsAndSaveNotes(order.Id);
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error Creating Order on CMS for nop order :: " + order.Id,
					 ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + order.Id, ex.Message, ex.StackTrace);

				return;
			}
			#endregion
		}

		public string GetToken()
		{
			try
			{
				string token;
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		public string GetOrderType(Order order)
		{
			if (order == null)
				return "takeaway";

			if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
			{
				if (order.CheckoutAttributeDescription.ToLower().Contains("levering"))
					return "delivery";
			}
			return "takeaway";
		}

		public void UpdateCustomerAddressOnLightSpeed(Order order, CmsCustomerNopCustomerMapping checkCmsCustomer)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, string.Empty);
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"deliveryCity\":\"" + order.ShippingAddress.City + "\",");
				postData.Append("\"deliveryCountry\":\"" + order.ShippingAddress.Country.TwoLetterIsoCode + "\",");
				postData.Append("\"deliveryStreet\":\"" + order.ShippingAddress.Address1 + "\",");
				postData.Append("\"deliveryStreetNumber\":\"" + order.ShippingAddress.Address2 + "\",");
				postData.Append("\"deliveryZip\":\"" + order.ShippingAddress.ZipPostalCode + "\",");
				postData.Append("\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + order.Customer.Email + "\",");

				var clientName = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
				var clientLastName = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
				{
					var dataArray = order.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1].Trim() + " | " + dataArray[3].Trim();
					clientLastName = order.Customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
				}
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating customer address on LS for order id #" + order.Id, exception.Message + "  ::  " + exception.StackTrace, _workContext.CurrentCustomer);
				_customCodeService.SendErrorEmail("Error updating customer address on LS for order id #" + order.Id, exception.Message, exception.StackTrace);
				return;
			}
		}

		protected void SendNotificationsAndSaveNotes(int orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(orderId);
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId,
						"Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);
					_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order ::  " + orderId, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, string.Empty);
					return;
				}

				//send email notifications
				var orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
				if (orderPlacedStoreOwnerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to store owner) has been queued. Queued email identifier: {0}.", orderPlacedStoreOwnerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}

				var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 _pdfService.PrintOrderToPdf(order) : null;
				var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 "order.pdf" : null;
				var orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService
					 .SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
				if (orderPlacedCustomerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: " + orderId,
					ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);
				_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order :: " + orderId, ex.Message, ex.StackTrace);

				return;
			}
		}		


		public void UpdateCustomerNameOnLightSpeed(int cmsOrderId, CmsCustomerNopCustomerMapping checkCmsCustomer, Customer customer, Order nopOrder)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerNameOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + customer.Email + "\",");

				var clientName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
				var clientLastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(nopOrder.CheckoutAttributeDescription))
				{
					var dataArray = nopOrder.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1].Trim() + " | " + dataArray[3].Trim();
					clientLastName = customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
				}

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating Name for customer on Light Speed CMS :: LS OrderId :: " + cmsOrderId + " || Nop Order Id :: " + nopOrder.Id + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message + "  ::  " + exception.StackTrace);
			}
		}



	}
}
