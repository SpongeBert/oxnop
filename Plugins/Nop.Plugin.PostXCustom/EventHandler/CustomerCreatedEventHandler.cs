﻿using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Logging;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Logging;
using Nop.Services.CustomService;
using Nop.Services.Common;

namespace Nop.Plugin.PostXCustom.EventHandler
{

	public class CustomerCreatedEventHandler : IConsumer<CustomerRegisteredEvent>
	{
		#region Fields
		private readonly ILogger _logger;
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICustomCodeService _customCodeService;
		#endregion

		#region Constructor
		public CustomerCreatedEventHandler(ILogger logger, ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICustomCodeService customCodeService)
		{
			_logger = logger;
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_customCodeService = customCodeService;
		}
		#endregion

		public void HandleEvent(CustomerRegisteredEvent customerRegisteredEvent)
		{
			#region Push Customer to WineShop. Commented Temporarily
			//try
			//{
			//	using (var client = new WebClient())
			//	{

			//		var values = new NameValueCollection();
			//		values["Username"] = customerRegisteredEvent.Customer.Username;
			//		values["Email"] = customerRegisteredEvent.Customer.Email;
			//		values["FirstName"] = customerRegisteredEvent.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
			//		values["LastName"] = customerRegisteredEvent.Customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
			//		values["Company"] = customerRegisteredEvent.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Company);
			//		values["DateOfBirth"] = customerRegisteredEvent.Customer.GetAttribute<string>(SystemCustomerAttributeNames.DateOfBirth);
			//		values["Gender"] = customerRegisteredEvent.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender);

			//		//values["SelectedCustomerRoles"] = customerRegisteredEvent.Customer.;
			//		//customerRegisteredEvent.Customer.GetAttribute<string>(SystemCustomerAttributeNames.pa

			//		//var response = client.UploadValues("http://localhost/wines/CustomerAPI/create", values);
			//		var response = client.UploadValues("http://www.webshopcompany.be/wineshop/CustomerAPI/create", values);

			//		var responseString = Encoding.Default.GetString(response);
			//	}
			//}
			//catch (Exception exception)
			//{
			//	_logger.InsertLog(LogLevel.Error, "OX Customer Sync :: Error pushing customer to wineshop.", exception.StackTrace, _workContext.CurrentCustomer);
			//}
			#endregion

			#region Push Customer to CMS
			var result = CreateCmcCustomer(customerRegisteredEvent.Customer);
			if (!string.IsNullOrEmpty(result))
			{
				var cmsNopCustomerMap = new CmsCustomerNopCustomerMapping
				{
					NopCustomerId = customerRegisteredEvent.Customer.Id,
					CmsCustomerId = Convert.ToInt32(result),
					CreatedOnUtc = DateTime.UtcNow,
					UpdatedOnUtc = DateTime.UtcNow
				};

				_cmsNopCustomerMappingService.Insert(cmsNopCustomerMap);
				CreateCustomerLoyaltyCard(customerRegisteredEvent.Customer, cmsNopCustomerMap.CmsCustomerId);
			}

			#endregion

		}

		public string CreateCmcCustomer(Customer customer)
		{
			try
			{
				string token = GetToken();
				string url = @"https://euc1.posios.com/PosServer/rest/core/customer";
				var postData = new StringBuilder();
																																		
				var clientName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
				var clientLastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);

				postData.Append("{\"city\":\"" + string.Empty + "\",");
				postData.Append("\"companyId\":32123,");
				postData.Append("\"country\":\"" + "BE" + "\",");
				postData.Append("\"credits\":0,");

				postData.Append("\"dateOfBirth\":\"\",");
				postData.Append("\"email\":\"" + customer.Email + "\",");
				//postData.Append("\"firstName\":\"" + "0 reward points available." + "\",");
				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"language\":\"" + "nl_BE" + "\",");
				//postData.Append("\"lastName\":\"" + customer.GetFullName() + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\",");
				postData.Append("\"modificationTime\":\"\",");
				postData.Append("\"note\":\"" + "Test Note" + "\",");
				postData.Append("\"street\":\"\",");
				postData.Append("\"streetNumber\":\"\",");
				postData.Append("\"telephone\":\"\",");
				postData.Append("\"uuid\":\"\",");
				postData.Append("\"vatNumber\":\"\",");
				postData.Append("\"zip\":\"\"}");


				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Customer#: " + customer.Id, postData.ToString());

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				return responseData;
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error creating customer on light speed cms :: Username :: " + customer.Username, "Nop Customer Id: " + customer.Id);

				_customCodeService.SendErrorEmail("Error creating Nop Customer :: " + customer.Username + " on LS CMS", ex.Message, ex.StackTrace);

				return string.Empty;
			}
		}

		public string GetToken()
		{
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			var token = json.GetValue("token").Value<string>();
			return token;
		}




		public string CreateCustomerLoyaltyCard(Customer customer, int cmsCustomerId)
		{
			try
			{
				string token = GetToken();
				string url = @"https://euc1.posios.com/PosServer/rest/core/customer/" + cmsCustomerId + "/loyaltycard";
				var postData = new StringBuilder();
				postData.Append("{\"barcode\":\"" + customer.Username + "\"}");

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				return responseData;
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error creating loyalty card for username: " + customer.Username, "LS Customer Id: " + cmsCustomerId + " || Nop Customer Id: " + customer.Id);
				_customCodeService.SendErrorEmail("Error creating Loyalty Card for Nop Customer ::  " + customer.Username + " || LS CMS Customer ID :: " + cmsCustomerId, ex.Message, ex.StackTrace);
				return string.Empty;
			}
		}

	}
}
