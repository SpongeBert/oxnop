﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Logging;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.CustomService;
using Nop.Services.Logging;
using Nop.Services.Tasks;

namespace Nop.Plugin.PostXCustom.Tasks
{
	public class UpdateCustomerTask : ITask
	{
		#region Fields

		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICustomCodeService _customCodeService;
		private readonly ISettingService _settingService;
		private readonly ILogger _logger;
		private readonly ICustomerService _customerService;
		#endregion

		#region Ctor
		public UpdateCustomerTask(ICmsNopCustomerMappingService cmsNopCustomerMappingService,
			ICustomCodeService customCodeService, ISettingService settingService, ILogger logger, ICustomerService customerService)
		{
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_customCodeService = customCodeService;							 
			_settingService = settingService;
			_logger = logger;
			_customerService = customerService;
		}
		#endregion


		public void Execute()
		{
			try
			{
				if (CheckImportTime())
				{
					_customCodeService.SendInformationEmail("Update Customer Name Task started. Date :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture), "The update customer name task started.");

					var cmsCustomerList = _cmsNopCustomerMappingService.GetAllCustomerMapping();

					foreach (var cmsCustomer in cmsCustomerList)
					{
						//if(cmsCustomer.CmsCustomerId != 210219)
						//	continue;


						var nopCustomer = _customerService.GetCustomerById(cmsCustomer.NopCustomerId);
						if (nopCustomer == null)
						{
							_logger.InsertLog(LogLevel.Error, "Error updating customer names :: Nop Customer not found for customer name details.");
							continue;
						}

						var token = GetToken();
						if (string.IsNullOrEmpty(token))
						{
							_logger.InsertLog(LogLevel.Error, "Error updating customer names :: failed to generate security token.");
							continue;
						}


						UpdateCustomerNameOnLightSpeed(cmsCustomer, nopCustomer);

					}

					_customCodeService.SendInformationEmail("Update Customer Name Task End. Date :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture), "The update customer name task ended.");
				}
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error Updating Customer Name On LS CMS.", exception.Message + "  ||  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Error Updating Customer Name On LS CMS.", exception.Message, exception.StackTrace);
				_settingService.SetSetting("cmscustomerupdateflag", "false");
			}
		}


		public bool CheckImportTime()
		{
			var manualImportSetting = _settingService.GetSetting("manualUpdateCustomer");
			if (manualImportSetting != null)
			{
				if (manualImportSetting.Value.ToLower() == "true")
				{
					return true;
				}
			}

			var checkSetting = _settingService.GetSetting("cmscustomerupdateflag");
			if (DateTime.Now.Hour == 2)
			{
				if (checkSetting == null)
				{
					_settingService.SetSetting("cmscustomerupdateflag", "true");
					return true;
				}

				if (checkSetting.Value == "true")
				{
					return false;
				}

				_settingService.SetSetting("cmscustomerupdateflag", "true");
				return true;
			}
			if (checkSetting == null)
			{
				_settingService.SetSetting("cmscustomerupdateflag", "false");
			}
			else if (checkSetting.Value == "true")
			{
				_settingService.SetSetting("cmscustomerupdateflag", "false");
			}
			return false;
		}


		public string GetToken()
		{
			try
			{
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";
				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				var token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Update Customer Name On LS CMS :: Error :: Failed to generate Token.", exception.Message + "  ||  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Update Customer Name On LS LS CMS :: Error :: Failed to generate Token.", exception.Message, exception.StackTrace);
				return string.Empty;
			}

		}

		public void UpdateCustomerNameOnLightSpeed(CmsCustomerNopCustomerMapping checkCmsCustomer, Customer customer)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerNameOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + customer.Email + "\",");

				var clientName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
				var clientLastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");

				//_logger.InsertLog(LogLevel.Information, "UpdateCustomerNameOnLightSpeed :: POSTDATA :: LSCustomer :: " + checkCmsCustomer.CmsCustomerId,postData.ToString());

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
				}
			}
			catch (Exception exception)
			{
				if (exception.Message == "The remote server returned an error: (404) Not Found.")
				{
					var nopCustomer = _customerService.GetCustomerById(checkCmsCustomer.NopCustomerId);
					if (nopCustomer != null)
					{
						nopCustomer.Active = false;
						_customerService.UpdateCustomer(nopCustomer);
						checkCmsCustomer.Deleted = true;
						_cmsNopCustomerMappingService.Update(checkCmsCustomer);	
					}
				}																					

				_logger.InsertLog(LogLevel.Error, "Error updating customer name on Light Speed CMS :: LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message + "  ::  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Error updating customer name on Light Speed CMS :: LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message, exception.StackTrace);
				return;
			}
		}

	}
}
