﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Logging;
using Nop.Services.Tasks;

namespace Nop.Plugin.PostXCustom.Tasks
{
	public class SyncCmsCustomers : ITask
	{
		private readonly ICustomerService _customerService;
		private readonly ILogger _logger;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ICustomerRegistrationService _customerRegistrationService;
		private readonly CustomerSettings _customerSettings;
		private readonly IWorkContext _workContext;
		private readonly IStoreContext _storeContext;
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;


		public SyncCmsCustomers(ICustomerService customerService, ILogger logger, IGenericAttributeService genericAttributeService,
				ICustomerRegistrationService customerRegistrationService, CustomerSettings customerSettings,
				IWorkContext workContext, IStoreContext storeContext, ICmsNopCustomerMappingService cmsNopCustomerMappingService)
		{
			_customerService = customerService;
			_logger = logger;
			_genericAttributeService = genericAttributeService;
			_customerRegistrationService = customerRegistrationService;
			_customerSettings = customerSettings;
			_workContext = workContext;
			_storeContext = storeContext;
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
		}

		public void Execute()
		{
			var url = @"https://euc1.posios.com/PosServer/rest/core/customer";

			var token = GetToken();

			var request = (HttpWebRequest)WebRequest.Create(url);
			request.Headers.Add("X-Auth-Token", token);
			request.AutomaticDecompression = DecompressionMethods.GZip;

			using (var response = (HttpWebResponse)request.GetResponse())
			using (var stream = response.GetResponseStream())
			{
				if (stream != null)
				{
					using (var reader = new StreamReader(stream))
					{
						string html = reader.ReadToEnd();

						var jsonText = JObject.Parse(html);

						var resultData = jsonText.GetValue("results"); //.Value<results>();
						var jsonArray = JArray.Parse(resultData.ToString());
						foreach (var item in jsonArray)
						{
							var jsonArrayItem = JsonConvert.DeserializeObject<results>(item.ToString());
							//Check if customer exist
							var checkExistingCustomer = _cmsNopCustomerMappingService.GetByCmsCustomerId(jsonArrayItem.id);
							var checkNopCustomer = _customerService.GetCustomerByEmail(jsonArrayItem.email);


							if (checkExistingCustomer == null && checkNopCustomer == null)
							{
								var newCustomer = new Customer
								{
									Username = jsonArrayItem.email,
									Email = jsonArrayItem.email,
									Active = true,
									Deleted = false,
									CreatedOnUtc = DateTime.UtcNow,
									CustomerGuid = Guid.NewGuid(),
									LastActivityDateUtc = DateTime.UtcNow,
									RegisteredInStoreId = _storeContext.CurrentStore.Id
								};
								_customerService.InsertCustomer(newCustomer);

								//form fields
								_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.FirstName,
									jsonArrayItem.firstName);
								_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.LastName,
									jsonArrayItem.lastName);
								if (_customerSettings.DateOfBirthEnabled)
									_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.DateOfBirth,
										jsonArrayItem.dateOfBirth);
								if (_customerSettings.CompanyEnabled)
									_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.Company,
										jsonArrayItem.companyId);
								if (_customerSettings.StreetAddressEnabled)
									_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.StreetAddress,
										jsonArrayItem.street);
								if (_customerSettings.StreetAddress2Enabled)
									_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.StreetAddress2,
										jsonArrayItem.streetNumber);
								if (_customerSettings.ZipPostalCodeEnabled)
									_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.ZipPostalCode,
										jsonArrayItem.zip);
								if (_customerSettings.CityEnabled)
									_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.City, jsonArrayItem.city);
								if (_customerSettings.CountryEnabled)
									_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.CountryId,
										jsonArrayItem.country);
								if (_customerSettings.PhoneEnabled)
									_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.Phone, jsonArrayItem.telephone);
								if (_customerSettings.FaxEnabled)
									_genericAttributeService.SaveAttribute(newCustomer, SystemCustomerAttributeNames.VatNumber,
										jsonArrayItem.vatNumber);
								_genericAttributeService.SaveAttribute(newCustomer, "UUID", jsonArrayItem.uuid);
								_genericAttributeService.SaveAttribute(newCustomer, "Credits", jsonArrayItem.credits);


								//password
								var changePassRequest = new ChangePasswordRequest(jsonArrayItem.email, false,
									_customerSettings.DefaultPasswordFormat, "123456");
								var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
								if (!changePassResult.Success)
								{
									//foreach (var changePassError in changePassResult.Errors)
									//	return Json(new { success = false, message = changePassError }, JsonRequestBehavior.AllowGet);
								}

								//customer roles
								var customerRole =
									_customerService.GetAllCustomerRoles(true).FirstOrDefault(cr => cr.SystemName.ToLower() == "registered");
								newCustomer.CustomerRoles.Add(customerRole);
								_customerService.UpdateCustomer(newCustomer);


								var cmsNopCustomerMapping = new CmsCustomerNopCustomerMapping
								{
									CmsCustomerId = jsonArrayItem.id,
									NopCustomerId = newCustomer.Id,
									CreatedOnUtc = DateTime.UtcNow,
									UpdatedOnUtc = DateTime.UtcNow
								};
								_cmsNopCustomerMappingService.Insert(cmsNopCustomerMapping);

								continue;
							}

							if (checkExistingCustomer == null)
							{
								var cmsNopCustomerMapping = new CmsCustomerNopCustomerMapping
								{
									CmsCustomerId = jsonArrayItem.id,
									NopCustomerId = checkNopCustomer.Id,
									CreatedOnUtc = DateTime.UtcNow,
									UpdatedOnUtc = DateTime.UtcNow
								};
								_cmsNopCustomerMappingService.Insert(cmsNopCustomerMapping);
							}


							if (checkNopCustomer != null)
							{
								//form fields
								_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.FirstName,
									jsonArrayItem.firstName);
								_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.LastName,
									jsonArrayItem.lastName);
								if (_customerSettings.DateOfBirthEnabled)
									_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.DateOfBirth,
										jsonArrayItem.dateOfBirth);
								if (_customerSettings.CompanyEnabled)
									_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.Company,
										jsonArrayItem.companyId);
								if (_customerSettings.StreetAddressEnabled)
									_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.StreetAddress,
										jsonArrayItem.street);
								if (_customerSettings.StreetAddress2Enabled)
									_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.StreetAddress2,
										jsonArrayItem.streetNumber);
								if (_customerSettings.ZipPostalCodeEnabled)
									_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.ZipPostalCode,
										jsonArrayItem.zip);
								if (_customerSettings.CityEnabled)
									_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.City, jsonArrayItem.city);
								if (_customerSettings.CountryEnabled)
									_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.CountryId,
										jsonArrayItem.country);
								if (_customerSettings.PhoneEnabled)
									_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.Phone,
										jsonArrayItem.telephone);
								if (_customerSettings.FaxEnabled)
									_genericAttributeService.SaveAttribute(checkNopCustomer, SystemCustomerAttributeNames.VatNumber,
										jsonArrayItem.vatNumber);
								_genericAttributeService.SaveAttribute(checkNopCustomer, "UUID", jsonArrayItem.uuid);
								_genericAttributeService.SaveAttribute(checkNopCustomer, "Credits", jsonArrayItem.credits);
							}

						}
					}
				}
			}
		}

		public string GetToken()
		{ 
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			var token = json.GetValue("token").Value<string>();
			return token;
		}
	}
}
