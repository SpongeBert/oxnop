﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.CustomService;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Stores;

namespace Nop.Plugin.PostXCustom.Tasks
{
	public class LightSpeedOrderPushRetryTask //: ITask
	{

		#region Fields
		private readonly IWorkContext _workContext;
		private readonly IStoreService _storeService;
		private readonly ISettingService _settingService;
		private readonly IOrderService _orderService;
		private readonly IOrderProcessingService _orderProcessingService;
		private readonly ILocalizationService _localizationService;
		private readonly ILogger _logger;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;
		private readonly ICustomCodeService _customCodeService;
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly OrderSettings _orderSettings;
		private readonly LocalizationSettings _localizationSettings;
		private readonly IPdfService _pdfService;
		private readonly IRewardPointService _rewardPointService;
		private readonly IStoreContext _storeContext;
		#endregion


		#region Ctor
		public LightSpeedOrderPushRetryTask(IWorkContext workContext, IStoreService storeService, ISettingService settingService,
			IOrderService orderService, IOrderProcessingService orderProcessingService, ILocalizationService localizationService, ILogger logger,
			IGenericAttributeService genericAttributeService, ICmsNopCustomerMappingService cmsNopCustomerMappingService,
			ICmsNopProductMappingService cmsNopProductMappingService, ICmsNopOrderMappingService cmsNopOrderMappingService,
			ICustomCodeService customCodeService, IWorkflowMessageService workflowMessageService, OrderSettings orderSettings,
			LocalizationSettings localiationSettings, IPdfService pdfService, IRewardPointService rewardPointService, IStoreContext storeContext)
		{
			_workContext = workContext;
			_storeService = storeService;
			_settingService = settingService;
			_orderService = orderService;
			_orderProcessingService = orderProcessingService;
			_localizationService = localizationService;
			_logger = logger;
			_genericAttributeService = genericAttributeService;
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_cmsNopProductMappingService = cmsNopProductMappingService;

			_cmsNopOrderMappingService = cmsNopOrderMappingService;
			_customCodeService = customCodeService;
			_workflowMessageService = workflowMessageService;
			_orderSettings = orderSettings;
			_localizationSettings = localiationSettings;
			_pdfService = pdfService;
			_rewardPointService = rewardPointService;
			_storeContext = storeContext;
		}
		#endregion


		public void Execute()
		{
			try
			{

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error Importing Products from LS CMS.", exception.Message + "  ||  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Error Importing Products from LS CMS.", exception.Message, exception.StackTrace);
				_settingService.SetSetting("cmsproductimportflag", "false");
			}
		}


		public object LSPush(int orderId)
		{
			#region Push Order to CMS
			try
			{
				var order = _orderService.GetOrderById(orderId);
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Order not found in webshop :: " + orderId,
						 "Order push to cms :: Error :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId, "Order push to cms :: Error :: Order not found in webshop :: " + orderId, string.Empty);

					return "";
				}

				if (order.PaymentStatus != PaymentStatus.Paid)
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, "Current payment status :: " + order.PaymentStatus);

					return "";
				}


				var cmsNopOrder = _cmsNopOrderMappingService.GetByNopOrderId(order.Id);
				if (cmsNopOrder != null)
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id + " :: LS Order Id :: " + cmsNopOrder.CmsOrderId,
						 "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId + " :: LS Order Id :: " + cmsNopOrder.CmsOrderId,
						 "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id, string.Empty);
					return "";
				}

				var checkCmsCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
				if (checkCmsCustomer == null)
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, string.Empty);
					return "";
				}

				string token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, string.Empty);
					return "";
				}

				string url = @"https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + checkCmsCustomer.CmsCustomerId + "/order";
				var postData = new StringBuilder();
				postData.Append("{\"customerId\":" + checkCmsCustomer.CmsCustomerId + ",");

				var currentData = DateTime.Now;
				string newDate = currentData.Year + "-" + currentData.Month + "-" + currentData.Day + "T" + currentData.Hour + ":" +
									  currentData.Minute + ":" + currentData.Second + "+00:00";
				postData.Append("\"deliveryDate\":\"" + newDate + "\",");

				var orderType = GetOrderType(order);
				postData.Append("\"type\":\"" + orderType + "\",");
				postData.Append("\"status\":\"" + "ACCEPTED" + "\",");
				postData.Append("\"description\":\"" + "order" + "\",");


				var deliveryTime = string.Empty;
				var deliveryTimeDescripton = order.CheckoutAttributeDescription.Substring(order.CheckoutAttributeDescription.LastIndexOf("Gev"));

				if (!string.IsNullOrEmpty(deliveryTimeDescripton))
				{
					deliveryTime = deliveryTimeDescripton;
				}

				postData.Append("\"note\":\"" + "Order Reference: " + order.Id + " || " + deliveryTime + "\",");



				postData.Append("\"orderItems\":[");
				var totalItems = order.OrderItems.Count;
				var counter = 0;
				foreach (var orderItem in order.OrderItems)
				{
					counter++;
					var cmsProduct = _cmsNopProductMappingService.GetByNopProductId(orderItem.ProductId);
					if (cmsProduct == null)
					{
						_logger.InsertLog(LogLevel.Error, "Error :: Manual Order sync with CMS :: Nop Order Id :: " + orderItem.OrderId,
							 "Could not find mapped CMS product against Nop Product :: ID :: " + orderItem.ProductId + " :: Product Name :: " +
							 orderItem.Product.Name, _workContext.CurrentCustomer);
						break;
					}

					postData.Append("{\"productId\":" + cmsProduct.CmsProductId + ",");
					postData.Append("\"amount\":" + orderItem.Quantity + ",");
					postData.Append("\"totalPrice\":" + orderItem.PriceInclTax + ",");
					postData.Append("\"totalPriceWithoutVat\":" + orderItem.PriceExclTax + ",");
					postData.Append("\"unitPrice\":" + orderItem.UnitPriceInclTax + ",");
					postData.Append("\"unitPriceWithoutVat\":" + orderItem.UnitPriceExclTax);
					postData.Append(counter >= totalItems ? "}" : "},");
				}


				if(order.PaymentMethodSystemName.ToLower()== "payments.worldline") {
					var paymentTypeId = 0;
					var paymentTypeTypeId = 0;
					var paymentOption = string.Empty;

					GetLSPaymentInformationFromLog(order.Id, ref paymentTypeId, ref paymentTypeTypeId, ref paymentOption);

					if (paymentTypeId == 0 || paymentTypeTypeId == 0)
					{
						_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId,
							 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, _workContext.CurrentCustomer);

						_customCodeService.SendErrorEmail("Error creating Manual order on CMS for nop order ::  " + orderId,
							 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, string.Empty);
						return "";
					}

					postData.Append("],\"orderPayments\":[{");
					postData.Append("\"amount\":" + order.OrderTotal + ",");
					postData.Append("\"paymentTypeId\":" + paymentTypeId + ",");
					postData.Append("\"paymentTypeTypeId\":" + paymentTypeTypeId + "}");

					if (order.RedeemedRewardPointsEntry != null)
					{
						//Use Cash
						postData.Append(",{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
						postData.Append("\"paymentTypeId\":" + 36191 + ",");
						//postData.Append("\"paymentTypeTypeId\":" + 1 + "}");
						postData.Append("\"paymentTypeTypeId\":" + 200 + "}");
					}
				}

				if (order.PaymentMethodSystemName.ToLower() == "payments.loyaltypoints") {									

					postData.Append("],\"orderPayments\":[");					

					if (order.RedeemedRewardPointsEntry != null)
					{
						//Use Cash
						postData.Append("{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
						postData.Append("\"paymentTypeId\":" + 36191 + ",");
						//postData.Append("\"paymentTypeTypeId\":" + 1 + "}");
						postData.Append("\"paymentTypeTypeId\":" + 200 + "}");
					}
				}


				


				postData.Append("],\"orderTaxInfo\":[{");
				postData.Append("\"tax\":" + "0" + ",");
				postData.Append("\"taxRate\":" + "0" + ",");
				postData.Append("\"totalWithTax\":" + order.OrderTotal + ",");
				postData.Append("\"totalWithoutTax\":" + order.OrderTotal);
				postData.Append("}]}");

				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Order#: " + order.Id, postData.ToString(), _workContext.CurrentCustomer);

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					if (orderType.ToLower() == "delivery")
					{
						UpdateCustomerAddressOnLightSpeed(order, checkCmsCustomer);
					}
					else
					{
						//Update customer name with delivery Info on LS
						UpdateCustomerNameOnLightSpeed(Convert.ToInt32(responseData), checkCmsCustomer, order.Customer, order);
					}

					

					var cmsOrderNopOrderMap = new CmsOrderNopOrderMapping
					{
						NopOrderId = order.Id,
						CmsOrderId = Convert.ToInt32(responseData),
						CreatedOnUtc = DateTime.UtcNow,
						UpdatedOnUtc = DateTime.UtcNow
					};

					_cmsNopOrderMappingService.Insert(cmsOrderNopOrderMap);
					order.CustomOrderNumber = responseData;
					_orderService.UpdateOrder(order);

					//notifications//Custom Code // Notification to be send after payment success
					SendNotificationsAndSaveNotesManual(orderId);

				}

			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Error Creating Order on CMS for nop order :: " + orderId,
					 ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId, ex.Message, ex.StackTrace);

				return "";
			}
			return "";
			#endregion
		}


		public void GetLSPaymentInformationFromLog(int orderId, ref int paymentTypeId, ref int paymentTypeTypeId, ref string paymentOption)
		{
			var logInfo = _logger.GetAllLogs().FirstOrDefault(x => x.ShortMessage.Contains("orderId=" + orderId));

			if (logInfo != null)
			{
				if (logInfo.ShortMessage.ToLower().Contains("visa"))
				{
					paymentTypeId = 40580;
					paymentTypeTypeId = 801;
					paymentOption = "Visa";
				}
				else if (logInfo.ShortMessage.ToLower().Contains("mastercard"))
				{

					paymentTypeId = 39893;
					paymentTypeTypeId = 801;
					paymentOption = "MasterCard";
				}
				else if (logInfo.ShortMessage.ToLower().Contains("bcmc"))
				{

					paymentTypeId = 36192;
					paymentTypeTypeId = 801;
					paymentOption = "bcmc";
				}
				else if (logInfo.ShortMessage.ToLower().Contains("maestro"))
				{

					paymentTypeId = 40976;
					paymentTypeTypeId = 801;
					paymentOption = "maestro";
				}
				else if (logInfo.ShortMessage.ToLower().Contains("vpay"))
				{

					paymentTypeId = 40977;
					paymentTypeTypeId = 801;
					paymentOption = "vpay";
				}
			}
		}


		protected void SendNotificationsAndSaveNotesManual(int orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(orderId);
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId,
						"Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order ::  " + orderId, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				//send email notifications
				var orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
				if (orderPlacedStoreOwnerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to store owner) has been queued. Queued email identifier: {0}.", orderPlacedStoreOwnerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}

				var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 _pdfService.PrintOrderToPdf(order) : null;
				var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 "order.pdf" : null;
				var orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService
					 .SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
				if (orderPlacedCustomerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: " + orderId,
					ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);
				_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order :: " + orderId, ex.Message, ex.StackTrace);
			}
		}



		public string GetToken()
		{
			try
			{
				string token;
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		public string GetOrderType(Core.Domain.Orders.Order order)
		{
			if (order == null)
				return "takeaway";

			if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
			{
				if (order.CheckoutAttributeDescription.ToLower().Contains("levering"))
					return "delivery";
			}
			return "takeaway";
		}

		public void UpdateCustomerAddressOnLightSpeed(Core.Domain.Orders.Order order, CmsCustomerNopCustomerMapping checkCmsCustomer)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, string.Empty);
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"deliveryCity\":\"" + order.ShippingAddress.City + "\",");
				postData.Append("\"deliveryCountry\":\"" + order.ShippingAddress.Country.TwoLetterIsoCode + "\",");
				postData.Append("\"deliveryStreet\":\"" + order.ShippingAddress.Address1 + "\",");
				postData.Append("\"deliveryStreetNumber\":\"" + order.ShippingAddress.Address2 + "\",");
				postData.Append("\"deliveryZip\":\"" + order.ShippingAddress.ZipPostalCode + "\",");
				postData.Append("\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + order.Customer.Email + "\",");

				//var loyaltyPoints = "0 reward points available.";
				//var customerRewardPointsBalance = _rewardPointService.GetRewardPointsBalance(order.Customer.Id, _storeContext.CurrentStore.Id);
				//if (customerRewardPointsBalance > 0)
				//	loyaltyPoints = customerRewardPointsBalance + " reward points available.";

				//postData.Append("\"firstName\":\"" + loyaltyPoints + "\",");
				//postData.Append("\"lastName\":\"" + order.Customer.GetFullName() + "\"");
				//postData.Append("}");

				var clientName = order.Customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.FirstName);
				var clientLastName = order.Customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
				{
					//clientName = order.CheckoutAttributeDescription;
					var dataArray = order.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1].Trim() + " | " + dataArray[3].Trim();
					clientLastName = order.Customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);

				}
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating customer address on LS for order id #" + order.Id, exception.Message + "  ::  " + exception.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Error updating customer address on LS for order id #" + order.Id, exception.Message, exception.StackTrace);
				return;
			}
		}

		protected void SendNotificationsAndSaveNotes(long orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(Convert.ToInt32(orderId));
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId,
						"Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order ::  " + orderId, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				//send email notifications
				var orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
				if (orderPlacedStoreOwnerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to store owner) has been queued. Queued email identifier: {0}.", orderPlacedStoreOwnerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}

				var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 _pdfService.PrintOrderToPdf(order) : null;
				var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 "order.pdf" : null;
				var orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService
					 .SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
				if (orderPlacedCustomerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: " + orderId,
					ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);
				_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order :: " + orderId, ex.Message, ex.StackTrace);

				return;
			}
		}

		public void UpdateCustomerNameOnLightSpeed(int cmsOrderId, CmsCustomerNopCustomerMapping checkCmsCustomer, Nop.Core.Domain.Customers.Customer customer, Order nopOrder)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerNameOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + customer.Email + "\",");

				var clientName = customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.FirstName);
				var clientLastName = customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(nopOrder.CheckoutAttributeDescription))
				{
					//clientName = nopOrder.CheckoutAttributeDescription;
					var dataArray = nopOrder.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1].Trim() + " | " + dataArray[3].Trim();
					clientLastName = customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
				}

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating Name for customer on Light Speed CMS :: LS OrderId :: " + cmsOrderId + " || Nop Order Id :: " + nopOrder.Id + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message + "  ::  " + exception.StackTrace);
			}
		}



	}
}
