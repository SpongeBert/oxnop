﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Logging;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.CustomService;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Tasks;

namespace Nop.Plugin.PostXCustom.Tasks
{
	public class UpdateLoyaltyPointsUsingReceiptTask : ITask
	{
		#region Fields

		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;
		private readonly ICustomCodeService _customCodeService;
		private readonly IRewardPointService _rewardPointService;
		private readonly IStoreContext _storeContext;
		private readonly ISettingService _settingService;
		private readonly ILogger _logger;
		private readonly ILightSpeedOfflineOrdersService _lightSpeedOfflineOrdersService;
		private readonly IOrderTotalCalculationService _orderTotalCalculationService;
		private readonly ICustomerService _customerService;
		private readonly IWebHelper _webHelper;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ILightSpeedReceiptsService _lightSpeedReceiptService;
		#endregion

		#region Ctor
		public UpdateLoyaltyPointsUsingReceiptTask(ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICmsNopOrderMappingService cmsNopOrderMappingService,
			ICustomCodeService customCodeService, IRewardPointService rewardPointService, IStoreContext storeContext,
			ISettingService settingService, ILogger logger, ILightSpeedOfflineOrdersService lightSpeedOfflineOrdersService,
			IOrderTotalCalculationService orderTotalCalculationService, ICustomerService customerService,
			IWebHelper webHelper, IGenericAttributeService genericAttributeService, ILightSpeedReceiptsService lightSpeedReceiptService)
		{
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_cmsNopOrderMappingService = cmsNopOrderMappingService;
			_customCodeService = customCodeService;
			_rewardPointService = rewardPointService;
			_storeContext = storeContext;
			_settingService = settingService;
			_logger = logger;
			_lightSpeedOfflineOrdersService = lightSpeedOfflineOrdersService;
			_orderTotalCalculationService = orderTotalCalculationService;
			_customerService = customerService;
			_webHelper = webHelper;
			_genericAttributeService = genericAttributeService;
			_lightSpeedReceiptService = lightSpeedReceiptService;
		}
		#endregion


		public void Execute()
		{
			try
			{
				if (CheckImportTime())
				{
					_customCodeService.SendInformationEmail("Import Receipts Task started. Date :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture), "The import receipts task started.");
					var offsetValue = 0;
					var orderDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");

					for (int count = 0; count < int.MaxValue; count++)
					{
						string html;
						string url = @"https://euc1.posios.com/PosServer/rest/financial/receipt?";
						url = url + "date=" + orderDate;

						if (offsetValue > 0)
							url = url + "&offset=" + offsetValue;

						var token = GetToken();
						if (string.IsNullOrEmpty(token))
						{
							_logger.InsertLog(LogLevel.Error,"Error processing loyalty :: failed to generate security token.");
							continue;//Apply a retry Code here
						}																																	

						HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
						request.Headers.Add("X-Auth-Token", token);
						request.AutomaticDecompression = DecompressionMethods.GZip;

						using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
						using (Stream stream = response.GetResponseStream())
						using (StreamReader reader = new StreamReader(stream))
						{
							html = reader.ReadToEnd();
							var jsonList = JsonConvert.DeserializeObject<CmsReceiptsListModel>(html);
							if (!jsonList.results.Any()) { break; }

							offsetValue = offsetValue + 50;

							foreach (var item in jsonList.results)
							{
								var checkReceiptMapping = _lightSpeedReceiptService.GetByLightSpeedReceiptId(item.id);
								if (checkReceiptMapping != null)
									continue;

								var reportItem = new LightSpeedReceiptsMapping
								{
									ImportedOnDate = DateTime.UtcNow,
									LightSpeedCustomerId = item.customerId,
									ReceiptCreateDate = Convert.ToDateTime(item.creationDate),
									OrderTotal = item.total,
									ReceiptId = item.id,
									LoyaltyPoints = 0

								};

								var cashPayment = 0M;
								var nonCashPayment = 0M;

								foreach (var paymentItem in item.payments)
								{
									if (paymentItem.paymentTypeId == 36191)
									{
										cashPayment = cashPayment + paymentItem.amount;
									}
									else
									{
										nonCashPayment = nonCashPayment + paymentItem.amount;
									}
								}

								reportItem.OrderTotalCash = cashPayment;
								reportItem.OrderTotalNonCash = nonCashPayment;

								bool loyaltyProcessed = false;
								var cmsNopCustomer = _cmsNopCustomerMappingService.GetByCmsCustomerId(item.customerId);
								if (cmsNopCustomer != null)
								{
									reportItem.NopCustomerId = cmsNopCustomer.NopCustomerId;
									reportItem.LoyaltyPoints = CalculcateLoyaltyPoints(nonCashPayment, cmsNopCustomer.NopCustomerId, item.id, cmsNopCustomer, ref loyaltyProcessed);
									if (loyaltyProcessed)
										reportItem.LoyaltyProcessedOnDate = DateTime.UtcNow;
								}
								else
								{
									loyaltyProcessed = true;
									reportItem.LoyaltyProcessedOnDate = DateTime.UtcNow;
								}

								reportItem.LoyaltyProcessed = loyaltyProcessed;
								_lightSpeedReceiptService.Insert(reportItem);

							}
						}
					}
					_customCodeService.SendInformationEmail("Import Receipts Task End. Date :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture), "The import receipts task ended.");
				}
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error Importing Loyalty Data from LS CMS.", exception.Message + "  ||  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Error Importing Loyalty Data from LS CMS.", exception.Message, exception.StackTrace);
				_settingService.SetSetting("cmsloyaltyimportflag", "false");
			}
		}


		public bool CheckImportTime()
		{
			var manualImportSetting = _settingService.GetSetting("manualImportLoyalty");
			if (manualImportSetting != null)
			{
				if (manualImportSetting.Value.ToLower() == "true")
				{
					return true;
				}
			}

			var checkSetting = _settingService.GetSetting("cmsloyaltyimportflag");
			if (DateTime.Now.Hour == 1)
			{
				if (checkSetting == null)
				{
					_settingService.SetSetting("cmsloyaltyimportflag", "true");
					return true;
				}

				if (checkSetting.Value == "true")
				{
					return false;
				}

				_settingService.SetSetting("cmsloyaltyimportflag", "true");
				return true;
			}
			if (checkSetting == null)
			{
				_settingService.SetSetting("cmsloyaltyimportflag", "false");
			}
			else if (checkSetting.Value == "true")
			{
				_settingService.SetSetting("cmsloyaltyimportflag", "false");
			}
			return false;
		}


		public string GetToken()
		{
			try
			{
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";
				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				var token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Import Loyalty Data from LS CMS :: Error :: Failed to generate Token.", exception.Message + "  ||  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Import Loyalty Data from LS CMS :: Error :: Failed to generate Token.", exception.Message, exception.StackTrace);
				return string.Empty;
			}

		}

		public void UpdateCustomerLoyaltyPointsOnLightSpeed(int cmsOrderId, CmsCustomerNopCustomerMapping checkCmsCustomer, Customer customer, ref bool loyaltyProcessed)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerLoyaltyPointsOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + customer.Email + "\",");

				var loyaltyPoints = "0 reward points available.";
				var customerRewardPointsBalance = _rewardPointService.GetRewardPointsBalance(customer.Id, _storeContext.CurrentStore.Id);
				if (customerRewardPointsBalance > 0)
					loyaltyPoints = customerRewardPointsBalance + " reward points available.";

				postData.Append("\"firstName\":\"" + loyaltyPoints + "\",");
				postData.Append("\"lastName\":\"" + customer.GetFullName() + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
					loyaltyProcessed = true;
				}

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating loyalty points for customer on Light Speed CMS :: LS ReceiptId :: " + cmsOrderId + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message + "  ::  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Error updating loyalty points for customer on Light Speed CMS :: LS ReceiptId :: " + cmsOrderId + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message, exception.StackTrace);
				loyaltyProcessed = false;
			}
		}



		public int CalculcateLoyaltyPoints(decimal orderTotal, int nopCustomerId, int receiptId, CmsCustomerNopCustomerMapping checkCmsCustomer, ref bool loyaltyProcessed)
		{
			int loyaltyPoints = 0;
			var nopCustomer = _customerService.GetCustomerById(nopCustomerId);
			if (nopCustomer != null)
			{
				loyaltyPoints = _orderTotalCalculationService.CalculateRewardPoints(nopCustomer, orderTotal);

				if (loyaltyPoints > 0)
				{
					UpdateCustomerLoyaltyPointsOnLightSpeed(receiptId, checkCmsCustomer, nopCustomer, ref loyaltyProcessed);//Updates the loyalty on LS
					if (loyaltyProcessed)
						_rewardPointService.AddRewardPointsHistoryEntry(nopCustomer, loyaltyPoints, _storeContext.CurrentStore.Id, "Loyalty Points for LS ReceiptId #" + receiptId);
				}

			}
			return loyaltyPoints;
		}

	}
}
