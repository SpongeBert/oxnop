﻿using System;
using System.Globalization;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Services.Localization;
using Nop.Services.Orders;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;
using System.IO;
using Nop.Plugin.PostXCustom.Services;
using Newtonsoft.Json;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Services.Customers;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Services.Logging;
using Nop.Core.Domain.Logging;
using Nop.Services.Tasks;

namespace Nop.Plugin.PostXCustom.Tasks
{
	public class UpdateOrderStatusTask : ITask
	{
		#region Fields	  
		private readonly IOrderService _orderService;
		private readonly IWorkContext _workContext;
		private readonly ILocalizationService _localizationService;
		private readonly IStoreContext _storeContext;
		private readonly IOrderTotalCalculationService _orderTotalCalculationService;
		private readonly IRewardPointService _rewardPointService;
		private readonly RewardPointsSettings _rewardPointsSettings;
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;
		private readonly ILogger _logger;
		#endregion

		#region Ctor
		public UpdateOrderStatusTask(IOrderService orderService,IWorkContext workContext,ILocalizationService localizationService,
				 IStoreContext storeContext,IOrderTotalCalculationService orderTotalCalculationService,IRewardPointService rewardPointService,
				 RewardPointsSettings rewardPointsSettings, ICmsNopCustomerMappingService cmsNopCustomerMappingService,
				ICmsNopOrderMappingService cmsNopOrderMappingService, ILogger logger)
		{
			_orderService = orderService;
			_workContext = workContext;
			_localizationService = localizationService;
			_storeContext = storeContext;
			_orderTotalCalculationService = orderTotalCalculationService;
			_rewardPointService = rewardPointService;
			_rewardPointsSettings = rewardPointsSettings;
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_cmsNopOrderMappingService = cmsNopOrderMappingService;
			_logger = logger;
		}
		#endregion
				 

		public void Execute()
		{
			var orders = _orderService.GetInCompleteOrders();
			if (orders.Any())
			{
				for (var i = 0; i < orders.Count; i++)
				{
					var record = orders[i];
					UpdateOrderStatusSingleOrder(record);
				}
			}
		}

		#region Custom Methods

		public bool UpdateOrderStatusSingleOrder(Order order)
		{
			try
			{
				if (order.OrderStatus == OrderStatus.Processing || order.OrderStatus == OrderStatus.Pending)
				{
					var cmsNopOrder = _cmsNopOrderMappingService.GetByNopOrderId(order.Id);
					if (cmsNopOrder == null)
						return false;

					var cmsNopCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
					if (cmsNopCustomer == null)
						return false;

					var url = "https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + cmsNopCustomer.CmsCustomerId + "/order/" + cmsNopOrder.CmsOrderId;
					var token = GetToken();
					if (string.IsNullOrEmpty(token))
					{
						_logger.InsertLog(LogLevel.Error, "Error :: UpdateOrderStatusSingleOrder :: Failed to get token from CMS");
						return false;
					}

					var request = (HttpWebRequest)WebRequest.Create(url);
					request.Headers.Add("X-Auth-Token", token);
					request.Headers.Add("customerId", cmsNopCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
					request.Headers.Add("orderId", cmsNopOrder.CmsOrderId.ToString(CultureInfo.InvariantCulture));
					request.ContentType = "application/json";
					request.Method = "GET";
					request.AutomaticDecompression = DecompressionMethods.GZip;

					var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
					var responseData = responseReader.ReadToEnd();
					if (!string.IsNullOrEmpty(responseData))
					{
						var jsonText = JsonConvert.DeserializeObject<CmsOrderApiModel>(responseData);						
						if (jsonText.status.ToLower() == "processed") //PROCESSED
						{
							order.OrderStatus = OrderStatus.Complete;
							_orderService.UpdateOrder(order);

							//add a note
							order.OrderNotes.Add(new OrderNote
							{
								Note = string.Format("Order status has been edited. New status: {0}", order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext)),
								DisplayToCustomer = false,
								CreatedOnUtc = DateTime.UtcNow
							});
							_orderService.UpdateOrder(order);

							_logger.InsertLog(LogLevel.Information, "LP#1 :: ODR :: " + order.CustomOrderNumber);
							AwardRewardPoints(order);
							//UpdateCustomerLoyaltyPointsOnLightSpeed(order, cmsNopCustomer);
							return true;
						}
					}
				}
				return false;

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error,"Error updating order status :: OrderId :: " + order.Id,exception.Message + "  ::  " + exception.StackTrace);
				return false;
			}
		}
					  	
					  
		public string GetToken()
		{
			try
			{
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				var token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error :: Failed to get token from CMS",exception.Message + "  :: " +	exception.StackTrace);
				return "";
			}
		}



		protected virtual void AwardRewardPoints(Order order)
		{
			
			var totalForRewardPoints = _orderTotalCalculationService.CalculateApplicableOrderTotalForRewardPoints(order.OrderShippingInclTax, order.OrderTotal);
			
			int points = _orderTotalCalculationService.CalculateRewardPoints(order.Customer, totalForRewardPoints);
			
			if (points == 0)
				return;
			
			//Ensure that reward points were not added (earned) before. We should not add reward points if they were already earned for this order
			if (order.RewardPointsHistoryEntryId.HasValue && order.RewardPointsHistoryEntryId.Value > 0)
				return;
			

			//check whether delay is set
			DateTime? activatingDate = null;
			if (_rewardPointsSettings.ActivationDelay > 0)
			{
				var delayPeriod = (RewardPointsActivatingDelayPeriod)_rewardPointsSettings.ActivationDelayPeriodId;
				var delayInHours = delayPeriod.ToHours(_rewardPointsSettings.ActivationDelay);
				activatingDate = DateTime.UtcNow.AddHours(delayInHours);
			}

			//add reward points
			order.RewardPointsHistoryEntryId = _rewardPointService.AddRewardPointsHistoryEntry(order.Customer, points, order.StoreId,
				 string.Format(_localizationService.GetResource("RewardPoints.Message.EarnedForOrder"), order.CustomOrderNumber), activatingDate: activatingDate);

			_orderService.UpdateOrder(order);
		}


		public void UpdateCustomerLoyaltyPointsOnLightSpeed(Order order, CmsCustomerNopCustomerMapping checkCmsCustomer)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerLoyaltyPointsOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();				
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + order.Customer.Email + "\",");

				var loyaltyPoints = "0 reward points available.";
				var customerRewardPointsBalance = _rewardPointService.GetRewardPointsBalance(order.Customer.Id, _storeContext.CurrentStore.Id);
				if (customerRewardPointsBalance > 0)
					loyaltyPoints = customerRewardPointsBalance + " reward points available.";

				postData.Append("\"firstName\":\"" + loyaltyPoints + "\",");
				postData.Append("\"lastName\":\"" + order.Customer.GetFullName() + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
				}																											  																															  

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating loyalty points for customer on Light Speed CMS :: OrderId :: " + order.Id+" :: CustomerId :: " + order.CustomerId, exception.Message + "  ::  " + exception.StackTrace);
			}
		}

		#endregion


	}
}
