﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Catalog;
using Nop.Services.Media;
using Nop.Services.Seo;
using Nop.Services.Tasks;
using Nop.Services.Logging;
using Nop.Core.Domain.Logging;
using Nop.Services.CustomService;
using Nop.Services.Configuration;
using System.Collections.Generic;

namespace Nop.Plugin.PostXCustom.Tasks
{
	public class ImportCmsProducts : ITask
	{
		private readonly IProductService _productService;
		private readonly IPictureService _pictureService;
		private readonly IUrlRecordService _urlRecordService;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly ICategoryService _categoryService;
		private readonly ICmsNopCategoryMappingService _cmsNopCategoryMappingService;
		private readonly ILogger _logger;
		private readonly ICustomCodeService _customCodeService;
		private readonly ISettingService _settingService;
		private readonly IProductAttributeService _productAttributeService;

		public ImportCmsProducts(IProductService productService, IPictureService pictureService, IUrlRecordService urlRecordService, ICmsNopProductMappingService cmsNopProductMappingService, ICategoryService categoryService,
			 ICmsNopCategoryMappingService cmsNopCategoryMappingService, ILogger logger, ICustomCodeService customCodeService, ISettingService settingService, IProductAttributeService productAttributeService)
		{
			_productService = productService;
			_pictureService = pictureService;
			_urlRecordService = urlRecordService;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_categoryService = categoryService;
			_cmsNopCategoryMappingService = cmsNopCategoryMappingService;
			_logger = logger;
			_customCodeService = customCodeService;
			_settingService = settingService;
			_productAttributeService = productAttributeService;
		}

		public void Execute()
		{
			try
			{
				if (CheckImportTime())
				{
					_customCodeService.SendInformationEmail("Import category and product task started. Date :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture), "The Import category and product task started.");

					//Step1: insert or update categories
					ImportCmsCategories();

					//Step2: insert or update products
					var offsetValue = 0;
					for (int count = 0; count < int.MaxValue; count++)
					{
						string html;
						string url = @"https://euc1.posios.com/PosServer/rest/inventory/product";

						if (offsetValue > 0)
							url = @"https://euc1.posios.com/PosServer/rest/inventory/product?offset=" + offsetValue;

						var token = GetToken();

						HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
						request.Headers.Add("X-Auth-Token", token);
						request.AutomaticDecompression = DecompressionMethods.GZip;

						using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
						using (Stream stream = response.GetResponseStream())
						using (StreamReader reader = new StreamReader(stream))
						{
							html = reader.ReadToEnd();
							JArray jsonArray = JArray.Parse(html);

							if (!jsonArray.Any()) { break; }

							offsetValue = offsetValue + 50;

							foreach (var item in jsonArray)
							{
								var jsonText = JsonConvert.DeserializeObject<CmsProductApiModel>(item.ToString());
								var company = string.Empty;
								var companyCode = string.Empty;
								var productName = jsonText.name;
								var sku = jsonText.sku;
								//split String for company & code  
								//string[] splitString = jsonText.name.Split('-');
								string[] splitString = jsonText.sku.Split('-');
								int displayOrder = 0;

								if (splitString.Any() && splitString.Count() > 1)
								{
									try
									{
										if (!string.IsNullOrEmpty(splitString[0]))
										{
											companyCode = splitString[0].Substring(0, 3);
											company = splitString[0].Substring(3, 3);
										}
										//productName = splitString[1];
										if (!String.IsNullOrEmpty(splitString[1]))
										{
											if (splitString[1].Any(c => char.IsDigit(c)))
												displayOrder = GetOrderNo(splitString[1]);
										}
									}
									catch (Exception)
									{
										_logger.InsertLog(LogLevel.Error, "Error Importing Product Company & Code .", jsonText.name);
									}
								}
								else
								{
									if (!String.IsNullOrEmpty(jsonText.sku))
									{
										if (jsonText.sku.Any(c => char.IsDigit(c)))
											displayOrder = GetOrderNo(jsonText.sku);
									}
								}


								//if (company == "009")
								//{
								//	var teste = jsonText.sku;
								//}

								//if (!String.IsNullOrEmpty(jsonText.sku))
								//{ 									
								//	if (jsonText.sku.Any(c => char.IsDigit(c)))
								//		displayOrder = GetOrderNo(jsonText.sku);
								//}

								//Check if product exist
								var checkExistingProduct = _cmsNopProductMappingService.GetByCmsProductId(jsonText.id);
								var pIsPublished = jsonText.visible;

								if (checkExistingProduct == null)
								{
									if (pIsPublished)
									{
										var newProduct = new Product
										{
											Name = productName,//jsonText.name,
											Price = jsonText.price,
											FullDescription = jsonText.info,
											Sku = jsonText.sku,
											Published = jsonText.visible,
											Deleted = false,
											StockQuantity = jsonText.stockAmount,
											//DisplayOrder = displayOrder,	//Commented 14 Sept 2018
											CreatedOnUtc = DateTime.UtcNow,
											UpdatedOnUtc = DateTime.UtcNow,
											ProductType = ProductType.SimpleProduct,
											ProductTemplateId = 1,
											OrderMinimumQuantity = 1,
											OrderMaximumQuantity = (jsonText.stockAmount < 1) ? 10000 : jsonText.stockAmount,
											VisibleIndividually = true,
											IsShipEnabled = true,
										};
										_productService.InsertProduct(newProduct);
										if (jsonText.additions != null)
										{
											foreach (var prodAttr in jsonText.additions)
											{
												var productAttribute = _cmsNopProductMappingService.GetProductAttributeByName(prodAttr.name);
												if (productAttribute == null)
												{
													productAttribute = new ProductAttribute()
													{
														Name = prodAttr.name
													};
													_productAttributeService.InsertProductAttribute(productAttribute);
												}
												var nopCmsProductAttributeMapping = _cmsNopProductMappingService.GetNopCmsProductAttributeMappingByCmsProductAttributeId(prodAttr.id);
												if (nopCmsProductAttributeMapping == null)
												{
													nopCmsProductAttributeMapping = new NopCmsProductAttributeMapping()
													{
														CmsProductAttributeId = prodAttr.id,
														CmsProductAttributeName = productAttribute.Name,
														NopProductAttributeId = productAttribute.Id
													};
													_cmsNopProductMappingService.InsertNopCmsProductAttributeMapping(nopCmsProductAttributeMapping);
												}

												var productAttributeMapping = new ProductAttributeMapping()
												{
													ProductId = newProduct.Id,
													Product = newProduct,
													ProductAttributeId = productAttribute.Id,
													TextPrompt = (prodAttr.displayName != null || prodAttr.displayName != "") ? prodAttr.displayName : prodAttr.name,
													AttributeControlTypeId = prodAttr.values.Count() > 0 ?(int)AttributeControlType.DropdownList : (int)AttributeControlType.TextBox
												};
												_productAttributeService.InsertProductAttributeMapping(productAttributeMapping);

												if (prodAttr.values.Count() > 0)
												{
													foreach (var prodAttributeValue in prodAttr.values)
													{
														var pav = new ProductAttributeValue
														{
															ProductAttributeMappingId = productAttributeMapping.Id,
															AttributeValueTypeId = (int)AttributeValueType.Simple,
															Name = prodAttributeValue.name,
															PriceAdjustment = prodAttributeValue.price
														};
														_productAttributeService.InsertProductAttributeValue(pav);

														var nopCmsPavm = new NopCmsProductAttributeValueMapping
														{
															ProductAttributeValueId = pav.Id,
															NopCmsProductAttributeMappingId = nopCmsProductAttributeMapping.Id,
															Name = prodAttributeValue.name,
															Price = prodAttributeValue.price,
															CmsProductAttributeValueMappingId = prodAttributeValue.id
														};
														_cmsNopProductMappingService.InsertNopCmsProductAttributeValueMapping(nopCmsPavm);
													}
												}
											}
										}
										if (newProduct.Id > 0)
										{
											if (!string.IsNullOrEmpty(jsonText.imageLocation))
											{
												//var productpicture = jsonText.imageLocation;
												var imageBytes = GetRemoteImage(jsonText.imageLocation);

												if (imageBytes != null)
												{
													var picture = _pictureService.InsertPicture(imageBytes, MimeTypes.ImageJpeg, null);

													if (picture != null)
													{
														var productPicture = new ProductPicture
														{
															Picture = picture,
															DisplayOrder = 1,
															PictureId = picture.Id,
															ProductId = newProduct.Id
														};
														newProduct.ProductPictures.Add(productPicture);
														_productService.UpdateProduct(newProduct);
													}
												}
											}
										}
										//search engine name
										var seName = newProduct.ValidateSeName(newProduct.Name.Replace(" ", ""), newProduct.Name, true);
										_urlRecordService.SaveSlug(newProduct, seName, 0);



										//Add Mapping for new product
										var cmsNopProductMap = new CmsProductNopProductMapping
										{
											NopProductId = newProduct.Id,
											CmsProductId = jsonText.id,
											CreatedOnUtc = DateTime.UtcNow,
											UpdatedOnUtc = DateTime.UtcNow,
											Code = companyCode,
											Company = company
										};
										_cmsNopProductMappingService.Insert(cmsNopProductMap);

										InsertUpdateProductCategoryMap(jsonText, newProduct);

										continue;
									}
								}
								else
								{
									if (jsonText.additions != null)
									{
										foreach (var prodAttr in jsonText.additions)
										{
											var productAttribute = _cmsNopProductMappingService.GetProductAttributeByName(prodAttr.name);
											if (productAttribute == null)
											{
												productAttribute = new ProductAttribute()
												{
													Name = prodAttr.name
												};
												_productAttributeService.InsertProductAttribute(productAttribute);
											}

											var nopCmsProductAttributeMapping = _cmsNopProductMappingService.GetNopCmsProductAttributeMappingByCmsProductAttributeId(prodAttr.id);
											if (nopCmsProductAttributeMapping == null)
											{
												nopCmsProductAttributeMapping = new NopCmsProductAttributeMapping()
												{
													CmsProductAttributeId = prodAttr.id,
													CmsProductAttributeName = productAttribute.Name,
													NopProductAttributeId = productAttribute.Id

												};
												_cmsNopProductMappingService.InsertNopCmsProductAttributeMapping(nopCmsProductAttributeMapping);
											}
											var productAttributeMapping = _cmsNopProductMappingService.GetProductAttributeMappingsByProductIdProductAttributeId(checkExistingProduct.NopProductId, productAttribute.Id);
											if (productAttributeMapping != null)
											{
												productAttributeMapping.ProductId = checkExistingProduct.NopProductId;
												productAttributeMapping.ProductAttributeId = productAttribute.Id;
												productAttributeMapping.TextPrompt = (prodAttr.displayName != null || prodAttr.displayName != "") ? prodAttr.displayName : prodAttr.name;
												//productAttributeMapping.AttributeControlTypeId = (int)AttributeControlType.DropdownList;
												productAttributeMapping.AttributeControlTypeId = prodAttr.values.Count() > 0
													? (int) AttributeControlType.DropdownList
													: (int) AttributeControlType.TextBox;
												_productAttributeService.UpdateProductAttributeMapping(productAttributeMapping);
											}
											else
											{
												productAttributeMapping = new ProductAttributeMapping()
												{
													ProductId = checkExistingProduct.NopProductId,
													ProductAttributeId = productAttribute.Id,
													TextPrompt = (prodAttr.displayName != null || prodAttr.displayName != "") ? prodAttr.displayName : prodAttr.name,
													//AttributeControlTypeId = (int)AttributeControlType.DropdownList
													AttributeControlTypeId = prodAttr.values.Count() > 0 ? (int)AttributeControlType.DropdownList : (int)AttributeControlType.TextBox

												};
												_productAttributeService.InsertProductAttributeMapping(productAttributeMapping);
											}
											if (prodAttr.values.Count() > 0)
											{
												var pavList = _productAttributeService.GetProductAttributeValues(productAttributeMapping.Id);
												foreach (var pavv in pavList)
												{
													_cmsNopProductMappingService.DeleteNopCmsProductAttributeValueMapping(pavv.Id);
													_productAttributeService.DeleteProductAttributeValue(pavv);

												}
												foreach (var prodAttributeValue in prodAttr.values)
												{
													var pav = _cmsNopProductMappingService.GetProductAttributeValuesByprodAttrMapIdByprodAttrValName(productAttributeMapping.Id, prodAttributeValue.name);
													if (pav != null)
													{
														pav.ProductAttributeMappingId = productAttributeMapping.Id;
														pav.AttributeValueTypeId = (int)AttributeValueType.Simple;
														pav.Name = prodAttributeValue.name;
														pav.PriceAdjustment = prodAttributeValue.price;
														_productAttributeService.UpdateProductAttributeValue(pav);

														var nopCmsProductAttributeValueMapping = _cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(pav.Id);
														if (nopCmsProductAttributeValueMapping == null)
														{
															nopCmsProductAttributeValueMapping = new NopCmsProductAttributeValueMapping
															{
																ProductAttributeValueId = pav.Id,
																NopCmsProductAttributeMappingId = nopCmsProductAttributeMapping.Id,
																Name = prodAttributeValue.name,
																Price = prodAttributeValue.price,
																CmsProductAttributeValueMappingId = prodAttributeValue.id
															};
															_cmsNopProductMappingService.InsertNopCmsProductAttributeValueMapping(nopCmsProductAttributeValueMapping);
														}
														else
														{
															nopCmsProductAttributeValueMapping.ProductAttributeValueId = pav.Id;
															nopCmsProductAttributeValueMapping.NopCmsProductAttributeMappingId = nopCmsProductAttributeMapping.Id;
															nopCmsProductAttributeValueMapping.Name = prodAttributeValue.name;
															nopCmsProductAttributeValueMapping.Price = prodAttributeValue.price;
															nopCmsProductAttributeValueMapping.CmsProductAttributeValueMappingId = prodAttributeValue.id;
															_cmsNopProductMappingService.UpdateNopCmsProductAttributeValueMapping(nopCmsProductAttributeValueMapping);
														}

													}
													else
													{
														var newPAV = new ProductAttributeValue
														{
															ProductAttributeMappingId = productAttributeMapping.Id,
															AttributeValueTypeId = (int)AttributeValueType.Simple,
															Name = prodAttributeValue.name,
															PriceAdjustment = prodAttributeValue.price
														};
														_productAttributeService.InsertProductAttributeValue(newPAV);

														var nopCmsProductAttributeValueMapping = new NopCmsProductAttributeValueMapping
														{
															ProductAttributeValueId = newPAV.Id,
															NopCmsProductAttributeMappingId = nopCmsProductAttributeMapping.Id,
															Name = prodAttributeValue.name,
															Price = prodAttributeValue.price,
															CmsProductAttributeValueMappingId = prodAttributeValue.id
														};
														_cmsNopProductMappingService.InsertNopCmsProductAttributeValueMapping(nopCmsProductAttributeValueMapping);
													}

												}
											}
										}
									}
								}

								if (checkExistingProduct != null)
								{
									var product = _productService.GetProductById(checkExistingProduct.NopProductId);
									if (product != null)
									{
										if (pIsPublished)
										{
											product.Name = productName;//jsonText.name;
											product.Price = jsonText.price;
											product.FullDescription = jsonText.info;
											product.Sku = jsonText.sku;
											product.Published = jsonText.visible;
											product.StockQuantity = jsonText.stockAmount;
											//product.DisplayOrder = displayOrder;	//Commented 14 Sept 2018
											product.UpdatedOnUtc = DateTime.UtcNow;
											product.OrderMaximumQuantity = (jsonText.stockAmount < 1) ? 10000 : jsonText.stockAmount;
											product.IsShipEnabled = true;
											_productService.UpdateProduct(product);

											if (!string.IsNullOrEmpty(jsonText.imageLocation))
											{

												#region delete old images
												if (product.ProductPictures.Any())
												{
													var productPictures = product.ProductPictures.ToList();
													foreach (var productImage in productPictures)
													{
														var pictureId = productImage.PictureId;
														_productService.DeleteProductPicture(productImage);

														var singlePicture = _pictureService.GetPictureById(pictureId);
														if (singlePicture != null)
															_pictureService.DeletePicture(singlePicture);
													}
												}
												#endregion

												var imageBytes = GetRemoteImage(jsonText.imageLocation);
												if (imageBytes != null)
												{
													var picture = _pictureService.InsertPicture(imageBytes, MimeTypes.ImageJpeg, null);
													if (picture != null)
													{
														var productPicture = new ProductPicture
														{
															Picture = picture,
															DisplayOrder = 1,
															PictureId = picture.Id,
															ProductId = product.Id
														};
														product.ProductPictures.Add(productPicture);
														_productService.UpdateProduct(product);
													}
												}
											}

											//InsertUpdateProductCategoryMap(jsonText, product);//16102018::category mapping removed on exiting products
										}
									}

									checkExistingProduct.Code = companyCode;
									checkExistingProduct.Company = company;
									_cmsNopProductMappingService.Update(checkExistingProduct);
								}
							}
						}
					}
					_customCodeService.SendInformationEmail("Import category and product task end. Date :: " + DateTime.Now.ToString(CultureInfo.InvariantCulture), "The Import category and product task ended.");
				}
			}
			catch (Exception exception)
			{

				_logger.InsertLog(LogLevel.Error, "Error Importing Products from LS CMS.", exception.Message + "  ||  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Error Importing Products from LS CMS.", exception.Message, exception.StackTrace);
				_settingService.SetSetting("cmsproductimportflag", "false");
			}
		}


		public bool CheckImportTime()
		{
			var manualImportSetting = _settingService.GetSetting("manualImportProducts");
			if (manualImportSetting != null)
			{
				if (manualImportSetting.Value.ToLower() == "true")
				{
					return true;
				}
			}


			var checkSetting = _settingService.GetSetting("cmsproductimportflag");
			if (DateTime.Now.Hour == 1)
			{
				if (checkSetting == null)
				{
					_settingService.SetSetting("cmsproductimportflag", "true");
					return true;
				}

				if (checkSetting.Value == "true")
				{
					return false;
				}

				_settingService.SetSetting("cmsproductimportflag", "true");
				return true;
			}
			if (checkSetting == null)
			{
				_settingService.SetSetting("cmsproductimportflag", "false");
			}
			else if (checkSetting.Value == "true")
			{
				_settingService.SetSetting("cmsproductimportflag", "false");
			}
			return false;
		}



		public string GetToken()
		{
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";
			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			var token = json.GetValue("token").Value<string>();
			return token;
		}


		private byte[] GetRemoteImage(string filePath)
		{
			try
			{
				WebClient webClient = new WebClient();
				byte[] imageBytes = webClient.DownloadData(filePath);
				return imageBytes;
			}
			catch (Exception ex)
			{
				_customCodeService.SendErrorEmail("Error importing Product Image from LS CMS.", "Image Path: " + filePath + "  ||  " + ex.Message, ex.StackTrace);
				return null;
			}
		}


		public void InsertUpdateProductCategoryMap(CmsProductApiModel cmsProduct, Product nopProduct)
		{
			if (cmsProduct.groupIds.Any())
			{
				foreach (var groupId in cmsProduct.groupIds)
				{
					var cmsNopCategory = _cmsNopCategoryMappingService.GetByCmsCategoryId(groupId);
					if (cmsNopCategory == null)
						continue;

					var productCategory = new ProductCategory
					{
						CategoryId = cmsNopCategory.NopCategoryId,
						IsFeaturedProduct = true,
						ProductId = nopProduct.Id
					};
					_categoryService.InsertProductCategory(productCategory);
				}
			}
		}

		public void ImportCmsCategories()
		{
			try
			{
				string html = string.Empty;
				string url = @"https://euc1.posios.com/PosServer/rest/inventory/productgroup";


				var token = GetToken();

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.AutomaticDecompression = DecompressionMethods.GZip;

				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				using (Stream stream = response.GetResponseStream())
				using (StreamReader reader = new StreamReader(stream))
				{
					html = reader.ReadToEnd();
					JArray jsonArray = JArray.Parse(html);
					foreach (var item in jsonArray)
					{

						var jsonText = JsonConvert.DeserializeObject<CmsCategoryApiModel>(item.ToString());
						//if (jsonText.name.ToLower() == "gerechten" || jsonText.name.ToLower() == "broodjes" || jsonText.name.ToLower() == "dranken" || jsonText.name.ToLower() == "dagaanbieding"
						//	|| jsonText.name.ToLower() == "burger" || jsonText.name.ToLower() == "croque monsieur" || jsonText.name.ToLower() == "pizza" || jsonText.name.ToLower() == "pasta"
						//	|| jsonText.name.ToLower() == "salade" || jsonText.name.ToLower() == "wraps" || jsonText.name.ToLower() == "hot dog" || jsonText.name.ToLower() == "soep")
						//{
						//Check if category exist							
						var checkExistingCategory = _cmsNopCategoryMappingService.GetByCmsCategoryName(jsonText.name);
						if (checkExistingCategory == null)
						{
							var newCategory = new Category
							{
								Name = jsonText.name,
								Published = jsonText.visible,
								Deleted = false,
								//DisplayOrder = jsonText.sequence,	//Commented 14 Sept 2018
								CreatedOnUtc = DateTime.UtcNow,
								UpdatedOnUtc = DateTime.UtcNow
							};
							_categoryService.InsertCategory(newCategory);

							//search engine name
							var seName = newCategory.ValidateSeName(newCategory.Name.Replace(" ", ""), newCategory.Name, true);
							_urlRecordService.SaveSlug(newCategory, seName, 0);

							//Add Mapping for new product
							var cmsNopCategoryMap = new CmsCategoryNopCategoryMapping
							{
								NopCategoryId = newCategory.Id,
								CmsCategoryId = jsonText.id,
								CmsCategoryName = jsonText.name,
								CreatedOnUtc = DateTime.UtcNow,
								UpdatedOnUtc = DateTime.UtcNow
							};
							_cmsNopCategoryMappingService.Insert(cmsNopCategoryMap);

							continue;
						}
						var category = _categoryService.GetCategoryById(checkExistingCategory.NopCategoryId);
						if (category != null)
						{
							category.Name = jsonText.name;
							//category.DisplayOrder = jsonText.sequence;//Commented 14 Sept 2018
							category.UpdatedOnUtc = DateTime.UtcNow;
							_categoryService.UpdateCategory(category);


							#region Remove CategoryProduct Mappings - Commented 17102018 to disable refresh mapping while importing
							//var categoryProducts = _categoryService.GetProductCategoriesByCategoryId(category.Id);
							//if (categoryProducts.Any())
							//{
							//	var nopProductCategories = categoryProducts.ToList();
							//	foreach (var nopCategory in nopProductCategories)
							//	{
							//		_categoryService.DeleteProductCategory(nopCategory);
							//	}
							//}
							#endregion


							checkExistingCategory.CmsCategoryId = jsonText.id;
							_cmsNopCategoryMappingService.Update(checkExistingCategory);
						}
						//}
					}
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error Importing Categories from LS CMS", ex.Message + "  ||  " + ex.StackTrace);
				_customCodeService.SendErrorEmail("Error importing Categories from LS CMS.", ex.Message, ex.StackTrace);
			}
		}

		private Int32 GetOrderNo(string sku)
		{

			int numCode, a = getIndexofNumber(sku);
			string Numberpart = sku.Substring(a, sku.Length - a);

			numCode = Convert.ToInt32(Numberpart);
			return numCode;
		}

		private int getIndexofNumber(string sku)
		{
			int indexofNum = -1;
			foreach (char c in sku)
			{
				indexofNum++;
				if (Char.IsDigit(c))
				{
					return indexofNum;
				}
			}
			return indexofNum;
		}

	}
}
