﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Logging;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.CustomService;
using Nop.Services.Logging;
using Nop.Services.Orders;

namespace Nop.Plugin.PostXCustom.Tasks
{
	public class UpdateLoyaltyPointsTask //: ITask
	{
		#region Fields

		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;
		private readonly ICustomCodeService _customCodeService;
		private readonly IRewardPointService _rewardPointService;
		private readonly IStoreContext _storeContext;
		private readonly ISettingService _settingService;
		private readonly ILogger _logger;
		private readonly ILightSpeedOfflineOrdersService _lightSpeedOfflineOrdersService;
		private readonly IOrderTotalCalculationService _orderTotalCalculationService;
		private readonly ICustomerService _customerService;
		#endregion

		#region Ctor
		public UpdateLoyaltyPointsTask(ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICmsNopOrderMappingService cmsNopOrderMappingService,
			ICustomCodeService customCodeService, IRewardPointService rewardPointService, IStoreContext storeContext,
			ISettingService settingService, ILogger logger, ILightSpeedOfflineOrdersService lightSpeedOfflineOrdersService,
			IOrderTotalCalculationService orderTotalCalculationService, ICustomerService customerService)
		{
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_cmsNopOrderMappingService = cmsNopOrderMappingService;
			_customCodeService = customCodeService;
			_rewardPointService = rewardPointService;
			_storeContext = storeContext;
			_settingService = settingService;
			_logger = logger;
			_lightSpeedOfflineOrdersService = lightSpeedOfflineOrdersService;
			_orderTotalCalculationService = orderTotalCalculationService;
			_customerService = customerService;
		}
		#endregion


		public void Execute()
		{
			try
			{
				var offsetValue = 0;
				for (int count = 0; count < int.MaxValue; count++)
				{
					string html;
					string url = @"https://euc1.posios.com/PosServer/rest/onlineordering/order";

					if (offsetValue > 0)
						url = @"https://euc1.posios.com/PosServer/rest/onlineordering/order?offset=" + offsetValue;

					var token = GetToken();

					HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
					request.Headers.Add("X-Auth-Token", token);
					request.AutomaticDecompression = DecompressionMethods.GZip;

					using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
					using (Stream stream = response.GetResponseStream())
					using (StreamReader reader = new StreamReader(stream))
					{
						html = reader.ReadToEnd();
						JArray jsonArray = JArray.Parse(html);

						if (!jsonArray.Any()) { break; }

						offsetValue = offsetValue + 50;

						foreach (var item in jsonArray)
						{
							var jsonText = JsonConvert.DeserializeObject<LightSpeedOrderSummaryModel>(item.ToString());

							//check if customer exists in nop
							var checkLightSpeedCustomer = _cmsNopCustomerMappingService.GetByCmsCustomerId(jsonText.customerId);
							if (checkLightSpeedCustomer == null)
								continue;


							//Check if product exist
							var checkExistingNopSyncOrder = _cmsNopOrderMappingService.GetByCmsOrderId(Convert.ToInt32(jsonText.id));
							if (checkExistingNopSyncOrder != null)
								continue;

							//Check if already processed offline order
							var checkAlreadyProcessedOrder = _lightSpeedOfflineOrdersService.GetByLightSpeedOrderId(Convert.ToInt32(jsonText.id));
							if (checkAlreadyProcessedOrder != null)
								continue;

							//Get Nop Customer
							var nopCustomer = _customerService.GetCustomerById(checkLightSpeedCustomer.NopCustomerId);
							if (nopCustomer == null)
								continue;


							//Get LS Order Details
							string orderDetailUrl = @"https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + jsonText.customerId + "/order/" + jsonText.id;
							token = GetToken();
							HttpWebRequest requestOrderDetail = (HttpWebRequest)WebRequest.Create(orderDetailUrl);
							requestOrderDetail.Headers.Add("X-Auth-Token", token);
							requestOrderDetail.AutomaticDecompression = DecompressionMethods.GZip;

							using (HttpWebResponse responseOrderDetail = (HttpWebResponse)requestOrderDetail.GetResponse())
							using (Stream streamOrderDetail = responseOrderDetail.GetResponseStream())
							using (StreamReader readerOrderDetail = new StreamReader(streamOrderDetail))
							{
								var htmlOrderDetail = readerOrderDetail.ReadToEnd();
								//JArray jsonArraOrderDetail = JArray.Parse(htmlOrderDetail);
								var jsonOrderDetail = JsonConvert.DeserializeObject<CmsOrderApiModel>(htmlOrderDetail.ToString());

								//check for Cash Payment
								var cashPayment = jsonOrderDetail.orderPayments.FirstOrDefault(x => x.paymentTypeId == 36191);
								var loyaltyOrderTotal = 0M;
								if (cashPayment != null)
									loyaltyOrderTotal = jsonOrderDetail.orderPayment.amount - cashPayment.amount;
								else
									loyaltyOrderTotal = jsonOrderDetail.orderPayment.amount;

								#region Loyalty points calculation
								//reward points to be earned
								var totalLoyaltyPoints = 0;
								totalLoyaltyPoints = _orderTotalCalculationService.CalculateRewardPoints(nopCustomer, loyaltyOrderTotal);

								if (totalLoyaltyPoints > 0)
								{
									_rewardPointService.AddRewardPointsHistoryEntry(nopCustomer, totalLoyaltyPoints, _storeContext.CurrentStore.Id, "Loyalty Points for offline LS order #" + jsonText.id);

									//UpdateCustomerLoyaltyPointsOnLightSpeed(Convert.ToInt32(jsonText.id), checkLightSpeedCustomer, nopCustomer);
								}
								#endregion


									#region Save LightSpeed order for future reference.
								var lightSpeedOfflineOrderRecord = new LightSpeedOfflineOrders
								{
									ImportedOnUtc = DateTime.UtcNow,
									LightSpeedCustomerId = jsonOrderDetail.customerId,
									LightSpeedOrderId = jsonOrderDetail.id,
									OrderTotal = jsonOrderDetail.orderPayment.amount
								};


								//if (cashPayment != null)
								//{
								//	lightSpeedOfflineOrderRecord.LoyaltyOrderTotal = jsonOrderDetail.orderPayment.amount - cashPayment.amount;
								//}

								lightSpeedOfflineOrderRecord.LoyaltyOrderTotal = loyaltyOrderTotal;

								var paymentSummary = string.Empty;
								foreach (var items in jsonOrderDetail.orderPayments)
								{
									paymentSummary = paymentSummary + items.paymentTypeId + ":" + items.paymentTypeTypeId + ":" + items.amount + "|";
								}

								_lightSpeedOfflineOrdersService.Insert(lightSpeedOfflineOrderRecord);

								#endregion
							}

						}
					}
				}
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error Importing Loyalty Data from LS CMS.", exception.Message + "  ||  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Error Importing Loyalty Data from LS CMS.", exception.Message, exception.StackTrace);
				_settingService.SetSetting("cmsloyaltyimportflag", "false");
			}
		}


		public bool CheckImportTime()
		{
			var manualImportSetting = _settingService.GetSetting("manualImportLoyalty");
			if (manualImportSetting != null)
			{
				if (manualImportSetting.Value.ToLower() == "true")
				{
					return true;
				}
			}

			var checkSetting = _settingService.GetSetting("cmsloyaltyimportflag");
			if (DateTime.Now.Hour == 1)
			{
				if (checkSetting == null)
				{
					_settingService.SetSetting("cmsloyaltyimportflag", "true");
					return true;
				}

				if (checkSetting.Value == "true")
				{
					return false;
				}

				_settingService.SetSetting("cmsloyaltyimportflag", "true");
				return true;
			}
			if (checkSetting == null)
			{
				_settingService.SetSetting("cmsloyaltyimportflag", "false");
			}
			else if (checkSetting.Value == "true")
			{
				_settingService.SetSetting("cmsloyaltyimportflag", "false");
			}
			return false;
		}


		public string GetToken()
		{
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";
			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			var token = json.GetValue("token").Value<string>();
			return token;
		}

		public void UpdateCustomerLoyaltyPointsOnLightSpeed(int cmsOrderId, CmsCustomerNopCustomerMapping checkCmsCustomer,Customer customer)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerLoyaltyPointsOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + customer.Email + "\",");

				var loyaltyPoints = "0 reward points available.";
				var customerRewardPointsBalance = _rewardPointService.GetRewardPointsBalance(customer.Id, _storeContext.CurrentStore.Id);
				if (customerRewardPointsBalance > 0)
					loyaltyPoints = customerRewardPointsBalance + " reward points available.";

				postData.Append("\"firstName\":\"" + loyaltyPoints + "\",");
				postData.Append("\"lastName\":\"" + customer.GetFullName() + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
				}

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating loyalty points for customer on Light Speed CMS :: Offline LS OrderId :: " + cmsOrderId + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message + "  ::  " + exception.StackTrace);
			}
		}

	}
}
