﻿using System;
using Nop.Core;

namespace Nop.Plugin.PostXCustom.Domain
{
	public partial class CmsCategoryNopCategoryMapping : BaseEntity
	{
		public int NopCategoryId { get; set; }

		public int CmsCategoryId { get; set; }

        public string CmsCategoryName { get; set; }

        public DateTime CreatedOnUtc { get; set; }

		public DateTime UpdatedOnUtc { get; set; }
	}
}
