﻿namespace Nop.Plugin.PostXCustom.Domain
{
	public enum VatRegimeType
	{
		/// <summary>
		/// B - Vat Required
		/// </summary>
		Btwplichtig = 1,

		/// <summary>
		/// P - Nop Vat Required
		/// </summary>
		Particulier = 2,

		/// <summary>
		/// E - Export
		/// </summary>
		Export = 3,
		/// <summary>
		/// F - Factuur
		/// </summary>
		Factuur = 4,
	}
}
