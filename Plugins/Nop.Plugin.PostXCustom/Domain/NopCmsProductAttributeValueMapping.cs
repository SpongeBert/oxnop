﻿using System;
using Nop.Core;

namespace Nop.Plugin.PostXCustom.Domain
{
	public partial class NopCmsProductAttributeValueMapping : BaseEntity
	{
		public int ProductAttributeValueId { get; set; }	 
		public int NopCmsProductAttributeMappingId { get; set; }		
		public string Name { get; set; }
		public decimal Price { get; set; }
		public int CmsProductAttributeValueMappingId { get; set; }

	}
}
