﻿using System;
using Nop.Core;

namespace Nop.Plugin.PostXCustom.Domain
{
	public partial class CmsCustomerNopCustomerMapping : BaseEntity
	{
		public int NopCustomerId { get; set; }

		public int CmsCustomerId { get; set; }

		public DateTime CreatedOnUtc { get; set; }

		public DateTime UpdatedOnUtc { get; set; }

		public bool Deleted { get; set; }
	}
}
