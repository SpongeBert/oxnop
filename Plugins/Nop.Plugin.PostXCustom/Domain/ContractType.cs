﻿namespace Nop.Plugin.PostXCustom.Domain
{
	public enum ContractType
	{
		/// <summary>
		/// Contract A
		/// </summary>
		ContractA = 1,

		/// <summary>
		/// Contract B
		/// </summary>
		ContractB = 2,

		/// <summary>
		/// Contract C
		/// </summary>
		ContractC = 3,

		/// <summary>
		/// Contract D for general users
		/// </summary>
		ContractD = 4,

		/// <summary>
		/// For loyalty Percentage as Zero
		/// </summary>
		ContractZ = 26
	}
}
