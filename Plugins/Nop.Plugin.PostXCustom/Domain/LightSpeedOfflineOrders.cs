﻿using System;
using Nop.Core;

namespace Nop.Plugin.PostXCustom.Domain
{
	public partial class LightSpeedOfflineOrders : BaseEntity
	{
		public int LightSpeedOrderId { get; set; }

		public int LightSpeedCustomerId { get; set; }

		public decimal OrderTotal { get; set; }

		public decimal LoyaltyOrderTotal { get; set; }

		public string PaymentDetails { get; set; }

		public DateTime ImportedOnUtc { get; set; }
	}
}
