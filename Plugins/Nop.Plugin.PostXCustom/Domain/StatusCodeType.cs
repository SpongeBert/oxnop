﻿namespace Nop.Plugin.PostXCustom.Domain
{
	public enum StatusCodeType
	{
		/// <summary>
		/// A - Active
		/// </summary>
		Active = 1,

		/// <summary>
		/// B - Blocked
		/// </summary>
		Blocked = 2,

		/// <summary>
		/// X - Delete
		/// </summary>
		X = 3,
	}
}
