﻿using System;
using Nop.Core;

namespace Nop.Plugin.PostXCustom.Domain
{
	public partial class LightSpeedReceiptsMapping : BaseEntity
	{
		public int ReceiptId { get; set; }

		public int LightSpeedCustomerId { get; set; }

		public int NopCustomerId { get; set; }
																	  																	  
		public decimal OrderTotal { get; set; }

		public decimal OrderTotalNonCash { get; set; }

		public decimal OrderTotalCash { get; set; }

		public int LoyaltyPoints { get; set; }

		public DateTime ReceiptCreateDate { get; set; }

		public DateTime ImportedOnDate { get; set; }

		public DateTime? LoyaltyProcessedOnDate { get; set; }

		public bool LoyaltyProcessed { get; set; }


		public string NopUsername { get; set; }

		public string LoyaltyPercentage { get; set; }

		public decimal VAT21Total { get; set; }

		public decimal VAT12Total { get; set; }

		public decimal VAT6Total { get; set; }

		public decimal VAT21OrderTotal { get; set; }

		public decimal VAT12OrderTotal { get; set; }

		public decimal VAT6OrderTotal { get; set; }

		public string VatRegime { get; set; }
	}
}
