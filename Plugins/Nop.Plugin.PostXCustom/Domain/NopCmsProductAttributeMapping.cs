﻿using System;
using Nop.Core;

namespace Nop.Plugin.PostXCustom.Domain
{
	public partial class NopCmsProductAttributeMapping : BaseEntity
	{
		public int NopProductAttributeId { get; set; }	 
		public int CmsProductAttributeId { get; set; }	 													 
		public string CmsProductAttributeName { get; set; }

	}
}
