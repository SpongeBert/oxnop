﻿using System;
using Nop.Core;

namespace Nop.Plugin.PostXCustom.Domain
{
	public partial class CmsOrderNopOrderMapping : BaseEntity
	{
		public int NopOrderId { get; set; }

		public int CmsOrderId { get; set; }

		public DateTime CreatedOnUtc { get; set; }

		public DateTime UpdatedOnUtc { get; set; }
	}
}
