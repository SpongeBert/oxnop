﻿namespace Nop.Plugin.PostXCustom.Domain
{
	public enum AuthorizationType
	{
		/// <summary>
		/// Private only
		/// </summary>
		Prive = 1,

		/// <summary>
		/// Private + Company
		/// </summary>
		Bedrijf = 2,
		
	}
}
