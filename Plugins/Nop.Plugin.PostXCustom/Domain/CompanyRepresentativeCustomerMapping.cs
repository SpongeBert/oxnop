﻿using Nop.Core;

namespace Nop.Plugin.PostXCustom.Domain
{
	public partial class CompanyRepresentativeCustomerMapping : BaseEntity
	{
		public int CompanyRepresentativeId { get; set; }

		public int AssociatedCustomerId { get; set; }
	}
}
