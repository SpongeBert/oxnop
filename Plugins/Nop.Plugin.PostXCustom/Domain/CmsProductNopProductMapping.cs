﻿using System;
using Nop.Core;

namespace Nop.Plugin.PostXCustom.Domain
{
	public partial class CmsProductNopProductMapping : BaseEntity
	{
		public int NopProductId { get; set; }

		public int CmsProductId { get; set; }
													 
		public DateTime CreatedOnUtc { get; set; }

		public DateTime UpdatedOnUtc { get; set; } 

		public string Company { get; set; }

		public string Code { get; set; }
	}
}
