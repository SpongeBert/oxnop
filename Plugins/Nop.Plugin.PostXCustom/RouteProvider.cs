﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.PostXCustom
{
	public partial class RouteProvider : IRouteProvider
	{
		#region IRouteProvider Members

		public void RegisterRoutes(RouteCollection routes)
		{
			routes.MapRoute("Nop.Plugin.PostXCustom.WinesConfigure",
				  "WinesConfigure/",
				  new { controller = "PostXCustom", action = "CustomConfigure" },
				  new[] { "Nop.Plugin.PostXCustom.Controllers" }
			 );

			routes.MapRoute("Nop.Plugin.PostXCustom.Configure",
					"Plugins/PostXCustom/Configure",
					new { controller = "PostXCustom", action = "Configure", },
					new[] { "Nop.Plugin.PostXCustom.Controllers" }
			 );

			//Admin Customer create override
			var adminCustomerCreate = routes.MapRoute("AdminCustomerCreate",
									  "admin/customer/create/",
									  new { controller = "AdminCustomer", action = "Create" },
									  new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(adminCustomerCreate);
			routes.Insert(0, adminCustomerCreate);

			//Admin Customer Edit override
			var adminCustomerEdit = routes.MapRoute("AdminCustomerEdit",
									  "admin/customer/edit/{id}",
									  new { controller = "AdminCustomer", action = "Edit" },
									  new { id = @"\d+" },
									  new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(adminCustomerEdit);
			routes.Insert(0, adminCustomerEdit);

			routes.MapRoute("AdminCustomerEdit",
					"admin/customer/edit/{id}",
					new { controller = "Customer", action = "Edit", },
					  new { id = @"\d+" },
					new[] { "Nop.Admin.Controllers" }
			 );

			routes.MapRoute("AdminCustomerList",
					"admin/customer/list",
					new { controller = "Customer", action = "List", },
					new[] { "Nop.Admin.Controllers" }
			 );

			routes.MapRoute("AdminCustomerOrderList",
					"admin/customer/OrderList/{customerId}",
					new { controller = "Customer", action = "OrderList", },
					new { customerId = @"\d+" },
					new[] { "Nop.Admin.Controllers" }
			 );

			routes.MapLocalizedRoute("PublicAutoLogin",
								 "Autologin/{username}/{password}",
								 new { controller = "PublicCustomer", action = "AutoLogin" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
				

			//Customer Login override
			var publicCustomerLogin = routes.MapLocalizedRoute("PublicCustomerLogin",
								"login/",
								new { controller = "PublicCustomer", action = "Login" },
								new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCustomerLogin);
			routes.Insert(0, publicCustomerLogin);



			var guestCheckout = routes.MapLocalizedRoute("PublicLoginCheckoutAsGuest",
								 "login/checkoutasguest",
								 new { controller = "PublicCustomer", action = "Login", checkoutAsGuest = true },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(guestCheckout);
			routes.Insert(0, guestCheckout);

			//Customer Register override
			var publicCustomerRegister = routes.MapLocalizedRoute("PublicCustomerRegister",
							 "register/",
							 new { controller = "PublicCustomer", action = "Register" },
							 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCustomerRegister);
			routes.Insert(0, publicCustomerRegister);

			//CustomerInfo override
			var publicCustomerInfo = routes.MapLocalizedRoute("PublicCustomerInfo",
									 "customer/info",
									 new { controller = "PublicCustomer", action = "Info" },
									 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCustomerInfo);
			routes.Insert(0, publicCustomerInfo);


			#region checkout pages

			var publicOnePageCheckout = routes.MapLocalizedRoute("PublicOnePageCheckout",
									 "onepagecheckout/",
									 new { controller = "PublicCheckout", action = "OnePageCheckout" },
									 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicOnePageCheckout);
			routes.Insert(0, publicOnePageCheckout);

			var publicCheckout = routes.MapLocalizedRoute("PublicCheckout",
									 "checkout/",
									 new { controller = "PublicCheckout", action = "Index" },
									 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckout);
			routes.Insert(0, publicCheckout);

			var publicCheckoutShippingAddress = routes.MapLocalizedRoute("PublicCheckoutShippingAddress",
									 "checkout/shippingaddress",
									 new { controller = "PublicCheckout", action = "ShippingAddress" },
									 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckoutShippingAddress);
			routes.Insert(0, publicCheckoutShippingAddress);

			var publicCheckoutSelectShippingAddress = routes.MapLocalizedRoute("PublicCheckoutSelectShippingAddress",
								 "checkout/selectshippingaddress",
								 new { controller = "PublicCheckout", action = "SelectShippingAddress" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckoutSelectShippingAddress);
			routes.Insert(0, publicCheckoutSelectShippingAddress);


			var publicCheckoutBillingAddress = routes.MapLocalizedRoute("PublicCheckoutBillingAddress",
								 "checkout/billingaddress",
								 new { controller = "PublicCheckout", action = "BillingAddress" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckoutBillingAddress);
			routes.Insert(0, publicCheckoutBillingAddress);


			var publicCheckoutSelectBillingAddress = routes.MapLocalizedRoute("PublicCheckoutSelectBillingAddress",
								 "checkout/selectbillingaddress",
								 new { controller = "PublicCheckout", action = "SelectBillingAddress", addressId = UrlParameter.Optional, shipToSameAddress = UrlParameter.Optional },
								  new { addressId = @"\d+" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckoutSelectBillingAddress);
			routes.Insert(0, publicCheckoutSelectBillingAddress);

			var publicCheckoutShippingMethod = routes.MapLocalizedRoute("PublicCheckoutShippingMethod",
								 "checkout/shippingmethod",
								 new { controller = "PublicCheckout", action = "ShippingMethod" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckoutShippingMethod);
			routes.Insert(0, publicCheckoutShippingMethod);


			var publicCheckoutPaymentMethod = routes.MapLocalizedRoute("PublicCheckoutPaymentMethod",
								 "checkout/paymentmethod",
								 new { controller = "PublicCheckout", action = "PaymentMethod" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckoutPaymentMethod);
			routes.Insert(0, publicCheckoutPaymentMethod);


			var publicCheckoutPaymentInfo = routes.MapLocalizedRoute("PublicCheckoutPaymentInfo",
								 "checkout/paymentinfo",
								 new { controller = "PublicCheckout", action = "PaymentInfo" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckoutPaymentInfo);
			routes.Insert(0, publicCheckoutPaymentInfo);


			var publicCheckoutConfirm = routes.MapLocalizedRoute("PublicCheckoutConfirm",
								 "checkout/confirm",
								 new { controller = "PublicCheckout", action = "Confirm" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckoutConfirm);
			routes.Insert(0, publicCheckoutConfirm);

			var publicCheckoutCompleted = routes.MapLocalizedRoute("PublicCheckoutCompleted",
								 "checkout/completed/{orderId}",
								 new { controller = "PublicCheckout", action = "Completed", orderId = UrlParameter.Optional },
								 new { orderId = @"\d+" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(publicCheckoutCompleted);
			routes.Insert(0, publicCheckoutCompleted);

			#endregion

			var orderDetails = routes.MapLocalizedRoute("PublicOrderDetails",
								 "orderdetails/{orderId}",
								 new { controller = "PublicOrder", action = "Details" },
								 new { orderId = @"\d+" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });

			routes.Remove(orderDetails);
			routes.Insert(0, orderDetails);


			routes.MapLocalizedRoute("PublicGetOrderPdfInvoice",
								"orderdetail/pdf/{orderId}",
								new { controller = "PublicOrder", action = "GetPdfInvoice" },
								new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.MapLocalizedRoute("PublicPrintOrderDetails",
								 "orderdetail/print/{orderId}",
								 new { controller = "PublicOrder", action = "PrintOrderDetails" },
								 new[] { "Nop.Plugin.PostXCustom.Controllers" });



			//Admin product list override
			var adminProductList = routes.MapRoute("AdminProductList",
				"admin/product/list/",
				new { controller = "AdminProduct", action = "List" },
				new[] { "Nop.Plugin.PostXCustom.Controllers" });
			routes.Remove(adminProductList);
			routes.Insert(0, adminProductList);



			//var adminCustomerAddressEdit = routes.MapRoute("AdminCustomerAddressEdit",
			//	"admin/customer/AddressEdit/{customerId}/{addressId}",
			//	new { controller = "AdminCustomer", action = "AddressEdit" },
			//	new { addressId = @"\d+", customerId =@"\d+" },
			//	new[] { "Nop.Plugin.PostXCustom.Controllers" });
			//var adminCustomerAddressEdit = routes.MapRoute("AdminCustomerAddressEdit",
			//	"admin/customer/AddressEdit/",
			//	new { controller = "AdminCustomer", action = "AddressEdit" },
			//	new { addressId = @"\d+", customerId = @"\d+" },
			//	new[] { "Nop.Plugin.PostXCustom.Controllers" });
			//routes.Remove(adminCustomerAddressEdit);
			//routes.Insert(0, adminCustomerAddressEdit);


		}

		public int Priority
		{
			get
			{
				return 0;
			}
		}

		#endregion
	}
}
