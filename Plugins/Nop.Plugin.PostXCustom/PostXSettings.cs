﻿using Nop.Core.Configuration;

namespace Nop.Plugin.PostXCustom
{
	public class PostXSettings : ISettings
	{
		public string CustomerPushUrl { get; set; }
		
		public int LoyaltyContractA { get; set; }
		
		public int LoyaltyContractB { get; set; }
		
		public int LoyaltyContractC { get; set; }

		public int LoyaltyContractD { get; set; }

		public int LoyaltyContractZ { get; set; }
	}
}
