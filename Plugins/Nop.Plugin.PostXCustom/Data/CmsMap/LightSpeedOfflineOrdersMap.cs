﻿using Nop.Data.Mapping;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Data.CmsMap
{
	public partial class LightSpeedOfflineOrdersMap : NopEntityTypeConfiguration<LightSpeedOfflineOrders>
	{
		public LightSpeedOfflineOrdersMap()
		{
			this.ToTable("LightSpeedOfflineOrders");
			this.HasKey(n => n.Id);

		}
	}
}
