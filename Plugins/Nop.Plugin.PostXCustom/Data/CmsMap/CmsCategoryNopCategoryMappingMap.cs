﻿using Nop.Data.Mapping;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Data.CmsMap
{

	public partial class CmsCategoryNopCategoryMappingMap : NopEntityTypeConfiguration<CmsCategoryNopCategoryMapping>
	{
		public CmsCategoryNopCategoryMappingMap()
		{
			this.ToTable("CmsCategoryNopCategoryMapping");
			this.HasKey(n => n.Id);

		}
	}
}
