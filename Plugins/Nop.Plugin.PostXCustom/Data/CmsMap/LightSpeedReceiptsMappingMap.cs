﻿using Nop.Data.Mapping;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Data.CmsMap
{
	public partial class LightSpeedReceiptsMappingMap : NopEntityTypeConfiguration<LightSpeedReceiptsMapping>
	{
		public LightSpeedReceiptsMappingMap()
		{
			this.ToTable("LightSpeedReceiptsMapping");
			this.HasKey(n => n.Id);

		}
	}
}
