﻿using Nop.Data.Mapping;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Data.CmsMap
{	
	public partial class NopCmsProductAttributeValueMappingMap : NopEntityTypeConfiguration<NopCmsProductAttributeValueMapping>
	{
		public NopCmsProductAttributeValueMappingMap()
		{
			this.ToTable("NopCmsProductAttributeValueMapping");
			this.HasKey(n => n.Id);
			
		}
	}
}
