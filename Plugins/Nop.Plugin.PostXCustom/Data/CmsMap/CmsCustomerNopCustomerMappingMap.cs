﻿using Nop.Data.Mapping;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Data.CmsMap
{
	public partial class CmsCustomerNopCustomerMappingMap : NopEntityTypeConfiguration<CmsCustomerNopCustomerMapping>
	{
		public CmsCustomerNopCustomerMappingMap()
		{
			this.ToTable("CmsCustomerNopCustomerMapping");
			this.HasKey(n => n.Id);
			this.Property(x => x.Deleted).IsOptional();
		}
	}
}
