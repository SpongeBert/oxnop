﻿using Nop.Data.Mapping;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Data.CmsMap
{
	public partial class CmsOrderNopOrderMappingMap : NopEntityTypeConfiguration<CmsOrderNopOrderMapping>
	{
		public CmsOrderNopOrderMappingMap()
		{
			this.ToTable("CmsOrderNopOrderMapping");
			this.HasKey(n => n.Id);		
		}
	}
}
