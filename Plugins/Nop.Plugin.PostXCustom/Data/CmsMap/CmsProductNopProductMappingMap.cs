﻿using Nop.Data.Mapping;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Data.CmsMap
{	
	public partial class CmsProductNopProductMappingMap : NopEntityTypeConfiguration<CmsProductNopProductMapping>
	{
		public CmsProductNopProductMappingMap()
		{
			this.ToTable("CmsProductNopProductMapping");
			this.HasKey(n => n.Id);
			
		}
	}
}
