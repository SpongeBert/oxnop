﻿using Nop.Data.Mapping;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Data.CmsMap
{	
	public partial class NopCmsProductAttributeMappingMap : NopEntityTypeConfiguration<NopCmsProductAttributeMapping>
	{
		public NopCmsProductAttributeMappingMap()
		{
			this.ToTable("NopCmsProductAttributeMapping");
			this.HasKey(n => n.Id);
			
		}
	}
}
