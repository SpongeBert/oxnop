﻿using System.Data.Entity;
using Nop.Core.Infrastructure;

namespace Nop.Plugin.PostXCustom.Data
{
	public class EfStartUpTask	: IStartupTask
	{
		#region IStartupTask Members

		public void Execute()
		{
			Database.SetInitializer<PostXCustomObjectContext>(null);
		}

		public int Order
		{
			get { return 0; }
		}

		#endregion
	}
}
