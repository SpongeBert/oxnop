﻿using Nop.Data.Mapping;
using Nop.Plugin.PostXCustom.Domain;

namespace Nop.Plugin.PostXCustom.Data.SalesPersonCustomer
{
	public partial class CompanyRepresentativeCustomerMappingMap : NopEntityTypeConfiguration<CompanyRepresentativeCustomerMapping>
	{
		public CompanyRepresentativeCustomerMappingMap()
		{
			this.ToTable("CompanyRepresentativeCustomerMapping");
			this.HasKey(n => n.Id);
			this.Property(n => n.CompanyRepresentativeId).IsRequired();
			this.Property(n => n.AssociatedCustomerId).IsRequired();
		}
	}
}
