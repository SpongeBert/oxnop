﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Nop.Data;
using Nop.Plugin.PostXCustom.Data.CmsMap;
using Nop.Plugin.PostXCustom.Domain;
using System.Data.Entity.Infrastructure;
using Nop.Core;
using Nop.Plugin.PostXCustom.Data.SalesPersonCustomer;


namespace Nop.Plugin.PostXCustom.Data
{
	public class PostXCustomObjectContext  : DbContext, IDbContext
	{
		#region	Ctor

		public PostXCustomObjectContext(string nameOrConnectionString)
			: base(nameOrConnectionString) { }
      #endregion

		#region Utilities
		protected override void OnModelCreating(DbModelBuilder dbModelBuilder)
		{	
			dbModelBuilder.Configurations.Add(new CompanyRepresentativeCustomerMappingMap());
			dbModelBuilder.Configurations.Add(new CmsProductNopProductMappingMap());
			dbModelBuilder.Configurations.Add(new CmsCustomerNopCustomerMappingMap());
			dbModelBuilder.Configurations.Add(new CmsOrderNopOrderMappingMap());
			dbModelBuilder.Configurations.Add(new CmsCategoryNopCategoryMappingMap());
			dbModelBuilder.Configurations.Add(new LightSpeedOfflineOrdersMap());
			dbModelBuilder.Configurations.Add(new LightSpeedReceiptsMappingMap());
			dbModelBuilder.Configurations.Add(new NopCmsProductAttributeMappingMap());
			dbModelBuilder.Configurations.Add(new NopCmsProductAttributeValueMappingMap());
			
			base.OnModelCreating(dbModelBuilder);
		}
		#endregion	

		#region	 Methods
		public string CreateDatabaseScript()
		{
			return ((IObjectContextAdapter)this).ObjectContext.CreateDatabaseScript();
		}

		public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
		{
			return base.Set<TEntity>();
		}

		public void Install()
		{
			var dbScript = CreateDatabaseScript();
			Database.ExecuteSqlCommand(dbScript);
			SaveChanges();
		}

		public void Uninstall()
		{
			var tableName = this.GetTableName<CompanyRepresentativeCustomerMapping>();
			this.DropPluginTable(tableName);

			tableName = this.GetTableName<CmsProductNopProductMapping>();
			this.DropPluginTable(tableName);

			tableName = this.GetTableName<CmsCustomerNopCustomerMapping>();
			this.DropPluginTable(tableName);

			tableName = this.GetTableName<CmsOrderNopOrderMapping>();
			this.DropPluginTable(tableName);

			tableName = this.GetTableName<CmsCategoryNopCategoryMapping>();
			this.DropPluginTable(tableName);

			tableName = this.GetTableName<LightSpeedOfflineOrders>();
			this.DropPluginTable(tableName);

			tableName = this.GetTableName<LightSpeedReceiptsMapping>();
			this.DropPluginTable(tableName);

			tableName = this.GetTableName<NopCmsProductAttributeMapping>();
			this.DropPluginTable(tableName);

			tableName = this.GetTableName<NopCmsProductAttributeValueMapping>();
			this.DropPluginTable(tableName);	  
		}

		public IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : BaseEntity, new ()
		{
			throw new NotImplementedException();
		}

		public IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
		{
			throw new NotImplementedException();
		}

		public int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters)
		{
			throw new NotImplementedException();
		}
		#endregion

		#region	Properties
		public bool AutoDetectChangesEnabled
		{
			get
			{
				return this.Configuration.AutoDetectChangesEnabled;
			}
			set
			{
				this.Configuration.AutoDetectChangesEnabled = value;
			}
		}

		public void Detach(object entity)
		{
			if(entity==null)
				throw new ArgumentException("entity");
			((IObjectContextAdapter)this).ObjectContext.Detach(entity);
		}

		public bool ProxyCreationEnabled
		{
			get
			{
				return this.Configuration.ProxyCreationEnabled;
			}
			set
			{
				this.Configuration.ProxyCreationEnabled = value;
			}
		}
		#endregion
	}
}
