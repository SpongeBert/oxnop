﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using Nop.Core.Domain.Customers;
using Nop.Core.Plugins;
using Nop.Plugin.PostXCustom.Data;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Security;
using Nop.Web.Framework.Events;
using Nop.Web.Framework.Menu;
using System.Linq;
using Nop.Services.Stores;
using Nop.Core;

namespace Nop.Plugin.PostXCustom
{	
	public class PostXCustomPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin, IConsumer<AdminTabStripCreated>
	{
		#region Fields
		private readonly PostXCustomObjectContext _postXCustomObjectContext;
		private readonly ILocalizationService _localizationService;
		private readonly ISettingService _settingService;
		private readonly ICustomerService _customerService;
		private readonly IPermissionService _permissionService;
		private readonly PostXSettings _postXSettings;
		private readonly IWorkContext _workContext;
		private readonly IStoreService _storeService;
		#endregion

		#region Ctor
		public PostXCustomPlugin(PostXCustomObjectContext postXCustomObjectContext,
			ILocalizationService localizationService, ISettingService settingService,
			ICustomerService customerService, IPermissionService permissionService,
			PostXSettings postXSettings,IWorkContext workContext,IStoreService storeService)
		{
			_postXCustomObjectContext = postXCustomObjectContext;
			_localizationService = localizationService;
			_settingService = settingService;
			_customerService = customerService;
			_permissionService = permissionService;
			_postXSettings = postXSettings;
			_storeService = storeService;
			_workContext = workContext;
		}
		#endregion

		#region Methods

		/// <summary>
		/// Gets a route for provider configuration
		/// </summary>
		/// <param name="actionName">Action name</param>
		/// <param name="controllerName">Controller name</param>
		/// <param name="routeValues">Route values</param>
		public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
		{
			actionName = "Configure";
			controllerName = "PostXCustom";
			routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.PostXCustom.Controllers" }, { "area", null } };
		}



		public override void Install()
		{
			var settings = new PostXSettings
			{
				CustomerPushUrl = "http://localhost/wines/CustomerAPI/create",
			};
			_settingService.SaveSetting(settings);

			_postXCustomObjectContext.Install();

			//Add Custom Roles
			var role = CompanyRepresentativeRole("Company Representative", "CompanyRepresentative");
			_customerService.InsertCustomerRole(role);
											 		  
			//locales
			//this.AddOrUpdatePluginLocaleResource("Key", "Value");
			//this.AddOrUpdatePluginLocaleResource("", "");

			base.Install();
		}

		public override void Uninstall()
		{											 
			_settingService.DeleteSetting<PostXSettings>();
			_postXCustomObjectContext.Uninstall();			  

			//locales			
			//this.DeletePluginLocaleResource("KeyName");
			base.Uninstall();
		}													  

		public void ManageSiteMap(SiteMapNode rootNode)
		{
			var menuItem = new SiteMapNode
			{
				Title = "Manage Ox Settings",
				ControllerName = "",
				ActionName = "",
				Visible = true,
				RouteValues = new RouteValueDictionary { { "area", null } }
			};

			var subMenuItemSetting = new SiteMapNode
			{
				Title = "Manage Settings",
				ControllerName = "PostXCustom",
				ActionName = "PostXCustomSettings",
				Visible = true,
				RouteValues = new RouteValueDictionary { { "area", null } }
			};

			var subMenuItemCustomer = new SiteMapNode
			{
				SystemName = "Nop.Plugin.PostXCustom",
				Title = _localizationService.GetResource("PostXCustom.Manage.Customer"),
				ControllerName = "CompanyRepresentative",
				ActionName = "List",
				Visible = true,
				RouteValues = new RouteValueDictionary { { "area", null } }
			};

			var subMenuItemCompany = new SiteMapNode
			{
				SystemName = "Nop.Plugin.PostXCustom",
				Title = _localizationService.GetResource("PostXCustom.Manage.Company"),
				ControllerName = "AdminCustomer",
				ActionName = "CompanyList",
				Visible = true,
				RouteValues = new RouteValueDictionary { { "area", null } }
			};

			var subMenuItemVatReport = new SiteMapNode
			{
				SystemName = "Nop.Plugin.PostXCustom",
				Title = _localizationService.GetResource("PostXCustom.Manage.VatReport"),
				ControllerName = "VatReport",
				ActionName = "List",
				Visible = true,
				RouteValues = new RouteValueDictionary { { "area", null } }
			};


			var subMenuItemLoyaltyReport = new SiteMapNode
			{
				SystemName = "Nop.Plugin.PostXCustom",
				Title = _localizationService.GetResource("PostXCustom.Manage.LoyaltyReport"),
				ControllerName = "LoyaltyReport",
				ActionName = "List",
				Visible = true,
				RouteValues = new RouteValueDictionary { { "area", null } }
			};

			var subMenuItemLoyaltyReportDatabase = new SiteMapNode
			{
				SystemName = "Nop.Plugin.PostXCustom",
				Title = _localizationService.GetResource("PostXCustom.Manage.LoyaltyReportDB"),
				ControllerName = "LoyaltyReport",
				ActionName = "LoyaltyList",
				Visible = true,
				RouteValues = new RouteValueDictionary { { "area", null } }
			};



			rootNode.ChildNodes.Add(menuItem);
			menuItem.ChildNodes.Add(subMenuItemCompany);
			menuItem.ChildNodes.Add(subMenuItemCustomer);
			menuItem.ChildNodes.Add(subMenuItemSetting);
			menuItem.ChildNodes.Add(subMenuItemVatReport);
			menuItem.ChildNodes.Add(subMenuItemLoyaltyReport);
			menuItem.ChildNodes.Add(subMenuItemLoyaltyReportDatabase);

		}
															  
		public void HandleEvent(AdminTabStripCreated eventMessage)
		{
			if (eventMessage.TabStripName == "customer-edit")
			{

				var customerId = Convert.ToInt32(System.Web.HttpContext.Current.Request.RequestContext.RouteData.Values["id"].ToString());
				const string actionName = "AssignCompanyRepresentativeTab";
				const string controllerName = "CompanyRepresentative";
				var routeValues = new RouteValueDictionary
				{
												  {"area", null},
												  {"customerId", customerId}
											 };
				var urlHelper = new UrlHelper(eventMessage.Helper.ViewContext.RequestContext).Action(actionName, controllerName, routeValues);

				const string tabName = "companyrepresentative";

				eventMessage.BlocksToRender.Add(new MvcHtmlString(
					"<script>"
					+ "$(document).ready(function() {"
					+ "$(\"<li><a data-tab-name='tab-" + tabName + "' data-toggle='tab' href='#tab-" + tabName + "'>"
					+ _localizationService.GetResource("PostXCustom.CompanyRepresentativeTab")
					+ "</a></li> \").appendTo('#customer-edit .nav-tabs:first');"
					+ "$.get('" + urlHelper + "', function(result) {"
					+ "$(\" <div class='tab-pane' id='tab-" + tabName + "'>\" + result + \"</div>\").appendTo('#customer-edit .tab-content:first');"
					+ "});"
					+ "});"
					+ "</script>"));
			}
		}

		//Customization company representative 
		public CustomerRole CompanyRepresentativeRole(string name, string system)
		{
			var customerRole = new CustomerRole
			{
				Name = name,
				FreeShipping = false,
				TaxExempt = false,
				Active = true,
				IsSystemRole = false,
				SystemName = system,
				PurchasedWithProductId = 0
			};
			return customerRole;
		}

		public void EnablePermissions()
		{
			var customerRoles = _customerService.GetAllCustomerRoles(true);
			foreach (var cr in customerRoles)
			{																	
				if (cr.SystemName == "CompanyRepresentative")
				{
					// get permission record AccessAdminPanel
					SetPermissions(cr, "AccessAdminPanel", true);
					break;
				}
			}
		}
		
		public void DisablePermissions()
		{																		
			var customerRoles = _customerService.GetAllCustomerRoles(true);
			foreach (var cr in customerRoles)
			{
				if (cr.SystemName == "CompanyRepresentative")
				{
					SetPermissions(cr, "AccessAdminPanel", false);
					break;
				}
			}
		}

		// Customization  company representative  
		public void SetPermissions(CustomerRole customerRole, string name, bool enable)
		{
			var permissionRecords = _permissionService.GetPermissionRecordBySystemName(name);

			if (enable)
			{
				if (permissionRecords.CustomerRoles.FirstOrDefault(x => x.Id == customerRole.Id) == null)
				{
					permissionRecords.CustomerRoles.Add(customerRole);
					_permissionService.UpdatePermissionRecord(permissionRecords);
				}
			}
			else
			{
				if (permissionRecords.CustomerRoles.FirstOrDefault(x => x.Id == customerRole.Id) != null)
				{
					permissionRecords.CustomerRoles.Remove(customerRole);
					_permissionService.UpdatePermissionRecord(permissionRecords);
				}
			}																
		}

		#endregion
	}														  
}															  