﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Logging;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.CustomService;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class LoyaltyReportController : BasePluginController
	{
		#region Fields

		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICustomCodeService _customCodeService;
		private readonly IRewardPointService _rewardPointService;
		private readonly IStoreContext _storeContext;
		private readonly ILogger _logger;
		private readonly IOrderTotalCalculationService _orderTotalCalculationService;
		private readonly ICustomerService _customerService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ILightSpeedReceiptsService _lightSpeedReceiptService;
		#endregion

		#region Ctor
		public LoyaltyReportController(ICmsNopCustomerMappingService cmsNopCustomerMappingService,ICustomCodeService customCodeService, 
			IRewardPointService rewardPointService, IStoreContext storeContext,ILogger logger,IOrderTotalCalculationService orderTotalCalculationService,
			ICustomerService customerService, IGenericAttributeService genericAttributeService, ILightSpeedReceiptsService lightSpeedReceiptService)
		{
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_customCodeService = customCodeService;
			_rewardPointService = rewardPointService;
			_storeContext = storeContext;
			_logger = logger;
			_orderTotalCalculationService = orderTotalCalculationService;
			_customerService = customerService;
			_genericAttributeService = genericAttributeService;
			_lightSpeedReceiptService = lightSpeedReceiptService;
		}
		#endregion

		#region LS Loyalty Report      

		public ActionResult List()
		{
			var model = new LoyaltyReportModelList {OrderDate = DateTime.Now.ToString("dd/MM/yyyy")};
			return View("~/Plugins/PostXCustom/Views/LoyaltyReport/List.cshtml", model);
		}

		[HttpPost]
		public virtual ActionResult GenerateLoyaltyReportList(DataSourceRequest command, LoyaltyReportModelList model1)
		{
			var model = new LoyaltyReportModelList();
			var offsetValue = 0;
			var orderDate = "";
			var fromDate = "";
			var toDate = "";

			if (model1.OrderDate != null)
				orderDate = Convert.ToDateTime(model1.OrderDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//model1.OrderDate.Value.ToString("yyyy-MM-dd");

			if (model1.OrderDate == null && model1.FromDate == null && model1.ToDate == null)
				orderDate = Convert.ToDateTime(model1.OrderDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//DateTime.Now.ToString("yyyy-MM-dd");

			if (model1.OrderDate == null && model1.FromDate != null && model1.ToDate != null)
			{
				fromDate = Convert.ToDateTime(model1.FromDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//model1.FromDate.Value.ToString("yyyy-MM-dd");
				toDate = Convert.ToDateTime(model1.ToDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//model1.ToDate.Value.ToString("yyyy-MM-dd");
			}
																														
			for (int count = 0; count < int.MaxValue; count++)
			{
				string url = @"https://euc1.posios.com/PosServer/rest/financial/receipt?";
				if (model1.OrderDate != null)
					url = url + "date=" + orderDate;

				if (model1.OrderDate == null && model1.FromDate == null && model1.ToDate == null)
					url = url + "date=" + orderDate;

				if (model1.OrderDate == null && model1.FromDate != null && model1.ToDate != null)
					url = url + "from=" + fromDate + "&to=" + toDate;

				if (!string.IsNullOrEmpty(model1.CustomerId))
					url = url + "&customerId=" + model1.CustomerId;

				if (!string.IsNullOrEmpty(model1.ReceiptId))
					url = url + "&receiptId=" + model1.ReceiptId;

				if (offsetValue > 0)
					url = url + "&offset=" + offsetValue;							

				var token = GetToken();

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.AutomaticDecompression = DecompressionMethods.GZip;

				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				using (Stream stream = response.GetResponseStream())
				using (StreamReader reader = new StreamReader(stream))
				{
					var html = reader.ReadToEnd();
					var jsonList = JsonConvert.DeserializeObject<CmsReceiptsListModel>(html);
					if (!jsonList.results.Any()) { break; }

					offsetValue = offsetValue + 50;

					foreach (var item in jsonList.results)
					{
						var receiptRecord = _lightSpeedReceiptService.GetByLightSpeedReceiptId(item.id);

						bool loyaltyProcessed = false;

						var reportItem = new LoyaltyReportModel
						{
							LightSpeedCustomerId = item.customerId,
							ReceiptCreateDate = Convert.ToDateTime(item.creationDate),
							OrderTotal = item.total,
							ReceiptId = item.id,
							LoyaltyPoints = 0
						};
																														
						if (receiptRecord != null)
						{								
							reportItem.VAT6Total = receiptRecord.VAT6Total;
							reportItem.VAT12Total = receiptRecord.VAT12Total;
							reportItem.VAT21Total = receiptRecord.VAT21Total;
							reportItem.VAT6OrderTotal = receiptRecord.VAT6OrderTotal;
							reportItem.VAT12OrderTotal = receiptRecord.VAT12OrderTotal;
							reportItem.VAT21OrderTotal = receiptRecord.VAT21OrderTotal;
							reportItem.OrderTotalCash = receiptRecord.OrderTotalCash;
							reportItem.OrderTotalNonCash = receiptRecord.OrderTotalNonCash;
							reportItem.VatRegime = receiptRecord.VatRegime;
							reportItem.NopUsername = receiptRecord.NopUsername;
							reportItem.NopCustomerId = receiptRecord.NopCustomerId;
							reportItem.LoyaltyPercentage = receiptRecord.LoyaltyPercentage;
							reportItem.LoyaltyPoints = receiptRecord.LoyaltyPoints;
							model.LoyaltyDataList.Add(reportItem);

							//Check if loyalty processed or not
							if (!receiptRecord.LoyaltyProcessed)
							{
								if (receiptRecord.NopCustomerId > 0)
								{
									var cmsNopCustomerMapping = _cmsNopCustomerMappingService.GetByCmsCustomerId(item.customerId);
									var calculatedPoints = CalculcateLoyaltyPoints(receiptRecord.OrderTotalNonCash, receiptRecord.NopCustomerId, item.id, cmsNopCustomerMapping, ref loyaltyProcessed);
									if (loyaltyProcessed) {
										receiptRecord.LoyaltyProcessed = true;
										receiptRecord.LoyaltyProcessedOnDate = DateTime.UtcNow;
									}
								}
								else
								{
									receiptRecord.LoyaltyProcessed = true;
									receiptRecord.LoyaltyProcessedOnDate = DateTime.UtcNow;
								}
								_lightSpeedReceiptService.Update(receiptRecord);
							}																					 

							continue;									  
						}	

						var total6Percent = 0M;
						var total12Percent = 0M;
						var total21Percent = 0M;	 
						var orderTotal6Percent = 0M;
						var orderTotal12Percent = 0M;
						var orderTotal21Percent = 0M;

						foreach (var orderItem in item.items)
						{
							if (orderItem.vatPercentage == 6)
							{
								total6Percent = total6Percent + (orderItem.totalPrice - orderItem.totalPriceWithoutVat);
								orderTotal6Percent = orderTotal6Percent + orderItem.totalPrice;
							}

							if (orderItem.vatPercentage == 12)
							{
								total12Percent = total12Percent + (orderItem.totalPrice - orderItem.totalPriceWithoutVat);
								orderTotal12Percent = orderTotal12Percent + orderItem.totalPrice;
							}

							if (orderItem.vatPercentage == 21)
							{
								total21Percent = total21Percent + (orderItem.totalPrice - orderItem.totalPriceWithoutVat);
								orderTotal21Percent = orderTotal21Percent + orderItem.totalPrice;
							}
						}
						reportItem.VAT6Total = total6Percent;
						reportItem.VAT12Total = total12Percent;
						reportItem.VAT21Total = total21Percent;
						reportItem.VAT6OrderTotal = orderTotal6Percent;
						reportItem.VAT12OrderTotal = orderTotal12Percent;
						reportItem.VAT21OrderTotal = orderTotal21Percent;
																									  
						var cashPayment = 0M;
						var nonCashPayment = 0M;											  

						foreach (var paymentItem in item.payments)
						{
							if (paymentItem.paymentTypeId == 36191)							
								cashPayment = cashPayment + paymentItem.amount;							
							else							
								nonCashPayment = nonCashPayment + paymentItem.amount;							
						}

						reportItem.OrderTotalCash = cashPayment;
						reportItem.OrderTotalNonCash = nonCashPayment;				  
						
						var cmsNopCustomer = _cmsNopCustomerMappingService.GetByCmsCustomerId(item.customerId);
						if (cmsNopCustomer != null)
						{
							var nopCustomer = _customerService.GetCustomerById(cmsNopCustomer.NopCustomerId);
							if (nopCustomer != null)
							{
								var attributeList = _genericAttributeService.GetAttributesForEntity(nopCustomer.Id, "Customer");
								var loyalityPercentage = GetAttributeValue(attributeList.ToList(), "LoyalityPercentage");
								var vatRegimeId = GetAttributeValue(attributeList.ToList(), "VatRegimeId");

								reportItem.VatRegime = !string.IsNullOrEmpty(vatRegimeId) ? ((VatRegimeType)Convert.ToInt32(vatRegimeId)).ToString() : "";
								reportItem.NopUsername = nopCustomer.Username;
								reportItem.NopCustomerId = cmsNopCustomer.NopCustomerId;
								reportItem.LoyaltyPercentage = loyalityPercentage;
								reportItem.LoyaltyPoints = CalculcateLoyaltyPoints(nonCashPayment, cmsNopCustomer.NopCustomerId, item.id, cmsNopCustomer, ref loyaltyProcessed);
							}
						}
						else
							loyaltyProcessed = true;							 
															
						receiptRecord = new LightSpeedReceiptsMapping
						{
							ImportedOnDate = DateTime.UtcNow,
							LightSpeedCustomerId = item.customerId,
							ReceiptCreateDate = Convert.ToDateTime(item.creationDate),
							OrderTotal = item.total,
							ReceiptId = item.id,
							LoyaltyPoints = reportItem.LoyaltyPoints,
							NopCustomerId = reportItem.NopCustomerId,
							OrderTotalCash = reportItem.OrderTotalCash,
							OrderTotalNonCash = reportItem.OrderTotalNonCash,
							LoyaltyPercentage = reportItem.LoyaltyPercentage,
							NopUsername = reportItem.NopUsername,
							VatRegime = reportItem.VatRegime,
							VAT6Total = total6Percent,
							VAT12Total = total12Percent,
							VAT21Total = total21Percent,
							VAT6OrderTotal = orderTotal6Percent,
							VAT12OrderTotal = orderTotal12Percent,
							VAT21OrderTotal = orderTotal21Percent,
							LoyaltyProcessed = loyaltyProcessed
						};

						if (loyaltyProcessed)
							receiptRecord.LoyaltyProcessedOnDate = DateTime.UtcNow;
						_lightSpeedReceiptService.Insert(receiptRecord);	 

						model.LoyaltyDataList.Add(reportItem);
					}
				}
			}												 

			var gridModel = new DataSourceResult
			{
				Data = model.LoyaltyDataList.Any() ? model.LoyaltyDataList.OrderByDescending(x => x.ReceiptCreateDate).ToList() : model.LoyaltyDataList,
				Total = model.LoyaltyDataList.Count
			};
			return Json(gridModel);
		}																					 

		public string GetToken()
		{
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			var token = json.GetValue("token").Value<string>();
			return token;
		}

		public void UpdateCustomerLoyaltyPointsOnLightSpeed(int cmsOrderId, CmsCustomerNopCustomerMapping checkCmsCustomer, Customer customer, ref bool loyaltyProcessed)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerLoyaltyPointsOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + customer.Email + "\",");

				var loyaltyPoints = "0 reward points available.";
				var customerRewardPointsBalance = _rewardPointService.GetRewardPointsBalance(customer.Id, _storeContext.CurrentStore.Id);
				if (customerRewardPointsBalance > 0)
					loyaltyPoints = customerRewardPointsBalance + " reward points available.";

				postData.Append("\"firstName\":\"" + loyaltyPoints + "\",");
				postData.Append("\"lastName\":\"" + customer.GetFullName() + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
					loyaltyProcessed = true;
				}

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating loyalty points for customer on Light Speed CMS :: LS ReceiptId :: " + cmsOrderId + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message + "  ::  " + exception.StackTrace);
				_customCodeService.SendErrorEmail("Error updating loyalty points for customer on Light Speed CMS :: LS ReceiptId :: " + cmsOrderId + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message, exception.StackTrace);
				loyaltyProcessed = false;
			}
		}


		public string GetAttributeValue(List<GenericAttribute> genericAttributes, string attributeToCheck)
		{
			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeToCheck);
			if (genericAttribute == null)
				return null;

			return genericAttribute.Value;
		}


		public int CalculcateLoyaltyPoints(decimal orderTotal, int nopCustomerId, int receiptId, CmsCustomerNopCustomerMapping checkCmsCustomer, ref bool loyaltyProcessed)
		{
			int loyaltyPoints = 0;
			var nopCustomer = _customerService.GetCustomerById(nopCustomerId);
			if (nopCustomer != null)
			{
				loyaltyPoints = _orderTotalCalculationService.CalculateRewardPoints(nopCustomer, orderTotal);

				if (loyaltyPoints > 0)
				{
					UpdateCustomerLoyaltyPointsOnLightSpeed(receiptId, checkCmsCustomer, nopCustomer, ref loyaltyProcessed);//Updates the loyalty on LS
					if (loyaltyProcessed)
						_rewardPointService.AddRewardPointsHistoryEntry(nopCustomer, loyaltyPoints, _storeContext.CurrentStore.Id, "Loyalty Points for LS ReceiptId #" + receiptId);
				}

			}
			return loyaltyPoints;
		}

		#endregion


		#region Nop Customer Loyalty Page

		public ActionResult LoyaltyList()
		{
			var model = new LightSpeedReceiptsListModel {OrderDate = DateTime.Now.ToString("dd/MM/yyyy")};
			return View("~/Plugins/PostXCustom/Views/LoyaltyReport/LoyaltyList.cshtml", model);
		}

		[HttpPost]
		public virtual ActionResult GetLoyaltyList(DataSourceRequest command, LightSpeedReceiptsListModel model1)
		{
			var model = new LightSpeedReceiptsListModel();
			DateTime orderDate = DateTime.Now;
			DateTime? toDate = null;
			int receiptId = 0;

			int filterNopCustomerId = 0;

			if (!string.IsNullOrEmpty(model1.NopUserName))
			{
				var nopCustomer = _customerService.GetCustomerByUsername(model1.NopUserName);
				if (nopCustomer != null)
					filterNopCustomerId = nopCustomer.Id;
			}

			if (model1.OrderDate != null)
				orderDate = Convert.ToDateTime(model1.OrderDate, CultureInfo.GetCultureInfo("nl-BE"));

			if (model1.OrderDate == null && model1.FromDate == null && model1.ToDate == null)
				orderDate = Convert.ToDateTime(model1.OrderDate, CultureInfo.GetCultureInfo("nl-BE"));

			if (model1.OrderDate == null && model1.FromDate != null && model1.ToDate != null)
			{
				orderDate = Convert.ToDateTime(model1.FromDate, CultureInfo.GetCultureInfo("nl-BE"));
				toDate = Convert.ToDateTime(model1.ToDate, CultureInfo.GetCultureInfo("nl-BE"));
			}

			if (model1.ReceiptId > 0)
				receiptId = Convert.ToInt32(model1.ReceiptId);

			var receiptDataList = _lightSpeedReceiptService.GetAllReceiptsFilter(orderDate, toDate, model1.LightSpeedCustomerId, model1.ReceiptId, filterNopCustomerId);

			foreach (var item in receiptDataList)
			{
				var reportItem = new LightSpeedReceiptsModel
				{
					LightSpeedCustomerId = item.LightSpeedCustomerId,
					ReceiptCreateDate = item.ReceiptCreateDate,
					OrderTotal = item.OrderTotal,
					ReceiptId = item.ReceiptId,
					LoyaltyPoints = item.LoyaltyPoints,
					OrderTotalCash = item.OrderTotalCash,
					OrderTotalNonCash = item.OrderTotalNonCash,
					LoyaltyPercentage = item.LoyaltyPercentage,
					NopCustomerId = item.NopCustomerId,
					NopUserName = item.NopUsername,
					LoyaltyProcessedOnDate = item.LoyaltyProcessedOnDate,
					VAT12OrderTotal = item.VAT12OrderTotal,
					VAT12Total = item.VAT12Total,
					VAT21OrderTotal = item.VAT21OrderTotal,
					VAT21Total = item.VAT21Total,
					VAT6OrderTotal = item.VAT6OrderTotal,
					VAT6Total = item.VAT6Total,
					VatRegime = item.VatRegime,
					ImportedOnDate = item.ImportedOnDate
				};

				model.LoyaltyDataList.Add(reportItem);
			}	  

			var gridModel = new DataSourceResult
			{
				Data = model.LoyaltyDataList.Any() ? model.LoyaltyDataList.OrderByDescending(x => x.ReceiptCreateDate).ToList() : model.LoyaltyDataList,
				Total = model.LoyaltyDataList.Count
			};
			return Json(gridModel);
		}
		#endregion
	}
}
