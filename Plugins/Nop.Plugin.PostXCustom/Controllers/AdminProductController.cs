﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Infrastructure.Cache;
using Nop.Admin.Models.Catalog;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Vendors;
using Nop.Services;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Date;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Plugin.PostXCustom.Helpers;
using Nop.Plugin.PostXCustom.Services;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class AdminProductController : BasePluginController
	{
		#region Fields

		private readonly IProductService _productService;
		private readonly IProductTemplateService _productTemplateService;
		private readonly ICategoryService _categoryService;
		private readonly IManufacturerService _manufacturerService;
		private readonly ICustomerService _customerService;
		private readonly IUrlRecordService _urlRecordService;
		private readonly IWorkContext _workContext;
		private readonly ILanguageService _languageService;
		private readonly ILocalizationService _localizationService;
		private readonly ILocalizedEntityService _localizedEntityService;
		private readonly ISpecificationAttributeService _specificationAttributeService;
		private readonly IPictureService _pictureService;
		private readonly ITaxCategoryService _taxCategoryService;
		private readonly IProductTagService _productTagService;
		private readonly ICopyProductService _copyProductService;
		private readonly IPdfService _pdfService;
		private readonly IExportManager _exportManager;
		private readonly IImportManager _importManager;
		private readonly ICustomerActivityService _customerActivityService;
		private readonly IPermissionService _permissionService;
		private readonly IAclService _aclService;
		private readonly IStoreService _storeService;
		private readonly IOrderService _orderService;
		private readonly IStoreMappingService _storeMappingService;
		private readonly IVendorService _vendorService;
		private readonly IDateRangeService _dateRangeService;
		private readonly IShippingService _shippingService;
		private readonly IShipmentService _shipmentService;
		private readonly ICurrencyService _currencyService;
		private readonly CurrencySettings _currencySettings;
		private readonly IMeasureService _measureService;
		private readonly MeasureSettings _measureSettings;
		private readonly ICacheManager _cacheManager;
		private readonly IDateTimeHelper _dateTimeHelper;
		private readonly IDiscountService _discountService;
		private readonly IProductAttributeService _productAttributeService;
		private readonly IBackInStockSubscriptionService _backInStockSubscriptionService;
		private readonly IShoppingCartService _shoppingCartService;
		private readonly IProductAttributeFormatter _productAttributeFormatter;
		private readonly IProductAttributeParser _productAttributeParser;
		private readonly IDownloadService _downloadService;
		private readonly ISettingService _settingService;
		private readonly TaxSettings _taxSettings;
		private readonly VendorSettings _vendorSettings;
		private readonly ILogger _logger;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly ICustomProductService _customProductService;

		#endregion

		#region Constructors

		public AdminProductController(IProductService productService,
			 IProductTemplateService productTemplateService,
			 ICategoryService categoryService,
			 IManufacturerService manufacturerService,
			 ICustomerService customerService,
			 IUrlRecordService urlRecordService,
			 IWorkContext workContext,
			 ILanguageService languageService,
			 ILocalizationService localizationService,
			 ILocalizedEntityService localizedEntityService,
			 ISpecificationAttributeService specificationAttributeService,
			 IPictureService pictureService,
			 ITaxCategoryService taxCategoryService,
			 IProductTagService productTagService,
			 ICopyProductService copyProductService,
			 IPdfService pdfService,
			 IExportManager exportManager,
			 IImportManager importManager,
			 ICustomerActivityService customerActivityService,
			 IPermissionService permissionService,
			 IAclService aclService,
			 IStoreService storeService,
			 IOrderService orderService,
			 IStoreMappingService storeMappingService,
			 IVendorService vendorService,
			 IDateRangeService dateRangeService,
			 IShippingService shippingService,
			 IShipmentService shipmentService,
			 ICurrencyService currencyService,
			 CurrencySettings currencySettings,
			 IMeasureService measureService,
			 MeasureSettings measureSettings,
			 ICacheManager cacheManager,
			 IDateTimeHelper dateTimeHelper,
			 IDiscountService discountService,
			 IProductAttributeService productAttributeService,
			 IBackInStockSubscriptionService backInStockSubscriptionService,
			 IShoppingCartService shoppingCartService,
			 IProductAttributeFormatter productAttributeFormatter,
			 IProductAttributeParser productAttributeParser,
			 IDownloadService downloadService,
			 ISettingService settingService,
			 TaxSettings taxSettings,
			 VendorSettings vendorSettings, ILogger logger, ICmsNopProductMappingService cmsNopProductMappingService,
			ICustomProductService customProductService)
		{
			this._productService = productService;
			this._productTemplateService = productTemplateService;
			this._categoryService = categoryService;
			this._manufacturerService = manufacturerService;
			this._customerService = customerService;
			this._urlRecordService = urlRecordService;
			this._workContext = workContext;
			this._languageService = languageService;
			this._localizationService = localizationService;
			this._localizedEntityService = localizedEntityService;
			this._specificationAttributeService = specificationAttributeService;
			this._pictureService = pictureService;
			this._taxCategoryService = taxCategoryService;
			this._productTagService = productTagService;
			this._copyProductService = copyProductService;
			this._pdfService = pdfService;
			this._exportManager = exportManager;
			this._importManager = importManager;
			this._customerActivityService = customerActivityService;
			this._permissionService = permissionService;
			this._aclService = aclService;
			this._storeService = storeService;
			this._orderService = orderService;
			this._storeMappingService = storeMappingService;
			this._vendorService = vendorService;
			this._dateRangeService = dateRangeService;
			this._shippingService = shippingService;
			this._shipmentService = shipmentService;
			this._currencyService = currencyService;
			this._currencySettings = currencySettings;
			this._measureService = measureService;
			this._measureSettings = measureSettings;
			this._cacheManager = cacheManager;
			this._dateTimeHelper = dateTimeHelper;
			this._discountService = discountService;
			this._productAttributeService = productAttributeService;
			this._backInStockSubscriptionService = backInStockSubscriptionService;
			this._shoppingCartService = shoppingCartService;
			this._productAttributeFormatter = productAttributeFormatter;
			this._productAttributeParser = productAttributeParser;
			this._downloadService = downloadService;
			this._settingService = settingService;
			this._taxSettings = taxSettings;
			this._vendorSettings = vendorSettings;

			_logger = logger;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_customProductService = customProductService;
		}

		#endregion

		#region Utilities

		//[NonAction]
		//protected virtual void UpdateLocales(Product product, ProductModel model)
		//{
		//	foreach (var localized in model.Locales)
		//	{
		//		_localizedEntityService.SaveLocalizedValue(product,
		//																	  x => x.Name,
		//																	  localized.Name,
		//																	  localized.LanguageId);
		//		_localizedEntityService.SaveLocalizedValue(product,
		//																	  x => x.ShortDescription,
		//																	  localized.ShortDescription,
		//																	  localized.LanguageId);
		//		_localizedEntityService.SaveLocalizedValue(product,
		//																	  x => x.FullDescription,
		//																	  localized.FullDescription,
		//																	  localized.LanguageId);
		//		_localizedEntityService.SaveLocalizedValue(product,
		//																	  x => x.MetaKeywords,
		//																	  localized.MetaKeywords,
		//																	  localized.LanguageId);
		//		_localizedEntityService.SaveLocalizedValue(product,
		//																	  x => x.MetaDescription,
		//																	  localized.MetaDescription,
		//																	  localized.LanguageId);
		//		_localizedEntityService.SaveLocalizedValue(product,
		//																	  x => x.MetaTitle,
		//																	  localized.MetaTitle,
		//																	  localized.LanguageId);

		//		//search engine name
		//		var seName = product.ValidateSeName(localized.SeName, localized.Name, false);
		//		_urlRecordService.SaveSlug(product, seName, localized.LanguageId);
		//	}
		//}

		[NonAction]
		protected virtual void UpdatePictureSeoNames(Product product)
		{
			foreach (var pp in product.ProductPictures)
				_pictureService.SetSeoFilename(pp.PictureId, _pictureService.GetPictureSeName(product.Name));
		}


		[NonAction]
		protected virtual List<int> GetChildCategoryIds(int parentCategoryId)
		{
			var categoriesIds = new List<int>();
			var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, true);
			foreach (var category in categories)
			{
				categoriesIds.Add(category.Id);
				categoriesIds.AddRange(GetChildCategoryIds(category.Id));
			}
			return categoriesIds;
		}





		#endregion

		#region Methods

		public virtual ActionResult AccessDeniedJson(string errorMessage)
		{
			var gridModel = new DataSourceResult
			{
				Errors = errorMessage
			};

			return Json(gridModel);
		}

		public virtual ActionResult AccessDenied(string pageUrl)
		{
			var currentCustomer = _workContext.CurrentCustomer;
			if (currentCustomer == null || currentCustomer.IsGuest())
			{
				_logger.Information(string.Format("Access denied to anonymous request on {0}", pageUrl));
				return View("~/Plugins/PostXCustom/Views/Security/AccessDenied.cshtml");
			}
			_logger.Information(string.Format("Access denied to user #{0} '{1}' on {2}", currentCustomer.Email, currentCustomer.Email, pageUrl));
			return View("~/Plugins/PostXCustom/Views/Security/AccessDenied.cshtml");
		}


		#region Product list / create / edit / delete

		//list products
		public virtual ActionResult Index()
		{
			return RedirectToAction("List");
		}

		public virtual ActionResult List()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var model = new ProductListModel();
			//a vendor should have access only to his products
			model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;
			model.AllowVendorsToImportProducts = _vendorSettings.AllowVendorsToImportProducts;

			//categories
			model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
			var categories = SelectListHelper.GetCategoryList(_categoryService, _cacheManager, true);
			foreach (var c in categories)
				model.AvailableCategories.Add(c);

			//manufacturers
			model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
			var manufacturers = SelectListHelper.GetManufacturerList(_manufacturerService, _cacheManager, true);
			foreach (var m in manufacturers)
				model.AvailableManufacturers.Add(m);

			//stores
			model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
			foreach (var s in _storeService.GetAllStores())
				model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

			//warehouses
			model.AvailableWarehouses.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
			foreach (var wh in _shippingService.GetAllWarehouses())
				model.AvailableWarehouses.Add(new SelectListItem { Text = wh.Name, Value = wh.Id.ToString() });

			//vendors
			model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
			var vendors = SelectListHelper.GetVendorList(_vendorService, _cacheManager, true);
			foreach (var v in vendors)
				model.AvailableVendors.Add(v);

			//product types
			model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
			model.AvailableProductTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

			//"published" property
			//0 - all (according to "ShowHidden" parameter)
			//1 - published only
			//2 - unpublished only
			model.AvailablePublishedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Catalog.Products.List.SearchPublished.All"), Value = "0" });
			model.AvailablePublishedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Catalog.Products.List.SearchPublished.PublishedOnly"), Value = "1" });
			model.AvailablePublishedOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Catalog.Products.List.SearchPublished.UnpublishedOnly"), Value = "2" });


			//Companies List
			var companyList = _cmsNopProductMappingService.CompanyList();
			model.AvailableCompanyOptions.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
			foreach (var cl in companyList)
				model.AvailableCompanyOptions.Add(new SelectListItem { Text = cl, Value = cl });

			//return View(model);
			return View("~/Plugins/PostXCustom/Views/AdminProduct/List.cshtml", model);
		}

		[HttpPost]
		public virtual ActionResult ProductList(DataSourceRequest command, ProductListModel model)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
				return AccessDeniedJson(_localizationService.GetResource("Admin.AccessDenied.Description"));

			//a vendor should have access only to his products
			if (_workContext.CurrentVendor != null)
			{
				model.SearchVendorId = _workContext.CurrentVendor.Id;
			}

			var categoryIds = new List<int> { model.SearchCategoryId };
			//include subcategories
			if (model.SearchIncludeSubCategories && model.SearchCategoryId > 0)
				categoryIds.AddRange(GetChildCategoryIds(model.SearchCategoryId));

			//0 - all (according to "ShowHidden" parameter)
			//1 - published only
			//2 - unpublished only
			bool? overridePublished = null;
			if (model.SearchPublishedId == 1)
				overridePublished = true;
			else if (model.SearchPublishedId == 2)
				overridePublished = false;

			//var products = _productService.SearchProducts(
			var products = _customProductService.SearchProducts(
				 categoryIds: categoryIds,
				 manufacturerId: model.SearchManufacturerId,
				 storeId: model.SearchStoreId,
				 vendorId: model.SearchVendorId,
				 warehouseId: model.SearchWarehouseId,
				 productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
				 keywords: model.SearchProductName,
				 pageIndex: command.Page - 1,
				 pageSize: command.PageSize,
				 showHidden: true,
				 overridePublished: overridePublished,
				companyId: (model.SearchCompanyId != "0" ? model.SearchCompanyId : null)
			);
			var gridModel = new DataSourceResult();
			gridModel.Data = products.Select(x =>
			{
				var productModel = x.ToModel();
				//little performance optimization: ensure that "FullDescription" is not returned
				productModel.FullDescription = "";

				//picture
				var defaultProductPicture = _pictureService.GetPicturesByProductId(x.Id, 1).FirstOrDefault();
				productModel.PictureThumbnailUrl = _pictureService.GetPictureUrl(defaultProductPicture, 75, true);
				//product type
				productModel.ProductTypeName = x.ProductType.GetLocalizedEnum(_localizationService, _workContext);
				//friendly stock qantity
				//if a simple product AND "manage inventory" is "Track inventory", then display
				if (x.ProductType == ProductType.SimpleProduct && x.ManageInventoryMethod == ManageInventoryMethod.ManageStock)
					productModel.StockQuantityStr = x.GetTotalStockQuantity().ToString();
				return productModel;
			});
			gridModel.Total = products.TotalCount;

			return Json(gridModel);
		}

		//[HttpPost, ActionName("List")]
		//[FormValueRequired("go-to-product-by-sku")]
		//public virtual ActionResult GoToSku(ProductListModel model)
		//{
		//	string sku = model.GoDirectlyToSku;

		//	//try to load a product entity
		//	var product = _productService.GetProductBySku(sku);

		//	//if not found, then try to load a product attribute combination
		//	if (product == null)
		//	{
		//		var combination = _productAttributeService.GetProductAttributeCombinationBySku(sku);
		//		if (combination != null)
		//		{
		//			product = combination.Product;
		//		}
		//	}

		//	if (product != null)
		//		return RedirectToAction("Edit", "Product", new { id = product.Id });

		//	//not found
		//	return List();
		//}



		////delete product
		//[HttpPost]
		//public virtual ActionResult Delete(int id)
		//{
		//	if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
		//		return AccessDeniedView();

		//	var product = _productService.GetProductById(id);
		//	if (product == null)
		//		//No product found with the specified id
		//		return RedirectToAction("List");

		//	//a vendor should have access only to his products
		//	if (_workContext.CurrentVendor != null && product.VendorId != _workContext.CurrentVendor.Id)
		//		return RedirectToAction("List");

		//	_productService.DeleteProduct(product);

		//	//activity log
		//	_customerActivityService.InsertActivity("DeleteProduct", _localizationService.GetResource("ActivityLog.DeleteProduct"), product.Name);

		//	SuccessNotification(_localizationService.GetResource("Admin.Catalog.Products.Deleted"));
		//	return RedirectToAction("List");
		//}

		//[HttpPost]
		//public virtual ActionResult DeleteSelected(ICollection<int> selectedIds)
		//{
		//	if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
		//		return AccessDeniedView();

		//	if (selectedIds != null)
		//	{
		//		_productService.DeleteProducts(_productService.GetProductsByIds(selectedIds.ToArray()).Where(p => _workContext.CurrentVendor == null || p.VendorId == _workContext.CurrentVendor.Id).ToList());
		//	}

		//	return Json(new { Result = true });
		//}

		//[HttpPost]
		//public virtual ActionResult CopyProduct(ProductModel model)
		//{
		//	if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
		//		return AccessDeniedView();

		//	var copyModel = model.CopyProductModel;
		//	try
		//	{
		//		var originalProduct = _productService.GetProductById(copyModel.Id);

		//		//a vendor should have access only to his products
		//		if (_workContext.CurrentVendor != null && originalProduct.VendorId != _workContext.CurrentVendor.Id)
		//			return RedirectToAction("List");

		//		var newProduct = _copyProductService.CopyProduct(originalProduct,
		//			 copyModel.Name, copyModel.Published, copyModel.CopyImages);
		//		SuccessNotification(_localizationService.GetResource("Admin.Catalog.Products.Copied"));
		//		return RedirectToAction("Edit", new { id = newProduct.Id });
		//	}
		//	catch (Exception exc)
		//	{
		//		ErrorNotification(exc.Message);
		//		return RedirectToAction("Edit", new { id = copyModel.Id });
		//	}
		//}

		#endregion



		#region Export / Import

		[HttpPost, ActionName("List")]
		[FormValueRequired("download-catalog-pdf")]
		public virtual ActionResult DownloadCatalogAsPdf(ProductListModel model)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			//a vendor should have access only to his products
			if (_workContext.CurrentVendor != null)
			{
				model.SearchVendorId = _workContext.CurrentVendor.Id;
			}

			var categoryIds = new List<int> { model.SearchCategoryId };
			//include subcategories
			if (model.SearchIncludeSubCategories && model.SearchCategoryId > 0)
				categoryIds.AddRange(GetChildCategoryIds(model.SearchCategoryId));

			//0 - all (according to "ShowHidden" parameter)
			//1 - published only
			//2 - unpublished only
			bool? overridePublished = null;
			if (model.SearchPublishedId == 1)
				overridePublished = true;
			else if (model.SearchPublishedId == 2)
				overridePublished = false;

			var products = _productService.SearchProducts(
				 categoryIds: categoryIds,
				 manufacturerId: model.SearchManufacturerId,
				 storeId: model.SearchStoreId,
				 vendorId: model.SearchVendorId,
				 warehouseId: model.SearchWarehouseId,
				 productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
				 keywords: model.SearchProductName,
				 showHidden: true,
				 overridePublished: overridePublished
			);




			try
			{
				byte[] bytes;
				using (var stream = new MemoryStream())
				{
					_pdfService.PrintProductsToPdf(stream, products);
					bytes = stream.ToArray();
				}
				return File(bytes, MimeTypes.ApplicationPdf, "pdfcatalog.pdf");
			}
			catch (Exception exc)
			{
				ErrorNotification(exc);
				return RedirectToAction("List");
			}
		}


		[HttpPost, ActionName("List")]
		[FormValueRequired("exportxml-all")]
		public virtual ActionResult ExportXmlAll(ProductListModel model)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			//a vendor should have access only to his products
			if (_workContext.CurrentVendor != null)
			{
				model.SearchVendorId = _workContext.CurrentVendor.Id;
			}

			var categoryIds = new List<int> { model.SearchCategoryId };
			//include subcategories
			if (model.SearchIncludeSubCategories && model.SearchCategoryId > 0)
				categoryIds.AddRange(GetChildCategoryIds(model.SearchCategoryId));

			//0 - all (according to "ShowHidden" parameter)
			//1 - published only
			//2 - unpublished only
			bool? overridePublished = null;
			if (model.SearchPublishedId == 1)
				overridePublished = true;
			else if (model.SearchPublishedId == 2)
				overridePublished = false;

			var products = _productService.SearchProducts(
				 categoryIds: categoryIds,
				 manufacturerId: model.SearchManufacturerId,
				 storeId: model.SearchStoreId,
				 vendorId: model.SearchVendorId,
				 warehouseId: model.SearchWarehouseId,
				 productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
				 keywords: model.SearchProductName,
				 showHidden: true,
				 overridePublished: overridePublished
			);

			try
			{
				var xml = _exportManager.ExportProductsToXml(products);
				return new XmlDownloadResult(xml, "products.xml");
			}
			catch (Exception exc)
			{
				ErrorNotification(exc);
				return RedirectToAction("List");
			}
		}

		


		[HttpPost, ActionName("List")]
		[FormValueRequired("exportexcel-all")]
		public virtual ActionResult ExportExcelAll(ProductListModel model)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			//a vendor should have access only to his products
			if (_workContext.CurrentVendor != null)
			{
				model.SearchVendorId = _workContext.CurrentVendor.Id;
			}

			var categoryIds = new List<int> { model.SearchCategoryId };
			//include subcategories
			if (model.SearchIncludeSubCategories && model.SearchCategoryId > 0)
				categoryIds.AddRange(GetChildCategoryIds(model.SearchCategoryId));

			//0 - all (according to "ShowHidden" parameter)
			//1 - published only
			//2 - unpublished only
			bool? overridePublished = null;
			if (model.SearchPublishedId == 1)
				overridePublished = true;
			else if (model.SearchPublishedId == 2)
				overridePublished = false;

			var products = _productService.SearchProducts(
				 categoryIds: categoryIds,
				 manufacturerId: model.SearchManufacturerId,
				 storeId: model.SearchStoreId,
				 vendorId: model.SearchVendorId,
				 warehouseId: model.SearchWarehouseId,
				 productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
				 keywords: model.SearchProductName,
				 showHidden: true,
				 overridePublished: overridePublished
			);
			try
			{
				var bytes = _exportManager.ExportProductsToXlsx(products);

				return File(bytes, MimeTypes.TextXlsx, "products.xlsx");
			}
			catch (Exception exc)
			{
				ErrorNotification(exc);
				return RedirectToAction("List");
			}
		}

		

		[HttpPost]
		public virtual ActionResult ImportExcel()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			if (_workContext.CurrentVendor != null && !_vendorSettings.AllowVendorsToImportProducts)
				//a vendor can not import products
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			try
			{
				var file = Request.Files["importexcelfile"];
				if (file != null && file.ContentLength > 0)
				{
					_importManager.ImportProductsFromXlsx(file.InputStream);
				}
				else
				{
					ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
					return RedirectToAction("List");
				}
				SuccessNotification(_localizationService.GetResource("Admin.Catalog.Products.Imported"));
				return RedirectToAction("List");
			}
			catch (Exception exc)
			{
				ErrorNotification(exc);
				return RedirectToAction("List");
			}

		}

		#endregion


		#endregion




	}
}
