﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Messages;
using Nop.Plugin.PostXCustom.Helpers;
using Nop.Plugin.PostXCustom.Models.CompanyRepresentativeCustomer;
using Nop.Services.Affiliates;
using Nop.Services.Authentication.External;
using Nop.Services.Common;
using Nop.Services.Logging;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using Nop.Services.Customers;
using Nop.Services.Messages;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Customers;
using Nop.Services.Directory;
using Nop.Core;
using Nop.Services.Vendors;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class CustomerAPIController : BasePluginController
	{
		#region Fields

		private readonly ICustomerService _customerService;
		private readonly ILogger _logger;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
		private readonly ICustomerRegistrationService _customerRegistrationService;
		private readonly IDateTimeHelper _dateTimeHelper;
		private readonly ILocalizationService _localizationService;
		private readonly DateTimeSettings _dateTimeSettings;
		private readonly TaxSettings _taxSettings;
		private readonly RewardPointsSettings _rewardPointsSettings;
		private readonly ICountryService _countryService;
		private readonly IStateProvinceService _stateProvinceService;
		private readonly CustomerSettings _customerSettings;
		private readonly IWorkContext _workContext;
		private readonly IVendorService _vendorService;
		private readonly IStoreContext _storeContext;
		private readonly ICustomerActivityService _customerActivityService;
		private readonly IOpenAuthenticationService _openAuthenticationService;
		private readonly IStoreService _storeService;
		private readonly ICustomerAttributeParser _customerAttributeParser;
		private readonly ICustomerAttributeService _customerAttributeService;
		private readonly IAffiliateService _affiliateService;
		private readonly ICacheManager _cacheManager;		  
		#endregion

		#region Ctor

		public CustomerAPIController(ICustomerService customerService,ILogger logger,
			IGenericAttributeService genericAttributeService, INewsLetterSubscriptionService newsLetterSubscriptionService,
				ICustomerRegistrationService customerRegistrationService,
				IDateTimeHelper dateTimeHelper,
				ILocalizationService localizationService,
				DateTimeSettings dateTimeSettings,
				TaxSettings taxSettings,
				RewardPointsSettings rewardPointsSettings,
				ICountryService countryService,
				IStateProvinceService stateProvinceService,
				CustomerSettings customerSettings,
				IWorkContext workContext,
				IVendorService vendorService,
				IStoreContext storeContext,
			ICustomerActivityService customerActivityService,
				IOpenAuthenticationService openAuthenticationService,
				IStoreService storeService,
				ICustomerAttributeParser customerAttributeParser,
				ICustomerAttributeService customerAttributeService,
				IAffiliateService affiliateService,
				ICacheManager cacheManager)
		{
			_customerService = customerService;
			_logger = logger;
			_genericAttributeService = genericAttributeService;
			_newsLetterSubscriptionService = newsLetterSubscriptionService;
			_customerRegistrationService = customerRegistrationService;
			_dateTimeHelper = dateTimeHelper;
			_localizationService = localizationService;
			_dateTimeSettings = dateTimeSettings;
			_taxSettings = taxSettings;
			_rewardPointsSettings = rewardPointsSettings;
			_countryService = countryService;
			_stateProvinceService = stateProvinceService;
			_customerSettings = customerSettings;
			_workContext = workContext;
			_vendorService = vendorService;
			_storeContext = storeContext;

			_customerActivityService = customerActivityService;
			_openAuthenticationService = openAuthenticationService;
			_storeService = storeService;
			_customerAttributeParser = customerAttributeParser;
			_customerAttributeService = customerAttributeService;
			_affiliateService = affiliateService;
			_cacheManager = cacheManager;
		}

		#endregion


		#region Utilities

		[NonAction]
		protected virtual string GetCustomerRolesNames(IList<CustomerRole> customerRoles, string separator = ",")
		{
			var sb = new StringBuilder();
			for (var i = 0; i < customerRoles.Count; i++)
			{
				sb.Append(customerRoles[i].Name);
				if (i != customerRoles.Count - 1)
				{
					sb.Append(separator);
					sb.Append(" ");
				}
			}
			return sb.ToString();
		}

		[NonAction]
		protected virtual CustomerModel PrepareCustomerModelForList(Customer customer)
		{
			return new CustomerModel
			{
				Id = customer.Id,
				Email = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest"),
				Username = customer.Username,
				FullName = customer.GetFullName(),
				Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
				Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
				ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
				CustomerRoleNames = GetCustomerRolesNames(customer.CustomerRoles.ToList()),
				Active = customer.Active,
				CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc),
				LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc),
			};
		}

		[NonAction]
		protected virtual IList<CustomerModel.AssociatedExternalAuthModel> GetAssociatedExternalAuthRecords(Customer customer)
		{
			if (customer == null)
				throw new ArgumentNullException("customer");

			var result = new List<CustomerModel.AssociatedExternalAuthModel>();
			foreach (var record in _openAuthenticationService.GetExternalIdentifiersFor(customer))
			{
				var method = _openAuthenticationService.LoadExternalAuthenticationMethodBySystemName(record.ProviderSystemName);
				if (method == null)
					continue;

				result.Add(new CustomerModel.AssociatedExternalAuthModel
				{
					Id = record.Id,
					Email = record.Email,
					ExternalIdentifier = record.ExternalIdentifier,
					AuthMethodName = method.PluginDescriptor.FriendlyName
				});
			}

			return result;
		}

		[NonAction]
		protected virtual void PrepareVendorsModel(CustomerModel model)
		{
			if (model == null)
				throw new ArgumentNullException("model");

			model.AvailableVendors.Add(new SelectListItem
			{
				Text = _localizationService.GetResource("Admin.Customers.Customers.Fields.Vendor.None"),
				Value = "0"
			});
			var vendors = SelectListHelper.GetVendorList(_vendorService, _cacheManager, true);
			foreach (var v in vendors)
				model.AvailableVendors.Add(v);
		}

		[NonAction]
		protected virtual void PrepareCustomerModel(CustomerModel model, Customer customer, bool excludeProperties)
		{
			var allStores = _storeService.GetAllStores();
			if (customer != null)
			{
				model.Id = customer.Id;
				if (!excludeProperties)
				{
					model.Email = customer.Email;
					model.Username = customer.Username;
					model.VendorId = customer.VendorId;
					model.AdminComment = customer.AdminComment;
					model.IsTaxExempt = customer.IsTaxExempt;
					model.Active = customer.Active;

					if (customer.RegisteredInStoreId == 0 || allStores.All(s => s.Id != customer.RegisteredInStoreId))
						model.RegisteredInStore = string.Empty;
					else
						model.RegisteredInStore = allStores.First(s => s.Id == customer.RegisteredInStoreId).Name;

					var affiliate = _affiliateService.GetAffiliateById(customer.AffiliateId);
					if (affiliate != null)
					{
						model.AffiliateId = affiliate.Id;
						model.AffiliateName = affiliate.GetFullName();
					}

					model.TimeZoneId = customer.GetAttribute<string>(SystemCustomerAttributeNames.TimeZoneId);
					model.VatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);
					model.VatNumberStatusNote = ((VatNumberStatus)customer.GetAttribute<int>(SystemCustomerAttributeNames.VatNumberStatusId))
						 .GetLocalizedEnum(_localizationService, _workContext);
					model.CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc);
					model.LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc);
					model.LastIpAddress = customer.LastIpAddress;
					model.LastVisitedPage = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastVisitedPage);

					model.SelectedCustomerRoleIds = customer.CustomerRoles.Select(cr => cr.Id).ToList();

					//newsletter subscriptions
					if (!String.IsNullOrEmpty(customer.Email))
					{
						var newsletterSubscriptionStoreIds = new List<int>();
						foreach (var store in allStores)
						{
							var newsletterSubscription = _newsLetterSubscriptionService
								 .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
							if (newsletterSubscription != null)
								newsletterSubscriptionStoreIds.Add(store.Id);
							model.SelectedNewsletterSubscriptionStoreIds = newsletterSubscriptionStoreIds.ToArray();
						}
					}

					//form fields
					model.FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
					model.LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
					model.Gender = customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender);
					model.DateOfBirth = customer.GetAttribute<DateTime?>(SystemCustomerAttributeNames.DateOfBirth);
					model.Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company);
					model.StreetAddress = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress);
					model.StreetAddress2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2);
					model.ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode);
					model.City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City);
					model.CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId);
					model.StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId);
					model.Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
					model.Fax = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax);
				}
			}

			model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
			model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
			foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
				model.AvailableTimeZones.Add(new SelectListItem { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == model.TimeZoneId) });
			model.DisplayVatNumber = customer != null && _taxSettings.EuVatEnabled;

			//vendors
			PrepareVendorsModel(model);
			//customer attributes
			PrepareCustomerAttributeModel(model, customer);

			model.GenderEnabled = _customerSettings.GenderEnabled;
			model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
			model.CompanyEnabled = _customerSettings.CompanyEnabled;
			model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
			model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
			model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
			model.CityEnabled = _customerSettings.CityEnabled;
			model.CountryEnabled = _customerSettings.CountryEnabled;
			model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
			model.PhoneEnabled = _customerSettings.PhoneEnabled;
			model.FaxEnabled = _customerSettings.FaxEnabled;

			//countries and states
			if (_customerSettings.CountryEnabled)
			{
				model.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
				foreach (var c in _countryService.GetAllCountries(showHidden: true))
				{
					model.AvailableCountries.Add(new SelectListItem
					{
						Text = c.Name,
						Value = c.Id.ToString(CultureInfo.InvariantCulture),
						Selected = c.Id == model.CountryId
					});
				}

				if (_customerSettings.StateProvinceEnabled)
				{
					//states
					var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
					if (states.Any())
					{
						model.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectState"), Value = "0" });

						foreach (var s in states)
						{
							model.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(CultureInfo.InvariantCulture), Selected = (s.Id == model.StateProvinceId) });
						}
					}
					else
					{
						bool anyCountrySelected = model.AvailableCountries.Any(x => x.Selected);

						model.AvailableStates.Add(new SelectListItem
						{
							Text = _localizationService.GetResource(anyCountrySelected ? "Admin.Address.OtherNonUS" : "Admin.Address.SelectState"),
							Value = "0"
						});
					}
				}
			}

			//newsletter subscriptions
			model.AvailableNewsletterSubscriptionStores = allStores
				 .Select(s => new CustomerModel.StoreModel { Id = s.Id, Name = s.Name })
				 .ToList();

			//customer roles
			var allRoles = _customerService.GetAllCustomerRoles(true);
			var adminRole = allRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered);
			//precheck Registered Role as a default role while creating a new customer through admin
			if (customer == null && adminRole != null)
			{
				model.SelectedCustomerRoleIds.Add(adminRole.Id);
			}

			if (_workContext.CurrentCustomer.IsAdmin())
			{
				foreach (var role in allRoles)
				{
					model.AvailableCustomerRoles.Add(new SelectListItem
					{
						Text = role.Name,
						Value = role.Id.ToString(CultureInfo.InvariantCulture),
						Selected = model.SelectedCustomerRoleIds.Contains(role.Id)
					});
				}
			}
			else
			{
				foreach (var role in allRoles)
				{

					var companyName = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Company);

					if (role.SystemName.ToLower() == "registered" ||
						 (!string.IsNullOrEmpty(companyName) && companyName.ToLower() == role.Name.ToLower()))
					{
						model.AvailableCustomerRoles.Add(new SelectListItem
						{
							Text = role.Name,
							Value = role.Id.ToString(CultureInfo.InvariantCulture),
							Selected = model.SelectedCustomerRoleIds.Contains(role.Id)
						});
					}
				}
			}

			//reward points history
			if (customer != null)
			{
				model.DisplayRewardPointsHistory = _rewardPointsSettings.Enabled;
				model.AddRewardPointsValue = 0;
				model.AddRewardPointsMessage = "Some comment here...";

				//stores
				foreach (var store in allStores)
				{
					model.RewardPointsAvailableStores.Add(new SelectListItem
					{
						Text = store.Name,
						Value = store.Id.ToString(CultureInfo.InvariantCulture),
						Selected = (store.Id == _storeContext.CurrentStore.Id)
					});
				}
			}
			else
			{
				model.DisplayRewardPointsHistory = false;
			}
			//external authentication records
			if (customer != null)
			{
				model.AssociatedExternalAuthRecords = GetAssociatedExternalAuthRecords(customer);
			}
			//sending of the welcome message:
			//1. "admin approval" registration method
			//2. already created customer
			//3. registered
			model.AllowSendingOfWelcomeMessage = _customerSettings.UserRegistrationType == UserRegistrationType.AdminApproval &&
				 customer != null &&
				 customer.IsRegistered();
			//sending of the activation message
			//1. "email validation" registration method
			//2. already created customer
			//3. registered
			//4. not active
			model.AllowReSendingOfActivationMessage = _customerSettings.UserRegistrationType == UserRegistrationType.EmailValidation &&
				 customer != null &&
				 customer.IsRegistered() &&
				 !customer.Active;
		}

		

		[NonAction]
		protected virtual string ValidateCustomerRoles(IList<CustomerRole> customerRoles)
		{
			if (customerRoles == null)
				throw new ArgumentNullException("customerRoles");

			//ensure a customer is not added to both 'Guests' and 'Registered' customer roles
			//ensure that a customer is in at least one required role ('Guests' and 'Registered')
			bool isInGuestsRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Guests) != null;
			bool isInRegisteredRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Registered) != null;
			if (isInGuestsRole && isInRegisteredRole)
				return _localizationService.GetResource("Admin.Customers.Customers.GuestsAndRegisteredRolesError");
			if (!isInGuestsRole && !isInRegisteredRole)
				return _localizationService.GetResource("Admin.Customers.Customers.AddCustomerToGuestsOrRegisteredRoleError");

			//no errors
			return "";
		}

		[NonAction]
		protected virtual void PrepareCustomerAttributeModel(CustomerModel model, Customer customer)
		{
			var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
			foreach (var attribute in customerAttributes)
			{
				var attributeModel = new CustomerModel.CustomerAttributeModel
				{
					Id = attribute.Id,
					Name = attribute.Name,
					IsRequired = attribute.IsRequired,
					AttributeControlType = attribute.AttributeControlType,
				};

				if (attribute.ShouldHaveValues())
				{
					//values
					var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
					foreach (var attributeValue in attributeValues)
					{
						var attributeValueModel = new CustomerModel.CustomerAttributeValueModel
						{
							Id = attributeValue.Id,
							Name = attributeValue.Name,
							IsPreSelected = attributeValue.IsPreSelected
						};
						attributeModel.Values.Add(attributeValueModel);
					}
				}


				//set already selected attributes
				if (customer != null)
				{
					var selectedCustomerAttributes = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes, _genericAttributeService);
					switch (attribute.AttributeControlType)
					{
						case AttributeControlType.DropdownList:
						case AttributeControlType.RadioList:
						case AttributeControlType.Checkboxes:
							{
								if (!String.IsNullOrEmpty(selectedCustomerAttributes))
								{
									//clear default selection
									foreach (var item in attributeModel.Values)
										item.IsPreSelected = false;

									//select new values
									var selectedValues = _customerAttributeParser.ParseCustomerAttributeValues(selectedCustomerAttributes);
									foreach (var attributeValue in selectedValues)
										foreach (var item in attributeModel.Values)
											if (attributeValue.Id == item.Id)
												item.IsPreSelected = true;
								}
							}
							break;
						case AttributeControlType.ReadonlyCheckboxes:
							{
								//do nothing
								//values are already pre-set
							}
							break;
						case AttributeControlType.TextBox:
						case AttributeControlType.MultilineTextbox:
							{
								if (!String.IsNullOrEmpty(selectedCustomerAttributes))
								{
									var enteredText = _customerAttributeParser.ParseValues(selectedCustomerAttributes, attribute.Id);
									if (enteredText.Any())
										attributeModel.DefaultValue = enteredText[0];
								}
							}
							break;
						case AttributeControlType.Datepicker:
						case AttributeControlType.ColorSquares:
						case AttributeControlType.ImageSquares:
						case AttributeControlType.FileUpload:
						default:
							//not supported attribute control types
							break;
					}
				}

				model.CustomerAttributes.Add(attributeModel);
			}
		}

		[NonAction]
		protected virtual string ParseCustomCustomerAttributes(FormCollection form)
		{
			if (form == null)
				throw new ArgumentNullException("form");

			string attributesXml = "";
			var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
			foreach (var attribute in customerAttributes)
			{
				string controlId = string.Format("customer_attribute_{0}", attribute.Id);
				switch (attribute.AttributeControlType)
				{
					case AttributeControlType.DropdownList:
					case AttributeControlType.RadioList:
						{
							var ctrlAttributes = form[controlId];
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								int selectedAttributeId = int.Parse(ctrlAttributes);
								if (selectedAttributeId > 0)
									attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
										 attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
							}
						}
						break;
					case AttributeControlType.Checkboxes:
						{
							var cblAttributes = form[controlId];
							if (!String.IsNullOrEmpty(cblAttributes))
							{
								foreach (var item in cblAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
								{
									int selectedAttributeId = int.Parse(item);
									if (selectedAttributeId > 0)
										attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
											 attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
								}
							}
						}
						break;
					case AttributeControlType.ReadonlyCheckboxes:
						{
							//load read-only (already server-side selected) values
							var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
							foreach (var selectedAttributeId in attributeValues
								 .Where(v => v.IsPreSelected)
								 .Select(v => v.Id)
								 .ToList())
							{
								attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
												attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
							}
						}
						break;
					case AttributeControlType.TextBox:
					case AttributeControlType.MultilineTextbox:
						{
							var ctrlAttributes = form[controlId];
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								string enteredText = ctrlAttributes.Trim();
								attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
									 attribute, enteredText);
							}
						}
						break;
					case AttributeControlType.Datepicker:
					case AttributeControlType.ColorSquares:
					case AttributeControlType.ImageSquares:
					case AttributeControlType.FileUpload:
					//not supported customer attributes
					default:
						break;
				}
			}

			return attributesXml;
		}

		[NonAction]
		private bool SecondAdminAccountExists(Customer customer)
		{
			var customers = _customerService.GetAllCustomers(customerRoleIds: new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Administrators).Id });

			return customers.Any(c => c.Active && c.Id != customer.Id);
		}


		//public virtual void AssignCompanyRepresentativeMapping(int recordId, int associatedCustomerId, int companyRepresentativeId)
		//{
		//	if (associatedCustomerId == 0)
		//	{
		//		return;
		//	}

		//	var salesPersonCustomerMap = _companyRepresentativeCustomerMappingService.GetByCustomerId(associatedCustomerId);

		//	if (salesPersonCustomerMap == null && companyRepresentativeId == 0)
		//		return;


		//	if (salesPersonCustomerMap == null && companyRepresentativeId > 0)
		//	{
		//		salesPersonCustomerMap = new CompanyRepresentativeCustomerMapping
		//		{
		//			AssociatedCustomerId = associatedCustomerId,
		//			CompanyRepresentativeId = companyRepresentativeId
		//		};
		//		_companyRepresentativeCustomerMappingService.Insert(salesPersonCustomerMap);
		//		return;
		//	}

		//	if (salesPersonCustomerMap != null && companyRepresentativeId == 0)
		//	{
		//		_companyRepresentativeCustomerMappingService.Delete(salesPersonCustomerMap);
		//		return;
		//	}

		//	if (salesPersonCustomerMap != null && companyRepresentativeId > 0)
		//	{
		//		salesPersonCustomerMap.CompanyRepresentativeId = companyRepresentativeId;
		//		_companyRepresentativeCustomerMappingService.Update(salesPersonCustomerMap);
		//	}
		//}


		#endregion


		#region Methods
		[HttpPost]
		public object Create(CustomerModel model)
		{
			if (!String.IsNullOrWhiteSpace(model.Email))
			{
				var cust2 = _customerService.GetCustomerByEmail(model.Email);
				if (cust2 != null)
					return Json(new { success = false, message = "Email is already registered" }, JsonRequestBehavior.AllowGet);
			}
			if (!String.IsNullOrWhiteSpace(model.Username) & _customerSettings.UsernamesEnabled)
			{
				var cust2 = _customerService.GetCustomerByUsername(model.Username);
				if (cust2 != null)
					return Json(new { success = false, message = "Username is already registered" }, JsonRequestBehavior.AllowGet);
			}

			//validate customer roles
			var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
			var newCustomerRoles = new List<CustomerRole>();
			foreach (var customerRole in allCustomerRoles)
			{
				if (model.SelectedCustomerRoleIds.Contains(customerRole.Id) || customerRole.SystemName.ToLower() == "registered")
					newCustomerRoles.Add(customerRole);
			}
			var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
			if (!String.IsNullOrEmpty(customerRolesError))
			{
				ModelState.AddModelError("", customerRolesError);
				ErrorNotification(customerRolesError, false);
			}

			// Ensure that valid email address is entered if Registered role is checked to avoid registered customers with empty email address
			if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null && !CommonHelper.IsValidEmail(model.Email))
			{
				ModelState.AddModelError("", _localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"));
				ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"), false);
			}

			if (ModelState.IsValid)
			{
				var customer = new Customer
				{
					CustomerGuid = Guid.NewGuid(),
					Email = model.Email,
					Username = model.Username,
					VendorId = model.VendorId,
					AdminComment = model.AdminComment,
					IsTaxExempt = model.IsTaxExempt,
					Active = true,//model.Active,
					CreatedOnUtc = DateTime.UtcNow,
					LastActivityDateUtc = DateTime.UtcNow,
					RegisteredInStoreId = _storeContext.CurrentStore.Id
				};
				_customerService.InsertCustomer(customer);

				//form fields
				if (_dateTimeSettings.AllowCustomersToSetTimeZone)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
				if (_customerSettings.GenderEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
				_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
				_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
				if (_customerSettings.DateOfBirthEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);
				if (_customerSettings.CompanyEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Company);
				if (_customerSettings.StreetAddressEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
				if (_customerSettings.StreetAddress2Enabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
				if (_customerSettings.ZipPostalCodeEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
				if (_customerSettings.CityEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
				if (_customerSettings.CountryEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
				if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
				if (_customerSettings.PhoneEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
				if (_customerSettings.FaxEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);

				////custom customer attributes
				//_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);


				//newsletter subscriptions
				if (!String.IsNullOrEmpty(customer.Email))
				{
					var allStores = _storeService.GetAllStores();
					foreach (var store in allStores)
					{
						var newsletterSubscription = _newsLetterSubscriptionService
							 .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
						if (model.SelectedNewsletterSubscriptionStoreIds != null &&
							 model.SelectedNewsletterSubscriptionStoreIds.Contains(store.Id))
						{
							//subscribed
							if (newsletterSubscription == null)
							{
								_newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
								{
									NewsLetterSubscriptionGuid = Guid.NewGuid(),
									Email = customer.Email,
									Active = true,
									StoreId = store.Id,
									CreatedOnUtc = DateTime.UtcNow
								});
							}
						}
						else
						{
							//not subscribed
							if (newsletterSubscription != null)
							{
								_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletterSubscription);
							}
						}
					}
				}


				//password
				if (!String.IsNullOrWhiteSpace(model.Password))
				{
					var changePassRequest = new ChangePasswordRequest(model.Email, false, _customerSettings.DefaultPasswordFormat, model.Password);
					var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
					if (!changePassResult.Success)
					{
						foreach (var changePassError in changePassResult.Errors)
							return Json(new { success = false, message = changePassError }, JsonRequestBehavior.AllowGet);
						//ErrorNotification(changePassError);
					}
				}

				//customer roles
				foreach (var customerRole in newCustomerRoles)
				{
					//ensure that the current customer cannot add to "Administrators" system role if he's not an admin himself
					if (customerRole.SystemName == SystemCustomerRoleNames.Administrators &&
						 !_workContext.CurrentCustomer.IsAdmin())
						continue;

					customer.CustomerRoles.Add(customerRole);
				}
				_customerService.UpdateCustomer(customer);	 

			
				//activity log
				_customerActivityService.InsertActivity("AddNewCustomer", _localizationService.GetResource("ActivityLog.AddNewCustomer"), customer.Id);

				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Added"));

				return Json(new { success = true, message = "New user created successfuly." }, JsonRequestBehavior.AllowGet);
			}

			//If we got this far, something failed, redisplay form
			PrepareCustomerModel(model, null, true);
			return Json(new { success = false, message = "Error! Unable to create user." }, JsonRequestBehavior.AllowGet);
		}
		#endregion

	}
}
