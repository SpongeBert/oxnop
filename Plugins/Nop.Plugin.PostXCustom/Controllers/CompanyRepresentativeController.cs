﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Messages;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Helpers;
using Nop.Plugin.PostXCustom.Models.CompanyRepresentativeCustomer;
using Nop.Services.Authentication;
using Nop.Services.Authentication.External;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Plugin.PostXCustom.Services;
using Nop.Core.Domain.Forums;
using Nop.Services.Messages;
using Nop.Services.Vendors;
using Nop.Services.Tax;
using Nop.Core.Domain.Tax;
using Nop.Services.Forums;
using Nop.Services.Affiliates;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using System.Net;
using System.Collections.Specialized;
using Nop.Services.Configuration;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Localization;
using Nop.Services.Events;
using System.IO;
using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Logging;
using Nop.Services.CustomService;
using Nop.Services.ExportImport;

namespace Nop.Plugin.PostXCustom.Controllers
{

	public partial class CompanyRepresentativeController : BasePluginController
	{
		#region Fields

		private readonly ICustomerService _customerService;
		private readonly CustomerSettings _customerSettings;
		private readonly ILocalizationService _localizationService;
		private readonly IDateTimeHelper _dateTimeHelper;
		private readonly IWorkContext _workContext;
		private readonly ICustomerActivityService _customerActivityService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly IAuthenticationService _authenticationService;
		private readonly ICountryService _countryService;
		private readonly IStateProvinceService _stateProvinceService;
		private readonly IOrderService _orderService;
		private readonly IStoreService _storeService;
		private readonly IPriceFormatter _priceFormatter;
		private readonly ICompanyRepresentativeCustomerMappingService _companyRepresentativeCustomerMappingService;
		private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
		private readonly ICustomerRegistrationService _customerRegistrationService;
		private readonly DateTimeSettings _dateTimeSettings;
		private readonly TaxSettings _taxSettings;
		private readonly RewardPointsSettings _rewardPointsSettings;
		private readonly ITaxService _taxService;
		private readonly IVendorService _vendorService;
		private readonly IStoreContext _storeContext;
		private readonly IQueuedEmailService _queuedEmailService;
		private readonly EmailAccountSettings _emailAccountSettings;
		private readonly IEmailAccountService _emailAccountService;
		private readonly ForumSettings _forumSettings;
		private readonly IForumService _forumService;
		private readonly IOpenAuthenticationService _openAuthenticationService;
		private readonly ICustomerAttributeParser _customerAttributeParser;
		private readonly ICustomerAttributeService _customerAttributeService;
		private readonly IAffiliateService _affiliateService;
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly ICacheManager _cacheManager;


		private readonly PostXSettings _postXSettings;
		private readonly ISettingService _settingService;
		private readonly LocalizationSettings _localizationSettings;
		private readonly ILogger _logger;
		private readonly IEventPublisher _eventPublisher;
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICustomCodeService _customCodeService;
		private readonly IExportManager _exportManager;
		private readonly IAddressService _addressService;
		#endregion

		#region Constructors

		public CompanyRepresentativeController(ICustomerService customerService,
			CustomerSettings customerSettings, ILocalizationService localizationService, IDateTimeHelper dateTimeHelper, IWorkContext workContext,
			ICustomerActivityService customerActivityService, IGenericAttributeService genericAttributeService, IAuthenticationService authenticationService,
			ICountryService countryService, IStateProvinceService stateProvinceService, IOrderService orderService, IStoreService storeService,
			IPriceFormatter priceFormatter, ICompanyRepresentativeCustomerMappingService companyRepresentativeCustomerMappingService,
			INewsLetterSubscriptionService newsLetterSubscriptionService, ICustomerRegistrationService customerRegistrationService,
			DateTimeSettings dateTimeSettings, TaxSettings taxSettings, RewardPointsSettings rewardPointsSettings,
			ITaxService taxService, IVendorService vendorService, IStoreContext storeContext,
			IQueuedEmailService queuedEmailService, EmailAccountSettings emailAccountSettings, IEmailAccountService emailAccountService, ForumSettings forumSettings, IForumService forumService,
			IOpenAuthenticationService openAuthenticationService, ICustomerAttributeParser customerAttributeParser,
			ICustomerAttributeService customerAttributeService, IAffiliateService affiliateService, IWorkflowMessageService workflowMessageService, ICacheManager cacheManager,
			ISettingService settingService, LocalizationSettings localizationSettings, ILogger logger, IEventPublisher eventPublisher,
			ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICustomCodeService customCodeService, IExportManager exportManager,IAddressService addressService)
		{
			_customerService = customerService;
			_customerSettings = customerSettings;
			_localizationService = localizationService;
			_dateTimeHelper = dateTimeHelper;
			_workContext = workContext;
			_customerActivityService = customerActivityService;
			_genericAttributeService = genericAttributeService;
			_authenticationService = authenticationService;
			_countryService = countryService;
			_stateProvinceService = stateProvinceService;
			_orderService = orderService;
			_storeService = storeService;
			_priceFormatter = priceFormatter;
			_companyRepresentativeCustomerMappingService = companyRepresentativeCustomerMappingService;

			_newsLetterSubscriptionService = newsLetterSubscriptionService;
			_customerRegistrationService = customerRegistrationService;
			_dateTimeSettings = dateTimeSettings;
			_taxSettings = taxSettings;
			_rewardPointsSettings = rewardPointsSettings;
			_taxService = taxService;
			_vendorService = vendorService;
			_storeContext = storeContext;
			_queuedEmailService = queuedEmailService;
			_emailAccountSettings = emailAccountSettings;
			_emailAccountService = emailAccountService;
			_forumSettings = forumSettings;
			_forumService = forumService;
			_openAuthenticationService = openAuthenticationService;
			_customerAttributeParser = customerAttributeParser;
			_customerAttributeService = customerAttributeService;
			_affiliateService = affiliateService;
			_workflowMessageService = workflowMessageService;
			_cacheManager = cacheManager;

			_settingService = settingService;
			var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
			_postXSettings = _settingService.LoadSetting<PostXSettings>(storeScope);
			_localizationSettings = localizationSettings;
			_logger = logger;
			_eventPublisher = eventPublisher;

			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_customCodeService = customCodeService;
			_exportManager = exportManager;
			_addressService = addressService;
		}

		#endregion

		#region Utilities

		[NonAction]
		protected virtual bool CheckCustomerLightSpeedMapping(int customerId)
		{
			var mappingRecord = _cmsNopCustomerMappingService.GetByNopCustomerId(customerId);
			if (mappingRecord != null && mappingRecord.Deleted == false)
				return false;

			return true;
		}

		public virtual void SetEmail(Customer customer, string newEmail, bool requireValidation)
		{
			if (customer == null)
				throw new ArgumentNullException("customer");

			if (newEmail == null)
				throw new NopException("Email cannot be null");

			newEmail = newEmail.Trim();
			string oldEmail = customer.Email;

			if (!CommonHelper.IsValidEmail(newEmail))
				throw new NopException(_localizationService.GetResource("Account.EmailUsernameErrors.NewEmailIsNotValid"));

			if (newEmail.Length > 100)
				throw new NopException(_localizationService.GetResource("Account.EmailUsernameErrors.EmailTooLong"));

			//var customer2 = _customerService.GetCustomerByEmail(newEmail);
			//if (customer2 != null && customer.Id != customer2.Id)
			//	throw new NopException(_localizationService.GetResource("Account.EmailUsernameErrors.EmailAlreadyExists"));

			if (requireValidation)
			{
				//re-validate email
				customer.EmailToRevalidate = newEmail;
				_customerService.UpdateCustomer(customer);

				//email re-validation message
				_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.EmailRevalidationToken, Guid.NewGuid().ToString());
				_workflowMessageService.SendCustomerEmailRevalidationMessage(customer, _workContext.WorkingLanguage.Id);
			}
			else
			{
				customer.Email = newEmail;
				_customerService.UpdateCustomer(customer);

				//update newsletter subscription (if required)
				if (!String.IsNullOrEmpty(oldEmail) && !oldEmail.Equals(newEmail, StringComparison.InvariantCultureIgnoreCase))
				{
					foreach (var store in _storeService.GetAllStores())
					{
						var subscriptionOld = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(oldEmail, store.Id);
						if (subscriptionOld != null)
						{
							subscriptionOld.Email = newEmail;
							_newsLetterSubscriptionService.UpdateNewsLetterSubscription(subscriptionOld);
						}
					}
				}
			}
		}

		[NonAction]
		protected virtual string GetCustomerRolesNames(IList<CustomerRole> customerRoles, string separator = ",")
		{
			var sb = new StringBuilder();
			for (var i = 0; i < customerRoles.Count; i++)
			{
				sb.Append(customerRoles[i].Name);
				if (i != customerRoles.Count - 1)
				{
					sb.Append(separator);
					sb.Append(" ");
				}
			}
			return sb.ToString();
		}

		[NonAction]
		protected virtual CustomerModel PrepareCustomerModelForList(Customer customer)
		{
			return new CustomerModel
			{
				Id = customer.Id,
				Email = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest"),
				Username = customer.Username,
				FullName = customer.GetFullName(),
				Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
				Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
				ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
				CustomerRoleNames = GetCustomerRolesNames(customer.CustomerRoles.ToList()),
				Active = customer.Active,
				CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc),
				LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc),
			};
		}

		[NonAction]
		protected virtual IList<CustomerModel.AssociatedExternalAuthModel> GetAssociatedExternalAuthRecords(Customer customer)
		{
			if (customer == null)
				throw new ArgumentNullException("customer");

			var result = new List<CustomerModel.AssociatedExternalAuthModel>();
			foreach (var record in _openAuthenticationService.GetExternalIdentifiersFor(customer))
			{
				var method = _openAuthenticationService.LoadExternalAuthenticationMethodBySystemName(record.ProviderSystemName);
				if (method == null)
					continue;

				result.Add(new CustomerModel.AssociatedExternalAuthModel
				{
					Id = record.Id,
					Email = record.Email,
					ExternalIdentifier = record.ExternalIdentifier,
					AuthMethodName = method.PluginDescriptor.FriendlyName
				});
			}

			return result;
		}

		[NonAction]
		protected virtual void PrepareVendorsModel(CustomerModel model)
		{
			if (model == null)
				throw new ArgumentNullException("model");

			model.AvailableVendors.Add(new SelectListItem
			{
				Text = _localizationService.GetResource("Admin.Customers.Customers.Fields.Vendor.None"),
				Value = "0"
			});
			var vendors = SelectListHelper.GetVendorList(_vendorService, _cacheManager, true);
			foreach (var v in vendors)
				model.AvailableVendors.Add(v);
		}

		[NonAction]
		protected virtual void PrepareCustomerModel(CustomerModel model, Customer customer, bool excludeProperties)
		{
			var allStores = _storeService.GetAllStores();
			if (customer != null)
			{
				model.Id = customer.Id;
				if (!excludeProperties)
				{
					model.Email = customer.Email;
					model.Username = customer.Username;
					model.VendorId = customer.VendorId;
					model.AdminComment = customer.AdminComment;
					model.IsTaxExempt = customer.IsTaxExempt;
					model.Active = customer.Active;

					if (customer.RegisteredInStoreId == 0 || allStores.All(s => s.Id != customer.RegisteredInStoreId))
						model.RegisteredInStore = string.Empty;
					else
						model.RegisteredInStore = allStores.First(s => s.Id == customer.RegisteredInStoreId).Name;

					var affiliate = _affiliateService.GetAffiliateById(customer.AffiliateId);
					if (affiliate != null)
					{
						model.AffiliateId = affiliate.Id;
						model.AffiliateName = affiliate.GetFullName();
					}

					model.TimeZoneId = customer.GetAttribute<string>(SystemCustomerAttributeNames.TimeZoneId);
					model.VatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);
					model.VatNumberStatusNote = ((VatNumberStatus)customer.GetAttribute<int>(SystemCustomerAttributeNames.VatNumberStatusId))
						 .GetLocalizedEnum(_localizationService, _workContext);
					model.CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc);
					model.LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc);
					model.LastIpAddress = customer.LastIpAddress;
					model.LastVisitedPage = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastVisitedPage);

					model.SelectedCustomerRoleIds = customer.CustomerRoles.Select(cr => cr.Id).ToList();

					//newsletter subscriptions
					if (!String.IsNullOrEmpty(customer.Email))
					{
						var newsletterSubscriptionStoreIds = new List<int>();
						foreach (var store in allStores)
						{
							var newsletterSubscription = _newsLetterSubscriptionService
								 .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
							if (newsletterSubscription != null)
								newsletterSubscriptionStoreIds.Add(store.Id);
							model.SelectedNewsletterSubscriptionStoreIds = newsletterSubscriptionStoreIds.ToArray();
						}
					}

					//form fields
					model.FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
					model.LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
					model.Gender = customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender);
					model.DateOfBirth = customer.GetAttribute<DateTime?>(SystemCustomerAttributeNames.DateOfBirth);
					model.Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company);
					model.StreetAddress = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress);
					model.StreetAddress2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2);
					model.ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode);
					model.City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City);
					model.CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId);
					model.StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId);
					model.Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
					model.Fax = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax);
				}
			}

			model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
			model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
			foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
				model.AvailableTimeZones.Add(new SelectListItem { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == model.TimeZoneId) });
			model.DisplayVatNumber = customer != null && _taxSettings.EuVatEnabled;

			//vendors
			PrepareVendorsModel(model);
			//customer attributes
			PrepareCustomerAttributeModel(model, customer);

			model.GenderEnabled = _customerSettings.GenderEnabled;
			model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
			model.CompanyEnabled = _customerSettings.CompanyEnabled;
			model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
			model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
			model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
			model.CityEnabled = _customerSettings.CityEnabled;
			model.CountryEnabled = _customerSettings.CountryEnabled;
			model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
			model.PhoneEnabled = _customerSettings.PhoneEnabled;
			model.FaxEnabled = _customerSettings.FaxEnabled;

			//countries and states
			if (_customerSettings.CountryEnabled)
			{
				model.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
				foreach (var c in _countryService.GetAllCountries(showHidden: true))
				{
					model.AvailableCountries.Add(new SelectListItem
					{
						Text = c.Name,
						Value = c.Id.ToString(CultureInfo.InvariantCulture),
						Selected = c.Id == model.CountryId
					});
				}

				if (_customerSettings.StateProvinceEnabled)
				{
					//states
					var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
					if (states.Any())
					{
						model.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectState"), Value = "0" });

						foreach (var s in states)
						{
							model.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(CultureInfo.InvariantCulture), Selected = (s.Id == model.StateProvinceId) });
						}
					}
					else
					{
						bool anyCountrySelected = model.AvailableCountries.Any(x => x.Selected);

						model.AvailableStates.Add(new SelectListItem
						{
							Text = _localizationService.GetResource(anyCountrySelected ? "Admin.Address.OtherNonUS" : "Admin.Address.SelectState"),
							Value = "0"
						});
					}
				}
			}

			//newsletter subscriptions
			model.AvailableNewsletterSubscriptionStores = allStores
				 .Select(s => new CustomerModel.StoreModel { Id = s.Id, Name = s.Name })
				 .ToList();

			//customer roles
			var allRoles = _customerService.GetAllCustomerRoles(true);
			var adminRole = allRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered);
			//precheck Registered Role as a default role while creating a new customer through admin
			if (customer == null && adminRole != null)
			{
				model.SelectedCustomerRoleIds.Add(adminRole.Id);
			}

			if (_workContext.CurrentCustomer.IsAdmin())
			{
				foreach (var role in allRoles)
				{
					model.AvailableCustomerRoles.Add(new SelectListItem
					{
						Text = role.Name,
						Value = role.Id.ToString(CultureInfo.InvariantCulture),
						Selected = model.SelectedCustomerRoleIds.Contains(role.Id)
					});
				}
			}
			else
			{
				foreach (var role in allRoles)
				{

					var companyName = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Company);

					if (role.SystemName.ToLower() == "registered" ||
						 (!string.IsNullOrEmpty(companyName) && companyName.ToLower() == role.Name.ToLower()))
					{
						model.AvailableCustomerRoles.Add(new SelectListItem
						{
							Text = role.Name,
							Value = role.Id.ToString(CultureInfo.InvariantCulture),
							Selected = model.SelectedCustomerRoleIds.Contains(role.Id)
						});
					}
				}
			}

			//reward points history
			if (customer != null)
			{
				model.DisplayRewardPointsHistory = _rewardPointsSettings.Enabled;
				model.AddRewardPointsValue = 0;
				model.AddRewardPointsMessage = "Some comment here...";

				//stores
				foreach (var store in allStores)
				{
					model.RewardPointsAvailableStores.Add(new SelectListItem
					{
						Text = store.Name,
						Value = store.Id.ToString(CultureInfo.InvariantCulture),
						Selected = (store.Id == _storeContext.CurrentStore.Id)
					});
				}
			}
			else
			{
				model.DisplayRewardPointsHistory = false;
			}
			//external authentication records
			if (customer != null)
			{
				model.AssociatedExternalAuthRecords = GetAssociatedExternalAuthRecords(customer);
			}
			//sending of the welcome message:
			//1. "admin approval" registration method
			//2. already created customer
			//3. registered
			model.AllowSendingOfWelcomeMessage = _customerSettings.UserRegistrationType == UserRegistrationType.AdminApproval &&
				 customer != null &&
				 customer.IsRegistered();
			//sending of the activation message
			//1. "email validation" registration method
			//2. already created customer
			//3. registered
			//4. not active
			model.AllowReSendingOfActivationMessage = _customerSettings.UserRegistrationType == UserRegistrationType.EmailValidation &&
				 customer != null &&
				 customer.IsRegistered() &&
				 !customer.Active;
		}

		[NonAction]
		protected virtual string ValidateCustomerRoles(IList<CustomerRole> customerRoles)
		{
			if (customerRoles == null)
				throw new ArgumentNullException("customerRoles");

			//ensure a customer is not added to both 'Guests' and 'Registered' customer roles
			//ensure that a customer is in at least one required role ('Guests' and 'Registered')
			bool isInGuestsRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Guests) != null;
			bool isInRegisteredRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Registered) != null;
			if (isInGuestsRole && isInRegisteredRole)
				return _localizationService.GetResource("Admin.Customers.Customers.GuestsAndRegisteredRolesError");
			if (!isInGuestsRole && !isInRegisteredRole)
				return _localizationService.GetResource("Admin.Customers.Customers.AddCustomerToGuestsOrRegisteredRoleError");

			//no errors
			return "";
		}

		[NonAction]
		protected virtual void PrepareCustomerAttributeModel(CustomerModel model, Customer customer)
		{
			var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
			foreach (var attribute in customerAttributes)
			{
				var attributeModel = new CustomerModel.CustomerAttributeModel
				{
					Id = attribute.Id,
					Name = attribute.Name,
					IsRequired = attribute.IsRequired,
					AttributeControlType = attribute.AttributeControlType,
				};

				if (attribute.ShouldHaveValues())
				{
					//values
					var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
					foreach (var attributeValue in attributeValues)
					{
						var attributeValueModel = new CustomerModel.CustomerAttributeValueModel
						{
							Id = attributeValue.Id,
							Name = attributeValue.Name,
							IsPreSelected = attributeValue.IsPreSelected
						};
						attributeModel.Values.Add(attributeValueModel);
					}
				}


				//set already selected attributes
				if (customer != null)
				{
					var selectedCustomerAttributes = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes, _genericAttributeService);
					switch (attribute.AttributeControlType)
					{
						case AttributeControlType.DropdownList:
						case AttributeControlType.RadioList:
						case AttributeControlType.Checkboxes:
							{
								if (!String.IsNullOrEmpty(selectedCustomerAttributes))
								{
									//clear default selection
									foreach (var item in attributeModel.Values)
										item.IsPreSelected = false;

									//select new values
									var selectedValues = _customerAttributeParser.ParseCustomerAttributeValues(selectedCustomerAttributes);
									foreach (var attributeValue in selectedValues)
										foreach (var item in attributeModel.Values)
											if (attributeValue.Id == item.Id)
												item.IsPreSelected = true;
								}
							}
							break;
						case AttributeControlType.ReadonlyCheckboxes:
							{
								//do nothing
								//values are already pre-set
							}
							break;
						case AttributeControlType.TextBox:
						case AttributeControlType.MultilineTextbox:
							{
								if (!String.IsNullOrEmpty(selectedCustomerAttributes))
								{
									var enteredText = _customerAttributeParser.ParseValues(selectedCustomerAttributes, attribute.Id);
									if (enteredText.Any())
										attributeModel.DefaultValue = enteredText[0];
								}
							}
							break;
						case AttributeControlType.Datepicker:
						case AttributeControlType.ColorSquares:
						case AttributeControlType.ImageSquares:
						case AttributeControlType.FileUpload:
						default:
							//not supported attribute control types
							break;
					}
				}

				model.CustomerAttributes.Add(attributeModel);
			}
		}

		[NonAction]
		protected virtual string ParseCustomCustomerAttributes(FormCollection form)
		{
			if (form == null)
				throw new ArgumentNullException("form");

			string attributesXml = "";
			var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
			foreach (var attribute in customerAttributes)
			{
				string controlId = string.Format("customer_attribute_{0}", attribute.Id);
				switch (attribute.AttributeControlType)
				{
					case AttributeControlType.DropdownList:
					case AttributeControlType.RadioList:
						{
							var ctrlAttributes = form[controlId];
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								int selectedAttributeId = int.Parse(ctrlAttributes);
								if (selectedAttributeId > 0)
									attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
										 attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
							}
						}
						break;
					case AttributeControlType.Checkboxes:
						{
							var cblAttributes = form[controlId];
							if (!String.IsNullOrEmpty(cblAttributes))
							{
								foreach (var item in cblAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
								{
									int selectedAttributeId = int.Parse(item);
									if (selectedAttributeId > 0)
										attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
											 attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
								}
							}
						}
						break;
					case AttributeControlType.ReadonlyCheckboxes:
						{
							//load read-only (already server-side selected) values
							var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
							foreach (var selectedAttributeId in attributeValues
								 .Where(v => v.IsPreSelected)
								 .Select(v => v.Id)
								 .ToList())
							{
								attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
												attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
							}
						}
						break;
					case AttributeControlType.TextBox:
					case AttributeControlType.MultilineTextbox:
						{
							var ctrlAttributes = form[controlId];
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								string enteredText = ctrlAttributes.Trim();
								attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
									 attribute, enteredText);
							}
						}
						break;
					case AttributeControlType.Datepicker:
					case AttributeControlType.ColorSquares:
					case AttributeControlType.ImageSquares:
					case AttributeControlType.FileUpload:
					//not supported customer attributes
					default:
						break;
				}
			}

			return attributesXml;
		}

		[NonAction]
		private bool SecondAdminAccountExists(Customer customer)
		{
			var customers = _customerService.GetAllCustomers(customerRoleIds: new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Administrators).Id });

			return customers.Any(c => c.Active && c.Id != customer.Id);
		}


		public virtual void AssignCompanyRepresentativeMapping(int recordId, int associatedCustomerId, int companyRepresentativeId)
		{
			if (associatedCustomerId == 0)
			{
				return;
			}

			var salesPersonCustomerMap = _companyRepresentativeCustomerMappingService.GetByCustomerId(associatedCustomerId);

			if (salesPersonCustomerMap == null && companyRepresentativeId == 0)
				return;


			if (salesPersonCustomerMap == null && companyRepresentativeId > 0)
			{
				salesPersonCustomerMap = new CompanyRepresentativeCustomerMapping
				{
					AssociatedCustomerId = associatedCustomerId,
					CompanyRepresentativeId = companyRepresentativeId
				};
				_companyRepresentativeCustomerMappingService.Insert(salesPersonCustomerMap);
				return;
			}

			if (salesPersonCustomerMap != null && companyRepresentativeId == 0)
			{
				_companyRepresentativeCustomerMappingService.Delete(salesPersonCustomerMap);
				return;
			}

			if (salesPersonCustomerMap != null && companyRepresentativeId > 0)
			{
				salesPersonCustomerMap.CompanyRepresentativeId = companyRepresentativeId;
				_companyRepresentativeCustomerMappingService.Update(salesPersonCustomerMap);
			}
		}


		#endregion

		#region Customers

		public virtual ActionResult Index()
		{
			return RedirectToAction("List");
		}

		public virtual ActionResult List()
		{
			//load registered customers by default
			var defaultRoleIds = new List<int> { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Registered).Id };
			var model = new CustomerListModel
			{
				UsernamesEnabled = _customerSettings.UsernamesEnabled,
				DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled,
				CompanyEnabled = _customerSettings.CompanyEnabled,
				PhoneEnabled = _customerSettings.PhoneEnabled,
				ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled,
				SearchCustomerRoleIds = defaultRoleIds,
				IsAdminRole = _workContext.CurrentCustomer.Id,
			};
			var allRoles = _customerService.GetAllCustomerRoles(true);
			foreach (var role in allRoles)
			{
				model.AvailableCustomerRoles.Add(new SelectListItem
				{
					Text = role.Name,
					Value = role.Id.ToString(CultureInfo.InvariantCulture),
					Selected = defaultRoleIds.Any(x => x == role.Id)
				});
			}

			return View("~/Plugins/PostXCustom/Views/CompanyRepresentative/List.cshtml", model);
		}

		[HttpPost]
		public virtual ActionResult CustomerList(DataSourceRequest command, CustomerListModel model,
			 [ModelBinder(typeof(CommaSeparatedModelBinder))] int[] searchCustomerRoleIds)
		{
			//we use own own binder for searchCustomerRoleIds property 
			var salesPersonCustomers = new List<int>();
			var searchDayOfBirth = 0;
			var searchMonthOfBirth = 0;
			if (!String.IsNullOrWhiteSpace(model.SearchDayOfBirth))
				searchDayOfBirth = Convert.ToInt32(model.SearchDayOfBirth);
			if (!String.IsNullOrWhiteSpace(model.SearchMonthOfBirth))
				searchMonthOfBirth = Convert.ToInt32(model.SearchMonthOfBirth);

			// If salesperson is login
			if (!_workContext.CurrentCustomer.IsAdmin())
			{
				salesPersonCustomers = _companyRepresentativeCustomerMappingService.GetCustomersBySalesPersonId(_workContext.CurrentCustomer.Id);
			}

			var customers = _companyRepresentativeCustomerMappingService.GetAllCustomers(
				 customerRoleIds: searchCustomerRoleIds,
				 email: model.SearchEmail,
				 username: model.SearchUsername,
				 firstName: model.SearchFirstName,
				 lastName: model.SearchLastName,
				 dayOfBirth: searchDayOfBirth,
				 monthOfBirth: searchMonthOfBirth,
				 company: model.SearchCompany,
				 phone: model.SearchPhone,
				 zipPostalCode: model.SearchZipPostalCode,
				 ipAddress: model.SearchIpAddress,
				 loadOnlyWithShoppingCart: false,
				 pageIndex: command.Page - 1,
				 pageSize: command.PageSize,
				 customerIds: salesPersonCustomers);

			
			var gridModel = new DataSourceResult
			{
				Data = customers.Select(PrepareCustomerModelForList),
				Total = customers.TotalCount
			};
			return Json(gridModel);
		}

		public ActionResult View(int id)
		{
			var customer = _customerService.GetCustomerById(id);
			if (customer == null || customer.Deleted)
				//No customer found with the specified id
				return RedirectToAction("List");

			var model = new CustomerModel();
			PrepareCustomerModel(model, customer, false);
			return View("~/Plugins/PostXCustom/Views/CompanyRepresentative/View.cshtml", model);
		}

		public ActionResult Impersonate(int id)
		{
			var customer = _customerService.GetCustomerById(id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			//activity log
			_customerActivityService.InsertActivity("Impersonation.Started",
				 _localizationService.GetResource("ActivityLog.Impersonation.Started.StoreOwner"), customer.Email, customer.Id);
			_customerActivityService.InsertActivity(customer, "Impersonation.Started",
				 _localizationService.GetResource("ActivityLog.Impersonation.Started.Customer"), _workContext.CurrentCustomer.Email, _workContext.CurrentCustomer.Id);

			_genericAttributeService.SaveAttribute<int?>(_workContext.CurrentCustomer,
				 SystemCustomerAttributeNames.ImpersonatedCustomerId, customer.Id);

			return RedirectToAction("Index", "Home", new { area = "" });
		}

		//available even when a store is closed
		[StoreClosed(true)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public ActionResult CustomerLogout()
		{
			//external authentication
			ExternalAuthorizerHelper.RemoveParameters();

			if (_workContext.OriginalCustomerIfImpersonated != null)
			{
				//activity log
				_customerActivityService.InsertActivity(_workContext.OriginalCustomerIfImpersonated, "Impersonation.Finished",
					 _localizationService.GetResource("ActivityLog.Impersonation.Finished.StoreOwner"), _workContext.CurrentCustomer.Email, _workContext.CurrentCustomer.Id);
				_customerActivityService.InsertActivity("Impersonation.Finished",
					 _localizationService.GetResource("ActivityLog.Impersonation.Finished.Customer"), _workContext.OriginalCustomerIfImpersonated.Email, _workContext.OriginalCustomerIfImpersonated.Id);

				//logout impersonated customer
				_genericAttributeService.SaveAttribute<int?>(_workContext.OriginalCustomerIfImpersonated,
					 SystemCustomerAttributeNames.ImpersonatedCustomerId, null);
				if (_workContext.OriginalCustomerIfImpersonated.Id != 1)
					return RedirectToAction("List");

				return RedirectToAction("Edit", "Customer", new { id = _workContext.CurrentCustomer.Id, area = "Admin" });
			}
			//activity log
			_customerActivityService.InsertActivity("PublicStore.Logout", _localizationService.GetResource("ActivityLog.PublicStore.Logout"));
			//standard logout 
			_authenticationService.SignOut();
			return RedirectToRoute("HomePage");
		}

		[HttpPost]
		public ActionResult OrderList(int customerId, DataSourceRequest command)
		{
			var orders = _orderService.SearchOrders(customerId: customerId);

			var gridModel = new DataSourceResult
			{
				Data = orders.PagedForCommand(command)
					 .Select(order =>
					 {
						 var store = _storeService.GetStoreById(order.StoreId);
						 var orderModel = new CustomerModel.OrderModel
						 {
							 Id = order.Id,
							 OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
							 OrderStatusId = order.OrderStatusId,
							 PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
							 ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
							 OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false),
							 StoreName = store != null ? store.Name : "Unknown",
							 CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc),
						 };
						 return orderModel;
					 }),
				Total = orders.Count
			};
			return Json(gridModel);
		}


		public virtual ActionResult Create()
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var model = new CustomerModel();
			PrepareCustomerModel(model, null, false);
			model.Username = "Username will be generated by the system";

			//default value
			model.Active = true;
			return View("~/Plugins/PostXCustom/Views/CompanyRepresentative/Create.cshtml", model);
		}


		private void AssignPersonId(out int personId)
		{
			var companyUserList = _companyRepresentativeCustomerMappingService.GetCustomersBySalesPerson(_workContext.CurrentCustomer.Id);

			if (companyUserList.Any())
			{
				var customerIdList = companyUserList.Select(x => x.AssociatedCustomerId);
				//var customerList = _customerService.GetAllCustomers().Where(x => customerIdList.Contains(x.Id));
				var customerList = _customerService.GetAllCustomersIncludeDeleted().Where(x => customerIdList.Contains(x.Id));
				var topCustomerIdRecord = customerList.OrderByDescending(x => x.PersonalId).First();
				if (topCustomerIdRecord == null)
				{
					personId = 2;
					return;
				}
				personId = topCustomerIdRecord.PersonalId + 1;
				return;
			}
			personId = 2;
		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		[FormValueRequired("save", "save-continue")]
		[ValidateInput(false)]
		public virtual ActionResult Create(CustomerModel model, bool continueEditing, FormCollection form)
		{
			//Commented below code to allow multiple customer with same email.
			if (!String.IsNullOrWhiteSpace(model.Email))
			{
				var cust2 = _customerService.GetCustomerByEmail(model.Email);
				if (cust2 != null)
					ModelState.AddModelError("", "Email is already registered");
			}


			int personId = 0;
			AssignPersonId(out personId);
			int companyId = _workContext.CurrentCustomer.CompanyId;
			model.Username = companyId.ToString("D3") + personId.ToString("D5");

			if (!String.IsNullOrWhiteSpace(model.Username) & _customerSettings.UsernamesEnabled)
			{
				var cust2 = _customerService.GetCustomerByUsername(model.Username);
				if (cust2 != null)
					ModelState.AddModelError("", "Username is already registered");
			}

			//validate customer roles
			var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
			var newCustomerRoles = new List<CustomerRole>();
			foreach (var customerRole in allCustomerRoles)
				if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
					newCustomerRoles.Add(customerRole);

			var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
			if (!String.IsNullOrEmpty(customerRolesError))
			{
				ModelState.AddModelError("", customerRolesError);
				ErrorNotification(customerRolesError, false);
			}

			// Ensure that valid email address is entered if Registered role is checked to avoid registered customers with empty email address
			if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null && !CommonHelper.IsValidEmail(model.Email))
			{
				ModelState.AddModelError("", _localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"));
				ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"), false);
			}

			//custom customer attributes
			var customerAttributesXml = ParseCustomCustomerAttributes(form);
			if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null)
			{
				var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
				foreach (var error in customerAttributeWarnings)
				{
					ModelState.AddModelError("", error);
				}
			}

			if (ModelState.IsValid)
			{
				var isActive = (model.StatusCodeTypeId == 1) ? true : false;
				var customer = new Customer
				{
					CustomerGuid = Guid.NewGuid(),
					Email = model.Email,
					Username = model.Username,
					VendorId = 0,
					AdminComment = model.AdminComment,
					IsTaxExempt = false,
					Active = isActive,
					CreatedOnUtc = DateTime.UtcNow,
					LastActivityDateUtc = DateTime.UtcNow,
					RegisteredInStoreId = _storeContext.CurrentStore.Id,
					CompanyId = companyId,
					PersonalId = personId
				};
				_customerService.InsertCustomer(customer);

				//form fields
				if (_dateTimeSettings.AllowCustomersToSetTimeZone)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
				if (_customerSettings.GenderEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
				_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
				_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
				if (_customerSettings.DateOfBirthEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);
				if (_customerSettings.StreetAddressEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
				if (_customerSettings.StreetAddress2Enabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
				if (_customerSettings.ZipPostalCodeEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
				if (_customerSettings.CityEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
				if (_customerSettings.CountryEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
				if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
				if (_customerSettings.PhoneEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
				if (_customerSettings.FaxEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);

				//custom customer attributes
				_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);



				//newsletter subscriptions
				if (!String.IsNullOrEmpty(customer.Email))
				{
					var allStores = _storeService.GetAllStores();
					foreach (var store in allStores)
					{
						var newsletterSubscription = _newsLetterSubscriptionService
							 .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
						if (model.SelectedNewsletterSubscriptionStoreIds != null &&
							 model.SelectedNewsletterSubscriptionStoreIds.Contains(store.Id))
						{
							//subscribed
							if (newsletterSubscription == null)
							{
								_newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
								{
									NewsLetterSubscriptionGuid = Guid.NewGuid(),
									Email = customer.Email,
									Active = true,
									StoreId = store.Id,
									CreatedOnUtc = DateTime.UtcNow
								});
							}
						}
						else
						{
							//not subscribed
							if (newsletterSubscription != null)
							{
								_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletterSubscription);
							}
						}
					}
				}

				//password
				if (!String.IsNullOrWhiteSpace(model.Password))
				{
					var changePassRequest = new ChangePasswordRequest(model.Username, false, _customerSettings.DefaultPasswordFormat, model.Password);
					var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
					if (!changePassResult.Success)
					{
						foreach (var changePassError in changePassResult.Errors)
							ErrorNotification(changePassError);
					}
				}

				//customer roles
				foreach (var customerRole in newCustomerRoles)
				{
					//ensure that the current customer cannot add to "Administrators" system role if he's not an admin himself
					if (customerRole.SystemName == SystemCustomerRoleNames.Administrators &&
						 !_workContext.CurrentCustomer.IsAdmin())
						continue;

					customer.CustomerRoles.Add(customerRole);
				}
				_customerService.UpdateCustomer(customer);


				//ensure that a customer with a vendor associated is not in "Administrators" role
				//otherwise, he won't have access to other functionality in admin area
				if (customer.IsAdmin() && customer.VendorId > 0)
				{
					customer.VendorId = 0;
					_customerService.UpdateCustomer(customer);
					ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminCouldNotbeVendor"));
				}

				//ensure that a customer in the Vendors role has a vendor account associated.
				//otherwise, he will have access to ALL products
				if (customer.IsVendor() && customer.VendorId == 0)
				{
					var vendorRole = customer
						 .CustomerRoles
						 .FirstOrDefault(x => x.SystemName == SystemCustomerRoleNames.Vendors);
					customer.CustomerRoles.Remove(vendorRole);
					_customerService.UpdateCustomer(customer);
					ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.CannotBeInVendoRoleWithoutVendorAssociated"));
				}

				//activity log
				_customerActivityService.InsertActivity("AddNewCustomer", _localizationService.GetResource("ActivityLog.AddNewCustomer"), customer.Id);
				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Added"));




				if (customer.Id > 0)
					AssignCompanyRepresentativeMapping(0, customer.Id, _workContext.CurrentCustomer.Id);

				//Assign Company Address
				//var companyAddress = _workContext.CurrentCustomer.Addresses.FirstOrDefault();
				//if (companyAddress != null)
				//{
				//	customer.Addresses.Add(companyAddress);
				//	_customerService.UpdateCustomer(customer);

				//	var defaultAddress = customer.Addresses.FirstOrDefault();
				//	customer.BillingAddress = defaultAddress;
				//	_customerService.UpdateCustomer(customer);
				//}


				#region Custom Field attributes
				var companyAttributeList = _genericAttributeService.GetAttributesForEntity(_workContext.CurrentCustomer.Id, "Customer");
				if (companyAttributeList.Any())
				{
					var companyName = _workContext.CurrentCustomer.GetAttribute<string>(SystemCustomerAttributeNames.Company);
					//Company Name
					if (!string.IsNullOrEmpty(companyName))
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, companyName);

					var newCustomerAttributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");

					var pictureAttribute = companyAttributeList.FirstOrDefault(x => x.Key == "PictureId");
					if (pictureAttribute != null)
						model.PictureId = Convert.ToInt32(pictureAttribute.Value);


					InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "AccessWord", model.Password);

					//var companyNumber = _workContext.CurrentCustomer.CompanyId;
					//model.CompanyNumber = !string.IsNullOrEmpty(companyNumber.ToString(CultureInfo.InvariantCulture)) ? companyNumber : 0;
					//InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "CompanyNumber",model.CompanyNumber.ToString(CultureInfo.InvariantCulture));

					var contractTypeId = GetAttributeValue(companyAttributeList.ToList(), "ContractTypeId");
					model.ContractTypeId = !string.IsNullOrEmpty(contractTypeId) ? Convert.ToInt32(contractTypeId) : 0;
					InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "ContractTypeId", model.ContractTypeId.ToString(CultureInfo.InvariantCulture));

					//var loyalityPercentage = GetAttributeValue(companyAttributeList.ToList(), "LoyalityPercentage");
					//model.LoyalityPercentage = !string.IsNullOrEmpty(loyalityPercentage) ? Convert.ToInt32(loyalityPercentage) : 0;
					//InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "LoyalityPercentage", model.LoyalityPercentage.ToString(CultureInfo.InvariantCulture));

					var vatRegimeId = GetAttributeValue(companyAttributeList.ToList(), "VatRegimeId");
					model.VatRegimeId = !string.IsNullOrEmpty(vatRegimeId) ? Convert.ToInt32(vatRegimeId) : 0;
					InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "VatRegimeId", model.VatRegimeId.ToString(CultureInfo.InvariantCulture));


					//Save Customer Image
					if (model.CustomerPictureId > 0)
					{
						InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "CustomerPictureId",
						model.CustomerPictureId.ToString(CultureInfo.InvariantCulture));
					}

					//InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "PersonalNumber",
					//	model.PersonalNumber.ToString(CultureInfo.InvariantCulture));

					InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "PersonalNumber", customer.Username);

					InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "ActivationDate",
						model.ActivationDate.ToString());

					InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "EndDate",
						model.EndDate.ToString());

					InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "AuthorizationTypeId",
						model.AuthorizationTypeId.ToString(CultureInfo.InvariantCulture));

					//if (model.AuthorizationTypeId == 2)
					//{																																 
					//	var loyalityPercentage = GetAttributeValue(companyAttributeList.ToList(), "LoyalityPercentage");
					//	model.LoyalityPercentage = !string.IsNullOrEmpty(loyalityPercentage) ? Convert.ToInt32(loyalityPercentage) : 0;
					//	InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "LoyalityPercentage", model.LoyalityPercentage.ToString(CultureInfo.InvariantCulture));
					//}
					//else
					//	InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "LoyalityPercentage", "0");

					var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
					var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeScope);

					switch (model.ContractTypeId)
					{
						case 1:
							InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractA.ToString(CultureInfo.InvariantCulture));
							break;
						case 2:
							InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractB.ToString(CultureInfo.InvariantCulture));
							break;
						case 3:
							InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractC.ToString(CultureInfo.InvariantCulture));
							break;
						case 4:
							InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractD.ToString(CultureInfo.InvariantCulture));
							break;
						case 26:
							InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractZ.ToString(CultureInfo.InvariantCulture));
							break;
					}

					InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "StatusCodeId", model.StatusCodeTypeId.ToString(CultureInfo.InvariantCulture));



					#region Company Address Map

					var addressGenericAttribute = companyAttributeList.FirstOrDefault(x => x.Key == "CompanyAddressId");
					if (addressGenericAttribute != null)
					{
						var companyAddress = _addressService.GetAddressById(Convert.ToInt32(addressGenericAttribute.Value));
						if (companyAddress != null)
						{
							customer.Addresses.Add(companyAddress);
							_customerService.UpdateCustomer(customer);
							var defaultAddress = customer.Addresses.FirstOrDefault();
							customer.BillingAddress = defaultAddress;
							_customerService.UpdateCustomer(customer);
						}
					}
					#endregion


				}



				#endregion

				//Send Email Notification
				_workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);
				_workflowMessageService.SendCustomerWelcomeMessage(customer, _localizationSettings.DefaultAdminLanguageId);

				//raise event       
				_eventPublisher.Publish(new CustomerRegisteredEvent(customer));

				#region Push To WineShop//Temp Commented

				//using (var client = new WebClient())
				//{
				//	var values = new NameValueCollection();
				//	values["Username"] = customer.Username;
				//	values["Email"] = customer.Email;
				//	values["FirstName"] = model.FirstName;
				//	values["LastName"] = model.LastName;
				//	values["Company"] = model.Company;
				//	values["DateOfBirth"] = model.DateOfBirth.ToString();
				//	values["Gender"] = model.Gender;
				//	values["Password"] = model.Password;
				//	values["Active"] = model.Active.ToString();


				//	//var response = client.UploadValues("http://localhost/wines/CustomerAPI/create", values);
				//	var response = client.UploadValues(_postXSettings.CustomerPushUrl, values);

				//	var responseString = Encoding.Default.GetString(response);
				//}

				#endregion


				if (continueEditing)
				{
					//selected tab
					//SaveSelectedTabName();

					return RedirectToAction("Edit", new { id = customer.Id });
				}
				return RedirectToAction("List");
			}

			//If we got this far, something failed, redisplay form
			PrepareCustomerModel(model, null, true);
			return View("~/Plugins/PostXCustom/Views/CompanyRepresentative/Create.cshtml", model);
		}

		public string GetAttributeValue(List<GenericAttribute> genericAttributes, string attributeToCheck)
		{
			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeToCheck);
			if (genericAttribute == null)
				return null;

			return genericAttribute.Value;
		}

		public void InsertUpdateAttribute(Customer customer, List<GenericAttribute> genericAttributes, string attributeName, string attributeValue)
		{
			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeName);
			if (genericAttribute == null)
			{
				genericAttribute = new GenericAttribute
				{
					EntityId = customer.Id,
					Key = attributeName,
					KeyGroup = "Customer",
					Value = attributeValue,
					StoreId = 0
				};
				_genericAttributeService.InsertAttribute(genericAttribute);
			}
			else
			{
				genericAttribute.Value = attributeValue;
				_genericAttributeService.UpdateAttribute(genericAttribute);
			}
		}

		public virtual ActionResult Edit(int id)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult(); 

			var customer = _customerService.GetCustomerById(id);
			if (customer == null || customer.Deleted)
				//No customer found with the specified id
				return RedirectToAction("List");


			var model = new CustomerModel();
			PrepareCustomerModel(model, customer, false);

			var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
			if (attributeList.Any())
			{
				var pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
				if (pictureAttribute != null)
					model.PictureId = Convert.ToInt32(pictureAttribute.Value);


				var currentPassword = GetAttributeValue(attributeList.ToList(), "AccessWord");
				model.CurrentPassword = !string.IsNullOrEmpty(currentPassword) ? currentPassword : "";

				model.CompanyNumber = customer.CompanyId;
				model.PersonalNumber = !string.IsNullOrEmpty(customer.Username) ? customer.Username : "personal number not assigned.";

				var bankAccountNumber = GetAttributeValue(attributeList.ToList(), "BankAccountNumber");
				model.BankAccountNumber = !string.IsNullOrEmpty(bankAccountNumber) ? bankAccountNumber : "";


				var contractTypeId = GetAttributeValue(attributeList.ToList(), "ContractTypeId");
				model.ContractTypeId = !string.IsNullOrEmpty(contractTypeId) ? Convert.ToInt32(contractTypeId) : 0;

				var loyalityPercentage = GetAttributeValue(attributeList.ToList(), "LoyalityPercentage");
				model.LoyalityPercentage = !string.IsNullOrEmpty(loyalityPercentage) ? Convert.ToInt32(loyalityPercentage) : 0;


				var authorizationTypeId = GetAttributeValue(attributeList.ToList(), "AuthorizationTypeId");
				model.AuthorizationTypeId = !string.IsNullOrEmpty(authorizationTypeId) ? Convert.ToInt32(authorizationTypeId) : 0;


				var statusCodeId = GetAttributeValue(attributeList.ToList(), "StatusCodeId");
				model.StatusCodeTypeId = !string.IsNullOrEmpty(statusCodeId) ? Convert.ToInt32(statusCodeId) : 0;

				InsertUpdateAttribute(customer, attributeList.ToList(), "StatusCodeId", model.StatusCodeTypeId.ToString(CultureInfo.InvariantCulture));


				var customerPictureId = GetAttributeValue(attributeList.ToList(), "CustomerPictureId");
				if (!string.IsNullOrEmpty(customerPictureId))
					model.CustomerPictureId = Convert.ToInt32(customerPictureId);

				var activationDate = GetAttributeValue(attributeList.ToList(), "ActivationDate");
				if (!string.IsNullOrEmpty(activationDate))
					model.ActivationDate = Convert.ToDateTime(activationDate);

				var endDate = GetAttributeValue(attributeList.ToList(), "EndDate");
				if (!string.IsNullOrEmpty(endDate))
					model.EndDate = Convert.ToDateTime(endDate);

			}
			model.IsSyncedToLightSpeed = CheckCustomerLightSpeedMapping(customer.Id);

			return View("~/Plugins/PostXCustom/Views/CompanyRepresentative/Edit.cshtml", model);
		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		[FormValueRequired("save", "save-continue")]
		[ValidateInput(false)]
		public virtual ActionResult Edit(CustomerModel model, bool continueEditing, FormCollection form)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null || customer.Deleted)
				//No customer found with the specified id
				return RedirectToAction("List");

			//validate customer roles
			var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
			var newCustomerRoles = new List<CustomerRole>();
			foreach (var customerRole in allCustomerRoles)
				if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
					newCustomerRoles.Add(customerRole);
			var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
			if (!String.IsNullOrEmpty(customerRolesError))
			{
				ModelState.AddModelError("", customerRolesError);
				ErrorNotification(customerRolesError, false);
			}

			// Ensure that valid email address is entered if Registered role is checked to avoid registered customers with empty email address
			if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null && !CommonHelper.IsValidEmail(model.Email))
			{
				ModelState.AddModelError("", _localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"));
				ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"), false);
			}

			//custom customer attributes
			var customerAttributesXml = ParseCustomCustomerAttributes(form);
			if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null)
			{
				var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
				foreach (var error in customerAttributeWarnings)
				{
					ModelState.AddModelError("", error);
				}
			}

			if (ModelState.IsValid)
			{
				try
				{
					customer.AdminComment = model.AdminComment;
					customer.IsTaxExempt = model.IsTaxExempt;

					var isActive = (model.StatusCodeTypeId == 1);
					model.Active = isActive;

					//prevent deactivation of the last active administrator
					if (!customer.IsAdmin() || model.Active || SecondAdminAccountExists(customer))
						customer.Active = model.Active;
					else
						ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.Deactivate"));

					//email
					if (!String.IsNullOrWhiteSpace(model.Email))
					{
						//_customerRegistrationService.SetEmail(customer, model.Email, false);
						SetEmail(customer, model.Email, false);
					}
					else
					{
						customer.Email = model.Email;
					}

					//username
					if (_customerSettings.UsernamesEnabled)
					{
						//if (!String.IsNullOrWhiteSpace(model.Username))
						//{
						//	_customerRegistrationService.SetUsername(customer, model.Username);
						//}
						//else
						//{
						//	customer.Username = model.Username;
						//}
					}

					//VAT number
					if (_taxSettings.EuVatEnabled)
					{
						var prevVatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);

						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.VatNumber, model.VatNumber);
						//set VAT number status
						if (!String.IsNullOrEmpty(model.VatNumber))
						{
							if (!model.VatNumber.Equals(prevVatNumber, StringComparison.InvariantCultureIgnoreCase))
							{
								_genericAttributeService.SaveAttribute(customer,
									 SystemCustomerAttributeNames.VatNumberStatusId,
									 (int)_taxService.GetVatNumberStatus(model.VatNumber));
							}
						}
						else
						{
							_genericAttributeService.SaveAttribute(customer,
								 SystemCustomerAttributeNames.VatNumberStatusId,
								 (int)VatNumberStatus.Empty);
						}
					}

					//vendor
					customer.VendorId = model.VendorId;

					//form fields
					if (_dateTimeSettings.AllowCustomersToSetTimeZone)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
					if (_customerSettings.GenderEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
					if (_customerSettings.DateOfBirthEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);

					//if (_customerSettings.CompanyEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Company);

					if (_customerSettings.StreetAddressEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
					if (_customerSettings.StreetAddress2Enabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
					if (_customerSettings.ZipPostalCodeEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
					if (_customerSettings.CityEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
					if (_customerSettings.CountryEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
					if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
					if (_customerSettings.PhoneEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
					if (_customerSettings.FaxEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);

					//custom customer attributes
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);


					//Update or insert Company Logo
					var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
					if (attributeList.Any())
					{
						var pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
						if (pictureAttribute == null)
						{
							pictureAttribute = new GenericAttribute
							{
								EntityId = customer.Id,
								Key = "PictureId",
								KeyGroup = "Customer",
								Value = model.PictureId.ToString(CultureInfo.InvariantCulture),
								StoreId = 0
							};
							_genericAttributeService.InsertAttribute(pictureAttribute);
						}
						else
						{
							pictureAttribute.Value = model.PictureId.ToString(CultureInfo.InvariantCulture);
							_genericAttributeService.UpdateAttribute(pictureAttribute);
						}

						var companyAttributeList = _genericAttributeService.GetAttributesForEntity(_workContext.CurrentCustomer.Id, "Customer");

						//var companyNumber = _workContext.CurrentCustomer.CompanyId;
						//model.CompanyNumber = !string.IsNullOrEmpty(companyNumber.ToString(CultureInfo.InvariantCulture)) ? companyNumber : 0;
						//InsertUpdateAttribute(customer, attributeList.ToList(), "CompanyNumber",model.CompanyNumber.ToString(CultureInfo.InvariantCulture));
						//InsertUpdateAttribute(customer, attributeList.ToList(), "PersonalNumber", model.PersonalNumber.ToString(CultureInfo.InvariantCulture));

						var contractTypeId = GetAttributeValue(companyAttributeList.ToList(), "ContractTypeId");
						model.ContractTypeId = !string.IsNullOrEmpty(contractTypeId) ? Convert.ToInt32(contractTypeId) : 0;
						InsertUpdateAttribute(customer, attributeList.ToList(), "ContractTypeId", model.ContractTypeId.ToString(CultureInfo.InvariantCulture));


						//if (model.AuthorizationTypeId == 2)
						//{
						//	var parentCustomerAttributes	= _genericAttributeService.GetAttributesForEntity(_workContext.CurrentCustomer.Id, "Customer");
						//	var parentLoyaltyPercentage = GetAttributeValue(parentCustomerAttributes.ToList(), "LoyalityPercentage");
						//	model.LoyalityPercentage = !string.IsNullOrEmpty(parentLoyaltyPercentage) ? Convert.ToInt32(parentLoyaltyPercentage) : 0;
						//	InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage",model.LoyalityPercentage.ToString(CultureInfo.InvariantCulture));
						//}
						//else
						//	InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", "0");													 

						var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
						var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeScope);

						switch (model.ContractTypeId)
						{
							case 1:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractA.ToString(CultureInfo.InvariantCulture));
								break;
							case 2:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractB.ToString(CultureInfo.InvariantCulture));
								break;
							case 3:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractC.ToString(CultureInfo.InvariantCulture));
								break;
							case 4:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractD.ToString(CultureInfo.InvariantCulture));
								break;
							case 26:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractZ.ToString(CultureInfo.InvariantCulture));
								break;
						}

						//Save Customer Image
						if (model.CustomerPictureId > 0)
						{
							InsertUpdateAttribute(customer, attributeList.ToList(), "CustomerPictureId",
							model.CustomerPictureId.ToString(CultureInfo.InvariantCulture));
						}

						InsertUpdateAttribute(customer, attributeList.ToList(), "ActivationDate", model.ActivationDate.ToString());
						InsertUpdateAttribute(customer, attributeList.ToList(), "EndDate", model.EndDate.ToString());
						InsertUpdateAttribute(customer, attributeList.ToList(), "AuthorizationTypeId", model.AuthorizationTypeId.ToString(CultureInfo.InvariantCulture));
						InsertUpdateAttribute(customer, attributeList.ToList(), "StatusCodeId", model.StatusCodeTypeId.ToString(CultureInfo.InvariantCulture));
					}

					//newsletter subscriptions
					if (!String.IsNullOrEmpty(customer.Email))
					{
						var allStores = _storeService.GetAllStores();
						foreach (var store in allStores)
						{
							var newsletterSubscription = _newsLetterSubscriptionService
								 .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
							if (model.SelectedNewsletterSubscriptionStoreIds != null &&
								 model.SelectedNewsletterSubscriptionStoreIds.Contains(store.Id))
							{
								//subscribed
								if (newsletterSubscription == null)
								{
									_newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
									{
										NewsLetterSubscriptionGuid = Guid.NewGuid(),
										Email = customer.Email,
										Active = true,
										StoreId = store.Id,
										CreatedOnUtc = DateTime.UtcNow
									});
								}
							}
							else
							{
								//not subscribed
								if (newsletterSubscription != null)
								{
									_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletterSubscription);
								}
							}
						}
					}


					//customer roles
					foreach (var customerRole in allCustomerRoles)
					{
						//ensure that the current customer cannot add/remove to/from "Administrators" system role
						//if he's not an admin himself
						if (customerRole.SystemName == SystemCustomerRoleNames.Administrators &&
							 !_workContext.CurrentCustomer.IsAdmin())
							continue;

						if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
						{
							//new role
							if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) == 0)
								customer.CustomerRoles.Add(customerRole);
						}
						else
						{
							//prevent attempts to delete the administrator role from the user, if the user is the last active administrator
							if (customerRole.SystemName == SystemCustomerRoleNames.Administrators && !SecondAdminAccountExists(customer))
							{
								ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.DeleteRole"));
								continue;
							}

							//remove role
							if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) > 0)
								customer.CustomerRoles.Remove(customerRole);
						}
					}
					_customerService.UpdateCustomer(customer);

					//Assign Company Address
					var companyAddress = _workContext.CurrentCustomer.Addresses.FirstOrDefault();
					if (companyAddress != null)
					{
						if (customer.Addresses.Any())
						{
							var addressList = customer.Addresses.ToList();
							foreach (var customerAddress in addressList)
							{
								customer.RemoveAddress(customerAddress);
								_customerService.UpdateCustomer(customer);
							}
						}

						customer.Addresses.Add(companyAddress);
						_customerService.UpdateCustomer(customer);
						var defaultAddress = customer.Addresses.FirstOrDefault();
						customer.BillingAddress = defaultAddress;
						_customerService.UpdateCustomer(customer);
					}


					//ensure that a customer with a vendor associated is not in "Administrators" role
					//otherwise, he won't have access to the other functionality in admin area
					if (customer.IsAdmin() && customer.VendorId > 0)
					{
						customer.VendorId = 0;
						_customerService.UpdateCustomer(customer);
						ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminCouldNotbeVendor"));
					}

					//ensure that a customer in the Vendors role has a vendor account associated.
					//otherwise, he will have access to ALL products
					if (customer.IsVendor() && customer.VendorId == 0)
					{
						var vendorRole = customer
							 .CustomerRoles
							 .FirstOrDefault(x => x.SystemName == SystemCustomerRoleNames.Vendors);
						customer.CustomerRoles.Remove(vendorRole);
						_customerService.UpdateCustomer(customer);
						ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.CannotBeInVendoRoleWithoutVendorAssociated"));
					}


					//activity log
					_customerActivityService.InsertActivity("EditCustomer", _localizationService.GetResource("ActivityLog.EditCustomer"), customer.Id);

					SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Updated"));
					if (continueEditing)
					{
						//selected tab
						//SaveSelectedTabName();

						return RedirectToAction("Edit", new { id = customer.Id });
					}
					return RedirectToAction("List");
				}
				catch (Exception exc)
				{
					ErrorNotification(exc.Message, false);
				}
			}


			//If we got this far, something failed, redisplay form
			PrepareCustomerModel(model, customer, true);
			if (!string.IsNullOrEmpty(customer.Username))
				model.PersonalNumber = customer.Username;

			if (!string.IsNullOrEmpty(customer.CompanyId.ToString(CultureInfo.InvariantCulture)))
				model.CompanyNumber = customer.CompanyId;
			var customerAttributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
			var currentPassword = GetAttributeValue(customerAttributeList.ToList(), "AccessWord");
			model.CurrentPassword = !string.IsNullOrEmpty(currentPassword) ? currentPassword : "";

			return View("~/Plugins/PostXCustom/Views/CompanyRepresentative/Edit.cshtml", model);
		}

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("changepassword")]
		public virtual ActionResult ChangePassword(CustomerModel model)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			//ensure that the current customer cannot change passwords of "Administrators" if he's not an admin himself
			if (customer.IsAdmin() && !_workContext.CurrentCustomer.IsAdmin())
			{
				ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.OnlyAdminCanChangePassword"));
				return RedirectToAction("Edit", new { id = customer.Id });
			}

			if (ModelState.IsValid)
			{
				var changePassRequest = new ChangePasswordRequest(model.Username,
					 false, _customerSettings.DefaultPasswordFormat, model.Password);
				var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
				if (changePassResult.Success)
				{
					var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
					InsertUpdateAttribute(customer, attributeList.ToList(), "AccessWord", model.Password);
					SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.PasswordChanged"));
				}
				else
					foreach (var error in changePassResult.Errors)
						ErrorNotification(error);
			}

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("markVatNumberAsValid")]
		public virtual ActionResult MarkVatNumberAsValid(CustomerModel model)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			_genericAttributeService.SaveAttribute(customer,
				 SystemCustomerAttributeNames.VatNumberStatusId,
				 (int)VatNumberStatus.Valid);

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("markVatNumberAsInvalid")]
		public virtual ActionResult MarkVatNumberAsInvalid(CustomerModel model)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			_genericAttributeService.SaveAttribute(customer,
				 SystemCustomerAttributeNames.VatNumberStatusId,
				 (int)VatNumberStatus.Invalid);

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("remove-affiliate")]
		public virtual ActionResult RemoveAffiliate(CustomerModel model)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			customer.AffiliateId = 0;
			_customerService.UpdateCustomer(customer);

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		[HttpPost]
		public virtual ActionResult Delete(int id)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			try
			{
				//prevent attempts to delete the user, if it is the last active administrator
				if (customer.IsAdmin() && !SecondAdminAccountExists(customer))
				{
					ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.DeleteAdministrator"));
					return RedirectToAction("Edit", new { id = customer.Id });
				}

				//ensure that the current customer cannot delete "Administrators" if he's not an admin himself
				if (customer.IsAdmin() && !_workContext.CurrentCustomer.IsAdmin())
				{
					ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.OnlyAdminCanDeleteAdmin"));
					return RedirectToAction("Edit", new { id = customer.Id });
				}

				//delete
				_customerService.DeleteCustomer(customer);

				//remove newsletter subscription (if exists)
				foreach (var store in _storeService.GetAllStores())
				{
					var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
					if (subscription != null)
						_newsLetterSubscriptionService.DeleteNewsLetterSubscription(subscription);
				}

				//activity log
				_customerActivityService.InsertActivity("DeleteCustomer", _localizationService.GetResource("ActivityLog.DeleteCustomer"), customer.Id);

				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Deleted"));
				return RedirectToAction("List");
			}
			catch (Exception exc)
			{
				ErrorNotification(exc.Message);
				return RedirectToAction("Edit", new { id = customer.Id });
			}
		}


		[HttpPost, ActionName("Edit")]
		[FormValueRequired("send-welcome-message")]
		public virtual ActionResult SendWelcomeMessage(CustomerModel model)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			_workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

			SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendWelcomeMessage.Success"));

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("resend-activation-message")]
		public virtual ActionResult ReSendActivationMessage(CustomerModel model)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			//email validation message
			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
			_workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

			SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.ReSendActivationMessage.Success"));

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		public virtual ActionResult SendEmail(CustomerModel model)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			try
			{
				if (String.IsNullOrWhiteSpace(customer.Email))
					throw new NopException("Customer email is empty");
				if (!CommonHelper.IsValidEmail(customer.Email))
					throw new NopException("Customer email is not valid");
				if (String.IsNullOrWhiteSpace(model.SendEmail.Subject))
					throw new NopException("Email subject is empty");
				if (String.IsNullOrWhiteSpace(model.SendEmail.Body))
					throw new NopException("Email body is empty");

				var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId) ??
										 _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
				if (emailAccount == null)
					throw new NopException("Email account can't be loaded");
				var email = new QueuedEmail
				{
					Priority = QueuedEmailPriority.High,
					EmailAccountId = emailAccount.Id,
					FromName = emailAccount.DisplayName,
					From = emailAccount.Email,
					ToName = customer.GetFullName(),
					To = customer.Email,
					Subject = model.SendEmail.Subject,
					Body = model.SendEmail.Body,
					CreatedOnUtc = DateTime.UtcNow,
					DontSendBeforeDateUtc = (model.SendEmail.SendImmediately || !model.SendEmail.DontSendBeforeDate.HasValue) ?
						 null : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.SendEmail.DontSendBeforeDate.Value)
				};
				_queuedEmailService.InsertQueuedEmail(email);
				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendEmail.Queued"));
			}
			catch (Exception exc)
			{
				ErrorNotification(exc.Message);
			}

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		public virtual ActionResult SendPm(CustomerModel model)
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
			//	return new HttpUnauthorizedResult();

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToAction("List");

			try
			{
				if (!_forumSettings.AllowPrivateMessages)
					throw new NopException("Private messages are disabled");
				if (customer.IsGuest())
					throw new NopException("Customer should be registered");
				if (String.IsNullOrWhiteSpace(model.SendPm.Subject))
					throw new NopException("PM subject is empty");
				if (String.IsNullOrWhiteSpace(model.SendPm.Message))
					throw new NopException("PM message is empty");


				var privateMessage = new PrivateMessage
				{
					StoreId = _storeContext.CurrentStore.Id,
					ToCustomerId = customer.Id,
					FromCustomerId = _workContext.CurrentCustomer.Id,
					Subject = model.SendPm.Subject,
					Text = model.SendPm.Message,
					IsDeletedByAuthor = false,
					IsDeletedByRecipient = false,
					IsRead = false,
					CreatedOnUtc = DateTime.UtcNow
				};

				_forumService.InsertPrivateMessage(privateMessage);
				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendPM.Sent"));
			}
			catch (Exception exc)
			{
				ErrorNotification(exc.Message);
			}

			return RedirectToAction("Edit", new { id = customer.Id });
		}


		#endregion

		#region methods - Company Representative Customer Mapping

		public ActionResult AssignCompanyRepresentativeTab(int customerId = 0)
		{
			if (ControllerContext == null)
			{
				var context = new ControllerContext(System.Web.HttpContext.Current.Request.RequestContext, this);
				ControllerContext = context;
			}

			var companyRepresentativeCustomerMap = _companyRepresentativeCustomerMappingService.GetByCustomerId(customerId);
			var model = new CompanyRepresentativeCustomerModel();

			if (companyRepresentativeCustomerMap == null)
			{
				model.AssociatedCustomerId = customerId;
				model.RecordId = 0;
				model.CompanyRepresentativeId = 0;
			}
			else
			{
				model.AssociatedCustomerId = customerId;
				model.RecordId = companyRepresentativeCustomerMap.Id;
				model.CompanyRepresentativeId = companyRepresentativeCustomerMap.CompanyRepresentativeId;
			}


			var customerList = _customerService.GetAllCustomers().Where(x => x.CustomerRoles.FirstOrDefault(z => z.SystemName.ToLower() == "companyrepresentative") != null).ToList();

			if (!customerList.Any())
				return PartialView("~/Plugins/PostXCustom/Views/CompanyRepresentative/AssignCompanyRepresentativeTab.cshtml", model);

			var salesPersonList = new List<SelectListItem> { new SelectListItem { Text = "Please select", Value = "0" } };
			salesPersonList.AddRange(customerList.Select(item => new SelectListItem { Text = item.GetFullName(), Value = item.Id.ToString(CultureInfo.InvariantCulture) }));

			model.CompanyRepresentativeList = salesPersonList;
			return PartialView("~/Plugins/PostXCustom/Views/CompanyRepresentative/AssignCompanyRepresentativeTab.cshtml", model);
		}

		[ValidateInput(false)]
		public virtual ActionResult AssignCompanyRepresentative(int recordId, int associatedCustomerId, int companyRepresentativeId)
		{
			if (associatedCustomerId == 0)
			{
				return Json(new { Result = false, Message = "Sorry! request could not be processed at the moment. Please refresh the page and try again." }, JsonRequestBehavior.AllowGet);
			}

			var salesPersonCustomerMap = _companyRepresentativeCustomerMappingService.GetByCustomerId(associatedCustomerId);

			if (salesPersonCustomerMap == null && companyRepresentativeId == 0)
				return Json(new { Result = false, Message = "Please select a SalesPerson." }, JsonRequestBehavior.AllowGet);


			if (salesPersonCustomerMap == null && companyRepresentativeId > 0)
			{
				salesPersonCustomerMap = new CompanyRepresentativeCustomerMapping
				{
					AssociatedCustomerId = associatedCustomerId,
					CompanyRepresentativeId = companyRepresentativeId
				};
				_companyRepresentativeCustomerMappingService.Insert(salesPersonCustomerMap);
				return Json(new { Result = true, Message = "SalesPerson Assigned Successfuly." }, JsonRequestBehavior.AllowGet);
			}

			if (salesPersonCustomerMap != null && companyRepresentativeId == 0)
			{
				_companyRepresentativeCustomerMappingService.Delete(salesPersonCustomerMap);
				return Json(new { Result = true, Message = "SalesPerson UnAssigned Successfuly." }, JsonRequestBehavior.AllowGet);
			}

			if (salesPersonCustomerMap != null && companyRepresentativeId > 0)
			{
				salesPersonCustomerMap.CompanyRepresentativeId = companyRepresentativeId;
				_companyRepresentativeCustomerMappingService.Update(salesPersonCustomerMap);
				return Json(new { Result = true, Message = "SalesPerson Updated Successfuly." }, JsonRequestBehavior.AllowGet);
			}

			return Json(new { Result = false, Message = "Sorry! request could not be processed at the moment. Please refresh the page and try again." }, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Manual Customer Push To LS(LightSpeed)

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("pushcustomer")]
		public ActionResult LSCustomerPush(CustomerModel model)
		{
			#region Push Customer to CMS
			var customer = _customerService.GetCustomerById(model.Id);

			var checkCmsCustomerMap = _cmsNopCustomerMappingService.GetByNopCustomerId(customer.Id);
			if (checkCmsCustomerMap != null && checkCmsCustomerMap.Deleted == false)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Customer Push :: Error :: Customer already pushed to CMS :: Username :: " + customer.Username + " :: LS Customer Id :: " + checkCmsCustomerMap.CmsCustomerId);
				_customCodeService.SendErrorEmail("Manual Customer Push :: Error :: Customer already pushed to CMS :: Username :: " + customer.Username + " :: LS Customer Id :: " + checkCmsCustomerMap.CmsCustomerId, "Manual Customer Create :: Error :: Customer already pushed to CMS :: Username" + customer.Username, string.Empty);
				ErrorNotification("Manual Customer Push :: Error :: Customer already pushed to CMS :: Username :: " + customer.Username + " :: LS Customer Id :: " + checkCmsCustomerMap.CmsCustomerId);
				return RedirectToAction("Edit", new { id = customer.Id });
			}

			var result = CreateCmsCustomer(customer);
			if (!string.IsNullOrEmpty(result))
			{
				if (checkCmsCustomerMap == null)
				{
					checkCmsCustomerMap = new CmsCustomerNopCustomerMapping
					{
						NopCustomerId = customer.Id,
						CmsCustomerId = Convert.ToInt32(result),
						CreatedOnUtc = DateTime.UtcNow,
						UpdatedOnUtc = DateTime.UtcNow,
						Deleted = false
					};

					_cmsNopCustomerMappingService.Insert(checkCmsCustomerMap);
					CreateCustomerLoyaltyCard(customer, checkCmsCustomerMap.CmsCustomerId);
				}
				else
				{
					checkCmsCustomerMap.CmsCustomerId = Convert.ToInt32(result);
					checkCmsCustomerMap.UpdatedOnUtc = DateTime.UtcNow;
					checkCmsCustomerMap.Deleted = false;
					_cmsNopCustomerMappingService.Update(checkCmsCustomerMap);
					CreateCustomerLoyaltyCard(customer, checkCmsCustomerMap.CmsCustomerId);
				}

				customer.Active = true;
				_customerService.UpdateCustomer(customer);
				//var cmsNopCustomerMap = new CmsCustomerNopCustomerMapping
				//{
				//	NopCustomerId = customer.Id,
				//	CmsCustomerId = Convert.ToInt32(result),
				//	CreatedOnUtc = DateTime.UtcNow,
				//	UpdatedOnUtc = DateTime.UtcNow
				//};

				//_cmsNopCustomerMappingService.Insert(cmsNopCustomerMap);
				//CreateCustomerLoyaltyCard(customer, cmsNopCustomerMap.CmsCustomerId);
				SuccessNotification("Customer pushed to CMS successfully.");
				return RedirectToAction("Edit", new { id = customer.Id });
			}
			ErrorNotification("Manual Customer Push :: Error :: Customer push to CMS failed :: Username" + customer.Username);
			#endregion

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		public string CreateCmsCustomer(Customer customer)
		{
			try
			{
				string token = GetToken();

				var clientName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
				var clientLastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);

				if (string.IsNullOrEmpty(token))
				{
					return "";
				}

				string url = @"https://euc1.posios.com/PosServer/rest/core/customer";
				var postData = new StringBuilder();

				postData.Append("{\"city\":\"" + string.Empty + "\",");
				postData.Append("\"companyId\":32123,");
				postData.Append("\"country\":\"" + "BE" + "\",");
				postData.Append("\"credits\":0,");

				postData.Append("\"dateOfBirth\":\"\",");
				postData.Append("\"email\":\"" + customer.Email + "\",");
				//postData.Append("\"firstName\":\"" + "0 reward points available." + "\",");
				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"language\":\"" + "nl_BE" + "\",");
				//postData.Append("\"lastName\":\"" + customer.GetFullName() + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\",");
				postData.Append("\"modificationTime\":\"\",");
				postData.Append("\"note\":\"" + "Test Note" + "\",");
				postData.Append("\"street\":\"\",");
				postData.Append("\"streetNumber\":\"\",");
				postData.Append("\"telephone\":\"\",");
				postData.Append("\"uuid\":\"\",");
				postData.Append("\"vatNumber\":\"\",");
				postData.Append("\"zip\":\"\"}");


				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Customer#: " + customer.Id, postData.ToString());

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				return responseData;
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Customer Push :: Error :: Customer push to CMS failed :: Username" + customer.Username, ex.Message + "  ||  " + ex.StackTrace);
				_customCodeService.SendErrorEmail("Manual Customer Push :: Error :: Customer push to CMS failed :: Username" + customer.Username, ex.Message, ex.StackTrace);
				return string.Empty;
			}
		}

		public string GetToken()
		{
			try
			{
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				var token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Customer Push :: Error :: Error generating Token", ex.Message + "  ||  " + ex.StackTrace);
				_customCodeService.SendErrorEmail("Manual Customer Push :: Error :: Error generating Token", ex.Message, ex.StackTrace);
				return string.Empty;
			}
		}

		public string CreateCustomerLoyaltyCard(Customer customer, int cmsCustomerId)
		{
			try
			{
				string token = GetToken();
				string url = @"https://euc1.posios.com/PosServer/rest/core/customer/" + cmsCustomerId + "/loyaltycard";
				var postData = new StringBuilder();
				postData.Append("{\"barcode\":\"" + customer.Username + "\"}");

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				return responseData;
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Customer Push :: Error ::Error creating Loyalty Card for Nop Customer :: Username" + customer.Username + " :: LS CustomerId :: " + cmsCustomerId, ex.Message + "  ||  " + ex.StackTrace);
				_customCodeService.SendErrorEmail("Manual Customer Push :: Error :: Error creating Loyalty Card for Nop Customer :: Username" + customer.Username + " :: LS CMS Customer ID :: " + cmsCustomerId, ex.Message, ex.StackTrace);
				return string.Empty;
			}
		}


		#endregion

		#region Export / Import

		[HttpPost, ActionName("List")]
		[FormValueRequired("exportcsv-all")]
		public virtual ActionResult ExportExcelAll(CustomerListModel model)
		{
			var searchDayOfBirth = 0;
			int searchMonthOfBirth = 0;
			if (!String.IsNullOrWhiteSpace(model.SearchDayOfBirth))
				searchDayOfBirth = Convert.ToInt32(model.SearchDayOfBirth);
			if (!String.IsNullOrWhiteSpace(model.SearchMonthOfBirth))
				searchMonthOfBirth = Convert.ToInt32(model.SearchMonthOfBirth);

			var salesPersonCustomers = new List<int>();

			// If salesperson is login
			if (!_workContext.CurrentCustomer.IsAdmin())
			{
				salesPersonCustomers = _companyRepresentativeCustomerMappingService.GetCustomersBySalesPersonId(_workContext.CurrentCustomer.Id);
			}


			var customers = _companyRepresentativeCustomerMappingService.GetAllCustomers(
				 customerRoleIds: model.SearchCustomerRoleIds.ToArray(),
				 email: model.SearchEmail,
				 username: model.SearchUsername,
				 firstName: model.SearchFirstName,
				 lastName: model.SearchLastName,
				 dayOfBirth: searchDayOfBirth,
				 monthOfBirth: searchMonthOfBirth,
				 company: model.SearchCompany,
				 phone: model.SearchPhone,
				 zipPostalCode: model.SearchZipPostalCode,
				 ipAddress: model.SearchIpAddress,
				 loadOnlyWithShoppingCart: false,
				 customerIds: salesPersonCustomers);

			try
			{
				string result = _exportManager.ExportCustomerCardsToTxt(customers);
				string fileName = String.Format("customer_card_{0}_{1}.csv", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
				return File(Encoding.UTF8.GetBytes(result), MimeTypes.TextCsv, fileName);
			}
			catch (Exception exc)
			{
				ErrorNotification(exc);
				return RedirectToAction("List");
			}
		}

		[HttpPost]
		public virtual ActionResult ExportExcelSelected(string selectedIds)
		{
			var customers = new List<Customer>();
			if (selectedIds != null)
			{
				var ids = selectedIds
					 .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
					 .Select(x => Convert.ToInt32(x))
					 .ToArray();
				customers.AddRange(_customerService.GetCustomersByIds(ids));
			}

			try
			{
				string result = _exportManager.ExportCustomerCardsToTxt(customers);
				string fileName = String.Format("customer_card_{0}_{1}.csv", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
				return File(Encoding.UTF8.GetBytes(result), MimeTypes.TextCsv, fileName);
			}
			catch (Exception exc)
			{
				ErrorNotification(exc);
				return RedirectToAction("List");
			}
		}
		#endregion
	}
}