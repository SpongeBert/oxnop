﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Catalog;
using Nop.Services.Configuration;
using Nop.Services.CustomService;
using Nop.Services.Logging;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class TestController : BasePluginController
	{

		private readonly IProductService _productService;				
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly ICategoryService _categoryService;
		private readonly ICmsNopCategoryMappingService _cmsNopCategoryMappingService;
		private readonly ILogger _logger;
		private readonly ICustomCodeService _customCodeService;
		private readonly ISettingService _settingService;

		public TestController(IProductService productService, ICmsNopProductMappingService cmsNopProductMappingService, ICategoryService categoryService,
			 ICmsNopCategoryMappingService cmsNopCategoryMappingService, ILogger logger, ICustomCodeService customCodeService, ISettingService settingService)
		{
			_productService = productService;			
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_categoryService = categoryService;
			_cmsNopCategoryMappingService = cmsNopCategoryMappingService;
			_logger = logger;
			_customCodeService = customCodeService;
			_settingService = settingService;
		}

		public string GetToken()
		{
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";
			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			var token = json.GetValue("token").Value<string>();
			return token;
		}


		public ActionResult TestCategoryUrl()
		{
			try
			{
				string html = string.Empty;
				string url = @"https://euc1.posios.com/PosServer/rest/inventory/productgroup";

				var dataList = new List<Category>();

				var token = GetToken();

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.AutomaticDecompression = DecompressionMethods.GZip;

				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				using (Stream stream = response.GetResponseStream())
				using (StreamReader reader = new StreamReader(stream))
				{
					html = reader.ReadToEnd();
					JArray jsonArray = JArray.Parse(html);
					foreach (var item in jsonArray)
					{
						var jsonText = JsonConvert.DeserializeObject<CmsCategoryApiModel>(item.ToString());
						if (jsonText.name.ToLower() == "dagaanbiedingen" || jsonText.name.ToLower() == "broodjes" || jsonText.name.ToLower() == "dranken")
						{
							//Check if category exist							
							var checkExistingCategory = _cmsNopCategoryMappingService.GetByCmsCategoryName(jsonText.name);
							if (checkExistingCategory == null)
							{
								var newCategory = new Category
								{
									Name = jsonText.name,
									Published = jsonText.visible,
									Deleted = false,
									DisplayOrder = jsonText.sequence,
									CreatedOnUtc = DateTime.UtcNow,
									UpdatedOnUtc = DateTime.UtcNow
								};

								dataList.Add(newCategory);
								continue;
							}
							var category = _categoryService.GetCategoryById(checkExistingCategory.NopCategoryId);
							if (category != null)
							{
								category.Name = jsonText.name;
								category.Published = jsonText.visible;
								category.DisplayOrder = jsonText.sequence;

								dataList.Add(category);
							}
						}
					}
				}

				return Json(new { success = true, message = "", data = dataList }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				return Json(new { success = false, message = ex.Message + " || " + ex.StackTrace }, JsonRequestBehavior.AllowGet);
			}

			
		}
	}
}
