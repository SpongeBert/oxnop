﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Services.ExportImport.Help;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Globalization;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class VATReportController : BasePluginController
	{
		#region Fields													
		private readonly IWorkContext _workContext;
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICustomerService _customerService;
		private readonly IGenericAttributeService _genericAttributeService;
		#endregion

		#region Constructors

		public VATReportController(IWorkContext workContext, ICmsNopCustomerMappingService cmsNopCustomerMappingService,
			ICustomerService customerService, IGenericAttributeService genericAttributeService)
		{
			_workContext = workContext;
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_customerService = customerService;
			_genericAttributeService = genericAttributeService;
		}

		#endregion


		public ActionResult List()
		{
			var model = new VatReportModelList { OrderDate = DateTime.Now.ToString("dd/MM/yyyy") };
			return View("~/Plugins/PostXCustom/Views/VatReport/List.cshtml", model);
		}

		[HttpPost]
		public virtual ActionResult GenerateVatReportList(DataSourceRequest command, VatReportModelList model1)
		{
			var model = new VatReportModelList();
			var offsetValue = 0;
			var orderDate = "";
			var fromDate = "";
			var toDate = "";

			if (model1.OrderDate != null)
				orderDate = Convert.ToDateTime(model1.OrderDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//model1.OrderDate.Value.ToString("yyyy-MM-dd");

			if (model1.OrderDate == null && model1.FromDate == null && model1.ToDate == null)
				orderDate = Convert.ToDateTime(model1.OrderDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//DateTime.Now.ToString("yyyy-MM-dd");

			if (model1.OrderDate == null && model1.FromDate != null && model1.ToDate != null)
			{
				fromDate = Convert.ToDateTime(model1.FromDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//model1.FromDate.Value.ToString("yyyy-MM-dd");
				toDate = Convert.ToDateTime(model1.ToDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//model1.ToDate.Value.ToString("yyyy-MM-dd");
			}


			for (int count = 0; count < int.MaxValue; count++)
			{
				string html;
				string url = @"https://euc1.posios.com/PosServer/rest/financial/receipt?";
				if (model1.OrderDate != null)
					url = url + "date=" + orderDate;

				if (model1.OrderDate == null && model1.FromDate == null && model1.ToDate == null)
					url = url + "date=" + orderDate;

				if (model1.OrderDate == null && model1.FromDate != null && model1.ToDate != null)
					url = url + "from=" + fromDate + "&to=" + toDate;

				if (!string.IsNullOrEmpty(model1.CustomerId))
					url = url + "&customerId=" + model1.CustomerId;

				if (!string.IsNullOrEmpty(model1.ReceiptId))
					url = url + "&receiptId=" + model1.ReceiptId;

				if (offsetValue > 0)
				{
					url = url + "&offset=" + offsetValue;
				}

				var token = GetToken();

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.AutomaticDecompression = DecompressionMethods.GZip;

				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				using (Stream stream = response.GetResponseStream())
				using (StreamReader reader = new StreamReader(stream))
				{
					html = reader.ReadToEnd();
					var jsonList = JsonConvert.DeserializeObject<CmsReceiptsListModel>(html);
					if (!jsonList.results.Any()) { break; }

					offsetValue = offsetValue + 50;

					foreach (var item in jsonList.results)
					{
						var reportItem = new VatReportModel
						{
							CustomerId = item.customerId,
							CreateDate = Convert.ToDateTime(item.creationDate),
							OrderTotal = item.total,
							ReceiptId = item.id
						};

						var total6Percent = 0M;
						var total12Percent = 0M;
						var total21Percent = 0M;

						var orderTotal6Percent = 0M;
						var orderTotal12Percent = 0M;
						var orderTotal21Percent = 0M;

						var cashPayment = 0M;
						var nonCashPayment = 0M;

						foreach (var orderItem in item.items)
						{
							if (orderItem.vatPercentage == 6)
							{
								total6Percent = total6Percent + (orderItem.totalPrice - orderItem.totalPriceWithoutVat);
								orderTotal6Percent = orderTotal6Percent + orderItem.totalPrice;
							}

							if (orderItem.vatPercentage == 12)
							{
								total12Percent = total12Percent + (orderItem.totalPrice - orderItem.totalPriceWithoutVat);
								orderTotal12Percent = orderTotal12Percent + orderItem.totalPrice;
							}

							if (orderItem.vatPercentage == 21)
							{
								total21Percent = total21Percent + (orderItem.totalPrice - orderItem.totalPriceWithoutVat);
								orderTotal21Percent = orderTotal21Percent + orderItem.totalPrice;
							}
						}
						reportItem.VAT6Total = total6Percent;
						reportItem.VAT12Total = total12Percent;
						reportItem.VAT21Total = total21Percent;
						reportItem.VAT6OrderTotal = orderTotal6Percent;
						reportItem.VAT12OrderTotal = orderTotal12Percent;
						reportItem.VAT21OrderTotal = orderTotal21Percent;

						foreach (var paymentItem in item.payments)
						{
							if (paymentItem.paymentTypeId == 36191)
							{
								cashPayment = cashPayment + paymentItem.amount;
							}
							else
							{
								nonCashPayment = nonCashPayment + paymentItem.amount;
							}
						}

						reportItem.OrderTotalCash = cashPayment;
						reportItem.OrderTotalNonCash = nonCashPayment;

						var cmsNopCustomer = _cmsNopCustomerMappingService.GetByCmsCustomerId(item.customerId);
						if (cmsNopCustomer != null)
						{
							var nopCustomer = _customerService.GetCustomerById(cmsNopCustomer.NopCustomerId);
							if (nopCustomer != null)
							{
								reportItem.Username = nopCustomer.Username;
								var attributeList = _genericAttributeService.GetAttributesForEntity(nopCustomer.Id, "Customer");

								var vatRegimeId = GetAttributeValue(attributeList.ToList(), "VatRegimeId");
								reportItem.VatRegime = !string.IsNullOrEmpty(vatRegimeId) ? ((VatRegimeType)Convert.ToInt32(vatRegimeId)).ToString() : "";
							}
						}

						model.VatDataList.Add(reportItem);
					}
				}
			}

			var gridModel = new DataSourceResult
			{
				Data = model.VatDataList.Any() ? model.VatDataList.OrderByDescending(x => x.CreateDate).ToList() : model.VatDataList,
				Total = model.VatDataList.Count
			};
			return Json(gridModel);
		}



		[HttpPost, ActionName("List")]
		[FormValueRequired("exportexcel-all")]
		public virtual ActionResult GenerateVatReportExcel(VatReportModelList model1)
		{
			var model = new VatReportModelList();
			var offsetValue = 0;
			var orderDate = "";
			var fromDate = "";
			var toDate = "";


			if (model1.OrderDate != null)
				orderDate = Convert.ToDateTime(model1.OrderDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//model1.OrderDate.Value.ToString("yyyy-MM-dd");

			if (model1.OrderDate == null && model1.FromDate == null && model1.ToDate == null)
				orderDate = Convert.ToDateTime(model1.OrderDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//DateTime.Now.ToString("yyyy-MM-dd");

			if (model1.OrderDate == null && model1.FromDate != null && model1.ToDate != null)
			{
				fromDate = Convert.ToDateTime(model1.FromDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//model1.FromDate.Value.ToString("yyyy-MM-dd");
				toDate = Convert.ToDateTime(model1.ToDate, CultureInfo.GetCultureInfo("nl-BE")).ToString("yyyy-MM-dd");//model1.ToDate.Value.ToString("yyyy-MM-dd");
			}




			for (int count = 0; count < int.MaxValue; count++)
			{
				string html;
				string url = @"https://euc1.posios.com/PosServer/rest/financial/receipt?";
				if (model1.OrderDate != null)
					url = url + "date=" + orderDate;

				if (model1.OrderDate == null && model1.FromDate == null && model1.ToDate == null)
					url = url + "date=" + orderDate;

				if (model1.OrderDate == null && model1.FromDate != null && model1.ToDate != null)
					url = url + "from=" + fromDate + "&to=" + toDate;

				if (!string.IsNullOrEmpty(model1.CustomerId))
					url = url + "&customerId=" + model1.CustomerId;

				if (!string.IsNullOrEmpty(model1.ReceiptId))
					url = url + "&receiptId=" + model1.ReceiptId;

				if (offsetValue > 0)
				{
					url = url + "&offset=" + offsetValue;
				}

				var token = GetToken();

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.AutomaticDecompression = DecompressionMethods.GZip;

				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				using (Stream stream = response.GetResponseStream())
				using (StreamReader reader = new StreamReader(stream))
				{
					html = reader.ReadToEnd();
					var jsonList = JsonConvert.DeserializeObject<CmsReceiptsListModel>(html);
					if (!jsonList.results.Any()) { break; }

					offsetValue = offsetValue + 50;

					foreach (var item in jsonList.results)
					{
						var reportItem = new VatReportModel
						{
							CustomerId = item.customerId,
							CreateDate = Convert.ToDateTime(item.creationDate),
							OrderTotal = item.total,
							ReceiptId = item.id
						};

						var total6Percent = 0M;
						var total12Percent = 0M;
						var total21Percent = 0M;

						var orderTotal6Percent = 0M;
						var orderTotal12Percent = 0M;
						var orderTotal21Percent = 0M;

						var cashPayment = 0M;
						var nonCashPayment = 0M;

						foreach (var orderItem in item.items)
						{
							if (orderItem.vatPercentage == 6)
							{
								total6Percent = total6Percent + (orderItem.totalPrice - orderItem.totalPriceWithoutVat);
								orderTotal6Percent = orderTotal6Percent + orderItem.totalPrice;
							}

							if (orderItem.vatPercentage == 12)
							{
								total12Percent = total12Percent + (orderItem.totalPrice - orderItem.totalPriceWithoutVat);
								orderTotal12Percent = orderTotal12Percent + orderItem.totalPrice;
							}

							if (orderItem.vatPercentage == 21)
							{
								total21Percent = total21Percent + (orderItem.totalPrice - orderItem.totalPriceWithoutVat);
								orderTotal21Percent = orderTotal21Percent + orderItem.totalPrice;
							}
						}

						reportItem.VAT6Total = total6Percent;
						reportItem.VAT12Total = total12Percent;
						reportItem.VAT21Total = total21Percent;
						reportItem.VAT6OrderTotal = orderTotal6Percent;
						reportItem.VAT12OrderTotal = orderTotal12Percent;
						reportItem.VAT21OrderTotal = orderTotal21Percent;

						foreach (var paymentItem in item.payments)
						{
							if (paymentItem.paymentTypeId == 36191)
							{
								cashPayment = cashPayment + paymentItem.amount;
							}
							else
							{
								nonCashPayment = nonCashPayment + paymentItem.amount;
							}
						}

						reportItem.OrderTotalCash = cashPayment;
						reportItem.OrderTotalNonCash = nonCashPayment;

						var cmsNopCustomer = _cmsNopCustomerMappingService.GetByCmsCustomerId(item.customerId);
						if (cmsNopCustomer != null)
						{
							var nopCustomer = _customerService.GetCustomerById(cmsNopCustomer.NopCustomerId);
							if (nopCustomer != null)
							{
								reportItem.Username = nopCustomer.Username;
								var attributeList = _genericAttributeService.GetAttributesForEntity(nopCustomer.Id, "Customer");

								var vatRegimeId = GetAttributeValue(attributeList.ToList(), "VatRegimeId");
								reportItem.VatRegime = !string.IsNullOrEmpty(vatRegimeId) ? ((VatRegimeType)Convert.ToInt32(vatRegimeId)).ToString() : "";
							}
						}

						model.VatDataList.Add(reportItem);
					}
				}
			}

			var bytes = ExportReportToXlsx(model.VatDataList.Any() ? model.VatDataList.OrderByDescending(x => x.CreateDate).ToList() : model.VatDataList);
			return File(bytes, MimeTypes.TextXlsx, "vatreport.xlsx");
		}


		public string GetToken()
		{
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			var token = json.GetValue("token").Value<string>();
			return token;
		}


		public string GetAttributeValue(List<GenericAttribute> genericAttributes, string attributeToCheck)
		{
			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeToCheck);
			if (genericAttribute == null)
				return null;

			return genericAttribute.Value;
		}


		public virtual byte[] ExportReportToXlsx(List<VatReportModel> products)
		{
			var properties = new[]
			{
				new PropertyByName<VatReportModel>("ReceiptId", p => p.ReceiptId),
				new PropertyByName<VatReportModel>("CustomerId", p => p.CustomerId),
				new PropertyByName<VatReportModel>("Username", p => p.Username),
				new PropertyByName<VatReportModel>("CreateDate", p => p.CreateDate.ToString("dd/MM/yyyy hh:mm tt")),
				new PropertyByName<VatReportModel>("OrderTotal", p => p.OrderTotal),
				new PropertyByName<VatReportModel>("VAT6OrderTotal", p => p.VAT6OrderTotal),
				new PropertyByName<VatReportModel>("VAT6Total", p => p.VAT6Total),
				new PropertyByName<VatReportModel>("VAT12OrderTotal", p => p.VAT12OrderTotal),
				new PropertyByName<VatReportModel>("VAT12Total", p => p.VAT12Total),
				new PropertyByName<VatReportModel>("VAT21OrderTotal", p => p.VAT21OrderTotal),
				new PropertyByName<VatReportModel>("VAT21Total", p => p.VAT21Total),
				new PropertyByName<VatReportModel>("OrderTotalCash", p => p.OrderTotalCash),
				new PropertyByName<VatReportModel>("OrderTotalNonCash", p => p.OrderTotalNonCash),
				new PropertyByName<VatReportModel>("VatRegime", p => p.VatRegime)
			};

			var productList = products.ToList();
			var productAdvancedMode = _workContext.CurrentCustomer.GetAttribute<bool>("product-advanced-mode");
			return ExportToXlsx(properties, productList);
		}

		protected virtual byte[] ExportToXlsx<T>(PropertyByName<T>[] properties, IEnumerable<T> itemsToExport)
		{
			using (var stream = new MemoryStream())
			{
				// ok, we can run the real code of the sample now
				using (var xlPackage = new ExcelPackage(stream))
				{
					// uncomment this line if you want the XML written out to the outputDir
					//xlPackage.DebugMode = true; 

					// get handles to the worksheets
					var worksheet = xlPackage.Workbook.Worksheets.Add(typeof(T).Name);
					var fWorksheet = xlPackage.Workbook.Worksheets.Add("DataForFilters");
					fWorksheet.Hidden = eWorkSheetHidden.VeryHidden;

					//create Headers and format them 
					var manager = new PropertyManager<T>(properties.Where(p => !p.Ignore));
					manager.WriteCaption(worksheet, SetCaptionStyle);

					var row = 2;
					foreach (var items in itemsToExport)
					{
						manager.CurrentObject = items;
						manager.WriteToXlsx(worksheet, row++, false, fWorksheet: fWorksheet);
					}

					xlPackage.Save();
				}
				return stream.ToArray();
			}
		}

		protected virtual void SetCaptionStyle(ExcelStyle style)
		{
			style.Fill.PatternType = ExcelFillStyle.Solid;
			style.Fill.BackgroundColor.SetColor(Color.FromArgb(184, 204, 228));
			style.Font.Bold = true;
		}


	}
}
