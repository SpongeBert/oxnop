﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Tax;
using Nop.Plugin.PostXCustom.Helpers;
using Nop.Plugin.PostXCustom.Models.Admin.Customers;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Affiliates;
using Nop.Services.Authentication.External;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Forums;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework.Controllers;
using Nop.Services.Events;
using Nop.Plugin.PostXCustom.Domain;
using System.IO;
using Nop.Core.Domain.Logging;
using Nop.Services.CustomService;
using Newtonsoft.Json.Linq;
using Nop.Admin.Extensions;
using Nop.Core.Domain.Directory;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class AdminCustomerController : BasePluginController
	{
		#region Fields

		private readonly ICustomerService _customerService;
		private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ICustomerRegistrationService _customerRegistrationService;
		private readonly IDateTimeHelper _dateTimeHelper;
		private readonly ILocalizationService _localizationService;
		private readonly DateTimeSettings _dateTimeSettings;
		private readonly TaxSettings _taxSettings;
		private readonly RewardPointsSettings _rewardPointsSettings;
		private readonly ICountryService _countryService;
		private readonly IStateProvinceService _stateProvinceService;
		private readonly CustomerSettings _customerSettings;
		private readonly IWorkContext _workContext;
		private readonly IVendorService _vendorService;
		private readonly IStoreContext _storeContext;
		private readonly ICustomerActivityService _customerActivityService;
		private readonly IPermissionService _permissionService;
		private readonly IQueuedEmailService _queuedEmailService;
		private readonly EmailAccountSettings _emailAccountSettings;
		private readonly IEmailAccountService _emailAccountService;
		private readonly ForumSettings _forumSettings;
		private readonly IForumService _forumService;
		private readonly IOpenAuthenticationService _openAuthenticationService;
		private readonly IStoreService _storeService;
		private readonly ICustomerAttributeParser _customerAttributeParser;
		private readonly ICustomerAttributeService _customerAttributeService;
		private readonly IAffiliateService _affiliateService;
		private readonly ICacheManager _cacheManager;
		private readonly ILogger _logger;

		private readonly PostXSettings _postXSettings;
		private readonly ISettingService _settingService;
		private readonly ITaxService _taxService;
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly LocalizationSettings _localizationSettings;
		private readonly ICompanyRepresentativeCustomerMappingService _companyRepresentativeCustomerMappingService;
		private readonly IEventPublisher _eventPublisher;

		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICustomCodeService _customCodeService;
		private readonly IAddressService _addressService;


		private readonly AddressSettings _addressSettings;
		private readonly IAddressAttributeParser _addressAttributeParser;
		private readonly IAddressAttributeService _addressAttributeService;
		private readonly IAddressAttributeFormatter _addressAttributeFormatter;


		#endregion

		#region Constructors

		public AdminCustomerController(ICustomerService customerService, INewsLetterSubscriptionService newsLetterSubscriptionService, IGenericAttributeService genericAttributeService,
			 ICustomerRegistrationService customerRegistrationService, IDateTimeHelper dateTimeHelper, ILocalizationService localizationService, DateTimeSettings dateTimeSettings,
			 TaxSettings taxSettings, RewardPointsSettings rewardPointsSettings, ICountryService countryService, IStateProvinceService stateProvinceService,
			 CustomerSettings customerSettings, IWorkContext workContext, IVendorService vendorService, IStoreContext storeContext,
			 ICustomerActivityService customerActivityService, IPermissionService permissionService, IQueuedEmailService queuedEmailService, EmailAccountSettings emailAccountSettings,
			 IEmailAccountService emailAccountService, ForumSettings forumSettings, IForumService forumService, IOpenAuthenticationService openAuthenticationService,
			 IStoreService storeService, ICustomerAttributeParser customerAttributeParser, ICustomerAttributeService customerAttributeService, IAffiliateService affiliateService,
			 ICacheManager cacheManager, ILogger logger, ISettingService settingService, ITaxService taxService, IWorkflowMessageService workflowMessageService, LocalizationSettings localizationSettings,
			ICompanyRepresentativeCustomerMappingService companyRepresentativeCustomerMappingService, IEventPublisher eventPublisher,
			ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICustomCodeService customCodeService,IAddressService addressService, AddressSettings addressSettings,
			IAddressAttributeParser addressAttributeParser, IAddressAttributeService addressAttributeService, IAddressAttributeFormatter addressAttributeFormatter)
		{
			_customerService = customerService;
			_newsLetterSubscriptionService = newsLetterSubscriptionService;
			_genericAttributeService = genericAttributeService;
			_customerRegistrationService = customerRegistrationService;
			_dateTimeHelper = dateTimeHelper;
			_localizationService = localizationService;
			_dateTimeSettings = dateTimeSettings;
			_taxSettings = taxSettings;
			_rewardPointsSettings = rewardPointsSettings;
			_countryService = countryService;
			_stateProvinceService = stateProvinceService;
			_customerSettings = customerSettings;
			_workContext = workContext;
			_vendorService = vendorService;
			_storeContext = storeContext;
			_customerActivityService = customerActivityService;
			_permissionService = permissionService;
			_queuedEmailService = queuedEmailService;
			_emailAccountSettings = emailAccountSettings;
			_emailAccountService = emailAccountService;
			_forumSettings = forumSettings;
			_forumService = forumService;
			_openAuthenticationService = openAuthenticationService;
			_storeService = storeService;
			_customerAttributeParser = customerAttributeParser;
			_customerAttributeService = customerAttributeService;
			_affiliateService = affiliateService;
			_cacheManager = cacheManager;
			_logger = logger;
			_settingService = settingService;
			var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
			_postXSettings = _settingService.LoadSetting<PostXSettings>(storeScope);
			_taxService = taxService;					
			_workflowMessageService = workflowMessageService;
			_localizationSettings = localizationSettings;
			_companyRepresentativeCustomerMappingService = companyRepresentativeCustomerMappingService;
			_eventPublisher = eventPublisher;
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_customCodeService = customCodeService;
			_addressService = addressService;
			_addressSettings = addressSettings;
			_addressAttributeParser = addressAttributeParser;
			_addressAttributeService = addressAttributeService;
			_addressAttributeFormatter = addressAttributeFormatter;
		}

		#endregion

		#region Utilities

		protected virtual void SaveSelectedTabName(string tabName = "", bool persistForTheNextRequest = true)
		{
			//keep this method synchronized with
			//"GetSelectedTabName" method of \Nop.Web.Framework\HtmlExtensions.cs
			if (string.IsNullOrEmpty(tabName))
			{
				tabName = Request.Form["selected-tab-name"];
			}

			if (!string.IsNullOrEmpty(tabName))
			{
				const string dataKey = "nop.selected-tab-name";
				if (persistForTheNextRequest)
				{
					TempData[dataKey] = tabName;
				}
				else
				{
					ViewData[dataKey] = tabName;
				}
			}
		}

		[NonAction]
		protected virtual string GetCustomerRolesNames(IList<CustomerRole> customerRoles, string separator = ",")
		{
			var sb = new StringBuilder();
			for (int i = 0; i < customerRoles.Count; i++)
			{
				sb.Append(customerRoles[i].Name);
				if (i != customerRoles.Count - 1)
				{
					sb.Append(separator);
					sb.Append(" ");
				}
			}
			return sb.ToString();
		}



		[NonAction]
		protected virtual IList<AdminCustomerModel.AssociatedExternalAuthModel> GetAssociatedExternalAuthRecords(Customer customer)
		{
			if (customer == null)
				throw new ArgumentNullException("customer");

			var result = new List<AdminCustomerModel.AssociatedExternalAuthModel>();
			foreach (var record in _openAuthenticationService.GetExternalIdentifiersFor(customer))
			{
				var method = _openAuthenticationService.LoadExternalAuthenticationMethodBySystemName(record.ProviderSystemName);
				if (method == null)
					continue;

				result.Add(new AdminCustomerModel.AssociatedExternalAuthModel
				{
					Id = record.Id,
					Email = record.Email,
					ExternalIdentifier = record.ExternalIdentifier,
					AuthMethodName = method.PluginDescriptor.FriendlyName
				});
			}

			return result;
		}

		[NonAction]
		protected virtual AdminCustomerModel PrepareCustomerModelForList(Customer customer)
		{
			return new AdminCustomerModel
			{
				Id = customer.Id,
				Email = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest"),
				Username = customer.Username,
				FullName = customer.GetFullName(),
				Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
				Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
				ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
				CustomerRoleNames = GetCustomerRolesNames(customer.CustomerRoles.ToList()),
				Active = customer.Active,
				CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc),
				LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc),
			};
		}

		[NonAction]
		protected virtual string ValidateCustomerRoles(IList<CustomerRole> customerRoles)
		{
			if (customerRoles == null)
				throw new ArgumentNullException("customerRoles");

			//ensure a customer is not added to both 'Guests' and 'Registered' customer roles
			//ensure that a customer is in at least one required role ('Guests' and 'Registered')
			bool isInGuestsRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Guests) != null;
			bool isInRegisteredRole = customerRoles.FirstOrDefault(cr => cr.SystemName == SystemCustomerRoleNames.Registered) != null;
			if (isInGuestsRole && isInRegisteredRole)
				return _localizationService.GetResource("Admin.Customers.Customers.GuestsAndRegisteredRolesError");
			if (!isInGuestsRole && !isInRegisteredRole)
				return _localizationService.GetResource("Admin.Customers.Customers.AddCustomerToGuestsOrRegisteredRoleError");

			//no errors
			return "";
		}

		[NonAction]
		protected virtual void PrepareVendorsModel(AdminCustomerModel model)
		{
			if (model == null)
				throw new ArgumentNullException("model");

			model.AvailableVendors.Add(new SelectListItem
			{
				Text = _localizationService.GetResource("Admin.Customers.Customers.Fields.Vendor.None"),
				Value = "0"
			});
			var vendors = SelectListHelper.GetVendorList(_vendorService, _cacheManager, true);
			foreach (var v in vendors)
				model.AvailableVendors.Add(v);
		}

		[NonAction]
		protected virtual void PrepareCustomerAttributeModel(AdminCustomerModel model, Customer customer)
		{
			var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
			foreach (var attribute in customerAttributes)
			{
				var attributeModel = new AdminCustomerModel.CustomerAttributeModel
				{
					Id = attribute.Id,
					Name = attribute.Name,
					IsRequired = attribute.IsRequired,
					AttributeControlType = attribute.AttributeControlType,
				};

				if (attribute.ShouldHaveValues())
				{
					//values
					var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
					foreach (var attributeValue in attributeValues)
					{
						var attributeValueModel = new AdminCustomerModel.CustomerAttributeValueModel
						{
							Id = attributeValue.Id,
							Name = attributeValue.Name,
							IsPreSelected = attributeValue.IsPreSelected
						};
						attributeModel.Values.Add(attributeValueModel);
					}
				}


				//set already selected attributes
				if (customer != null)
				{
					var selectedCustomerAttributes = customer.GetAttribute<string>(SystemCustomerAttributeNames.CustomCustomerAttributes, _genericAttributeService);
					switch (attribute.AttributeControlType)
					{
						case AttributeControlType.DropdownList:
						case AttributeControlType.RadioList:
						case AttributeControlType.Checkboxes:
							{
								if (!String.IsNullOrEmpty(selectedCustomerAttributes))
								{
									//clear default selection
									foreach (var item in attributeModel.Values)
										item.IsPreSelected = false;

									//select new values
									var selectedValues = _customerAttributeParser.ParseCustomerAttributeValues(selectedCustomerAttributes);
									foreach (var attributeValue in selectedValues)
										foreach (var item in attributeModel.Values)
											if (attributeValue.Id == item.Id)
												item.IsPreSelected = true;
								}
							}
							break;
						case AttributeControlType.ReadonlyCheckboxes:
							{
								//do nothing
								//values are already pre-set
							}
							break;
						case AttributeControlType.TextBox:
						case AttributeControlType.MultilineTextbox:
							{
								if (!String.IsNullOrEmpty(selectedCustomerAttributes))
								{
									var enteredText = _customerAttributeParser.ParseValues(selectedCustomerAttributes, attribute.Id);
									if (enteredText.Any())
										attributeModel.DefaultValue = enteredText[0];
								}
							}
							break;
						case AttributeControlType.Datepicker:
						case AttributeControlType.ColorSquares:
						case AttributeControlType.ImageSquares:
						case AttributeControlType.FileUpload:
						default:
							//not supported attribute control types
							break;
					}
				}

				model.CustomerAttributes.Add(attributeModel);
			}
		}

		[NonAction]
		protected virtual string ParseCustomCustomerAttributes(FormCollection form)
		{
			if (form == null)
				throw new ArgumentNullException("form");

			string attributesXml = "";
			var customerAttributes = _customerAttributeService.GetAllCustomerAttributes();
			foreach (var attribute in customerAttributes)
			{
				string controlId = string.Format("customer_attribute_{0}", attribute.Id);
				switch (attribute.AttributeControlType)
				{
					case AttributeControlType.DropdownList:
					case AttributeControlType.RadioList:
						{
							var ctrlAttributes = form[controlId];
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								int selectedAttributeId = int.Parse(ctrlAttributes);
								if (selectedAttributeId > 0)
									attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
										 attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
							}
						}
						break;
					case AttributeControlType.Checkboxes:
						{
							var cblAttributes = form[controlId];
							if (!String.IsNullOrEmpty(cblAttributes))
							{
								foreach (var item in cblAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
								{
									int selectedAttributeId = int.Parse(item);
									if (selectedAttributeId > 0)
										attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
											 attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
								}
							}
						}
						break;
					case AttributeControlType.ReadonlyCheckboxes:
						{
							//load read-only (already server-side selected) values
							var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
							foreach (var selectedAttributeId in attributeValues
								 .Where(v => v.IsPreSelected)
								 .Select(v => v.Id)
								 .ToList())
							{
								attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
												attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
							}
						}
						break;
					case AttributeControlType.TextBox:
					case AttributeControlType.MultilineTextbox:
						{
							var ctrlAttributes = form[controlId];
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								string enteredText = ctrlAttributes.Trim();
								attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
									 attribute, enteredText);
							}
						}
						break;
					case AttributeControlType.Datepicker:
					case AttributeControlType.ColorSquares:
					case AttributeControlType.ImageSquares:
					case AttributeControlType.FileUpload:
					//not supported customer attributes
					default:
						break;
				}
			}

			return attributesXml;
		}

		[NonAction]
		protected virtual void PrepareCustomerModel(AdminCustomerModel model, Customer customer, bool excludeProperties)
		{
			var allStores = _storeService.GetAllStores();
			if (customer != null)
			{
				model.Id = customer.Id;
				if (!excludeProperties)
				{
					model.Email = customer.Email;
					model.Username = customer.Username;
					model.VendorId = customer.VendorId;
					model.AdminComment = customer.AdminComment;
					model.IsTaxExempt = customer.IsTaxExempt;
					model.Active = customer.Active;

					if (customer.RegisteredInStoreId == 0 || allStores.All(s => s.Id != customer.RegisteredInStoreId))
						model.RegisteredInStore = string.Empty;
					else
						model.RegisteredInStore = allStores.First(s => s.Id == customer.RegisteredInStoreId).Name;

					var affiliate = _affiliateService.GetAffiliateById(customer.AffiliateId);
					if (affiliate != null)
					{
						model.AffiliateId = affiliate.Id;
						model.AffiliateName = affiliate.GetFullName();
					}

					model.TimeZoneId = customer.GetAttribute<string>(SystemCustomerAttributeNames.TimeZoneId);
					model.VatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);
					model.VatNumberStatusNote = ((VatNumberStatus)customer.GetAttribute<int>(SystemCustomerAttributeNames.VatNumberStatusId))
						 .GetLocalizedEnum(_localizationService, _workContext);
					model.CreatedOn = _dateTimeHelper.ConvertToUserTime(customer.CreatedOnUtc, DateTimeKind.Utc);
					model.LastActivityDate = _dateTimeHelper.ConvertToUserTime(customer.LastActivityDateUtc, DateTimeKind.Utc);
					model.LastIpAddress = customer.LastIpAddress;
					model.LastVisitedPage = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastVisitedPage);

					model.SelectedCustomerRoleIds = customer.CustomerRoles.Select(cr => cr.Id).ToList();

					//newsletter subscriptions
					if (!String.IsNullOrEmpty(customer.Email))
					{
						var newsletterSubscriptionStoreIds = new List<int>();
						foreach (var store in allStores)
						{
							var newsletterSubscription = _newsLetterSubscriptionService
								 .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
							if (newsletterSubscription != null)
								newsletterSubscriptionStoreIds.Add(store.Id);
							model.SelectedNewsletterSubscriptionStoreIds = newsletterSubscriptionStoreIds.ToArray();
						}
					}

					//form fields
					model.FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
					model.LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);
					model.Gender = customer.GetAttribute<string>(SystemCustomerAttributeNames.Gender);
					model.DateOfBirth = customer.GetAttribute<DateTime?>(SystemCustomerAttributeNames.DateOfBirth);
					model.Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company);
					model.StreetAddress = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress);
					model.StreetAddress2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2);
					model.ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode);
					model.City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City);
					model.CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId);
					model.StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId);
					model.Phone = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone);
					model.Fax = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax);
				}
			}

			model.UsernamesEnabled = _customerSettings.UsernamesEnabled;
			model.AllowCustomersToSetTimeZone = _dateTimeSettings.AllowCustomersToSetTimeZone;
			foreach (var tzi in _dateTimeHelper.GetSystemTimeZones())
				model.AvailableTimeZones.Add(new SelectListItem { Text = tzi.DisplayName, Value = tzi.Id, Selected = (tzi.Id == model.TimeZoneId) });
			model.DisplayVatNumber = customer != null && _taxSettings.EuVatEnabled;

			//vendors
			PrepareVendorsModel(model);
			//customer attributes
			PrepareCustomerAttributeModel(model, customer);

			model.GenderEnabled = _customerSettings.GenderEnabled;
			model.DateOfBirthEnabled = _customerSettings.DateOfBirthEnabled;
			model.CompanyEnabled = _customerSettings.CompanyEnabled;
			model.StreetAddressEnabled = _customerSettings.StreetAddressEnabled;
			model.StreetAddress2Enabled = _customerSettings.StreetAddress2Enabled;
			model.ZipPostalCodeEnabled = _customerSettings.ZipPostalCodeEnabled;
			model.CityEnabled = _customerSettings.CityEnabled;
			model.CountryEnabled = _customerSettings.CountryEnabled;
			model.StateProvinceEnabled = _customerSettings.StateProvinceEnabled;
			model.PhoneEnabled = _customerSettings.PhoneEnabled;
			model.FaxEnabled = _customerSettings.FaxEnabled;

			//countries and states
			if (_customerSettings.CountryEnabled)
			{
				model.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
				foreach (var c in _countryService.GetAllCountries(showHidden: true))
				{
					model.AvailableCountries.Add(new SelectListItem
					{
						Text = c.Name,
						Value = c.Id.ToString(CultureInfo.InvariantCulture),
						Selected = c.Id == model.CountryId
					});
				}

				if (_customerSettings.StateProvinceEnabled)
				{
					//states
					var states = _stateProvinceService.GetStateProvincesByCountryId(model.CountryId).ToList();
					if (states.Any())
					{
						model.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectState"), Value = "0" });

						foreach (var s in states)
						{
							model.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(CultureInfo.InvariantCulture), Selected = (s.Id == model.StateProvinceId) });
						}
					}
					else
					{
						bool anyCountrySelected = model.AvailableCountries.Any(x => x.Selected);

						model.AvailableStates.Add(new SelectListItem
						{
							Text = _localizationService.GetResource(anyCountrySelected ? "Admin.Address.OtherNonUS" : "Admin.Address.SelectState"),
							Value = "0"
						});
					}
				}
			}

			//newsletter subscriptions
			model.AvailableNewsletterSubscriptionStores = allStores
				 .Select(s => new AdminCustomerModel.StoreModel { Id = s.Id, Name = s.Name })
				 .ToList();

			//customer roles
			var allRoles = _customerService.GetAllCustomerRoles(true);
			var adminRole = allRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered);
			//precheck Registered Role as a default role while creating a new customer through admin
			if (customer == null && adminRole != null)
			{
				model.SelectedCustomerRoleIds.Add(adminRole.Id);
			}
			var companyRole = allRoles.FirstOrDefault(x => x.SystemName == "CompanyRepresentative");
			if (companyRole != null)
			{
				model.SelectedCustomerRoleIds.Add(companyRole.Id);
			}

			foreach (var role in allRoles)
			{
				model.AvailableCustomerRoles.Add(new SelectListItem
				{
					Text = role.Name,
					Value = role.Id.ToString(CultureInfo.InvariantCulture),
					Selected = model.SelectedCustomerRoleIds.Contains(role.Id)
				});


			}

			//reward points history
			if (customer != null)
			{
				model.DisplayRewardPointsHistory = _rewardPointsSettings.Enabled;
				model.AddRewardPointsValue = 0;
				model.AddRewardPointsMessage = "Some comment here...";

				//stores
				foreach (var store in allStores)
				{
					model.RewardPointsAvailableStores.Add(new SelectListItem
					{
						Text = store.Name,
						Value = store.Id.ToString(CultureInfo.InvariantCulture),
						Selected = (store.Id == _storeContext.CurrentStore.Id)
					});
				}
			}
			else
			{
				model.DisplayRewardPointsHistory = false;
			}
			//external authentication records
			if (customer != null)
			{
				model.AssociatedExternalAuthRecords = GetAssociatedExternalAuthRecords(customer);
			}
			//sending of the welcome message:
			//1. "admin approval" registration method
			//2. already created customer
			//3. registered
			model.AllowSendingOfWelcomeMessage = _customerSettings.UserRegistrationType == UserRegistrationType.AdminApproval &&
				 customer != null &&
				 customer.IsRegistered();
			//sending of the activation message
			//1. "email validation" registration method
			//2. already created customer
			//3. registered
			//4. not active
			model.AllowReSendingOfActivationMessage = _customerSettings.UserRegistrationType == UserRegistrationType.EmailValidation &&
				 customer != null &&
				 customer.IsRegistered() &&
				 !customer.Active;
		}

		[NonAction]
		private bool SecondAdminAccountExists(Customer customer)
		{
			var customers = _customerService.GetAllCustomers(customerRoleIds: new[] { _customerService.GetCustomerRoleBySystemName(SystemCustomerRoleNames.Administrators).Id });

			return customers.Any(c => c.Active && c.Id != customer.Id);
		}

		private void AssignCompanyId(out int companyId)
		{
			var topCustomerIdRecord = _customerService.GetAllCustomers().OrderByDescending(x => x.CompanyId).First();
			if (topCustomerIdRecord == null)
			{
				//customer.CompanyId = 1;
				//_customerService.UpdateCustomer(customer);
				companyId = 1;
				return;
			}
			companyId = topCustomerIdRecord.CompanyId + 1;
		}

		[NonAction]
		protected virtual bool CheckCustomerLightSpeedMapping(int customerId)
		{
			var mappingRecord = _cmsNopCustomerMappingService.GetByNopCustomerId(customerId);
			if (mappingRecord != null && mappingRecord.Deleted == false)
				return false;

			return true;
		}

		#endregion

		#region Customers		 
		public virtual ActionResult CompanyList()
		{
			return RedirectToRoute("AdminCustomerList");
		}

		public virtual ActionResult AccessDenied(string pageUrl)
		{
			var currentCustomer = _workContext.CurrentCustomer;
			if (currentCustomer == null || currentCustomer.IsGuest())
			{
				_logger.Information(string.Format("Access denied to anonymous request on {0}", pageUrl));
				return View("~/Plugins/PostXCustom/Views/Security/AccessDenied.cshtml");
			}
			_logger.Information(string.Format("Access denied to user #{0} '{1}' on {2}", currentCustomer.Email, currentCustomer.Email, pageUrl));
			return View("~/Plugins/PostXCustom/Views/Security/AccessDenied.cshtml");
		}

		public virtual ActionResult Create()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var model = new AdminCustomerModel { Username = "Username will be generated by the system" };
			PrepareCustomerModel(model, null, false);
			//default value
			model.Active = true;
			return View("~/Plugins/PostXCustom/Views/AdminCustomer/Create.cshtml", model);
		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		[FormValueRequired("save", "save-continue")]
		[ValidateInput(false)]
		public virtual ActionResult Create(AdminCustomerModel model, bool continueEditing, FormCollection form)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			if (!String.IsNullOrWhiteSpace(model.Email))
			{
				var cust2 = _customerService.GetCustomerByEmail(model.Email);
				if (cust2 != null)
					ModelState.AddModelError("", "Email is already registered");
			}

			int companyId = 0;
			AssignCompanyId(out companyId);
			model.Username = companyId.ToString("D3") + 1.ToString("D5");

			if (!String.IsNullOrWhiteSpace(model.Username) & _customerSettings.UsernamesEnabled)
			{
				var cust2 = _customerService.GetCustomerByUsername(model.Username);
				if (cust2 != null)
					ModelState.AddModelError("", "Username is already registered");
			}

			//validate customer roles
			var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
			var newCustomerRoles = new List<CustomerRole>();
			foreach (var customerRole in allCustomerRoles)
				if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
					newCustomerRoles.Add(customerRole);
			var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
			if (!String.IsNullOrEmpty(customerRolesError))
			{
				ModelState.AddModelError("", customerRolesError);
				ErrorNotification(customerRolesError, false);
			}

			// Ensure that valid email address is entered if Registered role is checked to avoid registered customers with empty email address
			if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null && !CommonHelper.IsValidEmail(model.Email))
			{
				ModelState.AddModelError("", _localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"));
				ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"), false);
			}

			//custom customer attributes
			var customerAttributesXml = ParseCustomCustomerAttributes(form);
			if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null)
			{
				var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
				foreach (var error in customerAttributeWarnings)
				{
					ModelState.AddModelError("", error);
				}
			}

			if (ModelState.IsValid)
			{
				var isActive = (model.StatusCodeTypeId == 1) ? true : false;
				var customer = new Customer
				{
					CustomerGuid = Guid.NewGuid(),
					Email = model.Email,
					Username = model.Username,
					VendorId = model.VendorId,
					AdminComment = model.AdminComment,
					//IsTaxExempt = model.IsTaxExempt,
					IsTaxExempt = false,
					//Active = model.Active,
					Active = isActive,
					CreatedOnUtc = DateTime.UtcNow,
					LastActivityDateUtc = DateTime.UtcNow,
					RegisteredInStoreId = _storeContext.CurrentStore.Id,
					CompanyId = companyId,
					PersonalId = 1,
				};
				_customerService.InsertCustomer(customer);

				#region Assign CompanyId


				#endregion

				//form fields
				if (_dateTimeSettings.AllowCustomersToSetTimeZone)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
				if (_customerSettings.GenderEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
				_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
				_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
				if (_customerSettings.DateOfBirthEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);
				if (_customerSettings.CompanyEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Company);
				if (_customerSettings.StreetAddressEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
				if (_customerSettings.StreetAddress2Enabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
				if (_customerSettings.ZipPostalCodeEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
				if (_customerSettings.CityEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
				if (_customerSettings.CountryEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
				if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
				if (_customerSettings.PhoneEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
				if (_customerSettings.FaxEnabled)
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);

				//custom customer attributes
				_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

				//Save Company Logo
				if (model.PictureId > 0)
				{

					var pictureAttribute = new GenericAttribute
					{
						EntityId = customer.Id,
						Key = "PictureId",
						KeyGroup = "Customer",
						Value = model.PictureId.ToString(CultureInfo.InvariantCulture),
						StoreId = 0
					};
					_genericAttributeService.InsertAttribute(pictureAttribute);
				}

				var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");


				//InsertUpdateAttribute(customer, attributeList.ToList(), "PersonalNumber",
				//			model.PersonalNumber.ToString(CultureInfo.InvariantCulture));

				InsertUpdateAttribute(customer, attributeList.ToList(), "AccessWord", model.Password);

				InsertUpdateAttribute(customer, attributeList.ToList(), "BankAccountNumber",
					model.BankAccountNumber);

				InsertUpdateAttribute(customer, attributeList.ToList(), "ContactPerson1",
					model.ContactPerson1);

				InsertUpdateAttribute(customer, attributeList.ToList(), "ContactPerson2",
					model.ContactPerson2);

				InsertUpdateAttribute(customer, attributeList.ToList(), "ContactPerson1Mobile",
					model.ContactPerson1Mobile);

				InsertUpdateAttribute(customer, attributeList.ToList(), "ContactPerson2Mobile",
					model.ContactPerson2Mobile);

				InsertUpdateAttribute(customer, attributeList.ToList(), "ContractTypeId",
					model.ContractTypeId.ToString(CultureInfo.InvariantCulture));

				//InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", model.LoyalityPercentage.ToString(CultureInfo.InvariantCulture));

				var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
				var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeScope);

				switch (model.ContractTypeId)
				{
					case 1:
						InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractA.ToString(CultureInfo.InvariantCulture));
						break;
					case 2:
						InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractB.ToString(CultureInfo.InvariantCulture));
						break;
					case 3:
						InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractC.ToString(CultureInfo.InvariantCulture));
						break;
					case 4:
						InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractD.ToString(CultureInfo.InvariantCulture));
						break;
					case 26:
						InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractZ.ToString(CultureInfo.InvariantCulture));
						break;
				}

				InsertUpdateAttribute(customer, attributeList.ToList(), "VatRegimeId", model.VatRegimeId.ToString(CultureInfo.InvariantCulture));

				//Save Customer Image
				if (model.CustomerPictureId > 0)
				{
					InsertUpdateAttribute(customer, attributeList.ToList(), "CustomerPictureId", model.CustomerPictureId.ToString(CultureInfo.InvariantCulture));
				}

				InsertUpdateAttribute(customer, attributeList.ToList(), "ActivationDate", model.ActivationDate.ToString());
				InsertUpdateAttribute(customer, attributeList.ToList(), "EndDate", model.EndDate.ToString());
				InsertUpdateAttribute(customer, attributeList.ToList(), "AuthorizationTypeId", model.AuthorizationTypeId.ToString(CultureInfo.InvariantCulture));
				InsertUpdateAttribute(customer, attributeList.ToList(), "StatusCodeId", model.StatusCodeTypeId.ToString(CultureInfo.InvariantCulture));


				//newsletter subscriptions
				if (!String.IsNullOrEmpty(customer.Email))
				{
					var allStores = _storeService.GetAllStores();
					foreach (var store in allStores)
					{
						var newsletterSubscription = _newsLetterSubscriptionService
							 .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
						if (model.SelectedNewsletterSubscriptionStoreIds != null &&
							 model.SelectedNewsletterSubscriptionStoreIds.Contains(store.Id))
						{
							//subscribed
							if (newsletterSubscription == null)
							{
								_newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
								{
									NewsLetterSubscriptionGuid = Guid.NewGuid(),
									Email = customer.Email,
									Active = true,
									StoreId = store.Id,
									CreatedOnUtc = DateTime.UtcNow
								});
							}
						}
						else
						{
							//not subscribed
							if (newsletterSubscription != null)
							{
								_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletterSubscription);
							}
						}
					}
				}

				//password
				if (!String.IsNullOrWhiteSpace(model.Password))
				{
					var changePassRequest = new ChangePasswordRequest(model.Username, false, _customerSettings.DefaultPasswordFormat, model.Password);
					var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
					if (!changePassResult.Success)
					{
						foreach (var changePassError in changePassResult.Errors)
							ErrorNotification(changePassError);
					}
				}

				//customer roles
				foreach (var customerRole in newCustomerRoles)
				{
					//ensure that the current customer cannot add to "Administrators" system role if he's not an admin himself
					if (customerRole.SystemName == SystemCustomerRoleNames.Administrators &&
						 !_workContext.CurrentCustomer.IsAdmin())
						continue;

					customer.CustomerRoles.Add(customerRole);
				}
				_customerService.UpdateCustomer(customer);


				//ensure that a customer with a vendor associated is not in "Administrators" role
				//otherwise, he won't have access to other functionality in admin area
				if (customer.IsAdmin() && customer.VendorId > 0)
				{
					customer.VendorId = 0;
					_customerService.UpdateCustomer(customer);
					ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminCouldNotbeVendor"));
				}

				//ensure that a customer in the Vendors role has a vendor account associated.
				//otherwise, he will have access to ALL products
				if (customer.IsVendor() && customer.VendorId == 0)
				{
					var vendorRole = customer
						 .CustomerRoles
						 .FirstOrDefault(x => x.SystemName == SystemCustomerRoleNames.Vendors);
					customer.CustomerRoles.Remove(vendorRole);
					_customerService.UpdateCustomer(customer);
					ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.CannotBeInVendoRoleWithoutVendorAssociated"));
				}

				//activity log
				_customerActivityService.InsertActivity("AddNewCustomer", _localizationService.GetResource("ActivityLog.AddNewCustomer"), customer.Id);

				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Added"));

				//Send email notification
				_workflowMessageService.SendCompanyRegisteredNotificationMessage(customer,
					 _localizationSettings.DefaultAdminLanguageId);
				_workflowMessageService.SendCompanyWelcomeMessage(customer,
					 _localizationSettings.DefaultAdminLanguageId);


				//raise event       
				_eventPublisher.Publish(new CustomerRegisteredEvent(customer));

				#region Push To WineShop

				using (var client = new WebClient())
				{
					var values = new NameValueCollection();
					values["Username"] = customer.Username;
					values["Email"] = customer.Email;
					values["FirstName"] = model.FirstName;
					values["LastName"] = model.LastName;
					values["Company"] = model.Company;
					values["DateOfBirth"] = model.DateOfBirth.ToString();
					values["Gender"] = model.Gender;
					values["Password"] = model.Password;
					values["Active"] = model.Active.ToString();

					var response = client.UploadValues(_postXSettings.CustomerPushUrl, values);

					var responseString = Encoding.Default.GetString(response);
				}

				#endregion

				if (continueEditing)
				{
					//selected tab
					SaveSelectedTabName();

					//return RedirectToAction("Edit", "Customer", new { namespaces = "Nop.Admin.Controllers", id = customer.Id });
					return RedirectToRoute("AdminCustomerEdit", new { id = customer.Id });
				}
				return RedirectToRoute("AdminCustomerList");
			}

			//If we got this far, something failed, redisplay form
			PrepareCustomerModel(model, null, true);
			return View("~/Plugins/PostXCustom/Views/AdminCustomer/Create.cshtml", model);
		}

		public virtual ActionResult SendEmail(AdminCustomerModel model)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToRoute("AdminCustomerList");

			try
			{
				if (String.IsNullOrWhiteSpace(customer.Email))
					throw new NopException("Customer email is empty");
				if (!CommonHelper.IsValidEmail(customer.Email))
					throw new NopException("Customer email is not valid");
				if (String.IsNullOrWhiteSpace(model.SendEmail.Subject))
					throw new NopException("Email subject is empty");
				if (String.IsNullOrWhiteSpace(model.SendEmail.Body))
					throw new NopException("Email body is empty");

				var emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId) ??
										 _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
				if (emailAccount == null)
					throw new NopException("Email account can't be loaded");
				var email = new QueuedEmail
				{
					Priority = QueuedEmailPriority.High,
					EmailAccountId = emailAccount.Id,
					FromName = emailAccount.DisplayName,
					From = emailAccount.Email,
					ToName = customer.GetFullName(),
					To = customer.Email,
					Subject = model.SendEmail.Subject,
					Body = model.SendEmail.Body,
					CreatedOnUtc = DateTime.UtcNow,
					DontSendBeforeDateUtc = (model.SendEmail.SendImmediately || !model.SendEmail.DontSendBeforeDate.HasValue) ?
						 null : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.SendEmail.DontSendBeforeDate.Value)
				};
				_queuedEmailService.InsertQueuedEmail(email);
				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendEmail.Queued"));
			}
			catch (Exception exc)
			{
				ErrorNotification(exc.Message);
			}

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		public virtual ActionResult SendPm(AdminCustomerModel model)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToRoute("AdminCustomerList");

			try
			{
				if (!_forumSettings.AllowPrivateMessages)
					throw new NopException("Private messages are disabled");
				if (customer.IsGuest())
					throw new NopException("Customer should be registered");
				if (String.IsNullOrWhiteSpace(model.SendPm.Subject))
					throw new NopException("PM subject is empty");
				if (String.IsNullOrWhiteSpace(model.SendPm.Message))
					throw new NopException("PM message is empty");


				var privateMessage = new PrivateMessage
				{
					StoreId = _storeContext.CurrentStore.Id,
					ToCustomerId = customer.Id,
					FromCustomerId = _workContext.CurrentCustomer.Id,
					Subject = model.SendPm.Subject,
					Text = model.SendPm.Message,
					IsDeletedByAuthor = false,
					IsDeletedByRecipient = false,
					IsRead = false,
					CreatedOnUtc = DateTime.UtcNow
				};

				_forumService.InsertPrivateMessage(privateMessage);
				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.SendPM.Sent"));
			}
			catch (Exception exc)
			{
				ErrorNotification(exc.Message);
			}

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		public virtual ActionResult Edit(int id)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var customer = _customerService.GetCustomerById(id);
			if (customer == null || customer.Deleted)
				//No customer found with the specified id
				return RedirectToRoute("AdminCustomerList");

			var model = new AdminCustomerModel();
			PrepareCustomerModel(model, customer, false);
			if (!string.IsNullOrEmpty(customer.CompanyId.ToString(CultureInfo.InvariantCulture)))
				model.CompanyId = customer.CompanyId;

			var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
			if (attributeList.Any())
			{
				var pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
				if (pictureAttribute != null)
					model.PictureId = Convert.ToInt32(pictureAttribute.Value);

				var currentPassword = GetAttributeValue(attributeList.ToList(), "AccessWord");
				model.CurrentPassword = !string.IsNullOrEmpty(currentPassword) ? currentPassword : "";

				model.PersonalNumber = customer.CompanyId.ToString("D3") + customer.PersonalId.ToString("D5");

				var bankAccountNumber = GetAttributeValue(attributeList.ToList(), "BankAccountNumber");
				model.BankAccountNumber = !string.IsNullOrEmpty(bankAccountNumber) ? bankAccountNumber : "";

				var contactPerson1 = GetAttributeValue(attributeList.ToList(), "ContactPerson1");
				model.ContactPerson1 = !string.IsNullOrEmpty(contactPerson1) ? contactPerson1 : "";

				var contactPerson2 = GetAttributeValue(attributeList.ToList(), "ContactPerson2");
				model.ContactPerson2 = !string.IsNullOrEmpty(contactPerson2) ? contactPerson2 : "";

				var contactPerson1Mobile = GetAttributeValue(attributeList.ToList(), "ContactPerson1Mobile");
				model.ContactPerson1Mobile = !string.IsNullOrEmpty(contactPerson1Mobile) ? contactPerson1Mobile : "";

				var contactPerson2Mobile = GetAttributeValue(attributeList.ToList(), "ContactPerson2Mobile");
				model.ContactPerson2Mobile = !string.IsNullOrEmpty(contactPerson2Mobile) ? contactPerson2Mobile : "";

				var contractTypeId = GetAttributeValue(attributeList.ToList(), "ContractTypeId");
				model.ContractTypeId = !string.IsNullOrEmpty(contractTypeId) ? Convert.ToInt32(contractTypeId) : 0;

				var loyalityPercentage = GetAttributeValue(attributeList.ToList(), "LoyalityPercentage");
				model.LoyalityPercentage = !string.IsNullOrEmpty(loyalityPercentage) ? Convert.ToInt32(loyalityPercentage) : 0;

				var vatRegimeId = GetAttributeValue(attributeList.ToList(), "VatRegimeId");
				model.VatRegimeId = !string.IsNullOrEmpty(vatRegimeId) ? Convert.ToInt32(vatRegimeId) : 0;

				var authorizationTypeId = GetAttributeValue(attributeList.ToList(), "AuthorizationTypeId");
				model.AuthorizationTypeId = !string.IsNullOrEmpty(authorizationTypeId) ? Convert.ToInt32(authorizationTypeId) : 0;

				var statusCodeId = GetAttributeValue(attributeList.ToList(), "StatusCodeId");
				model.StatusCodeTypeId = !string.IsNullOrEmpty(statusCodeId) ? Convert.ToInt32(statusCodeId) : 0;

				var customerPictureId = GetAttributeValue(attributeList.ToList(), "CustomerPictureId");
				if (!string.IsNullOrEmpty(customerPictureId))
					model.CustomerPictureId = Convert.ToInt32(customerPictureId);

				var activationDate = GetAttributeValue(attributeList.ToList(), "ActivationDate");
				if (!string.IsNullOrEmpty(activationDate))
					model.ActivationDate = Convert.ToDateTime(activationDate);

				var endDate = GetAttributeValue(attributeList.ToList(), "EndDate");
				if (!string.IsNullOrEmpty(endDate))
					model.EndDate = Convert.ToDateTime(endDate);
			}

			model.IsSyncedToLightSpeed = CheckCustomerLightSpeedMapping(customer.Id);


			return View("~/Plugins/PostXCustom/Views/AdminCustomer/Edit.cshtml", model);
		}

		public string GetAttributeValue(List<GenericAttribute> genericAttributes, string attributeToCheck)
		{
			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeToCheck);
			if (genericAttribute == null)
				return null;

			return genericAttribute.Value;
		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		[FormValueRequired("save", "save-continue")]
		[ValidateInput(false)]
		public virtual ActionResult Edit(AdminCustomerModel model, bool continueEditing, FormCollection form)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null || customer.Deleted)
				//No customer found with the specified id
				return RedirectToRoute("AdminCustomerList");

			//validate customer roles
			var allCustomerRoles = _customerService.GetAllCustomerRoles(true);
			var newCustomerRoles = new List<CustomerRole>();
			foreach (var customerRole in allCustomerRoles)
				if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
					newCustomerRoles.Add(customerRole);
			var customerRolesError = ValidateCustomerRoles(newCustomerRoles);
			if (!String.IsNullOrEmpty(customerRolesError))
			{
				ModelState.AddModelError("", customerRolesError);
				ErrorNotification(customerRolesError, false);
			}

			// Ensure that valid email address is entered if Registered role is checked to avoid registered customers with empty email address
			if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null && !CommonHelper.IsValidEmail(model.Email))
			{
				ModelState.AddModelError("", _localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"));
				ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.ValidEmailRequiredRegisteredRole"), false);
			}

			//custom customer attributes
			var customerAttributesXml = ParseCustomCustomerAttributes(form);
			if (newCustomerRoles.Any() && newCustomerRoles.FirstOrDefault(c => c.SystemName == SystemCustomerRoleNames.Registered) != null)
			{
				var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
				foreach (var error in customerAttributeWarnings)
				{
					ModelState.AddModelError("", error);
				}
			}

			if (ModelState.IsValid)
			{
				try
				{
					var isActive = (model.StatusCodeTypeId == 1) ? true : false;
					model.Active = isActive;

					customer.AdminComment = model.AdminComment;
					customer.IsTaxExempt = model.IsTaxExempt;

					//prevent deactivation of the last active administrator
					if (!customer.IsAdmin() || model.Active || SecondAdminAccountExists(customer))
						customer.Active = model.Active;
					else
						ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.Deactivate"));

					//email
					if (!String.IsNullOrWhiteSpace(model.Email))
					{
						_customerRegistrationService.SetEmail(customer, model.Email, false);
					}
					else
					{
						customer.Email = model.Email;
					}

					//username
					if (_customerSettings.UsernamesEnabled)
					{
						//if (!String.IsNullOrWhiteSpace(model.Username))
						//{
						//	_customerRegistrationService.SetUsername(customer, model.Username);
						//}
						//else
						//{
						//	customer.Username = model.Username;
						//}
					}

					//VAT number
					if (_taxSettings.EuVatEnabled)
					{
						var prevVatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);

						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.VatNumber, model.VatNumber);
						//set VAT number status
						if (!String.IsNullOrEmpty(model.VatNumber))
						{
							if (!model.VatNumber.Equals(prevVatNumber, StringComparison.InvariantCultureIgnoreCase))
							{
								_genericAttributeService.SaveAttribute(customer,
									 SystemCustomerAttributeNames.VatNumberStatusId,
									 (int)_taxService.GetVatNumberStatus(model.VatNumber));
							}
						}
						else
						{
							_genericAttributeService.SaveAttribute(customer,
								 SystemCustomerAttributeNames.VatNumberStatusId,
								 (int)VatNumberStatus.Empty);
						}
					}

					//vendor
					customer.VendorId = model.VendorId;

					//form fields
					if (_dateTimeSettings.AllowCustomersToSetTimeZone)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
					if (_customerSettings.GenderEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
					if (_customerSettings.DateOfBirthEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, model.DateOfBirth);
					if (_customerSettings.CompanyEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, model.Company);
					if (_customerSettings.StreetAddressEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
					if (_customerSettings.StreetAddress2Enabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
					if (_customerSettings.ZipPostalCodeEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
					if (_customerSettings.CityEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
					if (_customerSettings.CountryEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
					if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
					if (_customerSettings.PhoneEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
					if (_customerSettings.FaxEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);

					//custom customer attributes
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

					//Update or insert Company Logo
					var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
					if (attributeList.Any())
					{
						var pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
						if (pictureAttribute == null)
						{
							pictureAttribute = new GenericAttribute
							{
								EntityId = customer.Id,
								Key = "PictureId",
								KeyGroup = "Customer",
								Value = model.PictureId.ToString(CultureInfo.InvariantCulture),
								StoreId = 0
							};
							_genericAttributeService.InsertAttribute(pictureAttribute);

						}
						else
						{
							pictureAttribute.Value = model.PictureId.ToString(CultureInfo.InvariantCulture);
							_genericAttributeService.UpdateAttribute(pictureAttribute);
						}

						InsertUpdateAttribute(customer, attributeList.ToList(), "BankAccountNumber", model.BankAccountNumber);
						InsertUpdateAttribute(customer, attributeList.ToList(), "ContactPerson1", model.ContactPerson1);
						InsertUpdateAttribute(customer, attributeList.ToList(), "ContactPerson2", model.ContactPerson2);
						InsertUpdateAttribute(customer, attributeList.ToList(), "ContactPerson1Mobile", model.ContactPerson1Mobile);
						InsertUpdateAttribute(customer, attributeList.ToList(), "ContactPerson2Mobile", model.ContactPerson2Mobile);
						InsertUpdateAttribute(customer, attributeList.ToList(), "ContractTypeId", model.ContractTypeId.ToString(CultureInfo.InvariantCulture));
						//InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", model.LoyalityPercentage.ToString(CultureInfo.InvariantCulture));

						var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
						var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeScope);

						switch (model.ContractTypeId)
						{
							case 1:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractA.ToString(CultureInfo.InvariantCulture));
								break;
							case 2:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractB.ToString(CultureInfo.InvariantCulture));
								break;
							case 3:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractC.ToString(CultureInfo.InvariantCulture));
								break;
							case 4:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractD.ToString(CultureInfo.InvariantCulture));
								break;
							case 26:
								InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractZ.ToString(CultureInfo.InvariantCulture));
								break;
						}


						InsertUpdateAttribute(customer, attributeList.ToList(), "VatRegimeId", model.VatRegimeId.ToString(CultureInfo.InvariantCulture));
						InsertUpdateAttribute(customer, attributeList.ToList(), "StatusCodeId", model.StatusCodeTypeId.ToString(CultureInfo.InvariantCulture));
						//Save Customer Image
						if (model.CustomerPictureId > 0)
						{
							InsertUpdateAttribute(customer, attributeList.ToList(), "CustomerPictureId",
							model.CustomerPictureId.ToString(CultureInfo.InvariantCulture));
						}

						if (model.ActivationDate.HasValue)
							InsertUpdateAttribute(customer, attributeList.ToList(), "ActivationDate", model.ActivationDate.ToString());
						if (model.EndDate.HasValue)
							InsertUpdateAttribute(customer, attributeList.ToList(), "EndDate", model.EndDate.ToString());

						InsertUpdateAttribute(customer, attributeList.ToList(), "AuthorizationTypeId", model.AuthorizationTypeId.ToString(CultureInfo.InvariantCulture));
					}

					//newsletter subscriptions
					if (!String.IsNullOrEmpty(customer.Email))
					{
						var allStores = _storeService.GetAllStores();
						foreach (var store in allStores)
						{
							var newsletterSubscription = _newsLetterSubscriptionService
								 .GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
							if (model.SelectedNewsletterSubscriptionStoreIds != null &&
								 model.SelectedNewsletterSubscriptionStoreIds.Contains(store.Id))
							{
								//subscribed
								if (newsletterSubscription == null)
								{
									_newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
									{
										NewsLetterSubscriptionGuid = Guid.NewGuid(),
										Email = customer.Email,
										Active = true,
										StoreId = store.Id,
										CreatedOnUtc = DateTime.UtcNow
									});
								}
							}
							else
							{
								//not subscribed
								if (newsletterSubscription != null)
								{
									_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletterSubscription);
								}
							}
						}
					}


					//customer roles
					foreach (var customerRole in allCustomerRoles)
					{
						//ensure that the current customer cannot add/remove to/from "Administrators" system role
						//if he's not an admin himself
						if (customerRole.SystemName == SystemCustomerRoleNames.Administrators &&
							 !_workContext.CurrentCustomer.IsAdmin())
							continue;

						if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
						{
							//new role
							if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) == 0)
								customer.CustomerRoles.Add(customerRole);
						}
						else
						{
							//prevent attempts to delete the administrator role from the user, if the user is the last active administrator
							if (customerRole.SystemName == SystemCustomerRoleNames.Administrators && !SecondAdminAccountExists(customer))
							{
								ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.DeleteRole"));
								continue;
							}

							//remove role
							if (customer.CustomerRoles.Count(cr => cr.Id == customerRole.Id) > 0)
								customer.CustomerRoles.Remove(customerRole);
						}
					}
					_customerService.UpdateCustomer(customer);


					//ensure that a customer with a vendor associated is not in "Administrators" role
					//otherwise, he won't have access to the other functionality in admin area
					if (customer.IsAdmin() && customer.VendorId > 0)
					{
						customer.VendorId = 0;
						_customerService.UpdateCustomer(customer);
						ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminCouldNotbeVendor"));
					}

					//ensure that a customer in the Vendors role has a vendor account associated.
					//otherwise, he will have access to ALL products
					if (customer.IsVendor() && customer.VendorId == 0)
					{
						var vendorRole = customer
							 .CustomerRoles
							 .FirstOrDefault(x => x.SystemName == SystemCustomerRoleNames.Vendors);
						customer.CustomerRoles.Remove(vendorRole);
						_customerService.UpdateCustomer(customer);
						ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.CannotBeInVendoRoleWithoutVendorAssociated"));
					}


					//activity log
					_customerActivityService.InsertActivity("EditCustomer", _localizationService.GetResource("ActivityLog.EditCustomer"), customer.Id);

					SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Updated"));
					if (continueEditing)
					{
						//selected tab
						SaveSelectedTabName();

						return RedirectToAction("Edit", new { id = customer.Id });
					}
					return RedirectToRoute("AdminCustomerList");
				}
				catch (Exception exc)
				{
					ErrorNotification(exc.Message, false);
					//throw exc;
				}
			}


			//If we got this far, something failed, redisplay form
			PrepareCustomerModel(model, customer, true);

			if (!string.IsNullOrEmpty(customer.CompanyId.ToString(CultureInfo.InvariantCulture)))
				model.CompanyId = customer.CompanyId;
			var customerAttributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
			var currentPassword = GetAttributeValue(customerAttributeList.ToList(), "AccessWord");
			model.CurrentPassword = !string.IsNullOrEmpty(currentPassword) ? currentPassword : "";

			return View("~/Plugins/PostXCustom/Views/AdminCustomer/Edit.cshtml", model);
		}

		public void InsertUpdateAttribute(Customer customer, List<GenericAttribute> genericAttributes, string attributeName, string attributeValue)
		{
			if (string.IsNullOrEmpty(attributeValue))
				return;

			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeName);
			if (genericAttribute == null)
			{
				genericAttribute = new GenericAttribute
				{
					EntityId = customer.Id,
					Key = attributeName,
					KeyGroup = "Customer",
					Value = attributeValue,
					StoreId = 0
				};
				_genericAttributeService.InsertAttribute(genericAttribute);
			}
			else
			{
				genericAttribute.Value = attributeValue;
				_genericAttributeService.UpdateAttribute(genericAttribute);
			}
		}

		[HttpPost]
		public virtual ActionResult Delete(int id)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var customer = _customerService.GetCustomerById(id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToRoute("AdminCustomerList");

			try
			{
				//prevent attempts to delete the user, if it is the last active administrator
				if (customer.IsAdmin() && !SecondAdminAccountExists(customer))
				{
					ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.AdminAccountShouldExists.DeleteAdministrator"));
					//return RedirectToAction("Edit", new { id = customer.Id });
					return RedirectToRoute("AdminCustomerEdit", new { id = customer.Id });
				}

				//ensure that the current customer cannot delete "Administrators" if he's not an admin himself
				if (customer.IsAdmin() && !_workContext.CurrentCustomer.IsAdmin())
				{
					ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.OnlyAdminCanDeleteAdmin"));
					//return RedirectToAction("Edit", new { id = customer.Id });
					return RedirectToRoute("AdminCustomerEdit", new { id = customer.Id });
				}

				//delete
				_customerService.DeleteCustomer(customer);

				//remove newsletter subscription (if exists)
				foreach (var store in _storeService.GetAllStores())
				{
					var subscription = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email, store.Id);
					if (subscription != null)
						_newsLetterSubscriptionService.DeleteNewsLetterSubscription(subscription);
				}


				#region Remove Company Mapping

				var mappedCustomerList = _companyRepresentativeCustomerMappingService.GetCustomersBySalesPerson(customer.Id);
				if (mappedCustomerList.Any())
				{
					var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
					var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeScope);
					foreach (var mappedCustomer in mappedCustomerList)
					{
						var customerDetail = _customerService.GetCustomerById(mappedCustomer.AssociatedCustomerId);
						if (customerDetail != null)
						{
							var customerAttributeList = _genericAttributeService.GetAttributesForEntity(customerDetail.Id, "Customer");
							InsertUpdateAttribute(customerDetail, customerAttributeList.ToList(), "ContractTypeId", "4");
							InsertUpdateAttribute(customerDetail, customerAttributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractD.ToString(CultureInfo.InvariantCulture));
						}
						_companyRepresentativeCustomerMappingService.Delete(mappedCustomer);
					}
				}

				#endregion


				//activity log
				_customerActivityService.InsertActivity("DeleteCustomer", _localizationService.GetResource("ActivityLog.DeleteCustomer"), customer.Id);

				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Deleted"));
				return RedirectToRoute("AdminCustomerList");
			}
			catch (Exception exc)
			{
				ErrorNotification(exc.Message);
				//return RedirectToAction("Edit", new { id = customer.Id });
				return RedirectToRoute("AdminCustomerEdit", new { id = customer.Id });
			}
		}

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("changepassword")]
		public virtual ActionResult ChangePassword(AdminCustomerModel model)
		{

			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var customer = _customerService.GetCustomerById(model.Id);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToRoute("AdminCustomerList");

			//ensure that the current customer cannot change passwords of "Administrators" if he's not an admin himself
			if (customer.IsAdmin() && !_workContext.CurrentCustomer.IsAdmin())
			{
				ErrorNotification(_localizationService.GetResource("Admin.Customers.Customers.OnlyAdminCanChangePassword"));
				return RedirectToAction("Edit", new { id = customer.Id });
			}

			if (ModelState.IsValid)
			{
				//var changePassRequest = new ChangePasswordRequest(model.Email,
				//	 false, _customerSettings.DefaultPasswordFormat, model.Password);
				var changePassRequest = new ChangePasswordRequest(model.Username,
					 false, _customerSettings.DefaultPasswordFormat, model.Password);
				var changePassResult = _customerRegistrationService.ChangePassword(changePassRequest);
				if (changePassResult.Success)
				{
					var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
					InsertUpdateAttribute(customer, attributeList.ToList(), "AccessWord", model.Password);
					SuccessNotification(_localizationService.GetResource("Admin.Company.PasswordChanged"));
				}
				else
					foreach (var error in changePassResult.Errors)
						ErrorNotification(error);
			}

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		#endregion

		#region Manual Customer Push To LS(LightSpeed)

		[HttpPost, ActionName("Edit")]
		[FormValueRequired("pushcustomer")]
		public ActionResult LSCustomerPush(AdminCustomerModel model)
		{
			#region Push Customer to CMS
			var customer = _customerService.GetCustomerById(model.Id);

			var checkCmsCustomerMap = _cmsNopCustomerMappingService.GetByNopCustomerId(customer.Id);

			if (checkCmsCustomerMap != null && checkCmsCustomerMap.Deleted == false)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Customer Push :: Error :: Customer already pushed to CMS :: Username :: " + customer.Username + " :: LS Customer Id :: " + checkCmsCustomerMap.CmsCustomerId);
				_customCodeService.SendErrorEmail("Manual Customer Push :: Error :: Customer already pushed to CMS :: Username :: " + customer.Username + " :: LS Customer Id :: " + checkCmsCustomerMap.CmsCustomerId, "Manual Customer Create :: Error :: Customer already pushed to CMS :: Username" + customer.Username, string.Empty);
				ErrorNotification("Manual Customer Push :: Error :: Customer already pushed to CMS :: Username :: " + customer.Username + " :: LS Customer Id :: " + checkCmsCustomerMap.CmsCustomerId);
				return RedirectToAction("Edit", new { id = customer.Id });
			}

			var result = CreateCmsCustomer(customer);
			if (!string.IsNullOrEmpty(result))
			{
				if (checkCmsCustomerMap == null)
				{
					checkCmsCustomerMap = new CmsCustomerNopCustomerMapping
					{
						NopCustomerId = customer.Id,
						CmsCustomerId = Convert.ToInt32(result),
						CreatedOnUtc = DateTime.UtcNow,
						UpdatedOnUtc = DateTime.UtcNow,
						Deleted = false
					};

					_cmsNopCustomerMappingService.Insert(checkCmsCustomerMap);
					CreateCustomerLoyaltyCard(customer, checkCmsCustomerMap.CmsCustomerId);
				}
				else
				{
					checkCmsCustomerMap.CmsCustomerId = Convert.ToInt32(result);
					checkCmsCustomerMap.UpdatedOnUtc = DateTime.UtcNow;
					checkCmsCustomerMap.Deleted = false;
					_cmsNopCustomerMappingService.Update(checkCmsCustomerMap);
					CreateCustomerLoyaltyCard(customer, checkCmsCustomerMap.CmsCustomerId);
				}

				customer.Active = true;
				_customerService.UpdateCustomer(customer);

				//var cmsNopCustomerMap = new CmsCustomerNopCustomerMapping
				//{
				//	NopCustomerId = customer.Id,
				//	CmsCustomerId = Convert.ToInt32(result),
				//	CreatedOnUtc = DateTime.UtcNow,
				//	UpdatedOnUtc = DateTime.UtcNow
				//};

				//_cmsNopCustomerMappingService.Insert(cmsNopCustomerMap);
				//CreateCustomerLoyaltyCard(customer, cmsNopCustomerMap.CmsCustomerId);
				SuccessNotification("Customer pushed to CMS successfully.");
				return RedirectToAction("Edit", new { id = customer.Id });
			}
			ErrorNotification("Manual Customer Push :: Error :: Customer push to CMS failed :: Username" + customer.Username);
			#endregion

			return RedirectToAction("Edit", new { id = customer.Id });
		}

		public string CreateCmsCustomer(Customer customer)
		{
			try
			{
				string token = GetToken();

				var clientName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName);
				var clientLastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName);

				if (string.IsNullOrEmpty(token))
				{
					return "";
				}

				string url = @"https://euc1.posios.com/PosServer/rest/core/customer";
				var postData = new StringBuilder();

				postData.Append("{\"city\":\"" + string.Empty + "\",");
				postData.Append("\"companyId\":32123,");
				postData.Append("\"country\":\"" + "BE" + "\",");
				postData.Append("\"credits\":0,");

				postData.Append("\"dateOfBirth\":\"\",");
				postData.Append("\"email\":\"" + customer.Email + "\",");
				//postData.Append("\"firstName\":\"" + "0 reward points available." + "\",");
				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"language\":\"" + "nl_BE" + "\",");
				//postData.Append("\"lastName\":\"" + customer.GetFullName() + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\",");
				postData.Append("\"modificationTime\":\"\",");
				postData.Append("\"note\":\"" + "Test Note" + "\",");
				postData.Append("\"street\":\"\",");
				postData.Append("\"streetNumber\":\"\",");
				postData.Append("\"telephone\":\"\",");
				postData.Append("\"uuid\":\"\",");
				postData.Append("\"vatNumber\":\"\",");
				postData.Append("\"zip\":\"\"}");


				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Customer#: " + customer.Id, postData.ToString());

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				return responseData;
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Customer Push :: Error :: Customer push to CMS failed :: Username" + customer.Username, ex.Message + "  ||  " + ex.StackTrace);
				_customCodeService.SendErrorEmail("Manual Customer Push :: Error :: Customer push to CMS failed :: Username" + customer.Username, ex.Message, ex.StackTrace);
				return string.Empty;
			}
		}

		public string GetToken()
		{
			try
			{
				string token = string.Empty;
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Customer Push :: Error :: Error generating Token", ex.Message + "  ||  " + ex.StackTrace);
				_customCodeService.SendErrorEmail("Manual Customer Push :: Error :: Error generating Token", ex.Message, ex.StackTrace);
				return string.Empty;
			}
		}

		public string CreateCustomerLoyaltyCard(Customer customer, int cmsCustomerId)
		{
			try
			{
				string token = GetToken();
				string url = @"https://euc1.posios.com/PosServer/rest/core/customer/" + cmsCustomerId + "/loyaltycard";
				var postData = new StringBuilder();
				postData.Append("{\"barcode\":\"" + customer.Username + "\"}");

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				return responseData;
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Customer Push :: Error ::Error creating Loyalty Card for Nop Customer :: Username" + customer.Username + " :: LS CustomerId :: " + cmsCustomerId, ex.Message + "  ||  " + ex.StackTrace);
				_customCodeService.SendErrorEmail("Manual Customer Push :: Error :: Error creating Loyalty Card for Nop Customer :: Username" + customer.Username + " :: LS CMS Customer ID :: " + cmsCustomerId, ex.Message, ex.StackTrace);
				return string.Empty;
			}
		}
		#endregion


		#region Update Child Customer Address

		[NonAction]
		protected virtual void PrepareAddressModel(Nop.Admin.Models.Customers.CustomerAddressModel model, Address address, Customer customer, bool excludeProperties)
		{
			if (customer == null)
				throw new ArgumentNullException("customer");

			model.CustomerId = customer.Id;
			if (address != null)
			{
				if (!excludeProperties)
				{
					model.Address = address.ToModel();
				}
			}

			if (model.Address == null)
				model.Address = new Nop.Admin.Models.Common.AddressModel();

			model.Address.FirstNameEnabled = true;
			model.Address.FirstNameRequired = true;
			model.Address.LastNameEnabled = true;
			model.Address.LastNameRequired = true;
			model.Address.EmailEnabled = true;
			model.Address.EmailRequired = true;
			model.Address.CompanyEnabled = _addressSettings.CompanyEnabled;
			model.Address.CompanyRequired = _addressSettings.CompanyRequired;
			model.Address.CountryEnabled = _addressSettings.CountryEnabled;
			model.Address.CountryRequired = _addressSettings.CountryEnabled; //country is required when enabled
			model.Address.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
			model.Address.CityEnabled = _addressSettings.CityEnabled;
			model.Address.CityRequired = _addressSettings.CityRequired;
			model.Address.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
			model.Address.StreetAddressRequired = _addressSettings.StreetAddressRequired;
			model.Address.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
			model.Address.StreetAddress2Required = _addressSettings.StreetAddress2Required;
			model.Address.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
			model.Address.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
			model.Address.PhoneEnabled = _addressSettings.PhoneEnabled;
			model.Address.PhoneRequired = _addressSettings.PhoneRequired;
			model.Address.FaxEnabled = _addressSettings.FaxEnabled;
			model.Address.FaxRequired = _addressSettings.FaxRequired;
			//countries
			model.Address.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
			foreach (var c in _countryService.GetAllCountries(showHidden: true))
				model.Address.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == model.Address.CountryId) });
			//states
			var states = model.Address.CountryId.HasValue ? _stateProvinceService.GetStateProvincesByCountryId(model.Address.CountryId.Value, showHidden: true).ToList() : new List<StateProvince>();
			if (states.Any())
			{
				foreach (var s in states)
					model.Address.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == model.Address.StateProvinceId) });
			}
			else
				model.Address.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });
			//customer attribute services
			model.Address.PrepareCustomAddressAttributes(address, _addressAttributeService, _addressAttributeParser);
		}



		public virtual ActionResult AddressEdit(int addressId, int customerId)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var customer = _customerService.GetCustomerById(customerId);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToRoute("AdminCustomerList");

			var address = _addressService.GetAddressById(addressId);
			if (address == null)
				//No address found with the specified id
				return RedirectToAction("Edit", new { id = customer.Id });

			var model = new Nop.Admin.Models.Customers.CustomerAddressModel();
			PrepareAddressModel(model, address, customer, false);
//			return View(model);
			return View("~/Plugins/PostXCustom/Views/AdminCustomer/AddressEdit.cshtml", model);
		}

		[HttpPost]
		[ValidateInput(false)]
		public virtual ActionResult AddressEdit(Nop.Admin.Models.Customers.CustomerAddressModel model, FormCollection form)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCustomers))
				return RedirectToAction("AccessDenied", new { pageUrl = Request.RawUrl });

			var customer = _customerService.GetCustomerById(model.CustomerId);
			if (customer == null)
				//No customer found with the specified id
				return RedirectToRoute("AdminCustomerList");

			var address = _addressService.GetAddressById(model.Address.Id);
			if (address == null)
				//No address found with the specified id
				return RedirectToAction("Edit", new { id = customer.Id });

			//custom address attributes
			var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
			var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
			foreach (var error in customAttributeWarnings)
			{
				ModelState.AddModelError("", error);
			}

			if (ModelState.IsValid)
			{
				address = model.Address.ToEntity(address);
				address.CustomAttributes = customAttributes;
				_addressService.UpdateAddress(address);

				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Addresses.Updated"));
				return RedirectToAction("AddressEdit", new { addressId = model.Address.Id, customerId = model.CustomerId });
			}

			//If we got this far, something failed, redisplay form
			PrepareAddressModel(model, address, customer, true);

			//return View(model);
			return View("~/Plugins/PostXCustom/Views/AdminCustomer/AddressEdit.cshtml", model);
		}

		/// <summary>
		/// Custom Code- To mark address as default company address.
		/// </summary>
		/// <param name="parentCustomerId"></param>
		/// <param name="addressId"></param>
		/// <returns></returns>
		public virtual ActionResult MarkAsDefaultAddress(int parentCustomerId, int addressId)
		{
			var customerGenericAttributes = _genericAttributeService.GetAttributesForEntity(parentCustomerId, "Customer");
			var oldAddressId = 0;

			if (customerGenericAttributes.Any())
			{
				var addressGenericAttribute = customerGenericAttributes.FirstOrDefault(x => x.Key == "CompanyAddressId");
				if (addressGenericAttribute == null)
				{
					addressGenericAttribute = new GenericAttribute
					{
						EntityId = parentCustomerId,
						KeyGroup = "Customer",
						Key = "CompanyAddressId",
						Value = addressId.ToString(),
						StoreId = 0
					};
					_genericAttributeService.InsertAttribute(addressGenericAttribute);
				}
				else
				{
					oldAddressId = Convert.ToInt32(addressGenericAttribute.Value);
					addressGenericAttribute.Value = addressId.ToString();
					_genericAttributeService.UpdateAttribute(addressGenericAttribute);
				}
			}
			else
			{
				var addressGenericAttribute = new GenericAttribute
				{
					EntityId = parentCustomerId,
					KeyGroup = "Customer",
					Key = "CompanyAddressId",
					Value = addressId.ToString(),
					StoreId = 0
				};
				_genericAttributeService.InsertAttribute(addressGenericAttribute);
			}
			

			if (oldAddressId == addressId)
			{
				SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Addresses.MarkedAsDefaultCompanyAddress"));
				return RedirectToAction("AddressEdit", new { addressId = addressId, customerId = parentCustomerId });
			}

			return RedirectToAction("UpdateChildCustomerAddress", "AdminCustomer", new { parentCustomerId = parentCustomerId, newAddressId = addressId, oldAddressId = oldAddressId });
		}


		public ActionResult UpdateChildCustomerAddress(int parentCustomerId, int newAddressId, int oldAddressId)
		{
			var customer = _customerService.GetCustomerById(parentCustomerId);
			if (customer == null)
			{
				ErrorNotification("Update child customer address failed. Parent customer not found.");
			}

			if (string.IsNullOrEmpty(customer.Username))
			{
				ErrorNotification("Update child customer address failed. Parent Customer does not have a valid username.");

			}

			var companyId = customer.Username.Substring(0,3);

			var childCustomerList = _companyRepresentativeCustomerMappingService.GetAllChildCustomerInfoByCompanyId(companyId.ToString());

			if (childCustomerList.Any())
			{
				foreach (var childCustomer in childCustomerList)
				{
					var address = childCustomer.Addresses.FirstOrDefault(a => a.Id == oldAddressId);
					if (address == null)
						//No customer found with the specified id
						continue;
					childCustomer.RemoveAddress(address);
					_customerService.UpdateCustomer(childCustomer);

					address = _addressService.GetAddressById(newAddressId);
					if (address == null)
						//No customer found with the specified id
						continue;

					childCustomer.Addresses.Add(address);
					_customerService.UpdateCustomer(childCustomer);

				}
			}

			SuccessNotification(_localizationService.GetResource("Admin.Customers.Customers.Addresses.MarkedAsDefaultCompanyAddress"));
			return RedirectToAction("AddressEdit", new { addressId = newAddressId, customerId = parentCustomerId });
		}



		

		#endregion

	}
}