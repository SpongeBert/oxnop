﻿using System.Web.Mvc;
using Nop.Core;
using Nop.Plugin.PostXCustom.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class PostXCustomController : BasePluginController
	{
		private readonly ILogger _logger;
		private readonly ISettingService _settingService;
		private readonly ILocalizationService _localizationService;
		private readonly IWorkContext _workContext;
		private readonly IStoreService _storeService;


		public PostXCustomController(IWorkContext workContext, IStoreService storeService, ILogger logger, ISettingService settingService,
			 ILocalizationService localizationService)
		{
			_logger = logger;
			_settingService = settingService;
			_localizationService = localizationService;
			_workContext = workContext;
			_storeService = storeService;
		}
		
		[AdminAuthorize]
		[ChildActionOnly]
		public ActionResult Configure()
		{																									  
			return View("~/Plugins/PostXCustom/Views/PostXCustom/Configure.cshtml");
		}


		[AdminAuthorize]
		public ActionResult CustomConfigure()
		{
			return View("~/Plugins/PostXCustom/Views/PostXCustom/CustomConfigure.cshtml");
		}

		[AdminAuthorize]
		public ActionResult PostXCustomSettings()
		{
			var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
			var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeScope);

			var model = new PostXCustomSettingsModel
			{
				CustomerPushUrl = navNopSyncSettings.CustomerPushUrl,
				LoyaltyContractA = navNopSyncSettings.LoyaltyContractA,
				LoyaltyContractB = navNopSyncSettings.LoyaltyContractB,
				LoyaltyContractC = navNopSyncSettings.LoyaltyContractC,
				LoyaltyContractD = navNopSyncSettings.LoyaltyContractD,
				LoyaltyContractZ =  navNopSyncSettings.LoyaltyContractZ 
			};


			return View("~/Plugins/PostXCustom/Views/PostXCustom/PostXCustomSettings.cshtml", model);
		}

		[AdminAuthorize]
		[HttpPost]
		public ActionResult PostXCustomSettings(PostXCustomSettingsModel model)
		{
			if (!ModelState.IsValid)
				return PostXCustomSettings();

			var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
			var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeScope);

			navNopSyncSettings.CustomerPushUrl = model.CustomerPushUrl;
			navNopSyncSettings.LoyaltyContractA = model.LoyaltyContractA;
			navNopSyncSettings.LoyaltyContractB = model.LoyaltyContractB;
			navNopSyncSettings.LoyaltyContractC = model.LoyaltyContractC;
			navNopSyncSettings.LoyaltyContractD = model.LoyaltyContractD;
			navNopSyncSettings.LoyaltyContractZ = model.LoyaltyContractZ;

			_settingService.SaveSetting(navNopSyncSettings);

			_settingService.ClearCache();

			SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));


			return PostXCustomSettings();
		}

		

	}
}
