﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Plugin.PostXCustom.Factories.Public;
using Nop.Services.Common;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public class PublicOrderController : BasePluginController
	{
		#region Fields											
		private readonly IOrderModelFactory _orderModelFactory;
		private readonly IOrderService _orderService;
		private readonly IWorkContext _workContext;
		private readonly IOrderProcessingService _orderProcessingService;
		private readonly IPaymentService _paymentService;
		private readonly IPdfService _pdfService;
		private readonly IWebHelper _webHelper;
		#endregion

		#region Constructors

		public PublicOrderController(IOrderModelFactory orderModelFactory,IOrderService orderService,IWorkContext workContext,
		  IOrderProcessingService orderProcessingService,IPaymentService paymentService,IPdfService pdfService,
		  IWebHelper webHelper)
		{
			_orderModelFactory = orderModelFactory;
			_orderService = orderService;
			_workContext = workContext;
			_orderProcessingService = orderProcessingService;
			_paymentService = paymentService;
			_pdfService = pdfService;
			_webHelper = webHelper;
		}			  
		#endregion


		#region Methods

		//My account / Orders
		[NopHttpsRequirement(SslRequirement.Yes)]
		public virtual ActionResult CustomerOrders()
		{
			if (!_workContext.CurrentCustomer.IsRegistered())
				return new HttpUnauthorizedResult();

			var model = _orderModelFactory.PrepareCustomerOrderListModel();
			return View("~/Plugins/PostXCustom/Views/PublicOrder/CustomerOrders.cshtml", model);
		}

		//My account / Orders / Cancel recurring order
		[HttpPost, ActionName("CustomerOrders")]
		[PublicAntiForgery]
		[FormValueRequired(FormValueRequirement.StartsWith, "cancelRecurringPayment")]
		public virtual ActionResult CancelRecurringPayment(FormCollection form)
		{
			if (!_workContext.CurrentCustomer.IsRegistered())
				return new HttpUnauthorizedResult();

			//get recurring payment identifier
			int recurringPaymentId = 0;
			foreach (var formValue in form.AllKeys)
				if (formValue.StartsWith("cancelRecurringPayment", StringComparison.InvariantCultureIgnoreCase))
					recurringPaymentId = Convert.ToInt32(formValue.Substring("cancelRecurringPayment".Length));

			var recurringPayment = _orderService.GetRecurringPaymentById(recurringPaymentId);
			if (recurringPayment == null)
			{
				return RedirectToRoute("CustomerOrders");
			}

			if (_orderProcessingService.CanCancelRecurringPayment(_workContext.CurrentCustomer, recurringPayment))
			{
				var errors = _orderProcessingService.CancelRecurringPayment(recurringPayment);

				var model = _orderModelFactory.PrepareCustomerOrderListModel();
				model.RecurringPaymentErrors = errors;

				return View(model);
			}

			return RedirectToRoute("CustomerOrders");
		}

		//My account / Orders / Retry last recurring order
		[HttpPost, ActionName("CustomerOrders")]
		[PublicAntiForgery]
		[FormValueRequired(FormValueRequirement.StartsWith, "retryLastPayment")]
		public virtual ActionResult RetryLastRecurringPayment(FormCollection form)
		{
			if (!_workContext.CurrentCustomer.IsRegistered())
				return new HttpUnauthorizedResult();

			//get recurring payment identifier
			var recurringPaymentId = 0;
			if (!form.AllKeys.Any(formValue => formValue.StartsWith("retryLastPayment", StringComparison.InvariantCultureIgnoreCase) &&
				 int.TryParse(formValue.Substring(formValue.IndexOf('_') + 1), out recurringPaymentId)))
			{
				return RedirectToRoute("CustomerOrders");
			}

			var recurringPayment = _orderService.GetRecurringPaymentById(recurringPaymentId);
			if (recurringPayment == null)
				return RedirectToRoute("CustomerOrders");

			if (!_orderProcessingService.CanRetryLastRecurringPayment(_workContext.CurrentCustomer, recurringPayment))
				return RedirectToRoute("CustomerOrders");

			var errors = _orderProcessingService.ProcessNextRecurringPayment(recurringPayment);
			var model = _orderModelFactory.PrepareCustomerOrderListModel();
			model.RecurringPaymentErrors = errors.ToList();

			return View(model);
		}

		//My account / Reward points
		[NopHttpsRequirement(SslRequirement.Yes)]
		public virtual ActionResult CustomerRewardPoints()
		{
			if (!_workContext.CurrentCustomer.IsRegistered())
				return new HttpUnauthorizedResult();			

			var model = _orderModelFactory.PrepareCustomerRewardPoints();
			return View("~/Plugins/PostXCustom/Views/PublicOrder/CustomerRewardPoints.cshtml", model);
		}

		//My account / Order details page
		[NopHttpsRequirement(SslRequirement.Yes)]
		public virtual ActionResult Details(int orderId)
		{
			var order = _orderService.GetOrderById(orderId);
			if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
				return new HttpUnauthorizedResult();

			var model = _orderModelFactory.PrepareOrderDetailsModel(order);
			return View("~/Plugins/PostXCustom/Views/PublicOrder/Details.cshtml", model);
		}

		//My account / Order details page / Print
		[NopHttpsRequirement(SslRequirement.Yes)]
		public virtual ActionResult PrintOrderDetails(int orderId)
		{
			var order = _orderService.GetOrderById(orderId);
			if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
				return new HttpUnauthorizedResult();

			var model = _orderModelFactory.PrepareOrderDetailsModel(order);
			model.PrintMode = true;


			return View("~/Plugins/PostXCustom/Views/PublicOrder/Details.cshtml", model);
		}

		//My account / Order details page / PDF invoice
		public virtual ActionResult GetPdfInvoice(int orderId)
		{
			var order = _orderService.GetOrderById(orderId);
			if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
				return new HttpUnauthorizedResult();

			var orders = new List<Order> {order};
			byte[] bytes;
			using (var stream = new MemoryStream())
			{
				_pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
				bytes = stream.ToArray();
			}
			return File(bytes, MimeTypes.ApplicationPdf, string.Format("order_{0}.pdf", order.Id));
		}

		//My account / Order details page / re-order
		public virtual ActionResult ReOrder(int orderId)
		{
			var order = _orderService.GetOrderById(orderId);
			if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
				return new HttpUnauthorizedResult();

			_orderProcessingService.ReOrder(order);
			return RedirectToRoute("ShoppingCart");
		}

		//My account / Order details page / Complete payment
		[HttpPost, ActionName("Details")]
		[PublicAntiForgery]
		[FormValueRequired("repost-payment")]
		public virtual ActionResult RePostPayment(int orderId)
		{
			var order = _orderService.GetOrderById(orderId);
			if (order == null || order.Deleted || _workContext.CurrentCustomer.Id != order.CustomerId)
				return new HttpUnauthorizedResult();

			if (!_paymentService.CanRePostProcessPayment(order))
				return RedirectToRoute("OrderDetails", new {orderId });

			var postProcessPaymentRequest = new PostProcessPaymentRequest
			{
				Order = order
			};
			_paymentService.PostProcessPayment(postProcessPaymentRequest);

			if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
			{
				//redirection or POST has been done in PostProcessPayment
				return Content("Redirected");
			}

			//if no redirection has been done (to a third-party payment page)
			//theoretically it's not possible
			return RedirectToRoute("OrderDetails", new {orderId });
		}

		#endregion


		#region Custom Methods
		public string GetToken()
		{
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			var token = json.GetValue("token").Value<string>();
			return token;
		}
		#endregion

	}
}
