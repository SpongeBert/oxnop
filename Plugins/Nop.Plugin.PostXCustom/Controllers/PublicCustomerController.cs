﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.Tax;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Factories.Public;
using Nop.Plugin.PostXCustom.Models.Public.Customers;
using Nop.Plugin.PostXCustom.Models.Public.Media;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Authentication;
using Nop.Services.Authentication.External;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Framework.Security.Honeypot;
using OnBarcode.Barcode;
using System.Drawing;
using System.IO;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class PublicCustomerController : BasePluginController
	{
		#region Fields
		private readonly ICustomerModelFactory _customerModelFactory;
		private readonly IAuthenticationService _authenticationService;
		private readonly DateTimeSettings _dateTimeSettings;
		private readonly TaxSettings _taxSettings;
		private readonly ILocalizationService _localizationService;
		private readonly IWorkContext _workContext;
		private readonly IStoreContext _storeContext;
		private readonly ICustomerService _customerService;
		private readonly ICustomerAttributeParser _customerAttributeParser;
		private readonly ICustomerAttributeService _customerAttributeService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly ICustomerRegistrationService _customerRegistrationService;
		private readonly ITaxService _taxService;
		private readonly CustomerSettings _customerSettings;

		private readonly IAddressService _addressService;
		private readonly INewsLetterSubscriptionService _newsLetterSubscriptionService;
		private readonly IOpenAuthenticationService _openAuthenticationService;
		private readonly IWebHelper _webHelper;
		private readonly IEventPublisher _eventPublisher;
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly LocalizationSettings _localizationSettings;
		private readonly CaptchaSettings _captchaSettings;
		private readonly IStoreService _storeService;
		private readonly ISettingService _settingService;
		private readonly ICustomerActivityService _customerActivityService;
		private readonly IShoppingCartService _shoppingCartService;
		private readonly StoreInformationSettings _storeInformationSettings;
		private readonly IPictureService _pictureService;
		private readonly MediaSettings _mediaSettings;
		private readonly ICompanyRepresentativeCustomerMappingService _companyRepresentativeCustomerMappingService;
		private readonly ILogger _logger;

		#endregion

		#region Ctor

		public PublicCustomerController(ICustomerModelFactory customerModelFactory, IAuthenticationService authenticationService, DateTimeSettings dateTimeSettings,
			 TaxSettings taxSettings, ILocalizationService localizationService, IWorkContext workContext, IStoreContext storeContext, ICustomerService customerService,
			 ICustomerAttributeParser customerAttributeParser, ICustomerAttributeService customerAttributeService, IGenericAttributeService genericAttributeService,
			 ICustomerRegistrationService customerRegistrationService, ITaxService taxService, CustomerSettings customerSettings, IAddressService addressService,
			 INewsLetterSubscriptionService newsLetterSubscriptionService, IOpenAuthenticationService openAuthenticationService, IWebHelper webHelper,
			 IEventPublisher eventPublisher, IWorkflowMessageService workflowMessageService, LocalizationSettings localizationSettings, CaptchaSettings captchaSettings,
			IStoreService storeService, ISettingService settingService, ICustomerActivityService customerActivityService,
			IShoppingCartService shoppingCartService, StoreInformationSettings storeInformationSettings, IPictureService pictureService,
			MediaSettings mediaSettings, ICompanyRepresentativeCustomerMappingService companyRepresentativeCustomerMappingService, ILogger logger)
		{
			_customerModelFactory = customerModelFactory;
			_authenticationService = authenticationService;
			_dateTimeSettings = dateTimeSettings;
			_taxSettings = taxSettings;
			_localizationService = localizationService;
			_workContext = workContext;
			_storeContext = storeContext;
			_customerService = customerService;
			_customerAttributeParser = customerAttributeParser;
			_customerAttributeService = customerAttributeService;
			_genericAttributeService = genericAttributeService;
			_customerRegistrationService = customerRegistrationService;
			_taxService = taxService;
			_customerSettings = customerSettings;
			_addressService = addressService;
			_newsLetterSubscriptionService = newsLetterSubscriptionService;
			_openAuthenticationService = openAuthenticationService;
			_webHelper = webHelper;
			_eventPublisher = eventPublisher;

			_workflowMessageService = workflowMessageService;
			_localizationSettings = localizationSettings;
			_captchaSettings = captchaSettings;
			_storeService = storeService;
			_settingService = settingService;
			_customerActivityService = customerActivityService;
			_shoppingCartService = shoppingCartService;
			_storeInformationSettings = storeInformationSettings;
			_pictureService = pictureService;
			_mediaSettings = mediaSettings;
			_companyRepresentativeCustomerMappingService = companyRepresentativeCustomerMappingService;
			_logger = logger;
		}

		#endregion

		#region Utilities

		[NonAction]
		protected virtual void TryAssociateAccountWithExternalAccount(Customer customer)
		{
			var parameters = ExternalAuthorizerHelper.RetrieveParametersFromRoundTrip(true);
			if (parameters == null)
				return;

			if (_openAuthenticationService.AccountExists(parameters))
				return;

			_openAuthenticationService.AssociateExternalAccountWithUser(customer, parameters);
		}

		[NonAction]
		protected virtual string ParseCustomCustomerAttributes(FormCollection form)
		{
			if (form == null)
				throw new ArgumentNullException("form");

			string attributesXml = "";
			var attributes = _customerAttributeService.GetAllCustomerAttributes();
			foreach (var attribute in attributes)
			{
				string controlId = string.Format("customer_attribute_{0}", attribute.Id);
				switch (attribute.AttributeControlType)
				{
					case AttributeControlType.DropdownList:
					case AttributeControlType.RadioList:
						{
							var ctrlAttributes = form[controlId];
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								int selectedAttributeId = int.Parse(ctrlAttributes);
								if (selectedAttributeId > 0)
									attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
										 attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
							}
						}
						break;
					case AttributeControlType.Checkboxes:
						{
							var cblAttributes = form[controlId];
							if (!String.IsNullOrEmpty(cblAttributes))
							{
								foreach (var item in cblAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
								)
								{
									int selectedAttributeId = int.Parse(item);
									if (selectedAttributeId > 0)
										attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
											 attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
								}
							}
						}
						break;
					case AttributeControlType.ReadonlyCheckboxes:
						{
							//load read-only (already server-side selected) values
							var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
							foreach (var selectedAttributeId in attributeValues
								 .Where(v => v.IsPreSelected)
								 .Select(v => v.Id)
								 .ToList())
							{
								attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
									 attribute, selectedAttributeId.ToString(CultureInfo.InvariantCulture));
							}
						}
						break;
					case AttributeControlType.TextBox:
					case AttributeControlType.MultilineTextbox:
						{
							var ctrlAttributes = form[controlId];
							if (!String.IsNullOrEmpty(ctrlAttributes))
							{
								string enteredText = ctrlAttributes.Trim();
								attributesXml = _customerAttributeParser.AddCustomerAttribute(attributesXml,
									 attribute, enteredText);
							}
						}
						break;
					case AttributeControlType.Datepicker:
					case AttributeControlType.ColorSquares:
					case AttributeControlType.ImageSquares:
					case AttributeControlType.FileUpload:
					//not supported customer attributes
					default:
						break;
				}
			}

			return attributesXml;
		}

		#endregion

		#region Login / logout


		[NopHttpsRequirement(SslRequirement.Yes)]
		//available even when a store is closed
		[StoreClosed(true)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult AutoLogin(string username, string password)
		{
			var model = _customerModelFactory.PrepareAutoLoginModel(username, password);
			if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
			{
				return View("~/Plugins/PostXCustom/Views/PublicCustomer/Login.cshtml", model);
			}

			if (ModelState.IsValid)
			{
				if (_customerSettings.UsernamesEnabled && model.Username != null)
				{
					model.Username = model.Username.Trim();
				}
				var loginResult =
					  _customerRegistrationService.ValidateCustomer(
							 _customerSettings.UsernamesEnabled ? model.Username : model.Email, model.Password);
				switch (loginResult)
				{
					case CustomerLoginResults.Successful:
						{
							var customer = _customerSettings.UsernamesEnabled
								  ? _customerService.GetCustomerByUsername(model.Username)
								  : _customerService.GetCustomerByEmail(model.Email);

							//migrate shopping cart
							_shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

							//sign in new customer
							_authenticationService.SignIn(customer, model.RememberMe);

							//raise event       
							_eventPublisher.Publish(new CustomerLoggedinEvent(customer));

							//activity log
							_customerActivityService.InsertActivity(customer, "PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"));

							#region Check Login validity

							if (customer.IsAdmin())
								return RedirectToRoute("AdminCustomerList");

							if (customer.IsInCustomerRole("CompanyRepresentative"))
								return RedirectToAction("List", "CompanyRepresentative");

							#endregion
							return RedirectToAction("Welcome");
						}
					case CustomerLoginResults.CustomerNotExist:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
						break;
					case CustomerLoginResults.Deleted:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
						break;
					case CustomerLoginResults.NotActive:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
						break;
					case CustomerLoginResults.NotRegistered:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
						break;
					case CustomerLoginResults.LockedOut:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.LockedOut"));
						break;
					case CustomerLoginResults.WrongPassword:
					default:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
						break;
				}
			}

			//If we got this far, something failed, redisplay form
			model = _customerModelFactory.PrepareLoginModel(model.CheckoutAsGuest);
			return View("~/Plugins/PostXCustom/Views/PublicCustomer/Login.cshtml", model);

		}


		[HttpPost]
		[CaptchaValidator]
		//available even when a store is closed
		[StoreClosed(true)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult AutoLogin(PublicLoginModel model, string returnUrl, bool captchaValid)
		{
			//validate CAPTCHA
			if (_captchaSettings.Enabled && _captchaSettings.ShowOnLoginPage && !captchaValid)
			{
				ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
			}

			if (ModelState.IsValid)
			{
				if (_customerSettings.UsernamesEnabled && model.Username != null)
				{
					model.Username = model.Username.Trim();
				}
				var loginResult =
					  _customerRegistrationService.ValidateCustomer(
							 _customerSettings.UsernamesEnabled ? model.Username : model.Email, model.Password);
				switch (loginResult)
				{
					case CustomerLoginResults.Successful:
						{
							var customer = _customerSettings.UsernamesEnabled
								  ? _customerService.GetCustomerByUsername(model.Username)
								  : _customerService.GetCustomerByEmail(model.Email);

							//migrate shopping cart
							_shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

							//sign in new customer
							_authenticationService.SignIn(customer, model.RememberMe);

							//raise event       
							_eventPublisher.Publish(new CustomerLoggedinEvent(customer));

							//activity log
							_customerActivityService.InsertActivity(customer, "PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"));

							#region Check Login validity

							if (customer.IsAdmin())
								return RedirectToRoute("AdminCustomerList");

							if (customer.IsInCustomerRole("CompanyRepresentative"))
								return RedirectToAction("List", "CompanyRepresentative");

							#endregion
							return RedirectToAction("Welcome");
						}
					case CustomerLoginResults.CustomerNotExist:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
						break;
					case CustomerLoginResults.Deleted:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
						break;
					case CustomerLoginResults.NotActive:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
						break;
					case CustomerLoginResults.NotRegistered:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
						break;
					case CustomerLoginResults.LockedOut:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.LockedOut"));
						break;
					case CustomerLoginResults.WrongPassword:
					default:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
						break;
				}
			}

			//If we got this far, something failed, redisplay form
			model = _customerModelFactory.PrepareLoginModel(model.CheckoutAsGuest);
			return View("~/Plugins/PostXCustom/Views/PublicCustomer/Login.cshtml", model);
		}



		[NopHttpsRequirement(SslRequirement.Yes)]
		//available even when a store is closed
		[StoreClosed(true)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult Login(bool? checkoutAsGuest)
		{
			var model = _customerModelFactory.PrepareLoginModel(checkoutAsGuest);
			return View("~/Plugins/PostXCustom/Views/PublicCustomer/Login.cshtml", model);

		}



		[HttpPost]
		[CaptchaValidator]
		//available even when a store is closed
		[StoreClosed(true)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult Login(PublicLoginModel model, string returnUrl, bool captchaValid)
		{
			//validate CAPTCHA
			if (_captchaSettings.Enabled && _captchaSettings.ShowOnLoginPage && !captchaValid)
			{
				ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
			}

			if (ModelState.IsValid)
			{
				if (_customerSettings.UsernamesEnabled && model.Username != null)
				{
					model.Username = model.Username.Trim();
				}
				var loginResult =
					 _customerRegistrationService.ValidateCustomer(
						  _customerSettings.UsernamesEnabled ? model.Username : model.Email, model.Password);
				switch (loginResult)
				{
					case CustomerLoginResults.Successful:
						{
							var customer = _customerSettings.UsernamesEnabled
								 ? _customerService.GetCustomerByUsername(model.Username)
								 : _customerService.GetCustomerByEmail(model.Email);

							//migrate shopping cart
							_shoppingCartService.MigrateShoppingCart(_workContext.CurrentCustomer, customer, true);

							//sign in new customer
							_authenticationService.SignIn(customer, model.RememberMe);

							//raise event       
							_eventPublisher.Publish(new CustomerLoggedinEvent(customer));

							//activity log
							_customerActivityService.InsertActivity(customer, "PublicStore.Login", _localizationService.GetResource("ActivityLog.PublicStore.Login"));

							#region Check Login validity

							if (customer.IsAdmin())
								return RedirectToRoute("AdminCustomerList");

							if (customer.IsInCustomerRole("CompanyRepresentative"))
								return RedirectToAction("List", "CompanyRepresentative");

							#endregion
							return RedirectToAction("Welcome");
						}
					case CustomerLoginResults.CustomerNotExist:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.CustomerNotExist"));
						break;
					case CustomerLoginResults.Deleted:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.Deleted"));
						break;
					case CustomerLoginResults.NotActive:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotActive"));
						break;
					case CustomerLoginResults.NotRegistered:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.NotRegistered"));
						break;
					case CustomerLoginResults.LockedOut:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials.LockedOut"));
						break;
					case CustomerLoginResults.WrongPassword:
					default:
						ModelState.AddModelError("", _localizationService.GetResource("Account.Login.WrongCredentials"));
						break;
				}
			}

			//If we got this far, something failed, redisplay form
			model = _customerModelFactory.PrepareLoginModel(model.CheckoutAsGuest);
			return View("~/Plugins/PostXCustom/Views/PublicCustomer/Login.cshtml", model);
		}

		//available even when a store is closed
		[StoreClosed(true)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult Logout()
		{
			//external authentication
			ExternalAuthorizerHelper.RemoveParameters();

			if (_workContext.OriginalCustomerIfImpersonated != null)
			{
				//activity log
				_customerActivityService.InsertActivity(_workContext.OriginalCustomerIfImpersonated,
					 "Impersonation.Finished",
					 _localizationService.GetResource("ActivityLog.Impersonation.Finished.StoreOwner"),
					 _workContext.CurrentCustomer.Email, _workContext.CurrentCustomer.Id);
				_customerActivityService.InsertActivity("Impersonation.Finished",
					 _localizationService.GetResource("ActivityLog.Impersonation.Finished.Customer"),
					 _workContext.OriginalCustomerIfImpersonated.Email, _workContext.OriginalCustomerIfImpersonated.Id);

				//logout impersonated customer
				_genericAttributeService.SaveAttribute<int?>(_workContext.OriginalCustomerIfImpersonated,
					 SystemCustomerAttributeNames.ImpersonatedCustomerId, null);

				//redirect back to customer details page (admin area)
				return this.RedirectToAction("Edit", "Customer",
					 new { id = _workContext.CurrentCustomer.Id, area = "Admin" });

			}

			//activity log
			_customerActivityService.InsertActivity("PublicStore.Logout", _localizationService.GetResource("ActivityLog.PublicStore.Logout"));

			//standard logout 
			_authenticationService.SignOut();

			//raise logged out event       
			_eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

			//EU Cookie
			if (_storeInformationSettings.DisplayEuCookieLawWarning)
			{
				//the cookie law message should not pop up immediately after logout.
				//otherwise, the user will have to click it again...
				//and thus next visitor will not click it... so violation for that cookie law..
				//the only good solution in this case is to store a temporary variable
				//indicating that the EU cookie popup window should not be displayed on the next page open (after logout redirection to homepage)
				//but it'll be displayed for further page loads
				TempData["nop.IgnoreEuCookieLawWarning"] = true;
			}

			return RedirectToRoute("HomePage");
		}

		#endregion


		#region Register

		[NopHttpsRequirement(SslRequirement.Yes)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult Register()
		{
			//check whether registration is allowed
			if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
				return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

			var model = new PublicRegisterModel();
			model = _customerModelFactory.PrepareRegisterModel(model, false, setDefaultValues: true);

			return View("~/Plugins/PostXCustom/Views/PublicCustomer/Register.cshtml", model);
		}


		private void AssignPersonId(int parentCompanyId, out int personId)
		{
			var companyUserList = _companyRepresentativeCustomerMappingService.GetCustomersBySalesPerson(parentCompanyId);

			if (companyUserList.Any())
			{
				var customerIdList = companyUserList.Select(x => x.AssociatedCustomerId);
				var customerList = _customerService.GetAllCustomers().Where(x => customerIdList.Contains(x.Id));
				var topCustomerIdRecord = customerList.OrderByDescending(x => x.PersonalId).First();
				if (topCustomerIdRecord == null)
				{
					personId = 2;
					return;
				}
				personId = topCustomerIdRecord.PersonalId + 1;
				return;
			}
			personId = 2;
		}


		public string GetAttributeValue(List<GenericAttribute> genericAttributes, string attributeToCheck)
		{
			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeToCheck);
			if (genericAttribute == null)
				return null;

			return genericAttribute.Value;
		}



		public virtual void AssignCompanyRepresentativeMapping(int recordId, int associatedCustomerId, int companyRepresentativeId)
		{
			if (associatedCustomerId == 0)
			{
				return;
			}

			var salesPersonCustomerMap = _companyRepresentativeCustomerMappingService.GetByCustomerId(associatedCustomerId);

			if (salesPersonCustomerMap == null && companyRepresentativeId == 0)
				return;


			if (salesPersonCustomerMap == null && companyRepresentativeId > 0)
			{
				salesPersonCustomerMap = new CompanyRepresentativeCustomerMapping
				{
					AssociatedCustomerId = associatedCustomerId,
					CompanyRepresentativeId = companyRepresentativeId
				};
				_companyRepresentativeCustomerMappingService.Insert(salesPersonCustomerMap);
				return;
			}

			if (salesPersonCustomerMap != null && companyRepresentativeId == 0)
			{
				_companyRepresentativeCustomerMappingService.Delete(salesPersonCustomerMap);
				return;
			}

			if (salesPersonCustomerMap != null && companyRepresentativeId > 0)
			{
				salesPersonCustomerMap.CompanyRepresentativeId = companyRepresentativeId;
				_companyRepresentativeCustomerMappingService.Update(salesPersonCustomerMap);
			}
		}

		public string GenerateRandomPassword()
		{
			//return System.Web.Security.Membership.GeneratePassword(7,2);
			//return Guid.NewGuid().ToString("N").Substring(0, 6);//Membership.GeneratePassword(8,1).Replace(".","a").Replace("*", "2").Replace("/", "@").Replace("\\", "g").Replace("`", "6").Replace("~", "k").Replace("!", "e").Replace("<", "h").Replace(">", "7").Replace("#", "d");
			Random generator = new Random();
			var password = generator.Next(0, 1000000).ToString("D6");
			return password;
		}


		[HttpPost]
		[CaptchaValidator]
		[HoneypotValidator]
		[PublicAntiForgery]
		[ValidateInput(false)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult Register(PublicRegisterModel model, string returnUrl, bool captchaValid, FormCollection form)
		{
			//check whether registration is allowed
			if (_customerSettings.UserRegistrationType == UserRegistrationType.Disabled)
				return RedirectToRoute("RegisterResult", new { resultId = (int)UserRegistrationType.Disabled });

			if (_workContext.CurrentCustomer.IsRegistered())
			{
				//Already registered customer. 
				_authenticationService.SignOut();

				//raise logged out event       
				_eventPublisher.Publish(new CustomerLoggedOutEvent(_workContext.CurrentCustomer));

				//Save a new record
				_workContext.CurrentCustomer = _customerService.InsertGuestCustomer();
			}
			var customer = _workContext.CurrentCustomer;
			customer.RegisteredInStoreId = _storeContext.CurrentStore.Id;

			//custom customer attributes
			var customerAttributesXml = ParseCustomCustomerAttributes(form);
			var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
			foreach (var error in customerAttributeWarnings)
			{
				ModelState.AddModelError("", error);
			}

			//validate CAPTCHA
			if (_captchaSettings.Enabled && _captchaSettings.ShowOnRegistrationPage && !captchaValid)
			{
				ModelState.AddModelError("", _captchaSettings.GetWrongCaptchaMessage(_localizationService));
			}

			#region Assign Username

			var parentCompany = _customerService.GetAllCustomers().FirstOrDefault(x => x.CompanyId == 1 && x.PersonalId == 1);
			int personId = 0;
			AssignPersonId(parentCompany.Id, out personId);
			int companyId = parentCompany.CompanyId;
			model.Username = companyId.ToString("D3") + personId.ToString("D5");
			model.Password = GenerateRandomPassword();

			#endregion

			if (ModelState.IsValid)
			{
				if (_customerSettings.UsernamesEnabled && model.Username != null)
				{
					model.Username = model.Username.Trim();
				}

				bool isApproved = _customerSettings.UserRegistrationType == UserRegistrationType.Standard;
				var registrationRequest = new CustomerRegistrationRequest(customer,
					 model.Email,
					 _customerSettings.UsernamesEnabled ? model.Username : model.Email,
					 model.Password,
					 _customerSettings.DefaultPasswordFormat,
					 _storeContext.CurrentStore.Id,
					 isApproved);
				var registrationResult = _customerRegistrationService.RegisterCustomer(registrationRequest);
				if (registrationResult.Success)
				{

					customer.PersonalId = personId;
					customer.CompanyId = companyId;
					_customerService.UpdateCustomer(customer);

					//properties
					if (_dateTimeSettings.AllowCustomersToSetTimeZone)
					{
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId, model.TimeZoneId);
					}
					//VAT number
					if (_taxSettings.EuVatEnabled)
					{
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.VatNumber, model.VatNumber);

						string vatName;
						string vatAddress;
						var vatNumberStatus = _taxService.GetVatNumberStatus(model.VatNumber, out vatName,
							 out vatAddress);
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.VatNumberStatusId, (int)vatNumberStatus);
						//send VAT number admin notification
						if (!String.IsNullOrEmpty(model.VatNumber) && _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
							_workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer, model.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);
					}


					if (customer.Id > 0)
						AssignCompanyRepresentativeMapping(0, customer.Id, parentCompany.Id);

					//form fields
					if (_customerSettings.GenderEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender, model.Gender);
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName, model.FirstName);
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName, model.LastName);
					if (_customerSettings.DateOfBirthEnabled)
					{
						DateTime? dateOfBirth = model.ParseDateOfBirth();
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth, dateOfBirth);
					}

					if (_customerSettings.StreetAddressEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress, model.StreetAddress);
					if (_customerSettings.StreetAddress2Enabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2, model.StreetAddress2);
					if (_customerSettings.ZipPostalCodeEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode, model.ZipPostalCode);
					if (_customerSettings.CityEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
					if (_customerSettings.CountryEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId, model.CountryId);
					if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId, model.StateProvinceId);
					if (_customerSettings.PhoneEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
					if (_customerSettings.FaxEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);


					var companyAttributeList = _genericAttributeService.GetAttributesForEntity(parentCompany.Id, "Customer");
					var attributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");

					InsertUpdateAttribute(customer, attributeList.ToList(), "AccessWord", model.Password);

					var companyName = parentCompany.GetAttribute<string>(SystemCustomerAttributeNames.Company);
					//Company Name
					if (!string.IsNullOrEmpty(companyName))
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company, companyName);

					var contractTypeId = GetAttributeValue(companyAttributeList.ToList(), "ContractTypeId");
					InsertUpdateAttribute(customer, attributeList.ToList(), "ContractTypeId", (!string.IsNullOrEmpty(contractTypeId) ? contractTypeId : "0"));

					var vatRegimeId = GetAttributeValue(companyAttributeList.ToList(), "VatRegimeId");
					InsertUpdateAttribute(customer, attributeList.ToList(), "VatRegimeId", (!string.IsNullOrEmpty(vatRegimeId) ? vatRegimeId : "0"));

					InsertUpdateAttribute(customer, attributeList.ToList(), "ContractTypeId", "4");

					var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
					var navNopSyncSettings = _settingService.LoadSetting<PostXSettings>(storeScope);

					InsertUpdateAttribute(customer, attributeList.ToList(), "LoyalityPercentage", navNopSyncSettings.LoyaltyContractD.ToString(CultureInfo.InvariantCulture));

					InsertUpdateAttribute(customer, attributeList.ToList(), "PersonalNumber", customer.Username);

					#region Company Address Map

					var addressGenericAttribute = companyAttributeList.FirstOrDefault(x => x.Key == "CompanyAddressId");
					if (addressGenericAttribute != null)
					{
						var companyAddress = _addressService.GetAddressById(Convert.ToInt32(addressGenericAttribute.Value));
						if (companyAddress != null)
						{
							customer.Addresses.Add(companyAddress);
							_customerService.UpdateCustomer(customer);
							var defaultAddress = customer.Addresses.FirstOrDefault();
							customer.BillingAddress = defaultAddress;
							_customerService.UpdateCustomer(customer);
						}
					}
					#endregion



					//newsletter
					if (_customerSettings.NewsletterEnabled)
					{
						//save newsletter value
						var newsletter = _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(model.Email, _storeContext.CurrentStore.Id);
						if (newsletter != null)
						{
							if (model.Newsletter)
							{
								newsletter.Active = true;
								_newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);
							}
						}
						else
						{
							if (model.Newsletter)
							{
								_newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
								{
									NewsLetterSubscriptionGuid = Guid.NewGuid(),
									Email = model.Email,
									Active = true,
									StoreId = _storeContext.CurrentStore.Id,
									CreatedOnUtc = DateTime.UtcNow
								});
							}
						}
					}

					//save customer attributes
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

					//login customer now
					if (isApproved)
						_authenticationService.SignIn(customer, true);

					//associated with external account (if possible)
					TryAssociateAccountWithExternalAccount(customer);

					//insert default address (if possible)
					//var defaultAddress = new Address
					//{
					//	FirstName = customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName),
					//	LastName = customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName),
					//	Email = customer.Email,
					//	Company = customer.GetAttribute<string>(SystemCustomerAttributeNames.Company),
					//	CountryId = customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId) > 0
					//		 ? (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.CountryId)
					//		 : null,
					//	StateProvinceId = customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId) > 0
					//		 ? (int?)customer.GetAttribute<int>(SystemCustomerAttributeNames.StateProvinceId)
					//		 : null,
					//	City = customer.GetAttribute<string>(SystemCustomerAttributeNames.City),
					//	Address1 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress),
					//	Address2 = customer.GetAttribute<string>(SystemCustomerAttributeNames.StreetAddress2),
					//	ZipPostalCode = customer.GetAttribute<string>(SystemCustomerAttributeNames.ZipPostalCode),
					//	PhoneNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
					//	FaxNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.Fax),
					//	CreatedOnUtc = customer.CreatedOnUtc
					//};
					//if (_addressService.IsAddressValid(defaultAddress))
					//{
					//	//some validation
					//	if (defaultAddress.CountryId == 0)
					//		defaultAddress.CountryId = null;
					//	if (defaultAddress.StateProvinceId == 0)
					//		defaultAddress.StateProvinceId = null;
					//	//set default address
					//	customer.Addresses.Add(defaultAddress);
					//	customer.BillingAddress = defaultAddress;
					//	customer.ShippingAddress = defaultAddress;
					//	_customerService.UpdateCustomer(customer);
					//}

					//notifications
					if (_customerSettings.NotifyNewCustomerRegistration)
					{
						_workflowMessageService.SendCustomerRegisteredNotificationMessage(customer, _localizationSettings.DefaultAdminLanguageId);
						_workflowMessageService.SendCustomerWelcomeMessage(customer, _localizationSettings.DefaultAdminLanguageId);
					}


					//raise event       
					_eventPublisher.Publish(new CustomerRegisteredEvent(customer));

					#region Push To WineShop

					//using (var client = new WebClient())
					//{
					//	var values = new NameValueCollection();
					//	values["Username"] = customer.Username;
					//	values["Email"] = customer.Email;
					//	values["FirstName"] = model.FirstName;
					//	values["LastName"] = model.LastName;
					//	values["Company"] = model.Company;
					//	values["DateOfBirth"] = model.ParseDateOfBirth().ToString();
					//	values["Gender"] = model.Gender;
					//	values["Password"] = model.Password;
					//	values["Active"] = customer.Active.ToString();

					//	var response = client.UploadValues(_postXSettings.CustomerPushUrl, values);

					//	var responseString = Encoding.Default.GetString(response);
					//}

					#endregion

					switch (_customerSettings.UserRegistrationType)
					{
						case UserRegistrationType.EmailValidation:
							{
								//email validation message
								_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.AccountActivationToken, Guid.NewGuid().ToString());
								_workflowMessageService.SendCustomerEmailValidationMessage(customer, _workContext.WorkingLanguage.Id);

								//result
								return RedirectToAction("RegisterResult",
									 new { resultId = (int)UserRegistrationType.EmailValidation });
							}
						case UserRegistrationType.AdminApproval:
							{
								return RedirectToAction("RegisterResult",
									 new { resultId = (int)UserRegistrationType.AdminApproval });
							}
						case UserRegistrationType.Standard:
							{
								//send customer welcome message
								//_workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

								var redirectUrl = Url.Action("RegisterResult", new { resultId = (int)UserRegistrationType.Standard });
								if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
									redirectUrl = _webHelper.ModifyQueryString(redirectUrl, "returnurl=" + HttpUtility.UrlEncode(returnUrl), null);
								return Redirect(redirectUrl);
							}
						default:
							{
								return RedirectToRoute("HomePage");
							}
					}
				}

				//errors
				foreach (var error in registrationResult.Errors)
					ModelState.AddModelError("", error);
			}

			//If we got this far, something failed, redisplay form
			model = _customerModelFactory.PrepareRegisterModel(model, true, customerAttributesXml);
			return View("~/Plugins/PostXCustom/Views/PublicCustomer/Register.cshtml", model);
		}

		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult RegisterResult(int resultId)
		{
			var model = _customerModelFactory.PrepareRegisterResultModel(resultId);
			return View("~/Plugins/PostXCustom/Views/PublicCustomer/RegisterResult.cshtml", model);
		}

		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		[HttpPost]
		public virtual ActionResult RegisterResult(string returnUrl)
		{
			if (String.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
				return RedirectToRoute("HomePage");

			return Redirect(returnUrl);
		}

		[HttpPost]
		[PublicAntiForgery]
		[ValidateInput(false)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult CheckUsernameAvailability(string username)
		{
			var usernameAvailable = false;
			var statusText = _localizationService.GetResource("Account.CheckUsernameAvailability.NotAvailable");

			if (_customerSettings.UsernamesEnabled && !String.IsNullOrWhiteSpace(username))
			{
				if (_workContext.CurrentCustomer != null &&
					 _workContext.CurrentCustomer.Username != null &&
					 _workContext.CurrentCustomer.Username.Equals(username, StringComparison.InvariantCultureIgnoreCase))
				{
					statusText = _localizationService.GetResource("Account.CheckUsernameAvailability.CurrentUsername");
				}
				else
				{
					var customer = _customerService.GetCustomerByUsername(username);
					if (customer == null)
					{
						statusText = _localizationService.GetResource("Account.CheckUsernameAvailability.Available");
						usernameAvailable = true;
					}
				}
			}

			return Json(new { Available = usernameAvailable, Text = statusText });
		}

		[NopHttpsRequirement(SslRequirement.Yes)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult AccountActivation(string token, string email)
		{
			var customer = _customerService.GetCustomerByEmail(email);
			if (customer == null)
				return RedirectToRoute("HomePage");

			var cToken = customer.GetAttribute<string>(SystemCustomerAttributeNames.AccountActivationToken);
			if (string.IsNullOrEmpty(cToken))
				return
					 View(new PublicAccountActivationModel
					 {
						 Result = _localizationService.GetResource("Account.AccountActivation.AlreadyActivated")
					 });

			if (!cToken.Equals(token, StringComparison.InvariantCultureIgnoreCase))
				return RedirectToRoute("HomePage");

			//activate user account
			customer.Active = true;
			_customerService.UpdateCustomer(customer);
			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.AccountActivationToken, "");
			//send welcome message
			_workflowMessageService.SendCustomerWelcomeMessage(customer, _workContext.WorkingLanguage.Id);

			var model = new PublicAccountActivationModel
			{
				Result = _localizationService.GetResource("Account.AccountActivation.Activated")
			};
			return View(model);
		}

		#endregion

		#region My account / Info

		[ChildActionOnly]
		public virtual ActionResult CustomerNavigation(int selectedTabId = 0)
		{
			var model = _customerModelFactory.PrepareCustomerNavigationModel(selectedTabId);
			return PartialView("~/Plugins/PostXCustom/Views/PublicCustomer/CustomerNavigation.cshtml", model);
		}


		[NopHttpsRequirement(SslRequirement.Yes)]
		public virtual ActionResult Welcome()
		{
			if (!_workContext.CurrentCustomer.IsRegistered())
				return new HttpUnauthorizedResult();

			var model = new PublicCustomerInfoModel();
			model = _customerModelFactory.PrepareCustomerInfoModel(model, _workContext.CurrentCustomer, false);

			var attributeList = _genericAttributeService.GetAttributesForEntity(_workContext.CurrentCustomer.Id, "Customer");
			if (attributeList.Any())
			{
				var pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
				if (pictureAttribute != null)
				{
					model.PictureId = Convert.ToInt32(pictureAttribute.Value);

					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.PictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PictureModel = pictureModel;
				}
				else
				{
					var companyRepresentative =
						_companyRepresentativeCustomerMappingService.GetByCustomerId(_workContext.CurrentCustomer.Id);
					if (companyRepresentative != null)
					{
						var customer = _customerService.GetCustomerById(companyRepresentative.CompanyRepresentativeId);
						var companyAttributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
						if (companyAttributeList.Any())
						{
							pictureAttribute = companyAttributeList.FirstOrDefault(x => x.Key == "PictureId");
							if (pictureAttribute != null)
							{
								model.PictureId = Convert.ToInt32(pictureAttribute.Value);

								var pictureSize = _mediaSettings.AvatarPictureSize;
								var picture = _pictureService.GetPictureById(model.PictureId);
								var pictureModel = new PublicPictureModel
								{
									FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
									ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
									Title = model.FirstName,
									AlternateText = model.FirstName
								};
								model.PictureModel = pictureModel;
							}
						}
					}
				}


				var customerPictureAttribute = attributeList.FirstOrDefault(x => x.Key == "CustomerPictureId");
				if (customerPictureAttribute != null)
				{
					model.CustomerPictureId = Convert.ToInt32(customerPictureAttribute.Value);

					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.CustomerPictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PersonalPictureModel = pictureModel;
				}


				var personalNumberAttribute = attributeList.FirstOrDefault(x => x.Key == "PersonalNumber");

				model.PersonalNumber = (personalNumberAttribute != null && personalNumberAttribute.Value != "0") ? personalNumberAttribute.Value : _workContext.CurrentCustomer.CompanyId.ToString("D3") + _workContext.CurrentCustomer.PersonalId.ToString("D5");
				model.CompanyId = _workContext.CurrentCustomer.CompanyId;

			}

			return View("~/Plugins/PostXCustom/Views/PublicCustomer/CustomerWelcome.cshtml", model);
		}


		[NopHttpsRequirement(SslRequirement.Yes)]
		public virtual ActionResult Info()
		{
			if (!_workContext.CurrentCustomer.IsRegistered())
				return new HttpUnauthorizedResult();

			var model = new PublicCustomerInfoModel();
			model = _customerModelFactory.PrepareCustomerInfoModel(model, _workContext.CurrentCustomer, false);

			var attributeList = _genericAttributeService.GetAttributesForEntity(_workContext.CurrentCustomer.Id, "Customer");
			if (attributeList.Any())
			{
				var pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
				if (pictureAttribute != null)
				{
					model.PictureId = Convert.ToInt32(pictureAttribute.Value);

					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.PictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PictureModel = pictureModel;
				}
				else
				{
					var companyRepresentative =
						_companyRepresentativeCustomerMappingService.GetByCustomerId(_workContext.CurrentCustomer.Id);
					if (companyRepresentative != null)
					{
						var customer = _customerService.GetCustomerById(companyRepresentative.CompanyRepresentativeId);
						var companyAttributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
						if (companyAttributeList.Any())
						{
							pictureAttribute = companyAttributeList.FirstOrDefault(x => x.Key == "PictureId");
							if (pictureAttribute != null)
							{
								model.PictureId = Convert.ToInt32(pictureAttribute.Value);

								var pictureSize = _mediaSettings.AvatarPictureSize;
								var picture = _pictureService.GetPictureById(model.PictureId);
								var pictureModel = new PublicPictureModel
								{
									FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
									ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
									Title = model.FirstName,
									AlternateText = model.FirstName
								};
								model.PictureModel = pictureModel;
							}
						}
					}
				}


				var customrPictureAttribute = attributeList.FirstOrDefault(x => x.Key == "CustomerPictureId");
				if (customrPictureAttribute != null)
				{
					model.CustomerPictureId = Convert.ToInt32(customrPictureAttribute.Value);

					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.CustomerPictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PersonalPictureModel = pictureModel;
				}



			}

			return View("~/Plugins/PostXCustom/Views/PublicCustomer/Info.cshtml", model);
		}

		[HttpPost]
		[PublicAntiForgery]
		[ValidateInput(false)]
		public virtual ActionResult Info(PublicCustomerInfoModel model, FormCollection form)
		{
			if (!_workContext.CurrentCustomer.IsRegistered())
				return new HttpUnauthorizedResult();

			var customer = _workContext.CurrentCustomer;

			_logger.InsertLog(LogLevel.Error, "Image for customer number" + _workContext.CurrentCustomer.Id,
						"Image number" + model.CustomerPictureId, _workContext.CurrentCustomer);

			//custom customer attributes
			var customerAttributesXml = ParseCustomCustomerAttributes(form);
			var customerAttributeWarnings = _customerAttributeParser.GetAttributeWarnings(customerAttributesXml);
			foreach (var error in customerAttributeWarnings)
			{
				ModelState.AddModelError("", error);
			}

			try
			{

				_logger.InsertLog(LogLevel.Error, "Image for customer number" + _workContext.CurrentCustomer.Id,
						"Image number" + model.CustomerPictureId, _workContext.CurrentCustomer);

				if (ModelState.IsValid)
				{
					//username 
					//if (_customerSettings.UsernamesEnabled && this._customerSettings.AllowUsersToChangeUsernames)
					//{
					//	if (
					//		 !customer.Username.Equals(model.Username.Trim(), StringComparison.InvariantCultureIgnoreCase))
					//	{
					//		//change username
					//		_customerRegistrationService.SetUsername(customer, model.Username.Trim());

					//		//re-authenticate
					//		//do not authenticate users in impersonation mode
					//		if (_workContext.OriginalCustomerIfImpersonated == null)
					//			_authenticationService.SignIn(customer, true);
					//	}
					//}
					//email
					if (!customer.Email.Equals(model.Email.Trim(), StringComparison.InvariantCultureIgnoreCase))
					{
						//change email
						var requireValidation = _customerSettings.UserRegistrationType ==
														UserRegistrationType.EmailValidation;
						_customerRegistrationService.SetEmail(customer, model.Email.Trim(), requireValidation);

						//do not authenticate users in impersonation mode
						if (_workContext.OriginalCustomerIfImpersonated == null)
						{
							//re-authenticate (if usernames are disabled)
							if (!_customerSettings.UsernamesEnabled && !requireValidation)
								_authenticationService.SignIn(customer, true);
						}
					}

					//properties
					//if (_dateTimeSettings.AllowCustomersToSetTimeZone)
					//{
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.TimeZoneId,
					//		 model.TimeZoneId);
					//}
					//VAT number
					if (_taxSettings.EuVatEnabled)
					{
						var prevVatNumber = customer.GetAttribute<string>(SystemCustomerAttributeNames.VatNumber);

						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.VatNumber,
							 model.VatNumber);
						if (prevVatNumber != model.VatNumber)
						{
							string vatName;
							string vatAddress;
							var vatNumberStatus = _taxService.GetVatNumberStatus(model.VatNumber, out vatName,
								 out vatAddress);
							_genericAttributeService.SaveAttribute(customer,
								 SystemCustomerAttributeNames.VatNumberStatusId, (int)vatNumberStatus);
							//send VAT number admin notification
							if (!String.IsNullOrEmpty(model.VatNumber) &&
								 _taxSettings.EuVatEmailAdminWhenNewVatSubmitted)
								_workflowMessageService.SendNewVatSubmittedStoreOwnerNotification(customer,
									 model.VatNumber, vatAddress, _localizationSettings.DefaultAdminLanguageId);
						}
					}




					//form fields
					if (_customerSettings.GenderEnabled)
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Gender,
							 model.Gender);
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.FirstName,
						 model.FirstName);
					_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.LastName,
						 model.LastName);
					if (_customerSettings.DateOfBirthEnabled)
					{
						DateTime? dateOfBirth = model.ParseDateOfBirth();
						_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.DateOfBirth,
							 dateOfBirth);
					}
					//if (_customerSettings.CompanyEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Company,
					//		 model.Company);


					if (model.CustomerPictureId > 0)
					{
						var newCustomerAttributeList = _genericAttributeService.GetAttributesForEntity(customer.Id, "Customer");
						InsertUpdateAttribute(customer, newCustomerAttributeList.ToList(), "CustomerPictureId",
						model.CustomerPictureId.ToString(CultureInfo.InvariantCulture));
					}
					//if (_customerSettings.StreetAddressEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress,
					//		 model.StreetAddress);
					//if (_customerSettings.StreetAddress2Enabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StreetAddress2,
					//		 model.StreetAddress2);
					//if (_customerSettings.ZipPostalCodeEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.ZipPostalCode,
					//		 model.ZipPostalCode);
					//if (_customerSettings.CityEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.City, model.City);
					//if (_customerSettings.CountryEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.CountryId,
					//		 model.CountryId);
					//if (_customerSettings.CountryEnabled && _customerSettings.StateProvinceEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.StateProvinceId,
					//		 model.StateProvinceId);
					//if (_customerSettings.PhoneEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Phone, model.Phone);
					//if (_customerSettings.FaxEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Fax, model.Fax);

					//newsletter
					//if (_customerSettings.NewsletterEnabled)
					//{
					//	//save newsletter value
					//	var newsletter =
					//		 _newsLetterSubscriptionService.GetNewsLetterSubscriptionByEmailAndStoreId(customer.Email,
					//			  _storeContext.CurrentStore.Id);
					//	if (newsletter != null)
					//	{
					//		if (model.Newsletter)
					//		{
					//			newsletter.Active = true;
					//			_newsLetterSubscriptionService.UpdateNewsLetterSubscription(newsletter);
					//		}
					//		else
					//			_newsLetterSubscriptionService.DeleteNewsLetterSubscription(newsletter);
					//	}
					//	else
					//	{
					//		if (model.Newsletter)
					//		{
					//			_newsLetterSubscriptionService.InsertNewsLetterSubscription(new NewsLetterSubscription
					//			{
					//				NewsLetterSubscriptionGuid = Guid.NewGuid(),
					//				Email = customer.Email,
					//				Active = true,
					//				StoreId = _storeContext.CurrentStore.Id,
					//				CreatedOnUtc = DateTime.UtcNow
					//			});
					//		}
					//	}
					//}

					//if (_forumSettings.ForumsEnabled && _forumSettings.SignaturesEnabled)
					//	_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.Signature,
					//		 model.Signature);

					//save customer attributes
					_genericAttributeService.SaveAttribute(_workContext.CurrentCustomer,
						 SystemCustomerAttributeNames.CustomCustomerAttributes, customerAttributesXml);

					//return RedirectToRoute("CustomerInfo");
					return RedirectToAction("Welcome");
				}
			}
			catch (Exception exc)
			{
				ModelState.AddModelError("", exc.Message);
			}


			//If we got this far, something failed, redisplay form
			model = _customerModelFactory.PrepareCustomerInfoModel(model, customer, true, customerAttributesXml);

			var attributeList = _genericAttributeService.GetAttributesForEntity(_workContext.CurrentCustomer.Id, "Customer");
			if (attributeList.Any())
			{
				var pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
				if (pictureAttribute != null)
				{
					model.PictureId = Convert.ToInt32(pictureAttribute.Value);

					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.PictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PictureModel = pictureModel;
				}
				else
				{
					var companyRepresentative =
						_companyRepresentativeCustomerMappingService.GetByCustomerId(_workContext.CurrentCustomer.Id);
					if (companyRepresentative != null)
					{
						var companyCustomer = _customerService.GetCustomerById(companyRepresentative.CompanyRepresentativeId);
						attributeList = _genericAttributeService.GetAttributesForEntity(companyCustomer.Id, "Customer");
						if (attributeList.Any())
						{
							pictureAttribute = attributeList.FirstOrDefault(x => x.Key == "PictureId");
							if (pictureAttribute != null)
							{
								model.PictureId = Convert.ToInt32(pictureAttribute.Value);

								var pictureSize = _mediaSettings.AvatarPictureSize;
								var picture = _pictureService.GetPictureById(model.PictureId);
								var pictureModel = new PublicPictureModel
								{
									FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
									ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
									Title = model.FirstName,
									AlternateText = model.FirstName
								};
								model.PictureModel = pictureModel;
							}
						}
					}
				}


				var customrPictureAttribute = attributeList.FirstOrDefault(x => x.Key == "CustomerPictureId");
				if (customrPictureAttribute != null)
				{
					model.CustomerPictureId = Convert.ToInt32(customrPictureAttribute.Value);

					var pictureSize = _mediaSettings.AvatarPictureSize;
					var picture = _pictureService.GetPictureById(model.CustomerPictureId);
					var pictureModel = new PublicPictureModel
					{
						FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
						ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
						Title = model.FirstName,
						AlternateText = model.FirstName
					};
					model.PersonalPictureModel = pictureModel;
				}
			}

			return View("~/Plugins/PostXCustom/Views/PublicCustomer/Info.cshtml", model);
		}

		public void InsertUpdateAttribute(Customer customer, List<GenericAttribute> genericAttributes, string attributeName, string attributeValue)
		{
			var genericAttribute = genericAttributes.FirstOrDefault(x => x.Key == attributeName);
			if (genericAttribute == null)
			{
				genericAttribute = new GenericAttribute
				{
					EntityId = customer.Id,
					Key = attributeName,
					KeyGroup = "Customer",
					Value = attributeValue,
					StoreId = 0
				};
				_genericAttributeService.InsertAttribute(genericAttribute);
			}
			else
			{
				genericAttribute.Value = attributeValue;
				_genericAttributeService.UpdateAttribute(genericAttribute);
			}
		}


		[HttpPost]
		[PublicAntiForgery]
		public virtual ActionResult RemoveExternalAssociation(int id)
		{
			if (!_workContext.CurrentCustomer.IsRegistered())
				return new HttpUnauthorizedResult();

			//ensure it's our record
			var ear = _openAuthenticationService.GetExternalIdentifiersFor(_workContext.CurrentCustomer)
				 .FirstOrDefault(x => x.Id == id);

			if (ear == null)
			{
				return Json(new
				{
					redirect = Url.Action("Info"),
				});
			}

			_openAuthenticationService.DeleteExternalAuthenticationRecord(ear);

			return Json(new
			{
				redirect = Url.Action("Info"),
			});
		}

		[NopHttpsRequirement(SslRequirement.Yes)]
		//available even when navigation is not allowed
		[PublicStoreAllowNavigation(true)]
		public virtual ActionResult EmailRevalidation(string token, string email)
		{
			var customer = _customerService.GetCustomerByEmail(email);
			if (customer == null)
				return RedirectToRoute("HomePage");

			var cToken = customer.GetAttribute<string>(SystemCustomerAttributeNames.EmailRevalidationToken);
			if (string.IsNullOrEmpty(cToken))
				return View("~/Plugins/PostXCustom/Views/PublicCustomer/EmailRevalidation.cshtml", new PublicEmailRevalidationModel
				{
					Result = _localizationService.GetResource("Account.EmailRevalidation.AlreadyChanged")
				});

			if (!cToken.Equals(token, StringComparison.InvariantCultureIgnoreCase))
				return RedirectToRoute("HomePage");

			if (string.IsNullOrEmpty(customer.EmailToRevalidate))
				return RedirectToRoute("HomePage");

			if (_customerSettings.UserRegistrationType != UserRegistrationType.EmailValidation)
				return RedirectToRoute("HomePage");

			//change email
			try
			{
				_customerRegistrationService.SetEmail(customer, customer.EmailToRevalidate, false);
			}
			catch (Exception exc)
			{
				return View("~/Plugins/PostXCustom/Views/PublicCustomer/EmailRevalidation.cshtml", new PublicEmailRevalidationModel
				{
					Result = _localizationService.GetResource(exc.Message)
				});
			}
			customer.EmailToRevalidate = null;
			_customerService.UpdateCustomer(customer);
			_genericAttributeService.SaveAttribute(customer, SystemCustomerAttributeNames.EmailRevalidationToken, "");

			//re-authenticate (if usernames are disabled)
			if (!_customerSettings.UsernamesEnabled)
			{
				_authenticationService.SignIn(customer, true);
			}

			var model = new PublicEmailRevalidationModel
			{
				Result = _localizationService.GetResource("Account.EmailRevalidation.Changed")
			};
			return View("~/Plugins/PostXCustom/Views/PublicCustomer/EmailRevalidation.cshtml", model);
		}

		#endregion

			  
		public ActionResult GenerateBacode(string data, string filename)
		{
			Linear barcode = new Linear
			{
				Type = BarcodeType.EAN13,
				Data = data,
				Resolution = 150
			};
			barcode.drawBarcode(filename);
			var image = barcode.drawBarcode();
			ImageConverter converter = new ImageConverter();
			var byteArraye = (byte[])converter.ConvertTo(image, typeof(byte[]));
			return File(byteArraye, "image/jpeg");
		}

		[HttpPost]
		//do not validate request token (XSRF)
		[PublicAntiForgery(false)]
		public virtual ActionResult PublicAsyncUpload()
		{
			//if (!_permissionService.Authorize(StandardPermissionProvider.UploadPictures))
			//    return Json(new { success = false, error = "You do not have required permissions" }, "text/plain");

			//we process it distinct ways based on a browser
			//find more info here http://stackoverflow.com/questions/4884920/mvc3-valums-ajax-file-upload
			Stream stream = null;
			var fileName = "";
			var contentType = "";
			if (String.IsNullOrEmpty(Request["qqfile"]))
			{
				// IE
				HttpPostedFileBase httpPostedFile = Request.Files[0];
				if (httpPostedFile == null)
					throw new ArgumentException("No file uploaded");
				stream = httpPostedFile.InputStream;
				fileName = Path.GetFileName(httpPostedFile.FileName);
				contentType = httpPostedFile.ContentType;
			}
			else
			{
				//Webkit, Mozilla
				stream = Request.InputStream;
				fileName = Request["qqfile"];
			}

			var fileBinary = new byte[stream.Length];
			stream.Read(fileBinary, 0, fileBinary.Length);

			var fileExtension = Path.GetExtension(fileName);
			if (!String.IsNullOrEmpty(fileExtension))
				fileExtension = fileExtension.ToLowerInvariant();
			//contentType is not always available 
			//that's why we manually update it here
			//http://www.sfsu.edu/training/mimetype.htm
			if (String.IsNullOrEmpty(contentType))
			{
				switch (fileExtension)
				{
					case ".bmp":
						contentType = MimeTypes.ImageBmp;
						break;
					case ".gif":
						contentType = MimeTypes.ImageGif;
						break;
					case ".jpeg":
					case ".jpg":
					case ".jpe":
					case ".jfif":
					case ".pjpeg":
					case ".pjp":
						contentType = MimeTypes.ImageJpeg;
						break;
					case ".png":
						contentType = MimeTypes.ImagePng;
						break;
					case ".tiff":
					case ".tif":
						contentType = MimeTypes.ImageTiff;
						break;
					default:
						break;
				}
			}

			var picture = _pictureService.InsertPicture(fileBinary, contentType, null);
			//when returning JSON the mime-type must be set to text/plain
			//otherwise some browsers will pop-up a "Save As" dialog.
			return Json(new
			{
				success = true,
				pictureId = picture.Id,
				imageUrl = _pictureService.GetPictureUrl(picture, 100)
			},
				 MimeTypes.TextPlain);
		}





	}
}
