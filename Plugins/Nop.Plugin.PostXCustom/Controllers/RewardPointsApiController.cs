﻿using System;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Plugin.PostXCustom.Models.ApiModels;
using Nop.Services.Customers;
using Nop.Services.Orders;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class RewardPointsApiController : BasePluginController
	{
		#region Fields	
		private readonly IRewardPointService _rewardPointService;
		private readonly ICustomerService _customerService;
		private readonly IStoreContext _storeContext;
		#endregion

		#region Ctor	  
		public RewardPointsApiController(IRewardPointService rewardPointService, ICustomerService customerService, IStoreContext storeContext)
		{
			_rewardPointService = rewardPointService;
			_customerService = customerService;
			_storeContext = storeContext;
		}		  
		#endregion

		#region Methods

		public object UpdateRewardPoints(RewardPointsModel model)
		{
			var customer = _customerService.GetCustomerByEmail(model.CustomerEmail);
			if (customer == null)
				return Json(new { success = false, message = "Customer not found" }, JsonRequestBehavior.AllowGet);

			var rewardPoints = new RewardPointsHistory
			{
				CustomerId = customer.Id,
				Message = model.Message,
				Points = model.Points,
				PointsBalance = model.PointsBalance,
				UsedAmount = model.UsedAmount,
				StoreId = _storeContext.CurrentStore.Id,
				CreatedOnUtc = DateTime.Now
			};			
			_rewardPointService.AddRewardPointsHistoryEntry(customer, model.Points, _storeContext.CurrentStore.Id, model.Message, null, model.UsedAmount, null,"Wine");	 
			return Json(new { success = true, message = "Reward Points Updated." }, JsonRequestBehavior.AllowGet);
		}

		#endregion
	}
}
