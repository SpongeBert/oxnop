﻿using System.Net;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Plugin.PostXCustom.Services;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using System.IO;
using Nop.Core.Domain.Logging;
using Nop.Services.CustomService;
using Newtonsoft.Json.Linq;
using Nop.Services.Orders;
using Nop.Core.Domain.Orders;
using System;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using Newtonsoft.Json;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class AdminCustomController : BasePluginController
	{

		#region Fields
		private readonly ICustomerService _customerService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly IDateTimeHelper _dateTimeHelper;
		private readonly ILocalizationService _localizationService;
		private readonly DateTimeSettings _dateTimeSettings;
		private readonly CustomerSettings _customerSettings;
		private readonly IWorkContext _workContext;
		private readonly IStoreContext _storeContext;
		private readonly IStoreService _storeService;
		private readonly ICacheManager _cacheManager;
		private readonly ILogger _logger;
		private readonly PostXSettings _postXSettings;
		private readonly ISettingService _settingService;
		private readonly LocalizationSettings _localizationSettings;
		private readonly ICompanyRepresentativeCustomerMappingService _companyRepresentativeCustomerMappingService;
		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICustomCodeService _customCodeService;
		private readonly IOrderService _orderService;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;
		private readonly OrderSettings _orderSettings;
		#endregion

		#region Constructors

		public AdminCustomController(ICustomerService customerService, IGenericAttributeService genericAttributeService,
			 IDateTimeHelper dateTimeHelper, ILocalizationService localizationService, DateTimeSettings dateTimeSettings,
			 CustomerSettings customerSettings, IWorkContext workContext, IStoreContext storeContext,
			 IStoreService storeService, ICacheManager cacheManager, ILogger logger, ISettingService settingService, LocalizationSettings localizationSettings,
			ICompanyRepresentativeCustomerMappingService companyRepresentativeCustomerMappingService,
			ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICustomCodeService customCodeService, IOrderService orderService,
			ICmsNopOrderMappingService cmsNopOrderMappingService, OrderSettings orderSettings)
		{
			_customerService = customerService;
			_genericAttributeService = genericAttributeService;
			_dateTimeHelper = dateTimeHelper;
			_localizationService = localizationService;
			_dateTimeSettings = dateTimeSettings;
			_customerSettings = customerSettings;
			_workContext = workContext;
			_storeContext = storeContext;
			_storeService = storeService;
			_cacheManager = cacheManager;
			_logger = logger;
			_settingService = settingService;
			var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
			_postXSettings = _settingService.LoadSetting<PostXSettings>(storeScope);
			_localizationSettings = localizationSettings;
			_companyRepresentativeCustomerMappingService = companyRepresentativeCustomerMappingService;
			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_customCodeService = customCodeService;
			_orderService = orderService;
			_cmsNopOrderMappingService = cmsNopOrderMappingService;
			_orderSettings = orderSettings;
		}

		#endregion


		public ActionResult CheckOrderLSStatus(int orderId)
		{
			var cmsOrder = _cmsNopOrderMappingService.GetByNopOrderId(orderId);
			if (cmsOrder == null)
			{
				return Json(new { success = true, message = "STATUS NOT FOUND", orderStatus = false }, JsonRequestBehavior.AllowGet);
			}

			var nopOrder = _orderService.GetOrderById(orderId);
			if (nopOrder == null)
			{
				return Json(new { success = true, message = "STATUS NOT FOUND", orderStatus = true }, JsonRequestBehavior.AllowGet);
			}

			var cmsCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(nopOrder.CustomerId);
			if (cmsCustomer == null)
			{
				return Json(new { success = true, message = "STATUS NOT FOUND", orderStatus = true }, JsonRequestBehavior.AllowGet);
			}

			string html;
			string url = @"https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + cmsCustomer.CmsCustomerId + "/order/" + cmsOrder.CmsOrderId;


			var token = GetToken();
			if (string.IsNullOrEmpty(token))
			{
				_logger.InsertLog(LogLevel.Error, "Error Check LS Order Status :: failed to generate security token.");
				return Json(new { success = true, message = "STATUS NOT FOUND", orderStatus = true }, JsonRequestBehavior.AllowGet);
			}

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.Headers.Add("X-Auth-Token", token);
			request.AutomaticDecompression = DecompressionMethods.GZip;

			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
			using (Stream stream = response.GetResponseStream())
			using (StreamReader reader = new StreamReader(stream))
			{
				html = reader.ReadToEnd();
				var jsonList = JsonConvert.DeserializeObject<CmsOrderApiModel>(html);

				return Json(new { success = true, message = jsonList.status, orderStatus = true }, JsonRequestBehavior.AllowGet);
			}
		}

		public string GetToken()
		{
			try
			{
				string token;
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

	}
}