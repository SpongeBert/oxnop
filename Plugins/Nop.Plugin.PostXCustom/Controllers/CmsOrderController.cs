﻿using System.IO;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class CmsOrderController : BasePluginController
	{

		public ActionResult Test()
		{
			string html;
			var token = GetToken();
			var url = @"https://euc1.posios.com/PosServer/rest/onlineordering/order";
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.Headers.Add("X-Auth-Token", token);
			request.AutomaticDecompression = DecompressionMethods.GZip;

			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
			using (Stream stream = response.GetResponseStream())
			using (StreamReader reader = new StreamReader(stream))
			{
				html = reader.ReadToEnd();
				JArray jsonArray = JArray.Parse(html);
				foreach (var item in jsonArray)
				{
				}
			}
			return Json(new {data=html}, JsonRequestBehavior.AllowGet);
		}

		public string GetToken()
		{
			string token = string.Empty;
            string url = @"https://euc1.posios.com/PosServer/rest/token";
            string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";
            //string postData = "{\"companyId\": 32479,\"deviceId\": \"prod\",\"password\": \"z7y65tR\",\"username\": \"euc1@webshopcompany.be\"}";

            var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			token = json.GetValue("token").Value<string>();
			return token;
		}

	}
}
