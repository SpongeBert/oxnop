﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping.Date;
using Nop.Services.Tax;	
using Nop.Web.Framework.Security.Captcha;	
using Nop.Plugin.PostXCustom.Models.Public.CustomCode;	
using System.Text;
using System.Security.Cryptography;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework.Controllers;
using Nop.Plugin.PostXCustom.Factories.Public;
using Nop.Core.Data;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Plugin.PostXCustom.Services;

namespace Nop.Plugin.PostXCustom.Controllers
{
	public partial class CustomHomeController : BasePluginController
	{
		#region Fields												 
		private readonly ICategoryService _categoryService;
		private readonly IProductService _productService;
		private readonly IProductModelFactory _productModelFactory;		

		private readonly IWorkContext _workContext;
		private readonly IStoreContext _storeContext;
		private readonly IShoppingCartService _shoppingCartService;

		private readonly ILocalizationService _localizationService;
		private readonly IProductAttributeService _productAttributeService;
		private readonly IProductAttributeParser _productAttributeParser;
		private readonly ITaxService _taxService;
		private readonly ICurrencyService _currencyService;
		private readonly IPriceCalculationService _priceCalculationService;
		private readonly IPriceFormatter _priceFormatter;
		private readonly ICheckoutAttributeParser _checkoutAttributeParser;
		private readonly IDiscountService _discountService;
		private readonly ICustomerService _customerService;
		private readonly IGiftCardService _giftCardService;
		private readonly IDateRangeService _dateRangeService;
		private readonly ICheckoutAttributeService _checkoutAttributeService;
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly IPermissionService _permissionService;
		private readonly IDownloadService _downloadService;
		private readonly ICacheManager _cacheManager;
		private readonly IWebHelper _webHelper;
		private readonly ICustomerActivityService _customerActivityService;
		private readonly IGenericAttributeService _genericAttributeService;

		private readonly MediaSettings _mediaSettings;
		private readonly ShoppingCartSettings _shoppingCartSettings;
		private readonly OrderSettings _orderSettings;
		private readonly CaptchaSettings _captchaSettings;
		private readonly CustomerSettings _customerSettings;
		private readonly IRepository<CmsProductNopProductMapping> _cmsProductNopProductMappingRepository;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly IOrderService _orderService;
		#endregion

		#region Ctor
		public CustomHomeController(ICategoryService categoryService, IProductService productService, IProductModelFactory productModelFactory,  			
				IStoreContext storeContext,
				IWorkContext workContext,
				IShoppingCartService shoppingCartService,
				IPictureService pictureService,
				ILocalizationService localizationService,
				IProductAttributeService productAttributeService,
				IProductAttributeParser productAttributeParser,
				ITaxService taxService, ICurrencyService currencyService,
				IPriceCalculationService priceCalculationService,
				IPriceFormatter priceFormatter,
				ICheckoutAttributeParser checkoutAttributeParser,
				IDiscountService discountService,
				ICustomerService customerService,
				IGiftCardService giftCardService,
				IDateRangeService dateRangeService,
				ICheckoutAttributeService checkoutAttributeService,
				IWorkflowMessageService workflowMessageService,
				IPermissionService permissionService,
				IDownloadService downloadService,
				ICacheManager cacheManager,
				IWebHelper webHelper,
				ICustomerActivityService customerActivityService,
				IGenericAttributeService genericAttributeService,
				MediaSettings mediaSettings,
				ShoppingCartSettings shoppingCartSettings,
				OrderSettings orderSettings,
				CaptchaSettings captchaSettings,CustomerSettings customerSettings,
			IRepository<CmsProductNopProductMapping> cmsProductNopProductMappingRepository, ICmsNopProductMappingService cmsNopProductMappingService,
			IOrderService orderService)
		{
			_categoryService = categoryService;
			_productService = productService;
			_productModelFactory = productModelFactory;	
		
			this._productService = productService;
			this._workContext = workContext;
			this._storeContext = storeContext;
			this._shoppingCartService = shoppingCartService;
			this._localizationService = localizationService;
			this._productAttributeService = productAttributeService;
			this._productAttributeParser = productAttributeParser;
			this._taxService = taxService;
			this._currencyService = currencyService;
			this._priceCalculationService = priceCalculationService;
			this._priceFormatter = priceFormatter;
			this._checkoutAttributeParser = checkoutAttributeParser;
			this._discountService = discountService;
			this._customerService = customerService;
			this._giftCardService = giftCardService;
			this._dateRangeService = dateRangeService;
			this._checkoutAttributeService = checkoutAttributeService;
			this._workflowMessageService = workflowMessageService;
			this._permissionService = permissionService;
			this._downloadService = downloadService;
			this._cacheManager = cacheManager;
			this._webHelper = webHelper;
			this._customerActivityService = customerActivityService;
			_genericAttributeService = genericAttributeService;

			this._mediaSettings = mediaSettings;
			this._shoppingCartSettings = shoppingCartSettings;
			this._orderSettings = orderSettings;
			this._captchaSettings = captchaSettings;
			this._customerSettings = customerSettings;
			_cmsProductNopProductMappingRepository = cmsProductNopProductMappingRepository;
			_cmsNopProductMappingService = cmsNopProductMappingService;

			_orderService = orderService;
		}
		#endregion


		#region Utility
		public string GetPublicName(string categoryName)
		{									 
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == categoryName.ToLower());

			if (category == null)
			{
				return categoryName;
			};

			var publicDisplayName = category.GetLocalized(x => x.PublicDisplayName, _workContext.WorkingLanguage.Id);

			if (string.IsNullOrEmpty(publicDisplayName))
			{
				return categoryName;
			}

			return publicDisplayName;
		}
		#endregion



		public ActionResult Index()
		{

			var model = new List<ProductApiModel>();
			string html = string.Empty;
			string url = @"https://euc1.posios.com/PosServer/rest/inventory/product";

			var token = GetToken();

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.Headers.Add("X-Auth-Token", token);
			request.AutomaticDecompression = DecompressionMethods.GZip;

			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
			using (Stream stream = response.GetResponseStream())
			using (StreamReader reader = new StreamReader(stream))
			{
				html = reader.ReadToEnd();

				JArray jsonArray = JArray.Parse(html);

				foreach (var item in jsonArray)
				{
					var jsonText = JsonConvert.DeserializeObject<ProductApiModel>(item.ToString());

					model.Add(jsonText);
				}
			}



			return View(model);
		}

		public string GetToken()
		{
			string token = string.Empty;
			string url = @"https://euc1.posios.com/PosServer/rest/token";
			string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();
			var json = JObject.Parse(responseData);

			token = json.GetValue("token").Value<string>();
			return token;
		}


		public ActionResult GetCategoryPublicName(string categoryName)
		{

			
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == categoryName.ToLower());

			if (category == null)
			{
				ViewBag.Name = categoryName;
				return View("~/Themes/OxTheme/Views/CustomCode/GetCategoryPublicName.cshtml", categoryName);
				//return View("GetCategoryPublicName");
			};

			var publicDisplayName = category.GetLocalized(x => x.PublicDisplayName, _workContext.WorkingLanguage.Id);

			if (string.IsNullOrEmpty(publicDisplayName))
			{
				ViewBag.Name = categoryName;
				return View("~/Themes/OxTheme/Views/CustomCode/GetCategoryPublicName.cshtml");
				//return View("GetCategoryPublicName");
			}

			ViewBag.Name = publicDisplayName;
			return View("~/Themes/OxTheme/Views/CustomCode/GetCategoryPublicName.cshtml");
			//return View("GetCategoryPublicName");
		}

		#region HomePage Product Sliders


		public string CheckCompanyMappingAvailability(string actualCompanyId)
		{
			var checkMappings = _cmsNopProductMappingService.GetByCompanyId(actualCompanyId);
			if (checkMappings.Any())
			{
				return actualCompanyId;
			}
			else
			{
				return "001";
			}

		}


		public ActionResult GetDayOfferProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "gerechten");				

			//Get company details 
			var companyId = CheckCompanyMappingAvailability(_workContext.CurrentCustomer.Username != null ? _workContext.CurrentCustomer.Username.Substring(0, 3) : "001");


			int categoryId = 0;
			var model = new HomePageSubCategoryModel();			
			if (category != null)
			{
				categoryId = category.Id;
				var childCategories = _categoryService.GetAllCategoriesByParentCategoryId(category.Id);
				if (childCategories.Any())
				{
					
					foreach (var childCategory in childCategories)
					{
						var flag = 0; 
						var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { childCategory.Id });
						foreach (var product in categoryProducts)
						{
							var productCompanyMapping = (from cmsProduct in _cmsProductNopProductMappingRepository.Table
																  where cmsProduct.Company == companyId && cmsProduct.NopProductId == product.Id
																  select cmsProduct).FirstOrDefault();
							
							if (productCompanyMapping != null)
							{
								flag = 1;
								break;
							}
						}

						if (flag == 1)
						{
							var subCategoryRecord = new SubCategoryDetail
							{
								Id = childCategory.Id,
								Name = GetPublicName(childCategory.Name),
								DisplayOrder = childCategory.DisplayOrder
							};
							model.SubCategoryList.Add(subCategoryRecord);
						}
					}
					model.ParentCategoryId = categoryId;
					return View("~/Themes/OxTheme/Views/CustomCode/SubCategoryView.cshtml", model); 
				}
				else
				{
				var dayProducts = _categoryService.GetProductCategoriesByCategoryId(categoryId);

					List<Product> prodList = new List<Product>();
					foreach (var prod in dayProducts)
					{
						prodList.Add(prod.Product);
					}					
					var productModel = _productModelFactory.PrepareProductOverviewModels(prodList.ToList());
					return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts.cshtml", productModel);
					
				}
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts.cshtml", null); 	
		}

		public ActionResult GetCategoryProducts(int categoryId)
		{
			var category = _categoryService.GetCategoryById(categoryId);
			if (category != null)
			{
				categoryId = category.Id;
				var categoryList = new List<int> { categoryId };		

		 	  var dayProducts = 	_categoryService.GetProductCategoriesByCategoryId(categoryId);

				List<Product> prodList = new List<Product>();
				foreach(var prod in dayProducts)
				{ 				
					prodList.Add(prod.Product);
				}


				var model = new HomePageProducts();
				model.CategoryId = categoryId;

				var products = _productModelFactory.PrepareProductOverviewModels(prodList.ToList());
				model.Products = products.ToList();

				return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProductsGeneral.cshtml", model);					
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProductsGeneral.cshtml", null);			
		}
		public ActionResult GetLunchProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "broodjes");

			//Get company details 
			//var companyId = _workContext.CurrentCustomer.Username != null ? _workContext.CurrentCustomer.Username.Substring(0, 3) : "001";
			var companyId = CheckCompanyMappingAvailability(_workContext.CurrentCustomer.Username != null ? _workContext.CurrentCustomer.Username.Substring(0, 3) : "001");

			int categoryId = 0;
			var model = new HomePageSubCategoryModel();
			if (category != null)
			{
				categoryId = category.Id;
				var childCategories = _categoryService.GetAllCategoriesByParentCategoryId(category.Id);
				if (childCategories.Any())
				{
					foreach (var childCategory in childCategories)
					{
						var flag = 0;
						var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { childCategory.Id });
						foreach (var product in categoryProducts)
						{
							var productCompanyMapping = (from cmsProduct in _cmsProductNopProductMappingRepository.Table
																  where cmsProduct.Company == companyId && cmsProduct.NopProductId == product.Id
																  select cmsProduct).FirstOrDefault();

							if (productCompanyMapping != null)
							{
								flag = 1;
								break;
							}
						}

						if (flag == 1)
						{
							var subCategoryRecord = new SubCategoryDetail
							{
								Id = childCategory.Id,
								Name = GetPublicName(childCategory.Name),
								DisplayOrder = childCategory.DisplayOrder
							};
							model.SubCategoryList.Add(subCategoryRecord);
						}
					}
					model.ParentCategoryId = categoryId;
					return View("~/Themes/OxTheme/Views/CustomCode/SubCategoryView2.cshtml", model);
				}
				else
				{
					var lunchProducts = _categoryService.GetProductCategoriesByCategoryId(categoryId);
					List<Product> prodList = new List<Product>();
					foreach (var prod in lunchProducts)
					{
						prodList.Add(prod.Product);
					}
					var productModel = _productModelFactory.PrepareProductOverviewModels(prodList.ToList());
					return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts2.cshtml", productModel);
				}
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts2.cshtml", null);
		}	 

		public ActionResult GetDrinkProducts()
		{	
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "dranken");

			//Get company details 
			//var companyId = _workContext.CurrentCustomer.Username != null ? _workContext.CurrentCustomer.Username.Substring(0, 3) : "001";
			var companyId = CheckCompanyMappingAvailability(_workContext.CurrentCustomer.Username != null ? _workContext.CurrentCustomer.Username.Substring(0, 3) : "001");


			int categoryId = 0;
			var model = new HomePageSubCategoryModel();
			if (category != null)
			{
				categoryId = category.Id;
				var childCategories = _categoryService.GetAllCategoriesByParentCategoryId(category.Id);
				if (childCategories.Any())
				{
					foreach (var childCategory in childCategories)
					{
						var flag = 0;
						var categoryProducts = _productService.SearchProducts(categoryIds: new List<int> { childCategory.Id });

						foreach (var product in categoryProducts)
						{
							var productCompanyMapping = (from cmsProduct in _cmsProductNopProductMappingRepository.Table
																  where cmsProduct.Company == companyId && cmsProduct.NopProductId == product.Id
																  select cmsProduct).FirstOrDefault();

							if (productCompanyMapping != null)
							{
								flag = 1;
								break;
							}
						}
						if (flag == 1)
						{
							var subCategoryRecord = new SubCategoryDetail
							{
								Id = childCategory.Id,
								Name = GetPublicName(childCategory.Name),
								DisplayOrder = childCategory.DisplayOrder
							};
							model.SubCategoryList.Add(subCategoryRecord);
						}
					}
					model.ParentCategoryId = categoryId;
					return View("~/Themes/OxTheme/Views/CustomCode/SubCategoryView3.cshtml", model);	 					
				}
				else
				{
				  var drinkProducts = _categoryService.GetProductCategoriesByCategoryId(categoryId);

					List<Product> prodList = new List<Product>();
					foreach (var prod in drinkProducts)
					{
						prodList.Add(prod.Product);
					}						  
					var productModel = _productModelFactory.PrepareProductOverviewModels(prodList.ToList());
					return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts3.cshtml", productModel);	  					
				}
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts3.cshtml", null);	 		
		}


		public ActionResult GetBurgerProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "burger");
			int categoryId = 0;
			if (category != null)
			{
				categoryId = category.Id;
				var categoryList = new List<int> { categoryId };
				var dayProducts = _productService.SearchProducts(categoryIds: categoryList);
				var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder));
				return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts4.cshtml", model);	  				
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts4.cshtml", null); 			
		}

		public ActionResult GetCroqueMonsieurProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "croque monsieur");

			int categoryId = 0;
			if (category != null)
			{
				categoryId = category.Id;
				var categoryList = new List<int> { categoryId };
				var dayProducts = _productService.SearchProducts(categoryIds: categoryList);
				var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder));
				return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts5.cshtml", model);	  				
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts5.cshtml", null);	 		
		}

		public ActionResult GetPizzaProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "pizza");

			int categoryId = 0;
			if (category != null)
			{
				categoryId = category.Id;
				var categoryList = new List<int> { categoryId };
				var dayProducts = _productService.SearchProducts(categoryIds: categoryList);
				var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder));
				return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts6.cshtml", model);	 			
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts6.cshtml",null); 
		}

		public ActionResult GetPastaProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "pasta");

			int categoryId = 0;
			if (category != null)
			{
				categoryId = category.Id;
				var categoryList = new List<int> { categoryId };
				var dayProducts = _productService.SearchProducts(categoryIds: categoryList);
				var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder));
				return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts7.cshtml", model);		
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts7.cshtml", null);	 
		}

		public ActionResult GetSaladeProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "salade");

			int categoryId = 0;
			if (category != null)
			{
				categoryId = category.Id;
				var categoryList = new List<int> { categoryId };
				var dayProducts = _productService.SearchProducts(categoryIds: categoryList);
				var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder));
				return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts8.cshtml", model);	  				
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts8.cshtml", null);			
		}

		public ActionResult GetWrapsProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "wraps");

			int categoryId = 0;
			if (category != null)
			{
				categoryId = category.Id;
				var categoryList = new List<int> { categoryId };
				var dayProducts = _productService.SearchProducts(categoryIds: categoryList);
				var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder));
				return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts9.cshtml", model);				
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts9.cshtml", null);	 		
		}

		public ActionResult GetHotDogProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "hot dog");

			int categoryId = 0;
			if (category != null)
			{
				categoryId = category.Id;
				var categoryList = new List<int> { categoryId };
				var dayProducts = _productService.SearchProducts(categoryIds: categoryList);
				var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder));
				return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts10.cshtml", model);	 			
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts10.cshtml", null);	  		
		}

		public ActionResult GetSoepProducts()
		{
			var category = _categoryService.GetAllCategories().FirstOrDefault(x => x.Name.ToLower() == "soep");

			int categoryId = 0;
			if (category != null)
			{
				categoryId = category.Id;
				var categoryList = new List<int> { categoryId };
				var dayProducts = _productService.SearchProducts(categoryIds: categoryList);
				var model = _productModelFactory.PrepareProductOverviewModels(dayProducts.ToList().OrderBy(x => x.DisplayOrder));
				return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts11.cshtml", model);  				
			}
			return View("~/Themes/OxTheme/Views/CustomCode/HomePageFeaturedProducts11.cshtml", null);			
		}

		#endregion




		#region Check Customer Order Count for Day

		public virtual ActionResult CheckCustomerOrderLimit()
		{							  
			var customerOrders = _orderService.SearchOrders(customerId: _workContext.CurrentCustomer.Id,
				createdFromUtc: DateTime.UtcNow, createdToUtc: DateTime.UtcNow);

			if (customerOrders.Any())
			{
				return Json(new
				{
					success = true,
					AllowOrder =false
				});
			}

			return Json(new
			{
				success = true,
				AllowOrder = true
			});
		}

		#endregion


		#region TEST1
		public ActionResult TestSIPS()
		{
			var transactionReference = Guid.NewGuid().ToString().Replace("-", "");
			try
			{
				string token = string.Empty;
				string url = @"https://office-server.sips-atos.com/rs-services/v2/checkout/paymentProviderInitialize";
				string postData = "{\"amount\":\"2\", \"captureMode\":\"AUTHOR_CAPTURE\", \"currencyCode\":\"978\", \"interfaceVersion\":\"IR_WS_2.11\", \"keyVersion\":\"1\", \"merchantId\":\"225005036700001\", ";
				postData = postData + "\"merchantReturnUrl\":\"http://www.oxcfe.be\", \"orderChannel\":\"INTERNET\", \"paymentMeanBrand\":\"BCMCMOBILE\", \"responseKeyVersion\":\"1\",";
				string sealData = @"2AUTHOR_CAPTURE978IR_WS_2.11225005036700001http://www.oxcfe.beINTERNETBCMCMOBILE1" + transactionReference;
				var seal = GenerateSeal(sealData, "PCv4DO5zG-pBOIfjojGnslk1Q0jG7Wd5iVHHLhk2XXk");
				postData = postData + "\"transactionReference\":\"" + transactionReference + "\", \"seal\":\"" + seal + "\"}";

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var ss = new List<string>();



				return View(ss);
			}
			catch (Exception ex)
			{
				return View("error");
			}
			//return View("test");
		}

		public string GenerateSeal(string data, string key)
		{
			string sealAlgorithm = "HMAC-SHA-256";
			var rr = "";
			UTF8Encoding utF8Encoding = new UTF8Encoding();
			byte[] bytes = utF8Encoding.GetBytes(data);
			HMAC hmac = sealAlgorithm == "HMAC-SHA-1" ? (HMAC)new HMACSHA1() : (sealAlgorithm == "HMAC-SHA-384" ? (HMAC)new HMACSHA384() : (sealAlgorithm == "HMAC-SHA-512" ? (HMAC)new HMACSHA512() : (HMAC)new HMACSHA256()));
			hmac.Key = utF8Encoding.GetBytes(key);
			hmac.Initialize();
			return this._byteArrayToHEX(hmac.ComputeHash(bytes));
		}
		private string _byteArrayToHEX(byte[] ba)
		{
			StringBuilder stringBuilder = new StringBuilder(ba.Length * 2);
			foreach (byte num in ba)
				stringBuilder.AppendFormat("{0:x2}", (object)num);
			return stringBuilder.ToString();
		}
		#endregion				
	}
}