﻿using System;
using System.Linq;
using Nop.Plugin.PostXCustom.Models.Admin.Catalog;
using Nop.Core.Domain.Blogs;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Forums;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Logging;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Messages;
using Nop.Core.Domain.News;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Polls;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Stores;
using Nop.Core.Domain.Tax;
using Nop.Core.Domain.Topics;
using Nop.Core.Domain.Vendors;
using Nop.Core.Infrastructure.Mapper;
using Nop.Core.Plugins;
using Nop.Services.Authentication.External;
using Nop.Services.Cms;
using Nop.Services.Common;
using Nop.Services.Payments;
using Nop.Services.Shipping;
using Nop.Services.Shipping.Pickup;
using Nop.Services.Tax;
using Nop.Web.Framework.Security.Captcha;

namespace Nop.Plugin.PostXCustom.Extensions.Admin
{
	
	public static class AdminMappingExtensions
	{
		public static TDestination MapTo<TSource, TDestination>(this TSource source)
		{
			return AutoMapperConfiguration.Mapper.Map<TSource, TDestination>(source);
		}

		public static TDestination MapTo<TSource, TDestination>(this TSource source, TDestination destination)
		{
			return AutoMapperConfiguration.Mapper.Map(source, destination);
		}

		

		#region Products

		public static ProductModel ToModel(this Product entity)
		{
			return entity.MapTo<Product, ProductModel>();
		}

		public static Product ToEntity(this ProductModel model)
		{
			return model.MapTo<ProductModel, Product>();
		}

		public static Product ToEntity(this ProductModel model, Product destination)
		{
			return model.MapTo(destination);
		}

		#endregion

		

		

	}
}
