﻿namespace Nop.Plugin.PostXCustom.Models.CommonModels
{
	public class CmsOrderApiModel
    {

        public int companyId { get; set; }
        public string creationDate { get; set; }
        public int customerId { get; set; }
        public string deliveryDate { get; set; }
        public string description { get; set; }
        public int id { get; set; }
        public string note { get; set; }
        public OrderItemDTO[] orderItems { get; set; }
        public OrderPaymentDTO orderPayment { get; set; }
        public OrderPaymentDTO[] orderPayments { get; set; }
        public OrderTaxInfoDTO[] orderTaxInfo { get; set; }
        public int receiptId { get; set; }
        public string status { get; set; }
        public string type { get; set; }
    }


    public class OrderItemDTO
    {
        public int amount { get; set; }
        public int id { get; set; }
        public OrderItemModifierDTO[] modifiers { get; set; }
        public int productId { get; set; }
        public string productPlu { get; set; }
        public OrderItemDTO[] subitems { get; set; }
        public decimal totalPrice { get; set; }
        public decimal totalPriceWithoutVat { get; set; }
        public decimal unitPrice { get; set; }
        public decimal unitPriceWithoutVat { get; set; }
    }

    public class OrderPaymentDTO
    {
        public decimal amount { get; set; }
        public int id { get; set; }
        public int paymentTypeId { get; set; }
        public int paymentTypeTypeId { get; set; }
    }
    
    public class OrderTaxInfoDTO
    {
        public int id { get; set; }
        public decimal tax { get; set; }
        public decimal taxRate { get; set; }
        public decimal totalWithTax { get; set; }
        public decimal totalWithoutTax { get; set; }
    }
    
    public class OrderItemModifierDTO
    {
        public string description { get; set; }
        public int id { get; set; }
        public int modifierId { get; set; }
        public string modifierName { get; set; }
        public int modifierValueId { get; set; }
        public decimal price { get; set; }
        public decimal priceWithoutVat { get; set; }
    }
}
