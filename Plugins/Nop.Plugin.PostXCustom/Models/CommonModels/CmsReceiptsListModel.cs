﻿namespace Nop.Plugin.PostXCustom.Models.CommonModels
{
	public class CmsReceiptsListModel
	{
		public ReceiptDTO[] results { get; set; }

		public int amount { get; set; }

		public int offset { get; set; }

		public int total { get; set; }
	}


	public class ReceiptDTO
	{
		public ReceiptItemDTO[] actionItems { get; set; }
		public string closingDate { get; set; }
		public string creationDate { get; set; }
		public int customerId { get; set; }
		public string customerUuid { get; set; }
		public string deliveryDate { get; set; }
		public int floorId { get; set; }
		public int id { get; set; }
		public ReceiptItemDTO[] items { get; set; }
		public string modificationDate { get; set; }
		public int numberOfCustomers { get; set; }
		public int parentId { get; set; }
		public PaymentDTO[] payments { get; set; }
		public string printDate { get; set; }
		public int reservationId { get; set; }
		public int sequenceNumber { get; set; }
		public int sequentialId { get; set; }
		public string status { get; set; }
		public int tableId { get; set; }
		public decimal total { get; set; }
		public string type { get; set; }
		public int userId { get; set; }
		public string uuid { get; set; }
	}

	public class ReceiptItemDTO
	{
		public int amount { get; set; }
		public string barcode { get; set; }
		public string barcode2 { get; set; }
		public decimal deliveryVatPercentage { get; set; }
		public string id { get; set; }
		public string info { get; set; }
		public ModifierValueDTO[] modifierValues { get; set; }
		public string parentId { get; set; }
		public int priceTypeId { get; set; }
		public string prodId { get; set; }
		public string productId { get; set; }
		public string productName { get; set; }
		public string productPLU { get; set; }
		public decimal takeawayVatPercentage { get; set; }
		public decimal totalPrice { get; set; }
		public decimal totalPriceWithoutVat { get; set; }
		public decimal unitPrice { get; set; }
		public decimal unitPriceWithoutVat { get; set; }
		public decimal vatPercentage { get; set; }
	}


	public class PaymentDTO
	{
		public decimal amount { get; set; }
		public int id { get; set; }
		public int paymentTypeCategoryId { get; set; }
		public int paymentTypeId { get; set; }
		public decimal restitution { get; set; }
		public int statusId { get; set; }
		public decimal tips { get; set; }
		public string type { get; set; }
	}


	public class ModifierValueDTO
	{
		public int id { get; set; }
		public string info { get; set; }
		public int modifierId { get; set; }
		public string name { get; set; }
		public decimal price { get; set; }
		public decimal priceWithoutVAT { get; set; }
	}


}
