﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Nop.Plugin.PostXCustom.Models.CommonModels
{

	public class VatReportModelList
	{
		public VatReportModelList()
		{
			VatDataList = new List<VatReportModel>();
		}

		//[UIHint("DateNullable")]
		//public DateTime? OrderDate { get; set; }
		public string OrderDate { get; set; }

		//[UIHint("DateNullable")]
		//public DateTime? FromDate { get; set; }
		public string FromDate { get; set; }

		//[UIHint("DateNullable")]
		//public DateTime? ToDate { get; set; }
		public string ToDate { get; set; }

		[AllowHtml]
		public string CustomerId { get; set; }

		[AllowHtml]
		public string ReceiptId { get; set; }

		public List<VatReportModel> VatDataList { get; set; }
	}

	public class VatReportModel
	{
		public int CustomerId { get; set; }

		public int ReceiptId { get; set; }

		public string Username { get; set; }

		public DateTime CreateDate { get; set; }

		public decimal OrderTotal { get; set; }

		public decimal VAT21Total { get; set; }

		public decimal VAT12Total { get; set; }

		public decimal VAT6Total { get; set; }

		public decimal VAT21OrderTotal { get; set; }

		public decimal VAT12OrderTotal { get; set; }

		public decimal VAT6OrderTotal { get; set; }

		public decimal OrderTotalNonCash { get; set; }

		public decimal OrderTotalCash { get; set; }

		public string VatRegime { get; set; }
	}
}
