﻿using System;

namespace Nop.Plugin.PostXCustom.Models.CommonModels
{
	public class LightSpeedOrderSummaryModel
	{
		public string id { get; set; }
		public DateTime deliveryDate { get; set; }
		public DateTime creationDate { get; set; }
		public int customerId { get; set; }
		public string type { get; set; }
		public int receiptId { get; set; }
		public string status { get; set; }
		public LSOrderItemDTO[] orderItems { get; set; }
		public LSOrderPaymentDTO[] orderPayments { get; set; }
		public LSOrderTaxInfoDTO[] orderTaxInfo { get; set; }
		public string note { get; set; }
	}

	public class LSOrderItemDTO
	{
		public int amount { get; set; }
		public int id { get; set; }
		public LSOrderItemModifierDTO[] modifiers { get; set; }
		public int productId { get; set; }
		public string productPlu { get; set; }
		public LSOrderItemDTO[] subitems { get; set; }
		public decimal totalPrice { get; set; }
		public decimal totalPriceWithoutVat { get; set; }
		public decimal unitPrice { get; set; }
		public decimal unitPriceWithoutVat { get; set; }
	}

	public class LSOrderPaymentDTO
	{
		public decimal amount { get; set; }
		public int id { get; set; }
		public int paymentTypeId { get; set; }
		public int paymentTypeTypeId { get; set; }
	}

	public class LSOrderTaxInfoDTO
	{
		public int id { get; set; }
		public decimal tax { get; set; }
		public decimal taxRate { get; set; }
		public decimal totalWithTax { get; set; }
		public decimal totalWithoutTax { get; set; }
	}

	public class LSOrderItemModifierDTO
	{
		public string description { get; set; }
		public int id { get; set; }
		public int modifierId { get; set; }
		public string modifierName { get; set; }
		public int modifierValueId { get; set; }
		public decimal price { get; set; }
		public decimal priceWithoutVat { get; set; }
	}

}
