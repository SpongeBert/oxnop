﻿using System;

namespace Nop.Plugin.PostXCustom.Models.CommonModels
{
	public class CmsCustomerApiModel
	{
		public decimal amount { get; set; }

		public decimal offset { get; set; }

		
		public decimal total { get; set; }

		public results results { get; set; }

	}

	public class results
	{
		public string dateOfBirth { get; set; }
		public string deliveryCity { get; set; }
		public string deliveryCountry { get; set; }
		public string deliveryStreet { get; set; } 
		public string deliveryStreetNumber { get; set; }
		public string deliveryZip { get; set; }			
		public string email { get; set; }
		public string firstName { get; set; }				
		public int id { get; set; }
		public string language { get; set; }
		public string lastName { get; set; }
		public string note { get; set; }
		public string street { get; set; }
		public string streetNumber { get; set; }
		public string telephone { get; set; }
		public string uuid { get; set; }
		public string vatNumber { get; set; }
		public string zip { get; set; }						
		public string city { get; set; }
		public string country { get; set; }
		public int companyId { get; set; }
		public decimal credits { get; set; }

		public customerCards[] customerCards { get; set; }
	}

	public class customerCards
	{
		public string code { get; set; }
		public string customerUuid { get; set; }
		public string type { get; set; }
		public string status { get; set; }
		public string city { get; set; }
		public int companyId { get; set; }
		public int creationUserId { get; set; }
		public int customerId { get; set; }
		public int mainEstablishmentMasterId { get; set; }
		public int originalValue { get; set; }
		public int redeemedValue { get; set; }
		public int remainingValue { get; set; }
		public int soldValue { get; set; }
		public DateTime expiryDate { get; set; }
		public DateTime generatedDate { get; set; }
		public DateTime lastUsedDate { get; set; }
		public DateTime lastValueAddedDate { get; set; }

	}
}	 