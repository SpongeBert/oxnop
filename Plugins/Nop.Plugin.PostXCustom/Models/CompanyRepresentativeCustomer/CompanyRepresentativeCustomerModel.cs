﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Nop.Plugin.PostXCustom.Models.CompanyRepresentativeCustomer
{
	public class CompanyRepresentativeCustomerModel
	{
		public CompanyRepresentativeCustomerModel()
		{
			RecordId = 0;
			CompanyRepresentativeId = 0;
			CompanyRepresentativeList = new List<SelectListItem>();
		}

		public int RecordId { get; set; }

		public int CompanyRepresentativeId { get; set; }

		public int AssociatedCustomerId { get; set; }

		public List<SelectListItem> CompanyRepresentativeList { get; set; }

	}
}
