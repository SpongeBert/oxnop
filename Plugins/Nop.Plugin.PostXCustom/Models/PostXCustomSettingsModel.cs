﻿using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models
{
	public class PostXCustomSettingsModel : BaseNopModel
	{
		[NopResourceDisplayName("Nop.Plugin.PostXCustom.Fields.CustomerPushUrl")]
		public string CustomerPushUrl { get; set; }

		[NopResourceDisplayName("Nop.Plugin.PostXCustom.Fields.LoyaltyContractA")]
		public int LoyaltyContractA { get; set; }

		[NopResourceDisplayName("Nop.Plugin.PostXCustom.Fields.LoyaltyContractB")]
		public int LoyaltyContractB { get; set; }

		[NopResourceDisplayName("Nop.Plugin.PostXCustom.Fields.LoyaltyContractC")]
		public int LoyaltyContractC { get; set; }

		[NopResourceDisplayName("Nop.Plugin.PostXCustom.Fields.LoyaltyContractD")]
		public int LoyaltyContractD { get; set; }

		[NopResourceDisplayName("Nop.Plugin.PostXCustom.Fields.LoyaltyContractZ")]
		public int LoyaltyContractZ { get; set; }

	}
}
