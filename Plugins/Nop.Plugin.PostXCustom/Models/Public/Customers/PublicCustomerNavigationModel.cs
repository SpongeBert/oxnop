﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.Public.Customers
{	
	public partial class PublicCustomerNavigationModel : BaseNopModel
	{
		public PublicCustomerNavigationModel()
		{
			CustomerNavigationItems = new List<PublicCustomerNavigationItemModel>();
		}

		public IList<PublicCustomerNavigationItemModel> CustomerNavigationItems { get; set; }

		public PublicCustomerNavigationEnum SelectedTab { get; set; }
	}

	public class PublicCustomerNavigationItemModel
	{
		public string RouteName { get; set; }
		public string Title { get; set; }
		public PublicCustomerNavigationEnum Tab { get; set; }
		public string ItemClass { get; set; }
	}

	public enum PublicCustomerNavigationEnum
	{
		Info = 0,
		Addresses = 10,
		Orders = 20,
		BackInStockSubscriptions = 30,
		ReturnRequests = 40,
		DownloadableProducts = 50,
		RewardPoints = 60,
		ChangePassword = 70,
		Avatar = 80,
		ForumSubscriptions = 90,
		ProductReviews = 100,
		VendorInfo = 110
	}
}
