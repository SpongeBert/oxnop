﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Plugin.PostXCustom.Models.Public.Media;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using Nop.Plugin.PostXCustom.Validators.Public;

namespace Nop.Plugin.PostXCustom.Models.Public.Customers
{
	[Validator(typeof(PublicCustomerInfoValidator))]
	public partial class PublicCustomerInfoModel : BaseNopModel
	{
		public PublicCustomerInfoModel()
		{
			//AvailableTimeZones = new List<SelectListItem>();
			//AvailableCountries = new List<SelectListItem>();
			//AvailableStates = new List<SelectListItem>();
			//AssociatedExternalAuthRecords = new List<AssociatedExternalAuthModel>();
			CustomerAttributes = new List<PublicCustomerAttributeModel>();
			PictureModel = new PublicPictureModel();
			PersonalPictureModel = new PublicPictureModel();
			PictureId = 0;
			CustomerPictureId = 0;
		}

		[NopResourceDisplayName("Account.Fields.Email")]
		[AllowHtml]
		public string Email { get; set; }

		//[NopResourceDisplayName("Account.Fields.EmailToRevalidate")]
		//public string EmailToRevalidate { get; set; }

		//public bool CheckUsernameAvailabilityEnabled { get; set; }
		//public bool AllowUsersToChangeUsernames { get; set; }
		//public bool UsernamesEnabled { get; set; }
		//[NopResourceDisplayName("Account.Fields.Username")]
		//[AllowHtml]
		//public string Username { get; set; }

		//form fields & properties
		public bool GenderEnabled { get; set; }
		[NopResourceDisplayName("Account.Fields.Gender")]
		public string Gender { get; set; }

		[NopResourceDisplayName("Account.Fields.FirstName")]
		[AllowHtml]
		[RegularExpression("^[a-zA-Z0-9\\s]*$", ErrorMessage = "Geen vreemde tekens toegelaten.")]
		public string FirstName { get; set; }

		[NopResourceDisplayName("Account.Fields.LastName")]
		[AllowHtml]
		[RegularExpression("^[a-zA-Z0-9\\s]*$", ErrorMessage = "Geen vreemde tekens toegelaten.")]
		public string LastName { get; set; }


		public bool DateOfBirthEnabled { get; set; }
		[NopResourceDisplayName("Account.Fields.DateOfBirth")]
		public int? DateOfBirthDay { get; set; }
		[NopResourceDisplayName("Account.Fields.DateOfBirth")]
		public int? DateOfBirthMonth { get; set; }
		[NopResourceDisplayName("Account.Fields.DateOfBirth")]
		public int? DateOfBirthYear { get; set; }
		public bool DateOfBirthRequired { get; set; }
		public DateTime? ParseDateOfBirth()
		{
			if (!DateOfBirthYear.HasValue || !DateOfBirthMonth.HasValue || !DateOfBirthDay.HasValue)
				return null;

			DateTime? dateOfBirth = null;
			try
			{
				dateOfBirth = new DateTime(DateOfBirthYear.Value, DateOfBirthMonth.Value, DateOfBirthDay.Value);
			}
			catch { }
			return dateOfBirth;
		}

		public bool CompanyEnabled { get; set; }
		public bool CompanyRequired { get; set; }
		[NopResourceDisplayName("Account.Fields.Company")]
		[AllowHtml]
		public string Company { get; set; }

		//public bool StreetAddressEnabled { get; set; }
		//public bool StreetAddressRequired { get; set; }
		//[NopResourceDisplayName("Account.Fields.StreetAddress")]
		//[AllowHtml]
		//public string StreetAddress { get; set; }

		//public bool StreetAddress2Enabled { get; set; }
		//public bool StreetAddress2Required { get; set; }
		//[NopResourceDisplayName("Account.Fields.StreetAddress2")]
		//[AllowHtml]
		//public string StreetAddress2 { get; set; }

		//public bool ZipPostalCodeEnabled { get; set; }
		//public bool ZipPostalCodeRequired { get; set; }
		//[NopResourceDisplayName("Account.Fields.ZipPostalCode")]
		//[AllowHtml]
		//public string ZipPostalCode { get; set; }

		//public bool CityEnabled { get; set; }
		//public bool CityRequired { get; set; }
		//[NopResourceDisplayName("Account.Fields.City")]
		//[AllowHtml]
		//public string City { get; set; }

		//public bool CountryEnabled { get; set; }
		//public bool CountryRequired { get; set; }
		//[NopResourceDisplayName("Account.Fields.Country")]
		//public int CountryId { get; set; }
		//public IList<SelectListItem> AvailableCountries { get; set; }

		//public bool StateProvinceEnabled { get; set; }
		//public bool StateProvinceRequired { get; set; }
		//[NopResourceDisplayName("Account.Fields.StateProvince")]
		//public int StateProvinceId { get; set; }
		//public IList<SelectListItem> AvailableStates { get; set; }

		//public bool PhoneEnabled { get; set; }
		//public bool PhoneRequired { get; set; }
		//[NopResourceDisplayName("Account.Fields.Phone")]
		//[AllowHtml]
		//public string Phone { get; set; }

		//public bool FaxEnabled { get; set; }
		//public bool FaxRequired { get; set; }
		//[NopResourceDisplayName("Account.Fields.Fax")]
		//[AllowHtml]
		//public string Fax { get; set; }

		//public bool NewsletterEnabled { get; set; }
		//[NopResourceDisplayName("Account.Fields.Newsletter")]
		//public bool Newsletter { get; set; }

		//preferences
		//public bool SignatureEnabled { get; set; }
		//[NopResourceDisplayName("Account.Fields.Signature")]
		//[AllowHtml]
		//public string Signature { get; set; }

		//time zone
		//[NopResourceDisplayName("Account.Fields.TimeZone")]
		//public string TimeZoneId { get; set; }
		//public bool AllowCustomersToSetTimeZone { get; set; }
		//public IList<SelectListItem> AvailableTimeZones { get; set; }

		//EU VAT
		[NopResourceDisplayName("Account.Fields.VatNumber")]
		[AllowHtml]
		public string VatNumber { get; set; }
		public string VatNumberStatusNote { get; set; }
		public bool DisplayVatNumber { get; set; }

		//external authentication
		//[NopResourceDisplayName("Account.AssociatedExternalAuth")]
		//public IList<AssociatedExternalAuthModel> AssociatedExternalAuthRecords { get; set; }
		//public int NumberOfExternalAuthenticationProviders { get; set; }

		public IList<PublicCustomerAttributeModel> CustomerAttributes { get; set; }

		[UIHint("Picture")]
		[NopResourceDisplayName("Account.Fields.Picture")]
		public int PictureId { get; set; }

		public PublicPictureModel PictureModel { get; set; }

		[UIHint("Picture")]
		[NopResourceDisplayName("Account.Fields.Picture")]
		public int CustomerPictureId { get; set; }

		public PublicPictureModel PersonalPictureModel { get; set; }

		public string PersonalNumber { get; set; }
        public int CompanyId { get; set; }

        #region Nested classes

        public partial class AssociatedExternalAuthModel : BaseNopEntityModel
		{
			public string Email { get; set; }

			public string ExternalIdentifier { get; set; }

			public string AuthMethodName { get; set; }
		}

		#endregion
	}
}