﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.Public.Customers
{
    public partial class PublicRegisterResultModel : BaseNopModel
    {
        public string Result { get; set; }
    }
}