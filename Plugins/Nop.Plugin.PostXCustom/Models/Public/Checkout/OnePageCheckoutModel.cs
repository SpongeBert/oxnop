﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.Public.Checkout
{
    public partial class OnePageCheckoutModel : BaseNopModel
    {
        public bool ShippingRequired { get; set; }
        public bool DisableBillingAddressCheckoutStep { get; set; }
    }
}