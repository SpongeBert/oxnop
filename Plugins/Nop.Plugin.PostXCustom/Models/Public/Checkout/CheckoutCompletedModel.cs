﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.Public.Checkout
{
    public partial class CheckoutCompletedModel : BaseNopModel
    {
        public int OrderId { get; set; }
        public string CustomOrderNumber { get; set; }
        public bool OnePageCheckoutEnabled { get; set; }
    }
}