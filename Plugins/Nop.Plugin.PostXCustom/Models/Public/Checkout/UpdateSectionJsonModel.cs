﻿
namespace Nop.Plugin.PostXCustom.Models.Public.Checkout
{
    public class UpdateSectionJsonModel
    {
        public string name { get; set; }
        public string html { get; set; }
    }
}