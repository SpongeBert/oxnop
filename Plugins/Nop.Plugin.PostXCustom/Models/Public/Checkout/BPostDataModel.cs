﻿using System.Collections.Generic;
using Nop.Plugin.PostXCustom.Models.Public.Common;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.Public.Checkout
{
	public class BPostDataModel : BaseNopModel
	{
		public BPostDataModel()
		{
			Address = new AddressModel();
			Items = new List<CartItemModel>();
		}

		#region "PPIS for BPost"

		public AddressModel Address { get; set; }

		public string OrderTotal { get; set; }

		public string CheckSum { get; set; }

		public string AccountId { get; set; }

		public string OrderReference { get; set; }

		public string Language { get; set; }

		public string CountryCode { get; set; }

		public IList<CartItemModel> Items { get; set; }

		public bool IsMethodOverride { get; set; }


		#endregion

		public partial class CartItemModel : BaseNopEntityModel
		{
			public string Sku { get; set; }

			public int ProductId { get; set; }

			public string ProductName { get; set; }

			public string ProductSeName { get; set; }

			public string UnitPrice { get; set; }

			public string SubTotal { get; set; }

			public string Discount { get; set; }

			public int Quantity { get; set; }
		}

	}
}
