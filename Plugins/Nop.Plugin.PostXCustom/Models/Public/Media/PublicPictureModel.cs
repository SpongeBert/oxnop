﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.Public.Media
{	
	public partial class PublicPictureModel : BaseNopModel
	{
		public string ImageUrl { get; set; }

		public string ThumbImageUrl { get; set; }

		public string FullSizeImageUrl { get; set; }

		public string Title { get; set; }

		public string AlternateText { get; set; }
	}
}
