﻿using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.Public.CustomCode
{
	public partial class ProductApiModel : BaseNopModel
	{
		public int id { get; set; }

		public string imageLocation { get; set; }
		public string info { get; set; }
		public string name { get; set; }
		public decimal price { get; set; }
		public decimal priceWithoutVat { get; set; }
		public string productType { get; set; }
		public int sequence { get; set; }
		public string sku { get; set; }
		public int stockAmount { get; set; }
		public decimal takeawayPrice { get; set; }
		public decimal takeawayPriceWithoutVat { get; set; }
		public string takeawayTaxClass { get; set; }
		public string taxClass { get; set; }
		public bool visible { get; set; }

		public additions[] additions { get; set; }

		public decimal deliveryPrice { get; set; }
		public decimal deliveryPriceWithoutVat { get; set; }
		public string deliveryTaxClass { get; set; }
		public int[] groupIds { get; set; }





	}

	public class additions
	{
		public string displayName { get; set; }
		public int id { get; set; }
		public decimal maxSelectedAmount { get; set; }
		public decimal minSelectedAmount { get; set; }
		public bool multiselect { get; set; }
		public string name { get; set; }
		public values[] values { get; set; }
	}

	public class values
	{
		public string info { get; set; }
		public int id { get; set; }
		public int modifierId { get; set; }
		public decimal price { get; set; }
		public decimal priceWithoutVAT { get; set; }
		public string name { get; set; }
	}
}


