﻿using System;
using System.Collections.Generic;
using Nop.Plugin.PostXCustom.Models.Public.Common;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.Public.Order
{
	public partial class CustomerRewardPointsModel : BaseNopModel
	{
		public CustomerRewardPointsModel()
		{
			RewardPoints = new List<RewardPointsHistoryModel>();
		}

		public IList<RewardPointsHistoryModel> RewardPoints { get; set; }
		public PagerModel PagerModel { get; set; }
		public int RewardPointsBalance { get; set; }
		public string RewardPointsAmount { get; set; }
		public int MinimumRewardPointsBalance { get; set; }
		public string MinimumRewardPointsAmount { get; set; }

		#region Nested classes

		public partial class RewardPointsHistoryModel : BaseNopEntityModel
		{
			[NopResourceDisplayName("RewardPoints.Fields.Points")]
			public int Points { get; set; }

			[NopResourceDisplayName("RewardPoints.Fields.PointsBalance")]
			public string PointsBalance { get; set; }

			[NopResourceDisplayName("RewardPoints.Fields.Message")]
			public string Message { get; set; }

			[NopResourceDisplayName("RewardPoints.Fields.Date")]
			public DateTime CreatedOn { get; set; }
		}

		#endregion
	}
}
