﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.Public.ShoppingCart
{
	
	public partial class PublicOrderTotalsModel : BaseNopModel
	{
		public PublicOrderTotalsModel()
		{
			TaxRates = new List<TaxRate>();
			GiftCards = new List<GiftCard>();
		}
		public bool IsEditable { get; set; }

		public string SubTotal { get; set; }

		public string SubTotalDiscount { get; set; }

		public string Shipping { get; set; }
		public bool RequiresShipping { get; set; }
		public string SelectedShippingMethod { get; set; }
		public bool HideShippingTotal { get; set; }

		public string PaymentMethodAdditionalFee { get; set; }

		public string Tax { get; set; }
		public IList<TaxRate> TaxRates { get; set; }
		public bool DisplayTax { get; set; }
		public bool DisplayTaxRates { get; set; }


		public IList<GiftCard> GiftCards { get; set; }

		public string OrderTotalDiscount { get; set; }
		public int RedeemedRewardPoints { get; set; }
		public string RedeemedRewardPointsAmount { get; set; }

		public int WillEarnRewardPoints { get; set; }

		public string OrderTotal { get; set; }

		public decimal OrderTotalDecimal { get; set; }

		#region Nested classes

		public partial class TaxRate : BaseNopModel
		{
			public string Rate { get; set; }
			public string Value { get; set; }
		}

		public partial class GiftCard : BaseNopEntityModel
		{
			public string CouponCode { get; set; }
			public string Amount { get; set; }
			public string Remaining { get; set; }
		}
		#endregion
	}

}
