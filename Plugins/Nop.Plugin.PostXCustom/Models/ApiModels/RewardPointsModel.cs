﻿using System;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.PostXCustom.Models.ApiModels
{
	public class RewardPointsModel : BaseNopModel
	{
		public string CustomerEmail { get; set; }

		/// <summary>
		/// Gets or sets the points redeemed/added
		/// </summary>
		public int Points { get; set; }

		/// <summary>
		/// Gets or sets the points balance
		/// </summary>
		public int? PointsBalance { get; set; }

		/// <summary>
		/// Gets or sets the used amount
		/// </summary>
		public decimal UsedAmount { get; set; }

		/// <summary>
		/// Gets or sets the message
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the date and time of instance creation
		/// </summary>
		public DateTime CreatedOnUtc { get; set; }
	}
}
