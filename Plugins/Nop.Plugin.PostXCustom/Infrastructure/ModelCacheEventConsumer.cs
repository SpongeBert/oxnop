﻿using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Configuration;
using Nop.Core.Domain.Vendors;
using Nop.Core.Events;
using Nop.Core.Infrastructure;
using Nop.Services.Events;


namespace Nop.Plugin.PostXCustom.Infrastructure
{
	public partial class ModelCacheEventConsumer :
		//settings
		 IConsumer<EntityUpdated<Setting>>,
		//specification attributes
		 IConsumer<EntityInserted<SpecificationAttribute>>,
		 IConsumer<EntityUpdated<SpecificationAttribute>>,
		 IConsumer<EntityDeleted<SpecificationAttribute>>,
		//categories
		 IConsumer<EntityInserted<Category>>,
		 IConsumer<EntityUpdated<Category>>,
		 IConsumer<EntityDeleted<Category>>,
		//manufacturers
		 IConsumer<EntityInserted<Manufacturer>>,
		 IConsumer<EntityUpdated<Manufacturer>>,
		 IConsumer<EntityDeleted<Manufacturer>>,
		//vendors
		 IConsumer<EntityInserted<Vendor>>,
		 IConsumer<EntityUpdated<Vendor>>,
		 IConsumer<EntityDeleted<Vendor>>
	{
		/// <summary>
		/// Key for nopCommerce.com news cache
		/// </summary>
		public const string OFFICIAL_NEWS_MODEL_KEY = "Nop.pres.admin.official.news";
		public const string OFFICIAL_NEWS_PATTERN_KEY = "Nop.pres.admin.official.news";

		/// <summary>
		/// Key for specification attributes caching (product details page)
		/// </summary>
		public const string SPEC_ATTRIBUTES_MODEL_KEY = "Nop.pres.admin.product.specs";
		public const string SPEC_ATTRIBUTES_PATTERN_KEY = "Nop.pres.admin.product.specs";

		/// <summary>
		/// Key for categories caching
		/// </summary>
		/// <remarks>
		/// {0} : show hidden records?
		/// </remarks>
		public const string CATEGORIES_LIST_KEY = "Nop.pres.admin.categories.list-{0}";
		public const string CATEGORIES_LIST_PATTERN_KEY = "Nop.pres.admin.categories.list";

		/// <summary>
		/// Key for manufacturers caching
		/// </summary>
		/// <remarks>
		/// {0} : show hidden records?
		/// </remarks>
		public const string MANUFACTURERS_LIST_KEY = "Nop.pres.admin.manufacturers.list-{0}";
		public const string MANUFACTURERS_LIST_PATTERN_KEY = "Nop.pres.admin.manufacturers.list";

		/// <summary>
		/// Key for vendors caching
		/// </summary>
		/// <remarks>
		/// {0} : show hidden records?
		/// </remarks>
		public const string VENDORS_LIST_KEY = "Nop.pres.admin.vendors.list-{0}";
		public const string VENDORS_LIST_PATTERN_KEY = "Nop.pres.admin.vendors.list";



		/// <summary>
		/// Key for TopicModel caching
		/// </summary>
		/// <remarks>
		/// {0} : topic system name
		/// {1} : language id
		/// {2} : store id
		/// {3} : comma separated list of customer roles
		/// </remarks>
		public const string TOPIC_MODEL_BY_SYSTEMNAME_KEY = "Nop.pres.topic.details.bysystemname-{0}-{1}-{2}-{3}";
		/// <summary>
		/// Key for TopicModel caching
		/// </summary>
		/// <remarks>
		/// {0} : topic id
		/// {1} : language id
		/// {2} : store id
		/// {3} : comma separated list of customer roles
		/// </remarks>
		public const string TOPIC_MODEL_BY_ID_KEY = "Nop.pres.topic.details.byid-{0}-{1}-{2}-{3}";
		/// <summary>
		/// Key for TopicModel caching
		/// </summary>
		/// <remarks>
		/// {0} : topic system name
		/// {1} : language id
		/// {2} : store id
		/// </remarks>
		public const string TOPIC_SENAME_BY_SYSTEMNAME = "Nop.pres.topic.sename.bysystemname-{0}-{1}-{2}";
		/// <summary>
		/// Key for TopMenuModel caching
		/// </summary>
		/// <remarks>
		/// {0} : language id
		/// {1} : current store ID
		/// {2} : comma separated list of customer roles
		/// </remarks>
		public const string TOPIC_TOP_MENU_MODEL_KEY = "Nop.pres.topic.topmenu-{0}-{1}-{2}";
		/// <summary>
		/// Key for TopMenuModel caching
		/// </summary>
		/// <remarks>
		/// {0} : language id
		/// {1} : current store ID
		/// {2} : comma separated list of customer roles
		/// </remarks>
		public const string TOPIC_FOOTER_MODEL_KEY = "Nop.pres.topic.footer-{0}-{1}-{2}";
		public const string TOPIC_PATTERN_KEY = "Nop.pres.topic";

		/// <summary>
		/// Key for default product picture caching (all pictures)
		/// </summary>
		/// <remarks>
		/// {0} : product id
		/// {1} : picture size
		/// {2} : isAssociatedProduct?
		/// {3} : language ID ("alt" and "title" can depend on localized product name)
		/// {4} : is connection SSL secured?
		/// {5} : current store ID
		/// </remarks>
		public const string PRODUCT_DEFAULTPUBLICPICTURE_MODEL_KEY = "Nop.pres.product.detailspictures-{0}-{1}-{2}-{3}-{4}-{5}";
		public const string PRODUCT_DEFAULTPUBLICPICTURE_PATTERN_KEY = "Nop.pres.product.detailspictures";
		public const string PRODUCT_DEFAULTPUBLICPICTURE_PATTERN_KEY_BY_ID = "Nop.pres.product.detailspictures-{0}-";


		/// <summary>
		/// Key for ProductSpecificationModel caching
		/// </summary>
		/// <remarks>
		/// {0} : product id
		/// {1} : language id
		/// </remarks>
		public const string PRODUCT_SPECS_PICTURE_MODEL_KEY = "Nop.pres.product.specs-{0}-{1}";
		public const string PRODUCT_SPECS_PICTURE_PATTERN_KEY = "Nop.pres.product.specs";
		public const string PRODUCT_SPECS_PICTURE_PATTERN_KEY_BY_ID = "Nop.pres.product.specs-{0}-";

		/// <summary>
		/// Key for product reviews caching
		/// </summary>
		/// <remarks>
		/// {0} : product id
		/// {1} : current store ID
		/// </remarks>
		public const string PRODUCT_REVIEWS_MODEL_KEY = "Nop.pres.product.reviews-{0}-{1}";
		public const string PRODUCT_REVIEWS_PATTERN_KEY = "Nop.pres.product.reviews";
		public const string PRODUCT_REVIEWS_PATTERN_KEY_BY_ID = "Nop.pres.product.reviews-{0}-";
		private readonly ICacheManager _cacheManager;

		public ModelCacheEventConsumer()
		{
			//TODO inject static cache manager using constructor
			this._cacheManager = EngineContext.Current.ContainerManager.Resolve<ICacheManager>("nop_cache_static");
		}

		public void HandleEvent(EntityUpdated<Setting> eventMessage)
		{
			//clear models which depend on settings
			_cacheManager.RemoveByPattern(OFFICIAL_NEWS_PATTERN_KEY); //depends on AdminAreaSettings.HideAdvertisementsOnAdminArea
		}

		//specification attributes
		public void HandleEvent(EntityInserted<SpecificationAttribute> eventMessage)
		{
			_cacheManager.RemoveByPattern(SPEC_ATTRIBUTES_PATTERN_KEY);
		}
		public void HandleEvent(EntityUpdated<SpecificationAttribute> eventMessage)
		{
			_cacheManager.RemoveByPattern(SPEC_ATTRIBUTES_PATTERN_KEY);
		}
		public void HandleEvent(EntityDeleted<SpecificationAttribute> eventMessage)
		{
			_cacheManager.RemoveByPattern(SPEC_ATTRIBUTES_PATTERN_KEY);
		}

		//categories
		public void HandleEvent(EntityInserted<Category> eventMessage)
		{
			_cacheManager.RemoveByPattern(CATEGORIES_LIST_PATTERN_KEY);
		}
		public void HandleEvent(EntityUpdated<Category> eventMessage)
		{
			_cacheManager.RemoveByPattern(CATEGORIES_LIST_PATTERN_KEY);
		}
		public void HandleEvent(EntityDeleted<Category> eventMessage)
		{
			_cacheManager.RemoveByPattern(CATEGORIES_LIST_PATTERN_KEY);
		}

		//manufacturers
		public void HandleEvent(EntityInserted<Manufacturer> eventMessage)
		{
			_cacheManager.RemoveByPattern(MANUFACTURERS_LIST_PATTERN_KEY);
		}
		public void HandleEvent(EntityUpdated<Manufacturer> eventMessage)
		{
			_cacheManager.RemoveByPattern(MANUFACTURERS_LIST_PATTERN_KEY);
		}
		public void HandleEvent(EntityDeleted<Manufacturer> eventMessage)
		{
			_cacheManager.RemoveByPattern(MANUFACTURERS_LIST_PATTERN_KEY);
		}

		//vendors
		public void HandleEvent(EntityInserted<Vendor> eventMessage)
		{
			_cacheManager.RemoveByPattern(VENDORS_LIST_PATTERN_KEY);
		}
		public void HandleEvent(EntityUpdated<Vendor> eventMessage)
		{
			_cacheManager.RemoveByPattern(VENDORS_LIST_PATTERN_KEY);
		}
		public void HandleEvent(EntityDeleted<Vendor> eventMessage)
		{
			_cacheManager.RemoveByPattern(VENDORS_LIST_PATTERN_KEY);
		}
	}
}
