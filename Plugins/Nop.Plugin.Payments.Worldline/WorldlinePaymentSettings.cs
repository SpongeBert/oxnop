﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Payments.Worldline {
    public class WorldlinePaymentSettings : ISettings {
        public bool UseSandbox { get; set; }
        public string Account { get; set; }
        public string KeyVersion { get; set; }
        public string SecretKey { get; set; }
        public string InterfaceVersion { get; set; }
        public string NormalReturnUrl { get; set; }
        public string NotificationUrl { get; set; }
        public string SealAlgorithm { get; set; }
        public string TransactionReferencePrefix { get; set; }
    }
}