﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.Worldline.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using StarringJane.Worldline.Sips.Models;
using System.Collections.Specialized;
using Nop.Services.Common;
using System.Linq;
using Nop.Plugin.Payments.Worldline.Enums;
using Nop.Plugin.PostXCustom.Services;
using Nop.Core.Domain.Payments;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Nop.Plugin.PostXCustom.Models.CommonModels;
using System.Globalization;
using System.Text;
using Nop.Services.CustomService;
using Nop.Core.Domain.Logging;
using Nop.Plugin.PostXCustom.Domain;
using Nop.Services.Messages;
using Nop.Core.Domain.Localization;
using Nop.Services.Customers;
using System.Xml;

namespace Nop.Plugin.Payments.Worldline.Controllers
{
	public class PaymentWorldlineController : BasePaymentController
	{
		private readonly IWorkContext _workContext;
		private readonly IStoreService _storeService;
		private readonly ISettingService _settingService;
		private readonly IOrderService _orderService;
		private readonly IOrderProcessingService _orderProcessingService;
		private readonly ILocalizationService _localizationService;
		private readonly ILogger _logger;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly WorldlinePaymentSettings _worldlinePaymentSettings;

		private readonly ICmsNopCustomerMappingService _cmsNopCustomerMappingService;
		private readonly ICmsNopProductMappingService _cmsNopProductMappingService;
		private readonly ICmsNopOrderMappingService _cmsNopOrderMappingService;
		private readonly ICustomCodeService _customCodeService;

		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly OrderSettings _orderSettings;
		private readonly LocalizationSettings _localizationSettings;
		private readonly IPdfService _pdfService;
		private readonly IRewardPointService _rewardPointService;
		private readonly IStoreContext _storeContext;

		public PaymentWorldlineController(IWorkContext workContext,
			 IStoreService storeService,
			 ISettingService settingService,
			 IOrderService orderService,
			 IOrderProcessingService orderProcessingService,
			 ILocalizationService localizationService,
			 ILogger logger,
			 IGenericAttributeService genericAttributeService,
			 WorldlinePaymentSettings worldlinePaymentSettings,
			 ICmsNopCustomerMappingService cmsNopCustomerMappingService, ICmsNopProductMappingService cmsNopProductMappingService,
			 ICmsNopOrderMappingService cmsNopOrderMappingService, ICustomCodeService customCodeService,
			 IWorkflowMessageService workflowMessageService, OrderSettings orderSettings, LocalizationSettings localiationSettings,
			 IPdfService pdfService, IRewardPointService rewardPointService, IStoreContext storeContext)
		{
			_workContext = workContext;
			_storeService = storeService;
			_settingService = settingService;
			_orderService = orderService;
			_orderProcessingService = orderProcessingService;
			_localizationService = localizationService;
			_logger = logger;
			_worldlinePaymentSettings = worldlinePaymentSettings;
			_genericAttributeService = genericAttributeService;

			_cmsNopCustomerMappingService = cmsNopCustomerMappingService;
			_cmsNopProductMappingService = cmsNopProductMappingService;
			_cmsNopOrderMappingService = cmsNopOrderMappingService;
			_customCodeService = customCodeService;

			_workflowMessageService = workflowMessageService;
			_orderSettings = orderSettings;
			_localizationSettings = localiationSettings;
			_pdfService = pdfService;
			_rewardPointService = rewardPointService;
			_storeContext = storeContext;
		}

		[AdminAuthorize]
		[ChildActionOnly]
		public ActionResult Configure()
		{
			//load settings for a chosen store scope
			var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
			var worldlinePaymentSettings = _settingService.LoadSetting<WorldlinePaymentSettings>(storeScope);

			var model = new ConfigurationModel
			{
				UseSandbox = worldlinePaymentSettings.UseSandbox,
				Account = worldlinePaymentSettings.Account,
				KeyVersion = worldlinePaymentSettings.KeyVersion,
				SecretKey = worldlinePaymentSettings.SecretKey,
				InterfaceVersion = worldlinePaymentSettings.InterfaceVersion,
				NormalReturnUrl = worldlinePaymentSettings.NormalReturnUrl,
				NotificationUrl = worldlinePaymentSettings.NotificationUrl,
				SealAlgorithm = worldlinePaymentSettings.SealAlgorithm,
				ActiveStoreScopeConfiguration = storeScope,
				TransactionReferencePrefix = worldlinePaymentSettings.TransactionReferencePrefix
			};

			if (storeScope > 0)
			{
				model.UseSandbox_OverrideForStore = _settingService.SettingExists(worldlinePaymentSettings, x => x.UseSandbox, storeScope);
				model.Account_OverrideForStore = _settingService.SettingExists(worldlinePaymentSettings, x => x.Account, storeScope);
				model.KeyVersion_OverrideForStore = _settingService.SettingExists(worldlinePaymentSettings, x => x.KeyVersion, storeScope);
				model.SecretKey_OverrideForStore = _settingService.SettingExists(worldlinePaymentSettings, x => x.SecretKey, storeScope);
				model.InterfaceVersion_OverrideForStore = _settingService.SettingExists(worldlinePaymentSettings, x => x.InterfaceVersion, storeScope);
				model.NormalReturnUrl_OverrideForStore = _settingService.SettingExists(worldlinePaymentSettings, x => x.NormalReturnUrl, storeScope);
				model.NotificationUrl_OverrideForStore = _settingService.SettingExists(worldlinePaymentSettings, x => x.NotificationUrl, storeScope);
				model.SealAlgorithm_OverrideForStore = _settingService.SettingExists(worldlinePaymentSettings, x => x.SealAlgorithm, storeScope);
				model.TransactionReferencePrefix_OverrideForStore = _settingService.SettingExists(worldlinePaymentSettings, x => x.TransactionReferencePrefix, storeScope);
			}

			return View("~/Plugins/Payments.Worldline/Views/PaymentWorldline/Configure.cshtml", model);
		}

		[HttpPost]
		[AdminAuthorize]
		[ChildActionOnly]
		public ActionResult Configure(ConfigurationModel model)
		{
			if (!ModelState.IsValid) return Configure();

			//load settings for a chosen store scope
			var storeScope = GetActiveStoreScopeConfiguration(_storeService, _workContext);
			var worldlinePaymentSettings = _settingService.LoadSetting<WorldlinePaymentSettings>(storeScope);

			//save settings
			worldlinePaymentSettings.UseSandbox = model.UseSandbox;
			worldlinePaymentSettings.Account = model.Account;
			worldlinePaymentSettings.KeyVersion = model.KeyVersion;
			worldlinePaymentSettings.SecretKey = model.SecretKey;
			worldlinePaymentSettings.InterfaceVersion = model.InterfaceVersion;
			worldlinePaymentSettings.NormalReturnUrl = model.NormalReturnUrl;
			worldlinePaymentSettings.NotificationUrl = model.NotificationUrl;
			worldlinePaymentSettings.SealAlgorithm = model.SealAlgorithm;
			worldlinePaymentSettings.TransactionReferencePrefix = model.TransactionReferencePrefix;

			/* We do not clear cache after each setting update.
			 * This behavior can increase performance because cached settings will not be cleared 
			 * and loaded from database after each update */
			if (model.UseSandbox_OverrideForStore || storeScope == 0)
				_settingService.SaveSetting(worldlinePaymentSettings, x => x.UseSandbox, storeScope, false);
			else if (storeScope > 0)
				_settingService.DeleteSetting(worldlinePaymentSettings, x => x.UseSandbox, storeScope);

			if (model.Account_OverrideForStore || storeScope == 0)
				_settingService.SaveSetting(worldlinePaymentSettings, x => x.Account, storeScope, false);
			else if (storeScope > 0)
				_settingService.DeleteSetting(worldlinePaymentSettings, x => x.Account, storeScope);

			if (model.KeyVersion_OverrideForStore || storeScope == 0)
				_settingService.SaveSetting(worldlinePaymentSettings, x => x.KeyVersion, storeScope, false);
			else if (storeScope > 0)
				_settingService.DeleteSetting(worldlinePaymentSettings, x => x.KeyVersion, storeScope);

			if (model.SecretKey_OverrideForStore || storeScope == 0)
				_settingService.SaveSetting(worldlinePaymentSettings, x => x.SecretKey, storeScope, false);
			else if (storeScope > 0)
				_settingService.DeleteSetting(worldlinePaymentSettings, x => x.SecretKey, storeScope);

			if (model.InterfaceVersion_OverrideForStore || storeScope == 0)
				_settingService.SaveSetting(worldlinePaymentSettings, x => x.InterfaceVersion, storeScope, false);
			else if (storeScope > 0)
				_settingService.DeleteSetting(worldlinePaymentSettings, x => x.InterfaceVersion, storeScope);

			if (model.NormalReturnUrl_OverrideForStore || storeScope == 0)
				_settingService.SaveSetting(worldlinePaymentSettings, x => x.NormalReturnUrl, storeScope, false);
			else if (storeScope > 0)
				_settingService.DeleteSetting(worldlinePaymentSettings, x => x.NormalReturnUrl, storeScope);

			if (model.NotificationUrl_OverrideForStore || storeScope == 0)
				_settingService.SaveSetting(worldlinePaymentSettings, x => x.NotificationUrl, storeScope, false);
			else if (storeScope > 0)
				_settingService.DeleteSetting(worldlinePaymentSettings, x => x.NotificationUrl, storeScope);

			if (model.SealAlgorithm_OverrideForStore || storeScope == 0)
				_settingService.SaveSetting(worldlinePaymentSettings, x => x.SealAlgorithm, storeScope, false);
			else if (storeScope > 0)
				_settingService.DeleteSetting(worldlinePaymentSettings, x => x.SealAlgorithm, storeScope);

			if (model.TransactionReferencePrefix_OverrideForStore || storeScope == 0)
				_settingService.SaveSetting(worldlinePaymentSettings, x => x.TransactionReferencePrefix, storeScope, false);
			else if (storeScope > 0)
				_settingService.DeleteSetting(worldlinePaymentSettings, x => x.TransactionReferencePrefix, storeScope);

			//now clear settings cache
			_settingService.ClearCache();

			SuccessNotification(_localizationService.GetResource("Nop.Plugin.Payments.Worldline.Configure.ConfigurationSaved"));

			return Configure();
		}

		[ChildActionOnly]
		public ActionResult PaymentInfo()
		{
			return View("~/Plugins/Payments.Worldline/Views/PaymentWorldline/PaymentInfo.cshtml");
		}

		[NonAction]
		public override IList<string> ValidatePaymentForm(FormCollection form)
		{
			var warnings = new List<string>();

			return warnings;
		}

		private TransactionResult ProcessTransaction(PaymentInfo paymentInfo)
		{
			var prefix = _worldlinePaymentSettings.TransactionReferencePrefix;
			if (paymentInfo.Data.ContainsKey("transactionReference") && !string.IsNullOrEmpty(paymentInfo.Data["transactionReference"]) && paymentInfo.Data["transactionReference"].StartsWith(prefix))
			{
				var prefixLength = prefix.Length;
				var transactionIdPart = paymentInfo.Data["transactionReference"].Substring(prefixLength).Trim();
				if (!string.IsNullOrEmpty(transactionIdPart))
				{
					if (paymentInfo.Data.ContainsKey("responseCode"))
					{
						if (paymentInfo.Data.ContainsKey("orderId"))
						{
							int orderId = 0;
							var isInt = int.TryParse(paymentInfo.Data["orderId"], out orderId);
							if (isInt && orderId > 0)
							{
								var order = _orderService.GetOrderById(orderId);
								if (order != null)
								{
									var orderTransactionIDAttribute = _genericAttributeService.GetAttributesForEntity(order.Id, "Order")?.FirstOrDefault(x => x.Key == "TransactionID");
									if (orderTransactionIDAttribute != null)
									{
										if (paymentInfo.Data["responseCode"] == "17")
										{
											order.OrderNotes.Add(
												 new OrderNote
												 {
													 Note =
															"Received status=cancelled" + " for transactionId="
															+ paymentInfo.Data["transactionReference"],
													 DisplayToCustomer = false,
													 CreatedOnUtc = DateTime.UtcNow
												 });
											if (_orderProcessingService.CanCancelOrder(order))
											{
												_logger.Information("[Worldline] Marking transactionId=" + paymentInfo.Data["transactionReference"] + " as cancelled");
												_orderProcessingService.CancelOrder(order, false);
											}
											if (!paymentInfo.Data.ContainsKey("orderId"))
												paymentInfo.Data.Add("orderId", orderId.ToString());
											return TransactionResult.BuyerCancellation;
										}
										else if (paymentInfo.Data["responseCode"] == "00")
										{
											order.OrderNotes.Add(
												 new OrderNote
												 {
													 Note =
															"Received status=ok" + " for transactionId="
															+ paymentInfo.Data["transactionReference"],
													 DisplayToCustomer = false,
													 CreatedOnUtc = DateTime.UtcNow
												 });
											if (_orderProcessingService.CanMarkOrderAsPaid(order))
											{
												_logger.Information("[Worldline] Marking transactionId=" + paymentInfo.Data["transactionReference"] + " as paid");
												_orderProcessingService.MarkOrderAsPaid(order);
												_orderService.UpdateOrder(order);
											}
											return TransactionResult.AuthorisationAccepted;
										}
										else
										{
											order.OrderNotes.Add(
												 new OrderNote
												 {
													 Note =
															"Received status=exception" + " for transactionId="
															+ paymentInfo.Data["transactionReference"],
													 DisplayToCustomer = false,
													 CreatedOnUtc = DateTime.UtcNow
												 });
											if (!paymentInfo.Data.ContainsKey("orderId"))
												paymentInfo.Data.Add("orderId", orderId.ToString());
											return TransactionResult.Exception;
										}
									}
									else _logger.Warning("[Worldline] Incorrect payment response data received (\"transactionReference\" invalid).");
								}
								else _logger.Warning(string.Format("[Worldline] Incorrect payment response data received (no order with id = {0}).", orderId));
							}
							else _logger.Warning("[Worldline] Incorrect payment response data received (\"orderId\" is not a number).");
						}
						else _logger.Warning("[Worldline] Incorrect payment response data received (\"orderId\" is missing).");
					}
					else _logger.Warning("[Worldline] Incorrect payment response data received (\"responseCode\" is missing).");
				}
				else _logger.Warning("[Worldline] Incorrect payment response data received (\"transactionReference\" invalid).");
			}
			else _logger.Warning("[Worldline] Incorrect payment response data received (\"transactionReference\" missing).");
			return TransactionResult.Exception;
		}

		public ActionResult Success()
		{
			if (Request.HttpMethod.ToUpperInvariant() == "POST")
			{
				var paymentInfo = new PaymentInfo(Request.Form);

				switch (ProcessTransaction(paymentInfo))
				{
					case TransactionResult.AuthorisationAccepted:
						try
						{
							var transactionOrderId = Convert.ToInt64(paymentInfo.Data["orderId"]);
							LightSpeedOrderCreate(transactionOrderId, paymentInfo);//Replaced with Order create on payment success.

							//notifications//Custom Code // Notification to be send after payment success
							SendNotificationsAndSaveNotes(transactionOrderId);
							return RedirectToRoute("CheckoutCompleted", new { orderId = transactionOrderId });
						}
						catch (Exception ex)
						{
							var nopOrderId = Convert.ToInt64(paymentInfo.Data["orderId"]);
							_logger.InsertLog(LogLevel.Error, "SIPS error after payment success for nop order :: " + nopOrderId, ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);

							_customCodeService.SendErrorEmail("SIPS error after payment success for nop order ::  " + nopOrderId, ex.Message, ex.StackTrace);
							return RedirectToRoute("CheckoutCompleted", new { orderId = nopOrderId });
						}
					case TransactionResult.BuyerCancellation: return View("~/Plugins/Payments.Worldline/Views/PaymentWorldline/CancelPayment.cshtml", paymentInfo);
					case TransactionResult.Exception: return View("~/Plugins/Payments.Worldline/Views/PaymentWorldline/ExceptionPayment.cshtml", paymentInfo);
				}

				return View(paymentInfo);
			}
			else return View();
		}

		private string ExceptionDescription(string responseCode)
		{
			switch (responseCode)
			{
				case "02": return "Authorisation request to be performed via telephone with the issuer, as the card authorisation threshold has been exceeded. You need to be authorised to force transactions";
				case "03": return "Invalid Merchant contract";
				case "05": return "Authorisation refused";
				case "11": return "Used for differed check. The PAN is blocked.";
				case "12": return "Invalid transaction, check the request parameters";
				case "14": return "Invalid PAN or payment mean data(ex: card security code)";
				case "17": return "Buyer cancellation";
				case "24": return "Operation not authorized.The operation you wish to perform is not compliant with the transaction status";
				case "25": return "Transaction unknown by Sips";
				case "30": return "Format error";
				case "34": return "Fraud suspicion(seal erroneous)";
				case "40": return "Function not supported: the operation that you wish to perform is not part of the operation type for which you are authorised";
				case "51": return "Amount too high";
				case "54": return "Payment mean expiry date is past";
				case "60": return "Transaction pending";
				case "63": return "Security rules not observed, transaction stopped";
				case "75": return "Exceeded number of PAN attempts";
				case "90": return "Service temporarily not available";
				case "94": return "Duplicated transaction: the transactionReference has been used previously";
				case "97": return "Time frame exceeded, transaction refused";
				case "99": return "Temporary problem at the Sips server level";
				default: return string.Empty;
			}
		}

		[HttpPost]
		public ActionResult Notification()
		{
			NameValueCollection form = new NameValueCollection();
			for (int i = 0; i < Request.Form.Keys.Count; i++)
			{
				form.Add(Request.Form.Keys[i], Server.UrlDecode(Request.Form[Request.Form.Keys[i]]));
				_logger.Information(Request.Form.Keys[i] + " " + form[Request.Form.Keys[i]]);
			}
			var paymentInfo = new PaymentInfo(form);

			switch (ProcessTransaction(paymentInfo))
			{
				case TransactionResult.AuthorisationAccepted:
					var transactionOrderId = Convert.ToInt64(paymentInfo.Data["orderId"]);
					return RedirectToRoute("CheckoutCompleted", new { orderId = transactionOrderId });
				case TransactionResult.BuyerCancellation: return View("~/Plugins/Payments.Worldline/Views/PaymentWorldline/CancelPayment.cshtml", paymentInfo);
				case TransactionResult.Exception: return View("~/Plugins/Payments.Worldline/Views/PaymentWorldline/ExceptionPayment.cshtml", paymentInfo);
			}

			//Acknowledge receival and processing of status request
			return Content("ok");
		}

		public ActionResult Cancel()
		{
			return RedirectToAction("Index", "Home", new { area = "" });
		}

		public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
		{
			var paymentInfo = new ProcessPaymentRequest();

			return paymentInfo;
		}




		public void LightSpeedOrderStatusUpdate(long orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(Convert.ToInt32(orderId));
				if (order == null)
					return;

				//if (order.PaymentStatus == PaymentStatus.Authorized || order.PaymentStatus == PaymentStatus.Paid)
				if (order.PaymentStatus == PaymentStatus.Paid)
				{
					var cmsNopOrder = _cmsNopOrderMappingService.GetByNopOrderId(order.Id);
					if (cmsNopOrder == null)
						return;

					var cmsNopCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
					if (cmsNopCustomer == null)
						return;

					var url = "https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + cmsNopCustomer.CmsCustomerId + "/order/" + cmsNopOrder.CmsOrderId;
					var token = GetToken();

					var postData = new StringBuilder();
					postData.Append("{\"status\":\"ACCEPTED\"}");


					var request = (HttpWebRequest)WebRequest.Create(url);
					request.Headers.Add("X-Auth-Token", token);
					request.Headers.Add("customerId", cmsNopCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
					request.Headers.Add("orderId", cmsNopOrder.CmsOrderId.ToString(CultureInfo.InvariantCulture));
					request.ContentType = "application/json";
					request.Method = "PATCH";
					request.ContentLength = postData.Length;
					request.AutomaticDecompression = DecompressionMethods.GZip;

					var requestWriter = new StreamWriter(request.GetRequestStream());
					requestWriter.Write(postData);
					requestWriter.Close();

					var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
					var responseData = responseReader.ReadToEnd();
					if (!string.IsNullOrEmpty(responseData))
					{
						var jsonText = JsonConvert.DeserializeObject<CmsOrderApiModel>(responseData);
						if (jsonText.status.ToLower() == "done")
						{
							order.OrderStatus = OrderStatus.Complete;
							_orderService.UpdateOrder(order);
							return;
						}
					}
				}
				return;

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating order on CMS for nop order :: " + orderId, exception.Message + "  ||   " + exception.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Error updating order on CMS for nop order ::  " + orderId, exception.Message, exception.StackTrace);
				return;
			}
		}


		public void LightSpeedOrderCreate(long orderId, PaymentInfo paymentInfo)
		{
			#region Push Order to CMS
			try
			{
				var order = _orderService.GetOrderById(Convert.ToInt32(orderId));
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order not found in webshop :: " + orderId,
						 "Order push to cms :: Error :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId, "Order push to cms :: Error :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				if (order.PaymentStatus != PaymentStatus.Paid)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, "Current payment status :: " + order.PaymentStatus);

					return;
				}

				var checkCmsCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
				if (checkCmsCustomer == null)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, string.Empty);
					return;
				}

				string token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, string.Empty);
					return;
				}

				string url = @"https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + checkCmsCustomer.CmsCustomerId + "/order";
				var postData = new StringBuilder();
				postData.Append("{\"customerId\":" + checkCmsCustomer.CmsCustomerId + ",");

				var currentData = DateTime.Now;
				string newDate = currentData.Year + "-" + currentData.Month + "-" + currentData.Day + "T" + currentData.Hour + ":" +
									  currentData.Minute + ":" + currentData.Second + "+00:00";
				postData.Append("\"deliveryDate\":\"" + newDate + "\",");

				var orderType = GetOrderType(order);
				postData.Append("\"type\":\"" + orderType + "\",");
				postData.Append("\"status\":\"" + "ACCEPTED" + "\",");
				postData.Append("\"description\":\"" + "order" + "\",");

				//if (orderType.ToLower() == "delivery")
				//{
				//    var deliveryTime = string.Empty;
				//    var deliveryTimeDescripton = order.CheckoutAttributeDescription.Substring(order.CheckoutAttributeDescription.LastIndexOf("Gev"));

				//    if (!string.IsNullOrEmpty(deliveryTimeDescripton))
				//    {
				//        deliveryTime = deliveryTimeDescripton;
				//    }

				//    postData.Append("\"note\":\"" + "Order Reference: " + order.Id + " || " + deliveryTime + "\",");
				//}
				//else
				//    postData.Append("\"note\":\"" + "Order Reference: " + order.Id + "\",");


				var deliveryTime = string.Empty;
				var deliveryTimeDescripton = order.CheckoutAttributeDescription.Substring(order.CheckoutAttributeDescription.LastIndexOf("Gev"));

				if (!string.IsNullOrEmpty(deliveryTimeDescripton))
				{
					deliveryTime = deliveryTimeDescripton;
				}

				postData.Append("\"note\":\"" + "Order Reference: " + order.Id + " || " + deliveryTime + "\",");



				postData.Append("\"orderItems\":[");
				var totalItems = order.OrderItems.Count;
				var counter = 0;

				foreach (var orderItem in order.OrderItems)
				{
					var attributePriceDeduction = 0M;
					counter++;
					var cmsProduct = _cmsNopProductMappingService.GetByNopProductId(orderItem.ProductId);
					if (cmsProduct == null)
					{
						_logger.InsertLog(LogLevel.Error, "Error :: Order sync with CMS :: Nop Order Id :: " + orderItem.OrderId,
							 "Could not find mapped CMS product against Nop Product :: ID :: " + orderItem.ProductId + " :: Product Name :: " +
							 orderItem.Product.Name, _workContext.CurrentCustomer);
						break;
					}

					postData.Append("{\"productId\":" + cmsProduct.CmsProductId + ",");
					postData.Append("\"amount\":" + orderItem.Quantity + ",");
					if (!String.IsNullOrEmpty(orderItem.AttributesXml))
					{
						postData.Append("\"modifiers\":[");
						var xmlDoc = new XmlDocument();
						xmlDoc.LoadXml(orderItem.AttributesXml);

						var nodeCounter = 0;
						var attrCounter = 0;
						var nodeList = xmlDoc.SelectNodes(@"//Attributes/ProductAttribute");

						foreach (XmlNode node in xmlDoc.SelectNodes(@"//Attributes/ProductAttribute"))
						{
							nodeCounter++;
							if (node.Attributes != null && node.Attributes["ID"] != null)
							{
								int attributeId;
								var nopAttributeValueId = node.InnerText;
								if (!string.IsNullOrEmpty(nopAttributeValueId))
								{
									var nopCmsProductAttributeValueMapping =	_cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(Convert.ToInt32(nopAttributeValueId));

									var nopCmsProductAttributeMapping = new NopCmsProductAttributeMapping();
									if (nopCmsProductAttributeValueMapping != null)
									{
										nopCmsProductAttributeMapping =_cmsNopProductMappingService.GetNopCmsProductAttributeById(nopCmsProductAttributeValueMapping.NopCmsProductAttributeMappingId);
									}

									if (attrCounter > 0)
										postData.Append(",{");
									else
										postData.Append("{");

									if (nopCmsProductAttributeMapping != null)
									{
										postData.Append("\"description\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");
										postData.Append("\"modifierId\":" + nopCmsProductAttributeMapping.CmsProductAttributeId + ",");
										postData.Append("\"modifierName\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");

									}
									foreach (XmlNode attributeValue in node.SelectNodes("ProductAttributeValue"))
									{
										var value = attributeValue.SelectSingleNode("Value").InnerText.Trim();
										if (!String.IsNullOrEmpty(value))
										{
											var cmsProductAttributeValue = _cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(Convert.ToInt32(value));
											if (cmsProductAttributeValue != null)
											{
												postData.Append("\"modifierValueId\":" + cmsProductAttributeValue.CmsProductAttributeValueMappingId + ",");
												postData.Append("\"price\":" + cmsProductAttributeValue.Price + ",");
												postData.Append("\"priceWithoutVat\":" + cmsProductAttributeValue.Price);
												attributePriceDeduction += cmsProductAttributeValue.Price;
											}
										}
									}

									postData.Append("}");
									attrCounter++;
								}
							}
						}
						postData.Append("],");
					}
					var orderItemPrice = orderItem.PriceInclTax - attributePriceDeduction;
					postData.Append("\"totalPrice\":" + orderItemPrice + ",");
					postData.Append("\"totalPriceWithoutVat\":" + orderItemPrice + ",");
					postData.Append("\"unitPrice\":" + orderItemPrice + ",");
					postData.Append("\"unitPriceWithoutVat\":" + orderItemPrice);
					//postData.Append("\"totalPriceWithoutVat\":" + orderItem.PriceExclTax + ",");
					//postData.Append("\"unitPrice\":" + orderItem.UnitPriceInclTax + ",");
					//postData.Append("\"unitPriceWithoutVat\":" + orderItem.UnitPriceExclTax);
					postData.Append(counter >= totalItems ? "}" : "},");
				}
				//Temp Testing
				//postData.Append("],\"orderPayments\":[{");
				//postData.Append("\"amount\":" + order.OrderTotal + ",");
				//postData.Append("\"paymentTypeId\":" + 39893 + ",");
				//postData.Append("\"paymentTypeTypeId\":" + 4 + "}");

				var paymentTypeId = 0;
				var paymentTypeTypeId = 0;
				var paymentOption = string.Empty;

				GetLSPaymentInformation(paymentInfo, ref paymentTypeId, ref paymentTypeTypeId, ref paymentOption);

				if (paymentTypeId == 0 || paymentTypeTypeId == 0)
				{
					_logger.InsertLog(LogLevel.Error, "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId,
						 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, string.Empty);
					return;
				}

				postData.Append("],\"orderPayments\":[{");
				postData.Append("\"amount\":" + order.OrderTotal + ",");
				postData.Append("\"paymentTypeId\":" + paymentTypeId + ",");
				postData.Append("\"paymentTypeTypeId\":" + paymentTypeTypeId + "}");

				if (order.RedeemedRewardPointsEntry != null)
				{
					//Use Credit points
					//postData.Append(",{\"amount\":" + orderPlacedEvent.Order.RedeemedRewardPointsEntry.UsedAmount + ",");
					//postData.Append("\"paymentTypeId\":" + 5208 + ",");
					//postData.Append("\"paymentTypeTypeId\":" + 15 + "}");

					//Use Cash
					postData.Append(",{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
					postData.Append("\"paymentTypeId\":" + 36191 + ",");
					//postData.Append("\"paymentTypeTypeId\":" + 1 + "}");
					postData.Append("\"paymentTypeTypeId\":" + 200 + "}");
				}


				postData.Append("],\"orderTaxInfo\":[{");
				postData.Append("\"tax\":" + "0" + ",");
				postData.Append("\"taxRate\":" + "0" + ",");
				postData.Append("\"totalWithTax\":" + order.OrderTotal + ",");
				postData.Append("\"totalWithoutTax\":" + order.OrderTotal);
				postData.Append("}]}");

				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Order#: " + order.Id, postData.ToString(), _workContext.CurrentCustomer);

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					if (orderType.ToLower() == "delivery")
					{
						UpdateCustomerAddressOnLightSpeed(order, checkCmsCustomer);
					}
					else
					{
						//Update customer name with delivery Info on LS
						UpdateCustomerNameOnLightSpeed(Convert.ToInt32(responseData), checkCmsCustomer, order.Customer, order);
					}

					var cmsOrderNopOrderMap = new CmsOrderNopOrderMapping
					{
						NopOrderId = order.Id,
						CmsOrderId = Convert.ToInt32(responseData),
						CreatedOnUtc = DateTime.UtcNow,
						UpdatedOnUtc = DateTime.UtcNow
					};

					_cmsNopOrderMappingService.Insert(cmsOrderNopOrderMap);

					order.CustomOrderNumber = responseData;
					_orderService.UpdateOrder(order);
				}

			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error Creating Order on CMS for nop order :: " + orderId,
					 ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Error creating order on CMS for nop order ::  " + orderId, ex.Message, ex.StackTrace);

				return;
			}
			#endregion
		}

		public string GetToken()
		{
			try
			{
				string token;
				string url = @"https://euc1.posios.com/PosServer/rest/token";
				string postData = "{\"companyId\": 32123,\"deviceId\": \"prod\",\"password\": \"cmT4DkJk\",\"username\": \"lightspeed@cfe.com\"}";

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				var json = JObject.Parse(responseData);

				token = json.GetValue("token").Value<string>();
				return token;
			}
			catch (Exception ex)
			{
				return string.Empty;
			}
		}

		public string GetOrderType(Core.Domain.Orders.Order order)
		{
			if (order == null)
				return "takeaway";

			if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
			{
				if (order.CheckoutAttributeDescription.ToLower().Contains("levering"))
					return "delivery";
			}
			return "takeaway";
		}

		public void UpdateCustomerAddressOnLightSpeed(Core.Domain.Orders.Order order, CmsCustomerNopCustomerMapping checkCmsCustomer)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id,
						 "Error :: Customer Delivery Address Update on Light Speed :: Failed to get token from CMS to push address for nop order id:: " + order.Id, string.Empty);
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"deliveryCity\":\"" + order.ShippingAddress.City + "\",");
				postData.Append("\"deliveryCountry\":\"" + order.ShippingAddress.Country.TwoLetterIsoCode + "\",");
				postData.Append("\"deliveryStreet\":\"" + order.ShippingAddress.Address1 + "\",");
				postData.Append("\"deliveryStreetNumber\":\"" + order.ShippingAddress.Address2 + "\",");
				postData.Append("\"deliveryZip\":\"" + order.ShippingAddress.ZipPostalCode + "\",");
				postData.Append("\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + order.Customer.Email + "\",");

				//var loyaltyPoints = "0 reward points available.";
				//var customerRewardPointsBalance = _rewardPointService.GetRewardPointsBalance(order.Customer.Id, _storeContext.CurrentStore.Id);
				//if (customerRewardPointsBalance > 0)
				//	loyaltyPoints = customerRewardPointsBalance + " reward points available.";

				//postData.Append("\"firstName\":\"" + loyaltyPoints + "\",");
				//postData.Append("\"lastName\":\"" + order.Customer.GetFullName() + "\"");
				//postData.Append("}");


				var clientName = order.Customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.FirstName);
				var clientLastName = order.Customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(order.CheckoutAttributeDescription))
				{
					//clientName = order.CheckoutAttributeDescription;
					var dataArray = order.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1].Trim() + " | " + dataArray[3].Trim();
					clientLastName = order.Customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);

				}
			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating customer address on LS for order id #" + order.Id, exception.Message + "  ::  " + exception.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Error updating customer address on LS for order id #" + order.Id, exception.Message, exception.StackTrace);
				return;
			}
		}

		protected void SendNotificationsAndSaveNotes(long orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(Convert.ToInt32(orderId));
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId,
						"Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order ::  " + orderId, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				//send email notifications
				var orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
				if (orderPlacedStoreOwnerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to store owner) has been queued. Queued email identifier: {0}.", orderPlacedStoreOwnerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}

				var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 _pdfService.PrintOrderToPdf(order) : null;
				var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 "order.pdf" : null;
				var orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService
					 .SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
				if (orderPlacedCustomerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: " + orderId,
					ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);
				_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order :: " + orderId, ex.Message, ex.StackTrace);

				return;
			}
		}

		public void GetLSPaymentInformation(PaymentInfo paymentInfo, ref int paymentTypeId, ref int paymentTypeTypeId, ref string paymentOption)
		{
			if (paymentInfo.Data.Any())
			{
				var paymentMethod = paymentInfo.Data.FirstOrDefault(c => c.Key.ToLower() == "paymentmeanbrand");
				if (paymentMethod.Key != null && paymentMethod.Value != null)
				{
					paymentOption = paymentMethod.Value;
					//switch (paymentMethod.Value.ToLower())
					//{
					//	case "visa":
					//		paymentTypeId = 40580;
					//		paymentTypeTypeId = 801;
					//		break;
					//	case "mastercard":
					//		paymentTypeId = 39893;
					//		paymentTypeTypeId = 801;
					//		break;
					//	case "bcmc":
					//		paymentTypeId = 36192;
					//		paymentTypeTypeId = 801;
					//		break;
					//	case "maestro":
					//		paymentTypeId = 40976;
					//		paymentTypeTypeId = 801;
					//		break;
					//	case "vpay":
					//		paymentTypeId = 40977;
					//		paymentTypeTypeId = 801;
					//		break;
					//}	 

					switch (paymentMethod.Value.ToLower())
					{
						case "visa":
							paymentTypeId = 65537;
							paymentTypeTypeId = 801;
							break;
						case "mastercard":
							paymentTypeId = 65538;
							paymentTypeTypeId = 801;
							break;
						case "bcmc":
							paymentTypeId = 65536;
							paymentTypeTypeId = 801;
							break;
						case "maestro":
							paymentTypeId = 65557;
							paymentTypeTypeId = 801;
							break;
						case "vpay":
							paymentTypeId = 40977;
							paymentTypeTypeId = 801;
							break;
					}


				}
			}
		}


		public void UpdateCustomerNameOnLightSpeed(int cmsOrderId, CmsCustomerNopCustomerMapping checkCmsCustomer, Nop.Core.Domain.Customers.Customer customer, Nop.Core.Domain.Orders.Order nopOrder)
		{
			try
			{
				var url = "https://euc1.posios.com/PosServer/rest/core/customer/" + checkCmsCustomer.CmsCustomerId;
				var token = GetToken();
				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Error :: UpdateCustomerNameOnLightSpeed :: Failed to get token from CMS");
					return;
				}

				var postData = new StringBuilder();
				postData.Append("{\"id\":" + checkCmsCustomer.CmsCustomerId + ",");
				postData.Append("\"email\":\"" + customer.Email + "\",");

				var clientName = customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.FirstName);
				var clientLastName = customer.GetAttribute<string>(Nop.Core.Domain.Customers.SystemCustomerAttributeNames.LastName);

				if (!string.IsNullOrEmpty(nopOrder.CheckoutAttributeDescription))
				{
					//clientName = nopOrder.CheckoutAttributeDescription;
					//var dataArray = nopOrder.CheckoutAttributeDescription.Split(':');
					var dataArray = nopOrder.CheckoutAttributeDescription.Replace("<br />", ":").Split(':');
					clientName = dataArray[1].Trim() + " | " + dataArray[3].Trim();
					clientLastName = customer.GetFullName();
				}

				postData.Append("\"firstName\":\"" + clientName + "\",");
				postData.Append("\"lastName\":\"" + clientLastName + "\"");
				postData.Append("}");


				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "PUT";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					var jsonText = JsonConvert.DeserializeObject<CmsCustomerApiModel>(responseData);
				}

			}
			catch (Exception exception)
			{
				_logger.InsertLog(LogLevel.Error, "Error updating Name for customer on Light Speed CMS :: LS OrderId :: " + cmsOrderId + " || Nop Order Id :: " + nopOrder.Id + " || LS CustomerId :: " + checkCmsCustomer.CmsCustomerId + " || Nop CustomerId :: " + checkCmsCustomer.NopCustomerId, exception.Message + "  ::  " + exception.StackTrace);
			}
		}



		#region Manual LS(LightSpeed) Push
		public virtual ActionResult LSPush(int orderId)
		{
			#region Push Order to CMS
			try
			{
				var order = _orderService.GetOrderById(orderId);
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Order not found in webshop :: " + orderId,
						 "Order push to cms :: Error :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId, "Order push to cms :: Error :: Order not found in webshop :: " + orderId, string.Empty);

					return RedirectToRoute("AdminOrderEdit", new { id = orderId });
				}

				if (order.PaymentStatus != PaymentStatus.Paid)
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Order status is not paid yet for Nop Order Id :: " + order.Id, "Current payment status :: " + order.PaymentStatus);

					return RedirectToRoute("AdminOrderEdit", new { id = orderId });
				}


				var cmsNopOrder = _cmsNopOrderMappingService.GetByNopOrderId(order.Id);
				if (cmsNopOrder != null)
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id + " :: LS Order Id :: " + cmsNopOrder.CmsOrderId,
						 "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId + " :: LS Order Id :: " + cmsNopOrder.CmsOrderId,
						 "Order push to cms :: Error :: Order already pushed to LS CMS :: " + order.Id, string.Empty);
					return RedirectToRoute("AdminOrderEdit", new { id = orderId });
				}

				var checkCmsCustomer = _cmsNopCustomerMappingService.GetByNopCustomerId(order.CustomerId);
				if (checkCmsCustomer == null)
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: CMS Customer mapping not found for CustomerId :: " + order.CustomerId, string.Empty);
					return RedirectToRoute("AdminOrderEdit", new { id = orderId });
				}

				string token = GetToken();

				if (string.IsNullOrEmpty(token))
				{
					_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId,
						 "Order push to cms :: Error :: Failed to get token from CMS to push Nop order id:: " + orderId, string.Empty);
					return RedirectToRoute("AdminOrderEdit", new { id = orderId });
				}

				string url = @"https://euc1.posios.com/PosServer/rest/onlineordering/customer/" + checkCmsCustomer.CmsCustomerId + "/order";
				var postData = new StringBuilder();
				postData.Append("{\"customerId\":" + checkCmsCustomer.CmsCustomerId + ",");

				var currentData = DateTime.Now;
				string newDate = currentData.Year + "-" + currentData.Month + "-" + currentData.Day + "T" + currentData.Hour + ":" +
									  currentData.Minute + ":" + currentData.Second + "+00:00";
				postData.Append("\"deliveryDate\":\"" + newDate + "\",");

				var orderType = GetOrderType(order);
				postData.Append("\"type\":\"" + orderType + "\",");
				postData.Append("\"status\":\"" + "ACCEPTED" + "\",");
				postData.Append("\"description\":\"" + "order" + "\",");


				var deliveryTime = string.Empty;
				var deliveryTimeDescripton = order.CheckoutAttributeDescription.Substring(order.CheckoutAttributeDescription.LastIndexOf("Gev"));

				if (!string.IsNullOrEmpty(deliveryTimeDescripton))
				{
					deliveryTime = deliveryTimeDescripton;
				}

				postData.Append("\"note\":\"" + "Order Reference: " + order.Id + " || " + deliveryTime + "\",");



				postData.Append("\"orderItems\":[");
				var totalItems = order.OrderItems.Count;
				var counter = 0;
				foreach (var orderItem in order.OrderItems)
				{
					counter++;
					var cmsProduct = _cmsNopProductMappingService.GetByNopProductId(orderItem.ProductId);
					if (cmsProduct == null)
					{
						_logger.InsertLog(LogLevel.Error, "Error :: Manual Order sync with CMS :: Nop Order Id :: " + orderItem.OrderId,
							 "Could not find mapped CMS product against Nop Product :: ID :: " + orderItem.ProductId + " :: Product Name :: " +
							 orderItem.Product.Name, _workContext.CurrentCustomer);
						break;
					}

					postData.Append("{\"productId\":" + cmsProduct.CmsProductId + ",");
					postData.Append("\"amount\":" + orderItem.Quantity + ",");

					var attributePriceDeduction = 0M;
					if (!String.IsNullOrEmpty(orderItem.AttributesXml))
					{
						postData.Append("\"modifiers\":[");
						var xmlDoc = new XmlDocument();
						xmlDoc.LoadXml(orderItem.AttributesXml);
						var nodeCounter = 0;
						var attrCounter = 0;
						var nodeList = xmlDoc.SelectNodes(@"//Attributes/ProductAttribute");
						
						foreach (XmlNode node in xmlDoc.SelectNodes(@"//Attributes/ProductAttribute"))
						{
							nodeCounter++;
							if (node.Attributes != null && node.Attributes["ID"] != null)
							{
								int attributeId;
								var nopAttributeValueId = node.InnerText;
								if (!string.IsNullOrEmpty(nopAttributeValueId))
								{
									var nopCmsProductAttributeValueMapping =
										_cmsNopProductMappingService
											.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(
												Convert.ToInt32(nopAttributeValueId));

									var nopCmsProductAttributeMapping = new NopCmsProductAttributeMapping();
									if (nopCmsProductAttributeValueMapping != null)
									{
										nopCmsProductAttributeMapping =
											_cmsNopProductMappingService.GetNopCmsProductAttributeById(
												nopCmsProductAttributeValueMapping.NopCmsProductAttributeMappingId);
									}

									if (attrCounter > 0)
										postData.Append(",{");
									else
										postData.Append("{");
									
									if (nopCmsProductAttributeMapping !=null)
									{	
										postData.Append("\"description\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");
											postData.Append("\"modifierId\":" + nopCmsProductAttributeMapping.CmsProductAttributeId + ",");
											postData.Append("\"modifierName\":\"" + nopCmsProductAttributeMapping.CmsProductAttributeName + "\",");
										
									}
									foreach (XmlNode attributeValue in node.SelectNodes("ProductAttributeValue"))
									{
										var value = attributeValue.SelectSingleNode("Value").InnerText.Trim();
										if (!String.IsNullOrEmpty(value))
										{
											var cmsProductAttributeValue = _cmsNopProductMappingService.GetNopCmsProductAttributeValueMappingByProductAttributeValueId(Convert.ToInt32(value));
											if (cmsProductAttributeValue != null)
											{
												postData.Append("\"modifierValueId\":" + cmsProductAttributeValue.CmsProductAttributeValueMappingId + ",");
												postData.Append("\"price\":" + cmsProductAttributeValue.Price + ",");
												postData.Append("\"priceWithoutVat\":" + cmsProductAttributeValue.Price);
												attributePriceDeduction += cmsProductAttributeValue.Price;
											}
										}
									}
									
									postData.Append("}");
									attrCounter++;
								}
							}
						}
						postData.Append("],");
					}
					var orderItemPrice = orderItem.PriceInclTax - attributePriceDeduction;
					postData.Append("\"totalPrice\":" + orderItemPrice + ",");
					postData.Append("\"totalPriceWithoutVat\":" + orderItemPrice + ",");
					postData.Append("\"unitPrice\":" + orderItemPrice + ",");
					postData.Append("\"unitPriceWithoutVat\":" + orderItemPrice);

					//postData.Append("\"totalPrice\":" + orderItem.PriceInclTax + ",");
					//postData.Append("\"totalPriceWithoutVat\":" + orderItem.PriceExclTax + ",");
					//postData.Append("\"unitPrice\":" + orderItem.UnitPriceInclTax + ",");
					//postData.Append("\"unitPriceWithoutVat\":" + orderItem.UnitPriceExclTax);
					postData.Append(counter >= totalItems ? "}" : "},");
				}

				if (order.PaymentMethodSystemName.ToLower() == "payments.worldline")
				{
					var paymentTypeId = 0;
					var paymentTypeTypeId = 0;
					var paymentOption = string.Empty;

					GetLSPaymentInformationFromLog(order.Id, ref paymentTypeId, ref paymentTypeTypeId, ref paymentOption);

					if (paymentTypeId == 0 || paymentTypeTypeId == 0)
					{
						_logger.InsertLog(LogLevel.Error, "Manual Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId,
							 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, _workContext.CurrentCustomer);

						_customCodeService.SendErrorEmail("Error creating Manual order on CMS for nop order ::  " + orderId,
							 "Order push to cms :: Error :: Payment Type not configured :: " + paymentOption + " :: Nop Order Id :: " + orderId, string.Empty);
						return RedirectToRoute("AdminOrderEdit", new { id = orderId });
					}

					postData.Append("],\"orderPayments\":[{");
					postData.Append("\"amount\":" + order.OrderTotal + ",");
					postData.Append("\"paymentTypeId\":" + paymentTypeId + ",");
					postData.Append("\"paymentTypeTypeId\":" + paymentTypeTypeId + "}");

					if (order.RedeemedRewardPointsEntry != null)
					{
						//Use Cash
						postData.Append(",{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
						postData.Append("\"paymentTypeId\":" + 36191 + ",");
						//postData.Append("\"paymentTypeTypeId\":" + 1 + "}");
						postData.Append("\"paymentTypeTypeId\":" + 200 + "}");
					}
				}

				if (order.PaymentMethodSystemName.ToLower() == "payments.loyaltypoints")
				{

					postData.Append("],\"orderPayments\":[");

					if (order.RedeemedRewardPointsEntry != null)
					{
						//Use Cash
						postData.Append("{\"amount\":" + order.RedeemedRewardPointsEntry.UsedAmount + ",");
						postData.Append("\"paymentTypeId\":" + 36191 + ",");
						//postData.Append("\"paymentTypeTypeId\":" + 1 + "}");
						postData.Append("\"paymentTypeTypeId\":" + 200 + "}");
					}
				}


				postData.Append("],\"orderTaxInfo\":[{");
				postData.Append("\"tax\":" + "0" + ",");
				postData.Append("\"taxRate\":" + "0" + ",");
				postData.Append("\"totalWithTax\":" + order.OrderTotal + ",");
				postData.Append("\"totalWithoutTax\":" + order.OrderTotal);
				postData.Append("}]}");

				_logger.InsertLog(LogLevel.Information, "Payload Data for Nop Order#: " + order.Id, postData.ToString(), _workContext.CurrentCustomer);

				var request = (HttpWebRequest)WebRequest.Create(url);
				request.Headers.Add("X-Auth-Token", token);
				request.Headers.Add("customerId", checkCmsCustomer.CmsCustomerId.ToString(CultureInfo.InvariantCulture));
				request.ContentType = "application/json";
				request.Method = "POST";
				request.ContentLength = postData.Length;
				request.AutomaticDecompression = DecompressionMethods.GZip;

				var requestWriter = new StreamWriter(request.GetRequestStream());
				requestWriter.Write(postData);
				requestWriter.Close();

				var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
				var responseData = responseReader.ReadToEnd();
				if (!string.IsNullOrEmpty(responseData))
				{
					if (orderType.ToLower() == "delivery")
					{
						UpdateCustomerAddressOnLightSpeed(order, checkCmsCustomer);
					}
					else
					{
						//Update customer name with delivery Info on LS
						UpdateCustomerNameOnLightSpeed(Convert.ToInt32(responseData), checkCmsCustomer, order.Customer, order);
					}

					var cmsOrderNopOrderMap = new CmsOrderNopOrderMapping
					{
						NopOrderId = order.Id,
						CmsOrderId = Convert.ToInt32(responseData),
						CreatedOnUtc = DateTime.UtcNow,
						UpdatedOnUtc = DateTime.UtcNow
					};

					_cmsNopOrderMappingService.Insert(cmsOrderNopOrderMap);
					order.CustomOrderNumber = responseData;
					_orderService.UpdateOrder(order);

					//notifications//Custom Code // Notification to be send after payment success
					SendNotificationsAndSaveNotesManual(orderId);

				}

			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Manual Error Creating Order on CMS for nop order :: " + orderId,
					 ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);

				_customCodeService.SendErrorEmail("Manual Error creating order on CMS for nop order ::  " + orderId, ex.Message, ex.StackTrace);

				return RedirectToRoute("AdminOrderEdit", new { id = orderId });
			}
			return RedirectToRoute("AdminOrderEdit", new { id = orderId });
			#endregion
		}


		public void GetLSPaymentInformationFromLog(int orderId, ref int paymentTypeId, ref int paymentTypeTypeId, ref string paymentOption)
		{
			var logInfo = _logger.GetAllLogs().FirstOrDefault(x => x.ShortMessage.Contains("orderId=" + orderId));

			if (logInfo != null)
			{
				//Old
				//if (logInfo.ShortMessage.ToLower().Contains("visa"))
				//{
				//	paymentTypeId = 40580;
				//	paymentTypeTypeId = 801;
				//	paymentOption = "Visa";
				//}
				//else if (logInfo.ShortMessage.ToLower().Contains("mastercard"))
				//{		
				//	paymentTypeId = 39893;
				//	paymentTypeTypeId = 801;
				//	paymentOption = "MasterCard";
				//}
				//else if (logInfo.ShortMessage.ToLower().Contains("bcmc"))
				//{		
				//	paymentTypeId = 36192;
				//	paymentTypeTypeId = 801;
				//	paymentOption = "bcmc";
				//}
				//else if (logInfo.ShortMessage.ToLower().Contains("maestro"))
				//{		
				//	paymentTypeId = 40976;
				//	paymentTypeTypeId = 801;
				//	paymentOption = "maestro";
				//}
				//else if (logInfo.ShortMessage.ToLower().Contains("vpay"))
				//{		
				//	paymentTypeId = 40977;
				//	paymentTypeTypeId = 801;
				//	paymentOption = "vpay";
				//}


				//New
				if (logInfo.ShortMessage.ToLower().Contains("visa"))
				{
					paymentTypeId = 65537;
					paymentTypeTypeId = 801;
					paymentOption = "Visa";
				}
				else if (logInfo.ShortMessage.ToLower().Contains("mastercard"))
				{
					paymentTypeId = 65538;
					paymentTypeTypeId = 801;
					paymentOption = "MasterCard";
				}
				else if (logInfo.ShortMessage.ToLower().Contains("bcmc"))
				{
					paymentTypeId = 65536;
					paymentTypeTypeId = 801;
					paymentOption = "bcmc";
				}
				else if (logInfo.ShortMessage.ToLower().Contains("maestro"))
				{
					paymentTypeId = 65557;
					paymentTypeTypeId = 801;
					paymentOption = "maestro";
				}
				else if (logInfo.ShortMessage.ToLower().Contains("vpay"))
				{
					paymentTypeId = 40977;
					paymentTypeTypeId = 801;
					paymentOption = "vpay";
				}
			}
		}


		protected void SendNotificationsAndSaveNotesManual(int orderId)
		{
			try
			{
				var order = _orderService.GetOrderById(orderId);
				if (order == null)
				{
					_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId,
						"Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, _workContext.CurrentCustomer);

					_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order ::  " + orderId, "Error sending notifications after creating Order on CMS for nop order :: Order not found in webshop :: " + orderId, string.Empty);

					return;
				}

				//send email notifications
				var orderPlacedStoreOwnerNotificationQueuedEmailId = _workflowMessageService.SendOrderPlacedStoreOwnerNotification(order, _localizationSettings.DefaultAdminLanguageId);
				if (orderPlacedStoreOwnerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to store owner) has been queued. Queued email identifier: {0}.", orderPlacedStoreOwnerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}

				var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 _pdfService.PrintOrderToPdf(order) : null;
				var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
					 "order.pdf" : null;
				var orderPlacedCustomerNotificationQueuedEmailId = _workflowMessageService
					 .SendOrderPlacedCustomerNotification(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
				if (orderPlacedCustomerNotificationQueuedEmailId > 0)
				{
					order.OrderNotes.Add(new OrderNote
					{
						Note = string.Format("\"Order placed\" email (to customer) has been queued. Queued email identifier: {0}.", orderPlacedCustomerNotificationQueuedEmailId),
						DisplayToCustomer = false,
						CreatedOnUtc = DateTime.UtcNow
					});
					_orderService.UpdateOrder(order);
				}
			}
			catch (Exception ex)
			{
				_logger.InsertLog(LogLevel.Error, "Error sending notifications after creating Order on CMS for nop order :: " + orderId,
					ex.Message + "  ||   " + ex.StackTrace, _workContext.CurrentCustomer);
				_customCodeService.SendErrorEmail("Error sending notifications after creating Order on CMS for nop order :: " + orderId, ex.Message, ex.StackTrace);
			}
		}

		#endregion


	}
}
