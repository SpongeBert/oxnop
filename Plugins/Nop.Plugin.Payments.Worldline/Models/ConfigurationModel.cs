﻿using FluentValidation.Attributes;
using Nop.Plugin.Payments.Worldline.Validation;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Nop.Plugin.Payments.Worldline.Models {

    [Validator(typeof(ConfigurationModelValidator))]
    public class ConfigurationModel : BaseNopModel {
        public int ActiveStoreScopeConfiguration { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.UseSandbox")]
        public bool UseSandbox { get; set; }

        public bool UseSandbox_OverrideForStore { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.Account")]
        public string Account { get; set; }

        public bool Account_OverrideForStore { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.KeyVersion")]
        public string KeyVersion { get; set; }

        public bool KeyVersion_OverrideForStore { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.SecretKey")]
        public string SecretKey { get; set; }

        public bool SecretKey_OverrideForStore { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.InterfaceVersion")]
        public string InterfaceVersion { get; set; }

        public bool InterfaceVersion_OverrideForStore { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.NormalReturnUrl")]
        public string NormalReturnUrl { get; set; }

        public bool NormalReturnUrl_OverrideForStore { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.CancelUrl")]
        public string CancelUrl { get; set; }

        public bool CancelUrl_OverrideForStore { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.NotificationUrl")]
        public string NotificationUrl { get; set; }

        public bool NotificationUrl_OverrideForStore { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.SealAlgorithm")]
        public string SealAlgorithm { get; set; }

        public bool SealAlgorithm_OverrideForStore { get; set; }

        [NopResourceDisplayName("Nop.Plugin.Payments.Worldline.Configuration.Fields.TransactionReferencePrefix")]
        [StringLength(3, ErrorMessage = "The TransactionReferencePrefix value cannot exceed 3 characters.")]
        public string TransactionReferencePrefix { get; set; }

        public bool TransactionReferencePrefix_OverrideForStore { get; set; }
    }
}