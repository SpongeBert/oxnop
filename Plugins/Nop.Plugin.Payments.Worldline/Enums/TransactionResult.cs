﻿namespace Nop.Plugin.Payments.Worldline.Enums
{
    public enum TransactionResult
    {
        AuthorisationAccepted,
        BuyerCancellation,
        Exception
    }
}

