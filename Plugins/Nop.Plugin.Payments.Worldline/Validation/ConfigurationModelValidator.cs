﻿using FluentValidation;
using Nop.Plugin.Payments.Worldline.Formatting;
using Nop.Plugin.Payments.Worldline.Models;
using Nop.Services.Localization;

namespace Nop.Plugin.Payments.Worldline.Validation {
    class ConfigurationModelValidator : AbstractValidator<ConfigurationModel> {
        public ConfigurationModelValidator(ILocalizationService localizationService) {
            RuleFor(x => x.Account)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Nop.Plugin.Payments.Worldline.Validation.Account.Required"));
            RuleFor(x => x.KeyVersion)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Nop.Plugin.Payments.Worldline.Validation.KeyVersion.Required"));
            RuleFor(x => x.SecretKey)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Nop.Plugin.Payments.Worldline.Validation.SecretKey.Required"));
            RuleFor(x => x.InterfaceVersion)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Nop.Plugin.Payments.Worldline.Validation.InterfaceVersion.Required"));
            RuleFor(x => x.TransactionReferencePrefix)
                .Matches(RegularExpressions.AlphaNumericWord)
                .WithMessage(localizationService.GetResource("Nop.Plugin.Payments.Worldline.Validation.TransactionReferencePrefix.AlphaNumeric"));
            RuleFor(x => x.NormalReturnUrl)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Nop.Plugin.Payments.Worldline.Validation.NormalReturnUrl.Required"));
            RuleFor(x => x.NotificationUrl)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Nop.Plugin.Payments.Worldline.Validation.NotificationUrl.Required"));
            RuleFor(x => x.SealAlgorithm)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Nop.Plugin.Payments.Worldline.Validation.SealAlgorithm.Required"));
        }
    }
}