﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Payments.Worldline {
    public partial class RouteProvider : IRouteProvider {
        public void RegisterRoutes(RouteCollection routes) {
         
			//Success
            routes.MapRoute("Plugin.Payments.Worldline.Success",
                "Plugins/PaymentWorldline/Success",
                new {controller = "PaymentWorldline", action = "Success"},
                new[] {"Nop.Plugin.Payments.Worldline.Controllers"}
                );
         
			//Cancel
            routes.MapRoute("Plugin.Payments.Worldline.Cancel",
                "Plugins/PaymentWorldline/Cancel",
                new {controller = "PaymentWorldline", action = "Cancel"},
                new[] {"Nop.Plugin.Payments.Worldline.Controllers"}
                );

            //Notification
            routes.MapRoute("Plugin.Payments.Worldline.Notification",
                "Plugins/PaymentWorldline/Notification",
                new {controller = "PaymentWorldline", action = "Notification"},
                new[] {"Nop.Plugin.Payments.Worldline.Controllers"}
                );
													  
            routes.MapRoute("AdminOrderEdit",
                    "admin/order/edit/{id}",
                    new { controller = "Order", action = "Edit", },
                      new { id = @"\d+" },
                    new[] { "Nop.Admin.Controllers" }
             );


        }

        public int Priority {
            get { return 0; }
        }
    }
}