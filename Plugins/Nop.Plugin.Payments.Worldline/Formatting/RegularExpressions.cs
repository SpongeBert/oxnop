﻿using System.Text.RegularExpressions;

namespace Nop.Plugin.Payments.Worldline.Formatting {
    internal struct RegularExpressions {
        public static readonly Regex AlphaNumericWord = new Regex(@"^[^\W_]*$", RegexOptions.Compiled);
    }
}