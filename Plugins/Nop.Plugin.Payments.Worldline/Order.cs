﻿namespace Nop.Plugin.Payments.Worldline {
    public class Order {
        public string type { get; set; }
        public string order_id { get; set; }
        public string gateway { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
        public string description { get; set; }
        //public string var1 { get; set; }
        //public string var2 { get; set; }
        //public string var3 { get; set; }
        //public string items { get; set; }
        //public string manual { get; set; }
        //public string days_active { get; set; }
        public gateway_info gateway_info { get; set; }
        public payment_options payment_options { get; set; }
        //ublic customer customer { get; set; }
        //public google_analytics google_analytics { get; set; }
    }

    public class gateway_info {
        public string issuer_id { get; set; }
    }

    public class payment_options {
        public string notification_url { get; set; }
        public string redirect_url { get; set; }
        public string cancel_url { get; set; }
        public string close_window { get; set; } //true/false
    }

    public class customer {
        public string locale { get; set; }
        public string ip_address { get; set; }
        public string forwarded_ip { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string house_number { get; set; }
        public string zip_code { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }

    public class google_analytics {
        public string account { get; set; }
    }
}