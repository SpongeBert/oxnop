﻿using Nop.Core;
using Nop.Core.Plugins;
using Nop.Core.Domain.Payments;
using Nop.Core.Infrastructure;
using Nop.Plugin.Payments.Worldline.Controllers;
using Nop.Services.Configuration;
using Nop.Services.Payments;
using Nop.Services.Localization;
using Nop.Services.Logging;

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;


namespace Nop.Plugin.Payments.Worldline
{
    using Core.Domain.Customers;
    using Services.Common;
    using Services.Directory;
    using Services.Tax;
    using StarringJane.Worldline.Sips;
    using StarringJane.Worldline.Sips.Models;
    using System.Linq;
    public class WorldlinePaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly WorldlinePaymentSettings _worldlinePaymentSettings;
        private readonly ISettingService _settingService;
        private readonly ILogger _logger;
        private readonly IWebHelper _webHelper;
        private readonly HttpContextBase _httpContext;
        private readonly IStoreContext _storeContext;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly ICurrencyService _currencyService;
        private readonly ILanguageService _languageService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ILocalizationService _localizationService;
        #endregion

        public WorldlinePaymentProcessor(
            WorldlinePaymentSettings worldlinePaymentSettings,
            ISettingService settingService,
            ILogger logger,
            IWebHelper webHelper,
            HttpContextBase httpContext,
            IStoreContext storeContext,
            ICurrencyService currencyService, ILocalizationService localizationService)
        {
            _worldlinePaymentSettings = worldlinePaymentSettings;
            _settingService = settingService;
            _logger = logger;
            _webHelper = webHelper;
            _httpContext = httpContext;
            _storeContext = storeContext;
            _currencyService = currencyService;
            _taxCategoryService = EngineContext.Current.Resolve<ITaxCategoryService>();
            _languageService = EngineContext.Current.Resolve<ILanguageService>();
            _genericAttributeService = EngineContext.Current.Resolve<IGenericAttributeService>();
            _localizationService = localizationService;
        }

        public override void Install()
        {
            //settings
            var settings = new WorldlinePaymentSettings { UseSandbox = true };

            _settingService.SaveSetting(settings);

            //locales
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configure.ConfigurationSaved", "Worldline payment provider configuration saved");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.UseSandbox", "Sandbox");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.Account", "Merchant id");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.KeyVersion", "Version of the key");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.SecretKey", "Secret key");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.InterfaceVersion", "Interface version");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.NormalReturnUrl", "Payment Success Url");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.NotificationUrl", "Payment Notification Url");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.SealAlgorithm", "Seal algorithm");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.TransactionReferencePrefix", "Transaction reference prefix");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Cancel.OrderCancelled", "Payment cancelled");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Cancel.OrderCancelledByUser", "You have cancelled ");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Cancel.OrderId", "Order id");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Cancel.ContinueShopping", "Continue shopping");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configure.Notes", "Configure your Worldline payment provider");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configure.Save", "Save");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.PaymentInfo.RedirectionTip", "You will be redirected to the secured Worldliney payment environment to complete the order payment");
            this.AddOrUpdatePluginLocaleResource("Nop.Plugin.Payments.Worldline.Exception", "Error occured");

            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<WorldlinePaymentSettings>();

            //locales
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configure.ConfigurationSaved");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.UseSandbox");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.Account");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.KeyVersion");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.SecretKey");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.InterfaceVersion");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.NormalReturnUrl");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.NotificationUrl");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.SealAlgorithm");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configuration.Fields.TransactionReferencePrefix");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Cancel.OrderCancelled");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Cancel.OrderCancelledByUser");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Cancel.OrderId");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Cancel.ContinueShopping");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configure.Notes");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Configure.Save");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.PaymentInfo.RedirectionTip");
            this.DeletePluginLocaleResource("Nop.Plugin.Payments.Worldline.Exception");

            base.Uninstall();
        }

        public Type GetControllerType()
        {
            return typeof(PaymentWorldlineController);
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentWorldline";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Worldline.Controllers" }, { "area", null } };
        }

        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentWorldline";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Worldline.Controllers" }, { "area", null } };
        }

        public string Normalize(string s)
        {
            var normalizeDictionary = new Dictionary<string, string>();
            if (!normalizeDictionary.ContainsKey("Š")) normalizeDictionary.Add("Š", "S");
            if (!normalizeDictionary.ContainsKey("š")) normalizeDictionary.Add("š", "s");
            if (!normalizeDictionary.ContainsKey("Ð")) normalizeDictionary.Add("Ð", "Dj");
            if (!normalizeDictionary.ContainsKey("ð")) normalizeDictionary.Add("ð", "dj");
            if (!normalizeDictionary.ContainsKey("Ž")) normalizeDictionary.Add("Ž", "Z");
            if (!normalizeDictionary.ContainsKey("ž")) normalizeDictionary.Add("ž", "z");
            if (!normalizeDictionary.ContainsKey("Č")) normalizeDictionary.Add("Č", "C");
            if (!normalizeDictionary.ContainsKey("č")) normalizeDictionary.Add("č", "c");
            if (!normalizeDictionary.ContainsKey("Ć")) normalizeDictionary.Add("Ć", "C");
            if (!normalizeDictionary.ContainsKey("ć")) normalizeDictionary.Add("ć", "c");
            if (!normalizeDictionary.ContainsKey("À")) normalizeDictionary.Add("À", "A");
            if (!normalizeDictionary.ContainsKey("Á")) normalizeDictionary.Add("Á", "A");
            if (!normalizeDictionary.ContainsKey("Â")) normalizeDictionary.Add("Â", "A");
            if (!normalizeDictionary.ContainsKey("Ã")) normalizeDictionary.Add("Ã", "A");
            if (!normalizeDictionary.ContainsKey("Ä")) normalizeDictionary.Add("Ä", "A");
            if (!normalizeDictionary.ContainsKey("Å")) normalizeDictionary.Add("Å", "A");
            if (!normalizeDictionary.ContainsKey("Æ")) normalizeDictionary.Add("Æ", "A");
            if (!normalizeDictionary.ContainsKey("Ç")) normalizeDictionary.Add("Ç", "C");
            if (!normalizeDictionary.ContainsKey("È")) normalizeDictionary.Add("È", "E");
            if (!normalizeDictionary.ContainsKey("É")) normalizeDictionary.Add("É", "E");
            if (!normalizeDictionary.ContainsKey("Ê")) normalizeDictionary.Add("Ê", "E");
            if (!normalizeDictionary.ContainsKey("Ë")) normalizeDictionary.Add("Ë", "E");
            if (!normalizeDictionary.ContainsKey("Ì")) normalizeDictionary.Add("Ì", "I");
            if (!normalizeDictionary.ContainsKey("Í")) normalizeDictionary.Add("Í", "I");
            if (!normalizeDictionary.ContainsKey("Î")) normalizeDictionary.Add("Î", "I");
            if (!normalizeDictionary.ContainsKey("Ï")) normalizeDictionary.Add("Ï", "I");
            if (!normalizeDictionary.ContainsKey("Ñ")) normalizeDictionary.Add("Ñ", "N");
            if (!normalizeDictionary.ContainsKey("Ò")) normalizeDictionary.Add("Ò", "O");
            if (!normalizeDictionary.ContainsKey("Ó")) normalizeDictionary.Add("Ó", "O");
            if (!normalizeDictionary.ContainsKey("Ô")) normalizeDictionary.Add("Ô", "O");
            if (!normalizeDictionary.ContainsKey("Õ")) normalizeDictionary.Add("Õ", "O");
            if (!normalizeDictionary.ContainsKey("Ö")) normalizeDictionary.Add("Ö", "O");
            if (!normalizeDictionary.ContainsKey("Ø")) normalizeDictionary.Add("Ø", "O");
            if (!normalizeDictionary.ContainsKey("Ù")) normalizeDictionary.Add("Ù", "U");
            if (!normalizeDictionary.ContainsKey("Ú")) normalizeDictionary.Add("Ú", "U");
            if (!normalizeDictionary.ContainsKey("Û")) normalizeDictionary.Add("Û", "U");
            if (!normalizeDictionary.ContainsKey("Ü")) normalizeDictionary.Add("Ü", "U");
            if (!normalizeDictionary.ContainsKey("Ý")) normalizeDictionary.Add("Ý", "Y");
            if (!normalizeDictionary.ContainsKey("Þ")) normalizeDictionary.Add("Þ", "B");
            if (!normalizeDictionary.ContainsKey("ß")) normalizeDictionary.Add("ß", "Ss");
            if (!normalizeDictionary.ContainsKey("à")) normalizeDictionary.Add("à", "a");
            if (!normalizeDictionary.ContainsKey("á")) normalizeDictionary.Add("á", "a");
            if (!normalizeDictionary.ContainsKey("â")) normalizeDictionary.Add("â", "a");
            if (!normalizeDictionary.ContainsKey("ã")) normalizeDictionary.Add("ã", "a");
            if (!normalizeDictionary.ContainsKey("ä")) normalizeDictionary.Add("ä", "a");
            if (!normalizeDictionary.ContainsKey("å")) normalizeDictionary.Add("å", "a");
            if (!normalizeDictionary.ContainsKey("æ")) normalizeDictionary.Add("æ", "a");
            if (!normalizeDictionary.ContainsKey("ç")) normalizeDictionary.Add("ç", "c");
            if (!normalizeDictionary.ContainsKey("è")) normalizeDictionary.Add("è", "e");
            if (!normalizeDictionary.ContainsKey("é")) normalizeDictionary.Add("é", "e");
            if (!normalizeDictionary.ContainsKey("ê")) normalizeDictionary.Add("ê", "e");
            if (!normalizeDictionary.ContainsKey("ë")) normalizeDictionary.Add("ë", "e");
            if (!normalizeDictionary.ContainsKey("ì")) normalizeDictionary.Add("ì", "i");
            if (!normalizeDictionary.ContainsKey("í")) normalizeDictionary.Add("í", "i");
            if (!normalizeDictionary.ContainsKey("î")) normalizeDictionary.Add("î", "i");
            if (!normalizeDictionary.ContainsKey("ï")) normalizeDictionary.Add("ï", "i");
            if (!normalizeDictionary.ContainsKey("ñ")) normalizeDictionary.Add("ñ", "n");
            if (!normalizeDictionary.ContainsKey("ò")) normalizeDictionary.Add("ò", "o");
            if (!normalizeDictionary.ContainsKey("ó")) normalizeDictionary.Add("ó", "o");
            if (!normalizeDictionary.ContainsKey("ô")) normalizeDictionary.Add("ô", "o");
            if (!normalizeDictionary.ContainsKey("õ")) normalizeDictionary.Add("õ", "o");
            if (!normalizeDictionary.ContainsKey("ö")) normalizeDictionary.Add("ö", "o");
            if (!normalizeDictionary.ContainsKey("ø")) normalizeDictionary.Add("ø", "o");
            if (!normalizeDictionary.ContainsKey("ù")) normalizeDictionary.Add("ù", "u");
            if (!normalizeDictionary.ContainsKey("ú")) normalizeDictionary.Add("ú", "u");
            if (!normalizeDictionary.ContainsKey("û")) normalizeDictionary.Add("û", "u");
            if (!normalizeDictionary.ContainsKey("ý")) normalizeDictionary.Add("ý", "y");
            if (!normalizeDictionary.ContainsKey("þ")) normalizeDictionary.Add("þ", "b");
            if (!normalizeDictionary.ContainsKey("ÿ")) normalizeDictionary.Add("ÿ", "y");
            if (!normalizeDictionary.ContainsKey("Ŕ")) normalizeDictionary.Add("Ŕ", "R");
            if (!normalizeDictionary.ContainsKey("ŕ")) normalizeDictionary.Add("ŕ", "r");
            if (!normalizeDictionary.ContainsKey("`")) normalizeDictionary.Add("`", "'");
            if (!normalizeDictionary.ContainsKey("´")) normalizeDictionary.Add("´", "'");
            if (!normalizeDictionary.ContainsKey("„")) normalizeDictionary.Add("„", ",");
            if (!normalizeDictionary.ContainsKey("“")) normalizeDictionary.Add("“", "\\");
            if (!normalizeDictionary.ContainsKey("”")) normalizeDictionary.Add("”", "\\");
            if (!normalizeDictionary.ContainsKey("&acirc;€™")) normalizeDictionary.Add("&acirc;€™", "'");
            if (!normalizeDictionary.ContainsKey("{")) normalizeDictionary.Add("{", "");
            if (!normalizeDictionary.ContainsKey("~")) normalizeDictionary.Add("~", "");
            if (!normalizeDictionary.ContainsKey("–")) normalizeDictionary.Add("–", "-");
            if (!normalizeDictionary.ContainsKey("’")) normalizeDictionary.Add("’", "'");
            if (!normalizeDictionary.ContainsKey(">")) normalizeDictionary.Add(">", " ");
            if (!normalizeDictionary.ContainsKey("<")) normalizeDictionary.Add("<", " ");
            if (!normalizeDictionary.ContainsKey(";")) normalizeDictionary.Add(";", " ");
            if (!normalizeDictionary.ContainsKey("^")) normalizeDictionary.Add("^", " ");
            if (!normalizeDictionary.ContainsKey("µ")) normalizeDictionary.Add("µ", "u");
            if (!normalizeDictionary.ContainsKey("/")) normalizeDictionary.Add("/", " ");
            if (!normalizeDictionary.ContainsKey("\\")) normalizeDictionary.Add("\\", " ");
            if (!normalizeDictionary.ContainsKey("ü")) normalizeDictionary.Add("ü", "u");
            if (!normalizeDictionary.ContainsKey("(")) normalizeDictionary.Add("(", "");
            if (!normalizeDictionary.ContainsKey(")")) normalizeDictionary.Add(")", "");

            foreach (var kvp in normalizeDictionary.Where(x => s.Contains(x.Key))) s = s.Replace(kvp.Key, kvp.Value);
            return s;
        }

        public PaymentMethodType PaymentMethodType
        {
            get { return PaymentMethodType.Redirection; }
        }

        #region nopCommerce plugin methods

        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            var client = new SipsClient();
            var order = postProcessPaymentRequest.Order;

            // 1. Generate a unique identifier for this transaction (so you keep track of the transaction history)
            var transactionReference = GetTransactionReference(client, order);

            // 2. Create and initialize a PaymentRequest
            var merchantId = _worldlinePaymentSettings.Account; // Use "002001000000001" if you're using the Sandbox uri
            var interfaceVersion = _worldlinePaymentSettings.InterfaceVersion; // "IR_WS_2.14" is the current interface version
            var normalReturnUrl = _worldlinePaymentSettings.NormalReturnUrl;
            var automaticResponseUrl = _worldlinePaymentSettings.NotificationUrl;
            var paymentRequest = client.GetPaymentRequest(merchantId, interfaceVersion, transactionReference, normalReturnUrl, automaticResponseUrl, customerIpAddress: _webHelper.GetCurrentIpAddress());

            // 3. Set your PaymentRequest data (not Seal, SealAlgorithm, Key & KeyVersion)
            paymentRequest.SetCustomer(order.Customer.Id.ToString(), _languageService.GetLanguageById(order.CustomerLanguageId)?.LanguageCulture.Split('-')[0]);
            var billingContact = new Contact()
            {
                Firstname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.FirstName)),
                Lastname = Normalize(order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.LastName)),
                Phone = order.Customer.GetAttribute<string>(SystemCustomerAttributeNames.Phone),
                Email = order.Customer.Email
            };
            var billingAddress = new Address()
            {
                Street = string.Empty,
                Company = string.Empty,
                City = string.Empty,
                ZipCode = string.Empty,
                Country = string.Empty,
                State = string.Empty
            };
            paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Customer);
            paymentRequest.SetAddressAndContactInfo(billingContact, billingAddress, AddressType.Billing);

            if (order.ShippingAddress != null)
            {
                //Custom Code - 09112017 - commented as we will not send address
                //var shippingContact = new Contact()
                //{
                //    Firstname = Normalize(order.ShippingAddress.FirstName),
                //    Lastname = Normalize(order.ShippingAddress.LastName),
                //    Phone = order.ShippingAddress.PhoneNumber,
                //    Email = order.ShippingAddress.Email
                //};
                //var shippingAddress = new Address()
                //{
                //    Street = Normalize(string.Format("{0} {1}", order.ShippingAddress.Address1, order.ShippingAddress.Address2).Trim()),
                //    Company = Normalize(order.ShippingAddress.Company),
                //    City = Normalize(order.ShippingAddress.City),
                //    ZipCode = Normalize(order.ShippingAddress.ZipPostalCode.Length > 10 ? order.ShippingAddress.ZipPostalCode.Substring(0, 10) : order.ShippingAddress.ZipPostalCode),
                //    Country = order.ShippingAddress.Country?.ThreeLetterIsoCode,
                //    State = order.ShippingAddress.StateProvince?.Name
                //};
                //paymentRequest.SetAddressAndContactInfo(shippingContact, shippingAddress, AddressType.Delivery);
            }

            // Worldline only accepts value in smallest unit of currency
            paymentRequest.SetOrderDetails(order.Id.ToString(), (int)Math.Round(order.OrderTotal * 100), order.CustomerCurrencyCode);
            //foreach (var item in order.OrderItems)
            //{
            //    var taxAmount = (item.UnitPriceInclTax - item.UnitPriceExclTax) * 100;
            //    paymentRequest.ShoppingCartDetail.ShoppingCartItemList.Add(new ShoppingCartItem()
            //    {
            //        ProductSKU = item.Product.Sku,
            //        ProductName = item.Product.Name,
            //        ProductQuantity = item.Quantity.ToString(),
            //        ProductUnitAmount = ((int)Math.Round(item.UnitPriceExclTax * 100)).ToString(),
            //        ProductUnitTaxAmount = taxAmount >= 1 ? ((int)Math.Round(taxAmount)).ToString() : string.Empty
            //    });
            //}

            // 4. Now set Seal, SealAlgorithm, Key & KeyVersion for your PaymentRequest
            var secretKey = _worldlinePaymentSettings.SecretKey;
            if (!string.IsNullOrEmpty(_worldlinePaymentSettings.SealAlgorithm))
                paymentRequest.SetSealAndKeyVersion(secretKey, sealAlgorithm: _worldlinePaymentSettings.SealAlgorithm); // Use "002001000000001_KEY1" if you're using the Sandbox uri
            else
                paymentRequest.SetSealAndKeyVersion(secretKey);

            // 5. Send your PaymentRequest to Worldline & receive a RedirectionModel (with redirection Uri and data)
            var redirection = client.SendPaymentRequest(paymentRequest, _worldlinePaymentSettings.UseSandbox);

            // 6. If the payment request was successful (RedirectionStatusCode == "00") redirect to Worldline
            if (redirection != null && redirection.RedirectionStatusCode == "00")
                client.RedirectToWorldline(redirection);
            // 7. If the payment request was not succesful show an error message
            else if (redirection != null)
            {
                var errorMessage = string.Empty;
                switch (redirection.RedirectionStatusCode)
                {
                    case "30": errorMessage = "Request format is not valid."; break;
                    case "34": errorMessage = "There is a security problem: for example, the calculated seal is incorrect."; break;
                    case "94": errorMessage = "Transaction already exists."; break;
                    case "99": errorMessage = "Service temporarily unavailable."; break;
                    default: errorMessage = redirection.RedirectionStatusMessage; break;
                }
                _logger.Information("Error in Worldline Payment Provider, Status code: " + redirection.RedirectionStatusCode + ", Message: " + errorMessage);
            }
            else
                _logger.Information("Error in Worldline Payment Provider: Unable to retrieve a redirection model.");
        }

        private string GetTransactionReference(SipsClient client, Core.Domain.Orders.Order order)
        {
            var prefix = _worldlinePaymentSettings.TransactionReferencePrefix;
            var transactionId = Guid.NewGuid().ToString().Replace("-", "");

            // Save transactionId for order (only one per order, so the new transactionId will replace the old one)
            var orderTransactionIDAttribute = _genericAttributeService.GetAttributesForEntity(order.Id, "Order")?.FirstOrDefault(x => x.Key == "TransactionID");
            if (orderTransactionIDAttribute != null)
            {
                orderTransactionIDAttribute.Value = transactionId;
                _genericAttributeService.UpdateAttribute(orderTransactionIDAttribute);
            }
            else
            {
                _genericAttributeService.SaveAttribute<string>(order, "TransactionID", transactionId);
            }

            var transactionReference = string.Format("{0}{1}", prefix, transactionId);
            return transactionReference.Substring(0, Math.Min(35, transactionReference.Length));
        }

        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            return new ProcessPaymentResult { NewPaymentStatus = PaymentStatus.Pending };
        }

        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();

            result.AddError("Recurring payment not supported");

            return result;
        }

        public RecurringPaymentType RecurringPaymentType
        {
            get { return RecurringPaymentType.NotSupported; }
        }

        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();

            result.AddError("Refund method not supported");

            return result;
        }

        public bool SkipPaymentInfo
        {
            get { return true; }
        }

        public bool SupportCapture
        {
            get { return false; }
        }

        public bool SupportPartiallyRefund
        {
            get { return false; }
        }

        public bool SupportRefund
        {
            get { return false; }
        }

        public bool SupportVoid
        {
            get { return false; }
        }

        public string PaymentMethodDescription
        {
            //return description of this payment method to be display on "payment method" checkout step. good practice is to make it localizable
            //for example, for a redirection payment method, description may be like this: "You will be redirected to PayPal site to complete the payment"
            get { return _localizationService.GetResource("Plugins.Payment.Worldline.PaymentMethodDescription"); }
        }

        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();

            result.AddError("Void method not supported");

            return result;
        }



        #endregion

        public bool CanRePostProcessPayment(Core.Domain.Orders.Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException("order");
            }

            //let's ensure that at least 5 seconds passed after order is placed
            //P.S. there's no any particular reason for that. we just do it
            return !((DateTime.UtcNow - order.CreatedOnUtc).TotalSeconds < 5);
        }

        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();

            result.AddError("Recurring payment not supported");

            return result;
        }

        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();

            result.AddError("Capture method not supported");

            return result;
        }

        public decimal GetAdditionalHandlingFee(IList<Core.Domain.Orders.ShoppingCartItem> cart)
        {
            return 0m;
        }

        public bool HidePaymentMethod(IList<Core.Domain.Orders.ShoppingCartItem> cart)
        {
            return false;
        }
    }
}