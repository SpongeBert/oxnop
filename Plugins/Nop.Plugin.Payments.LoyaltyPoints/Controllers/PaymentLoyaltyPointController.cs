﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Payments;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Payments.LoyaltyPoints.Controllers
{
	public class PaymentLoyaltyPointController : BasePaymentController
	{		
		public PaymentLoyaltyPointController(IWorkContext workContext,
			 IStoreService storeService,
			 ISettingService settingService,
			 ILocalizationService localizationService)
		{
			
		}

		[AdminAuthorize]
		[ChildActionOnly]
		public ActionResult Configure()
		{				
			return View("~/Plugins/Payments.LoyaltyPoints/Views/Configure.cshtml");
		}

		[HttpPost]
		[AdminAuthorize]
		[ChildActionOnly]
		public ActionResult Configure(string test)
		{
			return View("~/Plugins/Payments.LoyaltyPoints/Views/Configure.cshtml");
		}
		 
		[ChildActionOnly]
		public ActionResult PaymentInfo()
		{
			return View("~/Plugins/Payments.LoyaltyPoints/Views/PaymentInfo.cshtml");
		}
				 
		[NonAction]
		public override IList<string> ValidatePaymentForm(FormCollection form)
		{
			var warnings = new List<string>();
			return warnings;
		}		 

		[NonAction]
		public override ProcessPaymentRequest GetPaymentInfo(FormCollection form)
		{
			var paymentInfo = new ProcessPaymentRequest();
			return paymentInfo;
		}		 
	}
}