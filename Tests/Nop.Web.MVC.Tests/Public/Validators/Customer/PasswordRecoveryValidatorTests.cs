﻿using FluentValidation.TestHelper;
using Nop.Web.Models.Customer;
using Nop.Web.Validators.Customer;
using NUnit.Framework;

namespace Nop.Web.MVC.Tests.Public.Validators.Customer
{
    [TestFixture]
    public class PasswordRecoveryValidatorTests : BaseValidatorTests
    {
        private PasswordRecoveryValidator _validator;
        
        [SetUp]
        public new void Setup()
        {
            _validator = new PasswordRecoveryValidator(_localizationService);
        }
        
        [Test]
        public void Should_have_error_when_email_is_null_or_empty()
        {
            var model = new PasswordRecoveryModel();
            model.Username = null;
            _validator.ShouldHaveValidationErrorFor(x => x.Username, model);
            model.Username = "";
            _validator.ShouldHaveValidationErrorFor(x => x.Username, model);
        }

        [Test]
        public void Should_have_error_when_email_is_wrong_format()
        {
            var model = new PasswordRecoveryModel();
            model.Username = "adminexample.com";
            _validator.ShouldHaveValidationErrorFor(x => x.Username, model);
        }

        [Test]
        public void Should_not_have_error_when_email_is_correct_format()
        {
            var model = new PasswordRecoveryModel();
            model.Username = "admin@example.com";
            _validator.ShouldNotHaveValidationErrorFor(x => x.Username, model);
        }
    }
}
