﻿using Nop.Core.Domain.Messages;
using Nop.Services.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Data;
using Nop.Services.Orders;
using Nop.Services.Helpers;

namespace Nop.Services.CustomService
{
	public partial class CustomCodeService : ICustomCodeService
	{
		#region Fields
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly IEmailAccountService _emailAccountService;
		private readonly EmailAccountSettings _emailAccountSettings;
		private readonly IQueuedEmailService _queuedEmailService;
		private readonly IRepository<Customer> _customerRepository;
		private readonly IWorkContext _workContext;
		private readonly IOrderService _orderService;
		private readonly IDateTimeHelper _dateTimeHelper;
		#endregion

		#region Ctor
		public CustomCodeService(IWorkflowMessageService workflowMessageService, IEmailAccountService emailAccountService, EmailAccountSettings emailAccountSettings,
				IQueuedEmailService queuedEmailService, IRepository<Customer> customerRepository, IWorkContext workContext, IOrderService orderService,
			IDateTimeHelper dateTimeHelper)
		{
			_workflowMessageService = workflowMessageService;
			_emailAccountService = emailAccountService;
			_emailAccountSettings = emailAccountSettings;
			_queuedEmailService = queuedEmailService;
			_customerRepository = customerRepository;

			_workContext = workContext;
			_orderService = orderService;
			_dateTimeHelper = dateTimeHelper;
		}
		#endregion

		public virtual void SendErrorEmail(string subject, string errorMessage, string stackTrace)
		{
			var emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault(x => x.Id == _emailAccountSettings.DefaultEmailAccountId);

			if (emailAccount != null)
			{
				var email = new QueuedEmail
				{
					Priority = QueuedEmailPriority.High,
					From = emailAccount.Email,
					FromName = emailAccount.DisplayName,
					To = "oxcfe@webshopcompany.be",
					ToName = "Admin",
					ReplyTo = "oxcfe@webshopcompany.be",
					ReplyToName = "Bert",
					CC = string.Empty,
					Bcc = "dev.wsc.test@gmail.com",
					Subject = subject,
					Body = errorMessage + "  ||  " + stackTrace,
					AttachmentFilePath = null,
					AttachmentFileName = null,
					AttachedDownloadId = 0,
					CreatedOnUtc = DateTime.UtcNow,
					EmailAccountId = emailAccount.Id,
					DontSendBeforeDateUtc = null
				};

				_queuedEmailService.InsertQueuedEmail(email);
			}
		}


		public void SendInformationEmail(string subject, string infoMessage)
		{
			var emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault(x => x.Id == _emailAccountSettings.DefaultEmailAccountId);

			if (emailAccount != null)
			{
				var email = new QueuedEmail
				{
					Priority = QueuedEmailPriority.High,
					From = emailAccount.Email,
					FromName = emailAccount.DisplayName,
					To = "oxcfe@webshopcompany.be",
					ToName = "Admin",
					ReplyTo = "oxcfe@webshopcompany.be",
					ReplyToName = "Bert",
					CC = string.Empty,
					Bcc = "dev.wsc.test@gmail.com",
					Subject = subject,
					Body = infoMessage,
					AttachmentFilePath = null,
					AttachmentFileName = null,
					AttachedDownloadId = 0,
					CreatedOnUtc = DateTime.UtcNow,
					EmailAccountId = emailAccount.Id,
					DontSendBeforeDateUtc = null
				};

				_queuedEmailService.InsertQueuedEmail(email);
			}
		}



		#region Custom Generic Attribute Methods

		public List<Customer> GetAllChildCustomerInfoByCompanyId(string companyId)
		{
			var query = _customerRepository.Table.Where(x =>
				!string.IsNullOrEmpty(x.Username) && x.Username.StartsWith(companyId));
			return query.ToList();
		}

		#endregion

		#region Check Customer Order Count for Day

		public bool CheckCustomerOrderLimit()
		{

			if (string.IsNullOrEmpty(_workContext.CurrentCustomer.Username))
				return true;

			if (!_workContext.CurrentCustomer.Username.StartsWith("009"))
				return true;

			var date = _dateTimeHelper.ConvertToUtcTime(DateTime.UtcNow.Date, TimeZoneInfo.Utc);


			var paymentStatus = new List<int> {30};

			var customerOrders = _orderService.SearchOrders(customerId: _workContext.CurrentCustomer.Id,	psIds:paymentStatus,createdFromUtc: date);

			if (customerOrders.Any())
			{
				return false;

			}

			return true;
		}

		#endregion
	}
}
