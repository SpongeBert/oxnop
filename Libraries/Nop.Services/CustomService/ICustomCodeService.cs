﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;

namespace Nop.Services.CustomService
{
    public partial interface ICustomCodeService
    {
        void SendErrorEmail(string subject, string errorMessage, string stackTrace);

        void SendInformationEmail(string subject, string infoMessage);

	    List<Customer> GetAllChildCustomerInfoByCompanyId(string companyId);


	    bool CheckCustomerOrderLimit();
    }
}
