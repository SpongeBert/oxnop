﻿using Nop.Core.Domain.Catalog;

namespace Nop.Data.Mapping.Catalog
{		
	public partial class ProductCompanyMap : NopEntityTypeConfiguration<ProductCompany>
	{
		public ProductCompanyMap()
		{
			this.ToTable("ProductCompanyMapping");
			this.HasKey(pc => pc.Id);

			this.HasRequired(pc => pc.Customer)
				 .WithMany()
				 .HasForeignKey(pc => pc.CustomerId);


			this.HasRequired(pc => pc.Product)
				 .WithMany(p => p.ProductCompanyMapping)
				 .HasForeignKey(pc => pc.ProductId);
		}
	}
}
