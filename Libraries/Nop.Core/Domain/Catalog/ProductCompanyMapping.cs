﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;

namespace Nop.Core.Domain.Catalog
{
	public partial class ProductCompany : BaseEntity
	{
		/// <summary>
		/// Gets or sets the product identifier
		/// </summary>
		public int ProductId { get; set; }

		/// <summary>
		/// Gets or sets the customer identifier
		/// </summary>
		public int CustomerId { get; set; }


		/// <summary>
		/// Gets the customer
		/// </summary>
		public virtual Customer Customer { get; set; }

		/// <summary>
		/// Gets the product
		/// </summary>
		public virtual Product Product { get; set; }
	}
}
