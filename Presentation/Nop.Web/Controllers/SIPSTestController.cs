﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Nop.Core.Domain.Logging;
using Nop.Services.Logging;

namespace Nop.Web.Controllers
{
	public class SIPSTestController : Controller
	{
		private readonly ILogger _logger;

		public SIPSTestController(ILogger logger)
		{
			_logger = logger;
		}
		// GET: SIPSTest
		public ActionResult Index()
		{
			var transactionReference = Guid.NewGuid().ToString().Replace("-", "");
			string token = string.Empty;

			string url = @"https://office-server.sips-atos.com/rs-services/v2/checkout/paymentProviderInitialize";
			string postData = "{\"amount\":\"2\", \"captureMode\":\"AUTHOR_CAPTURE\", \"currencyCode\":\"978\", \"interfaceVersion\":\"IR_WS_2.11\", \"keyVersion\":\"1\", \"merchantId\":\"225005036700001\", ";
			postData = postData + "\"merchantReturnUrl\":\"http://www.oxcfe.be/sipstest/GetFinalPost\", \"orderChannel\":\"INTERNET\", \"paymentMeanBrand\":\"BCMCMOBILE\", \"responseKeyVersion\":\"1\",";
			string sealData = @"2AUTHOR_CAPTURE978IR_WS_2.11225005036700001http://www.oxcfe.be/sipstest/GetFinalPostINTERNETBCMCMOBILE1" + transactionReference;
			var seal = GenerateSeal(sealData, "PCv4DO5zG-pBOIfjojGnslk1Q0jG7Wd5iVHHLhk2XXk");
			postData = postData + "\"transactionReference\":\"" + transactionReference + "\", \"seal\":\"" + seal + "\"}";

			var request = (HttpWebRequest)WebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.ContentLength = postData.Length;
			request.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWriter = new StreamWriter(request.GetRequestStream());
			requestWriter.Write(postData);
			requestWriter.Close();

			var responseReader = new StreamReader(request.GetResponse().GetResponseStream());
			var responseData = responseReader.ReadToEnd();

			JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
			var result = jsSerializer.DeserializeObject(responseData);
			Dictionary<string, object> obj2 = new Dictionary<string, object>();
			obj2 = (Dictionary<string, object>)(result);

			string redirectData = obj2["redirectionData"].ToString();
			string redirectUrl = obj2["redirectionUrl"].ToString();

			var model = new SipsTestModel { buttonCode = redirectUrl };

			return View(model);
		}


		public object GetFinalPost(string data, string seal)
		{
			_logger.Information("FinalPost step1 received on :: " + DateTime.Now);
			_logger.InsertLog(LogLevel.Information, "FinalPost step1 received on :: " + DateTime.Now, "data=" + data + " :: seal=" + seal);

			var base64EncodedBytes = System.Convert.FromBase64String(data);
			var dataString = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
			_logger.InsertLog(LogLevel.Information, "FinalPost step2 received on :: " + DateTime.Now, "dataString=" + dataString);

			var dataArray = dataString.Split('|');
			var redirectionDataA = dataArray.FirstOrDefault(x => x.Contains("redirectionData")).Split('=');
			var transactionReferenceA = dataArray.FirstOrDefault(x => x.Contains("transactionReference")).Split('=');
			_logger.InsertLog(LogLevel.Information, "FinalPost step3 received on :: " + DateTime.Now, "redirectionData=" + redirectionDataA[1] + " :: transactionReference=" + transactionReferenceA[1]);

			string redirectionData = redirectionDataA[1];
			string transactionReference = transactionReferenceA[1];

			string urlt = @"https://office-server.sips-atos.com/rs-services/v2/checkout/paymentProviderFinalize";
			string postDatat = "{\"interfaceVersion\":\"IR_WS_2.11\", \"keyVersion\":\"1\", \"merchantId\":\"225005036700001\",";
			postDatat = postDatat + "\"messageVersion\":\"0.1\", \"redirectionData\":\"" + redirectionData + "\",";
			string vsealData = @"IR_WS_2.112250050367000010.1" + redirectionData + transactionReference;
			var vseal = GenerateSeal(vsealData, "PCv4DO5zG-pBOIfjojGnslk1Q0jG7Wd5iVHHLhk2XXk");
			postDatat = postDatat + "\"transactionReference\":\"" + transactionReference + "\", \"seal\":\"" + vseal + "\"}";

			var requestt = (HttpWebRequest)WebRequest.Create(urlt);
			requestt.ContentType = "application/json";
			requestt.Method = "POST";
			requestt.ContentLength = postDatat.Length;
			requestt.AutomaticDecompression = DecompressionMethods.GZip;

			var requestWritert = new StreamWriter(requestt.GetRequestStream());
			requestWritert.Write(postDatat);
			requestWritert.Close();

			var responseReadert = new StreamReader(requestt.GetResponse().GetResponseStream());
			var responseDatat = responseReadert.ReadToEnd();
			_logger.InsertLog(LogLevel.Information, "FinalPost step4 received on :: " + DateTime.Now, responseDatat);
			return "";
		}

		public string GenerateSeal(string data, string key)
		{
			string sealAlgorithm = "HMAC-SHA-256";
			var rr = "";
			UTF8Encoding utF8Encoding = new UTF8Encoding();
			byte[] bytes = utF8Encoding.GetBytes(data);
			HMAC hmac = sealAlgorithm == "HMAC-SHA-1" ? (HMAC)new HMACSHA1() : (sealAlgorithm == "HMAC-SHA-384" ? (HMAC)new HMACSHA384() : (sealAlgorithm == "HMAC-SHA-512" ? (HMAC)new HMACSHA512() : (HMAC)new HMACSHA256()));
			hmac.Key = utF8Encoding.GetBytes(key);
			hmac.Initialize();
			return this._byteArrayToHEX(hmac.ComputeHash(bytes));
		}

		private string _byteArrayToHEX(byte[] ba)
		{
			StringBuilder stringBuilder = new StringBuilder(ba.Length * 2);
			foreach (byte num in ba)
				stringBuilder.AppendFormat("{0:x2}", (object)num);
			return stringBuilder.ToString();
		}

	}

	public class SipsTestModel
	{
		public string buttonCode { get; set; }
	}
}