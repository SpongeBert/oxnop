﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Models.CustomCode
{
	public class HomePageSubCategoryModel
	{
		public HomePageSubCategoryModel()
		{
			SubCategoryList = new List<SubCategoryDetail>();
		}

		public int ParentCategoryId { get; set; }

		public List<SubCategoryDetail> SubCategoryList { get; set; }
	}

	public class SubCategoryDetail
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public int DisplayOrder { get; set; }


	}

	public class HomePageProducts
	{
		public HomePageProducts()
		{
			Products = new List<ProductOverviewModel>();
		}
		public int CategoryId { get; set; }
		public List<ProductOverviewModel> Products { get; set; }

	}
}